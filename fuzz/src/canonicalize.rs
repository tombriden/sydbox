//
// syd: seccomp and landlock based application sandbox with support for namespaces
// fuzz/src/canonicalize.rs: Fuzz target for path canonicalization
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{ffi::OsString, os::unix::ffi::OsStringExt};

use syd::fs::{canonicalize, MissingHandling};

fn main() {
    afl::fuzz!(|data: &[u8]| {
        // Turn the fuzz input into an OsString.
        let path = OsString::from_vec(data.to_vec());

        // Call the canonicalize function with various different arguments.
        let _ = canonicalize(&path, false, MissingHandling::Normal);
        let _ = canonicalize(&path, true, MissingHandling::Normal);
        let _ = canonicalize(&path, false, MissingHandling::Existing);
        let _ = canonicalize(&path, true, MissingHandling::Existing);
        let _ = canonicalize(&path, false, MissingHandling::Missing);
        let _ = canonicalize(&path, true, MissingHandling::Missing);
    });
}
