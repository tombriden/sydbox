module goshell

go 1.21.1

require (
	git.sr.ht/~alip/syd/lib/src v0.0.0-20231214122200-dc104d0bc00c
	github.com/tmthrgd/tmpfile v0.0.0-20190904054337-6ce9e75706ab
)

require (
	github.com/tmthrgd/atomics v0.0.0-20190904060638-dc7a5fcc7e0d // indirect
	golang.org/x/sys v0.15.0 // indirect
)
