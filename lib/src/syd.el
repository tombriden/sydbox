;;; -*- lexical-binding: t -*-
;;;
;;; syd.el --- Emacs Lisp implementation of the syd stat interface
;;;
;;; syd: seccomp and landlock based application sandbox with support for namespaces
;;; lib/src/syd.el: Emacs Lisp implementation of the syd stat interface
;;;
;;; Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
;;;
;;; SPDX-License-Identifier: GPL-3.0-or-later

(defun syd-info ()
  "Reads the state of the syd sandbox from /dev/syd and returns it as an alist.
If the `json' module is not available, returns nil."
  (if (require 'json nil t)
      (condition-case nil
          (with-temp-buffer
            (insert-file-contents "/dev/syd")
            (with-no-warnings
              (let ((json-object-type 'alist)
                    (json-array-type 'list)
                    (json-key-type 'symbol)
                    (json-false nil)
                    (json-null nil))
                (json-read))))
        (file-error
         (message "Error reading /dev/syd.")
         nil)
        (json-error
         (message "JSON decoding error.")
         nil))
    (progn
      (message "JSON module not available.")
      nil)))

(defun syd-api ()
  "Performs a syd API check."
  (if (syd--stat "/dev/syd/3")
      3   ; API number on success
    nil)) ; On error, return nil

(defun syd-check ()
  "Check if '/dev/syd' is a character device."
  (syd--stat "/dev/syd"))

(defun syd-panic ()
  "Causes syd to exit immediately with code 127"
  (syd--stat "/dev/syd/panic"))

(defun syd-reset ()
  "Causes syd to reset sandboxing to the default state."
  (syd--stat "/dev/syd/reset"))

(defun syd-load (fd)
  "Causes syd to read configuration from the given file descriptor FD."
  (let ((path (concat "/dev/syd/load/" (number-to-string fd))))
    (syd--stat path)))

; Define lock states as keywords
(defconst syd-lock-off :lock-off
  "The sandbox lock is off, allowing all sandbox commands.")

(defconst syd-lock-exec :lock-exec
  "The sandbox lock is set to on for all processes except the initial process
\(syd exec child). This is the default state.")

(defconst syd-lock-on :lock-on
  "The sandbox lock is on, disallowing all sandbox commands.")

(defun syd-lock (state)
  "Sets the state of the sandbox lock.
STATE is one of the keywords :lock-off, :lock-exec, or :lock-on.
Returns t on success, nil on failure."
  (cond
   ((eq state syd-lock-off) (syd--stat "/dev/syd/lock:off"))
   ((eq state syd-lock-exec) (syd--stat "/dev/syd/lock:exec"))
   ((eq state syd-lock-on) (syd--stat "/dev/syd/lock:on"))
   (t nil))) ; Invalid state

(defun syd-enabled-mem ()
  "Checks if memory sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/mem?"))

(defun syd-enable-mem ()
  "Enable memory sandboxing."
  (syd--stat "/dev/syd/sandbox/mem:on"))

(defun syd-disable-mem ()
  "Disable memory sandboxing."
  (syd--stat "/dev/syd/sandbox/mem:off"))

(defun syd-enabled-pid ()
  "Checks if PID sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/pid?"))

(defun syd-enable-pid ()
  "Enable PID sandboxing."
  (syd--stat "/dev/syd/sandbox/pid:on"))

(defun syd-disable-pid ()
  "Disable PID sandboxing."
  (syd--stat "/dev/syd/sandbox/pid:off"))

(defun syd-enabled-read ()
  "Checks if Read sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/read?"))

(defun syd-enable-read ()
  "Enable Read sandboxing."
  (syd--stat "/dev/syd/sandbox/read:on"))

(defun syd-disable-read ()
  "Disable Read sandboxing."
  (syd--stat "/dev/syd/sandbox/read:off"))

(defun syd-enabled-stat ()
  "Checks if Stat sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/stat?"))

(defun syd-enable-stat ()
  "Enable Stat sandboxing."
  (syd--stat "/dev/syd/sandbox/stat:on"))

(defun syd-disable-stat ()
  "Disable Stat sandboxing."
  (syd--stat "/dev/syd/sandbox/stat:off"))

(defun syd-enabled-write ()
  "Checks if Write sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/write?"))

(defun syd-enable-write ()
  "Enable Write sandboxing."
  (syd--stat "/dev/syd/sandbox/write:on"))

(defun syd-disable-write ()
  "Disable Write sandboxing."
  (syd--stat "/dev/syd/sandbox/write:off"))

(defun syd-enabled-exec ()
  "Checks if Exec sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/exec?"))

(defun syd-enable-exec ()
  "Enable Exec sandboxing."
  (syd--stat "/dev/syd/sandbox/exec:on"))

(defun syd-disable-exec ()
  "Disable Exec sandboxing."
  (syd--stat "/dev/syd/sandbox/exec:off"))

(defun syd-enabled-net ()
  "Checks if Network sandboxing is enabled."
  (syd--stat "/dev/syd/sandbox/net?"))

(defun syd-enable-net ()
  "Enable Network sandboxing."
  (syd--stat "/dev/syd/sandbox/net:on"))

(defun syd-disable-net ()
  "Disable Network sandboxing."
  (syd--stat "/dev/syd/sandbox/net:off"))

(defun syd-kill-add (glob)
  "Adds to the list of glob patterns to kill in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "exec/kill" glob ?+)))

(defun syd-kill-del (glob)
  "Deletes the first matching item from the end of the list of glob patterns
to kill in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "exec/kill" glob ?-)))

(defun syd-kill-rem (glob)
  "Removes all matching items from the list of glob patterns
to kill in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "exec/kill" glob ?^)))

(defun syd-allow-read-add (glob)
  "Adds to the allowlist of read sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "allow/read" glob ?+)))

(defun syd-allow-read-del (glob)
  "Removes the first instance from the end of the allowlist of read sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "allow/read" glob ?-)))

(defun syd-allow-read-rem (glob)
  "Removes all matching items from the list of glob patterns
to allow-read in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "allow/read" glob ?^)))

(defun syd-deny-read-add (glob)
  "Adds to the denylist of read sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "deny/read" glob ?+)))

(defun syd-deny-read-del (glob)
  "Removes the first instance from the end of the denylist of read sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "deny/read" glob ?-)))

(defun syd-deny-read-rem (glob)
  "Removes all matching items from the list of glob patterns
to deny-read in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "deny/read" glob ?^)))

(defun syd-filter-read-add (glob)
  "Adds to the filter of read sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "filter/read" glob ?+)))

(defun syd-filter-read-del (glob)
  "Removes the first instance from the end of the filter of read sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "filter/read" glob ?-)))

(defun syd-filter-read-rem (glob)
  "Removes all matching items from the list of glob patterns
to filter-read in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "filter/read" glob ?^)))

(defun syd-allow-stat-add (glob)
  "Adds to the allowlist of stat sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "allow/stat" glob ?+)))

(defun syd-allow-stat-del (glob)
  "Removes the first instance from the end of the allowlist of stat sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "allow/stat" glob ?-)))

(defun syd-allow-stat-rem (glob)
  "Removes all matching items from the list of glob patterns
to allow-stat in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "allow/stat" glob ?^)))

(defun syd-deny-stat-add (glob)
  "Adds to the denylist of stat sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "deny/stat" glob ?+)))

(defun syd-deny-stat-del (glob)
  "Removes the first instance from the end of the denylist of stat sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "deny/stat" glob ?-)))

(defun syd-deny-stat-rem (glob)
  "Removes all matching items from the list of glob patterns
to deny-stat in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "deny/stat" glob ?^)))

(defun syd-filter-stat-add (glob)
  "Adds to the filter of stat sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "filter/stat" glob ?+)))

(defun syd-filter-stat-del (glob)
  "Removes the first instance from the end of the filter of stat sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "filter/stat" glob ?-)))

(defun syd-filter-stat-rem (glob)
  "Removes all matching items from the list of glob patterns
to filter-stat in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "filter/stat" glob ?^)))

(defun syd-allow-write-add (glob)
  "Adds to the allowlist of write sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "allow/write" glob ?+)))

(defun syd-allow-write-del (glob)
  "Removes the first instance from the end of the allowlist of write sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "allow/write" glob ?-)))

(defun syd-allow-write-rem (glob)
  "Removes all matching items from the list of glob patterns
to allow-write in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "allow/write" glob ?^)))

(defun syd-deny-write-add (glob)
  "Adds to the denylist of write sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "deny/write" glob ?+)))

(defun syd-deny-write-del (glob)
  "Removes the first instance from the end of the denylist of write sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "deny/write" glob ?-)))

(defun syd-deny-write-rem (glob)
  "Removes all matching items from the list of glob patterns
to deny-write in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "deny/write" glob ?^)))

(defun syd-filter-write-add (glob)
  "Adds to the filter of write sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "filter/write" glob ?+)))

(defun syd-filter-write-del (glob)
  "Removes the first instance from the end of the filter of write sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "filter/write" glob ?-)))

(defun syd-filter-write-rem (glob)
  "Removes all matching items from the list of glob patterns
to filter-write in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "filter/write" glob ?^)))

(defun syd-allow-exec-add (glob)
  "Adds to the allowlist of exec sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "allow/exec" glob ?+)))

(defun syd-allow-exec-del (glob)
  "Removes the first instance from the end of the allowlist of exec sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "allow/exec" glob ?-)))

(defun syd-allow-exec-rem (glob)
  "Removes all matching items from the list of glob patterns
to allow-exec in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "allow/exec" glob ?^)))

(defun syd-deny-exec-add (glob)
  "Adds to the denylist of exec sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "deny/exec" glob ?+)))

(defun syd-deny-exec-del (glob)
  "Removes the first instance from the end of the denylist of exec sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "deny/exec" glob ?-)))

(defun syd-deny-exec-rem (glob)
  "Removes all matching items from the list of glob patterns
to deny-exec in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "deny/exec" glob ?^)))

(defun syd-filter-exec-add (glob)
  "Adds to the filter of exec sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "filter/exec" glob ?+)))

(defun syd-filter-exec-del (glob)
  "Removes the first instance from the end of the filter of exec sandboxing.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "filter/exec" glob ?-)))

(defun syd-filter-exec-rem (glob)
  "Removes all matching items from the list of glob patterns
to filter-exec in the sandbox.
GLOB is a string representing the glob pattern."
  (syd--stat (syd--rule "filter/exec" glob ?^)))

(defun syd-allow-net-bind-add (addr)
  "Adds to the allowlist of net/bind sandboxing.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "allow/net/bind" addr ?+)))

(defun syd-allow-net-bind-del (addr)
  "Removes the first instance from the end of the allowlist of net/bind sandboxing.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "allow/net/bind" addr ?-)))

(defun syd-allow-net-bind-rem (addr)
  "Removes all matching items from the list of address patterns
to allow for net/bind in the sandbox.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "allow/net/bind" addr ?^)))

(defun syd-deny-net-bind-add (addr)
  "Adds to the denylist of net/bind sandboxing.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "deny/net/bind" addr ?+)))

(defun syd-deny-net-bind-del (addr)
  "Removes the first instance from the end of the denylist of net/bind sandboxing.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "deny/net/bind" addr ?-)))

(defun syd-deny-net-bind-rem (addr)
  "Removes all matching items from the list of address patterns
to deny for net/bind in the sandbox.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "deny/net/bind" addr ?^)))

(defun syd-filter-net-bind-add (addr)
  "Adds to the filter of net/bind sandboxing.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "filter/net/bind" addr ?+)))

(defun syd-filter-net-bind-del (addr)
  "Removes the first instance from the end of the filter of net/bind sandboxing.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "filter/net/bind" addr ?-)))

(defun syd-filter-net-bind-rem (addr)
  "Removes all matching items from the list of address patterns
to filter for net/bind in the sandbox.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "filter/net/bind" addr ?^)))

(defun syd-allow-net-connect-add (addr)
  "Adds to the allowlist of net/connect sandboxing.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "allow/net/connect" addr ?+)))

(defun syd-allow-net-connect-del (addr)
  "Removes the first instance from the end of the allowlist
of net/connect sandboxing.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "allow/net/connect" addr ?-)))

(defun syd-allow-net-connect-rem (addr)
  "Removes all matching items from the list of address patterns
to allow for net/connect in the sandbox.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "allow/net/connect" addr ?^)))

(defun syd-deny-net-connect-add (addr)
  "Adds to the denylist of net/connect sandboxing.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "deny/net/connect" addr ?+)))

(defun syd-deny-net-connect-del (addr)
  "Removes the first instance from the end of the denylist
of net/connect sandboxing.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "deny/net/connect" addr ?-)))

(defun syd-deny-net-connect-rem (addr)
  "Removes all matching items from the list of address patterns
to deny for net/connect in the sandbox.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "deny/net/connect" addr ?^)))

(defun syd-filter-net-connect-add (addr)
  "Adds to the filter of net/connect sandboxing.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "filter/net/connect" addr ?+)))

(defun syd-filter-net-connect-del (addr)
  "Removes the first instance from the end of the filter
of net/connect sandboxing.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "filter/net/connect" addr ?-)))

(defun syd-filter-net-connect-rem (addr)
  "Removes all matching items from the list of address patterns
to filter for net/connect in the sandbox.
ADDR is a string representing the address pattern."
  (syd--stat (syd--rule "filter/net/connect" addr ?^)))

(defun syd-mem-max (size)
  "Set syd maximum per-process memory usage limit.
SIZE can be an integer or a string representing the memory limit."
  (let ((size-str (cond ((integerp size) (number-to-string size))
                        ((stringp size) size)
                        (t (error "Size must be an integer or a string")))))
    (syd--stat (syd--rule "mem/max" size-str ?:))))

(defun syd-mem-vm-max (size)
  "Set syd maximum per-process virtual memory usage limit.
SIZE can be an integer or a string representing the memory limit."
  (let ((size-str (cond ((integerp size) (number-to-string size))
                        ((stringp size) size)
                        (t (error "Size must be an integer or a string")))))
    (syd--stat (syd--rule "mem/vm_max" size-str ?:))))

(defun syd-pid-max (size)
  "Set syd maximum process ID limit for PID sandboxing.
SIZE is a number representing the PID limit."
  (unless (numberp size)
    (error "Size must be a number"))
  (let ((path (format "/dev/syd/pid/max:%d" size)))
    (syd--stat path)))

(defun syd-filter-mem (state)
  "Toggles the reporting of access violations for memory sandboxing.
STATE is a boolean representing the state of the filter."
  (let ((path (if state "/dev/syd/filter/mem:1" "/dev/syd/filter/mem:0")))
    (syd--stat path)))

(defun syd-filter-pid (state)
  "Toggles the reporting of access violations for PID sandboxing.
STATE is a boolean representing the state of the filter."
  (let ((path (if state "/dev/syd/filter/pid:1" "/dev/syd/filter/pid:0")))
    (syd--stat path)))

(defun syd-kill-mem (state)
  "Toggle kill of the offending process for Memory sandboxing.
STATE is a boolean representing the state of the option."
  (let ((path (if state "/dev/syd/mem/kill:1" "/dev/syd/mem/kill:0")))
    (syd--stat path)))

(defun syd-kill-pid (state)
  "Toggle kill of the offending process for PID sandboxing.
STATE is a boolean representing the state of the option."
  (let ((path (if state "/dev/syd/pid/kill:1" "/dev/syd/pid/kill:0")))
    (syd--stat path)))

(defun syd-exec (file argv)
  "Execute a command outside the sandbox without sandboxing.
FILE is the file path of the command as a string.
ARGV is a list of strings representing the arguments to the command."
  (unless (stringp file)
    (error "File must be a string"))
  (let ((all-strings t))
    (dolist (arg argv)
      (unless (stringp arg)
        (setq all-strings nil)))
    (unless all-strings
      (error "All elements in ARGV must be strings")))

  (let ((cmd (mapconcat 'identity (cons file argv) "\x1F")))
    (syd--stat (concat "/dev/syd/cmd/exec!" cmd))))

(defun syd--rule (rule elem op)
  "Helper function to construct a path for syd operations.
RULE is a string representing the rule.
ELEM is a string representing the element.
OP is a character representing the operation."
  (unless (member op '(?+ ?- ?^ ?:))
    (error "Invalid operation"))
  (when (string-empty-p elem)
    (error "Element cannot be empty"))
  (concat "/dev/syd/" rule (char-to-string op) elem))

(defun syd--stat (path)
  "Check if the file at PATH exists using `file-modes'."
  (condition-case nil
      (not (null (file-modes path)))
    (error nil)))  ; On error, return nil

(provide 'syd)
;;; syd.el ends here
