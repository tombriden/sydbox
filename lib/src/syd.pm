#!/usr/bin/env perl
# coding: utf-8
#
# syd: seccomp and landlock based application sandbox with support for namespaces
# lib/src/syd.pl: Perl bindings of libsyd, the syd API C Library
# Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

package syd;

=head1 NAME

plsyd - Perl Bindings for the syd API Rust Library

=head1 SYNOPSIS

`plsyd` provides Perl bindings for `libsyd`, a C library written in Rust that
implements the syd stat API. This package allows interaction with the
`/dev/syd` interface of syd, enabling runtime configuration and interaction
within the syd sandboxing environment.

=head1 DESCRIPTION

The `plsyd` library is designed to interact with the syd sandboxing
environment through Perl. It offers functionalities to check and modify the
state of the sandbox lock, perform system calls to `/dev/syd`, and execute
commands within the sandbox. This makes it easier for Perl applications to
integrate with syd's features.

=head1 REQUIREMENTS

To use `plsyd`, the shared library `libsyd.so` must be available in the
system's library search path. Ensure this shared library is properly installed
and its location is included in the environment path where system libraries are
searched for.

=head1 ATTENTION

This library is currently a work in progress. The API is subject to change and
may not be stable. Users are advised to use it with caution and to stay updated
with the latest changes.

=head1 MORE INFORMATION

For more detailed information about `libsyd` and usage instructions, refer to
the syd manual: L<https://git.sr.ht/~alip/syd>. To read `libsyd` API
documentation, see L<https://libsyd.exherbolinux.org>.

=head1 AUTHOR

Ali Polatel (alip@chesswob.org)

=head1 LICENSE

This software is licensed under the LGPL-3.0-or-later license.

=cut

use strict;
use warnings;
use Exporter 'import';
use Carp;
use Errno qw(EINVAL ENOENT ENAMETOOLONG);
use JSON;

use FFI::Platypus 2.00;
use FFI::CheckLib qw( find_lib );

our $LIBSYD_PATH;
if ($ENV{'LD_LIBRARY_PATH'}) {
	my @paths = split(':', $ENV{'LD_LIBRARY_PATH'});
	($LIBSYD_PATH) = grep { -e "$_/libsyd.so" } @paths;
	$LIBSYD_PATH .= "/libsyd.so" if defined $LIBSYD_PATH;
}
if (defined $LIBSYD_PATH) {
	warn "Loading libsyd.so via LD_LIBRARY_PATH from $LIBSYD_PATH";
} else {
	$LIBSYD_PATH = find_lib(lib => 'syd') || croak "Failed to find libsyd: $!";
}
my $syd = FFI::Platypus->new(api => 2, lib => $LIBSYD_PATH);

# Define exportable and default functions
our @EXPORT_OK = qw(
  $LIBSYD_PATH
  info
  check api
  lock LOCK_OFF LOCK_EXEC LOCK_ON
  exec
  panic reset load
  enable_mem disable_mem enabled_mem
  enable_pid disable_pid enabled_pid
  enable_read disable_read enabled_read
  enable_stat disable_stat enabled_stat
  enable_write disable_write enabled_write
  enable_exec disable_exec enabled_exec
  enable_net disable_net enabled_net
  allow_read_add allow_read_del allow_read_rem
  deny_read_add deny_read_del deny_read_rem
  filter_read_add filter_read_del filter_read_rem
  allow_stat_add allow_stat_del allow_stat_rem
  deny_stat_add deny_stat_del deny_stat_rem
  filter_stat_add filter_stat_del filter_stat_rem
  allow_write_add allow_write_del allow_write_rem
  deny_write_add deny_write_del deny_write_rem
  filter_write_add filter_write_del filter_write_rem
  allow_exec_add allow_exec_del allow_exec_rem
  deny_exec_add deny_exec_del deny_exec_rem
  filter_exec_add filter_exec_del filter_exec_rem
  allow_net_bind_add allow_net_bind_del allow_net_bind_rem
  deny_net_bind_add deny_net_bind_del deny_net_bind_rem
  filter_net_bind_add filter_net_bind_del filter_net_bind_rem
  allow_net_connect_add allow_net_connect_del allow_net_connect_rem
  deny_net_connect_add deny_net_connect_del deny_net_connect_rem
  filter_net_connect_add filter_net_connect_del filter_net_connect_rem
  kill_add kill_del kill_rem
  mem_max mem_vm_max pid_max
  filter_mem filter_pid kill_mem kill_pid
);
our %EXPORT_TAGS = ('all' => [@EXPORT_OK],);

use constant {
	LOCK_OFF  => 0,
	LOCK_EXEC => 1,
	LOCK_ON   => 2,
};

sub info {
	open my $fh, '<', '/dev/syd' or croak "Cannot open /dev/syd: $!";
	my $text = do { local $/; <$fh> };
	close $fh;
	return decode_json($text);
}

=head1 FUNCTIONS

=head2 check

Performs an lstat system call on the file "/dev/syd".

=over 4

=item Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_check' => 'check'] => [] => 'int', \&check_return_void);

=head2 api

Performs a syd API check. The caller is advised to perform this check
before calling any other syd API calls.

=over 4

=item Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_api' => 'api'] => [] => 'int', \&check_return_void);

=head2 panic

Causes syd to exit immediately with code 127.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_panic' => 'panic'] => [] => 'int', \&check_return_void);

=head2 reset

Causes syd to reset sandboxing to the default state.
Allowlists, denylists and filters are going to be cleared.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_reset' => 'reset'] => [] => 'int', \&check_return_void);

=head2 load

Causes syd to read configuration from the given file descriptor.

=over 4

=item * Parameters

=over 4

=item - fd (integer)

The file descriptor to read the configuration from.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_load' => 'load'] => ['int'] => 'int', \&check_return_1);

=head2 lock

Sets the state of the sandbox lock.

=over 4

=item * Args

=over 4

=item - state

The desired state of the sandbox lock, possible values are:

=over 4

=item * LOCK_OFF

The sandbox lock is off, allowing all sandbox commands.

=item * LOCK_EXEC

The sandbox lock is set to on for all processes except the initial
process (syd exec child). This is the default state.

=item * LOCK_ON

The sandbox lock is on, disallowing all sandbox commands.

=back

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_lock' => 'lock'] => ['uint'] => 'int',
	sub {
		my ($func, $lock) = @_;
		my $r = $func->($lock);
		if ($r < 0) {
			$! = -$r;
			croak "libsyd error: $!";
		}
		return $r;
	}
);

=head2 exec

Execute a command outside the sandbox without sandboxing.

=over 4

=item * Parameters

=over 4

=item - file (string)

The file path of the command to be executed, as a string.

=item - argv (Array of strings)

The arguments to the command, as a list of strings.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_exec' => 'exec'] => ['string', 'opaque[]'] => 'int',
	sub {
		my ($func, $file, $argv) = @_;

		# Cast each argument in @argv to 'opaque'
		my @ptrs = map { $syd->cast('string' => 'opaque', $_) } @$argv;

		# Add a NULL pointer at the end of the argument list
		push @ptrs, undef;

		# Call the syd_exec function
		my $r = $func->($file, \@ptrs);

		# Check for errors
		if ($r == 0) {
			return 1;
		} elsif ($r < 0) {
			$! = -$r;
			croak "libsyd error: $!";
		}
		return $r;
	}
);

=head2 enable_mem

Enables memory sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_mem' => 'enable_mem'] => [] => 'int', \&check_return_void);

=head2 disable_mem

Disables memory sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_mem' => 'disable_mem'] => [] => 'int', \&check_return_void);

=head2 enabled_mem

Checks if memory sandboxing is enabled.

=over 4

=item * Returns

Non-zero if memory sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_mem' => 'enabled_mem'] => [] => 'bool', \&check_return_bool);

=head2 enable_pid

Enables PID sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_pid' => 'enable_pid'] => [] => 'int', \&check_return_void);

=head2 disable_pid

Disables PID sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_pid' => 'disable_pid'] => [] => 'int', \&check_return_void);

=head2 enabled_pid

Checks if PID sandboxing is enabled.

=over 4

=item * Returns

Non-zero if PID sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_pid' => 'enabled_pid'] => [] => 'bool', \&check_return_bool);

=head2 enable_read

Enables read sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_read' => 'enable_read'] => [] => 'int', \&check_return_void);

=head2 disable_read

Disables read sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_read' => 'disable_read'] => [] => 'int', \&check_return_void);

=head2 enabled_read

Checks if read sandboxing is enabled.

=over 4

=item * Returns

Non-zero if read sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_read' => 'enabled_read'] => [] => 'bool', \&check_return_bool);

=head2 enable_stat

Enables stat sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_stat' => 'enable_stat'] => [] => 'int', \&check_return_void);

=head2 disable_stat

Disables stat sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_stat' => 'disable_stat'] => [] => 'int', \&check_return_void);

=head2 enabled_stat

Checks if stat sandboxing is enabled.

=over 4

=item * Returns

Non-zero if stat sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_stat' => 'enabled_stat'] => [] => 'bool', \&check_return_bool);

=head2 enable_write

Enables write sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_write' => 'enable_write'] => [] => 'int', \&check_return_void);

=head2 disable_write

Disables write sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_write' => 'disable_write'] => [] => 'int', \&check_return_void);

=head2 enabled_write

Checks if write sandboxing is enabled.

=over 4

=item * Returns

Non-zero if write sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_write' => 'enabled_write'] => [] => 'bool', \&check_return_bool);

=head2 enable_exec

Enables exec sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_exec' => 'enable_exec'] => [] => 'int', \&check_return_void);

=head2 disable_exec

Disables exec sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_exec' => 'disable_exec'] => [] => 'int', \&check_return_void);

=head2 enabled_exec

Checks if exec sandboxing is enabled.

=over 4

=item * Returns

Non-zero if exec sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_exec' => 'enabled_exec'] => [] => 'bool', \&check_return_bool);

=head2 enable_net

Enables net sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_enable_net' => 'enable_net'] => [] => 'int', \&check_return_void);

=head2 disable_net

Disables net sandboxing.

=over 4

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_disable_net' => 'disable_net'] => [] => 'int', \&check_return_void);

=head2 enabled_net

Checks if net sandboxing is enabled.

=over 4

=item * Returns

Non-zero if net sandboxing is enabled, zero otherwise.

=back

=cut

$syd->attach(['syd_enabled_net' => 'enabled_net'] => [] => 'bool', \&check_return_bool);

=head2 allow_read_add

Adds to the allowlist of read sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_allow_read_add' => 'allow_read_add'] => ['string'] => 'int', \&check_return_1);

=head2 allow_read_del

Removes the first instance from the end of the allowlist of read sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_allow_read_del' => 'allow_read_del'] => ['string'] => 'int', \&check_return_1);

=head2 allow_read_rem

Removes all matching patterns from the allowlist of read sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_allow_read_rem' => 'allow_read_rem'] => ['string'] => 'int', \&check_return_1);

=head2 deny_read_add

Adds to the denylist of read sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_deny_read_add' => 'deny_read_add'] => ['string'] => 'int', \&check_return_1);

=head2 deny_read_del

Removes the first instance from the end of the denylist of read sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_deny_read_del' => 'deny_read_del'] => ['string'] => 'int', \&check_return_1);

=head2 deny_read_rem

Removes all matching patterns from the denylist of read sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_deny_read_rem' => 'deny_read_rem'] => ['string'] => 'int', \&check_return_1);

=head2 filter_read_add

Adds to the filter of read sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_filter_read_add' => 'filter_read_add'] => ['string'] => 'int', \&check_return_1);

=head2 filter_read_del

Removes the first instance from the end of the filter of read sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_filter_read_del' => 'filter_read_del'] => ['string'] => 'int', \&check_return_1);

=head2 filter_read_rem

Removes all matching patterns from the filter of read sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_filter_read_rem' => 'filter_read_rem'] => ['string'] => 'int', \&check_return_1);

=head2 allow_stat_add

Adds to the allowlist of stat sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_allow_stat_add' => 'allow_stat_add'] => ['string'] => 'int', \&check_return_1);

=head2 allow_stat_del

Removes the first instance from the end of the allowlist of stat sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_allow_stat_del' => 'allow_stat_del'] => ['string'] => 'int', \&check_return_1);

=head2 allow_stat_rem

Removes all matching patterns from the allowlist of stat sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_allow_stat_rem' => 'allow_stat_rem'] => ['string'] => 'int', \&check_return_1);

=head2 deny_stat_add

Adds to the denylist of stat sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_deny_stat_add' => 'deny_stat_add'] => ['string'] => 'int', \&check_return_1);

=head2 deny_stat_del

Removes the first instance from the end of the denylist of stat sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_deny_stat_del' => 'deny_stat_del'] => ['string'] => 'int', \&check_return_1);

=head2 deny_stat_rem

Removes all matching patterns from the denylist of stat sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_deny_stat_rem' => 'deny_stat_rem'] => ['string'] => 'int', \&check_return_1);

=head2 filter_stat_add

Adds to the filter of stat sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_filter_stat_add' => 'filter_stat_add'] => ['string'] => 'int', \&check_return_1);

=head2 filter_stat_del

Removes the first instance from the end of the filter of stat sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_filter_stat_del' => 'filter_stat_del'] => ['string'] => 'int', \&check_return_1);

=head2 filter_stat_rem

Removes all matching patterns from the filter of stat sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_filter_stat_rem' => 'filter_stat_rem'] => ['string'] => 'int', \&check_return_1);

=head2 allow_write_add

Adds to the allowlist of write sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_allow_write_add' => 'allow_write_add'] => ['string'] => 'int', \&check_return_1);

=head2 allow_write_del

Removes the first instance from the end of the allowlist of write sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_allow_write_del' => 'allow_write_del'] => ['string'] => 'int', \&check_return_1);

=head2 allow_write_rem

Removes all matching patterns from the allowlist of write sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_allow_write_rem' => 'allow_write_rem'] => ['string'] => 'int', \&check_return_1);

=head2 deny_write_add

Adds to the denylist of write sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_deny_write_add' => 'deny_write_add'] => ['string'] => 'int', \&check_return_1);

=head2 deny_write_del

Removes the first instance from the end of the denylist of write sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_deny_write_del' => 'deny_write_del'] => ['string'] => 'int', \&check_return_1);

=head2 deny_write_rem

Removes all matching patterns from the denylist of write sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_deny_write_rem' => 'deny_write_rem'] => ['string'] => 'int', \&check_return_1);

=head2 filter_write_add

Adds to the filter of write sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_filter_write_add' => 'filter_write_add'] => ['string'] => 'int',
	\&check_return_1
);

=head2 filter_write_del

Removes the first instance from the end of the filter of write sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_filter_write_del' => 'filter_write_del'] => ['string'] => 'int',
	\&check_return_1
);

=head2 filter_write_rem

Removes all matching patterns from the filter of write sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_filter_write_rem' => 'filter_write_rem'] => ['string'] => 'int',
	\&check_return_1
);

=head2 allow_exec_add

Adds to the allowlist of exec sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_allow_exec_add' => 'allow_exec_add'] => ['string'] => 'int', \&check_return_1);

=head2 allow_exec_del

Removes the first instance from the end of the allowlist of exec sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_allow_exec_del' => 'allow_exec_del'] => ['string'] => 'int', \&check_return_1);

=head2 allow_exec_rem

Removes all matching patterns from the allowlist of exec sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_allow_exec_rem' => 'allow_exec_rem'] => ['string'] => 'int', \&check_return_1);

=head2 deny_exec_add

Adds to the denylist of exec sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_deny_exec_add' => 'deny_exec_add'] => ['string'] => 'int', \&check_return_1);

=head2 deny_exec_del

Removes the first instance from the end of the denylist of exec sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_deny_exec_del' => 'deny_exec_del'] => ['string'] => 'int', \&check_return_1);

=head2 deny_exec_rem

Removes all matching patterns from the denylist of exec sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_deny_exec_rem' => 'deny_exec_rem'] => ['string'] => 'int', \&check_return_1);

=head2 filter_exec_add

Adds to the filter of exec sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_filter_exec_add' => 'filter_exec_add'] => ['string'] => 'int', \&check_return_1);

=head2 filter_exec_del

Removes the first instance from the end of the filter of exec sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_filter_exec_del' => 'filter_exec_del'] => ['string'] => 'int', \&check_return_1);

=head2 filter_exec_rem

Removes all matching patterns from the filter of exec sandboxing.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_filter_exec_rem' => 'filter_exec_rem'] => ['string'] => 'int', \&check_return_1);

=head2 allow_net_bind_add

Adds to the allowlist of net/bind sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_allow_net_bind_add' => 'allow_net_bind_add'] => ['string'] => 'int',
	\&check_return_1
);

=head2 allow_net_bind_del

Removes the first instance from the end of the allowlist of net/bind sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_allow_net_bind_del' => 'allow_net_bind_del'] => ['string'] => 'int',
	\&check_return_1
);

=head2 allow_net_bind_rem

Removes all matching patterns from the allowlist of net/bind sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_allow_net_bind_rem' => 'allow_net_bind_rem'] => ['string'] => 'int',
	\&check_return_1
);

=head2 deny_net_bind_add

Adds to the denylist of net/bind sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_deny_net_bind_add' => 'deny_net_bind_add'] => ['string'] => 'int',
	\&check_return_1
);

=head2 deny_net_bind_del

Removes the first instance from the end of the denylist of net/bind sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_deny_net_bind_del' => 'deny_net_bind_del'] => ['string'] => 'int',
	\&check_return_1
);

=head2 deny_net_bind_rem

Removes all matching patterns from the denylist of net/bind sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_deny_net_bind_rem' => 'deny_net_bind_rem'] => ['string'] => 'int',
	\&check_return_1
);

=head2 filter_net_bind_add

Adds to the filter of net/bind sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_filter_net_bind_add' => 'filter_net_bind_add'] => ['string'] => 'int',
	\&check_return_1
);

=head2 filter_net_bind_del

Removes the first instance from the end of the filter of net/bind sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_filter_net_bind_del' => 'filter_net_bind_del'] => ['string'] => 'int',
	\&check_return_1
);

=head2 filter_net_bind_rem

Removes all matching patterns from the filter of net/bind sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_filter_net_bind_rem' => 'filter_net_bind_rem'] => ['string'] => 'int',
	\&check_return_1
);

=head2 allow_net_connect_add

Adds to the allowlist of net/connect sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_allow_net_connect_add' => 'allow_net_connect_add'] => ['string'] => 'int',
	\&check_return_1
);

=head2 allow_net_connect_del

Removes the first instance from the end of the allowlist of net/connect sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_allow_net_connect_del' => 'allow_net_connect_del'] => ['string'] => 'int',
	\&check_return_1
);

=head2 allow_net_connect_rem

Removes all matching patterns from the allowlist of net/connect sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_allow_net_connect_rem' => 'allow_net_connect_rem'] => ['string'] => 'int',
	\&check_return_1
);

=head2 deny_net_connect_add

Adds to the denylist of net/connect sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_deny_net_connect_add' => 'deny_net_connect_add'] => ['string'] => 'int',
	\&check_return_1
);

=head2 deny_net_connect_del

Removes the first instance from the end of the denylist of net/connect sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_deny_net_connect_del' => 'deny_net_connect_del'] => ['string'] => 'int',
	\&check_return_1
);

=head2 deny_net_connect_rem

Removes all matching patterns from the denylist of net/connect sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_deny_net_connect_rem' => 'deny_net_connect_rem'] => ['string'] => 'int',
	\&check_return_1
);

=head2 filter_net_connect_add

Adds to the filter of net/connect sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_filter_net_connect_add' => 'filter_net_connect_add'] => ['string'] => 'int',
	\&check_return_1
);

=head2 filter_net_connect_del

Removes the first instance from the end of the filter of net/connect sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_filter_net_connect_del' => 'filter_net_connect_del'] => ['string'] => 'int',
	\&check_return_1
);

=head2 filter_net_connect_rem

Removes all matching patterns from the filter of net/connect sandboxing.

=over 4

=item * Parameters

=over 4

=item - addr (string)

Address pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(
	['syd_filter_net_connect_rem' => 'filter_net_connect_rem'] => ['string'] => 'int',
	\&check_return_1
);

=head2 kill_add

Adds to the list of glob patterns used to determine which paths should
be killed (prevented from executing) in the sandbox.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_kill_add' => 'kill_add'] => ['string'] => 'int', \&check_return_1);

=head2 kill_del

Deletes the first matching item from the end of the list of glob
patterns used to determine which paths should be killed (prevented from
executing) in the sandbox.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_kill_del' => 'kill_del'] => ['string'] => 'int', \&check_return_1);

=head2 kill_rem

Removes all matching items from the list of glob patterns used to
determine which paths should be killed (prevented from executing) in the
sandbox.

=over 4

=item * Parameters

=over 4

=item - glob (string)

Glob pattern as a string.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_kill_rem' => 'kill_rem'] => ['string'] => 'int', \&check_return_1);

=head2 mem_max

Set syd maximum per-process memory usage limit for memory sandboxing.
The parse-size crate is used to parse the value so formatted strings are OK.

=over 4

=item * Parameters

=over 4

=item - size (string)

Limit size as a string, which can be formatted (e.g., '10MB', '512KB').

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_mem_max' => 'mem_max'] => ['string'] => 'int', \&check_return_1);

=head2 mem_vm_max

Set syd maximum per-process virtual memory usage limit for memory sandboxing,
The parse-size crate is used to parse the value so formatted strings are OK.

=over 4

=item * Parameters

=over 4

=item - size (string)

Limit size as a string, which can be formatted (e.g., '10MB', '512KB').

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_mem_vm_max' => 'mem_vm_max'] => ['string'] => 'int', \&check_return_1);

=head2 pid_max

Set syd maximum process ID limit for PID sandboxing.

=over 4

=item * Parameters

=over 4

=item - size (unsigned int)

Limit size, must be greater than or equal to zero.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_pid_max' => 'pid_max'] => ['uint'] => 'int', \&check_return_1);

=head2 filter_mem

Toggle the reporting of access violations for memory sandboxing.

=over 4

=item * Parameters

=over 4

=item - state (bool)

True to report violations, false to keep silent.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_filter_mem' => 'filter_mem'] => ['bool'] => 'int', \&check_return_1);

=head2 filter_pid

Toggle the reporting of access violations for PID sandboxing.

=over 4

=item * Parameters

=over 4

=item - state (bool)

True to report violations, false to keep silent.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_filter_pid' => 'filter_pid'] => ['bool'] => 'int', \&check_return_1);

=head2 kill_mem

Toggle kill of the offending process for Memory sandboxing.

=over 4

=item * Parameters

=over 4

=item - state (bool)

True to kill offending process, false otherwise.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_kill_mem' => 'kill_mem'] => ['bool'] => 'int', \&check_return_1);

=head2 kill_pid

Toggle kill of the offending process for PID sandboxing.

=over 4

=item * Parameters

=over 4

=item - state (bool)

True to kill offending process, false otherwise.

=back

=item * Returns

Non-zero on successful operation, or croaks on failure.

=back

=cut

$syd->attach(['syd_kill_pid' => 'kill_pid'] => ['bool'] => 'int', \&check_return_1);

sub check_return_void {
	my ($func) = @_;
	my $r = $func->();
	if ($r == 0) {
		return 1;
	} elsif ($r < 0) {
		$! = -$r;
		croak "libsyd error: $!";
	}
	return $r;
}

sub check_return_bool {
	my ($func) = @_;
	return $func->() != 0;
}

sub check_return_1 {
	my ($func, $arg) = @_;
	my $r = $func->($arg);
	if ($r == 0) {
		return 1;
	} elsif ($r < 0) {
		$! = -$r;
		croak "libsyd error: $!";
	}
	return $r;
}

1;    # Return true to indicate successful module loading
