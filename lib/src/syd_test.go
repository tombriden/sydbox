// syd: seccomp and landlock based application sandbox with support for namespaces
// lib/src/syd_test.go: Tests for Go bindings of libsyd, the syd API C Library
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

// Package syd provides Go bindings for the libsyd C library.
package syd

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"syscall"
	"testing"
	"time"
)

// Function to check if a slice contains a specific string
func contains(slice []string, value string) bool {
	for _, item := range slice {
		if item == value {
			return true
		}
	}
	return false
}

func Test_01_Api(t *testing.T) {
	api, err := Api()
	if err != nil {
		t.Fatalf("Api failed: %v", err)
	}
	if api != 3 {
		t.Errorf("Api is not 3!")
	}
}

func Test_02_Stat(t *testing.T) {
	// Mem Test
	state := EnabledMem()
	if err := EnableMem(); err != nil {
		t.Fatalf("EnableMem failed: %v", err)
	}
	if enabled := EnabledMem(); !enabled {
		t.Error("Expected Mem to be enabled")
	}
	if err := DisableMem(); err != nil {
		t.Fatalf("DisableMem failed: %v", err)
	}
	if enabled := EnabledMem(); enabled {
		t.Error("Expected Mem to be disabled")
	}
	if state {
		EnableMem()
	} else {
		DisableMem()
	}

	// Pid Test
	state = EnabledPid()
	if err := EnablePid(); err != nil {
		t.Fatalf("EnablePid failed: %v", err)
	}
	if enabled := EnabledPid(); !enabled {
		t.Error("Expected Pid to be enabled")
	}
	if err := DisablePid(); err != nil {
		t.Fatalf("DisablePid failed: %v", err)
	}
	if enabled := EnabledPid(); enabled {
		t.Error("Expected Pid to be disabled")
	}
	if state {
		EnablePid()
	} else {
		DisablePid()
	}

	// Read Test
	state = EnabledRead()
	if err := EnableRead(); err != nil {
		t.Fatalf("EnableRead failed: %v", err)
	}
	if enabled := EnabledRead(); !enabled {
		t.Error("Expected Read to be enabled")
	}
	if err := DisableRead(); err != nil {
		t.Fatalf("DisableRead failed: %v", err)
	}
	if enabled := EnabledRead(); enabled {
		t.Error("Expected Read to be disabled")
	}
	if state {
		EnableRead()
	} else {
		DisableRead()
	}

	// Stat Test
	state = EnabledStat()
	if err := EnableStat(); err != nil {
		t.Fatalf("EnableStat failed: %v", err)
	}
	if enabled := EnabledStat(); !enabled {
		t.Error("Expected Stat to be enabled")
	}
	if err := DisableStat(); err != nil {
		t.Fatalf("DisableStat failed: %v", err)
	}
	if enabled := EnabledStat(); enabled {
		t.Error("Expected Stat to be disabled")
	}
	if state {
		EnableStat()
	} else {
		DisableStat()
	}

	// Exec Test
	state = EnabledExec()
	if err := EnableExec(); err != nil {
		t.Fatalf("EnableExec failed: %v", err)
	}
	if enabled := EnabledExec(); !enabled {
		t.Error("Expected Exec to be enabled")
	}
	if err := DisableExec(); err != nil {
		t.Fatalf("DisableExec failed: %v", err)
	}
	if enabled := EnabledExec(); enabled {
		t.Error("Expected Exec to be disabled")
	}
	if state {
		EnableExec()
	} else {
		DisableExec()
	}

	// Net Test
	state = EnabledNet()
	if err := EnableNet(); err != nil {
		t.Fatalf("EnableNet failed: %v", err)
	}
	if enabled := EnabledNet(); !enabled {
		t.Error("Expected Net to be enabled")
	}
	if err := DisableNet(); err != nil {
		t.Fatalf("DisableNet failed: %v", err)
	}
	if enabled := EnabledNet(); enabled {
		t.Error("Expected Net to be disabled")
	}
	if state {
		EnableNet()
	} else {
		DisableNet()
	}

	// Testing Filter{Mem,Pid}
	sandbox, err := Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.MemFilter {
		t.Errorf("Expected MemFilter to be false, got true")
	}
	if err := FilterMem(true); err != nil {
		t.Fatalf("FilterMem(true) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if !sandbox.MemFilter {
		t.Errorf("Expected MemFilter to be true, got false")
	}
	if err := FilterMem(false); err != nil {
		t.Fatalf("FilterMem(false) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.MemFilter {
		t.Errorf("Expected MemFilter to be false, got true")
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.PidFilter {
		t.Errorf("Expected PidFilter to be false, got true")
	}
	if err := FilterPid(true); err != nil {
		t.Fatalf("FilterPid(true) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if !sandbox.PidFilter {
		t.Errorf("Expected PidFilter to be true, got false")
	}
	if err := FilterPid(false); err != nil {
		t.Fatalf("FilterPid(false) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.PidFilter {
		t.Errorf("Expected PidFilter to be false, got true")
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if contains(sandbox.Flags, "kill-mem") {
		t.Errorf("Expected KillMem to be false, got true")
	}
	if err := KillMem(true); err != nil {
		t.Fatalf("KillMem(true) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if !contains(sandbox.Flags, "kill-mem") {
		t.Errorf("Expected KillMem to be true, got false")
	}
	if err := KillMem(false); err != nil {
		t.Fatalf("KillMem(false) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if contains(sandbox.Flags, "kill-mem") {
		t.Errorf("Expected KillMem to be false, got true")
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if contains(sandbox.Flags, "kill-pid") {
		t.Errorf("Expected KillPid to be false, got true")
	}
	if err := KillPid(true); err != nil {
		t.Fatalf("KillPid(true) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if !contains(sandbox.Flags, "kill-pid") {
		t.Errorf("Expected KillPid to be true, got false")
	}
	if err := KillPid(false); err != nil {
		t.Fatalf("KillPid(false) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if contains(sandbox.Flags, "kill-pid") {
		t.Errorf("Expected KillPid to be false, got true")
	}

	// Testing memory limits
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	memMaxOrig := sandbox.MemMax
	memVmMaxOrig := sandbox.MemVmMax
	pidMaxOrig := sandbox.PidMax

	// Test setting MemMax
	if err := MemMax("1G"); err != nil {
		t.Fatalf("MemMax(1G) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.MemMax != 1024*1024*1024 {
		t.Errorf("Expected MemMax to be %d, got %d", 1024*1024*1024, sandbox.MemMax)
	}
	MemMax(strconv.FormatInt(memMaxOrig, 10)) // Resetting to original

	// Similar tests for MemVmMax...
	if err := MemVmMax("1G"); err != nil {
		t.Fatalf("MemVmMax(1G) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.MemVmMax != 1024*1024*1024 {
		t.Errorf("Expected MemVmMax to be %d, got %d", 1024*1024*1024, sandbox.MemVmMax)
	}
	MemVmMax(strconv.FormatInt(memVmMaxOrig, 10)) // Resetting to original

	// Test setting PidMax
	if err := PidMax(-1); err == nil {
		t.Error("Expected PidMax(-1) to fail")
	}
	if err := PidMax(4096); err != nil {
		t.Fatalf("PidMax(4096) failed: %v", err)
	}
	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}
	if sandbox.PidMax != 4096 {
		t.Errorf("Expected PidMax to be 4096, got %d", sandbox.PidMax)
	}
	PidMax(pidMaxOrig) // Resetting to original
}

func Test_03_Glob(t *testing.T) {
	path := "/tmp/gosyd"

	testCases := []struct {
		AddFunc  func(string) error
		DelFunc  func(string) error
		RemFunc  func(string) error
		Act, Cap string
	}{
		{AllowReadAdd, AllowReadDel, AllowReadRem, "Allow", "r"},
		{DenyReadAdd, DenyReadDel, DenyReadRem, "Deny", "r"},
		{FilterReadAdd, FilterReadDel, FilterReadRem, "Filter", "r"},
		{AllowStatAdd, AllowStatDel, AllowStatRem, "Allow", "s"},
		{DenyStatAdd, DenyStatDel, DenyStatRem, "Deny", "s"},
		{FilterStatAdd, FilterStatDel, FilterStatRem, "Filter", "s"},
		{AllowWriteAdd, AllowWriteDel, AllowWriteRem, "Allow", "w"},
		{DenyWriteAdd, DenyWriteDel, DenyWriteRem, "Deny", "w"},
		{FilterWriteAdd, FilterWriteDel, FilterWriteRem, "Filter", "w"},
		{AllowExecAdd, AllowExecDel, AllowExecRem, "Allow", "x"},
		{DenyExecAdd, DenyExecDel, DenyExecRem, "Deny", "x"},
		{FilterExecAdd, FilterExecDel, FilterExecRem, "Filter", "x"},
	}

	for _, tc := range testCases {
		testName := tc.Act + tc.Cap
		rule := GlobRule{Act: tc.Act, Cap: tc.Cap, Pat: path}

		// Test Add
		err := tc.AddFunc(path)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		sandbox, err := Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx := findGlob(sandbox.GlobRules, rule)
		if idx != len(sandbox.GlobRules)-1 {
			t.Errorf("Expected %s rule to be last, got index %d", testName, idx)
		}

		// Test Del
		err = tc.DelFunc(path)
		if err != nil {
			t.Fatalf("%sDel failed: %v", testName, err)
		}
		sandbox, err = Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx = findGlob(sandbox.GlobRules, rule)
		if idx != -1 {
			t.Errorf("Expected %s rule to be absent, got index %d", testName, idx)
		}

		// Test Add, Add, Add, Rem
		err = tc.AddFunc(path)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.AddFunc(path)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.AddFunc(path)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.RemFunc(path)
		if err != nil {
			t.Fatalf("%sRem failed: %v", testName, err)
		}
		sandbox, err = Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx = findGlob(sandbox.GlobRules, rule)
		if idx != -1 {
			t.Errorf("Expected %s rule to be absent after %sRem, got index %d", testName, testName, idx)
		}
	}
}

func Test_04_Cidr_Port_Single(t *testing.T) {
	host := "127.3.1.4/8"
	port := 16
	addr := host + "!" + fmt.Sprint(port)
	aarg := string(addr)

	testCases := []struct {
		AddFunc  func(string) error
		DelFunc  func(string) error
		RemFunc  func(string) error
		Act, Cap string
	}{
		{AllowNetBindAdd, AllowNetBindDel, AllowNetBindRem, "Allow", "b"},
		{DenyNetBindAdd, DenyNetBindDel, DenyNetBindRem, "Deny", "b"},
		{FilterNetBindAdd, FilterNetBindDel, FilterNetBindRem, "Filter", "b"},
		{AllowNetConnectAdd, AllowNetConnectDel, AllowNetConnectRem, "Allow", "c"},
		{DenyNetConnectAdd, DenyNetConnectDel, DenyNetConnectRem, "Deny", "c"},
		{FilterNetConnectAdd, FilterNetConnectDel, FilterNetConnectRem, "Filter", "c"},
	}

	for _, tc := range testCases {
		testName := tc.Act + tc.Cap
		rule := CidrRule{
			Act: tc.Act,
			Cap: tc.Cap,
			Pat: Pattern{Addr: host, Port: port},
		}

		// Test Add
		err := tc.AddFunc(aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		sandbox, err := Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx := findCidr(sandbox.CidrRules, rule.Pat)
		if idx != len(sandbox.CidrRules)-1 {
			t.Errorf("Expected %s rule to be last, got index %d. CIDR Rules: %+v", testName, idx, sandbox.CidrRules)
		}

		// Test Del
		err = tc.DelFunc(aarg)
		if err != nil {
			t.Fatalf("%sDel failed: %v", testName, err)
		}
		sandbox, err = Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx = findCidr(sandbox.CidrRules, rule.Pat)
		if idx != -1 {
			t.Errorf("Expected %s rule to be absent, got index %d", testName, idx)
		}

		// Test Add, Add, Add, Rem
		err = tc.AddFunc(aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.AddFunc(aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.AddFunc(aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.RemFunc(aarg)
		if err != nil {
			t.Fatalf("%sRem failed: %v", testName, err)
		}
		sandbox, err = Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx = findCidr(sandbox.CidrRules, rule.Pat)
		if idx != -1 {
			t.Errorf("Expected %s rule to be absent, got index %d", testName, idx)
		}
	}
}

func Test_05_Cidr_Port_Double(t *testing.T) {
	host := "127.3.1.4/8"
	port := [2]int{1024, 65535}
	addr := host + "!" + fmt.Sprint(port[0]) + "-" + fmt.Sprint(port[1])
	aarg := string(addr)

	testCases := []struct {
		AddFunc  func(string) error
		DelFunc  func(string) error
		RemFunc  func(string) error
		Act, Cap string
	}{
		{AllowNetBindAdd, AllowNetBindDel, AllowNetBindRem, "Allow", "b"},
		{DenyNetBindAdd, DenyNetBindDel, DenyNetBindRem, "Deny", "b"},
		{FilterNetBindAdd, FilterNetBindDel, FilterNetBindRem, "Filter", "b"},
		{AllowNetConnectAdd, AllowNetConnectDel, AllowNetConnectRem, "Allow", "c"},
		{DenyNetConnectAdd, DenyNetConnectDel, DenyNetConnectRem, "Deny", "c"},
		{FilterNetConnectAdd, FilterNetConnectDel, FilterNetConnectRem, "Filter", "c"},
	}

	for _, tc := range testCases {
		testName := tc.Act + tc.Cap
		rule := CidrRule{
			Act: tc.Act,
			Cap: tc.Cap,
			Pat: Pattern{Addr: host, Port: port},
		}

		// Test Add
		err := tc.AddFunc(aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		sandbox, err := Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx := findCidr(sandbox.CidrRules, rule.Pat)
		if idx != len(sandbox.CidrRules)-1 {
			t.Errorf("Expected %s rule to be last, got index %d. CIDR Rules: %+v", testName, idx, sandbox.CidrRules)
		}

		// Test Del
		err = tc.DelFunc(aarg)
		if err != nil {
			t.Fatalf("%sDel failed: %v", testName, err)
		}
		sandbox, err = Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx = findCidr(sandbox.CidrRules, rule.Pat)
		if idx != -1 {
			t.Errorf("Expected %s rule to be absent, got index %d", testName, idx)
		}

		// Test Add, Add, Add, Rem
		err = tc.AddFunc(aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.AddFunc(aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.AddFunc(aarg)
		if err != nil {
			t.Fatalf("%sAdd failed: %v", testName, err)
		}
		err = tc.RemFunc(aarg)
		if err != nil {
			t.Fatalf("%sRem failed: %v", testName, err)
		}
		sandbox, err = Info()
		if err != nil {
			t.Fatalf("Info failed: %v", err)
		}
		idx = findCidr(sandbox.CidrRules, rule.Pat)
		if idx != -1 {
			t.Errorf("Expected %s rule to be absent, got index %d", testName, idx)
		}
	}
}

func Test_06_Kill(t *testing.T) {
	path := "/tmp/gosyd"
	rule := GlobRule{Act: "Kill", Cap: "x", Pat: path}

	// Assert KillAdd
	err := KillAdd(path)
	if err != nil {
		t.Fatalf("KillAdd failed: %v", err)
	}

	sandbox, err := Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}

	idx := findGlob(sandbox.GlobRules, rule)
	if idx != len(sandbox.GlobRules)-1 {
		t.Errorf("Expected rule to be last, got index %d", idx)
	}

	// Assert KillDel
	err = KillDel(path)
	if err != nil {
		t.Fatalf("KillDel failed: %v", err)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}

	idx = findGlob(sandbox.GlobRules, rule)
	if idx != -1 {
		t.Errorf("Expected rule to be absent, got index %d", idx)
	}

	// Additional checks for KillAdd and KillRem
	err = KillAdd(path)
	if err != nil {
		t.Fatalf("KillAdd failed: %v", err)
	}
	err = KillAdd(path)
	if err != nil {
		t.Fatalf("KillAdd failed: %v", err)
	}
	err = KillAdd(path)
	if err != nil {
		t.Fatalf("KillAdd failed: %v", err)
	}
	err = KillRem(path)
	if err != nil {
		t.Fatalf("KillRem failed: %v", err)
	}

	sandbox, err = Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}

	idx = findGlob(sandbox.GlobRules, rule)
	if idx != -1 {
		t.Errorf("Expected rule to be absent after KillRem, got index %d", idx)
	}
}

func Test_05_Exec(t *testing.T) {
	// Create a temporary directory
	tempDir, err := ioutil.TempDir("", "syd_test")
	if err != nil {
		t.Fatalf("Failed to create temporary directory: %v", err)
	}
	defer os.RemoveAll(tempDir) // Clean up

	// Path to the temporary file
	tempFile := filepath.Join(tempDir, "file")

	// Prepare command and arguments
	file := "/bin/sh"
	argv := []string{"-c", "echo 42 > " + tempFile}

	// Call Exec
	err = Exec(file, argv)
	if err != nil {
		t.Fatalf("Exec failed: %v", err)
	}

	// Wait for the command to execute
	time.Sleep(3 * time.Second)

	// Assert the contents of the file
	contents, err := ioutil.ReadFile(tempFile)
	if err != nil {
		t.Fatalf("Failed to read from temporary file: %v", err)
	}

	if strings.TrimSpace(string(contents)) != "42" {
		t.Errorf("Expected file contents to be '42', got '%s'", contents)
	}
}

func Test_07_Load(t *testing.T) {
	// Create a temporary file
	tempFile, err := ioutil.TempFile("", "syd_test")
	if err != nil {
		t.Fatalf("Failed to create temporary file: %v", err)
	}
	defer os.Remove(tempFile.Name()) // Clean up

	// Write test data to the temporary file
	_, err = tempFile.WriteString("pid/max:77\n")
	if err != nil {
		t.Fatalf("Failed to write to temporary file: %v", err)
	}

	// Seek back to the beginning of the file
	if _, err := tempFile.Seek(0, 0); err != nil {
		t.Fatalf("Failed to seek to beginning of the file: %v", err)
	}

	// Load the configuration from the temporary file
	if err := Load(int(tempFile.Fd())); err != nil {
		t.Fatalf("Load failed: %v", err)
	}

	// Retrieve the information using Info
	sandbox, err := Info()
	if err != nil {
		t.Fatalf("Info failed: %v", err)
	}

	// Assert the pid_max value
	expectedPidMax := 77
	if sandbox.PidMax != expectedPidMax {
		t.Errorf("Expected pid_max to be %d, got %d", expectedPidMax, sandbox.PidMax)
	}
}

func Test_08_Lock(t *testing.T) {
	if err := Lock(LockOff); err != nil {
		errno := err.(syscall.Errno)
		t.Errorf("Lock(LockOff): %v", errno)
	}
	if err := Lock(LockExec); err != nil {
		errno := err.(syscall.Errno)
		t.Errorf("Lock(LockExec): %v", errno)
	}

	if err := Lock(LockOff); err != nil {
		errno := err.(syscall.Errno)
		if errno != syscall.ENOENT {
			t.Errorf("Lock(LockOff): %v", errno)
		}
	} else {
		t.Errorf("Lock(LockOff): 0")
	}

	if err := Lock(LockExec); err != nil {
		errno := err.(syscall.Errno)
		if errno != syscall.ENOENT {
			t.Errorf("Lock(LockExec): %v", errno)
		}
	} else {
		t.Errorf("Lock(LockExec): 0")
	}

	if err := Lock(LockOn); err != nil {
		errno := err.(syscall.Errno)
		if errno != syscall.ENOENT {
			t.Errorf("Lock(LockOn): %v", errno)
		}
	} else {
		t.Errorf("Lock(LockOn): 0")
	}
}

// findGlob searches for a rule in the reversed list of GlobRules and returns its index.
func findGlob(rules []GlobRule, rule GlobRule) int {
	for idx, r := range rules {
		if reflect.DeepEqual(r, rule) {
			return idx
		}
	}
	return -1
}

func findCidr(rules []CidrRule, pattern Pattern) int {
	for idx, rule := range rules {
		if rule.Pat.Addr == pattern.Addr {
			fmt.Printf("rule: '%+v' == pat: '%+v'\n", rule.Pat.Addr, pattern.Addr)
		}
		if rule.Pat.Addr == pattern.Addr && comparePorts(rule.Pat.Port, pattern.Port) {
			return idx
		} else {
			fmt.Printf("port: '%+v' != pat: '%+v'\n", rule.Pat.Port, pattern.Port)
		}
	}
	return -1
}

func comparePorts(port1, port2 interface{}) bool {
	convertFloatSliceToIntSlice := func(floatSlice []interface{}) []int {
		intSlice := make([]int, len(floatSlice))
		for i, v := range floatSlice {
			if fv, ok := v.(float64); ok {
				intSlice[i] = int(fv)
			} else {
				fmt.Printf("Element in slice is not a float64: %v\n", v)
				return nil
			}
		}
		return intSlice
	}

	switch p1 := port1.(type) {
	case []interface{}:
		convertedP1 := convertFloatSliceToIntSlice(p1)
		if convertedP1 == nil {
			fmt.Printf("Failed to convert []interface{} to []int for Port1\n")
			return false
		}
		return comparePorts(convertedP1, port2)
	case float64:
		return comparePorts(int(p1), port2)
	case int:
		switch p2 := port2.(type) {
		case float64:
			return p1 == int(p2)
		case int:
			return p1 == p2
		case []int:
			return len(p2) == 2 && p1 >= p2[0] && p1 <= p2[1]
		case [2]int:
			return p1 >= p2[0] && p1 <= p2[1]
		case []interface{}:
			convertedP2 := convertFloatSliceToIntSlice(p2)
			if convertedP2 == nil {
				fmt.Printf("Failed to convert []interface{} to []int for Port2\n")
				return false
			}
			return comparePorts(p1, convertedP2)
		default:
			fmt.Printf("Pattern Port2 is of unexpected type %T\n", port2)
		}
	case []int:
		switch p2 := port2.(type) {
		case float64:
			return len(p1) == 1 && p1[0] == int(p2)
		case int:
			return len(p1) == 2 && p2 >= p1[0] && p2 <= p1[1]
		case []int:
			return reflect.DeepEqual(p1, p2)
		case [2]int:
			return reflect.DeepEqual(p1, p2[:])
		case []interface{}:
			convertedP2 := convertFloatSliceToIntSlice(p2)
			if convertedP2 == nil {
				fmt.Printf("Failed to convert []interface{} to []int for Port2\n")
				return false
			}
			return reflect.DeepEqual(p1, convertedP2)
		default:
			fmt.Printf("Pattern Port2 is of unexpected type %T\n", port2)
		}
	case [2]int:
		switch p2 := port2.(type) {
		case float64:
			return len(p1) == 1 && p1[0] == int(p2)
		case int:
			return p2 >= p1[0] && p2 <= p1[1]
		case []int:
			return reflect.DeepEqual(p1[:], p2)
		case [2]int:
			return reflect.DeepEqual(p1, p2)
		case []interface{}:
			convertedP2 := convertFloatSliceToIntSlice(p2)
			if convertedP2 == nil {
				fmt.Printf("Failed to convert []interface{} to []int for Port2\n")
				return false
			}
			return reflect.DeepEqual(p1[:], convertedP2)
		default:
			fmt.Printf("Pattern Port2 is of unexpected type %T\n", port2)
		}
	default:
		fmt.Printf("Rule Port1 is of unexpected type %T\n", port1)
	}
	return false
}
