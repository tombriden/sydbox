//
// libsyd: Rust-based C library for syd interaction via /dev/syd
// lib/src/lib.rs: syd API C Library
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: LGPL-3.0-or-later

//! # libsyd - syd API Rust Library
//!
//! `libsyd` is a C library written in Rust that implements the syd
//! stat API, providing an interface to the `/dev/syd` of syd. It
//! allows for runtime configuration and interaction with the syd
//! sandboxing environment.
//!
//! ## Overview
//! The library is designed to interact with the syd sandboxing
//! environment, offering functionalities to check and modify the state
//! of the sandbox lock, and perform system calls to `/dev/syd`.
//!
//! For more detailed information and usage instructions, refer to the syd
//! manual, available at [syd Manual](http://man.exherbolinux.org/syd.2.html).
//!
//! ## Author
//! Ali Polatel <alip@chesswob.org>

// We like safe, clean and simple code with documentation.
#![deny(missing_docs)]
#![deny(clippy::allow_attributes_without_reason)]
#![deny(clippy::arithmetic_side_effects)]
#![deny(clippy::as_ptr_cast_mut)]
#![deny(clippy::as_underscore)]
#![deny(clippy::assertions_on_result_states)]
#![deny(clippy::borrow_as_ptr)]
#![deny(clippy::branches_sharing_code)]
#![deny(clippy::case_sensitive_file_extension_comparisons)]
#![deny(clippy::cast_lossless)]
#![deny(clippy::cast_possible_truncation)]
#![deny(clippy::cast_possible_wrap)]
#![deny(clippy::cast_precision_loss)]
#![deny(clippy::cast_ptr_alignment)]
#![deny(clippy::cast_sign_loss)]
#![deny(clippy::checked_conversions)]
#![deny(clippy::clear_with_drain)]
#![deny(clippy::clone_on_ref_ptr)]
#![deny(clippy::cloned_instead_of_copied)]
#![deny(clippy::cognitive_complexity)]
#![deny(clippy::collection_is_never_read)]
#![deny(clippy::copy_iterator)]
#![deny(clippy::create_dir)]
#![deny(clippy::dbg_macro)]
#![deny(clippy::debug_assert_with_mut_call)]
#![deny(clippy::decimal_literal_representation)]
#![deny(clippy::default_trait_access)]
#![deny(clippy::default_union_representation)]
#![deny(clippy::derive_partial_eq_without_eq)]
#![deny(clippy::doc_link_with_quotes)]
#![deny(clippy::doc_markdown)]
#![deny(clippy::explicit_into_iter_loop)]
#![deny(clippy::explicit_iter_loop)]
#![deny(clippy::fallible_impl_from)]
#![deny(clippy::missing_safety_doc)]
#![deny(clippy::undocumented_unsafe_blocks)]

use std::{
    ffi::{CStr, OsStr, OsString},
    fs::{symlink_metadata, Metadata},
    os::{
        raw::{c_char, c_int},
        unix::{
            ffi::OsStrExt,
            fs::{FileTypeExt, MetadataExt},
        },
    },
    path::{Path, PathBuf},
};

/// An enumeration of the possible states for the sandbox lock.
#[repr(u8)]
#[allow(non_camel_case_types)]
pub enum lock_state_t {
    /// The sandbox lock is off, allowing all sandbox commands.
    LOCK_OFF,
    /// The sandbox lock is set to on for all processes except the initial
    /// process (syd exec child). This is the default state.
    LOCK_EXEC,
    /// The sandbox lock is on, disallowing all sandbox commands.
    LOCK_ON,
}

const EFAULT: i32 = 14;
const EINVAL: i32 = 22;

#[inline(always)]
fn check_stat(stat: &Metadata) -> bool {
    if !stat.file_type().is_char_device() {
        return false;
    }

    let rdev = stat.rdev();

    let major = (rdev >> 8) & 0xff;
    let minor = rdev & 0xff;

    // dev/null
    major == 1 && minor == 3
}

fn stat<P: AsRef<Path>>(path: P) -> c_int {
    match symlink_metadata(path) {
        Ok(stat) if check_stat(&stat) => 0,
        Ok(_) => -EINVAL,
        Err(error) => match error.raw_os_error() {
            Some(e) => e.checked_neg().unwrap_or(-EINVAL),
            None => -EINVAL,
        },
    }
}

fn esyd<P: AsRef<Path>>(rule: P, elem: *const c_char, op: u8) -> c_int {
    if !matches!(op, b'+' | b'-' | b'^' | b':') {
        return -EINVAL;
    }

    if elem.is_null() {
        return -EFAULT;
    }

    // SAFETY: Trust that `elem` is a null-terminated string.
    let elem = unsafe { CStr::from_ptr(elem) };
    let elem = OsStr::from_bytes(elem.to_bytes());

    // Manually concatenate the path segments
    let mut path = OsString::from("/dev/syd/");
    path.push(rule.as_ref());
    path.push(OsStr::from_bytes(&[op]));
    path.push(elem);

    // Convert the OsString to PathBuf
    let path = PathBuf::from(path);

    stat(path)
}

/// Performs a syd API check
///
/// The caller is advised to perform this check before
/// calling any other syd API calls.
///
/// Returns API number on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_api() -> c_int {
    match stat("/dev/syd/3") {
        0 => 3,
        n => n,
    }
}

/// Performs an lstat system call on the file "/dev/syd".
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_check() -> c_int {
    stat("/dev/syd")
}

/// Causes syd to exit immediately with code 127
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_panic() -> c_int {
    stat("/dev/syd/panic")
}

/// Causes syd to reset sandboxing to the default state.
/// Allowlists, denylists and filters are going to be cleared.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_reset() -> c_int {
    stat("/dev/syd/reset")
}

/// Causes syd to read configuration from the given file descriptor.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_load(fd: c_int) -> c_int {
    let mut path = PathBuf::from("/dev/syd/load");

    let mut buf = itoa::Buffer::new();
    path.push(buf.format(fd));

    stat(path)
}

/// Sets the state of the sandbox lock.
///
/// state: The desired state of the sandbox lock.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_lock(state: lock_state_t) -> c_int {
    match state as u8 {
        0 => stat("/dev/syd/lock:off"),
        1 => stat("/dev/syd/lock:exec"),
        2 => stat("/dev/syd/lock:on"),
        _ => -EINVAL,
    }
}

/// Checks if memory sandboxing is enabled.
///
/// Returns true if memory sandboxing is enabled, false otherwise.
#[no_mangle]
pub extern "C" fn syd_enabled_mem() -> bool {
    stat("/dev/syd/sandbox/mem?") == 0
}

/// Enable memory sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_enable_mem() -> c_int {
    stat("/dev/syd/sandbox/mem:on")
}

/// Disable memory sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_disable_mem() -> c_int {
    stat("/dev/syd/sandbox/mem:off")
}

/// Checks if PID sandboxing is enabled.
///
/// Returns true if PID sandboxing is enabled, false otherwise.
#[no_mangle]
pub extern "C" fn syd_enabled_pid() -> bool {
    stat("/dev/syd/sandbox/pid?") == 0
}

/// Enable PID sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_enable_pid() -> c_int {
    stat("/dev/syd/sandbox/pid:on")
}

/// Disable PID sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_disable_pid() -> c_int {
    stat("/dev/syd/sandbox/pid:off")
}

/// Checks if read sandboxing is enabled.
///
/// Returns true if read sandboxing is enabled, false otherwise.
#[no_mangle]
pub extern "C" fn syd_enabled_read() -> bool {
    stat("/dev/syd/sandbox/read?") == 0
}

/// Enable read sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_enable_read() -> c_int {
    stat("/dev/syd/sandbox/read:on")
}

/// Disable read sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_disable_read() -> c_int {
    stat("/dev/syd/sandbox/read:off")
}

/// Checks if stat sandboxing is enabled.
///
/// Returns true if stat sandboxing is enabled, false otherwise.
#[no_mangle]
pub extern "C" fn syd_enabled_stat() -> bool {
    stat("/dev/syd/sandbox/stat?") == 0
}

/// Enable stat sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_enable_stat() -> c_int {
    stat("/dev/syd/sandbox/stat:on")
}

/// Disable stat sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_disable_stat() -> c_int {
    stat("/dev/syd/sandbox/stat:off")
}

/// Checks if write sandboxing is enabled.
///
/// Returns true if write sandboxing is enabled, false otherwise.
#[no_mangle]
pub extern "C" fn syd_enabled_write() -> bool {
    stat("/dev/syd/sandbox/write?") == 0
}

/// Enable write sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_enable_write() -> c_int {
    stat("/dev/syd/sandbox/write:on")
}

/// Disable write sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_disable_write() -> c_int {
    stat("/dev/syd/sandbox/write:off")
}

/// Checks if exec sandboxing is enabled.
///
/// Returns true if exec sandboxing is enabled, false otherwise.
#[no_mangle]
pub extern "C" fn syd_enabled_exec() -> bool {
    stat("/dev/syd/sandbox/exec?") == 0
}

/// Enable exec sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_enable_exec() -> c_int {
    stat("/dev/syd/sandbox/exec:on")
}

/// Disable exec sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_disable_exec() -> c_int {
    stat("/dev/syd/sandbox/exec:off")
}

/// Checks if net sandboxing is enabled.
///
/// Returns true if net sandboxing is enabled, false otherwise.
#[no_mangle]
pub extern "C" fn syd_enabled_net() -> bool {
    stat("/dev/syd/sandbox/net?") == 0
}

/// Enable net sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_enable_net() -> c_int {
    stat("/dev/syd/sandbox/net:on")
}

/// Disable net sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_disable_net() -> c_int {
    stat("/dev/syd/sandbox/net:off")
}

/// Adds to the list of glob patterns used to determine which paths
/// should be killed (prevented from executing) in the sandbox.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_kill_add(glob: *const c_char) -> c_int {
    esyd("exec/kill", glob, b'+')
}

/// Deletes the first matching item from the end of the list of glob
/// patterns used to determine which paths should be killed (prevented
/// from executing) in the sandbox.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_kill_del(glob: *const c_char) -> c_int {
    esyd("exec/kill", glob, b'-')
}

/// Removes all matching items from the list of glob patterns used to
/// determine which paths should be killed (prevented from executing) in
/// the sandbox.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_kill_rem(glob: *const c_char) -> c_int {
    esyd("exec/kill", glob, b'^')
}

/// Adds to the allowlist of read sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_read_add(glob: *const c_char) -> c_int {
    esyd("allow/read", glob, b'+')
}

/// Removes the first instance from the end of the allowlist of read
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_read_del(glob: *const c_char) -> c_int {
    esyd("allow/read", glob, b'-')
}

/// Removes all matching patterns from the allowlist of read sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_read_rem(glob: *const c_char) -> c_int {
    esyd("allow/read", glob, b'^')
}

/// Adds to the denylist of read sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_read_add(glob: *const c_char) -> c_int {
    esyd("deny/read", glob, b'+')
}

/// Removes the first instance from the end of the denylist of read
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_read_del(glob: *const c_char) -> c_int {
    esyd("deny/read", glob, b'-')
}

/// Removes all matching patterns from the denylist of read sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_read_rem(glob: *const c_char) -> c_int {
    esyd("deny/read", glob, b'^')
}

/// Adds to the filter of read sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_read_add(glob: *const c_char) -> c_int {
    esyd("filter/read", glob, b'+')
}

/// Removes the first instance from the end of the filter of read
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_read_del(glob: *const c_char) -> c_int {
    esyd("filter/read", glob, b'-')
}

/// Removes all matching patterns from the filter of read sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_read_rem(glob: *const c_char) -> c_int {
    esyd("filter/read", glob, b'^')
}

/// Adds to the allowlist of stat sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_stat_add(glob: *const c_char) -> c_int {
    esyd("allow/stat", glob, b'+')
}

/// Removes the first instance from the end of the allowlist of stat
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_stat_del(glob: *const c_char) -> c_int {
    esyd("allow/stat", glob, b'-')
}

/// Removes all matching patterns from the allowlist of stat sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_stat_rem(glob: *const c_char) -> c_int {
    esyd("allow/stat", glob, b'^')
}

/// Adds to the denylist of stat sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_stat_add(glob: *const c_char) -> c_int {
    esyd("deny/stat", glob, b'+')
}

/// Removes the first instance from the end of the denylist of stat
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_stat_del(glob: *const c_char) -> c_int {
    esyd("deny/stat", glob, b'-')
}

/// Removes all matching patterns from the denylist of stat sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_stat_rem(glob: *const c_char) -> c_int {
    esyd("deny/stat", glob, b'^')
}

/// Adds to the filter of stat sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_stat_add(glob: *const c_char) -> c_int {
    esyd("filter/stat", glob, b'+')
}

/// Removes the first instance from the end of the filter of stat
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_stat_del(glob: *const c_char) -> c_int {
    esyd("filter/stat", glob, b'-')
}

/// Removes all matching patterns from the filter of stat sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_stat_rem(glob: *const c_char) -> c_int {
    esyd("filter/stat", glob, b'^')
}

/// Adds to the allowlist of write sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_write_add(glob: *const c_char) -> c_int {
    esyd("allow/write", glob, b'+')
}

/// Removes the first instance from the end of the allowlist of write
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_write_del(glob: *const c_char) -> c_int {
    esyd("allow/write", glob, b'-')
}

/// Removes all matching patterns from the allowlist of write sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_write_rem(glob: *const c_char) -> c_int {
    esyd("allow/write", glob, b'^')
}

/// Adds to the denylist of write sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_write_add(glob: *const c_char) -> c_int {
    esyd("deny/write", glob, b'+')
}

/// Removes the first instance from the end of the denylist of write
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_write_del(glob: *const c_char) -> c_int {
    esyd("deny/write", glob, b'-')
}

/// Removes all matching patterns from the denylist of write sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_write_rem(glob: *const c_char) -> c_int {
    esyd("deny/write", glob, b'^')
}

/// Adds to the filter of write sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_write_add(glob: *const c_char) -> c_int {
    esyd("filter/write", glob, b'+')
}

/// Removes the first instance from the end of the filter of write
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_write_del(glob: *const c_char) -> c_int {
    esyd("filter/write", glob, b'-')
}

/// Removes all matching patterns from the filter of write sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_write_rem(glob: *const c_char) -> c_int {
    esyd("filter/write", glob, b'^')
}

/// Adds to the allowlist of exec sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_exec_add(glob: *const c_char) -> c_int {
    esyd("allow/exec", glob, b'+')
}

/// Removes the first instance from the end of the allowlist of exec
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_exec_del(glob: *const c_char) -> c_int {
    esyd("allow/exec", glob, b'-')
}

/// Removes all matching patterns from the allowlist of exec sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_exec_rem(glob: *const c_char) -> c_int {
    esyd("allow/exec", glob, b'^')
}

/// Adds to the denylist of exec sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_exec_add(glob: *const c_char) -> c_int {
    esyd("deny/exec", glob, b'+')
}

/// Removes the first instance from the end of the denylist of exec
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_exec_del(glob: *const c_char) -> c_int {
    esyd("deny/exec", glob, b'-')
}

/// Removes all matching patterns from the denylist of exec sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_exec_rem(glob: *const c_char) -> c_int {
    esyd("deny/exec", glob, b'^')
}

/// Adds to the filter of exec sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_exec_add(glob: *const c_char) -> c_int {
    esyd("filter/exec", glob, b'+')
}

/// Removes the first instance from the end of the filter of exec
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_exec_del(glob: *const c_char) -> c_int {
    esyd("filter/exec", glob, b'-')
}

/// Removes all matching patterns from the filter of exec sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_exec_rem(glob: *const c_char) -> c_int {
    esyd("filter/exec", glob, b'^')
}

/// Adds to the allowlist of net/bind sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_net_bind_add(addr: *const c_char) -> c_int {
    esyd("allow/net/bind", addr, b'+')
}

/// Removes the first instance from the end of the allowlist of net/bind
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_net_bind_del(addr: *const c_char) -> c_int {
    esyd("allow/net/bind", addr, b'-')
}

/// Removes all matching patterns from the allowlist of net/bind sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_net_bind_rem(addr: *const c_char) -> c_int {
    esyd("allow/net/bind", addr, b'^')
}

/// Adds to the denylist of net/bind sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_net_bind_add(addr: *const c_char) -> c_int {
    esyd("deny/net/bind", addr, b'+')
}

/// Removes the first instance from the end of the denylist of net/bind
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_net_bind_del(addr: *const c_char) -> c_int {
    esyd("deny/net/bind", addr, b'-')
}

/// Removes all matching patterns from the denylist of net/bind sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_net_bind_rem(addr: *const c_char) -> c_int {
    esyd("deny/net/bind", addr, b'^')
}

/// Adds to the filter of net/bind sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_net_bind_add(addr: *const c_char) -> c_int {
    esyd("filter/net/bind", addr, b'+')
}

/// Removes the first instance from the end of the filter of net/bind
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_net_bind_del(addr: *const c_char) -> c_int {
    esyd("filter/net/bind", addr, b'-')
}

/// Removes all matching patterns from the filter of net/bind sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_net_bind_rem(addr: *const c_char) -> c_int {
    esyd("filter/net/bind", addr, b'^')
}

/// Adds to the allowlist of net/connect sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_net_connect_add(addr: *const c_char) -> c_int {
    esyd("allow/net/connect", addr, b'+')
}

/// Removes the first instance from the end of the allowlist of net/connect
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_net_connect_del(addr: *const c_char) -> c_int {
    esyd("allow/net/connect", addr, b'-')
}

/// Removes all matching patterns from the allowlist of net/connect sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_allow_net_connect_rem(addr: *const c_char) -> c_int {
    esyd("allow/net/connect", addr, b'^')
}

/// Adds to the denylist of net/connect sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_net_connect_add(addr: *const c_char) -> c_int {
    esyd("deny/net/connect", addr, b'+')
}

/// Removes the first instance from the end of the denylist of net/connect
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_net_connect_del(addr: *const c_char) -> c_int {
    esyd("deny/net/connect", addr, b'-')
}

/// Removes all matching patterns from the denylist of net/connect sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_deny_net_connect_rem(addr: *const c_char) -> c_int {
    esyd("deny/net/connect", addr, b'^')
}

/// Adds to the filter of net/connect sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_net_connect_add(addr: *const c_char) -> c_int {
    esyd("filter/net/connect", addr, b'+')
}

/// Removes the first instance from the end of the filter of net/connect
/// sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_net_connect_del(addr: *const c_char) -> c_int {
    esyd("filter/net/connect", addr, b'-')
}

/// Removes all matching patterns from the filter of net/connect sandboxing.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_net_connect_rem(addr: *const c_char) -> c_int {
    esyd("filter/net/connect", addr, b'^')
}

/// Toggle kill of the offending process for Memory sandboxing
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_kill_mem(state: bool) -> c_int {
    if state {
        stat("/dev/syd/mem/kill:1")
    } else {
        stat("/dev/syd/mem/kill:0")
    }
}

/// Set syd maximum per-process memory usage limit for memory sandboxing.
///
/// parse-size crate is used to parse the value so formatted strings are OK.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_mem_max(size: *const c_char) -> c_int {
    esyd("mem/max", size, b':')
}

/// Set syd maximum per-process virtual memory usage limit for memory sandboxing.
///
/// parse-size crate is used to parse the value so formatted strings are OK.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_mem_vm_max(size: *const c_char) -> c_int {
    esyd("mem/vm_max", size, b':')
}

/// Set syd maximum process id limit for PID sandboxing
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_pid_max(size: usize) -> c_int {
    let mut path = OsString::from("/dev/syd/pid/max:");

    let mut buf = itoa::Buffer::new();
    let max_str = OsStr::from_bytes(buf.format(size).as_bytes());
    path.push(max_str);

    stat(path)
}

/// Toggle the reporting of access violations for memory sandboxing
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_mem(state: bool) -> c_int {
    if state {
        stat("/dev/syd/filter/mem:1")
    } else {
        stat("/dev/syd/filter/mem:0")
    }
}

/// Toggle the reporting of access violations for PID sandboxing
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_filter_pid(state: bool) -> c_int {
    if state {
        stat("/dev/syd/filter/pid:1")
    } else {
        stat("/dev/syd/filter/pid:0")
    }
}

/// Toggle kill of the offending process for PID sandboxing
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub extern "C" fn syd_kill_pid(state: bool) -> c_int {
    if state {
        stat("/dev/syd/pid/kill:1")
    } else {
        stat("/dev/syd/pid/kill:0")
    }
}

/// Execute a command outside the sandbox without sandboxing
///
/// # Safety
///
/// This function is marked `unsafe` because it dereferences raw
/// pointers, which is inherently unsafe in Rust.
///
/// The caller must ensure the following conditions are met to safely
/// use this function:
///
/// 1. The `file` pointer must point to a valid, null-terminated C-style
///    string.
///
/// 2. The `argv` pointer must point to an array of pointers, where each
///    pointer refers to a valid, null-terminated C-style string. The
///    last pointer in the array must be null, indicating the end of the
///    array.
///
/// 3. The memory pointed to by `file` and `argv` must remain valid for
///    the duration of the call.
///
/// Failing to uphold these guarantees can lead to undefined behavior,
/// including memory corruption and data races.
///
/// Returns 0 on success, negated errno on failure.
#[no_mangle]
pub unsafe extern "C" fn syd_exec(file: *const c_char, argv: *const *const c_char) -> c_int {
    if file.is_null() || argv.is_null() {
        return -EFAULT;
    }

    // SAFETY: Trust that `file` is a null-terminated string.
    let file = CStr::from_ptr(file);
    let file = OsStr::from_bytes(file.to_bytes());

    let mut path = OsString::from("/dev/syd/cmd/exec!");
    path.push(file);

    let mut idx: isize = 0;
    while !(*argv.offset(idx)).is_null() {
        // SAFETY: Trust that each `argv` element is a null-terminated string.
        let arg = CStr::from_ptr(*argv.offset(idx));
        let arg = OsStr::from_bytes(arg.to_bytes());

        path.push(OsStr::from_bytes(&[b'\x1F'])); // ASCII Unit Separator
        path.push(arg);

        idx = idx.saturating_add(1);
    }

    let path = PathBuf::from(path);
    stat(path)
}
