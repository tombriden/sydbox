// syd: seccomp and landlock based application sandbox with support for namespaces
//
// lib/src/syd.go: Go bindings of libsyd, the syd API C Library
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: LGPL-3.0-or-later
//
// Package syd provides Go bindings for the libsyd C library.
//
// Note: Build with CGO_LDFLAGS=-static to link libsyd statically.
package syd

/*
#cgo LDFLAGS: -lsyd
#include <stdint.h>
#include <syd.h>
*/
import "C"
import (
	"encoding/json"
	"os"
	"syscall"
	"unsafe"
)

// LockState represents the state of the sandbox lock in Go.
type LockState uint8

// An enumeration of the possible states for the sandbox lock.
const (
	// LockOff indicates that the sandbox lock is off, allowing all sandbox commands.
	// This state means there are no restrictions imposed by the sandbox.
	LockOff LockState = iota

	// LockExec indicates that the sandbox lock is on for all processes except the
	// initial process (syd exec child). This is the default state, where the
	// sandbox imposes restrictions, but allows certain operations for the initial
	// process.
	LockExec

	// LockOn indicates that the sandbox lock is on, disallowing all sandbox commands.
	// In this state, the sandbox is in its most restrictive mode, not permitting
	// any operations that could modify its state or configuration.
	LockOn
)

type Sandbox struct {
	Flags     []string   `json:"flags"`
	State     string     `json:"state"`
	Lock      string     `json:"lock"`
	Cpid      int        `json:"cpid"`
	Root      bool       `json:"root"`
	MemMax    int64      `json:"mem_max"`
	MemVmMax  int64      `json:"mem_vm_max"`
	PidMax    int        `json:"pid_max"`
	MemFilter bool       `json:"mem_filter"`
	PidFilter bool       `json:"pid_filter"`
	CidrRules []CidrRule `json:"cidr_rules"`
	GlobRules []GlobRule `json:"glob_rules"`
}

type CidrRule struct {
	Act string  `json:"act"`
	Cap string  `json:"cap"`
	Pat Pattern `json:"pat"`
}

type GlobRule struct {
	Act string `json:"act"`
	Cap string `json:"cap"`
	Pat string `json:"pat"`
}

type Pattern struct {
	Addr string      `json:"addr"`
	Port interface{} `json:"port"` // Port could be an int or a slice of ints
}

// Info reads the state of the syd sandbox from /dev/syd and returns it as a Sandbox struct.
//
// If there is a failure in reading the file, the error returned is the corresponding syscall.Errno.
//
// If there is a JSON decoding error, syscall.EINVAL is returned.
func Info() (*Sandbox, error) {
	data, err := os.ReadFile("/dev/syd")
	if err != nil {
		return nil, err // os.ReadFile already returns an error of type syscall.Errno
	}

	var state Sandbox
	if err = json.Unmarshal(data, &state); err != nil {
		return nil, syscall.EINVAL // Return EINVAL for JSON decoding errors
	}

	return &state, nil
}

// Check performs an lstat system call on the file "/dev/syd".
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func Check() error {
	result := C.syd_check()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Api performs a syd API check. This function should be called before
// making any other syd API calls. It's used to ensure that the syd
// environment is correctly set up and ready to handle further API requests.
//
// Returns the API number on success. If the call fails, it returns an error
// corresponding to the negated errno. The successful return value is an integer
// representing the API number, and the error, if any, is of type syscall.Errno.
func Api() (int, error) {
	result := C.syd_api()
	if result < 0 {
		return -1, syscall.Errno(-result)
	}
	// On success, return the API number.
	return int(result), nil
}

// Panic causes syd to exit immediately with code 127.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func Panic() error {
	result := C.syd_panic()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Reset causes syd to reset sandboxing to the default state.
// Allowlists, denylists and filters are going to be cleared.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func Reset() error {
	result := C.syd_reset()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Load instructs syd to read its configuration from the specified file
// descriptor. This function is used to load syd configurations dynamically
// at runtime from a file represented by the given file descriptor.
//
// The function accepts a file descriptor (fd) as an argument. This file descriptor
// should be valid and point to a file containing the desired configuration.
//
// Returns nil on success. If the call fails, it returns an error corresponding
// to the negated errno. The error is of type syscall.Errno.
func Load(fd int) error {
	result := C.syd_load(C.int(fd))
	if result != 0 {
		// Convert the negated errno to a positive value and return it as an error.
		return syscall.Errno(-result)
	}
	return nil
}

// Lock sets the state of the sandbox lock.
// Returns nil on success and a syscall.Errno on failure.
func Lock(state LockState) error {
	result := C.syd_lock(C.lock_state_t(state))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// Exec executes a command outside the sandbox without applying sandboxing.
// This function is used to run a command in a non-sandboxed environment.
//
// The function accepts a string for the file to execute and a slice of strings
// representing the arguments to the command.
//
// Returns nil on success. If the call fails, it returns an error corresponding
// to the negated errno. The error is of type syscall.Errno.
func Exec(file string, argv []string) error {
	cFile := C.CString(file)
	defer C.free(unsafe.Pointer(cFile))

	cArgv := make([]*C.char, len(argv)+1)
	for i, arg := range argv {
		cArgv[i] = C.CString(arg)
		defer C.free(unsafe.Pointer(cArgv[i]))
	}
	cArgv[len(argv)] = nil // Null-terminate the argument list

	result := C.syd_exec(cFile, &cArgv[0])
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnableMem enables memory sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableMem() error {
	result := C.syd_enable_mem()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableMem disables memory sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableMem() error {
	result := C.syd_disable_mem()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledMem checks if memory sandboxing is enabled in the syd environment.
//
// It returns true if memory sandboxing is enabled, and false otherwise.
func EnabledMem() bool {
	result := C.syd_enabled_mem()
	return bool(result)
}

// EnablePid enables PID sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnablePid() error {
	result := C.syd_enable_pid()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisablePid disables PID sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisablePid() error {
	result := C.syd_disable_pid()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledPid checks if PID sandboxing is enabled in the syd environment.
//
// It returns true if PID sandboxing is enabled, and false otherwise.
func EnabledPid() bool {
	result := C.syd_enabled_pid()
	return bool(result)
}

// EnableRead enables read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableRead() error {
	result := C.syd_enable_read()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableRead disables read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableRead() error {
	result := C.syd_disable_read()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledRead checks if read sandboxing is enabled in the syd environment.
//
// It returns true if read sandboxing is enabled, and false otherwise.
func EnabledRead() bool {
	result := C.syd_enabled_read()
	return bool(result)
}

// EnableStat enables stat sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableStat() error {
	result := C.syd_enable_stat()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableStat disables stat sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableStat() error {
	result := C.syd_disable_stat()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledStat checks if stat sandboxing is enabled in the syd environment.
//
// It returns true if stat sandboxing is enabled, and false otherwise.
func EnabledStat() bool {
	result := C.syd_enabled_stat()
	return bool(result)
}

// EnableWrite enables write sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableWrite() error {
	result := C.syd_enable_write()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableWrite disables write sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableWrite() error {
	result := C.syd_disable_write()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledWrite checks if write sandboxing is enabled in the syd
// environment.
//
// It returns true if write sandboxing is enabled, and false otherwise.
func EnabledWrite() bool {
	result := C.syd_enabled_write()
	return bool(result)
}

// EnableExec enables exec sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableExec() error {
	result := C.syd_enable_exec()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableExec disables exec sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableExec() error {
	result := C.syd_disable_exec()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledExec checks if exec sandboxing is enabled in the syd environment.
//
// It returns true if exec sandboxing is enabled, and false otherwise.
func EnabledExec() bool {
	result := C.syd_enabled_exec()
	return bool(result)
}

// EnableNet enables network sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func EnableNet() error {
	result := C.syd_enable_net()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DisableNet disables network sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DisableNet() error {
	result := C.syd_disable_net()
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// EnabledNet checks if network sandboxing is enabled in the syd
// environment.
//
// It returns true if network sandboxing is enabled, and false otherwise.
func EnabledNet() bool {
	result := C.syd_enabled_net()
	return bool(result)
}

// AllowReadAdd adds the specified glob pattern to the allowlist of read
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowReadAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_allow_read_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowReadDel removes the first instance from the end of the allowlist of
// read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowReadDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_allow_read_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowReadRem removes all matching patterns from the allowlist of read
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowReadRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_allow_read_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyReadAdd adds the specified glob pattern to the denylist of read
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyReadAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_deny_read_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyReadDel removes the first instance from the end of the denylist of
// read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyReadDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_deny_read_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyReadRem removes all matching patterns from the denylist of read
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyReadRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_deny_read_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterReadAdd adds the specified glob pattern to the filter of read
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterReadAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_filter_read_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterReadDel removes the first instance from the end of the filter of
// read sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterReadDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_filter_read_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterReadRem removes all matching patterns from the filter of read
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterReadRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_filter_read_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowStatAdd adds the specified glob pattern to the allowlist of stat
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowStatAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_allow_stat_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowStatDel removes the first instance from the end of the allowlist of
// stat sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowStatDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_allow_stat_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowStatRem removes all matching patterns from the allowlist of stat
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowStatRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_allow_stat_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyStatAdd adds the specified glob pattern to the denylist of stat
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyStatAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_deny_stat_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyStatDel removes the first instance from the end of the denylist of
// stat sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyStatDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_deny_stat_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyStatRem removes all matching patterns from the denylist of stat
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyStatRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_deny_stat_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterStatAdd adds the specified glob pattern to the filter of stat
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterStatAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_filter_stat_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterStatDel removes the first instance from the end of the filter of
// stat sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterStatDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_filter_stat_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterStatRem removes all matching patterns from the filter of stat
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterStatRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_filter_stat_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowWriteAdd adds the specified glob pattern to the allowlist of write
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowWriteAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_allow_write_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowWriteDel removes the first instance from the end of the allowlist of
// write sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowWriteDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_allow_write_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowWriteRem removes all matching patterns from the allowlist of write
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowWriteRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_allow_write_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyWriteAdd adds the specified glob pattern to the denylist of write
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyWriteAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_deny_write_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyWriteDel removes the first instance from the end of the denylist of
// write sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyWriteDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_deny_write_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyWriteRem removes all matching patterns from the denylist of write
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyWriteRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_deny_write_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterWriteAdd adds the specified glob pattern to the filter of write
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterWriteAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_filter_write_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterWriteDel removes the first instance from the end of the filter of
// write sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterWriteDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_filter_write_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterWriteRem removes all matching patterns from the filter of write
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterWriteRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_filter_write_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowExecAdd adds the specified glob pattern to the allowlist of exec
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowExecAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_allow_exec_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowExecDel removes the first instance from the end of the allowlist of
// exec sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowExecDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_allow_exec_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowExecRem removes all matching patterns from the allowlist of exec
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowExecRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_allow_exec_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyExecAdd adds the specified glob pattern to the denylist of exec
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyExecAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_deny_exec_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyExecDel removes the first instance from the end of the denylist of
// exec sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyExecDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_deny_exec_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyExecRem removes all matching patterns from the denylist of exec
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyExecRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_deny_exec_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterExecAdd adds the specified glob pattern to the filter of exec
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterExecAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_filter_exec_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterExecDel removes the first instance from the end of the filter of
// exec sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterExecDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_filter_exec_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterExecRem removes all matching patterns from the filter of exec
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterExecRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_filter_exec_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowNetBindAdd adds the specified address pattern to the allowlist of net/bind
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowNetBindAdd(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_allow_net_bind_add(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowNetBindDel removes the first instance from the end of the allowlist of
// net/bind sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowNetBindDel(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_allow_net_bind_del(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowNetBindRem removes all matching patterns from the allowlist of net/bind
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowNetBindRem(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_allow_net_bind_rem(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyNetBindAdd adds the specified address pattern to the denylist of net/bind
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyNetBindAdd(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_deny_net_bind_add(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyNetBindDel removes the first instance from the end of the denylist of
// net/bind sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyNetBindDel(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_deny_net_bind_del(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyNetBindRem removes all matching patterns from the denylist of net/bind
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyNetBindRem(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_deny_net_bind_rem(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterNetBindAdd adds the specified address pattern to the filter of net/bind
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterNetBindAdd(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_filter_net_bind_add(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterNetBindDel removes the first instance from the end of the filter of
// net/bind sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterNetBindDel(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_filter_net_bind_del(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterNetBindRem removes all matching patterns from the filter of net/bind
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterNetBindRem(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_filter_net_bind_rem(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowNetConnectAdd adds the specified address pattern to the allowlist of net/connect
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowNetConnectAdd(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_allow_net_connect_add(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowNetConnectDel removes the first instance from the end of the allowlist of
// net/connect sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowNetConnectDel(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_allow_net_connect_del(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// AllowNetConnectRem removes all matching patterns from the allowlist of net/connect
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func AllowNetConnectRem(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_allow_net_connect_rem(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyNetConnectAdd adds the specified address pattern to the denylist of net/connect
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyNetConnectAdd(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_deny_net_connect_add(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyNetConnectDel removes the first instance from the end of the denylist of
// net/connect sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyNetConnectDel(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_deny_net_connect_del(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// DenyNetConnectRem removes all matching patterns from the denylist of net/connect
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func DenyNetConnectRem(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_deny_net_connect_rem(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterNetConnectAdd adds the specified address pattern to the filter of net/connect
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterNetConnectAdd(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_filter_net_connect_add(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterNetConnectDel removes the first instance from the end of the filter of
// net/connect sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterNetConnectDel(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_filter_net_connect_del(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterNetConnectRem removes all matching patterns from the filter of net/connect
// sandboxing.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterNetConnectRem(addr string) error {
	cAddr := C.CString(addr)
	defer C.free(unsafe.Pointer(cAddr))

	result := C.syd_filter_net_connect_rem(cAddr)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// KillAdd adds to the list of glob patterns used to determine which paths
// should be killed (prevented from executing) in the sandbox.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func KillAdd(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_kill_add(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// KillDel deletes the first matching item from the end of the list of glob
// patterns used to determine which paths should be killed (prevented from
// executing) in the sandbox.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func KillDel(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_kill_del(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// KillRem removes all matching items from the list of glob patterns used to
// determine which paths should be killed (prevented from executing) in the
// sandbox.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func KillRem(glob string) error {
	cGlob := C.CString(glob)
	defer C.free(unsafe.Pointer(cGlob))

	result := C.syd_kill_rem(cGlob)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MemMax sets the syd maximum per-process memory usage limit for memory
// sandboxing.
//
// The size parameter is a string that can represent the size in
// different formats, as the parse-size crate is used to parse the
// value.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MemMax(size string) error {
	cSize := C.CString(size)
	defer C.free(unsafe.Pointer(cSize))

	result := C.syd_mem_max(cSize)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// MemVmMax sets the syd maximum per-process virtual memory usage limit for
// memory sandboxing.
//
// The size parameter is a string that can represent the size in
// different formats, as the parse-size crate is used to parse the
// value.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func MemVmMax(size string) error {
	cSize := C.CString(size)
	defer C.free(unsafe.Pointer(cSize))

	result := C.syd_mem_vm_max(cSize)
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// PidMax sets the syd maximum process ID limit for PID sandboxing.
//
// The function takes an integer representing the maximum number of PIDs.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func PidMax(size int) error {
	if size < 0 {
		return syscall.EINVAL
	}
	result := C.syd_pid_max(C.size_t(size))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterMem toggles the reporting of access violations for memory sandboxing.
//
// The function takes a boolean as argument representing the state of the filter.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterMem(state bool) error {
	result := C.syd_filter_mem(C.bool(state))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// FilterPid toggles the reporting of access violations for PID sandboxing.
//
// The function takes a boolean as argument representing the state of the filter.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func FilterPid(state bool) error {
	result := C.syd_filter_pid(C.bool(state))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// KillMem toggles kill of the offending process for Memory sandboxing.
//
// The function takes a boolean as argument representing the state of the option.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func KillMem(state bool) error {
	result := C.syd_kill_mem(C.bool(state))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}

// KillPid toggles kill of the offending process for PID sandboxing.
//
// The function takes a boolean as argument representing the state of the option.
//
// Returns nil on success, and an error corresponding to the negated errno
// on failure. The error is of type syscall.Errno.
func KillPid(state bool) error {
	result := C.syd_kill_pid(C.bool(state))
	if result != 0 {
		return syscall.Errno(-result)
	}
	return nil
}
