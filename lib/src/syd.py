#!/usr/bin/env python3
# coding: utf-8
#
# syd: seccomp and landlock based application sandbox with support for namespaces
# lib/src/syd.py: Python ctypes bindings of libsyd, the syd API C Library
# Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

import os, sys, time
import enum, errno, ctypes
import json, tempfile, unittest

from typing import List, Union

"""
pysyd - Python Bindings for the syd API Rust Library

`pysyd` provides Python bindings for `libsyd`, a C library written in
Rust that implements the syd stat API. This package facilitates
interaction with the `/dev/syd` interface of syd, allowing for
runtime configuration and interaction within the syd sandboxing
environment.

Overview
--------
The `pysyd` library is designed to interact with the syd sandboxing
environment through Python. It offers functionalities to check and
modify the state of the sandbox lock, perform system calls to
`/dev/syd`, and execute commands within the sandbox. This makes it
easier for Python applications to integrate with syd's features.

Requirement
-----------
To use `pysyd`, the shared library `libsyd.so` must be available in the
system's library search path. Ensure that this shared library is
properly installed and its location is included in the environment path
where system libraries are searched for.

Attention
---------
This library is currently a work in progress. The API is subject to
change and may not be stable. Users are advised to use it with caution
and to stay updated with the latest changes.

Further Information
--------------------
For more detailed information about `libsyd` and usage instructions,
refer to the syd manual: https://git.sr.ht/~alip/syd

Author
------
Ali Polatel (alip@chesswob.org)

This Python wrapper is designed to provide a seamless and idiomatic
Python interface for interacting with the functionalities offered by
`libsyd`.
"""

__all__ = (
    "info",
    "check",
    "api",
    "panic",
    "reset",
    "load",
    "lock",
    "LockState",
    "exec",
    "enable_mem",
    "disable_mem",
    "enabled_mem",
    "enable_pid",
    "disable_pid",
    "enabled_pid",
    "enable_read",
    "disable_read",
    "enabled_read",
    "enable_stat",
    "disable_stat",
    "enabled_stat",
    "enable_write",
    "disable_write",
    "enabled_write",
    "enable_exec",
    "disable_exec",
    "enabled_exec",
    "enable_net",
    "disable_net",
    "enabled_net",
    "allow_read_add",
    "allow_read_del",
    "allow_read_rem",
    "deny_read_add",
    "deny_read_del",
    "deny_read_rem",
    "filter_read_add",
    "filter_read_del",
    "filter_read_rem",
    "allow_stat_add",
    "allow_stat_del",
    "allow_stat_rem",
    "deny_stat_add",
    "deny_stat_del",
    "deny_stat_rem",
    "filter_stat_add",
    "filter_stat_del",
    "filter_stat_rem",
    "allow_write_add",
    "allow_write_del",
    "allow_write_rem",
    "deny_write_add",
    "deny_write_del",
    "deny_write_rem",
    "filter_write_add",
    "filter_write_del",
    "filter_write_rem",
    "allow_exec_add",
    "allow_exec_del",
    "allow_exec_rem",
    "deny_exec_add",
    "deny_exec_del",
    "deny_exec_rem",
    "filter_exec_add",
    "filter_exec_del",
    "filter_exec_rem",
    "allow_net_bind_add",
    "allow_net_bind_del",
    "allow_net_bind_rem",
    "deny_net_bind_add",
    "deny_net_bind_del",
    "deny_net_bind_rem",
    "filter_net_bind_add",
    "filter_net_bind_del",
    "filter_net_bind_rem",
    "allow_net_connect_add",
    "allow_net_connect_del",
    "allow_net_connect_rem",
    "deny_net_connect_add",
    "deny_net_connect_del",
    "deny_net_connect_rem",
    "filter_net_connect_add",
    "filter_net_connect_del",
    "filter_net_connect_rem",
    "kill_add",
    "kill_del",
    "kill_rem",
    "mem_max",
    "mem_vm_max",
    "pid_max",
    "filter_mem",
    "filter_pid",
    "kill_mem",
    "kill_pid",
)

try:
    libsyd = ctypes.CDLL("libsyd.so")
except OSError as error:
    if error.errno == errno.ENOENT or "such file" in str(error):
        raise ImportError(f"install libsyd.so: {error}")
    raise ImportError(f"fix libsyd.so: {error}")
except error:
    raise ImportError(f"fix libsyd.so: {error}")


def check_return(negated_errno: int) -> bool:
    """
    Checks the returned negated errno from syd_kill and raises an OSError if it's an error code.

    Parameters:
        - negated_errno (int): The negated errno returned by the syd_kill function.

    Raises:
        OSError: If the negated_errno is a non-zero error code.
    """
    if negated_errno != 0:
        # Convert the negated errno back to the original errno
        errno = -negated_errno
        raise OSError(errno, os.strerror(errno))
    return True


def info() -> dict:
    """
    Reads the state of the syd sandbox from /dev/syd and returns it
    as a JSON object.

    This function opens the special file /dev/syd, which contains the
    current state of the syd sandbox in JSON format, and then parses
    and returns this state as a Python dictionary.

    Returns:
        dict: The current state of the syd sandbox.

    Raises:
        OSError: If the file /dev/syd cannot be opened.
        JSONDecodeError: If the content of /dev/syd is not valid JSON.
    """
    with open("/dev/syd") as f:
        return json.load(f)


def api() -> int:
    """
    Performs a syd API check
    The caller is advised to perform this check before
    calling any other syd API calls.

    Returns:
        int: API number on successful operation, or raises an OSError on failure.
    """
    api = libsyd.syd_api()
    if api < 0:
        raise OSError(-api, os.strerror(-api))
    return api


def check() -> bool:
    """
    Performs an lstat system call on the file "/dev/syd".

    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_check())


def panic() -> bool:
    """
    Causes syd to exit immediately with code 127.

    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_panic())


def reset() -> bool:
    """
    Causes syd to reset sandboxing to the default state.
    Allowlists, denylists and filters are going to be cleared.

    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_reset())


def load(fd: int) -> bool:
    """
    Causes syd to read configuration from the given file descriptor.

    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_load(fd))


@enum.unique
class LockState(enum.Enum):
    """
    Enum for representing the sandbox lock states:

    - LOCK_OFF: The sandbox lock is off, allowing all sandbox commands.
    - LOCK_EXEC: The sandbox lock is set to on for all processes except
      the initial process (syd exec child). This is the default state.
    - LOCK_ON: The sandbox lock is on, disallowing all sandbox commands.
    """

    LOCK_OFF = 0
    LOCK_EXEC = 1
    LOCK_ON = 2


def lock(state: LockState) -> bool:
    """
    Sets the state of the sandbox lock.

    Args:
        - state (LockState): The desired state of the sandbox lock, as
          defined by the `LockState` enum.

    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_lock(state.value))


def exec(file: bytes, argv: List[bytes]) -> bool:
    """
    Execute a command outside the sandbox without sandboxing

    Parameters:
        - file (bytes): The file path of the command to be executed, as bytes.
        - argv (List[bytes]): The arguments to the command, as a list of bytes.

    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    argv_array = (
        ctypes.c_char_p * (len(argv) + 1)
    )()  # Array of strings, null-terminated
    argv_array[:-1] = [arg for arg in argv]
    argv_array[-1] = None  # Null-terminate the array

    return check_return(libsyd.syd_exec(file, argv_array))


def enable_mem() -> bool:
    """
    Enables memory sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_mem())


def disable_mem() -> bool:
    """
    Disables memory sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_mem())


def enabled_mem() -> bool:
    """
    Checks if memory sandboxing is enabled.

    Returns:
        bool: True if memory sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_mem()


def enable_pid() -> bool:
    """
    Enables PID sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_pid())


def disable_pid() -> bool:
    """
    Disables PID sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_pid())


def enabled_pid() -> bool:
    """
    Checks if PID sandboxing is enabled.

    Returns:
        bool: True if PID sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_pid()


def enable_read() -> bool:
    """
    Enables read sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_read())


def disable_read() -> bool:
    """
    Disables read sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_read())


def enabled_read() -> bool:
    """
    Checks if read sandboxing is enabled.

    Returns:
        bool: True if read sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_read()


def enable_stat() -> bool:
    """
    Enables stat sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_stat())


def disable_stat() -> bool:
    """
    Disables stat sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_stat())


def enabled_stat() -> bool:
    """
    Checks if stat sandboxing is enabled.

    Returns:
        bool: True if stat sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_stat()


def enable_write() -> bool:
    """
    Enables write sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_write())


def disable_write() -> bool:
    """
    Disables write sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_write())


def enabled_write() -> bool:
    """
    Checks if write sandboxing is enabled.

    Returns:
        bool: True if write sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_write()


def enable_exec() -> bool:
    """
    Enables exec sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_exec())


def disable_exec() -> bool:
    """
    Disables exec sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_exec())


def enabled_exec() -> bool:
    """
    Checks if exec sandboxing is enabled.

    Returns:
        bool: True if exec sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_exec()


def enable_net() -> bool:
    """
    Enables net sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_enable_net())


def disable_net() -> bool:
    """
    Disables net sandboxing.

    Returns:
        bool: True on successful operation, or raises OSError on failure.
    """
    return check_return(libsyd.syd_disable_net())


def enabled_net() -> bool:
    """
    Checks if net sandboxing is enabled.

    Returns:
        bool: True if net sandboxing is enabled, False otherwise.
    """
    return libsyd.syd_enabled_net()


def kill_add(glob: bytes) -> bool:
    """
    Adds to the list of glob patterns used to determine which paths
    should be killed (prevented from executing) in the sandbox.

    Parameters:
        - glob (bytes): Glob pattern

    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_kill_add(glob))


def kill_del(glob: bytes) -> bool:
    """
    Deletes the first matching item from the end of the list of glob
    patterns used to determine which paths should be killed (prevented
    from executing) in the sandbox.

    Parameters:
        - glob (bytes): Glob pattern

    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_kill_del(glob))


def kill_rem(glob: bytes) -> bool:
    """
    Removes all matching items from the list of glob patterns used to
    determine which paths should be killed (prevented from executing) in
    the sandbox.

    Parameters:
        - glob (bytes): Glob pattern

    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_kill_rem(glob))


def allow_read_add(glob: bytes) -> bool:
    """
    Adds to the allowlist of read sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_read_add(glob))


def allow_read_del(glob: bytes) -> bool:
    """
    Removes the first instance from the end of the allowlist of read
    sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_read_del(glob))


def allow_read_rem(glob: bytes) -> bool:
    """
    Removes all matching patterns from the allowlist of read sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_read_rem(glob))


def deny_read_add(glob: bytes) -> bool:
    """
    Adds to the denylist of read sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_read_add(glob))


def deny_read_del(glob: bytes) -> bool:
    """
    Removes the first instance from the end of the denylist of read
    sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_read_del(glob))


def deny_read_rem(glob: bytes) -> bool:
    """
    Removes all matching patterns from the denylist of read sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_read_rem(glob))


def filter_read_add(glob: bytes) -> bool:
    """
    Adds to the filter of read sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_read_add(glob))


def filter_read_del(glob: bytes) -> bool:
    """
    Removes the first instance from the end of the filter of read
    sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_read_del(glob))


def filter_read_rem(glob: bytes) -> bool:
    """
    Removes all matching patterns from the filter of read sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_read_rem(glob))


def allow_stat_add(glob: bytes) -> bool:
    """
    Adds to the allowlist of stat sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_stat_add(glob))


def allow_stat_del(glob: bytes) -> bool:
    """
    Removes the first instance from the end of the allowlist of stat
    sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_stat_del(glob))


def allow_stat_rem(glob: bytes) -> bool:
    """
    Removes all matching patterns from the allowlist of stat sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_stat_rem(glob))


def deny_stat_add(glob: bytes) -> bool:
    """
    Adds to the denylist of stat sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_stat_add(glob))


def deny_stat_del(glob: bytes) -> bool:
    """
    Removes the first instance from the end of the denylist of stat
    sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_stat_del(glob))


def deny_stat_rem(glob: bytes) -> bool:
    """
    Removes all matching patterns from the denylist of stat sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_stat_rem(glob))


def filter_stat_add(glob: bytes) -> bool:
    """
    Adds to the filter of stat sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_stat_add(glob))


def filter_stat_del(glob: bytes) -> bool:
    """
    Removes the first instance from the end of the filter of stat
    sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_stat_del(glob))


def filter_stat_rem(glob: bytes) -> bool:
    """
    Removes all matching patterns from the filter of stat sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_stat_rem(glob))


def allow_write_add(glob: bytes) -> bool:
    """
    Adds to the allowlist of write sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_write_add(glob))


def allow_write_del(glob: bytes) -> bool:
    """
    Removes the first instance from the end of the allowlist of write
    sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_write_del(glob))


def allow_write_rem(glob: bytes) -> bool:
    """
    Removes all matching patterns from the allowlist of write sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_write_rem(glob))


def deny_write_add(glob: bytes) -> bool:
    """
    Adds to the denylist of write sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_write_add(glob))


def deny_write_del(glob: bytes) -> bool:
    """
    Removes the first instance from the end of the denylist of write
    sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_write_del(glob))


def deny_write_rem(glob: bytes) -> bool:
    """
    Removes all matching patterns from the denylist of write sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_write_rem(glob))


def filter_write_add(glob: bytes) -> bool:
    """
    Adds to the filter of write sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_write_add(glob))


def filter_write_del(glob: bytes) -> bool:
    """
    Removes the first instance from the end of the filter of write
    sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_write_del(glob))


def filter_write_rem(glob: bytes) -> bool:
    """
    Removes all matching patterns from the filter of write sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_write_rem(glob))


def allow_exec_add(glob: bytes) -> bool:
    """
    Adds to the allowlist of exec sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_exec_add(glob))


def allow_exec_del(glob: bytes) -> bool:
    """
    Removes the first instance from the end of the allowlist of exec
    sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_exec_del(glob))


def allow_exec_rem(glob: bytes) -> bool:
    """
    Removes all matching patterns from the allowlist of exec sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_exec_rem(glob))


def deny_exec_add(glob: bytes) -> bool:
    """
    Adds to the denylist of exec sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_exec_add(glob))


def deny_exec_del(glob: bytes) -> bool:
    """
    Removes the first instance from the end of the denylist of exec
    sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_exec_del(glob))


def deny_exec_rem(glob: bytes) -> bool:
    """
    Removes all matching patterns from the denylist of exec sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_exec_rem(glob))


def filter_exec_add(glob: bytes) -> bool:
    """
    Adds to the filter of exec sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_exec_add(glob))


def filter_exec_del(glob: bytes) -> bool:
    """
    Removes the first instance from the end of the filter of exec
    sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_exec_del(glob))


def filter_exec_rem(glob: bytes) -> bool:
    """
    Removes all matching patterns from the filter of exec sandboxing.

    Parameters:
        - glob (bytes): Glob pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_exec_rem(glob))


def allow_net_bind_add(addr: bytes) -> bool:
    """
    Adds to the allowlist of net_bind sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_net_bind_add(addr))


def allow_net_bind_del(addr: bytes) -> bool:
    """
    Removes the first instance from the end of the allowlist of net_bind
    sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_net_bind_del(addr))


def allow_net_bind_rem(addr: bytes) -> bool:
    """
    Removes all matching patterns from the allowlist of net_bind sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_net_bind_rem(addr))


def deny_net_bind_add(addr: bytes) -> bool:
    """
    Adds to the denylist of net_bind sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_net_bind_add(addr))


def deny_net_bind_del(addr: bytes) -> bool:
    """
    Removes the first instance from the end of the denylist of net_bind
    sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_net_bind_del(addr))


def deny_net_bind_rem(addr: bytes) -> bool:
    """
    Removes all matching patterns from the denylist of net_bind sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_net_bind_rem(addr))


def filter_net_bind_add(addr: bytes) -> bool:
    """
    Adds to the filter of net_bind sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_net_bind_add(addr))


def filter_net_bind_del(addr: bytes) -> bool:
    """
    Removes the first instance from the end of the filter of net_bind
    sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_net_bind_del(addr))


def filter_net_bind_rem(addr: bytes) -> bool:
    """
    Removes all matching patterns from the filter of net_bind sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_net_bind_rem(addr))


def allow_net_connect_add(addr: bytes) -> bool:
    """
    Adds to the allowlist of net_connect sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_net_connect_add(addr))


def allow_net_connect_del(addr: bytes) -> bool:
    """
    Removes the first instance from the end of the allowlist of net_connect
    sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_net_connect_del(addr))


def allow_net_connect_rem(addr: bytes) -> bool:
    """
    Removes all matching patterns from the allowlist of net_connect sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_allow_net_connect_rem(addr))


def deny_net_connect_add(addr: bytes) -> bool:
    """
    Adds to the denylist of net_connect sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_net_connect_add(addr))


def deny_net_connect_del(addr: bytes) -> bool:
    """
    Removes the first instance from the end of the denylist of net_connect
    sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_net_connect_del(addr))


def deny_net_connect_rem(addr: bytes) -> bool:
    """
    Removes all matching patterns from the denylist of net_connect sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_deny_net_connect_rem(addr))


def filter_net_connect_add(addr: bytes) -> bool:
    """
    Adds to the filter of net_connect sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_net_connect_add(addr))


def filter_net_connect_del(addr: bytes) -> bool:
    """
    Removes the first instance from the end of the filter of net_connect
    sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_net_connect_del(addr))


def filter_net_connect_rem(addr: bytes) -> bool:
    """
    Removes all matching patterns from the filter of net_connect sandboxing.

    Parameters:
        - addr (bytes): Address pattern
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_net_connect_rem(addr))


def mem_max(size: Union[int, str, bytes]) -> bool:
    """
    Set syd maximum per-process memory usage limit for memory sandboxing,
    parse-size crate is used to parse the value so formatted strings are OK.

    Parameters:
        - size (int|str|bytes): Limit size.
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    if isinstance(size, int):
        size = str(size)
    if isinstance(size, str):
        size = size.encode("utf-8")
    return check_return(libsyd.syd_mem_max(size))


def mem_vm_max(size: Union[int, str, bytes]) -> bool:
    """
    Set syd maximum per-process virtual memory usage limit for memory sandboxing,
    parse-size crate is used to parse the value so formatted strings are OK.

    Parameters:
        - size (int|str|bytes): Limit size.
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    if isinstance(size, int):
        size = str(size)
    if isinstance(size, str):
        size = size.encode("utf-8")
    return check_return(libsyd.syd_mem_vm_max(size))


def pid_max(size: int) -> bool:
    """
    Set syd maximum process id limit for PID sandboxing

    Parameters:
        - size (int): Limit size, must be greater than or equal to zero.
    Returns:
        bool: True on successful operation, or raises a ValueError or an OSError on failure.
    """
    if size < 0:
        raise ValueError("Invalid limit size")
    return check_return(libsyd.syd_pid_max(size))


def filter_mem(state: bool) -> bool:
    """
    Toggle the reporting of access violations for memory sandboxing

    Parameters:
        - state (bool): True to report violations, False to keep silent.
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_mem(state))


def filter_pid(state: bool) -> bool:
    """
    Toggle the reporting of access violations for PID sandboxing

    Parameters:
        - state (bool): True to report violations, False to keep silent.
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_filter_pid(state))


def kill_mem(state: bool) -> bool:
    """
    Toggle kill of the offending process for Memory sandboxing

    Parameters:
        - state (bool): True to kill offending process, False otherwise.
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_kill_mem(state))


def kill_pid(state: bool) -> bool:
    """
    Toggle kill of the offending process for PID sandboxing

    Parameters:
        - state (bool): True to kill offending process, False otherwise.
    Returns:
        bool: True on successful operation, or raises an OSError on failure.
    """
    return check_return(libsyd.syd_kill_pid(state))


###
# Ctypes Function Definitions
###
libsyd.syd_api.restype = ctypes.c_int
libsyd.syd_check.restype = ctypes.c_int
libsyd.syd_panic.restype = ctypes.c_int
libsyd.syd_reset.restype = ctypes.c_int

libsyd.syd_load.restype = ctypes.c_int
libsyd.syd_load.argtypes = [ctypes.c_int]

libsyd.syd_lock.restype = ctypes.c_int
libsyd.syd_lock.argtypes = [ctypes.c_uint]

libsyd.syd_exec.argtypes = [ctypes.c_char_p, ctypes.POINTER(ctypes.c_char_p)]
libsyd.syd_exec.restype = ctypes.c_int

libsyd.syd_enable_mem.restype = ctypes.c_int
libsyd.syd_disable_mem.restype = ctypes.c_int
libsyd.syd_enabled_mem.restype = ctypes.c_bool

libsyd.syd_enable_pid.restype = ctypes.c_int
libsyd.syd_disable_pid.restype = ctypes.c_int
libsyd.syd_enabled_pid.restype = ctypes.c_bool

libsyd.syd_enable_read.restype = ctypes.c_int
libsyd.syd_disable_read.restype = ctypes.c_int
libsyd.syd_enabled_read.restype = ctypes.c_bool

libsyd.syd_enable_stat.restype = ctypes.c_int
libsyd.syd_disable_stat.restype = ctypes.c_int
libsyd.syd_enabled_stat.restype = ctypes.c_bool

libsyd.syd_enable_write.restype = ctypes.c_int
libsyd.syd_disable_write.restype = ctypes.c_int
libsyd.syd_enabled_write.restype = ctypes.c_bool

libsyd.syd_enable_exec.restype = ctypes.c_int
libsyd.syd_disable_exec.restype = ctypes.c_int
libsyd.syd_enabled_exec.restype = ctypes.c_bool

libsyd.syd_enable_net.restype = ctypes.c_int
libsyd.syd_disable_net.restype = ctypes.c_int
libsyd.syd_enabled_net.restype = ctypes.c_bool

libsyd.syd_kill_add.argtypes = [ctypes.c_char_p]
libsyd.syd_kill_add.restype = ctypes.c_int
libsyd.syd_kill_del.argtypes = [ctypes.c_char_p]
libsyd.syd_kill_del.restype = ctypes.c_int
libsyd.syd_kill_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_kill_rem.restype = ctypes.c_int

libsyd.syd_allow_read_add.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_read_add.restype = ctypes.c_int
libsyd.syd_allow_read_del.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_read_del.restype = ctypes.c_int
libsyd.syd_allow_read_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_read_rem.restype = ctypes.c_int

libsyd.syd_deny_read_add.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_read_add.restype = ctypes.c_int
libsyd.syd_deny_read_del.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_read_del.restype = ctypes.c_int
libsyd.syd_deny_read_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_read_rem.restype = ctypes.c_int

libsyd.syd_filter_read_add.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_read_add.restype = ctypes.c_int
libsyd.syd_filter_read_del.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_read_del.restype = ctypes.c_int
libsyd.syd_filter_read_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_read_rem.restype = ctypes.c_int

libsyd.syd_allow_stat_add.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_stat_add.restype = ctypes.c_int
libsyd.syd_allow_stat_del.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_stat_del.restype = ctypes.c_int
libsyd.syd_allow_stat_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_stat_rem.restype = ctypes.c_int

libsyd.syd_deny_stat_add.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_stat_add.restype = ctypes.c_int
libsyd.syd_deny_stat_del.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_stat_del.restype = ctypes.c_int
libsyd.syd_deny_stat_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_stat_rem.restype = ctypes.c_int

libsyd.syd_filter_stat_add.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_stat_add.restype = ctypes.c_int
libsyd.syd_filter_stat_del.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_stat_del.restype = ctypes.c_int
libsyd.syd_filter_stat_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_stat_rem.restype = ctypes.c_int

libsyd.syd_allow_write_add.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_write_add.restype = ctypes.c_int
libsyd.syd_allow_write_del.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_write_del.restype = ctypes.c_int
libsyd.syd_allow_write_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_write_rem.restype = ctypes.c_int

libsyd.syd_deny_write_add.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_write_add.restype = ctypes.c_int
libsyd.syd_deny_write_del.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_write_del.restype = ctypes.c_int
libsyd.syd_deny_write_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_write_rem.restype = ctypes.c_int

libsyd.syd_filter_write_add.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_write_add.restype = ctypes.c_int
libsyd.syd_filter_write_del.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_write_del.restype = ctypes.c_int
libsyd.syd_filter_write_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_write_rem.restype = ctypes.c_int

libsyd.syd_allow_exec_add.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_exec_add.restype = ctypes.c_int
libsyd.syd_allow_exec_del.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_exec_del.restype = ctypes.c_int
libsyd.syd_allow_exec_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_exec_rem.restype = ctypes.c_int

libsyd.syd_deny_exec_add.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_exec_add.restype = ctypes.c_int
libsyd.syd_deny_exec_del.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_exec_del.restype = ctypes.c_int
libsyd.syd_deny_exec_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_exec_rem.restype = ctypes.c_int

libsyd.syd_filter_exec_add.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_exec_add.restype = ctypes.c_int
libsyd.syd_filter_exec_del.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_exec_del.restype = ctypes.c_int
libsyd.syd_filter_exec_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_exec_rem.restype = ctypes.c_int

libsyd.syd_allow_net_bind_add.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_net_bind_add.restype = ctypes.c_int
libsyd.syd_allow_net_bind_del.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_net_bind_del.restype = ctypes.c_int
libsyd.syd_allow_net_bind_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_net_bind_rem.restype = ctypes.c_int

libsyd.syd_deny_net_bind_add.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_net_bind_add.restype = ctypes.c_int
libsyd.syd_deny_net_bind_del.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_net_bind_del.restype = ctypes.c_int
libsyd.syd_deny_net_bind_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_net_bind_rem.restype = ctypes.c_int

libsyd.syd_filter_net_bind_add.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_net_bind_add.restype = ctypes.c_int
libsyd.syd_filter_net_bind_del.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_net_bind_del.restype = ctypes.c_int
libsyd.syd_filter_net_bind_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_net_bind_rem.restype = ctypes.c_int

libsyd.syd_allow_net_connect_add.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_net_connect_add.restype = ctypes.c_int
libsyd.syd_allow_net_connect_del.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_net_connect_del.restype = ctypes.c_int
libsyd.syd_allow_net_connect_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_allow_net_connect_rem.restype = ctypes.c_int

libsyd.syd_deny_net_connect_add.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_net_connect_add.restype = ctypes.c_int
libsyd.syd_deny_net_connect_del.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_net_connect_del.restype = ctypes.c_int
libsyd.syd_deny_net_connect_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_deny_net_connect_rem.restype = ctypes.c_int

libsyd.syd_filter_net_connect_add.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_net_connect_add.restype = ctypes.c_int
libsyd.syd_filter_net_connect_del.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_net_connect_del.restype = ctypes.c_int
libsyd.syd_filter_net_connect_rem.argtypes = [ctypes.c_char_p]
libsyd.syd_filter_net_connect_rem.restype = ctypes.c_int

libsyd.syd_mem_max.argtypes = [ctypes.c_char_p]
libsyd.syd_mem_max.restype = ctypes.c_int
libsyd.syd_mem_vm_max.argtypes = [ctypes.c_char_p]
libsyd.syd_mem_vm_max.restype = ctypes.c_int
libsyd.syd_pid_max.argtypes = [ctypes.c_size_t]
libsyd.syd_pid_max.restype = ctypes.c_int

libsyd.syd_filter_mem.argtypes = [ctypes.c_bool]
libsyd.syd_filter_mem.restype = ctypes.c_int
libsyd.syd_filter_pid.argtypes = [ctypes.c_bool]
libsyd.syd_filter_pid.restype = ctypes.c_int

libsyd.syd_kill_mem.argtypes = [ctypes.c_bool]
libsyd.syd_kill_mem.restype = ctypes.c_int
libsyd.syd_kill_pid.argtypes = [ctypes.c_bool]
libsyd.syd_kill_pid.restype = ctypes.c_int

if __name__ == "__main__":

    class test(unittest.TestCase):
        @classmethod
        def setUpClass(cls):
            # Global precheck with syd_check()
            try:
                check()
            except OSError:
                raise unittest.SkipTest("check() raised OsError, skipping tests.")

        @staticmethod
        def find(rules, pattern):
            for idx, rule in enumerate(reversed(rules)):
                if pattern == rule:
                    return len(rules) - 1 - idx
            return None

        # This must be the first test!
        def test_1_api(self):
            self.assertEqual(api(), 3)

        def test_2_stat(self):
            state = enabled_mem()
            self.assertTrue(enable_mem())
            self.assertTrue(enabled_mem())
            self.assertTrue(disable_mem())
            self.assertFalse(enabled_mem())
            if state:
                enable_mem()
            else:
                disable_mem()

            state = enabled_pid()
            self.assertTrue(enable_pid())
            self.assertTrue(enabled_pid())
            self.assertTrue(disable_pid())
            self.assertFalse(enabled_pid())
            if state:
                enable_pid()
            else:
                disable_pid()

            state = enabled_read()
            self.assertTrue(enable_read())
            self.assertTrue(enabled_read())
            self.assertTrue(disable_read())
            self.assertFalse(enabled_read())
            if state:
                enable_read()
            else:
                disable_read()

            state = enabled_stat()
            self.assertTrue(enable_stat())
            self.assertTrue(enabled_stat())
            self.assertTrue(disable_stat())
            self.assertFalse(enabled_stat())
            if state:
                enable_stat()
            else:
                disable_stat()

            state = enabled_write()
            self.assertTrue(enable_write())
            self.assertTrue(enabled_write())
            self.assertTrue(disable_write())
            self.assertFalse(enabled_write())
            if state:
                enable_write()
            else:
                disable_write()

            state = enabled_exec()
            self.assertTrue(enable_exec())
            self.assertTrue(enabled_exec())
            self.assertTrue(disable_exec())
            self.assertFalse(enabled_exec())
            if state:
                enable_exec()
            else:
                disable_exec()

            state = enabled_net()
            self.assertTrue(enable_net())
            self.assertTrue(enabled_net())
            self.assertTrue(disable_net())
            self.assertFalse(enabled_net())
            if state:
                enable_net()
            else:
                disable_net()

            self.assertTrue(filter_mem(True))
            self.assertTrue(info()["mem_filter"])
            self.assertTrue(filter_mem(False))
            self.assertFalse(info()["mem_filter"])

            self.assertTrue(filter_pid(True))
            self.assertTrue(info()["pid_filter"])
            self.assertTrue(filter_pid(False))
            self.assertFalse(info()["pid_filter"])

            self.assertTrue("kill-mem" not in info()["flags"])
            self.assertTrue(kill_mem(True))
            self.assertTrue("kill-mem" in info()["flags"])
            self.assertTrue(kill_mem(False))
            self.assertTrue("kill-mem" not in info()["flags"])

            self.assertTrue("kill-pid" not in info()["flags"])
            self.assertTrue(kill_pid(True))
            self.assertTrue("kill-pid" in info()["flags"])
            self.assertTrue(kill_pid(False))
            self.assertTrue("kill-pid" not in info()["flags"])

            mem_max_orig = str(info()["mem_max"]).encode("utf-8")
            mem_vm_max_orig = str(info()["mem_vm_max"]).encode("utf-8")
            pid_max_orig = info()["pid_max"]

            self.assertTrue(mem_max("1G".encode("utf-8")))
            self.assertEqual(info()["mem_max"], 1024 * 1024 * 1024)
            self.assertTrue(mem_max("10G".encode("utf-8")))
            self.assertEqual(info()["mem_max"], 10 * 1024 * 1024 * 1024)
            mem_max(mem_max_orig)

            self.assertTrue(mem_vm_max("1G".encode("utf-8")))
            self.assertEqual(info()["mem_vm_max"], 1024 * 1024 * 1024)
            self.assertTrue(mem_vm_max("10G".encode("utf-8")))
            self.assertEqual(info()["mem_vm_max"], 10 * 1024 * 1024 * 1024)
            mem_vm_max(mem_vm_max_orig)

            with self.assertRaises(ValueError):
                pid_max(-1)
            self.assertTrue(pid_max(4096))
            self.assertEqual(info()["pid_max"], 4096)
            self.assertTrue(pid_max(8192))
            self.assertEqual(info()["pid_max"], 8192)
            pid_max(pid_max_orig)

        def test_3_glob(self):
            path = "/tmp/pysyd"
            parg = path.encode("utf-8")

            rule = {"act": "Allow", "cap": "r", "pat": path}
            self.assertTrue(allow_read_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(allow_read_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(allow_read_add(parg))
            self.assertTrue(allow_read_add(parg))
            self.assertTrue(allow_read_add(parg))
            self.assertTrue(allow_read_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "r", "pat": path}
            self.assertTrue(deny_read_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(deny_read_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(deny_read_add(parg))
            self.assertTrue(deny_read_add(parg))
            self.assertTrue(deny_read_add(parg))
            self.assertTrue(deny_read_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "r", "pat": path}
            self.assertTrue(filter_read_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(filter_read_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(filter_read_add(parg))
            self.assertTrue(filter_read_add(parg))
            self.assertTrue(filter_read_add(parg))
            self.assertTrue(filter_read_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "s", "pat": path}
            self.assertTrue(allow_stat_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(allow_stat_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(allow_stat_add(parg))
            self.assertTrue(allow_stat_add(parg))
            self.assertTrue(allow_stat_add(parg))
            self.assertTrue(allow_stat_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "s", "pat": path}
            self.assertTrue(deny_stat_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(deny_stat_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(deny_stat_add(parg))
            self.assertTrue(deny_stat_add(parg))
            self.assertTrue(deny_stat_add(parg))
            self.assertTrue(deny_stat_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "s", "pat": path}
            self.assertTrue(filter_stat_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(filter_stat_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(filter_stat_add(parg))
            self.assertTrue(filter_stat_add(parg))
            self.assertTrue(filter_stat_add(parg))
            self.assertTrue(filter_stat_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "w", "pat": path}
            self.assertTrue(allow_write_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(allow_write_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(allow_write_add(parg))
            self.assertTrue(allow_write_add(parg))
            self.assertTrue(allow_write_add(parg))
            self.assertTrue(allow_write_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "w", "pat": path}
            self.assertTrue(deny_write_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(deny_write_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(deny_write_add(parg))
            self.assertTrue(deny_write_add(parg))
            self.assertTrue(deny_write_add(parg))
            self.assertTrue(deny_write_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "w", "pat": path}
            self.assertTrue(filter_write_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(filter_write_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(filter_write_add(parg))
            self.assertTrue(filter_write_add(parg))
            self.assertTrue(filter_write_add(parg))
            self.assertTrue(filter_write_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "x", "pat": path}
            self.assertTrue(allow_exec_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(allow_exec_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(allow_exec_add(parg))
            self.assertTrue(allow_exec_add(parg))
            self.assertTrue(allow_exec_add(parg))
            self.assertTrue(allow_exec_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "x", "pat": path}
            self.assertTrue(deny_exec_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(deny_exec_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(deny_exec_add(parg))
            self.assertTrue(deny_exec_add(parg))
            self.assertTrue(deny_exec_add(parg))
            self.assertTrue(deny_exec_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "x", "pat": path}
            self.assertTrue(filter_exec_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(filter_exec_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(filter_exec_add(parg))
            self.assertTrue(filter_exec_add(parg))
            self.assertTrue(filter_exec_add(parg))
            self.assertTrue(filter_exec_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

        def test_4_addr(self):
            host = "127.3.1.4/8"
            port = 16
            addr = f"{host}!{port}"
            aarg = addr.encode("utf-8")

            rule = {"act": "Allow", "cap": "b", "pat": {"addr": host, "port": port}}
            self.assertTrue(allow_net_bind_add(aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(allow_net_bind_del(aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(allow_net_bind_add(aarg))
            self.assertTrue(allow_net_bind_add(aarg))
            self.assertTrue(allow_net_bind_add(aarg))
            self.assertTrue(allow_net_bind_rem(aarg))
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "b", "pat": {"addr": host, "port": port}}
            self.assertTrue(deny_net_bind_add(aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(deny_net_bind_del(aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(deny_net_bind_add(aarg))
            self.assertTrue(deny_net_bind_add(aarg))
            self.assertTrue(deny_net_bind_add(aarg))
            self.assertTrue(deny_net_bind_rem(aarg))
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "b", "pat": {"addr": host, "port": port}}
            self.assertTrue(filter_net_bind_add(aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(filter_net_bind_del(aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(filter_net_bind_add(aarg))
            self.assertTrue(filter_net_bind_add(aarg))
            self.assertTrue(filter_net_bind_add(aarg))
            self.assertTrue(filter_net_bind_rem(aarg))
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Allow", "cap": "c", "pat": {"addr": host, "port": port}}
            self.assertTrue(allow_net_connect_add(aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(allow_net_connect_del(aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(allow_net_connect_add(aarg))
            self.assertTrue(allow_net_connect_add(aarg))
            self.assertTrue(allow_net_connect_add(aarg))
            self.assertTrue(allow_net_connect_rem(aarg))
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Deny", "cap": "c", "pat": {"addr": host, "port": port}}
            self.assertTrue(deny_net_connect_add(aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(deny_net_connect_del(aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(deny_net_connect_add(aarg))
            self.assertTrue(deny_net_connect_add(aarg))
            self.assertTrue(deny_net_connect_add(aarg))
            self.assertTrue(deny_net_connect_rem(aarg))
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            rule = {"act": "Filter", "cap": "c", "pat": {"addr": host, "port": port}}
            self.assertTrue(filter_net_connect_add(aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(filter_net_connect_del(aarg))
            rules = info()["cidr_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(filter_net_connect_add(aarg))
            self.assertTrue(filter_net_connect_add(aarg))
            self.assertTrue(filter_net_connect_add(aarg))
            self.assertTrue(filter_net_connect_rem(aarg))
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

        def test_5_kill(self):
            path = "/tmp/pysyd"
            parg = path.encode("utf-8")
            rule = {"act": "Kill", "cap": "x", "pat": path}

            self.assertTrue(kill_add(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertEqual(idx, len(rules) - 1)

            self.assertTrue(kill_del(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

            self.assertTrue(kill_add(parg))
            self.assertTrue(kill_add(parg))
            self.assertTrue(kill_add(parg))
            self.assertTrue(kill_rem(parg))
            rules = info()["glob_rules"]
            idx = self.find(rules, rule)
            self.assertIsNone(idx)

        def test_6_exec(self):
            with tempfile.TemporaryDirectory() as temp_dir:
                # Path to the temporary file
                temp_file = os.path.join(temp_dir, "file")

                # Prepare command and arguments
                file = b"/bin/sh"
                argv = [b"-c", b'echo 42 > "' + temp_file.encode() + b'"']

                # Call syd_exec
                self.assertTrue(exec(file, argv))

                # Wait for syd to execute the process.
                time.sleep(3)

                # Assert the contents of the file
                with open(temp_file, "r") as f:
                    contents = f.read().strip()
                    self.assertEqual(contents, "42")

        def test_7_load(self):
            with tempfile.TemporaryFile() as temp_file:
                temp_file.write(
                    b"""
pid/max:77
"""
                )
                temp_file.seek(0)
                load(temp_file.fileno())
                self.assertEqual(77, info()["pid_max"])

        # This _must_ be the final test,
        # because it locks the sandbox!!
        def test_8_lock(self):
            self.assertTrue(lock(LockState.LOCK_OFF))
            self.assertTrue(lock(LockState.LOCK_EXEC))
            self.assertTrue(lock(LockState.LOCK_ON))

            with self.assertRaises(OSError) as cm:
                lock(LockState.LOCK_OFF)
            self.assertEqual(cm.exception.errno, errno.ENOENT)

            with self.assertRaises(OSError) as cm:
                lock(LockState.LOCK_EXEC)
            self.assertEqual(cm.exception.errno, errno.ENOENT)

            with self.assertRaises(OSError) as cm:
                lock(LockState.LOCK_ON)
            self.assertEqual(cm.exception.errno, errno.ENOENT)

    unittest.main(verbosity=2)
