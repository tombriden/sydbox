#!/usr/bin/env perl
# coding: utf-8
#
# syd: seccomp and landlock based application sandbox with support for namespaces
# lib/src/test.pl: Tests for Perl bindings of libsyd, the syd API C Library
# Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

use strict;
use warnings;
use Test::More;
use Errno qw(EINVAL ENOENT);
use Encode qw( encode );
use File::Temp qw( tempdir tempfile );

BEGIN {
	use Cwd 'abs_path';
	use File::Basename 'dirname';
	unshift @INC, dirname(abs_path($0));
}
use syd qw(:all);

sub find {
	my ($rules_ref, $pattern_ref, $comparator) = @_;
	my @rules   = @{$rules_ref};
	my %pattern = %{$pattern_ref};

	for (my $i = 0; $i < @rules; $i++) {
		my %rule = %{$rules[$#rules - $i]};
		if ($comparator->(\%rule, \%pattern)) {
			return $#rules - $i;
		}
	}

	return undef;
}

eval { check() };
if ($@) {
	if ($! == ENOENT) {
		plan skip_all => 'not running under syd';
	}
	print "Unexpected error checking for syd: $!\n";
	exit 1;
}

plan tests => 245;

my $api = api();
is($api, 3, 'syd API');

my $temp = tempdir(CLEANUP => 1);
my $path = "${temp}/file";

my $file = "/bin/sh";
my @argv = ("-c", 'echo 42 > "' . $path . '"');
ok(exec($file, \@argv), 'exec');

# Wait for syd to execute the process.
sleep 3;

# Assert the contents of the file
{
	open my $fh, '<', $path or die "Can't open file: $!";
	my $contents = do { local $/; <$fh> };
	chomp $contents;
	is($contents, '42', 'exec contents');
}

my $state = enabled_mem();
ok(enable_mem(),   'enable_mem');
ok(enabled_mem(),  'enabled_mem');
ok(disable_mem(),  'disable_mem');
ok(!enabled_mem(), '!enabled_mem');
if ($state) {
	enable_mem();
} else {
	disable_mem();
}

$state = enabled_pid();
ok(enable_pid(),   "enable_pid");
ok(enabled_pid(),  "enabled_pid");
ok(disable_pid(),  "disable_pid");
ok(!enabled_pid(), "!enabled_pid");
if ($state) {
	enable_pid();
} else {
	disable_pid();
}

$state = enabled_read();
ok(enable_read(),   "enable_read");
ok(enabled_read(),  "enabled_read");
ok(disable_read(),  "disable_read");
ok(!enabled_read(), "!enabled_read");
if ($state) {
	enable_read();
} else {
	disable_read();
}

$state = enabled_stat();
ok(enable_stat(),   "enable_stat");
ok(enabled_stat(),  "enabled_stat");
ok(disable_stat(),  "disable_stat");
ok(!enabled_stat(), "!enabled_stat");
if ($state) {
	enable_stat();
} else {
	disable_stat();
}

$state = enabled_write();
ok(enable_write(),   "enable_write");
ok(enabled_write(),  "enabled_write");
ok(disable_write(),  "disable_write");
ok(!enabled_write(), "!enabled_write");
if ($state) {
	enable_write();
} else {
	disable_write();
}

$state = enabled_exec();
ok(enable_exec(),   "enable_exec");
ok(enabled_exec(),  "enabled_exec");
ok(disable_exec(),  "disable_exec");
ok(!enabled_exec(), "!enabled_exec");
if ($state) {
	enable_exec();
} else {
	disable_exec();
}

$state = enabled_net();
ok(enable_net(),   "enable_net");
ok(enabled_net(),  "enabled_net");
ok(disable_net(),  "disable_net");
ok(!enabled_net(), "!enabled_net");
if ($state) {
	enable_net();
} else {
	disable_net();
}

my $info = info();
ok(!$info->{mem_filter}, "mem_filter_orig");
ok(filter_mem(1),        "filter_mem_1");
$info = info();
ok($info->{mem_filter}, "mem_filter_1");
ok(filter_mem(0),       "filter_mem_0");
$info = info();
ok(!$info->{mem_filter}, "mem_filter_0");

ok(!$info->{pid_filter}, "pid_filter_orig");
ok(filter_pid(1),        "filter_pid_1");
$info = info();
ok($info->{pid_filter}, "pid_filter_1");
ok(filter_pid(0),       "filter_pid_0");
$info = info();
ok(!$info->{pid_filter}, "pid_filter_0");

ok(!grep(/kill-mem/, @{$info->{flags}}), "kill_mem_orig");
ok(kill_mem(1),                          "filter_mem_1");
$info = info();
ok(grep(/kill-mem/, @{$info->{flags}}), "kill_mem_1");
ok(kill_mem(0),                         "filter_mem_0");
$info = info();
ok(!grep(/kill-mem/, @{$info->{flags}}), "kill_mem_0");

ok(!grep(/kill-pid/, @{$info->{flags}}), "kill_pid_orig");
ok(kill_pid(1),                          "filter_pid_1");
$info = info();
ok(grep(/kill-pid/, @{$info->{flags}}), "kill_pid_1");
ok(kill_pid(0),                         "filter_pid_0");
$info = info();
ok(!grep(/kill-pid/, @{$info->{flags}}), "kill_pid_0");

$info = info();
my $mem_max_orig    = $info->{mem_max} . "";
my $mem_vm_max_orig = $info->{mem_vm_max} . "";
my $pid_max_orig    = $info->{pid_max};

ok(mem_max("1G"), "mem_max_1G");
$info = info();
is($info->{mem_max}, 1024 * 1024 * 1024, "mem_max_1G_check");
ok(mem_max("10G"), "mem_max_10G");
$info = info();
is($info->{mem_max}, 10 * 1024 * 1024 * 1024, "mem_max_10G_check");
mem_max($mem_max_orig);

ok(mem_vm_max("1G"), "mem_vm_max_1G");
$info = info();
is($info->{mem_vm_max}, 1024 * 1024 * 1024, "mem_vm_max_1G_check");
ok(mem_vm_max("10G"), "mem_vm_max_10G");
$info = info();
is($info->{mem_vm_max}, 10 * 1024 * 1024 * 1024, "mem_vm_max_10G_check");
mem_vm_max($mem_vm_max_orig);

ok(pid_max(4096), "pid_max_4096");
$info = info();
is($info->{pid_max}, 4096, "pid_max_4096_check");
ok(pid_max(8192), "pid_max_8192");
$info = info();
is($info->{pid_max}, 8192, "pid_max_8192_check");
pid_max($pid_max_orig);

$path = "/tmp/plsyd";
my %rule = (act => "Allow", cap => "r", pat => $path);
my $comp = sub {
	my ($rule_ref, $pattern_ref) = @_;

	# Check if 'act' and 'cap' fields match exactly
	return 0 unless $rule_ref->{act} eq $pattern_ref->{act} && $rule_ref->{cap} eq $pattern_ref->{cap};

	# Check if 'pat' field matches the given path
	return 0 unless $rule_ref->{pat} eq $path;

	# If all checks pass, the rule matches the pattern
	return 1;
};
ok(allow_read_add($path), "allow_read_add");
my $rules = info()->{"glob_rules"};
my $idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_read_add index");
ok(allow_read_del($path), "allow_read_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_read_del index");
ok(allow_read_add($path), "allow_read_add_1");
ok(allow_read_add($path), "allow_read_add_2");
ok(allow_read_add($path), "allow_read_add_3");
ok(allow_read_rem($path), "allow_read_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_read_rem index");

%rule = (act => "Deny", cap => "r", pat => $path);
ok(deny_read_add($path), "deny_read_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_read_add index");
ok(deny_read_del($path), "deny_read_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_read_del index");
ok(deny_read_add($path), "deny_read_add_1");
ok(deny_read_add($path), "deny_read_add_2");
ok(deny_read_add($path), "deny_read_add_3");
ok(deny_read_rem($path), "deny_read_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_read_rem index");

%rule = (act => "Filter", cap => "r", pat => $path);
ok(filter_read_add($path), "filter_read_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_read_add index");
ok(filter_read_del($path), "filter_read_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_read_del index");
ok(filter_read_add($path), "filter_read_add_1");
ok(filter_read_add($path), "filter_read_add_2");
ok(filter_read_add($path), "filter_read_add_3");
ok(filter_read_rem($path), "filter_read_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_read_rem index");

%rule = (act => "Allow", cap => "s", pat => $path);
ok(allow_stat_add($path), "allow_stat_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_stat_add index");
ok(allow_stat_del($path), "allow_stat_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_stat_del index");
ok(allow_stat_add($path), "allow_stat_add_1");
ok(allow_stat_add($path), "allow_stat_add_2");
ok(allow_stat_add($path), "allow_stat_add_3");
ok(allow_stat_rem($path), "allow_stat_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_stat_rem index");

%rule = (act => "Deny", cap => "s", pat => $path);
ok(deny_stat_add($path), "deny_stat_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_stat_add index");
ok(deny_stat_del($path), "deny_stat_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_stat_del index");
ok(deny_stat_add($path), "deny_stat_add_1");
ok(deny_stat_add($path), "deny_stat_add_2");
ok(deny_stat_add($path), "deny_stat_add_3");
ok(deny_stat_rem($path), "deny_stat_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_stat_rem index");

%rule = (act => "Filter", cap => "s", pat => $path);
ok(filter_stat_add($path), "filter_stat_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_stat_add index");
ok(filter_stat_del($path), "filter_stat_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_stat_del index");
ok(filter_stat_add($path), "filter_stat_add_1");
ok(filter_stat_add($path), "filter_stat_add_2");
ok(filter_stat_add($path), "filter_stat_add_3");
ok(filter_stat_rem($path), "filter_stat_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_stat_rem index");

%rule = (act => "Allow", cap => "w", pat => $path);
ok(allow_write_add($path), "allow_write_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_write_add index");
ok(allow_write_del($path), "allow_write_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_write_del index");
ok(allow_write_add($path), "allow_write_add_1");
ok(allow_write_add($path), "allow_write_add_2");
ok(allow_write_add($path), "allow_write_add_3");
ok(allow_write_rem($path), "allow_write_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_write_rem index");

%rule = (act => "Deny", cap => "w", pat => $path);
ok(deny_write_add($path), "deny_write_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_write_add index");
ok(deny_write_del($path), "deny_write_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_write_del index");
ok(deny_write_add($path), "deny_write_add_1");
ok(deny_write_add($path), "deny_write_add_2");
ok(deny_write_add($path), "deny_write_add_3");
ok(deny_write_rem($path), "deny_write_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_write_rem index");

%rule = (act => "Filter", cap => "w", pat => $path);
ok(filter_write_add($path), "filter_write_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_write_add index");
ok(filter_write_del($path), "filter_write_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_write_del index");
ok(filter_write_add($path), "filter_write_add_1");
ok(filter_write_add($path), "filter_write_add_2");
ok(filter_write_add($path), "filter_write_add_3");
ok(filter_write_rem($path), "filter_write_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_write_rem index");

%rule = (act => "Allow", cap => "x", pat => $path);
ok(allow_exec_add($path), "allow_exec_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_exec_add index");
ok(allow_exec_del($path), "allow_exec_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_exec_del index");
ok(allow_exec_add($path), "allow_exec_add_1");
ok(allow_exec_add($path), "allow_exec_add_2");
ok(allow_exec_add($path), "allow_exec_add_3");
ok(allow_exec_rem($path), "allow_exec_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_exec_rem index");

%rule = (act => "Deny", cap => "x", pat => $path);
ok(deny_exec_add($path), "deny_exec_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_exec_add index");
ok(deny_exec_del($path), "deny_exec_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_exec_del index");
ok(deny_exec_add($path), "deny_exec_add_1");
ok(deny_exec_add($path), "deny_exec_add_2");
ok(deny_exec_add($path), "deny_exec_add_3");
ok(deny_exec_rem($path), "deny_exec_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_exec_rem index");

%rule = (act => "Filter", cap => "x", pat => $path);
ok(filter_exec_add($path), "filter_exec_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_exec_add index");
ok(filter_exec_del($path), "filter_exec_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_exec_del index");
ok(filter_exec_add($path), "filter_exec_add_1");
ok(filter_exec_add($path), "filter_exec_add_2");
ok(filter_exec_add($path), "filter_exec_add_3");
ok(filter_exec_rem($path), "filter_exec_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_exec_rem index");

my $addr = "127.3.1.4/8";
my $port = 31415;
$path = "${addr}!${port}";
%rule = (act => "Allow", cap => "b", pat => {addr => $addr, port => $port});
$comp = sub {
	my ($rule_ref, $pattern_ref) = @_;

	# Check if 'act' and 'cap' fields match exactly
	return 0 unless $rule_ref->{act} eq $pattern_ref->{act} && $rule_ref->{cap} eq $pattern_ref->{cap};

	# Check if 'pat' field matches the given address.
	return 0 unless $rule_ref->{pat}->{addr} eq $addr;

	# Check if 'pat' field matches the given port.
	return 0 unless $rule_ref->{pat}->{port} eq $port;

	# If all checks pass, the rule matches the pattern
	return 1;
};
ok(allow_net_bind_add($path), "allow_net_bind_add");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_net_bind_add index");
ok(allow_net_bind_del($path), "allow_net_bind_del");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_net_bind_del index");
ok(allow_net_bind_add($path), "allow_net_bind_add_1");
ok(allow_net_bind_add($path), "allow_net_bind_add_2");
ok(allow_net_bind_add($path), "allow_net_bind_add_3");
ok(allow_net_bind_rem($path), "allow_net_bind_rem");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_net_bind_rem index");

%rule = (act => "Deny", cap => "b", pat => {addr => $addr, port => $port});
ok(deny_net_bind_add($path), "deny_net_bind_add");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_net_bind_add index");
ok(deny_net_bind_del($path), "deny_net_bind_del");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_net_bind_del index");
ok(deny_net_bind_add($path), "deny_net_bind_add_1");
ok(deny_net_bind_add($path), "deny_net_bind_add_2");
ok(deny_net_bind_add($path), "deny_net_bind_add_3");
ok(deny_net_bind_rem($path), "deny_net_bind_rem");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_net_bind_rem index");

%rule = (act => "Filter", cap => "b", pat => {addr => $addr, port => $port});
ok(filter_net_bind_add($path), "filter_net_bind_add");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_net_bind_add index");
ok(filter_net_bind_del($path), "filter_net_bind_del");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_net_bind_del index");
ok(filter_net_bind_add($path), "filter_net_bind_add_1");
ok(filter_net_bind_add($path), "filter_net_bind_add_2");
ok(filter_net_bind_add($path), "filter_net_bind_add_3");
ok(filter_net_bind_rem($path), "filter_net_bind_rem");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_net_bind_rem index");

%rule = (act => "Allow", cap => "c", pat => {addr => $addr, port => $port});
ok(allow_net_connect_add($path), "allow_net_connect_add");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "allow_net_connect_add index");
ok(allow_net_connect_del($path), "allow_net_connect_del");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_net_connect_del index");
ok(allow_net_connect_add($path), "allow_net_connect_add_1");
ok(allow_net_connect_add($path), "allow_net_connect_add_2");
ok(allow_net_connect_add($path), "allow_net_connect_add_3");
ok(allow_net_connect_rem($path), "allow_net_connect_rem");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "allow_net_connect_rem index");

%rule = (act => "Deny", cap => "c", pat => {addr => $addr, port => $port});
ok(deny_net_connect_add($path), "deny_net_connect_add");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "deny_net_connect_add index");
ok(deny_net_connect_del($path), "deny_net_connect_del");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_net_connect_del index");
ok(deny_net_connect_add($path), "deny_net_connect_add_1");
ok(deny_net_connect_add($path), "deny_net_connect_add_2");
ok(deny_net_connect_add($path), "deny_net_connect_add_3");
ok(deny_net_connect_rem($path), "deny_net_connect_rem");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "deny_net_connect_rem index");

%rule = (act => "Filter", cap => "c", pat => {addr => $addr, port => $port});
ok(filter_net_connect_add($path), "filter_net_connect_add");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "filter_net_connect_add index");
ok(filter_net_connect_del($path), "filter_net_connect_del");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_net_connect_del index");
ok(filter_net_connect_add($path), "filter_net_connect_add_1");
ok(filter_net_connect_add($path), "filter_net_connect_add_2");
ok(filter_net_connect_add($path), "filter_net_connect_add_3");
ok(filter_net_connect_rem($path), "filter_net_connect_rem");
$rules = info()->{"cidr_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "filter_net_connect_rem index");

$path = "/tmp/plsyd";
%rule = (act => "Kill", cap => "x", pat => $path);
$comp = sub {
	my ($rule_ref, $pattern_ref) = @_;

	# Check if 'act' and 'cap' fields match exactly
	return 0 unless $rule_ref->{act} eq $pattern_ref->{act} && $rule_ref->{cap} eq $pattern_ref->{cap};

	# Check if 'pat' field matches the given path
	return 0 unless $rule_ref->{pat} eq $path;

	# If all checks pass, the rule matches the pattern
	return 1;
};
ok(kill_add($path), "kill_add");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, scalar(@$rules) - 1, "kill_add index");
ok(kill_del($path), "kill_del");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "kill_del index");
ok(kill_add($path), "kill_add_1");
ok(kill_add($path), "kill_add_2");
ok(kill_add($path), "kill_add_3");
ok(kill_rem($path), "kill_rem");
$rules = info()->{"glob_rules"};
$idx   = find($rules, \%rule, $comp);
is($idx, undef, "kill_rem index");

my ($fh, $filename) = tempfile();
print $fh "pid/max:77\n";
seek($fh, 0, 0);
my $fd = fileno($fh);
ok(load($fd), "syd_load_$fd");
close($fh);
is(info()->{"pid_max"}, 77, "syd_load_$fd pid_max");

eval { syd::lock(-1) };
ok($! == EINVAL, "lock -1: $!");
eval { syd::lock(-10) };
ok($! == EINVAL, "lock -10: $!");
eval { syd::lock(-100) };
ok($! == EINVAL, "lock -100: $!");

is(syd::lock(LOCK_OFF),  0, "LOCK_OFF");
is(syd::lock(LOCK_EXEC), 0, "LOCK_EXEC");
is(syd::lock(LOCK_ON),   0, "LOCK_ON");

eval { syd::lock(LOCK_OFF) };
ok($! == ENOENT, "locked LOCK_OFF");
eval { syd::lock(LOCK_EXEC) };
ok($! == ENOENT, "locked LOCK_EXEC");
eval { syd::lock(LOCK_ON) };
ok($! == ENOENT, "locked LOCK_ON");

1;
