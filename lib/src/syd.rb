#!/usr/bin/env ruby
# frozen_string_literal: true

#
# syd: seccomp and landlock based application sandbox with support for namespaces
#
# lib/src/syd.rb: Ruby FFI bindings of libsyd, the syd API C Library
#
# Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
#
# SPDX-License-Identifier: LGPL-3.0-or-later

require "ffi"

# Ruby FFI bindings of libsyd, the syd API C Library
module Syd
  extend FFI::Library
  ffi_lib "syd"

  public

  # Enum for lock states with detailed documentation for each state.

  # LOCK_OFF: The sandbox lock is off, allowing all sandbox commands.
  # This state means that there are no restrictions on sandbox commands,
  # providing full access to sandbox functionalities.
  LOCK_OFF = 0

  # LOCK_EXEC: The sandbox lock is set to on for all processes except
  # the initial process (syd exec child). This is the default state.
  # In this state, the sandbox is locked for all new processes except
  # for the initial process that executed the syd command. This
  # provides a balance between security and functionality, allowing the
  # initial process some level of control while restricting others.
  LOCK_EXEC = 1

  # LOCK_ON: The sandbox lock is on, disallowing all sandbox commands.
  # This state imposes a complete lock down on the sandbox, preventing
  # any sandbox commands from being executed. This is the most
  # restrictive state, ensuring maximum security.
  LOCK_ON = 2

  # Reads the state of the syd sandbox from /dev/syd and returns it
  # as a Ruby hash.
  #
  # This method opens the special file /dev/syd, which contains the
  # current state of the syd sandbox in JSON format. It then parses
  # this state and returns it as a Ruby hash.
  #
  # @return [Hash, NilClass] The current state of the syd sandbox as
  # a Ruby hash, or nil if JSON module is not available.
  # @raise [Errno::ENOENT] If the file /dev/syd cannot be opened.
  # @raise [JSON::ParserError] If the content of /dev/syd is not valid JSON.
  def self.info
    begin
      require "json"
    rescue LoadError
      return nil
    end

    JSON.parse File.read("/dev/syd"), symbolize_names: true
  end

  # Performs a check by calling the 'syd_check' function from the 'syd'
  # library. This function essentially performs an lstat system call on the
  # file "/dev/syd".
  #
  # @return [TrueClass] Returns `true` if the operation is successful.
  # @raise [SystemCallError] Raises the appropriate Ruby exception
  # corresponding to the errno on failure.
  #
  # The 'syd_check' function returns 0 on success and negated errno on failure.
  # In Ruby, this method translates a non-zero return value into a
  # corresponding SystemCallError exception, providing a more idiomatic way of
  # error handling.
  def self.check
    check_return syd_check
  end

  # Performs a syd API check by calling the 'syd_api' function from the
  # 'syd' library.
  #
  # This method is intended to be used as a preliminary check before making any
  # other syd API calls. It is advisable to perform this check to ensure
  # the API is accessible and functioning as expected.
  #
  # @return [Integer] The API number on success.
  # @raise [SystemCallError] A Ruby exception corresponding to the negated errno on failure.
  def self.api
    check_return syd_api
  end

  # Causes syd to exit immediately with code 127.
  #
  # This function is designed to trigger an immediate exit of syd with a
  # specific exit code (127). It should be used in scenarios where an immediate
  # and complete termination of syd is necessary.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.panic
    check_return syd_panic
  end

  # Causes syd to reset sandboxing to the default state. This
  # includes clearing any allowlists, denylists, and filters.
  #
  # This function should be used when it is necessary to reset the state
  # of syd sandboxing environment to its default settings. It's
  # particularly useful in scenarios where the sandboxing environment
  # needs to be reconfigured or cleared of all previous configurations.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.reset
    check_return syd_reset
  end

  # Causes syd to read configuration from the given file descriptor.
  #
  # This function is utilized to load configuration settings for syd
  # from a file represented by the provided file descriptor. It's an
  # essential function for initializing or reconfiguring syd based on
  # external configuration files.
  #
  # @param fd [Integer] The file descriptor of the configuration file.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.load(fd)
    check_return syd_load(fd)
  end

  # Sets the state of the sandbox lock.
  #
  # @param state [Integer] The desired state of the sandbox lock, should be one of LOCK_OFF, LOCK_EXEC, or LOCK_ON.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.lock(state)
    check_return syd_lock(state)
  end

  # Execute a command outside the sandbox without sandboxing.
  #
  # This method is used to execute a command in the operating system, bypassing
  # the sandbox. It takes a file path and an array of arguments, converts them
  # to the appropriate C types, and then invokes the syd_exec function from the
  # syd library.
  #
  # @param file [String] The file path of the command to be executed.
  # @param argv [Array<String>] The arguments to the command.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.exec(file, argv)
    # Convert each argument into a memory pointer to a string
    argv_ptrs = argv.map { |arg| FFI::MemoryPointer.from_string(arg) }
    # Append a null pointer to the end of the array to signify the end of arguments
    argv_ptrs << nil

    # Create a memory pointer that will hold pointers to each argument string
    argv_ptr = FFI::MemoryPointer.new(:pointer, argv_ptrs.length)
    # Copy the pointers to the argument strings into the newly created memory pointer
    argv_ptr.put_array_of_pointer(0, argv_ptrs)

    # Call the syd_exec function and handle the return value
    check_return syd_exec(file, argv_ptr)
  end

  # Enable memory sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_mem
    check_return syd_enable_mem
  end

  # Disable memory sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_mem
    check_return syd_disable_mem
  end

  # Checks if memory sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if memory sandboxing is enabled, `false` otherwise.
  def self.enabled_mem
    syd_enabled_mem
  end

  # Enable PID sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_pid
    check_return syd_enable_pid
  end

  # Disable PID sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_pid
    check_return syd_disable_pid
  end

  # Checks if PID sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if PID sandboxing is enabled, `false` otherwise.
  def self.enabled_pid
    syd_enabled_pid
  end

  # Enable read sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_read
    check_return syd_enable_read
  end

  # Disable read sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_read
    check_return syd_disable_read
  end

  # Checks if read sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if Read sandboxing is enabled, `false` otherwise.
  def self.enabled_read
    syd_enabled_read
  end

  # Enable stat sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_stat
    check_return syd_enable_stat
  end

  # Disable stat sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_stat
    check_return syd_disable_stat
  end

  # Checks if stat sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if stat sandboxing is enabled, `false` otherwise.
  def self.enabled_stat
    syd_enabled_stat
  end

  # Enable write sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_write
    check_return syd_enable_write
  end

  # Disable write sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_write
    check_return syd_disable_write
  end

  # Checks if write sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if write sandboxing is enabled, `false` otherwise.
  def self.enabled_write
    syd_enabled_write
  end

  # Enable exec sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_exec
    check_return syd_enable_exec
  end

  # Disable exec sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_exec
    check_return syd_disable_exec
  end

  # Checks if exec sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if exec sandboxing is enabled, `false` otherwise.
  def self.enabled_exec
    syd_enabled_exec
  end

  # Enable net sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.enable_net
    check_return syd_enable_net
  end

  # Disable net sandboxing.
  #
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.disable_net
    check_return syd_disable_net
  end

  # Checks if net sandboxing is enabled.
  #
  # @return [Boolean] Returns `true` if net sandboxing is enabled, `false` otherwise.
  def self.enabled_net
    syd_enabled_net
  end

  # Adds a path to the allowlist for read sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_read_add(glob)
    check_return syd_allow_read_add(glob)
  end

  # Removes the first instance from the end of the allowlist for read
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_read_del(glob)
    check_return syd_allow_read_del(glob)
  end

  # Removes all matching patterns from the allowlist for read
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_read_rem(glob)
    check_return syd_allow_read_rem(glob)
  end

  # Adds a path to the denylist for read sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_read_add(glob)
    check_return syd_deny_read_add(glob)
  end

  # Removes the first instance from the end of the denylist for read
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_read_del(glob)
    check_return syd_deny_read_del(glob)
  end

  # Removes all matching patterns from the denylist for read
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_read_rem(glob)
    check_return syd_deny_read_rem(glob)
  end

  # Adds a path to the filter for read sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  # with the appropriate errno is raised.
  def self.filter_read_add(glob)
    check_return syd_filter_read_add(glob)
  end

  # Removes the first instance from the end of the filter for read
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_read_del(glob)
    check_return syd_filter_read_del(glob)
  end

  # Removes all matching patterns from the filter for read
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_read_rem(glob)
    check_return syd_filter_read_rem(glob)
  end

  # Adds a path to the allowlist for stat sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_stat_add(glob)
    check_return syd_allow_stat_add(glob)
  end

  # Removes the first instance from the end of the allowlist for stat
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_stat_del(glob)
    check_return syd_allow_stat_del(glob)
  end

  # Removes all matching patterns from the allowlist for stat
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_stat_rem(glob)
    check_return syd_allow_stat_rem(glob)
  end

  # Adds a path to the denylist for stat sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_stat_add(glob)
    check_return syd_deny_stat_add(glob)
  end

  # Removes the first instance from the end of the denylist for stat
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_stat_del(glob)
    check_return syd_deny_stat_del(glob)
  end

  # Removes all matching patterns from the denylist for stat
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_stat_rem(glob)
    check_return syd_deny_stat_rem(glob)
  end

  # Adds a path to the filter for stat sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_stat_add(glob)
    check_return syd_filter_stat_add(glob)
  end

  # Removes the first instance from the end of the filter for stat
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_stat_del(glob)
    check_return syd_filter_stat_del(glob)
  end

  # Removes all matching patterns from the filter for stat
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_stat_rem(glob)
    check_return syd_filter_stat_rem(glob)
  end

  # Adds a path to the allowlist for write sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_write_add(glob)
    check_return syd_allow_write_add(glob)
  end

  # Removes the first instance from the end of the allowlist for write
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_write_del(glob)
    check_return syd_allow_write_del(glob)
  end

  # Removes all matching patterns from the allowlist for write
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_write_rem(glob)
    check_return syd_allow_write_rem(glob)
  end

  # Adds a path to the denylist for write sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_write_add(glob)
    check_return syd_deny_write_add(glob)
  end

  # Removes the first instance from the end of the denylist for write
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_write_del(glob)
    check_return syd_deny_write_del(glob)
  end

  # Removes all matching patterns from the denylist for write
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_write_rem(glob)
    check_return syd_deny_write_rem(glob)
  end

  # Adds a path to the filter for write sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_write_add(glob)
    check_return syd_filter_write_add(glob)
  end

  # Removes the first instance from the end of the filter for write
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_write_del(glob)
    check_return syd_filter_write_del(glob)
  end

  # Removes all matching patterns from the filter for write
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_write_rem(glob)
    check_return syd_filter_write_rem(glob)
  end

  # Adds a path to the allowlist for exec sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_exec_add(glob)
    check_return syd_allow_exec_add(glob)
  end

  # Removes the first instance from the end of the allowlist for exec
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_exec_del(glob)
    check_return syd_allow_exec_del(glob)
  end

  # Removes all matching patterns from the allowlist for exec
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_exec_rem(glob)
    check_return syd_allow_exec_rem(glob)
  end

  # Adds a path to the denylist for exec sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_exec_add(glob)
    check_return syd_deny_exec_add(glob)
  end

  # Removes the first instance from the end of the denylist for exec
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_exec_del(glob)
    check_return syd_deny_exec_del(glob)
  end

  # Removes all matching patterns from the denylist for exec
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_exec_rem(glob)
    check_return syd_deny_exec_rem(glob)
  end

  # Adds a path to the filter for exec sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_exec_add(glob)
    check_return syd_filter_exec_add(glob)
  end

  # Removes the first instance from the end of the filter for exec
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_exec_del(glob)
    check_return syd_filter_exec_del(glob)
  end

  # Removes all matching patterns from the filter for exec
  # sandboxing.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_exec_rem(glob)
    check_return syd_filter_exec_rem(glob)
  end

  # Adds a path to the allowlist for net/bind sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_net_bind_add(addr)
    check_return syd_allow_net_bind_add(addr)
  end

  # Removes the first instance from the end of the allowlist for net/bind
  # sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_net_bind_del(addr)
    check_return syd_allow_net_bind_del(addr)
  end

  # Removes all matching patterns from the allowlist for net/bind
  # sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_net_bind_rem(addr)
    check_return syd_allow_net_bind_rem(addr)
  end

  # Adds a path to the denylist for net/bind sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to
  # the negated errno on failure.
  #
  # The method accepts an address pattern as a string. If the operation is
  # successful, it returns true. In case of an error, a SystemCallError
  # with the appropriate errno is raised.
  def self.deny_net_bind_add(addr)
    check_return syd_deny_net_bind_add(addr)
  end

  # Removes the first instance from the end of the denylist for net/bind
  # sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_net_bind_del(addr)
    check_return syd_deny_net_bind_del(addr)
  end

  # Removes all matching patterns from the denylist for net/bind
  # sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_net_bind_rem(addr)
    check_return syd_deny_net_bind_rem(addr)
  end

  # Adds a path to the filter for net/bind sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_net_bind_add(addr)
    check_return syd_filter_net_bind_add(addr)
  end

  # Removes the first instance from the end of the filter for net/bind
  # sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_net_bind_del(addr)
    check_return syd_filter_net_bind_del(addr)
  end

  # Removes all matching patterns from the filter for net/bind
  # sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_net_bind_rem(addr)
    check_return syd_filter_net_bind_rem(addr)
  end

  # Adds a path to the allowlist for net/connect sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_net_connect_add(addr)
    check_return syd_allow_net_connect_add(addr)
  end

  # Removes the first instance from the end of the allowlist for net/connect
  # sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_net_connect_del(addr)
    check_return syd_allow_net_connect_del(addr)
  end

  # Removes all matching patterns from the allowlist for net/connect
  # sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.allow_net_connect_rem(addr)
    check_return syd_allow_net_connect_rem(addr)
  end

  # Adds a path to the denylist for net/connect sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_net_connect_add(addr)
    check_return syd_deny_net_connect_add(addr)
  end

  # Removes the first instance from the end of the denylist for net/connect
  # sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_net_connect_del(addr)
    check_return syd_deny_net_connect_del(addr)
  end

  # Removes all matching patterns from the denylist for net/connect
  # sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.deny_net_connect_rem(addr)
    check_return syd_deny_net_connect_rem(addr)
  end

  # Adds a path to the filter for net/connect sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_net_connect_add(addr)
    check_return syd_filter_net_connect_add(addr)
  end

  # Removes the first instance from the end of the filter for net/connect
  # sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_net_connect_del(addr)
    check_return syd_filter_net_connect_del(addr)
  end

  # Removes all matching patterns from the filter for net/connect
  # sandboxing.
  #
  # @param addr [String] Address pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_net_connect_rem(addr)
    check_return syd_filter_net_connect_rem(addr)
  end

  # Adds to the list of glob patterns used to determine which paths
  # should be killed (prevented from executing) in the sandbox.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.kill_add(glob)
    check_return syd_kill_add(glob)
  end

  # Deletes the first matching item from the end of the list of glob
  # patterns used to determine which paths should be killed (prevented
  # from executing) in the sandbox.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.kill_del(glob)
    check_return syd_kill_del(glob)
  end

  # Removes all matching items from the list of glob patterns used to
  # determine which paths should be killed (prevented from executing) in
  # the sandbox.
  #
  # @param glob [String] Glob pattern
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.kill_rem(glob)
    check_return syd_kill_rem(glob)
  end

  # Set syd maximum per-process memory usage limit for memory sandboxing,
  # parse-size crate is used to parse the value so formatted strings are OK.
  #
  # @param size [String] Limit size.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mem_max(size)
    check_return syd_mem_max(size)
  end

  # Set syd maximum per-process virtual memory usage limit for memory sandboxing,
  # parse-size crate is used to parse the value so formatted strings are OK.
  #
  # @param size [String] Limit size.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.mem_vm_max(size)
    check_return syd_mem_vm_max(size)
  end

  # Set syd maximum process id limit for PID sandboxing
  #
  # @param size [Integer] Limit size, must be greater than or equal to zero.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.pid_max(size)
    check_return syd_pid_max(size)
  end

  # Toggle the reporting of access violations for memory sandboxing
  #
  # @param state [Boolean] `true` to report violations, false to keep silent.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_mem(state)
    check_return syd_filter_mem(state)
  end

  # Toggle the reporting of access violations for PID sandboxing
  #
  # @param state [Boolean] `true` to report violations, false to keep silent.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.filter_pid(state)
    check_return syd_filter_pid(state)
  end

  # Toggle kill of the offending process for Memory sandboxing
  #
  # @param state [Boolean] `true` to kill offending process, `false` otherwise.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.kill_mem(state)
    check_return syd_kill_mem(state)
  end

  # Toggle kill of the offending process for PID sandboxing
  #
  # @param state [Boolean] `true` to kill offending process, `false` otherwise.
  # @return [TrueClass] Returns `true` on successful operation.
  # @raise [SystemCallError] Raises a Ruby exception corresponding to the negated errno on failure.
  def self.kill_pid(state)
    check_return syd_kill_pid(state)
  end

  # Helper method to process return values from libsyd calls
  def self.check_return(r)
    # Convert negative errno to Ruby exception.
    raise Errno.const_get(Errno.constants.find { |e| -r == Errno.const_get(e)::Errno }) unless r >= 0

    r.zero? ? true : r
  end

  attach_function :syd_check, [], :int
  attach_function :syd_api, [], :int
  attach_function :syd_panic, [], :int
  attach_function :syd_reset, [], :int
  attach_function :syd_load, [:int], :int
  attach_function :syd_lock, [:uint8], :int
  attach_function :syd_exec, %i[string pointer], :int

  attach_function :syd_enable_mem, [], :int
  attach_function :syd_disable_mem, [], :int
  attach_function :syd_enabled_mem, [], :bool
  attach_function :syd_enable_pid, [], :int
  attach_function :syd_disable_pid, [], :int
  attach_function :syd_enabled_pid, [], :bool
  attach_function :syd_enable_read, [], :int
  attach_function :syd_disable_read, [], :int
  attach_function :syd_enabled_read, [], :bool
  attach_function :syd_enable_stat, [], :int
  attach_function :syd_disable_stat, [], :int
  attach_function :syd_enabled_stat, [], :bool
  attach_function :syd_enable_write, [], :int
  attach_function :syd_disable_write, [], :int
  attach_function :syd_enabled_write, [], :bool
  attach_function :syd_enable_exec, [], :int
  attach_function :syd_disable_exec, [], :int
  attach_function :syd_enabled_exec, [], :bool
  attach_function :syd_enable_net, [], :int
  attach_function :syd_disable_net, [], :int
  attach_function :syd_enabled_net, [], :bool

  attach_function :syd_allow_read_add, [:string], :int
  attach_function :syd_allow_read_del, [:string], :int
  attach_function :syd_allow_read_rem, [:string], :int
  attach_function :syd_deny_read_add, [:string], :int
  attach_function :syd_deny_read_del, [:string], :int
  attach_function :syd_deny_read_rem, [:string], :int
  attach_function :syd_filter_read_add, [:string], :int
  attach_function :syd_filter_read_del, [:string], :int
  attach_function :syd_filter_read_rem, [:string], :int
  attach_function :syd_allow_stat_add, [:string], :int
  attach_function :syd_allow_stat_del, [:string], :int
  attach_function :syd_allow_stat_rem, [:string], :int
  attach_function :syd_deny_stat_add, [:string], :int
  attach_function :syd_deny_stat_del, [:string], :int
  attach_function :syd_deny_stat_rem, [:string], :int
  attach_function :syd_filter_stat_add, [:string], :int
  attach_function :syd_filter_stat_del, [:string], :int
  attach_function :syd_filter_stat_rem, [:string], :int
  attach_function :syd_allow_write_add, [:string], :int
  attach_function :syd_allow_write_del, [:string], :int
  attach_function :syd_allow_write_rem, [:string], :int
  attach_function :syd_deny_write_add, [:string], :int
  attach_function :syd_deny_write_del, [:string], :int
  attach_function :syd_deny_write_rem, [:string], :int
  attach_function :syd_filter_write_add, [:string], :int
  attach_function :syd_filter_write_del, [:string], :int
  attach_function :syd_filter_write_rem, [:string], :int
  attach_function :syd_allow_exec_add, [:string], :int
  attach_function :syd_allow_exec_del, [:string], :int
  attach_function :syd_allow_exec_rem, [:string], :int
  attach_function :syd_deny_exec_add, [:string], :int
  attach_function :syd_deny_exec_del, [:string], :int
  attach_function :syd_deny_exec_rem, [:string], :int
  attach_function :syd_filter_exec_add, [:string], :int
  attach_function :syd_filter_exec_del, [:string], :int
  attach_function :syd_filter_exec_rem, [:string], :int
  attach_function :syd_allow_net_bind_add, [:string], :int
  attach_function :syd_allow_net_bind_del, [:string], :int
  attach_function :syd_allow_net_bind_rem, [:string], :int
  attach_function :syd_deny_net_bind_add, [:string], :int
  attach_function :syd_deny_net_bind_del, [:string], :int
  attach_function :syd_deny_net_bind_rem, [:string], :int
  attach_function :syd_filter_net_bind_add, [:string], :int
  attach_function :syd_filter_net_bind_del, [:string], :int
  attach_function :syd_filter_net_bind_rem, [:string], :int
  attach_function :syd_allow_net_connect_add, [:string], :int
  attach_function :syd_allow_net_connect_del, [:string], :int
  attach_function :syd_allow_net_connect_rem, [:string], :int
  attach_function :syd_deny_net_connect_add, [:string], :int
  attach_function :syd_deny_net_connect_del, [:string], :int
  attach_function :syd_deny_net_connect_rem, [:string], :int
  attach_function :syd_filter_net_connect_add, [:string], :int
  attach_function :syd_filter_net_connect_del, [:string], :int
  attach_function :syd_filter_net_connect_rem, [:string], :int

  attach_function :syd_kill_add, [:string], :int
  attach_function :syd_kill_del, [:string], :int
  attach_function :syd_kill_rem, [:string], :int

  attach_function :syd_mem_max, [:string], :int
  attach_function :syd_mem_vm_max, [:string], :int

  attach_function :syd_pid_max, [:uint], :int

  attach_function :syd_filter_mem, [:bool], :int
  attach_function :syd_filter_pid, [:bool], :int

  attach_function :syd_kill_mem, [:bool], :int
  attach_function :syd_kill_pid, [:bool], :int

  freeze
end

if __FILE__ == $PROGRAM_NAME
  require "minitest/autorun"

  # @api private
  class SydTest < Minitest::Test
    # This line ensures tests run sequentially
    # We need this because once you lock the sandbox,
    # there is no going back...
    i_suck_and_my_tests_are_order_dependent!

    # This method is run before each test
    def setup
      Syd.check
    rescue SystemCallError => e
      skip "check() raised SystemCallError, skipping tests: #{e}"
    end

    def test_1_api
      assert_equal 3, Syd.api, "Syd.api should return 3"
    end

    def test_2_stat
      state = Syd.enabled_mem
      assert_equal true, Syd.enable_mem
      assert_equal true, Syd.enabled_mem
      assert_equal true, Syd.disable_mem
      assert_equal false, Syd.enabled_mem
      if state
        Syd.enable_mem
      else
        Syd.disable_mem
      end

      state = Syd.enabled_pid
      assert_equal true, Syd.enable_pid
      assert_equal true, Syd.enabled_pid
      assert_equal true, Syd.disable_pid
      assert_equal false, Syd.enabled_pid
      if state
        Syd.enable_pid
      else
        Syd.disable_pid
      end

      state = Syd.enabled_read
      assert_equal true, Syd.enable_read
      assert_equal true, Syd.enabled_read
      assert_equal true, Syd.disable_read
      assert_equal false, Syd.enabled_read
      if state
        Syd.enable_read
      else
        Syd.disable_read
      end

      state = Syd.enabled_stat
      assert_equal true, Syd.enable_stat
      assert_equal true, Syd.enabled_stat
      assert_equal true, Syd.disable_stat
      assert_equal false, Syd.enabled_stat
      if state
        Syd.enable_stat
      else
        Syd.disable_stat
      end

      state = Syd.enabled_write
      assert_equal true, Syd.enable_write
      assert_equal true, Syd.enabled_write
      assert_equal true, Syd.disable_write
      assert_equal false, Syd.enabled_write
      if state
        Syd.enable_write
      else
        Syd.disable_write
      end

      state = Syd.enabled_exec
      assert_equal true, Syd.enable_exec
      assert_equal true, Syd.enabled_exec
      assert_equal true, Syd.disable_exec
      assert_equal false, Syd.enabled_exec
      if state
        Syd.enable_exec
      else
        Syd.disable_exec
      end

      state = Syd.enabled_net
      assert_equal true, Syd.enable_net
      assert_equal true, Syd.enabled_net
      assert_equal true, Syd.disable_net
      assert_equal false, Syd.enabled_net
      if state
        Syd.enable_net
      else
        Syd.disable_net
      end

      assert_equal true, Syd.filter_mem(true)
      assert_equal true, Syd.info[:mem_filter]
      assert_equal true, Syd.filter_mem(false)
      assert_equal false, Syd.info[:mem_filter]

      assert_equal true, Syd.filter_pid(true)
      assert_equal true, Syd.info[:pid_filter]
      assert_equal true, Syd.filter_pid(false)
      assert_equal false, Syd.info[:pid_filter]

      refute_includes Syd.info[:flags], "kill-mem"
      assert_equal true, Syd.kill_mem(true)
      assert_includes Syd.info[:flags], "kill-mem"
      assert_equal true, Syd.kill_mem(false)
      refute_includes Syd.info[:flags], "kill-mem"

      refute_includes Syd.info[:flags], "kill-pid"
      assert_equal true, Syd.kill_pid(true)
      assert_includes Syd.info[:flags], "kill-pid"
      assert_equal true, Syd.kill_pid(false)
      refute_includes Syd.info[:flags], "kill-pid"

      mem_max_orig = Syd.info[:mem_max]
      mem_vm_max_orig = Syd.info[:mem_vm_max]
      pid_max_orig = Syd.info[:pid_max]

      assert_equal true, Syd.mem_max("1G")
      assert_equal 1024 * 1024 * 1024, Syd.info[:mem_max]
      assert_equal true, Syd.mem_max("10G")
      assert_equal 10 * 1024 * 1024 * 1024, Syd.info[:mem_max]
      Syd.mem_max(mem_max_orig.to_s)

      assert_equal true, Syd.mem_vm_max("1G")
      assert_equal 1024 * 1024 * 1024, Syd.info[:mem_vm_max]
      assert_equal true, Syd.mem_vm_max("10G")
      assert_equal 10 * 1024 * 1024 * 1024, Syd.info[:mem_vm_max]
      Syd.mem_vm_max(mem_vm_max_orig.to_s)

      assert_equal true, Syd.pid_max(4096)
      assert_equal 4096, Syd.info[:pid_max]
      assert_equal true, Syd.pid_max(8192)
      assert_equal 8192, Syd.info[:pid_max]
      Syd.pid_max(pid_max_orig)
    end

    def test_3_glob
      path = "/tmp/rbsyd"

      rule = { act: "Allow", cap: "r", pat: path }
      assert Syd.allow_read_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.allow_read_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.allow_read_add(path) }
      assert Syd.allow_read_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "r", pat: path }
      assert Syd.deny_read_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.deny_read_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.deny_read_add(path) }
      assert Syd.deny_read_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "r", pat: path }
      assert Syd.filter_read_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.filter_read_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.filter_read_add(path) }
      assert Syd.filter_read_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "s", pat: path }
      assert Syd.allow_stat_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.allow_stat_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.allow_stat_add(path) }
      assert Syd.allow_stat_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "s", pat: path }
      assert Syd.deny_stat_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.deny_stat_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.deny_stat_add(path) }
      assert Syd.deny_stat_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "s", pat: path }
      assert Syd.filter_stat_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.filter_stat_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.filter_stat_add(path) }
      assert Syd.filter_stat_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "w", pat: path }
      assert Syd.allow_write_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.allow_write_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.allow_write_add(path) }
      assert Syd.allow_write_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "w", pat: path }
      assert Syd.deny_write_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.deny_write_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.deny_write_add(path) }
      assert Syd.deny_write_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "w", pat: path }
      assert Syd.filter_write_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.filter_write_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.filter_write_add(path) }
      assert Syd.filter_write_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "x", pat: path }
      assert Syd.allow_exec_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.allow_exec_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.allow_exec_add(path) }
      assert Syd.allow_exec_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "x", pat: path }
      assert Syd.deny_exec_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.deny_exec_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.deny_exec_add(path) }
      assert Syd.deny_exec_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "x", pat: path }
      assert Syd.filter_exec_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.filter_exec_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.filter_exec_add(path) }
      assert Syd.filter_exec_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx
    end

    def test_4_addr
      host = "127.3.1.4/8"
      port = 16
      addr = "#{host}!#{port}"

      rule = { act: "Allow", cap: "b", pat: { addr: host, port: port } }
      assert Syd.allow_net_bind_add(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.allow_net_bind_del(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.allow_net_bind_add(addr) }
      assert Syd.allow_net_bind_rem(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "b", pat: { addr: host, port: port } }
      assert Syd.deny_net_bind_add(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.deny_net_bind_del(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.deny_net_bind_add(addr) }
      assert Syd.deny_net_bind_rem(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "b", pat: { addr: host, port: port } }
      assert Syd.filter_net_bind_add(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.filter_net_bind_del(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.filter_net_bind_add(addr) }
      assert Syd.filter_net_bind_rem(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Allow", cap: "c", pat: { addr: host, port: port } }
      assert Syd.allow_net_connect_add(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.allow_net_connect_del(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.allow_net_connect_add(addr) }
      assert Syd.allow_net_connect_rem(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Deny", cap: "c", pat: { addr: host, port: port } }
      assert Syd.deny_net_connect_add(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.deny_net_connect_del(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.deny_net_connect_add(addr) }
      assert Syd.deny_net_connect_rem(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      rule = { act: "Filter", cap: "c", pat: { addr: host, port: port } }
      assert Syd.filter_net_connect_add(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.filter_net_connect_del(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.filter_net_connect_add(addr) }
      assert Syd.filter_net_connect_rem(addr)
      rules = Syd.info[:cidr_rules]
      idx = find(rules, rule)
      assert_nil idx
    end

    def test_5_kill
      path = "/tmp/rbsyd"

      rule = { act: "Kill", cap: "x", pat: path }
      assert Syd.kill_add(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_equal idx, rules.length - 1

      assert Syd.kill_del(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx

      3.times { assert Syd.kill_add(path) }
      assert Syd.kill_rem(path)
      rules = Syd.info[:glob_rules]
      idx = find(rules, rule)
      assert_nil idx
    end

    def test_6_exec
      # Create a temporary directory
      Dir.mktmpdir do |temp|
        path = File.join(temp, "file")

        # Prepare the command and arguments
        file = "/bin/sh"
        argv = ["-c", "echo 42 > '#{path}'"]

        # Execute the command
        assert_equal true, Syd.exec(file, argv), "exec"

        # Wait for the command to execute
        sleep 3

        # Assert the contents of the file
        contents = File.read(path).chomp
        assert_equal "42", contents, "exec contents"
      end
    end

    def test_7_load
      # Create a temporary file and write the specified content to it
      Tempfile.open do |tempfile|
        tempfile.write("pid/max:77\n")
        tempfile.rewind # Seek back to the beginning of the file

        # Load the file descriptor with Syd.load
        Syd.load(tempfile.fileno)

        # Fetch information with Syd.info
        info = Syd.info

        # Check if pid_max is equal to 77
        assert_equal 77, info[:pid_max], "Expected pid_max to be 77"
      end
    end

    def test_8_lock
      # Invalid states
      [-1, -10, -100, 10, 20, 30].each do |invalid_state|
        assert_raises(Errno::EINVAL, "lock #{invalid_state}") do
          Syd.lock(invalid_state)
        end
      end

      # This locks the sandbox in the last iteration.
      [Syd::LOCK_OFF, Syd::LOCK_EXEC, Syd::LOCK_ON].each do |valid_state|
        assert_equal true, Syd.lock(valid_state), "LOCK state set to #{valid_state}"
      end

      [Syd::LOCK_OFF, Syd::LOCK_EXEC, Syd::LOCK_ON].each do |no_state|
        # Once locked valid states will error too.
        assert_raises(Errno::ENOENT, "lock #{no_state}") do
          Syd.lock(no_state)
        end
      end
    end
  end

  private
  def find(rules, pattern)
    rules.reverse_each.with_index do |rule, idx|
      return rules.length - 1 - idx if pattern == rule
    end
    nil
  end
end
