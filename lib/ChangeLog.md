ChangeLog
=========

# 3.1.3

- Improve documentation

# 3.1.2

- Fix tests broken by parse-size switching from decimal to binary in
  syd-3.11.2

# 3.1.1

- Fix tests broken by the environment hardening in syd-3.11.1

# 3.1.0

- Sync API changes in syd-3.10.0

# 3.0.7

- go: Add new function `KillMem`.
- lisp: Add new function `syd-kill-mem`.
- perl: Add new function `kill_mem`.
- python: Add new function `kill_mem`.
- ruby: Add new function: `kill_mem`.
- Implement new function `syd_kill_mem`.

# 3.0.6

- go: Add new function `KillPid`.
- lisp: Add new function `syd-kill-pid`.
- perl: Add new function `kill_pid`.
- python: Add new function `kill_pid`.
- ruby: Add new function: `kill_pid`.
- Implement new function `syd_kill_pid`.
- Set MSRV to 1.56.1 as determined by `cargo-msrv`.
- Correct git repository in `Cargo.toml`.

# 3.0.5

- ruby: Add missing functions `Syd.deny_read_del` and `Syd.deny_read_rem`.
- lisp: fix various warnings during byte-compiling with recent Emacs.
- ruby: Add example demonstrating usage.

# 3.0.4

- ruby: Initial Ruby bindings using `ffi` gem.
- lisp: Add new function `syd-info`.
- lisp: Add new function `syd-filter-mem` and `syd-filter-pid`.
- go: Add new functions `FilterMem` and `FilterPid`.
- perl: Add new functions `filter_mem` and `filter_pid`.
- python: Add new functions `filter_mem` and `filter_pid`.
- Implement new functions `syd_filter_mem` and `syd_filter_pid`.

# 3.0.3

- Add `syd.el`, syd interface for Emacs Lisp
- use **itoa** crate to make `syd_pid_max` function simpler and more
  efficient.
- add examples demonstrating library usage

# 3.0.2

- Fix tests on musl systems
- Drop dependency on the **nix** crate
- Add additional safety checks that `/dev/syd` is the correct file
- Fix a segfault that happens on musl systems

# 3.0.1

- go: initial libsyd Go bindings

# 3.0.0

- Define `LockState` enum and `LOCK_OFF`, `LOCK_EXEC` and `LOCK_ON`
  constants. These were already defined for perl, and python so this
  makes the C library compatible.
- use **itoa** crate to make `syd_load` function simpler and more
  efficient.
- python: Implement the new function **load**.
- perl: Implement the new function **load**.
- Implement the new function **syd\_load**.
- python: Implement new functions **panic** and **reset**.
- perl: Implement new functions **panic** and **reset**.
- Implement new functions **syd\_panic()** and **syd\_reset()**.
- perl: fix argument handling of the `exec` function
- perl: new constant `$syd::LIBSYD_PATH` points to the path of libsyd.so.
- Start using `nix` to call `lstat` rather than Rust std.
- Define `__all__` for `pysyd` such that `from syd import *` becomes
  practical.

# 3.0.0-alpha.6

- Initial Perl bindings

# 3.0.0-alpha.5

- Fix tests

# 3.0.0-alpha.4

- Drop cbindgen dependency, include `syd.h`

# 3.0.0-alpha.3

- Add build instructions to the readme

# 3.0.0-alpha.2

- Fix a problem with Doxygen API documentation generation

# 3.0.0-alpha.1

- Initial implementation of libsyd & syd.py
