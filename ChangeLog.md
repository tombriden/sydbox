ChangeLog
=========

# ?

- **syd-mem** now correctly reports errors.
- Ensure config parser gracefully handles binary files.
- **syd-cat** now accepts a path argument to validate the given syd profile.
- Fix tests to run under docker, note `--cap-add CAP_SYS_PTRACE` is
  necessary.

# 3.14.0

- Plug a socket leak bringing loopback device up in net namespace.
- Drop sendfd crate dependency.
- Use a pipe pair rather than a socket pair to transfer seccomp fd.
- Set `trace/allow_unsafe_prlimit:true` for Paludis profile.
- Do not drop `CAP_DAC_OVERRIDE` unless entering a user ns.
- Increase thread pool keep alive timeout from 15 seconds to 90 seconds.
- Drop the capability `CAP_SETFCAP` at startup by default.
- Drop the capability `CAP_DAC_READ_SEARCH` at startup by default.
- Drop the capability `CAP_DAC_OVERRIDE` at startup by default.
- Drop broken /dev/syd handling in getdents handler.
- Detect and handle mapped IPv4 addresses for IPv6 addresses.
- Fix a minor bug with rule parsing for rules with a trailing slash.
- Fix a minor bug with rule parsing for allow/denylists.
- Harden the getdents handler by making an initial access check for the dir before entries.
- Rework network sandboxing, fix recvfrom handler to properly check for source address.
- Do not validate the address length argument network system calls as
  they cannot be fully trusted.
- PID sandboxing can now only be turned on at startup for performance reasons.
- Memory sandboxing can now only be turned on at startup for performance reasons.
- Fix **syd-read** from failing on existing entries.
- Do not do a preliminary normalize pass before canonicalize, breaks
  paths with both symlinks and dots in them.
- The tool **syd-norm** has been removed.
- Trim trailing nul-bytes in UNIX domain sockets before canonicalization.
- Harden landlock profile by allowing /proc read-only rather than read-write.
- Canonicalize paths of UNIX domain sockets.

# 3.13.4

- Revert: Harden `private_tmp` and `private_shm` by bindmounting
  /var/empty over /var/tmp. Breaks Paludis.

# 3.13.3

- Fix mkdir handler to return EEXIST on existing dirs rather than
  raising an access violation.
- Mask kernel filesystems in immutable profile.
- Mount private proc before bind mounts to allow mounting over proc entries.
- Continue execution in case a bindmount fails with ENOENT indicating
  source or target does not exist. This way profiles can provide
  limitations without failing on systems that don't have the respective
  files.
- syd will now fail early if `root:` is specified with one of
  `trace/private_shm` or `trace/private_tmp` as these features
  contradict with each other and do not work together.
- Fix chroot failing in mount namespace due to non-recursive bindmount.
- Harden `private_tmp` and `private_shm` by bindmounting /var/empty over /var/tmp.
- Harden `private_tmp` by mounting private /tmp with nodev and nosuid options.
- Harden `private_shm` by mounting private /dev/shm with nodev, nosuid and noexec options.
- Improve symlink loop detection in path canonicalizer.

# 3.13.2

- Various minor performace improvements to path canonicalizer and normalizer.
- Improve syscall handler lookup, avoid string match on each syscall.
- Fix logging not to create json objects if the log level is disabled.

# 3.13.1

- Fix empty path handling in readlink handlers.

# 3.13.0

- Add handlers for `readlink` and `readlinkat` for stat sandboxing.
- Bump MSRV from 1.70 to 1.71.
- Improve /proc/self, /proc/thread-self magic link handling, fixing a
  known sandbox break, see the integration test
  `procself_escape_symlink` for more information.
- Improve handling of the magic /proc/pid/fd symbolic links, return
  ELOOP if the path does not belong to the current process akin to
  `RESOLVE_NO_MAGICLINKS`. Such fds are often misused to break out of
  containers which makes this an important hardening.
- consolidate error logging in json logs.
- pass `RESOLVE_NO_MAGICLINKS` along with `RESOLVE_NO_SYMLINKS` to the
  openat2 calls in open and stat handlers for added security.
- new tool **syd-open** to lookup open flags by number or name.
- improve the efficiency of the stat handler
- improve open handler and fix handling of dir file descriptors in openat{,2}.
- drop noatime from immutable profile mounts to allow unprivileged mount.
- ldd: fix determining syd path on Exherbo.
- Implement `trace/allow_unsafe_perf` command to allow perf inside the sandbox.
- Implement `trace/allow_unsafe_ptrace` command to allow ptrace inside the sandbox.
- Drop `O_PATH` stat sandbox special casing.
- Add setting `trace/allow_unsafe_caps` to skip dropping Linux capabilities on startup.
- Rename `trace/allow_unsafe_socket_families` `trace/allow_unsafe_socket`.
- Rename `trace/allow_unsupported_socket_families` `trace/allow_unsupp_socket`.
- Rename `trace/allow_successful_bind` `trace/allow_safe_bind`.
- Rename `trace/allow_unsafe_environment` `trace/allow_unsafe_env`.
- Improve getdents handler by skipping dot entries from access check.
- Improve proc umask function.
- Add an improved proc tgid get implementation.
- Start using missing mode handlers for path canonicalization.
- Improve the /proc fd check in path canonicalizer.
- Improve the efficiency of `syd::proc::proc_cmdline` function.
- Verify process using seccomp-id-valid after `pidfd_open` calls.
- Drop excessive seccomp-id-valid calls.
- Avoid a needless `faccessat` in path canonicalizer.
- Improve path sandboxing implementation to reduce the number of syscalls.
- Avoid another needless canonicalize in read path function.
- Keep `CAP_FSETID` capability at startup.
- Keep `CAP_FOWNER` capability at startup.
- Keep `CAP_SETPCAP` capability at startup.
- Keep `CAP_MKNOD` capability at startup.
- Keep `CAP_AUDIT_WRITE` capability at startup.

# 3.12.4

- Avoid needless path canonicalization in getdents handler
- Simplify and improve process read directory function

# 3.12.3

- syd-tty no longer prints the tty of the current process when no pid is given.
- Improve process tty function
- syd-ldd: fix issue determining which syd to execute
- Avoid needlessly determining thread group id once per syscall
- Improve process umask function

# 3.12.2

- Drop `CAP_NET_BROADCAST` capability at startup.
- Do not drop the `CAP_DAC_OVERRIDE` and `CAP_DAC_READ_SEARCH` capabilities
  which may be necessary during path resolution.
- Remove needless, special casing **faccessat** calls for **EEXIST**
  check in **mkdir** and **mknod** handlers.
- Refactor path sandboxing to reduce allocations.
- Improve the path canonicalizer performance by allocating the symlink
  loop detection set only when really necessary.
- Add initial manual page for `syd.7`.

# 3.12.0

- Add initial manual page for `syd-cat`.
- Add initial manual page for `syd-env`.
- Add initial manual page for `syd-err`.
- Add initial manual page for `syd-exec`.
- Add initial manual page for `syd-ldd`.
- Add initial manual page for `syd-lock`.
- Add initial manual page for `syd-log`.
- Add initial manual page for `syd-ls`.
- Add initial manual page for `syd-mem`.
- Add initial manual page for `syd-norm`.
- Add initial manual page for `syd-read`.
- Add initial manual page for `syd-run`.
- Add initial manual page for `syd-size`.
- Add initial manual page for `syd-stat`.
- Add initial manual page for `syd-sys`.
- Add initial manual page for `syd-test`.
- Add initial manual page for `syd-tty`.
- `syd-ls` learned the new sets **setid**, **time**, and **uring**.
- New sandbox command `trace/allow_unsafe_uring` to allow io_uring
  interface.
- Improve symlink loop detection in path canonicalization.
- Consolidate boolean parsing in sandbox commands.
- Add initial `syd.1`, `syd.2`, and `syd.5` manual pages.
- Add initial manual page for `syd-chk`.
- New tool `syd-env` to run a command with the environment of a process.
- Helpers now support `-h` to print help.
- Add sample OpenNTPD profile.
- `syd-ls setid` now lists the setid system calls.
- Add `setgroups` and `setgroups32` to `SETID_SYSCALLS`.
- Add [Known Bugs](#known-bugs) section to the readme.
- Extend the parent seccomp filter, denying many unused system calls.
- Turn `chroot` and `pivot_root` into no-ops rather than denying them with
  `EACCES` for compatibility with daemon which change root into an empty
  directory once all path access is done.
- Include `O_PATH` open requests into access check for stat sandboxing.

# 3.11.4

- Fix a regression caused by the recent `AT_EMPTY_PATH` path fix.
  Notably this makes fstating special files such as pipes and sockets
  work correctly again under syd.

# 3.11.3

- Vendor in the caps crate and avoid using **thiserror** which breaks static
  linking.

# 3.11.2

- Do not resolve symbolic links with `AT_EMPTY_PATH` flag in newfstatat
  and statx handlers. Notably, this fixes tar unpacking symbolic links
  pointing outside tar root.
- Improve path exists check in mkdir, mknod handlers by using access
  instead of stat.
- Fix a symlink TOCTOU in symlink and symlinkat handlers.
- Fix a symlink TOCTOU in open handler.
- Do not prevent access to device special files in open handler. Use
  `bind` with `nodev` for a secure alternative.
- Add sample ntpd profile.
- Drop the `is-terminal` crate dependency.
- Fix an issue with stat handler which caused it to fail with a
  permission error when called with `AT_EMPTY_PATH` in a user namespace
  (but _not_ in a pid namespace), with /proc mounted with the option
  `hidepid=2`.
- Mount private procfs with `hidepid=2` for additional hardening.
- Keep capabilities through user namespaces, this makes `unshare/user:1`
  functionally identical to `unshare -U --keep-caps`.
- Use binary system rather than decimal when parsing human-formatted sizes.
- New tool `syd-mem` to calculate the memory usage of a process.
- Do not drop `CAP_SYS_PTRACE` capability on startup as it is necessary
  to call the system calls `process_vm_readv` and `process_vm_writev`.
- Drop `CAP_CHECKPOINT_RESTORE` capability on startup.
- Drop `CAP_IPC_OWNER` capability on startup.
- Drop `CAP_SYS_TTY_CONFIG` capability on startup.
- Start using the `caps` crate to interact with Linux capabilities.
- New tool `syd-stat` to print detailed statistics about a process in JSON.
- unshare: call `PR_SET_KEEPCAPS` after clone.
- Set `trace/allow_unsafe_socket_families:1` **paludis** and **user**
  profiles.

# 3.11.1

- New sandbox command `trace/allow_unsafe_socket_families` to keep the
  `CAP_NET_RAW` capability. Useful to allow `ping` in the sandbox.
- New sandbox command `trace/allow_unsafe_adjtime` to keep the
  `CAP_SYS_TIME` capability and allow the system calls `adjtimex` and
  `clock_adjtime`. This is mostly useful when sandboxing an ntp daemon.
- `-e var=` may be used to pass-through an unsafe environment variable.
- Clear unsafe environment variables, may be disabled with
  `trace/allow_unsafe_environment:1` on startup.
- New tool `syd-run` to run a program inside a syd container.
- `syd-ldd` now uses the `immutable` profile rather than the `container`
  profile.
- Fix `unshare/pid:1` to properly imply `unshare/mount:1`.
- New tool `syd-tty` to print the controlling terminal of the given PID
  or the current process.
- Simplify symlink loop detection in path canonicalizer.
- Fix a panic in bind config code path.
- Do not send logs to syslog for `-x`.
- Parse user profile on `-f` for login shell compat.

# 3.11.0

- `-f` argument has been renamed to `-P`. `-f` is now ignored for login
  shell compatibility.

# 3.10.2

- Fix a regression with user profile parsing for the login shell.

# 3.10.1

- Clean up temporary tmp and shm directories at exit.
- New sandbox command `trace/private_shm` to mount private /dev/shm in
  the new mount namespace.
- Fix a regular expression issue in **syd-err**, and **syd-sys** helpers.

# 3.10.0

- New sandbox command `trace/private_tmp` to mount private /tmp in the
  new mount namespace.
- Add new profile **immutable** to create immutable containers.
- Command line option `-C` has been renamed to `-f`.
- Simplify command line option parsing and avoid double parsing to
  prioritize CLI options when the user profile is parsed.
- `allowlist/` and `denylist/` prefixes on sandbox commands have been
  changed to `allow/` and `deny/` respectively.
- Move auxiliary functionality into separate binaries:
    - syd-cat profile-name
    - syd-chk
    - syd-err number|name-regex
    - syd-exec
    - syd-lock
    - syd-log
    - syd-ls allow|deny|hook|ioctl|prctl
    - syd-norm path
    - syd-read path
    - syd-size size|human-size
    - syd-sys [-a list|native|x86|x86_64|aarch64...] number|name-regex
- The short form `-v` has been renamed to `-V` for consistency.
- Fix default arg0 for the login shell.
- `SYD_SH` now defaults to `/bin/sh` rather than `/bin/bash`.
- The environment variable `SYD_UNSHARE_MOUNT` is no longer honoured.
- The environment variable `SYD_UNSHARE_UTS` is no longer honoured.
- The environment variable `SYD_UNSHARE_IPC` is no longer honoured.
- The environment variable `SYD_UNSHARE_USER` is no longer honoured.
- The environment variable `SYD_UNSHARE_PID` is no longer honoured.
- The environment variable `SYD_UNSHARE_NET` is no longer honoured.
- The environment variable `SYD_UNSHARE_CGROUP` is no longer honoured.
- `--domainname` has been dropped in favour of the new sandbox command
  `name/domain`.
- `--hostname` has been dropped in favour of the new sandbox command
  `name/host`.
- `--unshare-mount` has been dropped in favour of the sandbox command
  `unshare/mount`.
- `--unshare-uts` has been dropped in favour of the sandbox command
  `unshare/uts`.
- `--unshare-ipc` has been dropped in favour of the sandbox command
  `unshare/ipc`.
- `--unshare-user` has been dropped in favour of the sandbox command
  `unshare/user`.
- `--unshare-pid` has been dropped in favour of the sandbox command
  `unshare/pid`.
- `--unshare-net` has been dropped in favour of the sandbox command
  `unshare/net`.
- `--unshare-cgroup` has been dropped in favour of the sandbox command
  `unshare/cgroup`.
- The long version `--config` has been removed, use `-C`.
- The long version `--magic` has been removed, use `-m`.
- The long version `--arg0` has been removed, `-A` has been renamed to
  `-a`.
- The long version `--env` has been removed, `-E` has been renamed to
  `-e`.
- The `--lock` option has been removed, use the sandbox command
  `lock:on` instead.
- The environment variable `SYD_FAKEROOT` is no longer honoured.
- Change `--root` command line option to `root/fake` sandbox command.
- Change `--map-root` command line option to `root/map` sandbox command.
- Implement `root` sandbox command to change root directory before
  starting the process.
- Implement the `bind` sandbox command to recursively bind mount
  directories on startup.
- Upgrade `smallvec` crate from `1.11` to `1.13`.
- Upgrade `env_logger` crate from `0.10` to `0.11`.
- Drop `trace/allow_unsafe_getrandom` command and make `getrandom`
  system call part of read sandboxing. `getrandom` with the flag
  `GRND_RANDOM` is treated equivalent to calling open on
  `/dev/random`, and without this flag is treated as open on
  `/dev/urandom`.
- Drop the setuid/setgid `chmod` restrictions and remove the sandbox
  command `trace/allow_unsafe_chmod`. Since syd always calls
  `PR_SET_NO_NEW_PRIVS` this restriction did not provide any added
  security and caused issues with e.g. sticky bits on directories.
- `-E` uses the lib profile rather than the Paludis profile now.
- Allow comma delimited list for read, write, exec, stat as capability
  in allow/denylists and filters, see [Command Shortcuts](#command-shortcuts)
  for more information.
- Implement initial trace aka "dry run" mode, activated with `-x`.

# 3.9.14

- Build release binaries with the log feature.
- Add `SYD_LOG_FD` environment variable to override log file descriptor.
- Add --domainname option to set NIS/YP domain name in the UTS namespace.
- Add --hostname option to set host name in the UTS namespace.
- Drop the broken `--chroot` option.
- Add command line option -E to set/unset environment variables.
- Implement sandbox command `trace/deny_tsc` to disable reading timestamp
  counter on x86.

# 3.9.13

- Fix regression causing syd not being able to locate the login shell.
- No longer use nightly rust and `-Zbuild-std` when building release binaries.

# 3.9.12

- Drop `allowlist/lock/write+/dev/std{in,err,out}` from landlock and user
  profiles. This caused landlock to fail on user profile when running
  without a TTY.
- Do not respect `HOME` environment variable when figuring out user home
  directory for added security when used as a login shell.
- Fix user profile parsing on `-c` and `-l`.
- Fix regression causing make not to work under syd due to the
  `setresuid` system call getting denied with the wrong errno.
- Use nightly rust and `-Zbuild-std` when building release binaries.

# 3.9.11

- Fix `--chroot` to work with `--unshare-user` correctly again,
  this was broken by the user subnamespace safety restriction.

# 3.9.10

- When `unshare/user` is active, enter into a sub usernamespace
  after setting the `user.max_user_namespaces` to 1 to ensure
  sandbox process can not do further namespace modification.
  This is similar to bubblewrap's `--disable-userns` option.
- Respect the value of `--arg0` when spawning a login shell.
- Respect `HOME` environment variable when figuring out user home
  directory.
- The user profile is parsed early for login shells now such that
  overriding the configuration using command line parameters is
  possible.
- Fix undefined behaviour when forking into the new pid namespace
  with `unshare/pid:1`.

# 3.9.9

- Errors on disabling of coredumps is no longer fatal.
- Drop the experimental init daemon `syd-init`.
- Relax signal protection such that sending signal 0 (ie check for
  existence) to syd threads are permitted.
- Allowlist `/proc/sys/{fs,kernel,vm}` directories recursively for read
  and stat sandboxing in **paludis** and **user** profiles.
- Fix ioctl allowlisting failing on musl builds.
- Fix an issue with allowlisting TTY devices in **paludis** and **user**
  profiles.

# 3.9.8

- syd now registers itself as a child subreaper unless it is already
  pid1. This fixes issues with background processes getting reparented
  to the actual pid1 after which it is going to require ptrace rights
  to read /proc/pid/mem. With this change, syd works fine as a
  regular user with the sysctl `yama.ptrace_scope` set to 1.

# 3.9.7

- Set CPU scheduling priority to idle for syscall handler threads.
- syd no longer sandboxes **ftruncate** as it is impossible
  to call without bypassing `open()`.

# 3.9.6

- syd now by default disable setting process resource limits for
  sandbox process. Moreover syd also disables coredumps for the
  sandbox process. This may be disabled on startup with the sandbox
  command `trace/allow_unsafe_prlimit:1`.
- Set `SIGPIPE` to defalt earlier so `--syscall`, `--errno` etc. can
  benefit from it.

# 3.9.5

- Add new sandbox command `mem/kill` which may be set to true to kill
  offending processes in Memory sandboxing.

# 3.9.4

- Add new sandbox command `pid/kill` which may be set to true to kill
  offending processes in PID sandboxing.
- Remove the background interrupt handler thread which is not necessary
  with the `WAIT_KILLABLE_RECV` flag.
- Optimize pid sandboxing such that it's much more resillient
  against rapid PID starvation.
- Enable `unshare/net:1` in **container** profile. Now that syd
  brings the loopback interface up, this is actually useful.

# 3.9.3

- Drop the interrupt workaround in the bind handler which is no longer
  necessary.
- Do not check target argument of **symlink**, and **symlinkat** system
  calls. This is consistent with the original system calls.
- Fix **fchmodat** and **faccessat** handlers failing on proc fd links.
- Use OwnedFd more widely to ensure no FDs are leaked.

# 3.9.2

- Mention Sandboxing Emacs with syd asciicast in README.
- Preserve child pid information on **reset** sandbox command.
- Fix case insensitive matching for `--syscall` and `--errno`.
- Implement -R, --chroot=root to change root directory.
- Allowlist `/sbin` for **landlock** profile.
- Allowlist `/sbin` for **paludis** profile.

# 3.9.1

- Make open handler handle `/dev/syd` when both the sandbox lock and
  read sandboxing is off.
- Make getdents handler list `/dev/syd` when both the sandbox lock and
  stat sandboxing is off.
- Fix a segfault on musl during reading `/dev/syd`.

# 3.9.0

- New profile **lib**, the LibSyd helper profile, turns all sandboxing off.
  Useful to configure syd in the application using LibSyd.
- Upgrade **regex** crate from `1.9` to `1.10`.
- Upgrade **once\_cell** crate from `1.18` to `1.19`.
- Upgrade **nonempty** crate from `0.8` to `0.9`.
- The `access`, `faccessat`, and `faccessat2` are handled as part of **stat**
  sandboxing now. Previously the type of sandboxing depended on the access flags
  `R_OK`, `W_OK`, and `X_OK`.
- new tool **syd-ldd** which is a safe `ldd(1)` wrapper.
- use **smallvec** crate to efficiently handle path operations.
- use **itoa** crate to efficiently convert integers to paths (take 2).

# 3.8.9

- Return `EACCES` rather than `ENOSYS` on block device access.
- Use **itoa** crate to efficiently convert pids and file descriptors to
  paths.
- Avoid canonicalizing the current working directory on network calls
  with UNIX domain sockets. Fixes `ENAMETOOLONG` error in some cases.
- Optimize prioritization of syscall handler threads such that
  it only runs once on thread start.

# 3.8.8

- Make the **sync** and **syncfs** calls no-op under syd for added safety.
- Make the **paludis** profile stricter by refining access to `/`.

# 3.8.7

- Further restrict the parent syd process by disallowing **ptrace**,
  **chroot**, and **pivot\_root**.
- syd now brings the loopback interface up with `unshare/net:1`.
- Implement the **load** sandbox command.
- Implement the **panic** sandbox command.
- Implement the **reset** sandbox command.
- Remove the is-terminal check from error, warn level logs.

# 3.8.6

- Set i/o priority of system call handler threads to idle.
- Set parent-death signal to SIGKILL in syd process rather than the
  child for added safety and security.
- Drop Linux capabilities in syd process rather than the child for
  added security.
- Fix unicode issue in regex builder for `--error` and `--syscall` options.
- Reduce the default threadpool keepalive timeout from one minute to 15
  seconds so that syd becomes more reliable during pid starvation.
- Apply a seccomp filter to the syd process such that all set*id
  system calls return 0 without doing anything. This is an important
  security hardening.

# 3.8.5

- Enable debugging information in release mode to help with profiling.
- Use optimization level `3` rather than `z` in release mode.
- Use `unwind` rather than `abort` for panics in release mode.

# 3.8.4

- Implement virtual memory usage limiting for memory sandboxing, the
  command `mem/vm_max` may be used to configure the limit which defaults
  to 4GB.
- Exit with eldest process by default, add `trace/exit_wait_all` sandbox
  command to change behaviour.

# 3.8.3

- Optimize smaps lookup by stopping at the point the memory limit is reached.

# 3.8.2

- Implement `syd --parse human-size` to parse human-formatted size
  strings into bytes.
- Implement [Memory Sandboxing](#memory-sandboxing).

# 3.8.1

- Include build host information into `--version` output.
- Ignore `EACCES` and `ESRCH` errors in proc task counter, we already ignore
  `ENOENT` and `EPERM` so this is consistent.
- Slightly optimize the task limit check of PID sandboxing.
- Remove the broken **kill** mode for PID sandboxing and rename **deny** to
  **on**.
- Set system call handler threads' nice value to 19 to help prevent CPU
  starvation.

# 3.8.0

- Add new operator `^` to remove all matching elements from an allowlist,
  denylist or a filter.
- New sandboxing type called [PID sandboxing](#pid-sandboxing) to set a limit on
  the maximum number of tasks. This is best coupled with a pid name space.
- Guard the parent process with a tight seccomp filter when using namespaces.
- Use the `sendfd` crate and a safe `UnixStream` based socketpair implementation
  to send/receive the seccomp notification fd rather than the non-portable
  internal alternative.
- Avoid loading landlock twice on startup when namespaces are at play.
- `--arch <name>` may now be specified with `--syscall num|regex` to lookup the
  system call for the specified architecture rather than the native
  architecture. `--arch list` may be used to print the list of supported
  architectures.
- Denylist `/proc/1/***` for read, stat and write sandboxing in **container**
  and user profiles.

# 3.7.3

- Fix build on musl broken by recent 32-bit compat changes

# 3.7.2

- Write a socketcall hook for 32-bit systems.
- Optimize seccomp request preparation slightly by avoiding an ioctl call per
  request.
- Fix 32-bit build
- Allowlist the system call `mmap2` and `ugetrlimit` system calls.
- Fix an issue determining the syscall handler for non-native architectures
  (e.g. 32bit sandbox process with 64bit syd)

# 3.7.1

- Make the busy-wait in the background monitor thread less heavy by inserting a
  wait after each request reap cycle.
- Optimize pidfd handling.
- Optimize the `syd::fs::FileInformation::from_path` function which is used very
  frequently in path canonicalization.

# 3.7.0

- Increase the threadpool keepalive timeout from 7 seconds to a minute.
  Benchmarks have shown 7 seconds is actually too short and we're overloading
  the threadpool.
- Make the background monitor thread wait on a `Condvar` rather than waking up
  every n seconds and looping through the whole list of requests. The handler
  thread notifies the `Condvar` which wakes up the background monitor thread to
  handle interrupts for blocking system calls (e.g. interrupted open on a FIFO)
- Improve seccomp syscall priorities to better match a typical build process.
- Protect syd process and their threads from signals. Hook `kill`, `tkill`,
  `tgkill`, and `pidfd_open` calls and return **EACCES** in case sandbox process
  tries to send a signal to a process id related to syd.

# 3.6.6

- Avoid waiting for threads in Supervisor::wait avoiding hangs in some cases.

# 3.6.5

- New profile **container** to activate Linux namespaces. This is currently
  equivalent to `--unshare-mount,uts,ipc,user,pid,net,cgroup`.

# 3.6.4

- Exit with 128 plus signal value rather than **EFAULT** when the sandbox
  process is killed by a signal.
- syd process is included into the namespace now so that it has identical
  view of /proc.
- Mount /proc inside the mount namespace as necessary.
- Return proper exit codes on early spawn failures.
- Allowlist the directory `/sys/devices/system/node` recursively for read & stat
  sandboxing in **paludis** profile.

# 3.6.3

- Fix an issue with symbolic loop detection in path canonicalizer and make it
  more robust. **Milestone** Paludis' tests pass under syd now.
- Ensure seccomp sender and receiver socketpair is closed properly which avoids
  hangs when there is an error spawning the sandbox process.

# 3.6.2

- New `landlock` profile to make practical use of LandLock.
- Drop the interrupt workaround for kernel misbehaving with
  `WAIT_KILLABLE_RECV` seccomp flag.
- Stat handler incorrectly returned a directory when the sandbox process stats
  one of the magic symlinks `/proc/self`, `/proc/thread-self`, `/dev/fd`,
  `/dev/stdin`, `/dev/stderr` and `/dev/stdout`. This is now fixed. Notably,
  this makes `ps` work under syd.
- Report running kernel version and landlock status in `--version`
- Add `--landlock` which checks if LandLock ABI v3 is fully supported.

# 3.6.1

- The `-` op on magic commands now removes the most recently added matching item
  rather than all matching items for predictability.
- Fix `esyd disable` subcommand.
- Allowlist /dev/stdin for landlock read/write in user profile. /dev/stdout and stderr
  were already allowed.

# 3.6.0

- Stat sandboxing can no longer be bypassed by attempting to read, write or
  execute a denylisted/hidden path.
- Log messages with process IDs are enriched using `/proc/pid/comm` rather than
  `/proc/pid/cwd` and `/proc/pid/cmdline` when the **log** feature is disabled
  (default). This is much lightweight since it avoids filesystem access.
- Implemented various small usability improvements for `syd-test`.
- Ioctl restrictions was not applied correctly when syd was built with musl.
  This is now fixed.
- New feature `log` to include debug logging into the program. By default
  logs of severity debug and trace are compiled out. This was previously
  dependent on debug build mode.
- `esyd enable`, `enabled`, `enable_path`, `enabled_path`, `disable`,
  `disabled`, `disable_path`, and `disabled_path` now works for read, write and
  stat sandboxing rather than just write sandboxing. use the `_write` suffixed
  versions of the subcommands for write-only.
- `esyd deny`, `deny_path`, `nodeny`, and `nodeny_path` now works for read,
  write and stat sandboxing rather than just write sandboxing, use `esyd
  deny_write`, `nodeny_write` to add/remove from the write-only denylist.
- `esyd allow`, `allow_path`, `disallow` and `disallow_path` now works for read,
  write and stat sandboxing rather than just write sandboxing, use `esyd
  allow_write`, `disallow_write` to add/remove from write-only allowlist.
- Allowlist the directory `/proc/sys/vm` for read & stat sandboxing in
  **paludis** and **user** profiles.
- Allowlist files with CPU information under `/sys/devices/system/cpu`
  for read & stat sandboxing in **paludis** profile.
- Allowlist the directory `/proc/pid/attr` for read & stat sandboxing in
  **paludis** and **user** profiles.
- Reduce the severity of sandbox config change logs from **warn** to **info**.
- `sandbox/stat`, aka Stat Sandboxing, defaults to **on** rather than **off**
  now.
- `sandbox/read`, aka Read Sandboxing, defaults to **on** rather than **off**
  now.
- `sandbox/exec`, aka Exec Sandboxing, defaults to **on** rather than **off**
  now.
- `trace/allow_unsupported_socket_families` defaults to **false** rather than
  **true** now.
- `trace/allow_successful_bind` default to **false** rather than **true** now.
- Mention asciicasts in README.

# 3.5.2

- Fix various issues with /proc handling of stat and open handlers.
- Support Linux-specific statx flags in statx handler.

# 3.5.1

- Make mkdir, mkdirat, mknod and mknodat handlers more resillient to interrupts.
- Make connect handler more resillient to interrupts.

# 3.5.0

- Make expensive tests usable (preparation for `src_test_expensive` on Exherbo).
- Rename **local** alias to **local4**, define the new **local** alias an union
  of **local{4,6}**.
- Rename **any** alias to **any4**, define the new **any** alias as an union of
  **any{4,6}**.
- Rename **loopback** alias to **loopback4**, define the new **loopback** alias
  as an union of **loopback{4,6}**.
- Add **linklocal**, **linklocal4**, and **linklocal6** network aliases.
- Network aliases are now case-insensitive.
- Support Plan9 style network addresses such as `1.1.1.1!80`. This is the format
  we're going to use moving forward. `@` is still supported as a split character
  for backwards compatibility.
- Make bind handler more resillient to interrupts.

# 3.4.3

- Fix **allowlist/net/bind-**, **allowlist/net/connect-**,
  **denylist/net/bind-**, **denylist/net/connect-** sandbox commands to
  correctly remove the address when the port is given as a single port rather
  than a port range.
- Fix a bug with seccomp request tracking of the background syd::m☮☮n thread
  causing spurious signals to be sent to system call handler threads.

# 3.4.2

- Start making binary releases

# 3.4.1

- Replace `threadpool` crate with the `rusty_poll` crate

# 3.4.0

- Teach syd::m☮☮n thread the ability to resize the syscall handler threadpool
  size upon investigating the current active, queued and maximum count of
  the threadpool. This makes syd automatically adapt when there's a sudden
  burst of blocking system calls (e.g. opening a FIFO, or binding a socket)
  and avoid deadlocks. When the burst is gone, syd::m☮☮n kicks in again and
  decreases the pool size back to a normal state. Since the handling is
  completely automatic, the environment variable `SYD_NPROC` to set the size of
  the system call handler thread pool is no longer supported. The value defaults
  to the number of CPUs on startup and is adapted automatically according to the
  needs of the sandbox process.
- Fix various issues with UNIX domain socket handling.
- Honour process umask properly in bind handler.
- Make the bind syscall handler much more resillient to quickly restarting
  interrupted syscalls.
- Improve interrupt handling by spawning a background thread called syd::m☮☮n,
  to reap invalidated seccomp requests and interrupt respective syscall handler
  threads.

# 3.3.4

- Fix a bug in symlink loop handling of path canonicalization and make it more
  efficient.
- Simplify FIFO handling using a thread rather than forking. Credit goes to
  **Johannes Nixdorf** for coming up with the idea and testing a POC.

# 3.3.3

- Fix handling of unix domain socket connections with relative paths.
- Drop the umask lock and support input/output to FIFOs.

# 3.3.2

- Handle the virtual paths **/dev/stdin**, **/dev/stdout**, and **/dev/stderr**
  specially during syscall emulation.
- Fix fgetxattr handler to correctly determine the path to the file descriptor.
- Fix an issue with fgetxattr handler where the handler would erroneously return
  EFAULT on some valid fgetxattr calls.
- Fix an issue emulating newfstatat calls with `AT_EMPTH_PATH` flag.

# 3.3.1

- Fix another bug with ends with dot check in path canonicalizer which
  caused some paths to erroneously return ENOENT rather than EEXIST.
- Fix the ends with dot check in path canonicalizer which caused
  creating/removing directories with a dot in the name fail with EINVAL.
- Improve handling of the special paths `/dev/fd/$fd` and `/proc/$pid/fd/$fd`.
- Improve path canonicalizer by avoiding double stat on symlinks.
- Allow **TIOCSCTTY** ioctl by default.
- Rather than disallowing access to `/dev/tty` with **ENXIO** unconditionally,
  try to determine sandbox process' controlling terminal and use it.
- New command `syd-init` which is a simple init system to run under syd.
- Switch fuzzer to use afl++ rather than libfuzzer
- Document **-c** and **-l** options correctly. Ignore **--login** as well for
  login shell compatibility.
- Add a CTF guide section in the README

# 3.3.0

- `-r` short option of `--root` has been removed for consistency.
- `-l` option is a no-op now rather than being a short option for `--lock` for
  login shell compatibility.
- `-c` short option has been changed to `-C` for **--config**. **-c** causes
  command to be executed under a shell for login shell compatibility

# 3.2.11

- Announce the CTF game in the README.
- Move the system calls **getxattr**, **lgetxattr**, **fgetxattr**,
  **listxattr**, **flistxattr**, and **llistxattr** from read sandboxing to stat
  sandboxing for consistency with **stat** calls.
- Do not replace `/proc/self` with `/proc/pid` on stat with nofollow. This fixes
  `ps` to work under syd above all.

# 3.2.10

- `syd --read` now works with relative paths as well as absolute paths.
- New profile `silent` to silence all access violations.
- Fix a bug with path normalization where double dots at root position were
  erroneously removed resulting in path not found errors during syscall
  handling.

# 3.2.9

- Drop trailing slash from paths before matching.
- Update bpftrace scripts
- Fix /dev/pts glob in `paludis` and `user` profiles.

# 3.2.8

- Disallow access to `/dev/tty` with `ENXIO` as syd cannot safely emulate
  access to the controlling terminal.
- Implement `syd --syscall number|name-regex` to search for syscall numbers and
  names.
- Fix stat handler from erroneously returning ELOOP on symbolic links with a
  trailing slash.
- Fix a bug with symbolic link loop detection in remote path canonicalization.
- Properly exit with EBUSY when seccomp filter cannot be loaded on startup.
- Print libsecc☮mp version, api version and native architecture in `syd --help`
  output.
- Print libsecc☮mp native architecture in `syd --version` output.
- Implement `syd --arch` to print the name of the native libsecc☮mp
  architecture.
- Implement `syd --errno number|name-regex` to search for errno numbers and
  names.

# 3.2.7

- Move esyd.sh from data/ to src/ as another attempt to fix `cargo install`.
- Use openat2 with `RESOLVE_NO_SYMLINKS` when stating in fs::canonicalize
  function removing another potential TOCTOU vector.

# 3.2.6

- Do not call `include_str!` with a relative path which breaks `cargo install`.
  Use cargo build environment variables instead.
- Always deny access violations with EACCES. Previously syd would deny
  silently with ENOENT if the path does not exist. This was a feature to ease
  test/dev cycle in early stages of syd-3 but it creates confusion, so it is now
  removed.

# 3.2.5

- Fix a file descriptor leak in stat handler. Credit goes to **Johannes
  Nixdorf** for identifying the bug.
- Report libsecc☮mp API in `syd --version`
- `syd-test` now lists known failures at the end of the test run.
- Ensure restarted open system calls with `O_EXCL` flags succeed. With this fix
  `git clone` works under syd.
- Fix parsing of LOCAL and LOCAL6 network aliases.

# 3.2.4

- Fix tests

# 3.2.3

- Ensure opening directories in write mode fails with EISDIR in open handler.
- Deny mknod for fifos and block devices with ENOSYS rather than ENOPERM
  correctly signaling the sandbox process the lack of support for named pipes.
- Do not follow symbolic links in chmod handler.
- Preserve `O_CLOEXEC` flag as necessary in the added fd for open system call
  handlers.
- Ensure system call emulators fail with ENOTDIR when fd argument is a regular
  file and the path argument is a dot.
- Avoid updating file access times during remote path canonicalization which may
  break expectations of sandbox processes.
- open handlers now return ENOENT when the path argument is an empty string.
- unlink, unlinkat, rename, renameat, and renameat2 handlers now return EINVAL
  when the last path of the component is a dot.
- Fix a regression in recvfrom remote socket address writing. This caused UDP
  connections, such as DNS to fail under syd.
- Handle task death between seccomp notify poll event receive and seccomp
  request receive gracefully.

# 3.2.2

- Add statistics about the file in reports for path access violations.
- Access violation returns EACCES if file exists and the errno if the file does
  not exist. Previously it would always return ENOENT in the latter case.
- Do not follow symbolic links in mkdir and mkdirat handlers.
- Lift chmod and getrandom restrictions for the paludis profile.
- `trace/allow_unsafe_getrandom` sandbox command may be used to lift getrandom
  restrictions and allow the use of `GRND_RANDOM` flag with getrandom which
  accesses `/dev/random` under the hood.
- `trace/allow_unsafe_chmod` sandbox command may be used to lift chmod
  restrictions and allow the creation of setuid/setgid files.
- Return correct errno on open errors due to remote path canonicalization
  failures.
- System call handlers properly return EBADF on invalid fd arguments now.
- Fix symbolic link handling in open syscall handlers.
- Fix symlink loop detection in remote path canonicalization.
- We issue continue syscall for connection-mode sockets in recvfrom/sendto
  system calls. Since the pointer argument is NULL in these cases we're safe
  from TOCTOU.
- Do not follow symbolic links in rename, renameat, and renameat2 handlers.
- Return correct errno on failures from statx and newfstatat handlers.
- Use original target argument in symlink, symlinkat handlers so that creation
  of relative symbolic links is now possible under syd.
- Honor sandbox process umask in link and linkat system calls.
- Honor sandbox process umask when creating UNIX sockets.
- Honor sandbox process umask in mkdir, mkdirat, mknod, and mknodat syscall handlers.
- Trailing slash handling has been improved across all system call handlers.
- link, and linkat handlers no longer follow symbolic links in newpath as
  mandated by POSIX.
- linkat now honours `AT_SYMLINK_FOLLOW` correctly when following symlinks.
- link no longer follows symbolic links on its first argument as it should.
- open, and openat with `O_CREAT` now properly returns ENOENT on paths ending
  with a trailing slash.
- Handle mkdir, mkdirat, rmdir, and unlinkat correctly and return EINVAL when
  the last component is a dot.
- Fix a path canonicalization bug to follow symbolic links in the last component
  in case the component ends with a slash, ie if it has to be a directory.
- Simplify stat handling.
- Various fixes for xattr related system call handlers, above all handle value
  argument being NULL gracefully.
- Avoid resolving target path in **symlink** and **symlinkat** emulators.

# 3.2.1

- Fix handling of `lchown{,32}` emulators where we mistakenly followed symbolic
  links before.
- Use use a fd with `O_PATH+RESOLVE_NO_SYMLINKS` during syscall emulation for
  safety against symlink attacks, we hard require Linux-5.6 or newer with this.
- Sandbox **ftruncate**, **fgetxattr** and **lgetxattr**.
- Call renameat2 directly as a syscall as musl libc is lacking this function at
  the moment and their usage breaks musl builds.

# 3.2.0

- Numerous minor fixes to path normalization and canonicalization.
- Emulate all sandboxing calls but **exec**, and **chdir**.
- Handle symbolic links and the `AT_SYMLINK_NOFOLLOW` flag correctly.
- Handle empty paths and the `AT_EMPTY_PATH` flag correctly in system calls.
- `trace/allow_successful_bind` is now fixed to correctly allow successful bind
  calls.
- syd now emulates all the respective system calls for network sandboxing
  **making network sandboxing completely TOCTOU-free.**
- syd no longer allows the opening of existing device special files or named pipes.
- syd no longer allows the creation of device special files or named pipes.

# 3.1.11

- Fix an issue with network address filtering causing some filters to match
  regardless of their port restrictions.
- Fix an issue with network address matching causing some rules to match
  regardless of their port restrictions.

# 3.1.10

- Add sample user configuration file under `data/user.syd-3`.
- Use `/etc/user.syd-3` rather than `/etc/rc.syd-3` which is more consistent.
- syd now properly spawns the underlying shell as a login shell when syd
  itself is invoked as a login shell.
- Add sandbox commands **unshare/{mount,uts,ipc,user,pid,net,cgroup}** which are
  equivalent to the command line options
  `--unshare-{mount,uts,ipc,user,pid,net,cgroup}`. In addition they may be
  queried using the stat interface during runtime, e.g. `test -e
  /dev/syd/unshare/user?'
- Implement `trace/allow_unsafe_{io,pr}ctl` sandbox commands which may be
  used to lift the restrictions on the respective system calls.
- The function `syd::proc::proc_cmdline` now trims overly long command lines.
- Simplify capabilities handling. Drop `CAP_BPF`.

# 3.1.9

- The lock is honoured during initial configuration updates so e.g.
  setting the sandbox lock in the file `/etc/rc.syd-3` will prevent
  `~/.user.syd-3` from loading. This is useful to enforce site-wide
  configuration.
- **user** profile now parser `/etc/rc.syd-3` before `~/.user.syd-3`.
- syd now honours the environment variables
  `SYD_UNSHARE_{MOUNT,UTS,IPC,USER,PID,NET,CGROUP}` to create namespaces.
- You may now use syd as your login shell by adding it to `/etc/shells`. The
  actual shell to execute under syd defaults to `/bin/bash` and can be
  changed on runtime via `SYD_SHELL` environment variable or during compile time
  by changing the variable `SYD_SH` in `src/config.rs`.
- Fix a bug with path normalization to handle double dots at root position
  correctly.
- The set-id family calls are now no-ops under syd.
- The `/dev/syd` may be read to get syd state in JSON in case sandbox is
  unlocked.
- Better ZSH compatibility for the `data/esyd.sh` script which is also available
  via `esyd --sh`.

# 3.1.8

- Fix linkat, renameat, and renameat2 system call handlers' argument handling.
- Fix dropping of capabilities with `--map-root`.
- Listing `/dev` now lists `/dev/syd` in case the sandbox lock is off.
- Simplify handling of the special paths `/proc/self` and `/dev/fd`.
- syd now properly returns `ENAMETOOLONG` for too long paths.
- Ensure the validity of the sandbox process is checked using
  `SECCOMP_IOCTL_NOTIF_ID_VALID` after every interaction with the sandbox
  process memory.
- syd now allows **ioctl** requests for **PTY** handling.
- syd now properly closes the seccomp notify file descriptor after poll
  errors.
- syd now sets the **no\_new\_privs** attribute for the syd process as
  well as the sandbox process. Previously we only set this in the child process.
- Fix a bug in path canonicalization function preventing an infinite loop,
  when following certain symbolic links.

# 3.1.7

- Vendor in the caps crate and avoid using **thiserror** which breaks static
  linking.

# 3.1.6

- Stop using the **thiserror** crate which breaks static linking.

# 3.1.5

- Stop using the `derive` feature of the **serde** crate which breaks static
  linking.

# 3.1.4

- Allow the system calls **setgid**, **setgriups**, **setregid**, **setresgid**,
  **setresuid**, **setreuid**, **setuid** inside the sandbox. Since we drop the
  capabilities `CAP_SETUID` and `CAP_SETGID` on startup this is safe.
- Vendor in the landlock create, use bitflags rather than enumflags2 which
  depends on emumflags2\_derive crate and that used to break both static linking
  and address sanitizer.
- Reading from files under `/dev/syd` succeeds with the lock off. This is to
  provide consistency with the stat interface. The open system call handler just
  opens `/dev/null` instead under the hood.
- Handle pipes under `/proc/pid/task/fd` directories correctly.
- `syd-test` now honours the **SYD\_TEST\_VALGRIND** environment variable to run
  syd under valgrind during integration tests.
- syd now logs the current user id with the log messages.
- The stack size of the syd execve child has been increased from 4k to 128k.
- Block **getrandom** calls with **GRND\_RANDOM** flag. Sandbox processes are
  not allowed to access **/dev/random**. Access to **/dev/urandom** is fine.
- Fix environment clearing code which fixes the broken functionality of
  `SYD_NO_SYSLOG` and `SYD_NO_CROSS_MEMORY_ATTACH` environment variables.
- The **stat** system call handler now properly handles symbolic links.
- **paludis** and **user** profiles allow access to files `/proc/version` and
  `/proc/pid/map`.
- Fix and document **ioctl**, **prctl** restrictions.
- syd now writes "deny" to `/proc/pid/setgroups` before writing the `gid_map`
  file. This way `setgroups(2)` is permanently disabled in user namespace and
  writing to the gid map file can succeed without having the `CAP_SETGID`
  capability.

# 3.1.3

- syd restricts prctl usage with a list of allowlisted prctls. This prevents
  potentially dangerous prctls such as **PR_SET_MM** which can create
  self-modifying executables. The list of allowlisted prctls can be listed using
  `syd --list prctl`.
- syd restricts ioctl usage with a list of allowlisted ioctls. This prevents
  sandbox escapes such as utilizing **TIOCSTI** to write to the controlling
  terminal. The list of allowlisted ioctls can be listed using `syd --list
  ioctl`.
- Use the errno **EACCES** rather than **EPERM** on access violations.
- **paludis** profile disables read access to `/dev/random`. stat access to this
  file is granted. Read access to `/dev/urandom` works too.

# 3.1.2

- The stat system call handler now handles deleted files correctly and fstats on
  the fd rathet than the dangling /proc symlink
- The stat system call handler now handles special files such as sockets or poll
  file descriptors correctly and fstats on the fd rather than the dangling
  /proc symbolic link.
- **paludis** and **user** profiles allow read/stat access to `/proc/stat` now
  so that `ps` works correctly in the sandbox.
- Add `--sh` option which makes syd drop a shell script to standard output
  which defines **esyd** the sandbbox helper.

# 3.1.1

- CGroups support has been dropped, use other means to create CGroups and then
  spawn syd inside.
- The *paludis* and *user* profiles now allow read/stat access to
  the files `/proc/sys/kernel/osrelease` and `/proc/uptime`.
- Fix a panic trying to log paths with non UTF-8 pathnames.

# 3.1.0

- The **stat** system call emulator no longer fails to fstat on pipes.
  The solution is **TOCTOU-free**, when we hit on a pipe fd, we get the
  file descriptor, fstat it and close it, then return the stat buffer.
- Add support for CGroups via `--limit-{cpu,io,mem,pid}`. The command-line
  arguments have conservative defaults. RTFM for more information.
- Disallow the system calls **bpf**, **ioperm**, **iopl**, **setgid**,
  **setgroups**, **setregid**, **setresgid**, **setresuid**, setreuid**, and
  **vhangup** inside the sandbox to improve security.
- Improve architecture-dependent code, improve support for ARM and S390.
- Edit **paludis** and **user** profiles to have a "deny-by-default and
  allowlist known goods" strategy for the directories `/dev` and `/proc`. This
  brings added safety as it adds read restrictions and hides many sensitive
  paths such as `/dev/kmem` or `/proc/pid/mem`.
- The **memfd_secret** system call is now allowed in the sandbox.
- The **act** and **syslog** system calls are no longer allowed in the sandbox.
- syd drops some capabilities on startup which provides added safety to the
  sandbox. The list of dropped capabilities are listed under
  [Security](#security).
- Implement **--map-root** command line flag to map current user to root in the
  sandbox. This implies **--unshare-user**.
- Fix the prevention of  **setuid**/**setgid** files to be created in the
  sandbox.

# 3.0.16

- syd now allows the system calls **setdomainname**, **sethostname**,
  **syslog**, and **signalfd4** system calls inside the sandbox.
- The **stat** family system calls are no fully emulated and do not suffer from
  **TOCTOU** issues.
- syd no longer allows the `TIOCSTI` **ioctl** call which can be used to
  write to the controlling terminal for added security.
- When syd is invoked with `--unshare-user` option to create a new user
  namespace, the creation of new user namespaces inside the sandbox is no longer
  allowed for added security.
- syd now allows the system calls **pidfd\_open** and **unshare**.
- syd no longer allows the system calls **mbind**, **migrate\_pages**,
  **move\_pages**, **perf\_event\_open**, **set\_mempolicy**, and
  **userfaultfd** inside the sandbox for added security.
- syd no longer allows setuid/setgid files to be created inside the sandbox.
- **fchmod**, and **fchown** system calls are now sandboxed.

# 3.0.15

- Turn on the [empty
  alternates](https://docs.rs/globset/latest/globset/struct.GlobBuilder.html#method.empty_alternates)
  building Globs such that `foo{,txt}` in a pattern will match both `foo` and
  `foo.txt`.
- Take advantage of **globset** crate's ability to match a set of patterns at
  once. This way regardless of how many rules are present in a glob pattern
  list, such as allowlist/read, denylist/stat, syd does a single pattern
  match during access control. This increase performance considerably,
  especially for very long rulesets.
- replace **glob-match** crate with **globset** crate. **globset** can work
  directly on `Path`s and requires no `String` conversion.
- Use `Path`/`PathBuf` rather than `&str`/`String` in many places where we
  handle path names. This ensures path names with invalid UTF-8 in their names
  are handled correctly.

# 3.0.14

- syd now uses Landlock ABI version 3 rather than version 1. A Linux kernel
  running version 6.2 or newer is required to get the most out of it. However
  older versions also work quite well. See [this
  table](https://man.archlinux.org/man/landlock.7.en#VERSIONS) for an overview
  on Landlock features and the corresponding kernel version when they were
  implemented.

# 3.0.13

- **esyd check** now utilizes `syd --check` rather than stating the file
  `/dev/syd`. This way it can still detect if the process is running under
  syd despite the sandbox lock being on.
- **esyd exec** subcommand has been fixed.
- The **user** profile added `/dev/tty` to the list of read-write allowed paths
  for LandLock sandboxing.
- The **user** profile now allows read access to **/var/log/journal** for
  systemd journal access.
- **esyd dump** subcommand now forwards it command line arguments and pipes its
  output to **jq** if it's available.
- **Security**: Start emulating **creat** system call which prevents the
  `TOCTOU` scenario where an attacker can create a denylisted file by
  editing the dereferenced pointer argument after the access control but
  before the system call actually happens. We have an integration test,
  called **ptrmod_toctou_creat** which confirms the fix.
- The **esyd** helper saw some fixes, fixing `deny*` subcommands.

# 3.0.12

- syd now logs sandbox command attempts so as to better couple with **esyd**.
- Many improvements, fixes and documentation for the **esyd** helper.

# 3.0.11

- Added new network aliases `ANY` and `ANY6` which match the whole Ipv4 and Ipv6
  address spaces respectively.
- **Security**: Add `NULL` guards to all system call hooks which prevents
  potential crashes if one of the pointer arguments is 0, one of which was
  discovered by trinity on the getdents handler here:
  https://builds.sr.ht/~alip/job/1077263
- **Security**: Fix a crash in getdents handler discovered by trinity fuzzer in
  this build: https://builds.sr.ht/~alip/job/1077263
- Support compatible system call ABIs as necessary, e.g. on `x86-64`, we now
  support `x86`, and `x32` ABIs, on `aarch64` we support `arm` too etc. With
  this out of the way, the default bad architecture action has been changed to
  "kill process".
- Added helper script `data/esyd.bash` which when sourced into a bash
  environment, defines the convenience function `esyd` to interact with syd
  sandbox commands.
- Stat'ing the magic path `/dev/syd/stat` prints the syd status on standard
  error.
- Reading from the magic path `/dev/syd/dump` returns the current syd
  state as JSON. This is only available when the sandbox is not locked, or
  it's only available to the syd execve child via `lock:exec`.
- `syd --read path` may be used to canonicalize a path.
- Log messages with process ID information are now enriched with the current
  working directory of the process.
- **lchown**, and **lgetxattr** system calls are now sandboxed.
- Implement `--list set` to display the list of system calls in the given set.
  The supported sets are **allow**, **deny**, and **hook**.
- Fix BusyBox compatibility issues in integration tests.

# 3.0.10

- Fix unit tests

# 3.0.9

- Fix yet another case where a path with invalid UTF-8 would make syd panic.
- **Security**: syd now normalizes the **path** argument of the emulated
  **open** system call which prevents some jail breaks, the simplest being to
  invoke `cat /proc/./self/status` inside syd which erroneously opens the
  proc directory of syd rather then that of the process. We have added about
  80 integration tests which test various relative paths to break the sandbox
  and syd passes all these tests after this fix.
- Use the **paludis** profile rather than the **user** in tests to improve
  reproducibility. Since the **user** profile parsers `~/.user.syd-3` this could
  cause random test failures.
- Calling a system call in an inaccessible directory would fail with `EACCES`
  even if the path argument is an absolute path. This is now fixed.

# 3.0.8

- Fix a panic in open system call hook for invalid UTF-8 paths.
- Add `/home` to the list of read-only directories for Landlock for `user`
  profile.
- `SYD_NPROC` environment variable can be used to configure the number of system
  call handler threads.
- Command options are now pretty printed in `test -e /dev/syd/dump` output.
- Reduce the duration of write lock contention in open system call handlers.
- Consider open calls with the flag `O_CREAT` for write sandboxing regardless of
  access mode.

# 3.0.7

- Use `epoll` rather than `poll` in the syd poll thread.
- Ensure the syd process supervisor does not leak the seccomp file descriptor
  on error conditions.
- syd's thread group id determiner function which reads `/proc/pid/status`
  would hang forever in case the process exits after we open the file but before
  we're finished with reading. This is now fixed.
- The --print-profile CLI option has been renamed to --print.
- Added `syd --check` to check if the process is running under syd.

# 3.0.6

- syd now honors the umask of the environment rather than setting a strict
  umask.
- Fix the open emulator to properly handle open calls with `O_TMPFILE` flag.

# 3.0.5

- Handle **AT\_EMPTY\_PATH** flag properly in **execveat**, **fchownat**,
  **linkat**, **statx**, **newfstatat**, and **utimensat** syscall hooks.

# 3.0.4

- The system call hook of **open** family system calls now properly sets umask
  to that of the process before emulating open so the umasks in sandbox are now
  properly honoured.
- Properly handle system calls with a file descriptor and an empty path as
  argument.
- Follow symbolic links in path resolution regardless of the system call.
- New command line option **--print-profile** to print the rules of the given
  sandbox profile.
- The sandbox profiles **paludis** and **user** have been hardened by utilizing
  [Read Sandboxing](#read-sandboxing) and [Stat Sandboxing](#stat-sandboxing).
  Many sensitive paths such as **/proc/pid/mem**, **/dev/mem** are both hidden
  and denylisted for read.
- **Landlock** errors are no longer fatal.
- **syd** has now basic support for UID/GID mapping inside user namespaces,
  where by default the current user is mapped with the same UID/GID inside the
  container.
- **syd-test** now changes its current working directory to a temporary
  directory before running integration tests. There is also a new validation in
  place when **syd-test** will refuse to run as root. This is due to the fact
  that the integration tests will fail randomly when run with elevated
  privileges.
- Use **SECCOMP_IOCTL_NOTIF_ADDFD** in **open**, **openat** and **openat2**
  calls to close the **TOCTOU** window, providing security. Once POC for
  **open** system call which utilizes pointer modification to break out of jail
  has been included in the test suite and is fixed with this change.

# 3.0.3

- **Security**: syd did not check the target argument of **symlink** and
  **symlinkat** system calls which makes a jail break possible through a symlink
  attack. Two POCs, one for each system call respectively, are included in the
  test suite. With syd checking the target argument these breaks no longer
  work.
- `syd -t`, and `syd-test` now accept many of either a name regex, a test index,
  or a test index range as arguments to filter which integration tests to run.

# 3.0.2

- `-H, --hostname name`, `-D, --domainname name` added to set host, domain name
  of sandboxed process. This requires `--unshare-uts`.
- `-u name, --uid=name` and `-g name, --gid=name` options have been added to run
  the sandboxed process as another user.
- `-A alias, --arg0=alias` has been added to set an alias for the sandbox
  process.
- `-W dir, --work-dir=dir` option has been added to change into a directory before
  executing sandbox process.
- `-C dir, --chroot=dir` option has been added to chroot into a directory before
  executing sandbox process.
- `--unshare-pid,net,mount,uts,ipc,user` command line arguments have been added
  for namespaces support.
- `--export pfc` now has detailed information about the seccomp rules, and lists
  of allowed and notified system calls.
- The old and unused **_sysctl** system call is no longer allowed by syd.
- syd now reports libsecc☮mp version in `--version` output.
- Remove read beneath /home for landlock in user profile.
- Clean syd related environment variables from the environment of the
  sandboxed process.

# 3.0.1

- New sandboxing type [Lock Sandboxing](#lock-sandboxing) to utilize
  [Landlock](https://landlock.io/)
  [LSM](https://en.wikipedia.org/wiki/Linux_Security_Modules).
- syd no longer sets umask to 077 for the sandbox process.
- Disable **setuid** system call in the sandbox for added security. Since this
  system call normally requires an environment with new privileges, this is not
  possible under syd as the sandbox has "no new privileges" flag set.

# 3.0.0

- **Milestone**: Paludis builds under syd with recommended tests using this
  [MR](https://gitlab.exherbo.org/paludis/paludis/-/merge_requests/86).
- Sandbox command lock now defaults to **exec** rather than **off** for added
  security.
- `allowlist/successful_bind` was broken by a recent change. This is now fixed.
- The `trace/memory_access` command is fixed, `strace -c` confirms the results

# 3.0.0-beta.15

- Test suite now properly recognizes that it is running under syd and skips
  the integration tests.
- syd now properly exits with the exit code of the sandbox process and exit
  codes for error conditions are documented in `--help`.
- Fix an issue with triple star extension in path glob matches.

# 3.0.0-beta.14

- Fix an issue with /proc/pid/cmdline reader.
- `symlink` and `symlinkat` system call interceptors no longer check the target
  for access.
- Skip running integration tests when running under syd.
- `lock:exec` no longer waits for the initial **exec** call to lock the sandbox
  for all processes except the syd exec child.

# 3.0.0-beta.13

- Drop the `built` crate dependency.
- Drop the `procfs` crate dependency.
- Use the `built` crate without the `git2` feature.
- Don't use `snmalloc` as the global allocator anymore. This fixes issues with
  static linking on Gentoo.

# 3.0.0-beta.12

- Fix an issue of **stat** sandboxing with path hiding.
- The environment variable **SYD\_NO\_CROSS\_MEMORY\_ATTACH** may be set to
  disable using cross memory attach and fallback to `/proc/pid/mem`.
- The environment variable **SYD\_NO\_SYSLOG** may be set to disable logging to **syslog**.
- Canonicalize UNIX socket addresses before sandbox access check.
- Add common system directories to the allowlist in **user** profile to make
  usage more practical.
- Add `--export` argument to export secure computing rules in binary **Berkeley
  Packet Filter** format and textual **Pseudo Filter Code** formats.
- System call hooks now use system call name and arguments to determine whether
  remote path canonicalization should resolve symbolic links.
- bump MSRV from `1.69` to `1.70`.
- `error` and `warn` level logs are not written to standard error unless
  standard error is a terminal. Since logs of these levels also go to **syslog**
  this is no loss for the user. This is merely to provide convenience when
  running terminal user interfaces under syd.
- `user` profile now enables `stat` sandboxing with the user home directory
  allowlisted.

# 3.0.0-beta.11

- Added `stat` sandboxing which can be used to hide files and directories from
  the sandboxed process.
- The sandbox command `denylist/network` has been renamed to `denylist/net`.
- The sandbox command `allowlist/network` has been renamed to `allowlist/net`.
- The sandbox command `filter/network` has been renamed to `filter/net`.
- The sandbox command `sandbox/network` has been renamed to `sandbox/net`.
- `user` profile now properly allowlists screen and tmux connections.

# 3.0.0-beta.10

- When debug mode is enabled with `SYD_LOG=debug`, syd now logs all system
  calls with seccomp action other than `Allow` to the kernel log. This is useful
  in tackling problems with build failures.
- System calls with bad architecture know return `ENOSYS` rather than syd
  killing the thread.
- Disallowed system calls are now denied with `EACCES` rather than `ENOSYS`.
- syd now sets seccomp system call priority of hotter system calls to a
  higher value to improve performance.
- Fix a potential panic with `/proc/self` -> `/proc/pid`  handling in remote
  paths.

# 3.0.0-beta.9

- Fix an issue with remote path canonicalization.

# 3.0.0-beta.8

- Consolidate error handling, making it faster and more robust.
- Various fixes and improvements for the remote path canonicalization code which
  makes it faster and more robust with regards to error handling.

# 3.0.0-beta.7

- syd now ignores the signals `SIGHUP`, `SIGTSTP`, `SIGTTOU`, and `SIGTTIN`
  for uninterrupted tracing.
- The **user** profile now sets the environment variable
  `GIT_CEILING_DIRECTORIES` to `HOME` to save the user from some useless and
  annoying access violations.

# 3.0.0-beta.6

- Make the **user** profile Exherbo friendly.

# 3.0.0-beta.5

- The `user` profile now has **read** and **exec** sandboxing enabled as well as
  **write** and **network** sandboxing.
- The **triple star** extension is applied to glob patterns, ie `/dev/***`
  matches both `/dev` and any file recursively under `/dev`.
- When run without arguments, the home directory of the current user is now
  looked up from `passwd(5)` data rather than using the `HOME`
  environment variable.
- The clause **last matching rule wins** was not honored at all times. This is
  now fixed.

# 3.0.0-beta.4

- The `user` profile now also parses the file `~/.user.syd-3` if it exists.
  Note, syd uses this profile when invoked without arguments. This provides an
  easy way to spawn a working shell under sandbox.
- Fix UDP network sandboxing which was broken due to invalid error handling for
  connection-mode sockets.
- Some glob patterns in sandbox profiles `paludis`, and `user` have been fixed.

# 3.0.0-beta.3

- Run tests as integration tests, drop the `test-bin` development dependency.

# 3.0.0-beta.2

- Added the new `user` sandbox profile which allows access to user-specific
  directories such as `HOME`, and connections such as `X`, `screen`, `tmux` etc.
  When invoked without arguments, `syd` now drops to a shell with this profile.
- Replace `regex` crate with the more lightweight and performant `regex-lite`
  crate.
- Implement the `cmd/exec` sandbox command and the `syd exec` subcommand.
- Switch from `glob` crate to the `glob-match` crate for matching glob patterns.
- Fallback to `/proc/$pid/mem` if cross memory attach is not enabled in the
  kernel. Use `SYD_PROC_MEM` environment variable or the sandbox command
  `trace/memory_access:1` to force `/proc` fallback.
- `exec/kill_if_match` has been renamed to `exec/kill` which is a **breaking
  change**.
- Set `panic = abort` in release builds for reduced binary size.
- Name the polling thread `syd-poll`.
- Better error handling, and cleaner code.
- Use `parking_lot` crate for `Mutex`, and `RwLock`.
- The default magic virtual device path has been updated from `/dev/syd` to
  `/dev/syd` saving three letters on each typing!! This is a **breaking
  change**.
- The `core/` prefix has been removed from the configuration items
  `core/sandbox`, e.g use `sandbox/exec:on` rather than `core/sandbox/exec:on`.
  `allowlist/successful_bind` has been renamed to `trace/allow_successful_bind`,
  and `allowlist/unsupported_socket_families` has been renamed to
  `trace/allow_unsupported_socket_families`. Moreover the config item
  `core/trace/magic_lock` has been renamed to simply `lock`. This is a
  **breaking change**.
- The prefixes `unix:`, `unix-abstract:`, `inet:`, `inet6:` are no longer used
  in network addresses. Instead the pattern is treated as a UNIX shell style
  pattern if it starts with `/`, and as an IP address otherwise. There is no
  distinction between unix sockets and abstract unix sockets anymore. This is a
  **breaking change**. Check the `data/` subdirectory for a `syd.bash` for
  use with `Paludis`.
- Fix a bug with remote path canonicalization.
- Access violations are logged to syslog now. Use, e.g. `journalctl
  SYSLOG_IDENTIFIER=syd` to view them.

# 3.0.0-alpha.2

- When run without arguments, `syd` now drops into user's current running
  shell allowlisting the `HOME` directory.
- Document the CLI option `-p`, `--profile` and add `noipv4` and `noipv6`
  profiles in addition the `paludis` profile. These profiles may be stacked by
  specifying more than one `-p` arguments.
- Use a Seccomp `BPF` filter rather than a `Notify` filter for fakeroot mode.
- Improve logging to achieve consistency. We have a very simple Logger which logs
  to standard error in format `JSON` lines. There are some common keys `id` is
  always `syd`, `l` gives the `Log::Level` as an integer whereby the lower the
  value of the integer the more severe is the log condition. `t` gives a UNIX
  time stamp in seconds, and `ctx` has short context on the log entry. Errors are
  represented with the `err` key, and system call names are given with the `sys`
  key.
- The `--profile <profile-name>` and `--config @<profile-name>` is now
  supported. `Paludis` uses the former so it is important for compatibility.
  The profile file is **no longer** installed under `${sharedir}/syd` where
  `{sharedir}` is usually `/usr/share` and is kept as a static array in the
  program itself. In the future when `syd-3` has an exheres we can improve on
  this but for now this gets us going.
- The `setuid` system call is now allowed in the sandbox.
- Use `snmalloc` as the global allocator for improved performance.

# 3.0.0-alpha.1

- **New**: Added `core/allowlist/successful_bind`.
  - Utilizes `getsockname` hook, `pidfd_getfd`, and `process_vm_writev` for complete emulation.
  - Features a `TTL` of 3 mins for tracking addresses to manage zero port arguments in `bind()` system calls.

- **Improved**: Refined read, write, network/{bind,connect} sandboxing.
  - Simpler implementation, yet compatible with `Paludis` via `esandbox`.
  - No per-process sandboxing or process tree tracking; uses `/proc/$pid/cwd` when required.
  - Single set of sandbox rules with configurations pushed upfront.
  - **API Change**: Replaced `allow`, `deny` modes with simpler `on/off` toggle.
  - `core/sandbox/network` can be set to `bind` or `connect` for selective sandboxing.
  - Rule matching favors the latest rule for configuration stacking.
  - Streamlined `core/trace/magic_lock:exec` due to lack of parent/child tracking.

- **New**: Introduced `seccomp` process supervision.
  - Implemented primarily in `syd::hook` and `syd::remote`.
  - Derived from the `greenhook` crate, but with a deny-by-default `seccomp` policy.
  - Allowlisted system calls maintained in `syd::config` (currently immutable by users).
  - Notable system calls like `ptrace`, `process_vm_writev`, and `io-uring` are disabled to counteract `TOCTOU` vulnerabilities.

<!-- vim: set spell spelllang=en tw=80 : -->
