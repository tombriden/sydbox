# ChangeLog

## 0.8.4

- Improve documentation.

## 0.8.3

- Add `--timeout` option to `profile` subcommand to limit process
  runtime with a duration. The option takes human-formatted time as
  argument.
- Block SIGINT when profiling so interrupting the underlying process
  does not interrupt profile generation.

## 0.8.2..

See git history
