//
// pandora: syd's Dump Inspector & Profile Writer
// pandora.rs: Main entry point
//
// Copyright (c) 2021, 2024 Ali Polatel <alip@exherbo.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#![allow(clippy::disallowed_methods)]

use std::{
    fs::OpenOptions,
    io::BufRead,
    iter::FromIterator,
    os::unix::io::FromRawFd,
    path::Path,
    process::{exit, Command, ExitCode},
    thread,
    time::{Duration, SystemTime, UNIX_EPOCH},
};

use clap::{Arg, ArgAction};
use humantime::parse_duration;
use nix::{
    libc::pid_t,
    sys::{
        signal::{kill, sigprocmask, SigmaskHow, Signal},
        signalfd::SigSet,
    },
    unistd::Pid,
};
use serde::{Deserialize, Serialize};
use time::{format_description, OffsetDateTime};

pub mod built_info {
    // The file has been placed there by the build script.
    include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, PartialOrd, Ord, Hash)]
#[repr(u8)]
enum Sandbox {
    Bind,
    Connect,
    Exec,
    Write,
    Read,
    Stat,
}

impl std::fmt::Display for Sandbox {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Bind => write!(f, "allow/net/bind"),
            Self::Connect => write!(f, "allow/net/connect"),
            Self::Write => write!(f, "allow/write"),
            Self::Exec => write!(f, "allow/exec"),
            Self::Read => write!(f, "allow/read"),
            Self::Stat => write!(f, "allow/stat"),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(untagged)]
enum Access {
    InetAddr {
        ctx: String,
        cap: String,
        addr: String,
        t: u64,
    },
    UnixAddr {
        ctx: String,
        cap: String,
        unix: String,
        t: u64,
    },
    Path {
        ctx: String,
        cap: String,
        path: String,
        t: u64,
    },
    Run {
        ctx: String,
        comm: String,
        args: Vec<String>,
        t: u64,
    },
    Any {
        ctx: String,
    },
}

fn command_profile<'b>(
    bin: &'b str,
    cmd: &[&'b str],
    output_path: &'b str,
    path_limit: u8,
    cmd_timeout: Option<Duration>,
    config: Option<&[&'b str]>,
) -> u8 {
    if Path::new(output_path).exists() {
        eprintln!("pandora: Output file {output_path} exists, bailing out.");
        return 1;
    }

    let (fd_rd, fd_rw) = match nix::unistd::pipe() {
        Ok((fd_rd, fd_rw)) => (fd_rd, fd_rw),
        Err(error) => {
            eprintln!("pandora: error creating pipe: {}", error);
            return 1;
        }
    };

    let log_fd = fd_rw.to_string();
    let mut syd = Command::new(bin);
    syd.env("SYD_NO_SYSLOG", "1");
    syd.env("SYD_LOG", "info");
    syd.env("SYD_LOG_FD", log_fd);
    syd.arg("-x");
    syd.arg("-ppandora");
    if let Some(config) = config {
        let args: Vec<String> = config.iter().map(|arg| format!("-m{arg}")).collect();
        syd.args(args);
    }
    syd.arg("--").args(cmd);
    let mut child = syd.spawn().expect("syd command failed to start");

    // Block SIGINT in the parent process.
    let mut mask = SigSet::empty();
    mask.add(Signal::SIGINT);
    sigprocmask(SigmaskHow::SIG_BLOCK, Some(&mask), None).expect("Failed to block signals");

    if let Some(cmd_timeout) = cmd_timeout {
        let pid = Pid::from_raw(child.id() as pid_t);
        thread::spawn(move || {
            thread::sleep(cmd_timeout);
            eprintln!("pandora: Timeout expired, terminating process...");
            let _ = kill(pid, Signal::SIGTERM);
        });
    }

    nix::unistd::close(fd_rw).expect("failed to close write end of pipe");
    let input = Box::new(std::io::BufReader::new(unsafe {
        std::fs::File::from_raw_fd(fd_rd)
    }));
    let r = do_inspect(input, output_path, path_limit, config);

    child.wait().expect("failed to wait for syd");
    eprintln!("pandora: Profile has been written to {output_path}.");
    eprintln!("pandora: To use it, do: syd -P {output_path} command args...");

    r
}

fn command_inspect(input_path: &str, output_path: &str, path_limit: u8) -> u8 {
    let input = open_input(input_path);
    do_inspect(input, output_path, path_limit, None)
}

fn main() -> ExitCode {
    let matches = clap::Command::new(built_info::PKG_NAME)
        .about(built_info::PKG_DESCRIPTION)
        .author(built_info::PKG_AUTHORS)
        .version(built_info::PKG_VERSION)
        .arg_required_else_help(true)
        .help_expected(true)
        .next_line_help(false)
        .infer_long_args(true)
        .infer_subcommands(true)
        .propagate_version(true)
        .subcommand_required(true)
        .max_term_width(80)
        .help_template(
            r#"
{before-help}{name} {version}
{about}
Copyright (c) 2023, 2024 {author}
SPDX-License-Identifier: GPL-3.0-or-later

{usage-heading} {usage}

{all-args}{after-help}
"#,
        )
        .after_help(format!(
            "\
Hey you, out there beyond the wall,
Breaking bottles in the hall,
Can you help me?

Send bug reports to {}
Attaching poems encourages consideration tremendously.

License: {}
Homepage: {}
Repository: {}
",
            built_info::PKG_AUTHORS,
            built_info::PKG_LICENSE,
            built_info::PKG_HOMEPAGE,
            built_info::PKG_REPOSITORY,
        ))
        .subcommand(
            clap::Command::new("profile")
                .about("Execute a program under inspection and write a syd profile")
                .arg(
                    Arg::new("bin")
                        .default_value("syd")
                        .help("Path to syd binary")
                        .long("bin")
                        .env("SYD_BIN")
                        .num_args(1),
                )
                .arg(
                    Arg::new("magic")
                        .action(ArgAction::Append)
                        .help("Run a sandbox command during init, may be repeated")
                        .long("magic")
                        .short('m')
                        .num_args(1),
                )
                .arg(
                    Arg::new("output")
                        .default_value("./out.syd-3")
                        .help("Path to syd profile output")
                        .long("output")
                        .short('o')
                        .env("PANDORA_OUT")
                        .num_args(1),
                )
                .arg(
                    Arg::new("limit")
                        .default_value("7")
                        .required(false)
                        .help("Maximum number of path members before trim, 0 to disable")
                        .long("limit")
                        .short('l')
                        .value_parser(clap::value_parser!(u64).range(0..=u64::from(u8::MAX))),
                )
                .arg(
                    Arg::new("timeout")
                        .required(false)
                        .help("Human-formatted timeout duration")
                        .long("timeout")
                        .short('t')
                        .value_parser(|s: &str| parse_duration(s).map_err(|e| e.to_string())),
                )
                .arg(
                    Arg::new("cmd")
                        .required(true)
                        .help("Command to run under syd")
                        .num_args(1..),
                ),
        )
        .subcommand(
            clap::Command::new("inspect")
                .about("Read a syd core dump and write a syd profile")
                .arg(
                    Arg::new("input")
                        .required(true)
                        .help("Path to syd core dump")
                        .long("input")
                        .short('i'),
                )
                .arg(
                    Arg::new("output")
                        .default_value("./out.syd-3")
                        .required(true)
                        .help("Path to syd profile output")
                        .long("output")
                        .short('o')
                        .env("PANDORA_OUT"),
                )
                .arg(
                    Arg::new("limit")
                        .default_value("7")
                        .required(false)
                        .help("Maximum number of path members before trim, 0 to disable")
                        .long("limit")
                        .short('l')
                        .value_parser(clap::value_parser!(u64).range(0..=u64::from(u8::MAX))),
                ),
        )
        .get_matches();

    let (subcommand, submatches) = matches.subcommand().expect("missing subcommand");

    match subcommand {
        "profile" => {
            let bin = submatches.get_one::<String>("bin").expect("bin");
            let out = submatches.get_one::<String>("output").expect("output");
            let limit = *submatches.get_one::<u64>("limit").expect("limit") as u8;
            let timeout = submatches.get_one::<Duration>("timeout").copied();
            let cmd: Vec<&str> = submatches
                .get_many::<String>("cmd")
                .expect("cmd")
                .map(|s| s.as_str())
                .collect();
            let magic: Option<Vec<&str>> = if submatches.contains_id("magic") {
                Some(
                    submatches
                        .get_many::<String>("magic")
                        .expect("magic")
                        .map(|s| s.as_str())
                        .collect(),
                )
            } else {
                None
            };
            ExitCode::from(command_profile(
                bin,
                &cmd,
                out,
                limit,
                timeout,
                magic.as_deref(),
            ))
        }
        "inspect" => {
            let input = submatches.get_one::<String>("input").expect("input");
            let output = submatches.get_one::<String>("output").expect("output");
            let limit = *submatches.get_one::<u64>("limit").expect("limit") as u8;
            ExitCode::from(command_inspect(input, output, limit))
        }
        _ => unreachable!(),
    }
}

fn do_inspect(
    input: Box<dyn std::io::BufRead>,
    output_path: &str,
    path_limit: u8,
    config: Option<&[&str]>,
) -> u8 {
    let mut output = open_output(output_path);
    let mut magic = std::collections::HashSet::<(Sandbox, String)>::new();
    let mut program_invocation_name = "?".to_string();
    let mut program_command_line = vec![];
    let mut program_startup_time = UNIX_EPOCH;

    for line in input.lines() {
        let serialized = match line {
            Ok(line) if line.is_empty() => {
                break; /* EOF */
            }
            Ok(line) => line,
            Err(error) => {
                eprintln!("pandora: failed to read line from input: {error}");
                return 1;
            }
        };

        // Parse JSON
        if let Some((comm, args, timestamp)) = parse_json_line(&serialized, &mut magic, path_limit)
        {
            program_invocation_name = comm;
            program_command_line = args;
            program_startup_time = timestamp;
        }
    }

    /* Step 1: Print out the magic header. */
    writeln!(
        &mut output,
        "#
# syd profile generated by pandora-{}
# Date: {}

###
# Global Defaults
###
sandbox/read:on
sandbox/stat:on
sandbox/write:on
sandbox/exec:on
sandbox/net:on

{}

###
# Auto-generated magic entries
# Program: {}
# Arguments: {:?}
###
",
        built_info::PKG_VERSION,
        format_system_time(program_startup_time),
        config
            .map(|config| config.join("\n"))
            .unwrap_or("".to_string()),
        program_invocation_name,
        program_command_line,
    )
    .unwrap_or_else(|_| panic!("failed to print header to output »{}«", output_path));

    /* Step 2: Print out magic entries */
    let mut list = Vec::from_iter(magic);
    list.sort_by_key(|(_, argument)| argument.clone()); /* secondary alphabetical sort. */
    #[allow(clippy::clone_on_copy)]
    list.sort_by_cached_key(|(sandbox, _)| sandbox.clone()); /* primary sandbox sort. */
    for entry in list {
        writeln!(&mut output, "{}+{}", entry.0, entry.1).unwrap_or_else(|_| {
            panic!(
                "failed to print entry »{:?}« to output »{}«",
                entry, output_path
            )
        });
    }

    writeln!(&mut output, "\n# Lock configuration\nlock:on")
        .unwrap_or_else(|_| panic!("failed to lock configuration for output »{}«", output_path));

    0
}

#[allow(clippy::type_complexity)]
fn parse_json_line(
    serialized: &str,
    magic: &mut std::collections::HashSet<(Sandbox, String)>,
    path_limit: u8,
) -> Option<(String, Vec<String>, SystemTime)> {
    match serde_json::from_str(serialized)
        .unwrap_or_else(|e| panic!("failed to parse line: »{}«", e))
    {
        Access::Path { cap, path, .. } => {
            if let Some(path) = process_path(&path, path_limit) {
                let sandbox = match cap.as_str() {
                    "r" => crate::Sandbox::Read,
                    "s" => crate::Sandbox::Stat,
                    "w" => crate::Sandbox::Write,
                    "x" => crate::Sandbox::Exec,
                    _ => unreachable!(),
                };
                magic.insert((sandbox, path));
            }
        }
        Access::InetAddr { cap, addr, .. }
        | Access::UnixAddr {
            cap, unix: addr, ..
        } => {
            let sandbox = match cap.as_str() {
                "b" => crate::Sandbox::Bind,
                "c" => crate::Sandbox::Connect,
                _ => unreachable!(),
            };
            magic.insert((sandbox, addr));
        }
        Access::Run { comm, args, t, .. } => {
            return Some((comm, args, UNIX_EPOCH + Duration::from_secs(t)));
        }
        _ => {}
    };

    None
}

fn open_input(path_or_stdin: &str) -> Box<dyn std::io::BufRead> {
    match path_or_stdin {
        "-" => Box::new(std::io::BufReader::new(std::io::stdin())),
        path => Box::new(std::io::BufReader::new(
            match OpenOptions::new().read(true).open(path) {
                Ok(file) => file,
                Err(error) => {
                    eprintln!("pandora: Failed to open file »{path}«: {error}");
                    exit(1);
                }
            },
        )),
    }
}

fn open_output(path_or_stdout: &str) -> Box<dyn std::io::Write> {
    match path_or_stdout {
        "-" => Box::new(std::io::BufWriter::new(std::io::stdout())),
        path => Box::new(std::io::BufWriter::new(
            match OpenOptions::new().write(true).create_new(true).open(path) {
                Ok(file) => file,
                Err(error) => {
                    eprintln!("pandora: Failed to open file »{path}«: {error}");
                    exit(1);
                }
            },
        )),
    }
}

fn process_path(path: &str, limit: u8) -> Option<String> {
    if limit == 0 || path == "/" {
        Some(path.to_string())
    } else if path.ends_with("/.") || path.ends_with("/..") {
        None
    } else if let Some(glob) = path2glob(path) {
        Some(glob)
    } else {
        let members: Vec<&str> = path.split('/').collect();
        let limit = limit as usize;
        Some(if limit > 0 && limit <= members.len() {
            members[0..limit].join("/")
        } else {
            members.join("/")
        })
    }
}

fn path2glob(path: &str) -> Option<String> {
    let components: Vec<&str> = path.split('/').collect();

    if path.starts_with("/proc/")
        && components.len() >= 3
        && components[2].chars().all(char::is_numeric)
    {
        if components.len() > 4
            && components[4].chars().all(char::is_numeric)
            && components[3] == "task"
        {
            // Handle the /proc/$pid/task/$tid/... case
            let rest_of_path = if components.len() > 5 {
                format!("/{}", components[5..].join("/"))
            } else {
                String::new()
            };
            Some(format!("/proc/[0-9]*/task/[0-9]*{}", rest_of_path))
        } else {
            // Handle the general /proc/$pid/... case
            let rest_of_path = if components.len() > 3 {
                format!("/{}", components[3..].join("/"))
            } else {
                String::new()
            };
            Some(format!("/proc/[0-9]*{}", rest_of_path))
        }
    } else if path.starts_with("/dev/pts/") {
        if path.split('/').count() == 4
            && path
                .split('/')
                .nth(3)
                .unwrap()
                .chars()
                .all(char::is_numeric)
        {
            Some("/dev/pts/[0-9]*".to_string())
        } else {
            None
        }
    } else if path.starts_with("/dev/tty") {
        Some("/dev/tty*".to_string())
    } else {
        None
    }
}

fn format_system_time(system_time: SystemTime) -> String {
    let datetime = OffsetDateTime::from(system_time);
    let format =
        format_description::parse("[year]-[month]-[day] [hour]:[minute]:[second].[subsecond]")
            .unwrap();
    datetime.format(&format).unwrap()
}
