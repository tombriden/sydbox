//
// syd: seccomp and landlock based application sandbox with support for namespaces
// benches/bench.rs Benchmarks
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    fs::{create_dir_all, remove_dir_all},
    os::unix::fs::symlink,
    path::PathBuf,
};

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use nix::unistd::Pid;
use syd::fs::{canonicalize, MissingHandling};
use tempfile::tempdir;

fn setup_paths() -> (PathBuf, PathBuf, PathBuf, PathBuf) {
    let temp_dir = tempdir().expect("Failed to create a temp dir");
    let temp_dir_path = temp_dir.path();

    // Existing path with symlinks
    let existing_path = temp_dir_path.join("existing");
    create_dir_all(&existing_path).expect("Failed to create existing path");
    let symlink_path = temp_dir_path.join("symlink");
    symlink(&existing_path, &symlink_path).expect("Failed to create symlink");

    // Self-referencing loop
    let loop_path = temp_dir_path.join("loop");
    create_dir_all(&loop_path).expect("Failed to create loop path");
    let loop_symlink = loop_path.join("self_loop");
    symlink(&loop_path, &loop_symlink).expect("Failed to create self-referencing symlink");

    // Non-existing path
    let non_existing_path = temp_dir_path.join("non_existing");

    (
        temp_dir_path.to_path_buf(),
        non_existing_path,
        symlink_path,
        loop_symlink,
    )
}

pub fn canonicalize_benchmark(c: &mut Criterion) {
    let (temp_dir_path, non_existing, symlink, loop_path) = setup_paths();

    let pid = Pid::this();
    let modes = [
        MissingHandling::Normal,
        MissingHandling::Existing,
        MissingHandling::Missing,
    ];

    for &mode in &modes {
        c.bench_function(&format!("canonicalize_non_existing_{:?}", mode), |b| {
            b.iter(|| {
                canonicalize(
                    pid,
                    black_box(&non_existing),
                    black_box(true),
                    black_box(mode),
                )
            })
        });

        c.bench_function(&format!("canonicalize_symlink_{:?}", mode), |b| {
            b.iter(|| canonicalize(pid, black_box(&symlink), black_box(true), black_box(mode)))
        });

        c.bench_function(&format!("canonicalize_loop_{:?}", mode), |b| {
            b.iter(|| canonicalize(pid, black_box(&loop_path), black_box(true), black_box(mode)))
        });
    }

    let _ = remove_dir_all(temp_dir_path);
}

pub fn sandbox_globset_benchmark(c: &mut Criterion) {
    let path = PathBuf::from("/etc/passwd");
    for i in &[10, 100, 1000, 10000, 100000] {
        let mut sandbox = syd::sandbox::Sandbox::default();
        for j in 0..*i {
            sandbox
                .config(if j % 2 == 0 {
                    "allow/read+/etc/***"
                } else {
                    "deny/read+/etc/***"
                })
                .unwrap();
        }
        sandbox.build_globsets().unwrap();
        c.bench_function(&format!("sandbox_match {i}"), |b| {
            b.iter(|| {
                sandbox.match_action(syd::sandbox::Capability::CAP_READ, black_box(&path));
            })
        });
        drop(sandbox);
    }
}

criterion_group!(benches, canonicalize_benchmark, sandbox_globset_benchmark);
criterion_main!(benches);
