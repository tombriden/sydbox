#!/usr/bin/env bash

mounted() {
        grep -q "$1" /proc/self/mounts
}

set -x

CDIR="${1:-$(git rev-parse --show-toplevel)/root}"

[[ -n "$CDIR" ]] || exit 127

sudo cp -L /etc/resolv.conf "$CDIR"/etc/resolv.conf

mounted "$CDIR"/dev     || sudo mount -o bind /dev     "$CDIR"/dev
mounted "$CDIR"/dev/pts || sudo mount -o bind /dev/pts "$CDIR"/dev/pts
mounted "$CDIR"/dev/shm || sudo mount -o bind /dev/shm "$CDIR"/dev/shm
mounted "$CDIR"/proc    || sudo mount -t proc procfs   "$CDIR"/proc
mounted "$CDIR"/sys     || sudo mount -t sysfs sysfs   "$CDIR"/sys
sudo chroot "$CDIR" /init
sudo umount "$CDIR"/proc
sudo umount "$CDIR"/sys
sudo umount "$CDIR"/dev/shm
sudo umount "$CDIR"/dev/pts
sudo umount "$CDIR"/dev
