#!/usr/bin/env python
# coding: utf-8

import re, sys, netrc, shlex, ssl, threading, time
import irc.bot, irc.strings
import paramiko

MAX_MSG_LENGTH = 370
MAX_MSG_CHUNKS = 3

STFU_TIMEOUT = 60
STFU_INIT = re.compile(r"sydbot[:,]\s+stfu", re.I)
STFU_DONE = re.compile(r"sydbot[:,]\s+done", re.I)

class SydBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channels, nickname, realname, server, port=6697):
        credentials = netrc.netrc().authenticators(server)
        password = credentials[2] if credentials else None

        ssl_factory = irc.connection.Factory(wrapper=ssl.wrap_socket)
        irc.bot.SingleServerIRCBot.__init__(
            self,
            [(server, port, password)],
            nickname,
            realname,
            connect_factory=ssl_factory,
        )
        self.channel_list = channels
        self.channel_timeouts = {}

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        for channel in self.channel_list:
            c.join(channel)
            print(f"Joining {channel}", file=sys.stderr)

    def on_privmsg(self, c, e):
        print(f"Received command: {e.arguments[0]}", file=sys.stderr)
        self.do_command(e, e.arguments[0], e.source.nick)

    def on_pubmsg(self, c, e):
        if STFU_DONE.match(e.arguments[0]):
            if e.target in self.channel_timeouts:
                del self.channel_timeouts[e.target]
            c.privmsg(e.target, "aye")
            return
        if e.target in self.channel_timeouts and time.time() < self.channel_timeouts[e.target]:
            return # Ignore message if channel is in timeout
        if STFU_INIT.match(e.arguments[0]):
            self.channel_timeouts[e.target] = time.time() + STFU_TIMEOUT
            c.privmsg(e.target, "aye")
            return

        cmd = None
        if e.arguments[0].startswith(";"):
            cmd = e.arguments[0][1:]
            sh = "rc"
        elif e.arguments[0].startswith("$"):
            cmd = e.arguments[0][1:]
            sh = "bash"

        if cmd is not None:
            print(
                f"Received command: {cmd} in {e.target} using shell {sh}",
                file=sys.stderr,
            )
            self.do_command(e, e.arguments[0][1:], e.target, sh=sh)

    def do_command(self, e, cmd, target, sh="bash"):
        c = self.connection

        if sh == "bash":
            sh = "/bin/bash"
            cmd = shlex.quote(f"({cmd}) 2>&1")
        elif sh == "rc":
            sh = "env PATH=/opt/plan9port/bin:$PATH /opt/plan9port/bin/rc"
            cmd = shlex.quote(f"{{{cmd}}} >[2=1]")
        else:
            raise ValueError(f"Invalid shell {sh}")

        def execute_ssh_command():
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect("syd.chesswob.org", username="syd", password="syd")

            channel = ssh.get_transport().open_session()
            channel.settimeout(7)
            channel.exec_command(f"{sh} -c {cmd}")

            output = ""
            end_time = time.time() + 7  # 7 seconds from now

            while not channel.exit_status_ready():  # Wait for command to complete
                if time.time() > end_time:
                    break
                if channel.recv_ready():
                    output += channel.recv(1024).decode("utf-8", "ignore")
                time.sleep(0.1)  # Small delay to prevent high CPU usage

            # Read any remaining output
            if channel.recv_ready():
                output += channel.recv(1024).decode("utf-8", "ignore")

            ssh.close()
            output = " ".join(
                output.split()
            ).strip()  # Replacing newlines with spaces and removing extra spaces

            if not output:
                output = "<no output>"
            if len(output) > MAX_MSG_LENGTH * MAX_MSG_CHUNKS:
                # Trim the output and add ellipsis if it's longer than the total allowed length
                output = output[: MAX_MSG_LENGTH * MAX_MSG_CHUNKS - 1] + "…"
                # Divide the output into chunks of max_length
                chunks = [
                    output[i : i + MAX_MSG_LENGTH].strip()
                    for i in range(0, len(output), MAX_MSG_LENGTH)
                ]
            else:
                # If the output is within the total allowed length, just divide it into chunks
                chunks = [
                    output[i : i + MAX_MSG_LENGTH].strip()
                    for i in range(0, len(output), MAX_MSG_LENGTH)
                ]

            for chunk in chunks:
                print(f"Sending output '{chunk}' to {target}", file=sys.stderr)
                c.privmsg(target, chunk)

        # Run SSH command in a separate thread to avoid blocking the bot
        thread = threading.Thread(target=execute_ssh_command)
        thread.start()


def main():
    if len(sys.argv) != 5:
        print("Usage: sydbot <server[:port]> <channel,...> <nickname> <realname>")
        sys.exit(1)

    s = sys.argv[1].split(":", 1)
    server = s[0]
    if len(s) == 2:
        try:
            port = int(s[1])
        except ValueError:
            print("Error: Erroneous port.")
            sys.exit(1)
    else:
        port = 6697
    channels = sys.argv[2].split(",")
    nickname = sys.argv[3]
    realname = sys.argv[4]

    bot = SydBot(channels, nickname, realname, server, port)
    bot.start()


if __name__ == "__main__":
    main()
