[![build status](https://builds.sr.ht/~alip/syd.svg)](https://builds.sr.ht/~alip/syd?)
[![repology](https://repology.org/badge/latest-versions/sydbox.svg)](https://repology.org/project/sydbox/versions)
[![dependency status](https://deps.rs/repo/sourcehut/~alip/syd/status.svg)](https://deps.rs/repo/sourcehut/~alip/syd)

[![syd](https://git.sr.ht/~alip/syd/blob/main/data/syd.png)](https://todo.sr.ht/~alip/syd)

Read the fine manuals of [syd](https://man.exherbolinux.org/),
[libsyd](https://libsyd.exherbolinux.org/),
[gosyd](https://gosyd.exherbolinux.org/),
[plsyd](https://plsyd.exherbolinux.org/),
[pysyd](https://pysyd.exherbolinux.org/),
[rbsyd](https://rbsyd.exherbolinux.org/),
[syd.el](https://sydel.exherbolinux.org/) and watch the asciicasts [Memory
Sandboxing](https://asciinema.org/a/625243), [PID
Sandboxing](https://asciinema.org/a/625170), [Network
Sandboxing](https://asciinema.org/a/623664), and [Sandboxing Emacs with
syd](https://asciinema.org/a/627055). Join the CTF event at
https://ctftime.org/event/2178 and try to read the file `/etc/CTF`¹ on
syd.chesswob.org with ssh user/pass: syd.

- Use cargo to install from source, requires [libseccomp](https://github.com/seccomp/libseccomp).
- Packaged on [Gentoo](https://gentoo.org/) as `sys-apps/syd`.
- Packaged on [Exherbo](https://exherbolinux.org/) as `sys-apps/sydbox`.
- Binary releases located at https://distfiles.exherbolinux.org/#sydbox/
- Releases are signed with this key: https://keybase.io/alip/pgp_keys.asc
- Change Log is here: https://git.sr.ht/~alip/syd/tree/main/item/ChangeLog.md

Maintained by Ali Polatel. Up-to-date sources can be found at
https://git.sr.ht/~alip/syd and bugs/patches can be submitted by email to
[~alip/sydbox-devel@lists.sr.ht](mailto:~alip/sydbox-devel@lists.sr.ht).

¹: The [SHA256](https://en.wikipedia.org/wiki/SHA-2)
checksum is `f1af8d3946546f9d3b1af4fe15f0209b2298166208d51a481cf51ac8c5f4b294`.

²: [That cat's something I can't explain!](https://gitlab.exherbo.org/paludis/paludis/-/commit/dd0566f16e27f2110581234fe1c48a11d18a7d64)

<!-- vim: set spell spelllang=en : -->
