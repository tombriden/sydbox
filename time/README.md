libsydtime is a
[LD_PRELOAD](https://en.wikipedia.org/wiki/Dynamic_linker#Systems_using_ELF) library
to replace [vDSO](https://en.wikipedia.org/wiki/VDSO) time calls with system calls.
This library is meant as a complement to [syd](https://sydbox.exherbolinux.org)'s
`trace/deny_tsc` sandbox command.

To install from source, clone the repository at https://git.sr.ht/~alip/syd, change
into the directory `time` and run `make`, `make test` and `doas make install`. The
only prerequisite is Rust 1.56.1 or later.

Below is a simple example of how to use libsydtime with syd:

```
$ strace -qf -e%clock syd -ppaludis -mtrace/deny_tsc:1 -eLD_PRELOAD=/usr/local/lib/libsydtime.so date
[pid 27095] clock_gettime(CLOCK_REALTIME, {tv_sec=1706186439, tv_nsec=28829866}) = 0
Thu Jan 25 12:40:39 UTC 2024
[pid 27095] +++ exited with 0 +++
$ strace -qf -e%clock syd -ppaludis date # vDSO
Thu Jan 25 12:41:25 UTC 2024
[pid 27103] +++ exited with 0 +++
$ strace -qf -e%clock syd -ppaludis -mtrace/deny_tsc:1 date # vDSO not permitted
[pid 27113] --- SIGSEGV {si_signo=SIGSEGV, si_code=SI_KERNEL, si_addr=NULL} ---
[pid 27113] +++ killed by SIGSEGV +++
$
```

Maintained by Ali Polatel. Up-to-date sources can be found at
https://git.sr.ht/~alip/syd and bugs/patches can be submitted by email to
[~alip/sydbox-devel@lists.sr.ht](mailto:~alip/sydbox-devel@lists.sr.ht).
