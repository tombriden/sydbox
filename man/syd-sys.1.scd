SYD-SYS(1)

# NAME

syd-sys - lookup system calls by numbers or regular expressions

# SYNOPSIS

*syd-sys* _[-a arch]_ _number|regex_

*syd-sys* _-a list_

# DESCRIPTION

Given a number, *syd-sys* prints the matching syscall name.

Given a regex, *syd-sys* prints case-insensitively matching syscall names.

# OPTIONS

|[ *-a*
:< Specify alternative architecture, such as *x86*, *x86_64* and *aarch64*.
|[
:< Use *list* to print the list of libseccomp supported architectures.

# SEE ALSO

_syd_(1), _syd_(2), _syd_(5), _syscall_(2)

*syd* homepage: https://sydbox.exherbolinux.org/

# AUTHORS

Maintained by Ali Polatel. Up-to-date sources can be found at
https://git.sr.ht/~alip/syd and bugs/patches can be submitted by email to
~alip/sydbox-devel@lists.sr.ht. Exherbo related bugs/issues can be submitted to
https://gitlab.exherbo.org/groups/sydbox/-/issues.
