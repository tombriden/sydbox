SYD-RUN(1)

# NAME

syd-run - run a program inside a syd container with the given process ID

# SYNOPSIS

*syd-run* _pid_ _{command [arg...]}_

# DESCRIPTION

The *syd-run* utility runs a program inside a syd container with the given
process ID.

# SEE ALSO

_syd_(1), _syd_(2), _syd_(5)

*syd* homepage: https://sydbox.exherbolinux.org/

# AUTHORS

Maintained by Ali Polatel. Up-to-date sources can be found at
https://git.sr.ht/~alip/syd and bugs/patches can be submitted by email to
~alip/sydbox-devel@lists.sr.ht. Exherbo related bugs/issues can be submitted to
https://gitlab.exherbo.org/groups/sydbox/-/issues.
