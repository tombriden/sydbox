SYD-LOG(1)

# NAME

syd-log - display syd access violation logs using _journalctl_(1)

# SYNOPSIS

*syd-log*

# DESCRIPTION

The *syd-log* utility may be used as a shorthand to display syd access
violation logs using _journalctl_(1). It is functionally identical to running
*journalctl SYSLOG_IDENTIFIER=syd*.

# SEE ALSO

_syd_(1), _syd_(2), _syd_(5), _journalctl_(1)

*syd* homepage: https://sydbox.exherbolinux.org/

# AUTHORS

Maintained by Ali Polatel. Up-to-date sources can be found at
https://git.sr.ht/~alip/syd and bugs/patches can be submitted by email to
~alip/sydbox-devel@lists.sr.ht. Exherbo related bugs/issues can be submitted to
https://gitlab.exherbo.org/groups/sydbox/-/issues.
