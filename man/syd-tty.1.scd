SYD-TTY(1)

# NAME

syd-tty - print the controlling terminal of the given process

# SYNOPSIS

*syd-tty* _pid_

# DESCRIPTION

The *syd-tty* utility prints the controlling terminal of the given process. It
is similar to the _tty_(1) utility except it allows printing the controlling
terminal of an arbitrary process.

# SEE ALSO

_syd_(1), _syd_(2), _syd_(5), _tty_(1), _ttyname_(3)

*syd* homepage: https://sydbox.exherbolinux.org/

# AUTHORS

Maintained by Ali Polatel. Up-to-date sources can be found at
https://git.sr.ht/~alip/syd and bugs/patches can be submitted by email to
~alip/sydbox-devel@lists.sr.ht. Exherbo related bugs/issues can be submitted to
https://gitlab.exherbo.org/groups/sydbox/-/issues.
