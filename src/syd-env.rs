//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/syd-env.rs: Run a command with the environment of the process with the given PID.
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    collections::VecDeque,
    os::unix::process::CommandExt,
    process::{Command, ExitCode, Stdio},
};

use nix::{
    libc::pid_t,
    sys::signal::{signal, SigHandler::SigDfl, SIGPIPE},
};
use procfs::process::Process;

fn main() -> ExitCode {
    let mut args: VecDeque<_> = std::env::args().skip(1).collect();
    let pid: pid_t = match args.pop_front().as_deref() {
        None | Some("-h") => {
            help();
            return ExitCode::SUCCESS;
        }
        Some(pid) => match pid.parse() {
            Ok(pid) => pid,
            Err(error) => {
                eprintln!("Invalid PID: {error}");
                return ExitCode::FAILURE;
            }
        },
    };

    let proc = match Process::new(pid) {
        Ok(proc) => proc,
        Err(error) => {
            eprintln!("syd-env: {error}");
            return ExitCode::FAILURE;
        }
    };

    let environ = match proc.environ() {
        Ok(environ) => environ,
        Err(error) => {
            eprintln!("syd-env: {error}");
            return ExitCode::FAILURE;
        }
    };

    // SAFETY: Set SIGPIPE to default handler for convenience.
    #[allow(clippy::disallowed_methods)]
    unsafe { signal(SIGPIPE, SigDfl) }.expect("sigpipe");

    let error = Command::new("env")
        .args(args)
        .env_clear()
        .envs(&environ)
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .exec();
    eprintln!("syd-env: {error}");
    ExitCode::FAILURE
}

fn help() {
    println!("Usage: syd-env pid [-i] [name=value]... {{command [arg...]}}");
    println!("Run a command with the environment of the process with the given PID.");
}
