//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/fs.rs: Filesystem utilities
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
// Based in part upon uutils coreutils package's src/lib/features/fs.rs which is:
//   (c) Joseph Crail <jbcrail@gmail.com>
//   (c) Jian Zeng <anonymousknight96 AT gmail.com>
// Tests base based in part upon gnulib packages' tests/test-canonicalize.c which is:
//   (c) Free Software Foundation, Inc.
//
// SPDX-License-Identifier: GPL-3.0-or-later

//! Set of functions to manage files and symlinks

use std::{
    collections::{HashSet, VecDeque},
    ffi::{CStr, OsStr, OsString},
    hash::Hash,
    os::unix::ffi::OsStrExt,
    path::{Component, Path, PathBuf},
};

use nix::{
    errno::Errno,
    fcntl::readlink,
    sys::stat::{lstat, stat, FileStat, Mode, SFlag},
    unistd::{access, AccessFlags, Pid},
    NixPath,
};
use serde::{ser::SerializeMap, Serialize, Serializer};

use crate::proc::{proc_fd, proc_tgid};

// Wrapper around FileStat to implement Serialize
pub(crate) struct FileStatWrapper(pub(crate) FileStat);

impl Serialize for FileStatWrapper {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let ftype = match SFlag::from_bits_truncate(self.0.st_mode) {
            SFlag::S_IFREG => Some("reg"),
            SFlag::S_IFDIR => Some("dir"),
            SFlag::S_IFLNK => Some("lnk"),
            SFlag::S_IFCHR => Some("chr"),
            SFlag::S_IFBLK => Some("blk"),
            SFlag::S_IFIFO => Some("fifo"),
            SFlag::S_IFSOCK => Some("sock"),
            _ => None,
        };
        let perm = format!("{:o}", Mode::from_bits_truncate(self.0.st_mode).bits());

        let mut map = serializer.serialize_map(Some(7))?;
        map.serialize_entry("dev", &self.0.st_dev)?;
        map.serialize_entry("ino", &self.0.st_ino)?;
        map.serialize_entry("type", &ftype)?;
        map.serialize_entry("perm", &perm)?;
        map.serialize_entry("uid", &self.0.st_uid)?;
        map.serialize_entry("gid", &self.0.st_gid)?;
        map.serialize_entry("size", &self.0.st_size)?;
        map.serialize_entry("mtime", &self.0.st_mtime)?;
        map.end()
    }
}

/// Information to uniquely identify a file
struct FileInformation(nix::sys::stat::FileStat);

impl FileInformation {
    /// Get information for a given path.
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Self, Errno> {
        stat(path.as_ref()).map(Self)
    }

    /// Get information for a given link.
    pub fn from_link<P: AsRef<Path>>(path: P) -> Result<Self, Errno> {
        lstat(path.as_ref()).map(Self)
    }

    /// Check if the path is a directory.
    pub fn is_dir(&self) -> bool {
        self.0.st_mode & SFlag::S_IFMT.bits() == SFlag::S_IFDIR.bits()
    }
}

impl PartialEq for FileInformation {
    fn eq(&self, other: &Self) -> bool {
        self.0.st_dev == other.0.st_dev && self.0.st_ino == other.0.st_ino
    }
}

impl Eq for FileInformation {}

impl Hash for FileInformation {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.st_dev.hash(state);
        self.0.st_ino.hash(state);
    }
}

/// Controls how missing components should be handled when canonicalizing a path.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum MissingHandling {
    /// Last component may exist, other components must exist.
    Normal,

    /// All components must exist.
    Existing,

    /// Last component must not exist, other componenets must exist.
    Missing,
}

impl Serialize for MissingHandling {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let variant_str = match *self {
            MissingHandling::Normal => "normal",
            MissingHandling::Existing => "exist",
            MissingHandling::Missing => "miss",
        };

        serializer.serialize_str(variant_str)
    }
}

/// Returns true if the given `Path` ends with a slash.
#[inline]
pub fn path_ends_with_slash<P: AsRef<Path>>(path: P) -> bool {
    path.as_ref().as_os_str().as_bytes().last() == Some(&b'/')
}

/// Resolve a symbolic link honouring magic proc links.
pub fn resolve_symlink<P: AsRef<Path>>(pid: Pid, path: P) -> Result<PathBuf, Errno> {
    // SAFETY: Handle /proc/self and /proc/thread-self magic links
    match path.as_ref().as_os_str().as_bytes() {
        b"/proc/self" => {
            let mut b = itoa::Buffer::new();
            Ok(PathBuf::from(b.format(pid.as_raw())))
        }
        b"/proc/thread-self" => {
            let mut b0 = itoa::Buffer::new();
            let mut b1 = itoa::Buffer::new();
            let mut p = PathBuf::from(b0.format(proc_tgid(pid)?.as_raw()));
            p.push("task");
            p.push(b1.format(pid.as_raw()));
            Ok(p)
        }
        _ => read_link(&path),
    }
}

/// Resolve a symbolic link.
pub fn read_link<P: AsRef<Path>>(path: P) -> Result<PathBuf, Errno> {
    readlink(path.as_ref()).map(PathBuf::from)
}

/// Checks if a given path ends with a dot component.
///
/// This function iterates through the bytes of the path from end to start,
/// and determines whether the last component before any slashes is a dot.
///
/// # Arguments
///
/// * `path`: A reference to a `Box<CStr>` representing the path.
///
/// # Returns
///
/// * `bool`: Returns `true` if the path ends with a dot component, otherwise `false`.
///
/// # Examples
///
/// ```ignore
/// use std::ffi::CString;
/// assert_eq!(path_ends_with_dot(&Box::new(CString::new("some/path/.").unwrap())), true);
/// assert_eq!(path_ends_with_dot(&Box::new(CString::new("some/path/foo").unwrap())), false);
/// assert_eq!(path_ends_with_dot(&Box::new(CString::new("some/path/./").unwrap())), true);
/// assert_eq!(path_ends_with_dot(&Box::new(CString::new("some/path/././././///").unwrap())), true);
/// ```
#[allow(clippy::arithmetic_side_effects)]
#[allow(clippy::if_same_then_else)]
#[inline]
pub(crate) fn path_ends_with_dot(path: &CStr) -> bool {
    let bytes = path.to_bytes();

    // Start from the end of the string and move backwards
    let mut index = bytes.len();
    if index == 0 {
        return false;
    }

    // Skip trailing slashes
    while index > 0 && bytes[index - 1] == b'/' {
        index -= 1;
    }

    // If the path is empty after removing trailing slashes, it does not end with a dot
    if index == 0 {
        return false;
    }

    // Check for '.' or '..'
    if bytes[index - 1] == b'.' {
        if index == 1 || bytes[index - 2] == b'/' {
            return true; // Matches '.' or '*/.'
        } else if index > 1 && bytes[index - 2] == b'.' && (index == 2 || bytes[index - 3] == b'/')
        {
            return true; // Matches '..' or '*/..'
        }
    }

    false
}

enum OwningComponent {
    RootDir,
    CurDir,
    ParentDir,
    Normal(OsString),
}

impl OwningComponent {
    fn as_os_str(&self) -> &OsStr {
        match self {
            Self::RootDir => Component::RootDir.as_os_str(),
            Self::CurDir => Component::CurDir.as_os_str(),
            Self::ParentDir => Component::ParentDir.as_os_str(),
            Self::Normal(s) => s.as_os_str(),
        }
    }
}

impl<'a> From<Component<'a>> for OwningComponent {
    fn from(comp: Component<'a>) -> Self {
        match comp {
            Component::RootDir => Self::RootDir,
            Component::CurDir => Self::CurDir,
            Component::ParentDir => Self::ParentDir,
            Component::Normal(s) => Self::Normal(s.to_os_string()),
            Component::Prefix(_) => unreachable!(),
        }
    }
}

/// Return the canonical, absolute form of a path
///
/// This function is a generalization of [`std::fs::canonicalize`] that
/// allows controlling how symbolic links are resolved and how to deal
/// with missing components. It returns the canonical, absolute form of
/// a path.
///
/// The `resolve` is a boolean parameter which controls whether
/// the last component should be resolved or not. Remaining components
/// are always resolved.
///
/// The `miss_mode` parameter controls how missing components are handled.
#[allow(clippy::cognitive_complexity)]
pub fn canonicalize<P: AsRef<Path>>(
    pid: Pid,
    path: P,
    mut resolve: bool,
    miss_mode: MissingHandling,
) -> Result<PathBuf, Errno> {
    let path = path.as_ref();
    let path = if path.is_empty() {
        return Err(Errno::ENOENT);
    } else if path.is_absolute() {
        path
    } else {
        // relative path passed to canonicalize is not supported.
        return Err(Errno::EINVAL);
    };

    // Symbolic Link Loop Detection.
    const SYMLINKS_TO_LOOK_FOR_LOOPS: i32 = 20;
    let mut followed_symlinks = 0;
    let mut visited_files: Option<HashSet<FileInformation>> = None;

    let has_to_be_directory = path_ends_with_slash(path);
    resolve = resolve || has_to_be_directory;
    let mut parts: VecDeque<OwningComponent> = path.components().map(|part| part.into()).collect();
    let mut result = PathBuf::new();

    while let Some(part) = parts.pop_front() {
        match part {
            OwningComponent::RootDir | OwningComponent::Normal(..) => {
                result.push(part.as_os_str());
            }
            OwningComponent::CurDir => {}
            OwningComponent::ParentDir => {
                result.pop();
            }
        }
        let should_resolve = if parts.is_empty() { resolve } else { true };
        if !should_resolve {
            continue;
        }

        match resolve_symlink(pid, &result) {
            Ok(link_path) => {
                // SAFETY: NO_MAGICLINKS
                if proc_fd(pid, &result)?.is_some() && link_path.is_relative() {
                    // pipe:42 socket:42 etc, special paths.
                    continue;
                }

                //// Symbolic Link Loop Detection.
                // SYMLINKS_TO_LOOK_FOR_LOOPS is much smaller than i32::MAX.
                #[allow(clippy::arithmetic_side_effects)]
                if followed_symlinks < SYMLINKS_TO_LOOK_FOR_LOOPS {
                    followed_symlinks += 1;
                } else {
                    // SAFETY: readlink() returned success, expect() is fine here.
                    #[allow(clippy::disallowed_methods)]
                    let stat = FileInformation::from_link(&result).expect("lstat");
                    if let Some(ref mut visited_files) = visited_files {
                        if !visited_files.insert(stat) {
                            return Err(Errno::ELOOP);
                        }
                    } else {
                        // Allocate the HashSet only when it's really necessary.
                        visited_files = Some(HashSet::from([stat]));
                    }
                }
                ////

                for link_part in link_path.components().rev() {
                    parts.push_front(link_part.into());
                }
                result.pop();
            }
            Err(Errno::EINVAL) => {
                // File is not a symbolic link, continue.
            }
            Err(error) => {
                if miss_mode == MissingHandling::Existing
                    || (miss_mode == MissingHandling::Normal && !parts.is_empty())
                {
                    return Err(error);
                }
            }
        }
    }

    // Raise ENOTDIR if required.
    match miss_mode {
        MissingHandling::Normal => {
            if has_to_be_directory {
                let r = if resolve {
                    FileInformation::from_path(&result)
                } else {
                    FileInformation::from_link(&result)
                };

                if let Ok(info) = r {
                    if !info.is_dir() {
                        return Err(Errno::ENOTDIR);
                    }
                }
            }
        }
        MissingHandling::Existing => {
            match if resolve {
                FileInformation::from_path(&result)
            } else {
                FileInformation::from_link(&result)
            } {
                Ok(info) if has_to_be_directory && !info.is_dir() => return Err(Errno::ENOTDIR),
                Err(error) => return Err(error), // Last component must exist, but does not.
                _ => {}
            }
        }
        MissingHandling::Missing => {
            if access(&result, AccessFlags::F_OK).is_ok() {
                return Err(Errno::EEXIST);
            }
        }
    }

    // Preserve trailing slash as necessary
    if has_to_be_directory {
        result.push("");
    }

    Ok(result)
}

#[cfg(test)]
mod tests {
    use std::{
        ffi::CString,
        fs::{self, OpenOptions},
        os::{
            fd::AsRawFd,
            unix::fs::{symlink, OpenOptionsExt},
        },
        process::Command,
        thread::sleep,
        time::{Duration, SystemTime},
    };

    use nix::unistd::getpid;

    use super::{MissingHandling::*, *};

    type TestResult = Result<(), Box<dyn std::error::Error>>;

    // A helper function to get the current atime of a file
    fn get_atime<P: AsRef<Path>>(path: P) -> SystemTime {
        let metadata = fs::metadata(path).expect("Failed to get metadata");
        metadata.accessed().expect("Failed to get accessed time")
    }

    // Helper function to assert that the atime of a file or directory has not changed
    fn assert_atime_unchanged<P: AsRef<Path>, F>(path: P, func: F)
    where
        F: FnOnce() -> Result<PathBuf, Errno>,
    {
        let original_atime_f = get_atime(&path);
        let original_atime_p = get_atime(path.as_ref().parent().unwrap());
        sleep(Duration::from_secs(7));
        assert!(
            func().is_ok(),
            "canonicalize {} failed",
            path.as_ref().display()
        );
        // We allow a 1-second tolerance since some filesystems do not have nanosecond precision.
        let new_atime_f = get_atime(&path);
        let new_atime_p = get_atime(path.as_ref().parent().unwrap());
        assert!(
            new_atime_f <= original_atime_f + Duration::new(1, 0),
            "The atime of the file should not have significantly changed."
        );
        assert!(
            new_atime_p <= original_atime_p + Duration::new(1, 0),
            "The atime of the parent dir should not have significantly changed."
        );
    }

    // std::fs::remove_dir_all stops on the first error.
    // we need something more forceful.
    fn remove_dir_all<P: AsRef<Path>>(path: P) -> std::io::Result<()> {
        let status = Command::new("rm")
            .arg("-rf")
            .arg(path.as_ref().to_string_lossy().to_string())
            .status()?;
        if status.success() {
            Ok(())
        } else {
            Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!("Failed to remove directory: {}", path.as_ref().display()),
            ))
        }
    }

    // Helper function to create a symlink chain that eventually loops back to an earlier link
    fn setup_symlink_loop(tmp_dir: &tempfile::TempDir, links: &[(&str, &str)]) {
        for &(src, dst) in links {
            let src_path = tmp_dir.path().join(src);

            // Check and create parent directory for the source if necessary
            if let Some(parent) = src_path.parent() {
                if !parent.exists() {
                    fs::create_dir_all(parent).unwrap();
                }
            }

            // The destination is given relative to the source
            let dst_path = Path::new(dst);

            // Avoid creating a symlink if the source already exists
            if src_path.exists() {
                fs::remove_file(&src_path).unwrap();
            }

            // If the destination is an absolute path or starts with '/', we strip the '/' and prefix with tmp_dir
            let full_dst_path = if dst_path.is_absolute() {
                tmp_dir.path().join(dst_path.strip_prefix("/").unwrap())
            } else {
                src_path.parent().unwrap().join(dst_path)
            };

            // Create parent directories for the destination if they don't exist
            if let Some(parent) = full_dst_path.parent() {
                if !parent.exists() {
                    fs::create_dir_all(parent).unwrap();
                }
            }

            // Create the symlink
            symlink(&full_dst_path, &src_path).expect(&format!(
                "Unable to symlink {} -> {}",
                src_path.display(),
                full_dst_path.display()
            ));
        }
    }

    struct EndsWithDotTestCase<'a> {
        path: &'a str,
        test: bool,
    }

    const ENDS_WITH_DOT_TESTS: [EndsWithDotTestCase; 10] = [
        EndsWithDotTestCase {
            path: ".",
            test: true,
        },
        EndsWithDotTestCase {
            path: "..",
            test: true,
        },
        EndsWithDotTestCase {
            path: "...",
            test: false,
        },
        EndsWithDotTestCase {
            path: "/.",
            test: true,
        },
        EndsWithDotTestCase {
            path: "/..",
            test: true,
        },
        EndsWithDotTestCase {
            path: "/...",
            test: false,
        },
        EndsWithDotTestCase {
            path: "foo.",
            test: false,
        },
        EndsWithDotTestCase {
            path: "foo./.",
            test: true,
        },
        EndsWithDotTestCase {
            path: "foo/./././/./",
            test: true,
        },
        EndsWithDotTestCase {
            path: "conftest.dir/././././////",
            test: true,
        },
    ];

    fn tempdir() -> Result<PathBuf, Box<dyn std::error::Error>> {
        let path = Path::new(".syd-test");
        std::fs::create_dir_all(path)?;
        let _ = OpenOptions::new()
            .write(true)
            .create(true)
            .mode(0o600)
            .open(path.join("test"))?;
        Ok(path.to_path_buf())
    }

    #[test]
    fn test_ends_with_dot() {
        for (idx, test) in ENDS_WITH_DOT_TESTS.iter().enumerate() {
            let path = CString::new(test.path).unwrap();
            let ends = path_ends_with_dot(&path);
            assert_eq!(
                test.test, ends,
                "EndsWithDotTestCase {} -> \"{}\": {} != {}",
                idx, test.path, test.test, ends
            );
        }
    }

    #[test]
    fn test_canonicalize_empty_path() -> TestResult {
        assert_eq!(
            canonicalize(Pid::this(), "", true, Normal),
            Err(Errno::ENOENT)
        );
        assert_eq!(
            canonicalize(Pid::this(), "", true, Existing),
            Err(Errno::ENOENT)
        );
        assert_eq!(
            canonicalize(Pid::this(), "", true, Missing),
            Err(Errno::ENOENT)
        );
        assert_eq!(
            canonicalize(Pid::this(), "", false, Normal),
            Err(Errno::ENOENT)
        );
        assert_eq!(
            canonicalize(Pid::this(), "", false, Existing),
            Err(Errno::ENOENT)
        );
        assert_eq!(
            canonicalize(Pid::this(), "", false, Missing),
            Err(Errno::ENOENT)
        );

        Ok(())
    }

    #[test]
    fn test_canonicalize_repetitive_slashes() -> TestResult {
        let result_test = canonicalize(Pid::this(), "/etc/passwd", true, Normal)?;
        let paths = vec![
            "/etc/passwd",
            "/etc//passwd",
            "/etc///passwd",
            "//etc/passwd",
            "//etc//passwd",
            "//etc///passwd",
            "///etc/passwd",
            "///etc//passwd",
            "///etc///passwd",
        ];
        for path in &paths {
            let result = canonicalize(Pid::this(), path, true, Normal)?;
            assert_eq!(result, result_test);
        }

        Ok(())
    }

    #[test]
    fn test_canonicalize_dots_slashes() -> TestResult {
        let base = tempdir()?;

        let cwd = std::env::current_dir()?.display().to_string();
        let path = base.display().to_string();

        let result1 = canonicalize(
            Pid::this(),
            format!("{cwd}/{path}//./..//{path}/test"),
            true,
            Normal,
        )?
        .display()
        .to_string();
        let result2 = canonicalize(
            Pid::this(),
            format!("{cwd}/{path}//./..//{path}/test"),
            true,
            Existing,
        )?
        .display()
        .to_string();

        assert!(!result1.is_empty(), "result:{result1}");
        assert!(!result2.is_empty(), "result:{result2}");
        assert_eq!(result1, result2);

        Ok(())
    }

    #[test]
    fn test_canonicalize_non_directory_with_slash() -> TestResult {
        let cwd = std::env::current_dir()?.display().to_string();
        let path = tempdir()?.display().to_string();
        let test = format!("{cwd}/{path}/test/");

        assert_eq!(
            canonicalize(Pid::this(), &test, true, Normal),
            Err(Errno::ENOTDIR)
        );
        assert_eq!(
            canonicalize(Pid::this(), &test, true, Existing),
            Err(Errno::ENOTDIR)
        );
        assert_eq!(
            canonicalize(Pid::this(), &test, true, Missing),
            Err(Errno::EEXIST)
        );
        assert_eq!(
            canonicalize(Pid::this(), &test, false, Normal),
            Err(Errno::ENOTDIR)
        );
        assert_eq!(
            canonicalize(Pid::this(), &test, false, Existing),
            Err(Errno::ENOTDIR)
        );
        assert_eq!(
            canonicalize(Pid::this(), &test, false, Missing),
            Err(Errno::EEXIST)
        );

        Ok(())
    }

    /// FIXME: The asserts return success rather than failure.
    /// Bug or feature?
    #[test]
    #[ignore]
    fn test_canonicalize_missing_directory_returns_enoent() -> TestResult {
        assert_eq!(
            canonicalize(Pid::this(), "/zzz/..", true, Normal),
            Err(Errno::ENOENT)
        );
        assert_eq!(
            canonicalize(Pid::this(), "/zzz/..", true, Existing),
            Err(Errno::ENOENT)
        );
        assert_eq!(
            canonicalize(Pid::this(), "/zzz/..", false, Normal),
            Err(Errno::ENOENT)
        );
        assert_eq!(
            canonicalize(Pid::this(), "/zzz/..", false, Existing),
            Err(Errno::ENOENT)
        );

        Ok(())
    }

    #[test]
    fn test_relative_symlink_resolution() -> TestResult {
        // Setup
        let root_test_dir = Path::new("test_root_relative_symlink_resolution");
        let deep_dir = root_test_dir.join("a/b/c");
        let _ = remove_dir_all(&root_test_dir);
        fs::create_dir_all(&root_test_dir.join("d"))?;
        fs::create_dir_all(&deep_dir)?;

        // Create a symlink in "b" that points upwards to "a"
        let rel_link = root_test_dir.join("a/b/rel_link");
        symlink("../..", &rel_link)?;

        // Append /proc/self/cwd to get an absolute path to our symlinked path
        let abs_link_path = Path::new("/proc/self/cwd").join(root_test_dir.join("a/b/rel_link/d"));

        // Call canonicalize
        let result = canonicalize(Pid::this(), &abs_link_path, true, MissingHandling::Existing);
        assert!(
            matches!(result, Ok(_)),
            "canonicalize:{} result:{:?}",
            abs_link_path.display(),
            result
        );
        let resolved_path = result.unwrap();

        // We expect the path to be resolved to "test_root/a/d", but we need to canonicalize it
        let expected_path =
            fs::canonicalize(Path::new("/proc/self/cwd").join(root_test_dir.join("d")))?;

        // Cleanup
        let _ = remove_dir_all(&root_test_dir);

        assert_eq!(resolved_path, expected_path);

        Ok(())
    }

    // FIXME: This test broke after we removed normalize()
    // The question: Is the test incorrect or is canonicalize()?
    #[ignore]
    #[test]
    fn test_complex_interplay_symlinks_dots() -> TestResult {
        // Setup
        let cwd = Path::new("/proc/self/cwd").canonicalize()?;
        let root_test_dir = cwd.join("test_root_complex_interplay_symlinks_dots");
        let _ = remove_dir_all(&root_test_dir);
        fs::create_dir_all(root_test_dir.join("a/b/c")).unwrap();
        fs::create_dir(root_test_dir.join("d")).unwrap();
        fs::create_dir(root_test_dir.join("e")).unwrap();
        fs::create_dir(root_test_dir.join("x")).unwrap();

        // Create several symlinks
        symlink("./a", root_test_dir.join("link_to_a")).unwrap();
        symlink("e", root_test_dir.join("link_to_e")).unwrap();
        symlink("a/b", root_test_dir.join("link_to_b")).unwrap();
        symlink("../../x", root_test_dir.join("a/b/rel_link")).unwrap();

        let path = root_test_dir.join("link_to_a/../link_to_b/rel_link/../..");
        let resolved_path =
            canonicalize(Pid::this(), path, true, MissingHandling::Existing).unwrap();

        // Cleanup
        let _ = remove_dir_all(&root_test_dir);

        // Assertion
        assert_eq!(resolved_path, root_test_dir);

        Ok(())
    }

    #[test]
    fn test_trailing_slash_handling() -> TestResult {
        let path = Path::new("/usr/");
        let pabs = canonicalize(Pid::this(), &path, true, MissingHandling::Normal).unwrap();
        assert_eq!(path, pabs);

        let path = Path::new("/proc/self/");
        let pexp = format!("/proc/{}/", getpid());
        let pexp = Path::new(&pexp);
        let pabs = canonicalize(Pid::this(), &path, true, MissingHandling::Normal).unwrap();
        assert_eq!(pabs, pexp);
        let pabs = canonicalize(Pid::this(), &path, false, MissingHandling::Normal).unwrap();
        assert_eq!(pabs, pexp);

        Ok(())
    }

    #[ignore]
    #[test]
    fn test_canonicalize_no_atime_change_normal() -> TestResult {
        let cdir = std::env::current_dir()?;
        let base = cdir.join(tempdir()?);
        let path = base.join("file");
        fs::File::create(&path)?;

        assert_atime_unchanged(&path, || {
            canonicalize(Pid::this(), &path, true, MissingHandling::Normal)
        });

        let _ = remove_dir_all(&base);
        Ok(())
    }

    #[ignore]
    #[test]
    fn test_canonicalize_no_atime_change_existing() -> TestResult {
        let cdir = std::env::current_dir()?;
        let base = cdir.join(tempdir()?);
        let path = base.join("file");
        fs::File::create(&path)?;

        assert_atime_unchanged(&path, || {
            canonicalize(Pid::this(), &path, true, MissingHandling::Existing)
        });

        let _ = remove_dir_all(&base);
        Ok(())
    }

    #[test]
    fn test_canonicalize_symlink_loop() {
        let tmp_dir = tempfile::tempdir().expect("Failed to create temp dir");
        let dir_path = tmp_dir.path();

        // Create a symlink loop: link_a -> link_b -> link_a
        let mut link_a = dir_path.join("link_a");
        let mut link_b = dir_path.join("link_b");
        symlink(&link_b, &link_a).expect("Failed to create symlink a");
        symlink(&link_a, &link_b).expect("Failed to create symlink b");

        // Now check that canonicalize detects the loop correctly
        let result = canonicalize(Pid::this(), &link_a, false, MissingHandling::Normal);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &link_a, true, MissingHandling::Normal);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &link_a, false, MissingHandling::Existing);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &link_a, true, MissingHandling::Existing);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &link_a, false, MissingHandling::Missing);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &link_a, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        // Add a trailing slash and retest.
        link_a.push("");

        let result = canonicalize(Pid::this(), &link_a, false, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &link_a, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        // Add a trailing slash and retest.
        link_b.push("");

        let result = canonicalize(Pid::this(), &link_b, false, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &link_b, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));
    }

    #[test]
    fn test_canonicalize_nonexistent_final_component() {
        let tmp_dir = tempfile::tempdir().expect("Failed to create temp dir");
        let dir_path = tmp_dir.path();

        // Create a valid symlink to a non-existent final component
        let mut valid_link = dir_path.join("valid_link");
        let nonexistent_target = dir_path.join("nonexistent");
        symlink(&nonexistent_target, &valid_link)
            .expect("Failed to create symlink to non-existent target");

        // Now check that canonicalize handles the non-existent final component correctly
        let result = canonicalize(Pid::this(), &valid_link, false, MissingHandling::Normal);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &valid_link, true, MissingHandling::Normal);
        // FIXME: assert_eq!(result, Err(Errno::ENOENT));
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &valid_link, false, MissingHandling::Existing);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &valid_link, true, MissingHandling::Existing);
        assert_eq!(result, Err(Errno::ENOENT));

        let result = canonicalize(Pid::this(), &valid_link, false, MissingHandling::Missing);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &valid_link, true, MissingHandling::Missing);
        assert!(result.is_ok(), "{result:?}");

        // Add a trailing slash and retest.
        valid_link.push("");

        let result = canonicalize(Pid::this(), &valid_link, false, MissingHandling::Missing);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &valid_link, true, MissingHandling::Missing);
        assert!(result.is_ok(), "{result:?}");
    }

    #[test]
    fn test_canonicalize_self_referential_symlink() {
        let tmp_dir = tempfile::tempdir().expect("Failed to create temp dir");
        let mut symlink_path = tmp_dir.path().join("self_link");
        symlink(&symlink_path, &symlink_path).expect("Failed to create self-referential symlink");

        let result = canonicalize(Pid::this(), &symlink_path, false, MissingHandling::Normal);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &symlink_path, true, MissingHandling::Normal);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &symlink_path, false, MissingHandling::Existing);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &symlink_path, true, MissingHandling::Existing);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &symlink_path, false, MissingHandling::Missing);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &symlink_path, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        // Add a trailing slash and retest.
        symlink_path.push("");

        let result = canonicalize(Pid::this(), &symlink_path, false, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &symlink_path, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));
    }

    #[test]
    fn test_canonicalize_broken_symlink() {
        let tmp_dir = tempfile::tempdir().expect("Failed to create temp dir");
        let mut broken_link = tmp_dir.path().join("broken_link");
        let nonexistent_target = tmp_dir.path().join("nonexistent_target");
        symlink(&nonexistent_target, &broken_link).expect("Failed to create broken symlink");

        let result = canonicalize(Pid::this(), &broken_link, false, MissingHandling::Normal);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &broken_link, true, MissingHandling::Normal);
        // FIXME: assert_eq!(result, Err(Errno::ENOENT));
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &broken_link, false, MissingHandling::Existing);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &broken_link, true, MissingHandling::Existing);
        assert_eq!(result, Err(Errno::ENOENT));

        let result = canonicalize(Pid::this(), &broken_link, false, MissingHandling::Missing);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &broken_link, true, MissingHandling::Missing);
        assert!(result.is_ok(), "{result:?}");

        // Add a trailing slash and retest.
        broken_link.push("");

        let result = canonicalize(Pid::this(), &broken_link, false, MissingHandling::Missing);
        assert!(result.is_ok(), "{result:?}");

        let result = canonicalize(Pid::this(), &broken_link, true, MissingHandling::Missing);
        assert!(result.is_ok(), "{result:?}");
    }

    #[test]
    fn test_canonicalize_symlink_to_directory() {
        let tmp_dir = tempfile::tempdir().expect("Failed to create temp dir");
        let dir = tmp_dir.path().join("dir");
        fs::create_dir(&dir).expect("Failed to create directory");

        let symlink_path = tmp_dir.path().join("dir_link");
        symlink(&dir, &symlink_path).expect("Failed to create symlink to directory");

        let result = canonicalize(Pid::this(), &symlink_path, false, MissingHandling::Normal);
        assert!(result.is_ok(), "{result:?}");
        assert!(result.clone().unwrap().is_symlink(), "{result:?}");

        let result = canonicalize(Pid::this(), &symlink_path, true, MissingHandling::Normal);
        assert!(result.is_ok(), "{result:?}");
        assert!(result.clone().unwrap().is_dir(), "{result:?}");

        let result = canonicalize(Pid::this(), &symlink_path, false, MissingHandling::Existing);
        assert!(result.is_ok(), "{result:?}");
        assert!(result.clone().unwrap().is_symlink(), "{result:?}");

        let result = canonicalize(Pid::this(), &symlink_path, true, MissingHandling::Existing);
        assert!(result.is_ok(), "{result:?}");
        assert!(result.clone().unwrap().is_dir(), "{result:?}");

        let result = canonicalize(Pid::this(), &symlink_path, false, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::EEXIST));

        let result = canonicalize(Pid::this(), &symlink_path, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::EEXIST));
    }

    #[test]
    fn test_canonicalize_symlink_chain() {
        let tmp_dir = tempfile::tempdir().expect("Failed to create temp dir");
        let link1 = tmp_dir.path().join("link1");
        let link2 = tmp_dir.path().join("link2");
        let link3 = tmp_dir.path().join("link3");
        let file = tmp_dir.path().join("file");
        fs::write(&file, "content").expect("Failed to write file");

        // Create a chain of symlinks: link1 -> link2 -> link3 -> file
        symlink(&link2, &link1).expect("Failed to create link1");
        symlink(&link3, &link2).expect("Failed to create link2");
        symlink(&file, &link3).expect("Failed to create link3");

        let result = canonicalize(Pid::this(), &link1, false, MissingHandling::Normal);
        assert!(result.is_ok(), "{result:?}");
        assert!(result.clone().unwrap().is_symlink(), "{result:?}");

        let result = canonicalize(Pid::this(), &link1, true, MissingHandling::Normal);
        assert!(result.is_ok(), "{result:?}");
        assert!(result.clone().unwrap().is_file(), "{result:?}");

        let result = canonicalize(Pid::this(), &link1, false, MissingHandling::Existing);
        assert!(result.is_ok(), "{result:?}");
        assert!(result.clone().unwrap().is_symlink(), "{result:?}");

        let result = canonicalize(Pid::this(), &link1, true, MissingHandling::Existing);
        assert!(result.is_ok(), "{result:?}");
        assert!(result.clone().unwrap().is_file(), "{result:?}");

        let result = canonicalize(Pid::this(), &link1, false, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::EEXIST));

        let result = canonicalize(Pid::this(), &link1, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::EEXIST));
    }

    #[test]
    fn test_canonicalize_complex_symlink_loop_with_intermediate_components() {
        let tmp_dir = tempfile::tempdir().expect("Failed to create temp dir");

        // Setting up a complex symlink scenario
        setup_symlink_loop(
            &tmp_dir,
            &[("a", "b/c"), ("b/c", "d"), ("b/d", "../e"), ("e", "f/../a")],
        );

        let mut path = tmp_dir.path().join("a");

        let result = canonicalize(Pid::this(), &path, false, MissingHandling::Normal);
        assert!(result.is_ok(), "{result:?}");
        assert!(result.clone().unwrap().is_symlink(), "{result:?}");

        let result = canonicalize(Pid::this(), &path, true, MissingHandling::Normal);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &path, false, MissingHandling::Existing);
        assert!(result.is_ok(), "{result:?}");
        assert!(result.clone().unwrap().is_symlink(), "{result:?}");

        let result = canonicalize(Pid::this(), &path, true, MissingHandling::Existing);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &path, false, MissingHandling::Missing);
        assert!(result.is_ok(), "{result:?}");
        assert!(result.clone().unwrap().is_symlink(), "{result:?}");

        let result = canonicalize(Pid::this(), &path, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        // Add a trailing slash and retest.
        path.push("");

        let result = canonicalize(Pid::this(), &path, false, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &path, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        // Add a final component and retest.
        path.push("foo");

        let result = canonicalize(Pid::this(), &path, false, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &path, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));
    }

    #[test]
    fn test_canonicalize_symlinks_with_dot_and_dotdot_components() {
        let tmp_dir = tempfile::tempdir().expect("Failed to create temp dir");

        // Create a complex directory structure with dots and symlinks
        fs::create_dir_all(tmp_dir.path().join("b")).expect("Failed to create directory b");
        symlink("b", tmp_dir.path().join("a")).expect("Failed to create symlink a -> b");
        symlink("..///e", tmp_dir.path().join("b").join("d"))
            .expect("Failed to create symlink b/d -> ../e");
        symlink("b/.///./d", tmp_dir.path().join("e")).expect("Failed to create symlink e -> b/d");

        let mut path = tmp_dir.path().join("a").join(".").join("d");

        let result = canonicalize(Pid::this(), &path, false, MissingHandling::Normal);
        assert!(result.is_ok(), "{result:?}");
        assert!(!result.clone().unwrap().exists(), "{result:?}");

        let result = canonicalize(Pid::this(), &path, true, MissingHandling::Normal);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &path, false, MissingHandling::Existing);
        assert!(result.is_ok(), "{result:?}");
        assert!(!result.clone().unwrap().exists(), "{result:?}");

        let result = canonicalize(Pid::this(), &path, true, MissingHandling::Existing);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &path, false, MissingHandling::Missing);
        assert!(result.is_ok(), "{result:?}");
        assert!(!result.clone().unwrap().exists(), "{result:?}");

        let result = canonicalize(Pid::this(), &path, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        // Add a trailing slash and retest.
        path.push("");

        let result = canonicalize(Pid::this(), &path, false, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &path, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        // Add a final component and retest.
        path.push("foo");

        let result = canonicalize(Pid::this(), &path, false, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));

        let result = canonicalize(Pid::this(), &path, true, MissingHandling::Missing);
        assert_eq!(result, Err(Errno::ELOOP));
    }

    #[test]
    fn test_canonicalize_proc_self() -> TestResult {
        let path = canonicalize(Pid::this(), "/proc/self", false, MissingHandling::Normal)?;
        assert_eq!(path, PathBuf::from("/proc/self"));

        Ok(())
    }

    #[test]
    fn test_canon_glob_std() -> TestResult {
        assert!(canonicalize(
            Pid::this(),
            "/proc/self/fd/0",
            true,
            MissingHandling::Normal
        )
        .is_ok());
        assert!(canonicalize(
            Pid::this(),
            "/proc/self/fd/1",
            true,
            MissingHandling::Normal
        )
        .is_ok());
        assert!(canonicalize(
            Pid::this(),
            "/proc/self/fd/2",
            true,
            MissingHandling::Normal
        )
        .is_ok());
        assert!(canonicalize(
            Pid::this(),
            "/proc/self/fd/0",
            true,
            MissingHandling::Existing
        )
        .is_ok());
        assert!(canonicalize(
            Pid::this(),
            "/proc/self/fd/1",
            true,
            MissingHandling::Existing
        )
        .is_ok());
        assert!(canonicalize(
            Pid::this(),
            "/proc/self/fd/2",
            true,
            MissingHandling::Existing
        )
        .is_ok());
        assert_eq!(
            canonicalize(
                Pid::this(),
                "/proc/self/fd/0",
                true,
                MissingHandling::Missing
            ),
            Err(Errno::EEXIST)
        );
        assert_eq!(
            canonicalize(
                Pid::this(),
                "/proc/self/fd/1",
                true,
                MissingHandling::Missing
            ),
            Err(Errno::EEXIST)
        );
        assert_eq!(
            canonicalize(
                Pid::this(),
                "/proc/self/fd/2",
                true,
                MissingHandling::Missing
            ),
            Err(Errno::EEXIST)
        );
        Ok(())
    }

    #[test]
    fn test_canon_glob_pipe() -> TestResult {
        let (read_end, write_end) = std::os::unix::net::UnixStream::pair()?;

        let fd = read_end.as_raw_fd();
        let path = format!("/proc/self/fd/{fd}");
        assert!(canonicalize(Pid::this(), &path, true, MissingHandling::Normal).is_ok());

        let fd = write_end.as_raw_fd();
        let path = format!("/proc/self/fd/{fd}");
        assert!(canonicalize(Pid::this(), &path, true, MissingHandling::Normal).is_ok());

        Ok(())
    }
}
