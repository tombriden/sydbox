//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/lib.rs: Common utility functions
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

//! # syd: The ☮ther SⒶndbøx
//!
//! [![Shine On You Crazy Diamond!](https://img.shields.io/badge/Shine%20On%20You%20Crazy%20Diamond!-8A2BE2)](https://en.wikipedia.org/wiki/Syd_Barrett)
//! [![license](https://img.shields.io/crates/l/jja.svg)](https://git.sr.ht/~alip/syd/tree/main/item/COPYING)
//! [![msrv](https://img.shields.io/badge/rustc-1.70%2B-green?style=plastic)](https://blog.rust-lang.org/2023/06/01/Rust-1.70.0.html)
//! [![build status](https://builds.sr.ht/~alip/syd.svg)](https://builds.sr.ht/~alip/syd?)
//! [![maintenance-status](https://img.shields.io/badge/maintenance-actively--developed-brightgreen.svg)](https://git.sr.ht/~alip/syd)
//! [![dependency status](https://deps.rs/repo/sourcehut/~alip/syd/status.svg)](https://deps.rs/repo/sourcehut/~alip/syd)
//! [![repology](https://repology.org/badge/latest-versions/syd.svg)](https://repology.org/project/syd/versions)
//!
//! [![syd](https://git.sr.ht/~alip/syd/blob/main/data/syd.png)](https://todo.sr.ht/~alip/syd)
//! [![GNU](https://web.archive.org/web/20221222061733if_/https://dev.exherbo.org/~alip/images/gnu.png)](https://www.gnu.org/philosophy/philosophy.html)
//! [![Linux](https://chesswob.org/jja/tux.png)](https://www.kernel.org/category/about.html)
//! [![Exherbo](https://web.archive.org/web/20230518155203if_/https://dev.exherbo.org/~alip/images/zebrapig.png)](https://www.exherbolinux.org/docs/gettingstarted.html)
//! [![musl libc](https://www.chesswob.org/jja/musl-inside.png)](https://www.musl-libc.org/)
//! [![libsecc☮mp](https://web.archive.org/web/20221222061720if_/https://dev.exherbo.org/~alip/images/libseccomp.png)](https://github.com/seccomp/libseccomp)
//! [![Paludis](http://paludis.exherbolinux.org/paludis_270.png)](https://paludis.exherbolinux.org)
//!
//! syd is a **seccomp**(2) based sandboxing utility for modern Linux\[\>=5.6\]
//! machines to sandbox unwanted process access to filesystem and network resources.
//! syd requires *no root access* and *no ptrace* rights. All you need is a
//! recent Linux kernel and libsecc☮mp which is available on many different
//! architectures, including **x86**, **x86\_64**, **x32**, **arm**, **aarch64**,
//! **mips**, **mips64**... This makes it very easy for a regular user to use. This is
//! the motto of syd: *bring easy, simple, flexible and powerful access restriction
//! to the Linux user!*
//!
//! The basic idea of syd is to run a command under certain restrictions. These
//! restrictions define which system calls the command is permitted to run and which
//! argument values are permitted for the given system call. The restrictions may be
//! applied via two ways.  *seccomp-bpf* can be used to apply simple Secure Computing
//! user filters to run sandboxing fully on kernel space, and *seccomp-notify*
//! functionality can be used to run sandboxing on kernel space and fallback to user
//! space to dereference pointer arguments of system calls (**See
//! [Security](#security) about `TOCTOU` et. al**), which are one of
//! **[pathname](https://en.wikipedia.org/wiki/Path_(computing))**, **[UNIX socket
//! address](https://en.wikipedia.org/wiki/Unix_domain_socket)**,
//! **[IPv4](https://en.wikipedia.org/wiki/IPv4)** or
//! **[IPv6](https://en.wikipedia.org/wiki/IPv6)** network address, and make dynamic
//! decisions using [Unix shell style patterns](https://docs.rs/globset) such as
//! `allow/write+/home/syd/***`, or `allow/write+/run/user/*/pulse` for
//! **[pathnames](https://en.wikipedia.org/wiki/Path_(computing))**, and using
//! **[CIDR](https://docs.rs/ipnetwork)** notation such as
//! `allow/net/connect+127.0.0.1/8!9050`, or
//! `allow/net/connect+::1/8!9050` for
//! **[IPv4](https://en.wikipedia.org/wiki/IPv4)** and
//! **[IPv6](https://en.wikipedia.org/wiki/IPv6)** addresses and perform an action
//! which is by default denying the system call with an appropriate error, which is
//! usually **access denied**, aka `EACCES`. For default disallowed system calls,
//! such as `ptrace` or `process_vm_writev` (**See [Security](#security) about
//! `TOCTOU` et. al**) syd returns `EACCES` as well.
//!
//! To be able to use syd, you need a recent Linux kernel with the system calls
//! **pidfd_getfd**, **pidfd_send_signal**. The Secure Computing facility of the
//! Linux kernel should support the **SECCOMP_USER_NOTIF_FLAG_CONTINUE** operation.
//! It is recommended to have the **CONFIG_CROSS_MEMORY_ATTACH** kernel option
//! enabled, if this option is not enabled, syd will fallback to reading/writing
//! from `/proc/$pid/mem`. Linux-5.11 or later is recommended.

// We like clean and simple code with documentation.
// Keep in sync with main.rs.
#![deny(missing_docs)]
#![deny(clippy::allow_attributes_without_reason)]
#![deny(clippy::arithmetic_side_effects)]
#![deny(clippy::as_ptr_cast_mut)]
#![deny(clippy::as_underscore)]
#![deny(clippy::assertions_on_result_states)]
#![deny(clippy::borrow_as_ptr)]
#![deny(clippy::branches_sharing_code)]
#![deny(clippy::case_sensitive_file_extension_comparisons)]
#![deny(clippy::cast_lossless)]
#![deny(clippy::cast_possible_truncation)]
#![deny(clippy::cast_possible_wrap)]
#![deny(clippy::cast_precision_loss)]
#![deny(clippy::cast_ptr_alignment)]
#![deny(clippy::cast_sign_loss)]
#![deny(clippy::checked_conversions)]
#![deny(clippy::clear_with_drain)]
#![deny(clippy::clone_on_ref_ptr)]
#![deny(clippy::cloned_instead_of_copied)]
#![deny(clippy::cognitive_complexity)]
#![deny(clippy::collection_is_never_read)]
#![deny(clippy::copy_iterator)]
#![deny(clippy::create_dir)]
#![deny(clippy::dbg_macro)]
#![deny(clippy::debug_assert_with_mut_call)]
#![deny(clippy::decimal_literal_representation)]
#![deny(clippy::default_trait_access)]
#![deny(clippy::default_union_representation)]
#![deny(clippy::derive_partial_eq_without_eq)]
#![deny(clippy::doc_link_with_quotes)]
//#![deny(clippy::doc_markdown)]
#![deny(clippy::explicit_into_iter_loop)]
#![deny(clippy::explicit_iter_loop)]
#![deny(clippy::fallible_impl_from)]
#![deny(clippy::missing_safety_doc)]
#![deny(clippy::undocumented_unsafe_blocks)]

/// Compatibility code for different libcs
pub(crate) mod compat;
/// Static configuration, edit & recompile!
pub mod config;
/// Filesystem utilities
pub mod fs;
/// Utilities for hashing
pub mod hash;
/// Secure computing hooks
#[allow(clippy::as_ptr_cast_mut)]
#[allow(clippy::cast_sign_loss)]
#[allow(clippy::undocumented_unsafe_blocks)]
pub mod hook;
/// Simple logging on standard error using JSON lines
pub mod log;
/// /proc utilities
pub mod proc;
/// Sandbox configuration
pub mod sandbox;

// Vendored crates:
/// Interface to Linux capabilities
#[allow(dead_code)]
#[allow(missing_docs)]
#[allow(clippy::arithmetic_side_effects)]
#[allow(clippy::cast_possible_truncation)]
#[allow(clippy::missing_safety_doc)]
#[allow(clippy::undocumented_unsafe_blocks)]
pub mod caps;
/// Interface to LandLock LSM
#[allow(dead_code)]
#[allow(missing_docs)]
#[allow(clippy::as_underscore)]
#[allow(clippy::cast_possible_truncation)]
#[allow(clippy::decimal_literal_representation)]
#[allow(clippy::missing_safety_doc)]
#[allow(clippy::type_complexity)]
#[allow(clippy::undocumented_unsafe_blocks)]
pub mod landlock;
/// The low-level interface for linux namespaces (containers)
pub mod unshare;

use std::{
    ffi::CStr,
    io::Result as IOResult,
    os::{
        fd::{AsRawFd, FromRawFd, OwnedFd, RawFd},
        unix::ffi::OsStrExt,
    },
    path::Path,
    process::exit,
};

use libseccomp::{
    error::SeccompErrno, ScmpAction, ScmpArch, ScmpFilterContext, ScmpNotifData, ScmpNotifReq,
    ScmpNotifResp, ScmpSyscall,
};
use nix::{
    errno::Errno,
    sys::{
        signal::{sigaction, SaFlags, SigAction, SigHandler, SigSet, Signal},
        socket::{socket, AddressFamily, SockFlag, SockType},
        wait::{waitpid, WaitStatus},
    },
    unistd::{fork, ForkResult, Uid, User},
};
use once_cell::sync::Lazy;
use serde::{ser::SerializeMap, Serialize, Serializer};

use crate::landlock::{
    path_beneath_rules, Access, AccessFs, RestrictionStatus, Ruleset, RulesetAttr,
    RulesetCreatedAttr, RulesetError, RulesetStatus, ABI,
};

/* Data structures */
#[derive(Debug, Eq, PartialEq)]
pub(crate) struct Sydcall(ScmpSyscall, ScmpArch);

impl std::hash::Hash for Sydcall {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        i32::from(self.0).hash(state);
        match self.1 {
            ScmpArch::X86 => 0,
            ScmpArch::X8664 => 1,
            ScmpArch::X32 => 2,
            ScmpArch::Arm => 3,
            ScmpArch::Aarch64 => 4,
            ScmpArch::Mips => 5,
            ScmpArch::Mips64 => 6,
            ScmpArch::Mips64N32 => 7,
            ScmpArch::Mipsel => 8,
            ScmpArch::Mipsel64 => 9,
            ScmpArch::Mipsel64N32 => 10,
            ScmpArch::Ppc => 11,
            ScmpArch::Ppc64 => 12,
            ScmpArch::Ppc64Le => 13,
            ScmpArch::S390 => 14,
            ScmpArch::S390X => 15,
            ScmpArch::Parisc => 16,
            ScmpArch::Parisc64 => 17,
            ScmpArch::Riscv64 => 18,
            _ => u8::MAX,
        }
        .hash(state);
    }
}

// Define a struct that wraps a CStr to implement Display.
pub(crate) struct SydCStr<'a>(pub &'a CStr);

// Implement the Display trait for SydCStr
impl<'a> std::fmt::Display for SydCStr<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Convert CStr to a string slice for Debug formatting
        let s = format!("{:?}", self.0);

        // Trim the leading and trailing quotes
        // Let's panic if Debug ever changes format so we can mock rust devs.
        #[allow(clippy::arithmetic_side_effects)]
        let s = &s[1..s.len() - 1];

        // Write the trimmed string to the formatter
        write!(f, "{s}")
    }
}

pub(crate) struct SydNotifReq(ScmpNotifReq);

impl Serialize for SydNotifReq {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_map(Some(4))?;
        map.serialize_entry("id", &self.0.id)?;
        map.serialize_entry("pid", &self.0.pid)?;
        map.serialize_entry("flags", &self.0.flags)?;
        map.serialize_entry("data", &SydNotifData(self.0.data))?;
        map.end()
    }
}

pub(crate) struct SydNotifData(ScmpNotifData);

impl Serialize for SydNotifData {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_map(Some(4))?;
        let name = self
            .0
            .syscall
            .get_name_by_arch(self.0.arch)
            .unwrap_or_else(|_| "?".to_string());
        let arch = format!("{:?}", self.0.arch).to_ascii_lowercase();
        let arch = if arch == { "x8664" } { "x86_64" } else { &arch };
        map.serialize_entry("sys", &name)?;
        map.serialize_entry("arch", &arch)?;
        map.serialize_entry("ip", &self.0.instr_pointer)?;
        map.serialize_entry("args", &self.0.args)?;
        map.end()
    }
}

pub(crate) struct SydNotifResp(ScmpNotifResp);

impl Serialize for SydNotifResp {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_map(Some(4))?;
        map.serialize_entry("id", &self.0.id)?;
        map.serialize_entry("val", &self.0.val)?;
        map.serialize_entry("err", &self.0.error)?;
        map.serialize_entry("flags", &self.0.flags)?;
        map.end()
    }
}

/* Constants */

/// Number of CPUs on the system.
/// Initialized lazily on startup.
pub static NPROC: Lazy<usize> = Lazy::new(num_cpus::get);

/* Utilities */

#[inline]
pub(crate) fn op2name(op: u8) -> &'static str {
    match op {
        0x2 => "bind",
        0x3 => "connect",
        0xb => "sendto",
        0xc => "recvfrom",
        _ => unreachable!(),
    }
}

/// Checks LandLock ABI v3 is supported.
/// Returns:
/// - 0: Fully enforced
/// - 1: Partially enforced
/// - 2: Not enforced
/// - 127: Unsupported
pub fn lock_enabled() -> u8 {
    let abi = ABI::V3;
    let path_ro = vec!["/".to_string()];
    let path_rw = vec!["/".to_string()];

    // A helper function to wrap the operations and reduce duplication
    fn landlock_operation(
        path_ro: &[String],
        path_rw: &[String],
        abi: ABI,
    ) -> Result<RestrictionStatus, RulesetError> {
        let ruleset = Ruleset::default().handle_access(AccessFs::from_all(abi))?;
        let created_ruleset = ruleset.create()?;
        let ro_rules = path_beneath_rules(path_ro, AccessFs::from_read(abi));
        let updated_ruleset = created_ruleset.add_rules(ro_rules)?;
        let rw_rules = path_beneath_rules(path_rw, AccessFs::from_all(abi));
        let final_ruleset = updated_ruleset.add_rules(rw_rules)?;
        final_ruleset.restrict_self().map_err(RulesetError::from)
    }

    match landlock_operation(&path_ro, &path_rw, abi) {
        Ok(status) => match status.ruleset {
            RulesetStatus::FullyEnforced => 0,
            RulesetStatus::PartiallyEnforced => 1,
            RulesetStatus::NotEnforced => 2,
        },
        Err(_) => 127,
    }
}

/// Returns true if we are running under syd.
#[allow(clippy::disallowed_methods)]
pub fn syd_enabled() -> bool {
    // This will not work if the sandbox is locked.
    // Path::new("/dev/syd").exists() || Path::new("/dev/syd").exists()
    // SAFETY: In libc, we trust.
    match unsafe { fork() } {
        Ok(ForkResult::Parent { child, .. }) => {
            match waitpid(child, None) {
                Ok(WaitStatus::Exited(_, code)) => {
                    // Check the child's exit status.
                    // Exit status of 0 means syd is enabled.
                    code == 0
                }
                _ => {
                    // If there's an error waiting on the
                    // child, assume syd is not enabled.
                    false
                }
            }
        }
        Ok(ForkResult::Child) => {
            let mut ctx = match ScmpFilterContext::new_filter(ScmpAction::Allow) {
                Ok(ctx) => ctx,
                Err(_) => exit(1),
            };

            let syscall = ScmpSyscall::new("open");
            if ctx.add_rule(ScmpAction::Notify, syscall).is_err() {
                exit(1);
            }

            if ctx.load().is_err() && Errno::last() == Errno::EBUSY {
                // seccomp filter exists
                // syd is in business.
                exit(0);
            } else {
                // seccomp filter does not exist
                exit(1);
            }
        }
        Err(_) => {
            // If there's an error forking,
            // assume syd is not enabled.
            false
        }
    }
}

/// Returns the name of the libsecc☮mp native architecture.
pub fn seccomp_arch_native_name() -> Option<&'static str> {
    match ScmpArch::native() {
        ScmpArch::X86 => Some("x86"),
        ScmpArch::X8664 => Some("x86_64"),
        ScmpArch::X32 => Some("x32"),
        ScmpArch::Arm => Some("arm"),
        ScmpArch::Aarch64 => Some("aarch64"),
        ScmpArch::Mips => Some("mips"),
        ScmpArch::Mips64 => Some("mips64"),
        ScmpArch::Mips64N32 => Some("mips64n32"),
        ScmpArch::Mipsel => Some("mipsel"),
        ScmpArch::Mipsel64 => Some("mipsel64"),
        ScmpArch::Mipsel64N32 => Some("mipsel64n32"),
        ScmpArch::Ppc => Some("ppc"),
        ScmpArch::Ppc64 => Some("ppc64"),
        ScmpArch::Ppc64Le => Some("ppc64le"),
        ScmpArch::S390 => Some("s390"),
        ScmpArch::S390X => Some("s390s"),
        ScmpArch::Parisc => Some("parisc"),
        ScmpArch::Parisc64 => Some("parisc64"),
        ScmpArch::Riscv64 => Some("riscv64"),
        _ => None,
    }
}

/// Given a `Uid`, return the user name of the user.
/// On any error conditions, return "nobody".
pub fn get_user_name(uid: Uid) -> String {
    match User::from_uid(uid) {
        Ok(Some(user)) => user.name,
        _ => "nobody".to_string(),
    }
}

/// Given a username, return the home directory of the user.
/// On any error conditions, return "/var/empty".
pub fn get_user_home(username: &str) -> String {
    // Fetch user details.
    match User::from_name(username) {
        Ok(Some(user)) => user.dir.to_string_lossy().to_string(),
        _ => "/var/empty".to_string(),
    }
}

// Sets the specified signal to be ignored.
//
// This function utilizes the `sigaction` system call to set the specified signal's action
// to `SIG_IGN`, effectively causing the process to ignore that signal.
//
// # Arguments
//
// * `signal` - The signal number (e.g., `SIGTSTP`, `SIGTTIN`, `SIGTTOU`).
//
// # Returns
//
// * `Result<(), Error>` - Returns `Ok(())` if successful, or an error if the operation fails.
//
// # Example
//
// ```no_run
// use nix::sys::signal::SIGTSTP;
//
// let result = syd::ignore_signal(SIGTSTP);
// assert!(result.is_ok());
// ```
pub(crate) fn ignore_signal(signal: Signal) -> Result<(), Errno> {
    let sig_action = SigAction::new(
        SigHandler::SigIgn, // Set to ignore
        SaFlags::empty(),
        SigSet::empty(),
    );

    // SAFETY: The unsafe call to `sigaction` is used to set the signal's disposition
    // to "ignore". We're not invoking any handlers or performing any operations that
    // could lead to data races or other undefined behaviors. Hence, it's safe to call
    // in this context.
    unsafe {
        sigaction(signal, &sig_action)
            .map(|_| ())
            .map_err(|_| Errno::last())
    }
}

/// Return system call priority by system call name.
#[inline(always)]
pub(crate) fn syscall_priority(name: &str) -> u8 {
    if matches!(name, "brk" | "mmap" | "mmap2") {
        255
    } else if name.starts_with("open") {
        240
    } else if name.contains("stat") {
        225
    } else if name.contains("access") {
        200
    } else if name.contains("readlink") {
        190
    } else if name.starts_with("execve") || name.contains("fork") || name.starts_with("clone") {
        175
    } else if name.starts_with("mkdir") || name.starts_with("rename") || name.starts_with("unlink")
    {
        150
    } else if matches!(
        name,
        "bind" | "connect" | "recvfrom" | "sendto" | "socketcall"
    ) {
        125
    } else if name.starts_with("getdents") {
        100
    } else {
        25
    }
}

const IOPRIO_CLASS_IDLE: i32 = 3;
const IOPRIO_WHO_PROCESS: i32 = 1;

/// Sets the I/O priority of the current thread to idle.
///
/// This function uses the `ioprio_set` syscall to set the I/O
/// scheduling priority of the current thread to the idle class. The
/// idle I/O class is designed for tasks that should only use disk
/// resources when no other process needs them. When a thread is set to
/// idle, it will not compete with other (non-idle) processes for I/O
/// bandwidth.
///
/// Note that this setting is applied at the thread level in Linux,
/// where each thread is treated as a separate scheduling entity. As a
/// result, calling this function will only affect the I/O priority of
/// the thread from which it is called. If the application is
/// multi-threaded and a global I/O priority change is desired, this
/// function needs to be called from each thread, or specific threads
/// requiring the priority change should be targeted.
///
/// The function does not require any parameters and returns a `Result`:
/// - `Ok(())` on success.
/// - `Err(Errno)` containing Errno.
///
/// # Safety
///
/// This function involves an unsafe block due to the direct system call
/// (`nix::libc::syscall`). The `ioprio_set` syscall is considered
/// unsafe as it directly interfaces with the kernel, bypassing Rust's
/// safety guarantees.  However, the usage in this context is safe given
/// that:
/// - We are specifying `IOPRIO_WHO_PROCESS` with `0`, which correctly
///   targets the current thread.
/// - The `ioprio` value is correctly constructed for the idle I/O
///   class.
///
/// Users of this function do not need to take any special safety precautions.
pub(crate) fn set_io_priority_idle() -> Result<(), Errno> {
    // Set I/O priority: higher bits for the class, lower bits for the priority.
    // IOPRIO_CLASS_IDLE is shifted left by 13 bits to fit the class into higher bits.
    // Priority for idle class is not used, hence set to 0 (lower 13 bits).
    let ioprio = IOPRIO_CLASS_IDLE << 13;

    // SAFETY:
    // The syscall libc::SYS_ioprio_set is used to set the I/O priority
    // of a process. This call is considered unsafe because it involves
    // a direct system call, which bypasses the safety checks and
    // abstractions provided by Rust. However, this usage is safe under
    // the following conditions:
    // 1. The first argument IOPRIO_WHO_PROCESS specifies the target as
    //    a process.
    // 2. The second argument 0 refers to the current process. In the
    //    context of ioprio_set, passing 0 for the 'who' parameter
    //    targets the calling process. This is why getpid() is not
    //    necessary here, as 0 implicitly represents the current
    //    process's PID.
    // 3. The third argument ioprio is correctly constructed with a
    //    valid I/O class and priority, ensuring the syscall behaves as
    //    expected.
    if unsafe { nix::libc::syscall(nix::libc::SYS_ioprio_set, IOPRIO_WHO_PROCESS, 0, ioprio) } == 0
    {
        Ok(())
    } else {
        Err(Errno::last())
    }
}

/// Set the current thread's CPU scheduling policy to 'idle'.
///
/// This function sets the CPU scheduling policy of the current thread
/// to SCHED_IDLE, indicating that the thread should only be scheduled
/// to run when the system is idle.
///
/// # Returns
///
/// * `Ok(())` on successful setting of the scheduling policy and priority.
/// * `Err` on failure, with the specific error indicating the cause of the failure.
pub(crate) fn set_cpu_priority_idle() -> Result<(), Errno> {
    // SAFETY: We zero out the sched_param struct. This is safe because:
    // 1. sched_param is a plain data struct with no invariants related
    //    to its fields.
    // 2. All-zero is a valid representation for this struct in the
    //    context of SCHED_IDLE policy.
    let param: nix::libc::sched_param = unsafe { std::mem::zeroed() };

    // SAFETY: The call to nix::libc::sched_setscheduler is safe because:
    // 1. We are passing valid arguments: a PID of 0 for the current
    //    thread, a valid policy (SCHED_IDLE), and a pointer to a
    //    properly initialized sched_param structure.
    // 2. There are no thread-safety issues since the operation only
    //    affects the current thread.
    if unsafe { nix::libc::sched_setscheduler(0, nix::libc::SCHED_IDLE, std::ptr::addr_of!(param)) }
        == 0
    {
        Ok(())
    } else {
        Err(Errno::last())
    }
}

const SECCOMP_ARCH_LIST: &[ScmpArch] = &[
    ScmpArch::X86,
    ScmpArch::X8664,
    ScmpArch::X32,
    ScmpArch::Arm,
    ScmpArch::Aarch64,
    ScmpArch::Mips,
    ScmpArch::Mips64,
    ScmpArch::Mips64N32,
    ScmpArch::Mipsel,
    ScmpArch::Mipsel64,
    ScmpArch::Mipsel64N32,
    ScmpArch::Ppc,
    ScmpArch::Ppc64,
    ScmpArch::Ppc64Le,
    ScmpArch::S390,
    ScmpArch::S390X,
    ScmpArch::Parisc,
    ScmpArch::Parisc64,
    ScmpArch::Riscv64,
];

/// Print list of libseccomp's supported architectures
/// Used by `syd --arch list`
pub fn print_seccomp_architectures() {
    let native = ScmpArch::native();
    for arch in SECCOMP_ARCH_LIST {
        let mut repr = format!("{arch:?}").to_ascii_lowercase();
        if repr == "x8664" {
            // Fix potential confusion.
            repr = "x86_64".to_string();
        }
        if *arch == native {
            println!("- {repr} [*]")
        } else {
            println!("- {repr}");
        }
    }
}

// List of libseccomp supported architectures for the current system.
#[cfg(target_arch = "x86_64")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::X8664, ScmpArch::X86, ScmpArch::X32];
#[cfg(target_arch = "x86")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::X86];
#[cfg(target_arch = "arm")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::Arm];
#[cfg(target_arch = "aarch64")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::Aarch64, ScmpArch::Arm];
#[cfg(target_arch = "mips")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::Mips];
#[cfg(target_arch = "mips64")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::Mips64, ScmpArch::Mips64N32, ScmpArch::Mips];
#[cfg(target_arch = "mips64n32")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::Mips64N32, ScmpArch::Mips, ScmpArch::Mips64];
#[cfg(target_arch = "mipsel")]
pub(crate) const SCMP_ARCH: &[ScmpArch] =
    &[ScmpArch::Mipsel64, ScmpArch::Mipsel64N32, ScmpArch::Mipsel];
#[cfg(target_arch = "mipsel64")]
pub(crate) const SCMP_ARCH: &[ScmpArch] =
    &[ScmpArch::Mipsel64, ScmpArch::Mipsel, ScmpArch::Mipsel64N32];
#[cfg(target_arch = "mipsel64n32")]
pub(crate) const SCMP_ARCH: &[ScmpArch] =
    &[ScmpArch::Mipsel64N32, ScmpArch::Mipsel, ScmpArch::Mipsel64];
#[cfg(target_arch = "powerpc")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::Ppc];
#[cfg(target_arch = "powerpc64")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::Ppc64, ScmpArch::Ppc64Le, ScmpArch::Ppc];
#[cfg(target_arch = "parisc")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::Parisc];
#[cfg(target_arch = "parisc64")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::Parisc64, ScmpArch::Parisc];
#[cfg(target_arch = "riscv64")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::Riscv64];
#[cfg(target_arch = "s390x")]
pub(crate) const SCMP_ARCH: &[ScmpArch] = &[ScmpArch::S390X, ScmpArch::S390];

/// Add all supported architectures to the given filter.
#[allow(clippy::cognitive_complexity)]
pub(crate) fn seccomp_add_architectures(ctx: &mut ScmpFilterContext) -> IOResult<()> {
    // Add architectures based on the current architecture
    for arch in SCMP_ARCH {
        seccomp_add_arch(ctx, *arch)?;
    }
    Ok(())
}

fn seccomp2errno(errno: SeccompErrno) -> Errno {
    match errno {
        SeccompErrno::EACCES => Errno::EACCES,
        SeccompErrno::ECANCELED => Errno::ECANCELED,
        SeccompErrno::EDOM => Errno::EDOM,
        SeccompErrno::EEXIST => Errno::EEXIST,
        SeccompErrno::EFAULT => Errno::EFAULT,
        SeccompErrno::EINVAL => Errno::EINVAL,
        SeccompErrno::ENOENT => Errno::ENOENT,
        SeccompErrno::ENOMEM => Errno::ENOMEM,
        SeccompErrno::EOPNOTSUPP => Errno::EOPNOTSUPP,
        SeccompErrno::ERANGE => Errno::ERANGE,
        SeccompErrno::ESRCH => Errno::ESRCH,
        _ => Errno::ECANCELED,
    }
}

fn seccomp_add_arch(ctx: &mut ScmpFilterContext, arch: ScmpArch) -> IOResult<bool> {
    ctx.add_arch(arch).map_err(|e| {
        if let Some(errno) = e.errno() {
            std::io::Error::from_raw_os_error(seccomp2errno(errno) as i32)
        } else {
            std::io::Error::new(std::io::ErrorKind::Other, "Unknown seccomp error")
        }
    })
}

/// Simple human size formatter.
#[allow(clippy::arithmetic_side_effects)]
#[allow(clippy::cast_precision_loss)]
pub fn human_size(bytes: usize) -> String {
    const SIZES: &[char] = &['B', 'K', 'M', 'G', 'T', 'P', 'E'];
    let factor = 1024usize;

    let mut size = bytes as f64;
    let mut i = 0;

    while size > factor as f64 && i < SIZES.len() - 1 {
        size /= factor as f64;
        i += 1;
    }

    format!("{:.2}{}", size, SIZES[i])
}

/// Parse a FD from a Path.
pub(crate) fn parse_fd<P: AsRef<Path>>(path: P) -> Result<RawFd, Errno> {
    let bytes = path.as_ref().as_os_str().as_bytes();

    // Parsing bytes directly to integer
    let mut fd: RawFd = 0;
    #[allow(clippy::arithmetic_side_effects)]
    for &b in bytes {
        // Check if the byte is a valid digit
        if !b.is_ascii_digit() {
            return Err(Errno::EBADF);
        }
        fd = fd.saturating_mul(10).saturating_add((b - b'0') as RawFd);
    }

    Ok(fd)
}

/// Check if a path is `.' or `..'
#[inline]
pub(crate) fn path_is_dot<P: AsRef<Path>>(path: P) -> bool {
    let path = path.as_ref().as_os_str().as_bytes();
    match path.len() {
        1 if path[0] == b'.' => true,
        2 if path[0] == b'.' && path[1] == b'.' => true,
        _ => false,
    }
}

#[cfg(target_env = "musl")]
pub(crate) type IoctlRequest = nix::libc::c_int;
#[cfg(not(target_env = "musl"))]
pub(crate) type IoctlRequest = nix::libc::c_ulong;

const SIOCGIFFLAGS: IoctlRequest = nix::libc::SIOCGIFFLAGS as IoctlRequest;
const SIOCSIFFLAGS: IoctlRequest = nix::libc::SIOCSIFFLAGS as IoctlRequest;

/// Functionally equivalent to "ifconfig lo up".
pub fn bring_up_loopback() -> Result<(), Errno> {
    // Create a socket
    let sockfd: RawFd = socket(
        AddressFamily::Inet,
        SockType::Stream,
        SockFlag::empty(),
        None,
    )?;

    // SAFETY: socket returns a valid FD.
    let sockfd = unsafe { OwnedFd::from_raw_fd(sockfd) };

    // Prepare the interface request
    let mut ifreq = nix::libc::ifreq {
        ifr_name: {
            let mut name = [0 as nix::libc::c_char; 16]; // Initialize array with zeros as c_char
            let lo = b"lo\0"; // Interface name as byte array
            #[allow(clippy::cast_possible_wrap)]
            for (dest, &src) in name.iter_mut().zip(lo.iter()) {
                *dest = src as nix::libc::c_char; // Convert u8 to c_char and store in name
            }
            name // Use this initialized array
        },
        // SAFETY: Manually initialize ifr_ifru
        ifr_ifru: unsafe { std::mem::zeroed() },
    };

    // SAFETY: Get the current flags
    if unsafe { nix::libc::ioctl(sockfd.as_raw_fd(), SIOCGIFFLAGS, &mut ifreq) } != 0 {
        return Err(Errno::last());
    }

    // Modify the flags to bring up the interface
    // SAFETY: We're accessing the field of a union here.
    #[allow(clippy::cast_possible_truncation)]
    unsafe {
        ifreq.ifr_ifru.ifru_flags |=
            (nix::libc::IFF_UP | nix::libc::IFF_RUNNING) as nix::libc::c_short
    };

    // SAFETY: Set the new flags
    if unsafe { nix::libc::ioctl(sockfd.as_raw_fd(), SIOCSIFFLAGS, &mut ifreq) } != 0 {
        return Err(Errno::last());
    }

    Ok(())
}
