//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/syd-chk.rs: Exit with success if the process is running under syd
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::process::ExitCode;

fn main() -> ExitCode {
    ExitCode::from(if syd::syd_enabled() { 0 } else { 1 })
}
