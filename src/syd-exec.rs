//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/syd-exec.rs: Construct a sandbox command to execute a process outside syd.
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::process::ExitCode;

fn main() -> ExitCode {
    // Split the arguments using the ASCII Unit Separator character
    let args = std::env::args().skip(1).collect::<Vec<_>>().join("\x1F");

    // Format it using /dev/syd/cmd/exec!<concatenated-path>
    print!("/dev/syd/cmd/exec!{args}");

    ExitCode::SUCCESS
}
