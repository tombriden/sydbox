//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/syd-ldd.rs: syd's secure ldd(1) wrapper
//
// Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    fs::canonicalize,
    os::unix::process::CommandExt,
    process::{Command, ExitCode},
};

use nix::{errno::Errno, unistd::ttyname};

/// Resembles the `which` command, finds a program in PATH.
fn which(command: &str, realpath: bool) -> Result<String, Errno> {
    let out = match Command::new("which").arg(command).output() {
        Ok(out) => out.stdout,
        Err(error) => {
            return Err(Errno::from_i32(
                error.raw_os_error().unwrap_or(nix::libc::ENOENT),
            ));
        }
    };
    if out.is_empty() {
        return Err(Errno::ENOENT);
    }
    let bin = String::from_utf8_lossy(&out);
    let bin = bin.trim();
    if !realpath {
        return Ok(bin.to_string());
    }
    Ok(canonicalize(bin)
        .map_err(|_| Errno::last())?
        .to_string_lossy()
        .into_owned())
}

fn main() -> ExitCode {
    // Step 0: Determine syd path.
    let syd = if which("syd", false).is_ok() {
        "syd"
    } else {
        eprintln!("syd not found in PATH");
        return ExitCode::from(1);
    };

    // Step 1: Find the real path to ldd(1)
    let ldd = match which("ldd", true) {
        Ok(p) => p,
        Err(error) => {
            eprintln!("Failed to locate ldd: {error}");
            return ExitCode::from(2);
        }
    };

    // Step 2: Find the real path to /bin/sh
    let sh = match canonicalize("/bin/sh") {
        Ok(p) => p.to_string_lossy().to_string(),
        Err(error) => {
            eprintln!("Failed to canonicalize /bin/sh: {error}");
            return ExitCode::from(3);
        }
    };

    // Step 2: Find the path to TTYs.
    let tty_0 = ttyname(nix::libc::STDIN_FILENO)
        .map(|p| p.to_string_lossy().to_string())
        .unwrap_or("/dev/null".to_string());
    let tty_1 = ttyname(nix::libc::STDOUT_FILENO)
        .map(|p| p.to_string_lossy().to_string())
        .unwrap_or("/dev/null".to_string());
    let tty_2 = ttyname(nix::libc::STDERR_FILENO)
        .map(|p| p.to_string_lossy().to_string())
        .unwrap_or("/dev/null".to_string());

    // Step 3: Gather path arguments and canonicalize to allow for read sandboxing.
    let argv: Vec<String> = std::env::args().skip(1).collect();
    let list: Vec<String> = argv
        .clone()
        .into_iter()
        .filter(|arg| !arg.starts_with('-'))
        .map(|arg| match canonicalize(&arg) {
            Ok(canonicalized_path) => {
                format!("-mallow/read+{}", canonicalized_path.to_string_lossy())
            }
            Err(_) => format!("-mallow/read+{}", arg),
        })
        .collect();

    // Step 4: Execute ldd(1) under syd.
    Command::new(syd)
        .env(
            "SYD_LOG",
            std::env::var("SYD_LOG").unwrap_or("error".to_string()),
        )
        .env("SYD_NO_SYSLOG", "1")
        .args(list)
        .args([
            "-pimmutable",
            "-msandbox/read:on",
            "-msandbox/stat:off",
            "-msandbox/exec:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-msandbox/lock:on",
            "-mallow/read+/etc/ld-*.path",
            "-mallow/read+/etc/locale.alias",
            "-mallow/read+/usr/share/locale*/**/*.mo",
            "-mallow/read+/usr/share/locale*/locale.alias",
            "-mallow/read+/usr/lib*/locale*/locale-archive",
            "-mallow/read+/usr/lib*/**/gconv-modules*",
            "-mallow/read+/usr/**/LC_{ALL,COLLATE,CTYPE,IDENTIFICATION,MESSAGES}",
            "-mallow/read+/**/*.so.[0-9]*",
            "-mallow/exec+/lib/**/ld-linux*.so.[0-9]",
            "-mallow/exec+/usr/lib*/**/ld-linux*.so.[0-9]",
            "-mallow/write+/dev/null",
            "-mallow/lock/read+/",
            "-mallow/lock/write+/dev/null",
            &format!("-mallow/read+{ldd}"),
            &format!("-mallow/read+{sh}"),
            &format!("-mallow/exec+{ldd}"),
            &format!("-mallow/read+{tty_0}"),
            &format!("-mallow/write+{tty_1}"),
            &format!("-mallow/write+{tty_2}"),
            "-mlock:on",
            "--",
            "ldd",
        ])
        .args(&argv)
        .exec();
    ExitCode::from(127)
}
