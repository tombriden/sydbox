//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/log.rs: Simple logging on standard error using JSON lines
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

/* Simple logging with JSON lines */
#[cfg(feature = "log")]
use std::ffi::OsString;
#[cfg(feature = "log")]
use std::path::PathBuf;
use std::{
    env,
    ffi::CString,
    fs::File,
    io::{self, Write},
    os::fd::{FromRawFd, RawFd},
    time::{SystemTime, UNIX_EPOCH},
};

use env_logger::{Builder, Logger};
use log::{Level, SetLoggerError};
use nix::unistd::{geteuid, Pid};
use once_cell::sync::Lazy;
use parking_lot::Mutex;
use serde_json::{json, Map, Value};

use crate::{config::*, proc::*};

/// info! logging macro
#[macro_export]
macro_rules! info {
    ($($key:literal : $value:expr),+) => {
        if log::log_enabled!(log::Level::Info) {
            #[allow(clippy::disallowed_methods)]
            $crate::log::log_with_data(log::Level::Info, serde_json::json!({$($key: $value),+}).as_object().unwrap().clone());
        }
    }
}

/// error! logging macro
#[macro_export]
macro_rules! error {
    ($($key:literal : $value:expr),+) => {
        if log::log_enabled!(log::Level::Error) {
            #[allow(clippy::disallowed_methods)]
            $crate::log::log_with_data(log::Level::Error, serde_json::json!({$($key: $value),+}).as_object().unwrap().clone());
        }
    }
}

/// warn! logging macro
#[macro_export]
macro_rules! warn {
    ($($key:literal : $value:expr),+) => {
        if log::log_enabled!(log::Level::Warn) {
            #[allow(clippy::disallowed_methods)]
            $crate::log::log_with_data(log::Level::Warn, serde_json::json!({$($key: $value),+}).as_object().unwrap().clone());
        }
    }
}

/// debug! logging macro
#[macro_export]
macro_rules! debug {
    ($($key:literal : $value:expr),+) => {
        #[cfg(feature = "log")]
        if log::log_enabled!(log::Level::Debug) {
            #[allow(clippy::disallowed_methods)]
            $crate::log::log_with_data(log::Level::Debug, serde_json::json!({$($key: $value),+}).as_object().unwrap().clone());
        }
    }
}

/// trace! logging macro
#[macro_export]
macro_rules! trace {
    ($($key:literal : $value:expr),+) => {
        #[cfg(feature = "log")]
        if log::log_enabled!(log::Level::Trace) {
            #[allow(clippy::disallowed_methods)]
            $crate::log::log_with_data(log::Level::Trace, serde_json::json!({$($key: $value),+}).as_object().unwrap().clone());
        }
    }
}

type Writer = Box<dyn Write + Send>;

/// Simple logging on standard error using JSON lines
pub struct JsonLinesLogger {
    filter: Logger,
    writer: Mutex<Writer>,
}

impl JsonLinesLogger {
    fn new() -> Self {
        let mut builder = Builder::new();
        builder.parse_filters(&env::var(ENV_LOG).unwrap_or("warn".to_string()));

        let writer: Box<dyn io::Write + Send> = match env::var(ENV_LOG_FD) {
            Ok(fd) => {
                // SAFETY: We panic on parse errors.
                #[allow(clippy::disallowed_methods)]
                let fd: RawFd = fd.parse().expect("Invalid log file descriptor");
                // SAFETY: We trust the user to pass a valid FD.
                Box::new(unsafe { File::from_raw_fd(fd) })
            }
            Err(_) => Box::new(io::stderr()),
        };

        Self {
            filter: builder.build(),
            writer: Mutex::new(writer),
        }
    }

    /// Initialize logging
    pub fn init() -> Result<(), SetLoggerError> {
        let logger = Self::new();

        log::set_max_level(logger.filter.filter());
        log::set_boxed_logger(Box::new(logger))
    }
}

impl log::Log for JsonLinesLogger {
    fn enabled(&self, metadata: &log::Metadata) -> bool {
        self.filter.enabled(metadata)
    }

    fn log(&self, record: &log::Record) {
        if self.enabled(record.metadata()) {
            let mut writer = self.writer.lock();
            let _ = writeln!(writer, "{}", record.args());
        }
    }

    fn flush(&self) {}
}

/// Helper for logging using JSON lines.
#[allow(clippy::cognitive_complexity)]
pub fn log_with_data(level: Level, data: Map<String, Value>) {
    static NO_SYSLOG: Lazy<bool> = Lazy::new(|| std::env::var(ENV_NO_SYSLOG).is_ok());
    static LOG_UID: Lazy<nix::libc::uid_t> = Lazy::new(|| geteuid().as_raw());
    #[allow(clippy::disallowed_methods)]
    let mut log_entry = json!({
        "uid": *LOG_UID,
        "l" : level as usize,
        "t": SystemTime::now().duration_since(UNIX_EPOCH).unwrap_or_default().as_secs(),
    })
    .as_object_mut()
    .unwrap()
    .clone();
    log_entry.extend(data);

    if let Some(pid) = log_entry.get("pid") {
        // Process ID is given.
        // If log feature is enabled enrichen the message with cmdline and cwd.
        // If log feature is not enabled enrichen the message with comm only.
        #[allow(clippy::cast_possible_truncation)]
        #[allow(clippy::disallowed_methods)]
        let pid = Pid::from_raw(pid.as_i64().unwrap_or(0) as nix::libc::pid_t);
        if pid.as_raw() != 0 {
            #[cfg(not(feature = "log"))]
            {
                let cmd = proc_comm(pid).unwrap_or("?".to_string());
                log_entry.insert("cmd".to_string(), Value::String(cmd));
            }

            #[cfg(feature = "log")]
            {
                let cmd = proc_cmdline(pid)
                    .unwrap_or(OsString::from("?"))
                    .to_string_lossy()
                    .to_string();
                let cwd = format!("{}", proc_cwd(pid).unwrap_or(PathBuf::from("?")).display());
                log_entry.insert("cmd".to_string(), Value::String(cmd));
                log_entry.insert("cwd".to_string(), Value::String(cwd));
            }
        }
    }

    let log_entry = Value::Object(log_entry).to_string();
    match level {
        Level::Error => {
            log::error!("{log_entry}");
        }
        Level::Warn => {
            if !*NO_SYSLOG {
                syslog(&log_entry);
            }
            log::warn!("{log_entry}");
        }
        Level::Info => log::info!("{log_entry}"),
        Level::Debug => log::debug!("{log_entry}"),
        Level::Trace => log::trace!("{log_entry}"),
    }
}

/// Logs a message to the system's syslog.
///
/// # Arguments
///
/// * `message` - The message string to be logged.
fn syslog(message: &str) {
    let c_msg = CString::new(message).unwrap_or_else(|_|
        // SAFETY: We ensure the string has no null bytes and the vector
        // we pass into the function is an actual byte vector.
        unsafe { CString::from_vec_unchecked(b"?".to_vec()) });

    // SAFETY: Use the syslog interface provided by libc.
    unsafe {
        nix::libc::openlog(std::ptr::null(), nix::libc::LOG_PID, nix::libc::LOG_USER);
        nix::libc::syslog(
            nix::libc::LOG_WARNING,
            "%s\0".as_ptr().cast(),
            c_msg.as_ptr(),
        );
        nix::libc::closelog();
    }
}
