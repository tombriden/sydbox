//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/syd-sys.rs: Given a number, print the matching syscall name and exit.
//                 Given a regex, print case-insensitively matching syscall names and exit.
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{process::ExitCode, str::FromStr};

use getargs::{Opt, Options};
use libseccomp::{ScmpArch, ScmpSyscall};

fn main() -> ExitCode {
    let args = std::env::args().skip(1).collect::<Vec<_>>();

    // If no arguments are provided, display help
    if args.is_empty() {
        help();
        return ExitCode::SUCCESS;
    }

    let mut opts = Options::new(args.iter().map(String::as_str));
    let mut arch = ScmpArch::Native; // -a

    // SAFETY: We panic on parse errors.
    #[allow(clippy::disallowed_methods)]
    while let Some(opt) = opts.next_opt().expect("next opt") {
        match opt {
            Opt::Short('h') => {
                help();
                return ExitCode::SUCCESS;
            }
            Opt::Short('a') => {
                let value = match opts.value() {
                    Ok(value) => value,
                    Err(_) => {
                        eprintln!("-a requires an argument!");
                        eprintln!("Do '-a list' to print the list of architectures.");
                        return ExitCode::FAILURE;
                    }
                };
                if matches!(value.to_ascii_lowercase().as_str(), "help" | "list") {
                    syd::print_seccomp_architectures();
                    return ExitCode::SUCCESS;
                }
                arch = match ScmpArch::from_str(&format!(
                    "SCMP_ARCH_{}",
                    value.to_ascii_uppercase()
                )) {
                    Ok(arch) => arch,
                    Err(_) => {
                        eprintln!("Invalid architecture `{value}'");
                        return ExitCode::FAILURE;
                    }
                };
            }
            _ => {
                eprintln!("Unknown option: {opt:?}!");
                return ExitCode::FAILURE;
            }
        }
    }

    match opts.positionals().next() {
        None => {
            eprintln!("Expected syscall number or name regex as first argument!");
            ExitCode::FAILURE
        }
        Some(value) => {
            match value.parse::<i32>() {
                Ok(num) => {
                    let syscall = ScmpSyscall::from(num);
                    if let Ok(name) = syscall.get_name_by_arch(arch) {
                        println!("{num}\t{name}");
                    } else {
                        return ExitCode::FAILURE;
                    }
                }
                Err(_) => match regex::RegexBuilder::new(value).build() {
                    Ok(pattern) => {
                        let mut ok = false;
                        for (num, name) in (0..4096)
                            .map(|n| {
                                (
                                    n,
                                    ScmpSyscall::from(n)
                                        .get_name_by_arch(arch)
                                        .unwrap_or_default(),
                                )
                            })
                            .filter(|(_, name)| !name.is_empty())
                        {
                            if pattern.is_match(&name) {
                                println!("{num}\t{name}");
                                ok = true;
                            }
                        }
                        if !ok {
                            return ExitCode::FAILURE;
                        }
                    }
                    Err(error) => {
                        eprintln!("Invalid syscall regex \"{value}\": {error}");
                        return ExitCode::FAILURE;
                    }
                },
            }
            ExitCode::SUCCESS
        }
    }
}

fn help() {
    println!("Usage: syd-sys [-a list|native|x86|x86_64|aarch64...] number|name-regex");
    println!("Given a number, print the matching syscall name and exit.");
    println!("Given a regex, print case-insensitively matching syscall names and exit.");
}
