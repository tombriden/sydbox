//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/hash.rs: Utilities for hashing
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    collections::{hash_map::DefaultHasher, HashSet},
    hash::{BuildHasher, Hash, Hasher},
};

/// Avoid duplicate hashing while using `HashSet` with u64 keys.
pub struct NoHasher {
    value: u64,
}

impl Hasher for NoHasher {
    fn write(&mut self, _bytes: &[u8]) {
        unreachable!("NoHasher should only be used for u64 keys");
    }

    fn write_u64(&mut self, i: u64) {
        self.value = i;
    }

    fn finish(&self) -> u64 {
        self.value
    }
}

/// A builder for creating instances of `NoHasher`.
#[derive(Clone)]
pub struct NoHasherBuilder;

impl Default for NoHasherBuilder {
    fn default() -> Self {
        Self
    }
}

impl BuildHasher for NoHasherBuilder {
    type Hasher = NoHasher;

    fn build_hasher(&self) -> Self::Hasher {
        NoHasher { value: 0 }
    }
}

/// A `HashSet` with no hashers.
pub type NoHashSet = HashSet<u64, NoHasherBuilder>;

/// Hash a string slice.
pub fn hash_str(s: &str) -> u64 {
    let mut hasher = DefaultHasher::new();
    s.hash(&mut hasher);
    hasher.finish()
}
