//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/syd-ls.rs: Print the names of the system calls which belong to the given set and exit
//                  If set is ioctl, print the list of allowed ioctl requests
//                  If set is prctl, print the list of allowed prctl options
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{collections::HashSet, process::ExitCode};

use libseccomp::ScmpSyscall;

fn main() -> ExitCode {
    let mut args = std::env::args();

    match args.nth(1).as_deref() {
        None | Some("-h") => {
            println!("Usage: syd-ls set");
            println!("Print the names of the system calls which belong to the given set and exit.");
            println!(
                "Available sets are dead, deny, hook, noop, ptrace, safe, setid, time, and uring."
            );
            println!("If set is drop, print the list of capabilities that are dropped at startup.");
            println!("If set is env, print the list of unsafe environment variables.");
            println!("If set is ioctl, print the list of allowed ioctl requests.");
            println!("If set is prctl, print the list of allowed prctl options.");
        }
        Some("dead") => {
            for name in syd::config::DEAD_SYSCALLS {
                println!("{name}");
            }
        }
        Some("deny") => {
            let mut syscall_set: HashSet<_> = syd::config::SAFE_SYSCALLS
                .iter()
                .map(|&s| String::from(s))
                .collect();
            for syscall in syd::config::HOOK_SYSCALLS {
                syscall_set.insert(syscall.to_string());
            }
            let mut list = vec![];
            for syscall_number in 0..=600 {
                let syscall = ScmpSyscall::from(syscall_number);
                if let Ok(name) = syscall.get_name() {
                    if !syscall_set.contains(&name) {
                        list.push(name);
                    }
                }
            }
            list.sort_unstable();
            for name in list {
                println!("{name}");
            }
        }
        Some("hook") => {
            for name in syd::config::HOOK_SYSCALLS {
                println!("{name}");
            }
        }
        Some("noop") => {
            for name in syd::config::NOOP_SYSCALLS {
                println!("{name}");
            }
        }
        Some("safe") | Some("allow") => {
            for name in syd::config::SAFE_SYSCALLS {
                println!("{name}");
            }
        }
        Some("setid") => {
            for name in syd::config::SET_ID_SYSCALLS {
                println!("{name}");
            }
        }
        Some("time") => {
            for name in syd::config::TIME_SYSCALLS {
                println!("{name}");
            }
        }
        Some("uring") => {
            for name in syd::config::IOURING_SYSCALLS {
                println!("{name}");
            }
        }
        Some("drop") => {
            for cap in syd::config::CAPS_DROP {
                println!("{cap}");
            }
        }
        Some("env") => {
            for env in syd::config::UNSAFE_ENV {
                println!("{env}");
            }
        }
        Some("ioctl") => {
            // We have duplicate names due tu musl/glibc ioctl compat bits.
            // Let's sort and deduplicate here to avoid confusion.
            let mut list = syd::config::ALLOWLIST_IOCTL.to_vec();
            list.sort_unstable();
            list.dedup();
            for (name, _) in list {
                println!("{name}");
            }
        }
        Some("prctl") => {
            for (_, name) in syd::config::ALLOWLIST_PRCTL {
                println!("{name}");
            }
        }
        Some("ptrace") => {
            for name in syd::config::PTRACE_SYSCALLS {
                println!("{name}");
            }
        }
        Some(set) => {
            eprintln!("No such set: '{set}'");
            return ExitCode::FAILURE;
        }
    }

    ExitCode::SUCCESS
}
