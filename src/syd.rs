//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/syd.rs: Main entry point
//
// Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

//! syd: seccomp and landlock based application sandbox with support for namespaces
//! Main entry point.

// We like clean and simple code with documentation.
#![deny(missing_docs)]
#![deny(clippy::allow_attributes_without_reason)]
#![deny(clippy::arithmetic_side_effects)]
#![deny(clippy::as_ptr_cast_mut)]
#![deny(clippy::as_underscore)]
#![deny(clippy::assertions_on_result_states)]
#![deny(clippy::borrow_as_ptr)]
#![deny(clippy::branches_sharing_code)]
#![deny(clippy::case_sensitive_file_extension_comparisons)]
#![deny(clippy::cast_lossless)]
#![deny(clippy::cast_possible_truncation)]
#![deny(clippy::cast_possible_wrap)]
#![deny(clippy::cast_precision_loss)]
#![deny(clippy::cast_ptr_alignment)]
#![deny(clippy::cast_sign_loss)]
#![deny(clippy::checked_conversions)]
#![deny(clippy::clear_with_drain)]
#![deny(clippy::clone_on_ref_ptr)]
#![deny(clippy::cloned_instead_of_copied)]
#![deny(clippy::cognitive_complexity)]
#![deny(clippy::collection_is_never_read)]
#![deny(clippy::copy_iterator)]
#![deny(clippy::create_dir)]
#![deny(clippy::dbg_macro)]
#![deny(clippy::debug_assert_with_mut_call)]
#![deny(clippy::decimal_literal_representation)]
#![deny(clippy::default_trait_access)]
#![deny(clippy::default_union_representation)]
#![deny(clippy::derive_partial_eq_without_eq)]
#![deny(clippy::doc_link_with_quotes)]
#![deny(clippy::doc_markdown)]
#![deny(clippy::explicit_into_iter_loop)]
#![deny(clippy::explicit_iter_loop)]
#![deny(clippy::fallible_impl_from)]
#![deny(clippy::missing_safety_doc)]
#![deny(clippy::undocumented_unsafe_blocks)]

use std::{
    env,
    ffi::{CString, OsStr},
    fs::File,
    io::Write,
    os::unix::{ffi::OsStrExt, process::CommandExt},
    path::{Path, PathBuf},
    process::{exit, ExitCode},
    str::FromStr,
};

use anyhow::{anyhow, bail, Context, Error};
use getargs::{Opt, Options};
use libseccomp::{ScmpAction, ScmpFilterContext, ScmpSyscall, ScmpVersion};
use nix::{
    errno::Errno,
    libc::{setdomainname, STDERR_FILENO},
    mount::{mount, umount2, MntFlags, MsFlags},
    sched::{unshare, CloneFlags},
    sys::{
        resource::{setrlimit, Resource},
        signal::{signal, SigHandler, Signal},
        utsname::uname,
        wait::{waitpid, WaitStatus},
    },
    unistd::{
        chdir, chroot, fork, getgid, getuid, pivot_root, sethostname, unlinkat, write, ForkResult,
        UnlinkatFlags,
    },
    NixPath,
};
use once_cell::sync::Lazy;
use syd::{
    bring_up_loopback, caps,
    config::*,
    error,
    hash::{hash_str, NoHashSet},
    hook::{ExportMode, Supervisor},
    info,
    landlock::{
        path_beneath_rules, Access, AccessFs, RestrictionStatus, Ruleset, RulesetAttr,
        RulesetCreatedAttr, RulesetError, RulesetStatus, ABI,
    },
    log::JsonLinesLogger,
    proc::proc_limit_userns,
    sandbox::Sandbox,
    unshare::{GidMap, UidMap},
};

static SYD_VERSION: Lazy<&'static str> = Lazy::new(|| {
    if env!("SYD_GITHEAD").is_empty() {
        env!("CARGO_PKG_VERSION")
    } else {
        env!("SYD_GITHEAD")
    }
});

// This line will include the content of "esyd.sh" at compile-time
const ESYD: &str = include_str!("esyd.sh");

// Landlock API in use.
const LANDLOCK_ABI: ABI = ABI::V3;
const LANDLOCK_ABI_STR: &str = "v3";

#[derive(Debug, Default)]
struct Arguments {
    reexec: bool,

    arg0: Option<String>,
    argv: Vec<String>,
    envp: NoHashSet,

    export: Option<ExportMode>,

    sandbox: Sandbox,
}

// A helper function to wrap the operations and reduce duplication
fn landlock_operation(
    path_ro: &[String],
    path_rw: &[String],
    abi: ABI,
) -> Result<RestrictionStatus, RulesetError> {
    let ruleset = Ruleset::default().handle_access(AccessFs::from_all(abi))?;
    let created_ruleset = ruleset.create()?;
    let ro_rules = path_beneath_rules(path_ro, AccessFs::from_read(abi));
    let updated_ruleset = created_ruleset.add_rules(ro_rules)?;
    let rw_rules = path_beneath_rules(path_rw, AccessFs::from_all(abi));
    let final_ruleset = updated_ruleset.add_rules(rw_rules)?;
    final_ruleset.restrict_self().map_err(RulesetError::from)
}

// Set SIGPIPE handler to default.
fn setup_sigpipe() -> Result<(), Errno> {
    // SAFETY: The nix::sys::signal::signal function is unsafe because
    // it affects the global state of the program by changing how a
    // signal (SIGPIPE in this case) is handled. It's safe to call here
    // because changing the SIGPIPE signal to its default behavior will
    // not interfere with any other part of this program that could be
    // relying on a custom SIGPIPE signal handler.
    unsafe { signal(Signal::SIGPIPE, SigHandler::SigDfl) }.map(|_| ())
}

#[allow(clippy::cognitive_complexity)]
fn main() -> anyhow::Result<ExitCode> {
    // Set SIGPIPE handler to default.
    setup_sigpipe().context("Failed to set SIGPIPE signal handler to default.")?;

    // Parse CLI arguments
    let mut args = Arguments {
        reexec: env::var("SYD_INIT").is_ok(),
        ..Default::default()
    };

    // Note: If someone ever needs/wants non-UTF-8 CLI arguments,
    // we can consider changing this, but till then we consider
    // this safe and sound.
    #[allow(clippy::disallowed_methods)]
    let mut argv = argv::iter()
        .map(|os| os.to_str().expect("Argument is not valid UTF-8!"))
        .peekable();

    // Initialize Options.
    // SAFETY: Safe to assume argv has at least one element.
    // On the off chance that it's empty, we panic.
    #[allow(clippy::disallowed_methods)]
    let is_login = argv.next().expect("arg0").starts_with('-');
    let user_done = if is_login || argv.peek().is_none() {
        args.sandbox
            .parse_profile("user")
            .context("Error parsing user profile")?;
        true
    } else {
        false
    };
    let mut user_parse = false;
    let mut opts = Options::new(argv);

    // Local options handled by this function.
    let sh = env::var(ENV_SH).unwrap_or(SYD_SH.to_string());

    while let Some(opt) = opts.next_opt().context("calling Options::next")? {
        match opt {
            /*
             * Basic options
             */
            Opt::Short('h') | Opt::Long("help") => {
                help();
                exit(0);
            }
            Opt::Short('V') | Opt::Long("version") => {
                version();
                exit(0);
            }

            /*
             * Sandbox options
             */
            Opt::Long("sh") => {
                println!("{ESYD}");
                exit(0);
            }
            Opt::Short('E') => {
                args.export = Some(ExportMode::from_str(
                    opts.value().context("-E requires an argument!")?,
                )?);
                args.sandbox
                    .parse_profile("lib")
                    .context("Error parsing lib profile")?;
                if args.export == Some(ExportMode::PseudoFiltercode) {
                    // This variable makes setup_seccomp_parent print rules.
                    env::set_var("SYD_SECX", "1");
                } else {
                    env::remove_var("SYD_SECX");
                }
            }
            Opt::Short('x') => {
                env::set_var(ENV_NO_SYSLOG, "1");
                args.sandbox.set_trace(true);
            }
            Opt::Short('m') => {
                let cmd = opts.value().context("-m requires an argument!")?;
                if args.sandbox.locked() {
                    bail!("Failed to execute magic command `{cmd}': sandbox locked!");
                } else {
                    args.sandbox
                        .config(cmd)
                        .context(format!("Failed to execute magic command `{cmd}'."))?;
                }
            }
            Opt::Short('f') => {
                // Login shell compatibility:
                // Parse user profile as necessary.
                user_parse = true;
            }
            Opt::Short('l') | Opt::Long("login") => {
                // Login shell compatibility:
                // Parse user profile as necessary.
                user_parse = true;
            }
            Opt::Short('c') => {
                // When multiple -c arguments are given,
                // only the first one is honoured and
                // the rest is ignored in consistency
                // with how bash and dash behaves.
                user_parse = true;
                if args.argv.is_empty() {
                    args.argv.push(sh.clone());
                    args.argv.push("-c".to_string());
                    args.argv.push(
                        opts.value()
                            .context("-c requires an argument!")?
                            .to_string(),
                    );
                }
            }
            Opt::Short('P') => {
                let path = opts.value().context("-f requires an argument!")?;
                if args.sandbox.locked() {
                    error!("ctx": "config", "path": path, "error": "lock");
                } else {
                    args.sandbox
                        .parse_config_file(path)
                        .context(format!("Failed to parse configuration file `{path}'."))?;
                }
            }
            /* We keep --profile for syd-1 compatibility.
             * It's undocumented. */
            Opt::Short('p') | Opt::Long("profile") => {
                let profile = opts.value().context("--profile requires an argument!")?;
                if args.sandbox.locked() {
                    error!("ctx": "config", "profile": profile, "error": "lock");
                } else {
                    args.sandbox.parse_profile(profile).context(format!(
                        "Failed to parse configuration profile `{profile}'."
                    ))?;
                }
            }

            /*
             * Unshare options
             */
            Opt::Short('a') => {
                let val = opts
                    .value()
                    .context("-a requires an argument!")?
                    .to_string();
                args.arg0 = Some(val);
            }
            Opt::Short('e') => {
                let value = opts.value().context("-e requires an argument!")?;
                match value.split_once('=') {
                    Some((var, val)) => {
                        args.envp.insert(hash_str(var));
                        if !val.is_empty() {
                            // This way we give the user the chance to pass-through
                            // denylisted environment variables e.g.
                            //      syd -eLD_LIBRARY_PATH= cmd
                            // is equivalent to
                            //      syd -eLD_LIBRARY_PATH=$LD_LIBRARY_PATH cmd
                            env::set_var(var, val);
                        }
                    }
                    None => {
                        args.envp.remove(&hash_str(value));
                        env::remove_var(value);
                    }
                }
            }

            Opt::Short(c) => {
                bail!("Invalid option `-{c}'!");
            }
            Opt::Long(c) => {
                bail!("Invalid option `--{c}'!");
            }
        }
    }

    if user_parse && !user_done && !args.sandbox.locked() {
        args.sandbox
            .parse_profile("user")
            .context("Error parsing user profile")?;
    }

    // Prepare the command to execute, which may be a login shell.
    args.argv.extend(opts.positionals().map(|s| s.to_string()));
    let argv0 = if !args.argv.is_empty() {
        Some(args.argv.remove(0))
    } else {
        None
    };

    let argv0 = match (args.export.is_some(), argv0, is_login) {
        (true, _, _) => "/bin/true".to_string(),
        (false, Some(argv0), false) => argv0.to_string(),
        (false, None, false) | (false, _, true) => {
            if args.arg0.is_none() {
                // Allow user to override with -a
                // SAFETY: unwrap is safe as `sh` is UTF-8.
                #[allow(clippy::disallowed_methods)]
                let sh = Path::new(&sh)
                    .file_name()
                    .map(|s| s.to_str().unwrap())
                    .unwrap_or("-");
                args.arg0 = Some(format!("-{sh}"));
            }
            sh.to_string()
        }
    };

    // Handle namespace re-exec
    if args.reexec {
        return run(&argv0, args);
    }

    // Limit process resources.
    let restrict_prlimit = !args.sandbox.allow_unsafe_prlimit();
    if restrict_prlimit {
        if let Err(errno) = setrlimit(Resource::RLIMIT_CORE, 0, 0) {
            error!("ctx": "limit_core", "errno": errno as i32);
        }
    }

    // root/map:1 implies unshare/user:1
    // We do this here rather than in sandbox.rs,
    // because the user may toggle this flag many times.
    if args.sandbox.map_root {
        args.sandbox.set_unshare_user(true);
    }

    // private /dev/shm and /tmp don't work with chroot.
    if args.sandbox.root.is_some() && (args.sandbox.private_shm() || args.sandbox.private_tmp()) {
        bail!("Private /dev/shm or /tmp specified when changing root!");
    }

    // Create private /dev/shm, /tmp and configure bind mounts as necessary.
    let mut clean_dirs = Vec::with_capacity(2);
    if args.sandbox.private_shm() {
        let tmp = CString::new(format!("{PATH_TMP}/.syd-shm-XXXXXX"))
            .context("Failed to create CString for private shm directory")?
            .into_raw();

        // SAFETY: In libc we trust.
        if unsafe { nix::libc::mkdtemp(tmp) }.is_null() {
            let errno = Errno::last();
            return Err(anyhow::Error::new(errno).context("Failed to create private shm directory"));
        }

        // SAFETY: In libc we trust.
        let tmp = unsafe { CString::from_raw(tmp) }
            .into_string()
            .context("Invalid UTF-8 in private shm directory path name")?;

        args.sandbox
            .config(&format!("bind+{tmp}:/dev/shm:nodev,nosuid,noexec"))
            .context("Invalid bind mount for private /dev/shm")?;
        clean_dirs.push(tmp);
    }
    if args.sandbox.private_tmp() {
        let tmp = CString::new(format!("{PATH_TMP}/.syd-tmp-XXXXXX"))
            .context("Failed to create CString for private temporary directory")?
            .into_raw();

        // SAFETY: In libc we trust.
        if unsafe { nix::libc::mkdtemp(tmp) }.is_null() {
            let errno = Errno::last();
            return Err(
                anyhow::Error::new(errno).context("Failed to create private temporary directory")
            );
        }

        // SAFETY: In libc we trust.
        let tmp = unsafe { CString::from_raw(tmp) }
            .into_string()
            .context("Invalid UTF-8 in private temporary directory path name")?;

        args.sandbox
            .config(&format!("bind+{tmp}:/tmp:nodev,nosuid"))
            .context("Invalid bind mount for private /tmp")?;
        clean_dirs.push(tmp);
    }

    // Collect bind mounts, set unshare-mount if we've any.
    let bind_mounts = args.sandbox.collect_bind_mounts();
    if bind_mounts.is_some() {
        args.sandbox.set_unshare_mount(true);
    }

    // SAFETY: We cannot support NEWPID without NEWNS.
    // ie, pid namespace must have its own private /proc.
    if args.sandbox.unshare_pid() {
        args.sandbox.set_unshare_mount(true);
    }

    // Set up Linux namespaces if requested. Note,
    // we set it up here before spawning the child so as to
    // include the syd process into the pid namespace as well
    // such that the sandbox process and syd have the identical
    // view of /proc.
    let mut namespaces = CloneFlags::empty();
    for namespace in args.sandbox.namespaces() {
        namespaces |= syd::unshare::namespace::to_clone_flag(namespace);
    }
    if namespaces.is_empty() {
        // No namespace arguments passed, run normally.
        return run(&argv0, args);
    }

    // Prepare command to reexec syd asserting initialization is done.
    // We search `syd' in PATH when changing root for convenience.
    let exe = if args.sandbox.root.is_some() {
        PathBuf::from("syd")
    } else {
        env::current_exe().context("Failed to get current executable")?
    };
    let arg: Vec<String> = env::args().skip(1).collect();
    let mut syd = std::process::Command::new(exe);
    syd.args(&arg).env("SYD_INIT", "1");

    let id_buf = if args.sandbox.unshare_user() {
        // create the UID and GID mappings.
        let uid = getuid().as_raw();
        let gid = getgid().as_raw();

        let uid_buf = {
            let uid_maps = vec![
                UidMap {
                    inside_uid: if args.sandbox.map_root { 0 } else { uid },
                    outside_uid: uid,
                    count: 1,
                }, // Map the current user.
            ];
            let mut buf = Vec::new();
            for map in uid_maps {
                writeln!(
                    &mut buf,
                    "{} {} {}",
                    map.inside_uid, map.outside_uid, map.count
                )
                .context("Failed to format uidmap")?;
            }
            buf
        };

        let gid_buf = {
            let gid_maps = vec![
                GidMap {
                    inside_gid: if args.sandbox.map_root { 0 } else { gid },
                    outside_gid: gid,
                    count: 1,
                }, // Map the current group.
            ];
            let mut buf = Vec::new();
            for map in gid_maps {
                writeln!(
                    &mut buf,
                    "{} {} {}",
                    map.inside_gid, map.outside_gid, map.count
                )
                .context("Failed to format gidmap")?;
            }
            buf
        };
        Some((uid_buf, gid_buf))
    } else {
        None
    };

    // Tell the kernel to keep the capabilities after the unshare call.
    // This is important because unshare() can change the user
    // namespace, which often leads to a loss of capabilities.
    caps::securebits::set_keepcaps(true).context("Failed to set keep capabilities flag")?;

    unshare(namespaces).context("Failed to create namespace")?;

    // Write uid/gid map for user namespace.
    if let Some((ref uid_buf, ref gid_buf)) = id_buf {
        // SAFETY: Limit maximum user namespaces to 1.
        // We'll enter into a user subnamespace in run().
        proc_limit_userns().context("Failed to limit maximum user namespaces")?;

        // Write "deny" to /proc/self/setgroups before writing to gid_map.
        File::create("/proc/self/setgroups")
            .and_then(|mut f| f.write_all(b"deny"))
            .context("Failed to write to /proc/self/setgroups")?;
        File::create("/proc/self/gid_map")
            .and_then(|mut f| f.write_all(&gid_buf[..]))
            .context("Failed to write to /proc/self/gid_map")?;
        File::create("/proc/self/uid_map")
            .and_then(|mut f| f.write_all(&uid_buf[..]))
            .context("Failed to write to /proc/self/uid_map")?;

        // Set inheritable mask and ambient caps to retain caps after execve(2).
        caps::securebits::set_keepcaps(true).context("Failed to set keep capabilities flag")?;
        let permitted_caps = caps::read(None, caps::CapSet::Permitted)
            .context("Failed to read permitted capabilities")?;
        caps::set(None, caps::CapSet::Inheritable, &permitted_caps)
            .context("Failed to set inheritable capabilities mask")?;

        // Set the same capabilities as ambient, if necessary.
        for cap in permitted_caps {
            caps::raise(None, caps::CapSet::Ambient, cap)
                .context(format!("Failed to raise capability {cap} to ambient"))?;
        }
    }

    // Bring up loopback device for net namespace.
    if args.sandbox.unshare_net() {
        // Set up the loopback interface.
        if let Err(errno) = bring_up_loopback() {
            error!("ctx": "ifup_lo", "errno": errno as i32);
        }
    }

    // Set host and domain name for uts namespace.
    if args.sandbox.unshare_uts() {
        // SAFETY: Domain name must not contain a nul-byte.
        #[allow(clippy::disallowed_methods)]
        let domainname = CString::new(args.sandbox.domainname.clone()).unwrap();

        // SAFETY: There's no setdomainname wrapper in nix.
        let _ = unsafe { setdomainname(domainname.as_ptr() as *const _, domainname.len()) };
        let _ = sethostname(&args.sandbox.hostname);
    }

    // SAFETY: In libc we trust.
    match unsafe { fork() }.context("Failed to fork")? {
        ForkResult::Parent { child, .. } => {
            // SAFETY: Set up a Landlock sandbox to disallow all access.
            // Exception: /var/tmp where private shm & tmp reside.
            let p = vec![PATH_TMP.to_string()];
            let _ = landlock_operation(&p, &p, LANDLOCK_ABI);

            // SAFETY: Set up a seccomp filter which only allows
            // 1. read, write and exit
            // 2. wait4 and waitpid
            // 3. memory allocation functions
            // 4. unlinkat to clean up temporary directories.
            let mut filter = ScmpFilterContext::new_filter(ScmpAction::KillProcess)
                .context("Failed to create seccomp filter")?;
            let allow_call = [
                "read",
                "write",
                "exit",
                "exit_group",
                "wait4",
                "waitpid",
                "brk",
                "madvise",
                "mmap",
                "mmap2",
                "munmap",
                "sigaltstack",
                "unlinkat",
            ];
            for call in allow_call {
                filter.add_rule(ScmpAction::Allow, ScmpSyscall::new(call))?;
            }
            filter.load().context("Failed to load seccomp filter")?;

            let ret = loop {
                match waitpid(child, None) {
                    Ok(WaitStatus::Exited(_, code)) => {
                        #[allow(clippy::cast_possible_truncation)]
                        #[allow(clippy::cast_sign_loss)]
                        break Ok(ExitCode::from(code as u8));
                    }
                    Ok(WaitStatus::StillAlive) | Err(Errno::EINTR) => {}
                    Ok(_) => {
                        break Ok(ExitCode::from(127));
                    }
                    Err(error) => {
                        break Err(anyhow!("Failed to wait: {error}"));
                    }
                }
            };

            // Clean private /dev/shm and /tmp directories.
            // SAFETY: We only remove if directories are empty.
            for dir in clean_dirs {
                let _ = unlinkat(None, Path::new(&dir), UnlinkatFlags::RemoveDir);
            }

            ret
        }
        ForkResult::Child => {
            // SAFETY: It is unsafe to allocate here.
            // println!, unwrap() etc. are also unsafe.

            // Set mount propagation on the root filesystem for mount namespace.
            if args.sandbox.unshare_mount() {
                const NONE: Option<&PathBuf> = None::<PathBuf>.as_ref();
                if let Some(flags) = args.sandbox.propagation {
                    if !flags.is_empty() {
                        if let Err(errno) = mount(Some("none"), "/", NONE, flags, NONE) {
                            write(
                                STDERR_FILENO,
                                b"syd: Failed to change root filesystem propagation\n",
                            )
                            .ok();
                            // SAFETY: _exit is async-signal-safe.
                            unsafe { nix::libc::_exit(errno as i32) };
                        }
                    }
                }

                // Change root directory if requested.
                if let Some(ref root) = args.sandbox.root {
                    // root _must_ be a mountpoint,
                    // We recursively bind mount it onto itself to workaround this restriction.
                    if let Err(errno) = mount(
                        Some(root),
                        root,
                        NONE,
                        MsFlags::MS_BIND | MsFlags::MS_REC,
                        NONE,
                    ) {
                        write(
                            STDERR_FILENO,
                            b"syd: Failed to bind mount the new root directory\n",
                        )
                        .ok();
                        // SAFETY: _exit is async-signal-safe.
                        unsafe { nix::libc::_exit(errno as i32) };
                    }

                    // For subsequent actions the current directory must equal root.
                    if let Err(errno) = chdir(root) {
                        write(STDERR_FILENO, b"syd: Failed to change to root directory\n").ok();
                        // SAFETY: _exit is async-signal-safe.
                        unsafe { nix::libc::_exit(errno as i32) };
                    }

                    // Mount procfs.
                    // This may or may not be a private procfs depending on --unshare-pid.
                    // We need it in both cases because we're chrooting and the procfs outside
                    // is going to be inaccessible.
                    if let Some(ref proc) = args.sandbox.proc {
                        let flags = MsFlags::MS_NOSUID | MsFlags::MS_NOEXEC | MsFlags::MS_NODEV;
                        if let Err(errno) =
                            mount(Some("proc"), proc, Some("proc"), flags, Some("hidepid=2"))
                        {
                            write(STDERR_FILENO, b"syd: Failed to mount proc filesystem\n").ok();
                            // SAFETY: _exit is async-signal-safe.
                            unsafe { nix::libc::_exit(errno as i32) };
                        }
                    }

                    // Change the root mount to current directory.
                    // We move the old mount temporarily over ./proc.
                    if let Err(errno) = pivot_root(".", "./proc") {
                        write(STDERR_FILENO, b"syd: Failed to pivot root\n").ok();
                        // SAFETY: _exit is async-signal-safe.
                        unsafe { nix::libc::_exit(errno as i32) };
                    }

                    // Unmount the old root which is not necessary.
                    if let Err(errno) = umount2("/proc", MntFlags::MNT_DETACH) {
                        write(STDERR_FILENO, b"syd: Failed to unmount old root\n").ok();
                        // SAFETY: _exit is async-signal-safe.
                        unsafe { nix::libc::_exit(errno as i32) };
                    }

                    // Process bind mounts as necessary.
                    if let Some(bind_mounts) = bind_mounts {
                        for bind in bind_mounts {
                            let mut flags = bind.opt | MsFlags::MS_BIND | MsFlags::MS_REC;
                            if let Err(errno) = mount(Some(&bind.src), &bind.dst, NONE, flags, NONE)
                            {
                                write(STDERR_FILENO, b"syd: Failed to bind mount: ").ok();
                                write(STDERR_FILENO, bind.src.as_os_str().as_bytes()).ok();
                                write(STDERR_FILENO, b" -> ").ok();
                                write(STDERR_FILENO, bind.dst.as_os_str().as_bytes()).ok();
                                write(STDERR_FILENO, b"\n").ok();
                                if errno != Errno::ENOENT {
                                    // SAFETY: _exit is async-signal-safe.
                                    unsafe { nix::libc::_exit(errno as i32) };
                                }
                                write(
                                    STDERR_FILENO,
                                    b"syd: Source or target doesn't exist, continuing...\n",
                                )
                                .ok();
                                continue;
                            }
                            flags |= MsFlags::MS_REMOUNT;
                            if let Err(errno) = mount(Some("none"), &bind.dst, NONE, flags, NONE) {
                                write(STDERR_FILENO, b"syd: Failed to remount: ").ok();
                                write(STDERR_FILENO, bind.dst.as_os_str().as_bytes()).ok();
                                write(STDERR_FILENO, b"\n").ok();
                                // SAFETY: _exit is async-signal-safe.
                                unsafe { nix::libc::_exit(errno as i32) };
                            }
                        }
                    }

                    // Almost there, let's reensure our current working directory equals root.
                    if let Err(errno) = chdir("/") {
                        write(
                            STDERR_FILENO,
                            b"syd: Failed to change to new root directory\n",
                        )
                        .ok();
                        // SAFETY: _exit is async-signal-safe.
                        unsafe { nix::libc::_exit(errno as i32) };
                    }

                    // All done, let's chroot into cwd.
                    if let Err(errno) = chroot(".") {
                        write(STDERR_FILENO, b"syd: Failed to change root directory\n").ok();
                        // SAFETY: _exit is async-signal-safe.
                        unsafe { nix::libc::_exit(errno as i32) };
                    }
                } else {
                    // Mount private procfs as necessary.
                    if args.sandbox.unshare_pid() {
                        if let Some(ref proc) = args.sandbox.proc {
                            let flags = MsFlags::MS_NOSUID | MsFlags::MS_NOEXEC | MsFlags::MS_NODEV;
                            if let Err(errno) = mount(Some("proc"), proc, Some("proc"), flags, NONE)
                            {
                                write(STDERR_FILENO, b"syd: Failed to mount proc filesystem\n")
                                    .ok();
                                // SAFETY: _exit is async-signal-safe.
                                unsafe { nix::libc::_exit(errno as i32) };
                            }
                        }
                    }

                    // Process bind mounts as necessary.
                    if let Some(bind_mounts) = bind_mounts {
                        for bind in bind_mounts {
                            let mut flags = bind.opt | MsFlags::MS_BIND | MsFlags::MS_REC;
                            if let Err(errno) = mount(Some(&bind.src), &bind.dst, NONE, flags, NONE)
                            {
                                write(STDERR_FILENO, b"syd: Failed to bind mount: ").ok();
                                write(STDERR_FILENO, bind.src.as_os_str().as_bytes()).ok();
                                write(STDERR_FILENO, b" -> ").ok();
                                write(STDERR_FILENO, bind.dst.as_os_str().as_bytes()).ok();
                                write(STDERR_FILENO, b"\n").ok();
                                if errno != Errno::ENOENT {
                                    // SAFETY: _exit is async-signal-safe.
                                    unsafe { nix::libc::_exit(errno as i32) };
                                }
                                write(
                                    STDERR_FILENO,
                                    b"syd: Source or target doesn't exist, continuing...\n",
                                )
                                .ok();
                                continue;
                            }
                            flags |= MsFlags::MS_REMOUNT;
                            if let Err(errno) = mount(Some("none"), &bind.dst, NONE, flags, NONE) {
                                write(STDERR_FILENO, b"Failed to remount: ").ok();
                                write(STDERR_FILENO, bind.dst.as_os_str().as_bytes()).ok();
                                write(STDERR_FILENO, b"\n").ok();
                                // SAFETY: _exit is async-signal-safe.
                                unsafe { nix::libc::_exit(errno as i32) };
                            }
                        }
                    }
                }
            }

            // Rexecute syd in the new namespace.
            syd.exec();
            // SAFETY: _exit is async-signal-safe.
            unsafe { nix::libc::_exit(127) };
        }
    }
}

#[allow(clippy::cognitive_complexity)]
fn run(argv0: &str, mut args: Arguments) -> anyhow::Result<ExitCode> {
    // SAFETY: If entering into a new user namespace:
    // 1. Limit max_user_namespaces to 1 (this was done right after unshare).
    // 2. Enter into a second user namespace.
    // This way we prevent sandbox process from creating subnamespaces
    // with a possibly different view of the filesystem. This is
    // functionally identical to bubblewrap's --disable-userns option.
    // We must to this by default unconditionally because syd must
    // have an identical view of the filesystem with the sandbox process
    // to operate correctly.
    let unshare_user = args.sandbox.unshare_user();
    if unshare_user {
        // Careful, we have to do this before the unshare.
        let uid = getuid().as_raw();
        let gid = getgid().as_raw();

        match unshare(CloneFlags::CLONE_NEWUSER) {
            Err(Errno::EPERM) => {
                // SAFETY: We ignore `EPERM` since this may happen when e.g. rootfs
                // is mounted read-only. This is safe as our sole intention is to
                // disable creation of user subnamespaces anyway.
            }
            Err(errno) => {
                return Err(
                    anyhow::Error::new(errno).context("Failed to enter into user subnamespace")
                )
            }
            Ok(_) => {
                // create the UID and GID mappings.
                let uid_buf = {
                    let uid_maps = vec![
                        UidMap {
                            inside_uid: if args.sandbox.map_root { 0 } else { uid },
                            outside_uid: if args.sandbox.map_root { 0 } else { uid },
                            count: 1,
                        }, // Map the current user.
                    ];
                    let mut buf = Vec::new();
                    for map in uid_maps {
                        writeln!(
                            &mut buf,
                            "{} {} {}",
                            map.inside_uid, map.outside_uid, map.count
                        )
                        .context("Failed to format uidmap")?;
                    }
                    buf
                };

                let gid_buf = {
                    let gid_maps = vec![
                        GidMap {
                            inside_gid: if args.sandbox.map_root { 0 } else { gid },
                            outside_gid: if args.sandbox.map_root { 0 } else { gid },
                            count: 1,
                        }, // Map the current group.
                    ];
                    let mut buf = Vec::new();
                    for map in gid_maps {
                        writeln!(
                            &mut buf,
                            "{} {} {}",
                            map.inside_gid, map.outside_gid, map.count
                        )
                        .context("Failed to format gidmap")?;
                    }
                    buf
                };

                // Write uid/gid map for user subnamespace.
                // Write "deny" to /proc/self/setgroups before writing to gid_map.
                File::create("/proc/self/setgroups")
                    .and_then(|mut f| f.write_all(b"deny"))
                    .context("Failed to write to /proc/self/setgroups")?;
                File::create("/proc/self/gid_map")
                    .and_then(|mut f| f.write_all(&gid_buf[..]))
                    .context("Failed to write to /proc/self/gid_map")?;
                File::create("/proc/self/uid_map")
                    .and_then(|mut f| f.write_all(&uid_buf[..]))
                    .context("Failed to write to /proc/self/uid_map")?;

                // Set inheritable mask and ambient caps to retain caps
                // after execve(2).
                caps::securebits::set_keepcaps(true)
                    .context("Failed to set keep capabilities flag")?;
                let permitted_caps = caps::read(None, caps::CapSet::Permitted)
                    .context("Failed to read permitted capabilities")?;
                caps::set(None, caps::CapSet::Inheritable, &permitted_caps)
                    .context("Failed to set inheritable capabilities mask")?;

                // Set the same capabilities as ambient, if necessary.
                for cap in permitted_caps {
                    caps::raise(None, caps::CapSet::Ambient, cap)
                        .context(format!("Failed to raise capability {cap} to ambient"))?;
                }
            }
        }
    }

    // Initialize logging.
    JsonLinesLogger::init().context("Failed to initialize logging.")?;

    // Initialize sandbox environment, drop capabilities.
    Supervisor::init_env(args.sandbox.flags, unshare_user)?;

    // Clean up the environment as necessary.
    if !args.sandbox.allow_unsafe_env() {
        for &var in UNSAFE_ENV {
            if !args.envp.contains(&hash_str(var)) && env::var(var).is_ok() {
                info!("ctx": "init", "env_unset": var);
                env::remove_var(var);
            }
        }
    }

    // Set up the Landlock sandbox if requested. Note,
    // we set it up here before spawning the child so as to
    // include the syd sandbox threads into the sandbox as
    // well. This is done for added security.
    // Note, Landlock errors are not fatal.
    if let Some((mut path_ro, mut path_rw)) = args.sandbox.collect_landlock() {
        path_ro.sort();
        path_rw.sort();

        match landlock_operation(&path_ro, &path_rw, LANDLOCK_ABI) {
            Ok(status) => {
                info!("ctx": "landlock",
                "abi": LANDLOCK_ABI_STR,
                "path_ro": path_ro,
                "path_rw": path_rw,
                "status": match status.ruleset {
                    // The FullyEnforced case must be tested by the developer.
                    RulesetStatus::FullyEnforced => "fully_enforced",
                    RulesetStatus::PartiallyEnforced => "partially_enforced",
                    // Users should be warned that they are not protected.
                    RulesetStatus::NotEnforced => "not_enforced",
                });
            }
            Err(error) => {
                info!("ctx": "landlock",
                    "abi": LANDLOCK_ABI_STR,
                    "path_ro": path_ro,
                    "path_rw": path_rw,
                    "status": "unsupported",
                    "error": error.to_string());
            }
        }
    }

    // Finalize Sandbox, build GlobSets.
    args.sandbox
        .build_globsets()
        .context("Failed to build GlobSets.")?;

    // Step 6: Initialize sandbox supervisor.
    let deny_tsc = args.sandbox.deny_tsc();
    let wait_all = args.sandbox.exit_wait_all();
    let mut supervisor =
        Supervisor::new(args.sandbox, *syd::NPROC, args.export).context(format!(
            "Error creating sandbox with {} threads and export mode {:?}.",
            *syd::NPROC,
            args.export,
        ))?;
    supervisor.init();

    // Prepare the command to execute.
    let mut command = syd::unshare::Command::new(argv0).context("Failed to create pipe!")?;
    command.deny_tsc(deny_tsc);
    command.args(&args.argv);
    if let Some(ref arg0) = args.arg0 {
        command.arg0(arg0);
    }
    info!("ctx": "run", "comm": args.arg0.unwrap_or(argv0.to_string()), "args": args.argv);

    // Spawn the program under sandbox.
    let (pid, thread_handle, pool) = match supervisor.spawn(command) {
        Ok((pid, thread_handle, pool)) => (pid, thread_handle, pool),
        Err(error) => {
            let errno = Errno::last() as u8;
            let error = error.context("Failed to spawn command under sandbox.");
            eprintln!("{error:?}");
            return Ok(ExitCode::from(errno));
        }
    };

    // Wait for the process to exit and return the same error code.
    let code = match Supervisor::wait(pid, thread_handle, pool, wait_all) {
        Ok(code) => code,
        Err(errno) => {
            let error = Error::new(errno).context("Failed to wait for sandboxed process.");
            eprintln!("{error:?}");
            errno as i32
        }
    };
    Ok(ExitCode::from(
        u8::try_from(code).context("Invalid exit code!")?,
    ))
}

fn help() {
    println!(
        "syd [-acefhlmpxEPV] [--] {{command [arg...]}}
syd --sh
syd-cat profile-name|list
syd-chk
syd-env pid [-i] [name=value]... {{command [arg...]}}
syd-err number|name-regex
syd-exec {{command [arg...]}}
syd-ldd
syd-lock
syd-log
syd-ls set
syd-mem [-HV] [pid]
syd-norm path
syd-read path
syd-run pid {{command [arg...]}}
syd-size size|human-size
syd-stat [pid]
syd-sys [-a list|native|x86|x86_64|aarch64...] number|name-regex
syd-test [<name-regex>|<number>|<number>..<number>]..
syd-tty [pid]"
    );
}

/// Print version information.
fn version() {
    println!("syd {}", *SYD_VERSION);
    println!("Author: Ali Polatel");
    println!("License: GPL-3.0-or-later");

    let feat = [
        #[cfg(debug_assertions)]
        "+debug",
        #[cfg(not(debug_assertions))]
        "-debug",
        #[cfg(feature = "log")]
        "+log",
        #[cfg(not(feature = "log"))]
        "-log",
        #[cfg(feature = "uring")]
        "+uring",
        #[cfg(not(feature = "uring"))]
        "-uring",
    ];
    println!("Features: {}", feat.join(", "));

    let state = match syd::lock_enabled() {
        0 => "fully enforced",
        1 => "partially enforced",
        2 => "not enforced",
        _ => "unsupported",
    };
    println!("Landlock: {state}");

    let libapi = libseccomp::get_api();
    match ScmpVersion::current() {
        Ok(libver) => {
            println!(
                "libseccomp: v{}.{}.{} api:{}",
                libver.major, libver.minor, libver.micro, libapi
            );
        }
        Err(error) => {
            println!("libseccomp: ? (error: {error})");
        }
    }

    let uname = match uname() {
        Ok(info) => OsStr::to_str(info.release()).unwrap_or("?").to_string(),
        Err(_) => "?".to_string(),
    };
    println!(
        "Host: {uname} {}",
        syd::seccomp_arch_native_name().unwrap_or("?")
    );
    println!("Comp: {}", env!("SYD_BUILDHOST"));
}
