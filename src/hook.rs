//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/hook.rs: Secure computing hooks
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
// Based in part upon greenhook which is under public domain.
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    collections::{HashMap, HashSet},
    ffi::{CStr, CString, OsStr, OsString},
    fs::{File, OpenOptions},
    io::{self, BufReader, IoSlice, IoSliceMut, Read, Result as IOResult, Seek, SeekFrom, Write},
    mem::MaybeUninit,
    net::{IpAddr, Ipv4Addr},
    os::{
        fd::{AsRawFd, FromRawFd, OwnedFd, RawFd},
        unix::ffi::{OsStrExt, OsStringExt},
    },
    path::{Path, PathBuf},
    process::exit,
    str::FromStr,
    sync::Arc,
    thread::{Builder, JoinHandle},
};

use anyhow::{bail, Context};
use libseccomp::{
    ScmpAction, ScmpArgCompare, ScmpCompareOp, ScmpFilterContext, ScmpNotifReq, ScmpNotifResp,
    ScmpNotifRespFlags, ScmpSyscall, ScmpVersion,
};
use log::{log_enabled, Level};
use nix::{
    errno::Errno,
    fcntl::{open, renameat, AtFlags, OFlag},
    libc::{ioctl, AT_FDCWD},
    mount::{mount, umount, umount2, MntFlags, MsFlags},
    sched::{clone, CloneFlags},
    sys::{
        epoll::{
            epoll_create1, epoll_ctl, epoll_wait, EpollCreateFlags, EpollEvent, EpollFlags, EpollOp,
        },
        signal::{kill, SIGCHLD, SIGHUP, SIGKILL, SIGTSTP, SIGTTIN, SIGTTOU},
        socket::{
            bind, connect, getsockname, sendto, AddressFamily, MsgFlags, SockaddrLike,
            SockaddrStorage,
        },
        stat::{fchmod, fchmodat, mknod, umask, FchmodatFlags, Mode, SFlag},
        uio::{process_vm_readv, process_vm_writev, RemoteIoVec},
        utsname::uname,
        wait::{waitpid, WaitPidFlag, WaitStatus},
    },
    unistd::{
        access, chown, close, faccessat, fchown, fchownat, linkat, lseek, mkdir, mkstemp,
        symlinkat, sysconf, truncate, unlink, unlinkat, write, AccessFlags, FchownatFlags, Gid,
        LinkatFlags, Pid, SysconfVar, Uid, UnlinkatFlags, Whence,
    },
    NixPath,
};
use once_cell::sync::Lazy;
use parking_lot::RwLock;
use procfs::process::Process;
use rusty_pool::ThreadPool;
use serde::{ser::SerializeMap, Serialize};
use smallvec::{smallvec, SmallVec};

use crate::{
    caps,
    compat::getdents,
    config::*,
    debug, error,
    fs::{
        canonicalize, path_ends_with_dot, path_ends_with_slash, read_link, MissingHandling,
        MissingHandling::*,
    },
    info, op2name, parse_fd, path_is_dot,
    proc::{proc_fd, proc_mem_limit, proc_task_limit, proc_tgid, proc_tty, proc_umask},
    sandbox::{Action, Capability, Flag, Sandbox, SandboxGuard},
    seccomp_add_architectures, set_cpu_priority_idle, set_io_priority_idle, trace, warn,
    IoctlRequest, Sydcall, SCMP_ARCH,
};
#[cfg(feature = "log")]
use crate::{SydCStr, SydNotifReq, SydNotifResp};

const EACCES: i32 = -nix::libc::EACCES;
const ENOENT: i32 = -nix::libc::ENOENT;

const UNIX_PATH_MAX: usize = 108;

/*
 * Seccomp constants
 */

const SECCOMP_IOCTL_NOTIF_ADDFD: IoctlRequest = 0x40182103;
/*
const SECCOMP_IOCTL_NOTIF_ID_VALID: IoctlRequest = 0x40082102;
const SECCOMP_IOCTL_NOTIF_RECV: IoctlRequest = 0xc0502100;
const SECCOMP_IOCTL_NOTIF_SEND: IoctlRequest = 0xc0182101;
*/

/*
 * Thread local variables
 */
thread_local! {
    static PRIORITY_SET: std::cell::Cell<bool> = const { std::cell::Cell::new(false) }
}

/*
 * Macros
 */
macro_rules! syscall_handler {
    ($request:expr, $body:expr) => {{
        let inner = |request: &UNotifyEventRequest| -> Result<libseccomp::ScmpNotifResp, Errno> {
            let (req, proc) = request.prepare();
            $body(req, &proc)
        };

        match inner($request) {
            Ok(result) => result,
            Err(error) => $request.fail_syscall(error as i32),
        }
    }};
}

macro_rules! remote_path_n {
    ($remote_process:expr, $request:expr, $n:expr, $event_request:expr) => {
        $remote_process.remote_path($request.data.args[$n] as usize, &$event_request)
    };
}

/// Seccomp sandbox profile export modes.
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum ExportMode {
    /// Berkeley Packet Filter (binary, machine readable)
    BerkeleyPacketFilter,
    /// Pseudo Filter Code (text, human readable)
    PseudoFiltercode,
}

impl FromStr for ExportMode {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "bpf" => Ok(Self::BerkeleyPacketFilter),
            "pfc" => Ok(Self::PseudoFiltercode),
            _ => bail!("Invalid export mode, expected one of bpf, or pfc!"),
        }
    }
}

/// `SyscallPathArgument` represents a system call path argument,
/// coupled with a directory file descriptor as necessary.
pub(crate) struct SyscallPathArgument {
    /// DirFd index in syscall args, if applicable.
    pub dirfd: Option<usize>,
    /// Path index in syscall args, if applicable.
    pub path: Option<usize>,
    /// Whether if it's ok for the path to be a NULL pointer.
    pub null: bool,
    /// Whether if it's ok for the path to be empty.
    pub empty: bool,
    /// Whether symbolic links should be resolved.
    pub resolve: bool,
    /// Whether dot as final component must return the given `Errno`.
    pub dotlast: Option<Errno>,
    /// Missing mode parameter for canonicalize.
    pub miss: MissingHandling,
}

impl Serialize for SyscallPathArgument {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(2))?;
        map.serialize_entry("dirfd", &self.dirfd)?;
        map.serialize_entry("path", &self.path)?;
        map.serialize_entry("null", &self.null)?;
        map.serialize_entry("empty", &self.empty)?;
        map.serialize_entry("resolve", &self.resolve)?;
        map.serialize_entry("dotlast", &self.dotlast.map(|e| e as i32))?;
        map.serialize_entry("miss", &self.miss)?;
        map.end()
    }
}

/// `UNotifyEventRequest` is the type of parameter that user's function
/// would get.
#[derive(Debug)]
pub struct UNotifyEventRequest {
    request: libseccomp::ScmpNotifReq,
    notify_fd: RawFd,
    sandbox: Arc<RwLock<Sandbox>>,
}

type RequestWithProcess<'a> = (&'a ScmpNotifReq, RemoteProcess);

impl UNotifyEventRequest {
    fn new(
        request: libseccomp::ScmpNotifReq,
        notify_fd: RawFd,
        sandbox: Arc<RwLock<Sandbox>>,
    ) -> Self {
        UNotifyEventRequest {
            request,
            notify_fd,
            sandbox,
        }
    }

    /// Given a `UNotifyEventRequest` return the seccomp request and a corresponding `RemoteProcess` instance.
    pub fn prepare(&self) -> RequestWithProcess {
        let req = self.get_request();
        #[allow(clippy::cast_possible_wrap)]
        let pid = Pid::from_raw(req.pid as i32);
        let proc = RemoteProcess::new(pid);
        (req, proc)
    }

    /// Returns the unotify request (`libseccomp::ScmpNotifReq`) of
    /// this event.
    pub fn get_request(&self) -> &libseccomp::ScmpNotifReq {
        &self.request
    }

    /// Returns the internal `Sandbox` object locking it as necessary,
    /// and wrapped in a `SandboxGuard`.
    pub fn get_sandbox(&self, write: bool) -> SandboxGuard {
        if write {
            SandboxGuard::Write(self.sandbox.write())
        } else {
            SandboxGuard::Read(self.sandbox.read())
        }
    }

    /// Let the kernel continue the syscall.
    ///
    /// # Safety
    /// CAUTION! This method is unsafe because it may suffer TOCTOU attack.
    /// Please read `seccomp_unotify(2)` "NOTES/Design goals; use of `SECCOMP_USER_NOTIF_FLAG_CONTINUE`"
    /// before using this method.
    pub unsafe fn continue_syscall(&self) -> libseccomp::ScmpNotifResp {
        libseccomp::ScmpNotifResp::new(self.request.id, 0, 0, ScmpNotifRespFlags::CONTINUE.bits())
    }

    /// Returns error to supervised process.
    /// `err` parameter should be a number larger than 0.
    pub fn fail_syscall(&self, err: i32) -> libseccomp::ScmpNotifResp {
        debug_assert!(err > 0);
        #[allow(clippy::arithmetic_side_effects)]
        libseccomp::ScmpNotifResp::new(self.request.id, 0, -err, 0)
    }

    /// Returns value to supervised process.
    pub fn return_syscall(&self, val: i64) -> libseccomp::ScmpNotifResp {
        libseccomp::ScmpNotifResp::new(self.request.id, val, 0, 0)
    }

    /// Check if this event is still valid.
    /// In some cases this is necessary, please check `seccomp_unotify(2)` for more information.
    pub fn is_valid(&self) -> bool {
        libseccomp::notify_id_valid(self.notify_fd, self.request.id).is_ok()
    }

    /// Add a file descriptor to the supervised process.
    /// This could help avoid TOCTOU attack in some cases.
    pub fn add_fd(&self, src_fd: RawFd, close_on_exec: bool) -> Result<RawFd, Errno> {
        #[allow(clippy::cast_sign_loss)]
        let src_fd = if src_fd >= 0 {
            src_fd as u32
        } else {
            return Err(Errno::EBADF);
        };
        let newfd_flags = if close_on_exec {
            nix::libc::O_CLOEXEC as u32
        } else {
            0
        };
        let addfd: libseccomp_sys::seccomp_notif_addfd = libseccomp_sys::seccomp_notif_addfd {
            id: self.request.id,
            flags: 0,
            srcfd: src_fd,
            newfd: 0,
            newfd_flags,
        };

        // SAFETY: The 'ioctl' function is a low-level interface to the
        // kernel, and its safety depends on the correctness of its
        // arguments.  Here, we ensure that 'self.notify_fd' is a valid
        // file descriptor and 'addr_of!(addfd)' provides a valid
        // pointer to 'addfd'.  The usage of ioctl is considered safe
        // under these conditions, as it does not lead to undefined
        // behavior.
        let new_fd = unsafe {
            ioctl(
                self.notify_fd,
                SECCOMP_IOCTL_NOTIF_ADDFD,
                std::ptr::addr_of!(addfd),
            )
        };
        if new_fd < 0 {
            Err(Errno::last())
        } else {
            Ok(new_fd as RawFd)
        }
    }
}

/// By using `RemoteProcess`, you can get some information about the supervised process.
#[derive(Debug)]
pub struct RemoteProcess {
    pid: Pid,
}

impl RemoteProcess {
    /// Create a `RemoteProcess` object from a `Pid`.
    ///
    /// # Examples
    ///
    /// ```ignore
    /// let remote = RemoteProcess::new(Pid::from_raw(req.request.pid as i32));
    /// ```
    pub fn new(pid: Pid) -> Self {
        RemoteProcess { pid }
    }

    /// Get file descriptor from remote process with `pidfd_getfd()`.
    /// This function requires Linux 5.6+.
    pub fn get_fd(&self, remote_fd: RawFd, req: &UNotifyEventRequest) -> Result<OwnedFd, Errno> {
        // Get the Thread Group ID of the given Thread ID.
        let tgid = proc_tgid(self.pid)?;

        // SAFETY: The call to `libc::syscall` for `SYS_pidfd_getfd` is
        // safe provided the arguments are correct.  Here, `self.fd` is
        // assumed to be a valid file descriptor, and `remote_fd` is a
        // valid RawFd. The third argument, `0`, is a valid flag for
        // the syscall. Assuming these conditions, the syscall does not
        // lead to undefined behavior.
        #[allow(clippy::cast_possible_truncation)]
        let pid_fd = match unsafe { libc::syscall(libc::SYS_pidfd_open, tgid, 0) } {
            e if e < 0 => return Err(Errno::last()),
            fd => {
                let fd = unsafe { OwnedFd::from_raw_fd(fd as RawFd) };
                if !req.is_valid() {
                    return Err(Errno::ESRCH);
                }
                fd
            }
        };
        let local_fd =
            unsafe { libc::syscall(libc::SYS_pidfd_getfd, pid_fd.as_raw_fd(), remote_fd, 0) };
        if local_fd < 0 {
            Err(Errno::last())
        } else {
            // SAFETY: Valid FD
            Ok(unsafe { OwnedFd::from_raw_fd(local_fd as RawFd) })
        }
    }

    /// Read data from remote process's memory with `process_vm_readv()`.
    pub fn read_mem(
        &self,
        local_buffer: &mut [u8],
        remote_addr: usize,
        request: &UNotifyEventRequest,
    ) -> Result<usize, Errno> {
        static FORCE_PROC: Lazy<bool> =
            Lazy::new(|| std::env::var(ENV_NO_CROSS_MEMORY_ATTACH).is_ok());
        if *FORCE_PROC {
            return self.read_mem_proc(local_buffer, remote_addr, request);
        }

        if remote_addr == 0 {
            return Err(Errno::EFAULT);
        }
        let len = local_buffer.len();
        match process_vm_readv(
            self.pid,
            &mut [IoSliceMut::new(local_buffer)],
            &[RemoteIoVec {
                len,
                base: remote_addr,
            }],
        ) {
            Ok(n) => {
                if request.is_valid() {
                    Ok(n)
                } else {
                    Err(Errno::ESRCH)
                }
            }
            Err(Errno::ENOSYS | Errno::EPERM) => {
                self.read_mem_proc(local_buffer, remote_addr, request)
            }
            Err(e) => Err(e),
        }
    }

    /// Write data to remote process's memory with `process_vm_writev()`.
    pub fn write_mem(
        &self,
        local_buffer: &[u8],
        remote_addr: usize,
        request: &UNotifyEventRequest,
    ) -> Result<usize, Errno> {
        static FORCE_PROC: Lazy<bool> =
            Lazy::new(|| std::env::var(ENV_NO_CROSS_MEMORY_ATTACH).is_ok());
        if *FORCE_PROC {
            return self.write_mem_proc(local_buffer, remote_addr, request);
        }

        if remote_addr == 0 {
            return Err(Errno::EFAULT);
        }
        let len = local_buffer.len();
        match process_vm_writev(
            self.pid,
            &[IoSlice::new(local_buffer)],
            &[RemoteIoVec {
                len,
                base: remote_addr,
            }],
        ) {
            Ok(n) => {
                if request.is_valid() {
                    Ok(n)
                } else {
                    Err(Errno::ESRCH)
                }
            }
            Err(Errno::ENOSYS | Errno::EPERM) => {
                self.write_mem_proc(local_buffer, remote_addr, request)
            }
            Err(e) => Err(e),
        }
    }

    /// Fallback method to read data from `/proc/$pid/mem` when `process_vm_readv()` is unavailable.
    pub fn read_mem_proc(
        &self,
        local_buffer: &mut [u8],
        remote_addr: usize,
        request: &UNotifyEventRequest,
    ) -> Result<usize, Errno> {
        if remote_addr == 0 {
            return Err(Errno::EFAULT);
        }
        let mut buf = itoa::Buffer::new();
        let mut path = PathBuf::from("/proc");
        path.push(buf.format(self.pid.as_raw()));
        path.push("mem");
        let mut file = File::open(path).map_err(|_| Errno::last())?;
        if !request.is_valid() {
            return Err(Errno::ESRCH);
        }
        file.seek(SeekFrom::Start(remote_addr as u64))
            .map_err(|_| Errno::last())?;
        file.read(local_buffer).map_err(|_| Errno::last())
    }

    /// Fallback method to write data to `/proc/$pid/mem` when `process_vm_writev()` is unavailable.
    fn write_mem_proc(
        &self,
        local_buffer: &[u8],
        remote_addr: usize,
        request: &UNotifyEventRequest,
    ) -> Result<usize, Errno> {
        if remote_addr == 0 {
            return Err(Errno::EFAULT);
        }
        let mut buf = itoa::Buffer::new();
        let mut path = PathBuf::from("/proc");
        path.push(buf.format(self.pid.as_raw()));
        path.push("mem");
        let mut file = OpenOptions::new()
            .write(true)
            .open(path)
            .map_err(|_| Errno::last())?;
        if !request.is_valid() {
            return Err(Errno::ESRCH);
        }
        file.seek(SeekFrom::Start(remote_addr as u64))
            .map_err(|_| Errno::last())?;
        file.write(local_buffer).map_err(|_| Errno::last())
    }

    /// Read path from the given system call argument with the given request,
    /// returning together the optional raw system call argument.
    #[allow(clippy::type_complexity)]
    fn read_path_opt(
        &self,
        request: &UNotifyEventRequest,
        arg: &SyscallPathArgument,
    ) -> Result<(PathBuf, Option<PathBuf>), Errno> {
        let req = request.get_request();
        let orig = match arg.path {
            Some(idx) => {
                if req.data.args[idx] == 0 {
                    if arg.null {
                        // NULL is ok, use fd
                        None
                    } else {
                        return Err(Errno::EFAULT);
                    }
                } else {
                    Some(remote_path_n!(self, req, idx, request)?)
                }
            }
            None => None,
        };
        let mut doterr = false;
        let (path, path_raw) = if let Some(ref path) = orig {
            if !arg.empty && path.is_empty() {
                return Err(Errno::ENOENT);
            }
            let ends_with_dot = path_ends_with_dot(path);
            if ends_with_dot {
                match arg.dotlast {
                    Some(Errno::ENOENT) => {
                        // This will be handled later,
                        // as we may need to return EEXIST instead
                        // of ENOENT if the path exists.
                        doterr = true;
                    }
                    Some(errno) => {
                        return Err(errno);
                    }
                    _ => {}
                }
            }
            #[allow(clippy::cast_possible_wrap)]
            let pid = Pid::from_raw(req.pid as i32);
            let mut path = PathBuf::from(OsStr::from_bytes(path.to_bytes()));
            let path_raw = Some(path.clone());
            if path.is_absolute() {
                if !path.starts_with(MAGIC_PREFIX) {
                    path = canonicalize(pid, path, arg.resolve, arg.miss)?;
                }
            } else if path.is_empty() && !ends_with_dot {
                // SAFETY: The ends_with_dot check above
                // ensures we return ENOTDIR when e.g. path is
                // a dot and the file descriptor argument is a
                // regular file. This happens because in this
                // case, joining the directory with an empty
                // path on the next branch essentially adds a
                // trailing slash to the path, making the
                // system call emulator fail with ENOTDIR if
                // the argument is not a directory. This way,
                // we avoid stat'ing the path here to
                // determine whether it's a directory or not.
                path = self.read_directory(request, arg)?;
            } else if path == Path::new(".") {
                // SAFETY: Add a trailing slash to the directory
                // to assert it must be a directory.
                path = self.read_directory(request, arg)?.join("");
            } else {
                path = self.read_directory(request, arg)?.join(&path);
                path = canonicalize(pid, path, arg.resolve, arg.miss)?;
            };
            if doterr {
                // Delayed dotlast Errno::ENOENT handler, see above for the rationale.
                if access(&path, AccessFlags::F_OK).is_ok() {
                    return Err(Errno::EEXIST);
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            (path, path_raw)
        } else {
            (self.read_directory(request, arg)?, None)
        };

        trace!("ctx": "read_path",
            "path": format!("{}", path.display()),
            "orig": format!("{}", SydCStr(orig.as_deref().unwrap_or(CString::default().as_c_str()))),
            "arg": arg);
        Ok((path, path_raw))
    }

    /// Read path from the given system call argument with the given request.
    #[allow(clippy::cognitive_complexity)]
    fn read_path(
        &self,
        request: &UNotifyEventRequest,
        arg: &SyscallPathArgument,
    ) -> Result<PathBuf, Errno> {
        let req = request.get_request();
        let orig = match arg.path {
            Some(idx) => {
                if req.data.args[idx] == 0 {
                    if arg.null {
                        // NULL is ok, use fd
                        None
                    } else {
                        return Err(Errno::EFAULT);
                    }
                } else {
                    Some(remote_path_n!(self, req, idx, request)?)
                }
            }
            None => None,
        };
        let mut doterr = false;
        let path = if let Some(ref path) = orig {
            if !arg.empty && path.is_empty() {
                return Err(Errno::ENOENT);
            }
            let ends_with_dot = path_ends_with_dot(path);
            if ends_with_dot {
                match arg.dotlast {
                    Some(Errno::ENOENT) => {
                        // This will be handled later,
                        // as we may need to return EEXIST instead
                        // of ENOENT if the path exists.
                        doterr = true;
                    }
                    Some(errno) => {
                        return Err(errno);
                    }
                    _ => {}
                }
            }
            let mut path = PathBuf::from(OsStr::from_bytes(path.to_bytes()));
            #[allow(clippy::cast_possible_wrap)]
            let pid = Pid::from_raw(req.pid as i32);
            if path.is_absolute() {
                path = canonicalize(pid, path, arg.resolve, arg.miss)?;
            } else if path.is_empty() && !ends_with_dot {
                // SAFETY: The ends_with_dot check above
                // ensures we return ENOTDIR when e.g. path is
                // a dot and the file descriptor argument is a
                // regular file. This happens because in this
                // case, joining the directory with an empty
                // path on the next branch essentially adds a
                // trailing slash to the path, making the
                // system call emulator fail with ENOTDIR if
                // the argument is not a directory. This way,
                // we avoid stat'ing the path here to
                // determine whether it's a directory or not.
                path = self.read_directory(request, arg)?;
            } else if path == Path::new(".") {
                // SAFETY: Add a trailing slash to the directory
                // to assert it must be a directory.
                path = self.read_directory(request, arg)?.join("");
            } else {
                path = self.read_directory(request, arg)?.join(&path);
                path = canonicalize(pid, path, arg.resolve, arg.miss)?;
            };
            if doterr {
                // Delayed dotlast Errno::ENOENT handler, see above for the rationale.
                if access(&path, AccessFlags::F_OK).is_ok() {
                    return Err(Errno::EEXIST);
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            path
        } else {
            self.read_directory(request, arg)?
        };

        trace!("ctx": "read_path",
            "path": format!("{}", path.display()),
            "orig": format!("{}", SydCStr(orig.as_deref().unwrap_or(CString::default().as_c_str()))),
            "arg": arg);
        Ok(path)
    }

    /// Read directory from the given system call argument with the given request.
    fn read_directory(
        &self,
        request: &UNotifyEventRequest,
        arg: &SyscallPathArgument,
    ) -> Result<PathBuf, Errno> {
        let req = request.get_request();
        let sym = match arg.dirfd {
            Some(idx) => Self::remote_dirfd(req.pid, Self::remote_fd(req, idx)),
            None => Self::remote_dirfd(req.pid, None),
        };
        match read_link(&sym) {
            Ok(dir) => {
                Ok(if dir.is_relative() {
                    sym // /proc/1/fd/0 -> pipe:42
                } else {
                    dir // genuine dir
                })
            }
            Err(Errno::ENOENT) => Err(Errno::EBADF),
            Err(errno) => Err(errno),
        }
    }

    /// Convert the file descriptor argument into a `RawFd`.
    /// If the argument refers to the current working directory,
    /// this function returns `None`.
    pub fn remote_fd(req: &ScmpNotifReq, idx: usize) -> Option<RawFd> {
        #[allow(clippy::cast_possible_truncation)]
        let fd = req.data.args[idx] as i32;
        if fd == AT_FDCWD {
            None
        } else {
            Some(fd as RawFd)
        }
    }

    /// Returns the file descriptor or current working directory path for the given `Pid`.
    pub fn remote_dirfd(pid: u32, fd: Option<RawFd>) -> PathBuf {
        let mut buf0 = itoa::Buffer::new();
        let mut path = PathBuf::from("/proc");
        path.push(buf0.format(pid));

        if let Some(fd) = fd {
            let mut buf1 = itoa::Buffer::new();
            path.push("fd");
            path.push(buf1.format(fd));
        } else {
            path.push("cwd");
        }

        path
    }

    /// Read the `nix::libc::open_how` struct from process memory
    /// at the given address and size.
    pub fn remote_ohow(
        &self,
        addr: usize,
        size: usize,
        request: &UNotifyEventRequest,
    ) -> Result<nix::libc::open_how, Errno> {
        if size != std::mem::size_of::<nix::libc::open_how>() {
            return Err(Errno::EINVAL);
        }

        let mut buf = [0u8; std::mem::size_of::<nix::libc::open_how>()];
        self.read_mem(&mut buf, addr, request)?;

        // SAFETY: The following unsafe block assumes that:
        // 1. The memory layout of open_how in our Rust environment matches that of the target process.
        // 2. The proc.read_mem call has populated buf with valid data of the appropriate size (ensured by the size check above).
        // 3. The buffer is appropriately aligned for reading an open_how struct. If the remote process's representation of open_how
        //    was correctly aligned, our local buffer should be too, since it's an array on the stack.
        Ok(unsafe { std::ptr::read_unaligned(buf.as_ptr() as *const _) })
    }

    /// Read the `nix::libc::utimbuf` struct from process memory at the given address.
    /// Convert it to a `nix::libc::timespec[2]` for easy interoperability.
    fn remote_utimbuf(
        &self,
        addr: usize,
        request: &UNotifyEventRequest,
    ) -> Result<Option<[nix::libc::timespec; 2]>, Errno> {
        if addr == 0 {
            return Ok(None);
        }

        let mut buf = [0u8; std::mem::size_of::<nix::libc::utimbuf>()];
        self.read_mem(&mut buf, addr, request)?;

        // SAFETY: The following unsafe block assumes that:
        // 1. The memory layout of utimbuf in our Rust environment matches that of the target process.
        // 2. The proc.read_mem call has populated buf with valid data of the appropriate size (ensured by the size check above).
        // 3. The buffer is appropriately aligned for reading a utimbuf struct. If the remote process's representation of utimbuf
        //    was correctly aligned, our local buffer should be too, since it's an array on the stack.
        let t: nix::libc::utimbuf = unsafe { std::ptr::read_unaligned(buf.as_ptr() as *const _) };
        Ok(Some([
            nix::libc::timespec {
                tv_sec: t.actime,
                tv_nsec: 0,
            },
            nix::libc::timespec {
                tv_sec: t.modtime,
                tv_nsec: 0,
            },
        ]))
    }

    /// Read the `nix::libc::timeval[2]` struct from process memory at the given address.
    /// Convert it to a `nix::libc::timespec[2]` for easy interoperability.
    fn remote_timeval(
        &self,
        addr: usize,
        request: &UNotifyEventRequest,
    ) -> Result<Option<[nix::libc::timespec; 2]>, Errno> {
        if addr == 0 {
            return Ok(None);
        }

        let mut buf = [0u8; std::mem::size_of::<nix::libc::timeval>() * 2];
        self.read_mem(&mut buf, addr, request)?;

        // SAFETY: The following unsafe block assumes that:
        // 1. The memory layout of timeval in our Rust environment matches that of the target process.
        // 2. The proc.read_mem call has populated buf with valid data of the appropriate size (ensured by the size check above).
        // 3. The buffer is appropriately aligned for reading a timeval struct. If the remote process's representation of timeval
        //    was correctly aligned, our local buffer should be too, since it's an array on the stack.
        let t: [nix::libc::timeval; 2] =
            unsafe { std::ptr::read_unaligned(buf.as_ptr() as *const _) };
        Ok(Some([
            nix::libc::timespec {
                tv_sec: t[0].tv_sec,
                tv_nsec: (t[0].tv_usec as nix::libc::c_long).saturating_mul(1_000), /* ms->ns */
            },
            nix::libc::timespec {
                tv_sec: t[1].tv_sec,
                tv_nsec: (t[1].tv_usec as nix::libc::c_long).saturating_mul(1_000), /* ms->ns */
            },
        ]))
    }

    /// Read the `nix::libc::timespec[2]` struct from process memory at the given address.
    fn remote_timespec(
        &self,
        addr: usize,
        request: &UNotifyEventRequest,
    ) -> Result<Option<[nix::libc::timespec; 2]>, Errno> {
        if addr == 0 {
            return Ok(None);
        }

        let mut buf = [0u8; std::mem::size_of::<nix::libc::timespec>() * 2];
        self.read_mem(&mut buf, addr, request)?;

        // SAFETY: The following unsafe block assumes that:
        // 1. The memory layout of timespec in our Rust environment matches that of the target process.
        // 2. The proc.read_mem call has populated buf with valid data of the appropriate size (ensured by the size check above).
        // 3. The buffer is appropriately aligned for reading a timespec struct. If the remote process's representation of timespec
        //    was correctly aligned, our local buffer should be too, since it's an array on the stack.
        Ok(Some(unsafe {
            std::ptr::read_unaligned(buf.as_ptr() as *const _)
        }))
    }

    /// Read the path from memory of the process with the given `Pid` with the given address.
    fn remote_path(&self, addr: usize, request: &UNotifyEventRequest) -> Result<Box<CStr>, Errno> {
        let mut buf = [0u8; nix::libc::PATH_MAX as usize];
        self.read_mem(&mut buf, addr, request)?;

        // SAFETY: If buffer has no null byte,
        // return ENAMETOOLONG as the path is too
        // long for us to handle.
        Ok(Box::from(
            CStr::from_bytes_until_nul(&buf)
                .map_err(|_| Errno::ENAMETOOLONG)?
                .to_owned(),
        ))
    }
}

type ChildHandle = JoinHandle<Result<(), Errno>>;
type Child = (Pid, ChildHandle, ThreadPool);
type UserHookFunc = Box<dyn Fn(&UNotifyEventRequest) -> libseccomp::ScmpNotifResp + Send + Sync>;
type HandlerMap = HashMap<Sydcall, (&'static str, Arc<UserHookFunc>)>;

/// supervisor of a syd sandbox.
pub struct Supervisor {
    export: Option<ExportMode>,
    handlers: HandlerMap,
    sysallow: HashSet<ScmpSyscall>,
    thread_pool: ThreadPool,
    sandbox: Arc<RwLock<Sandbox>>,
}

impl Supervisor {
    /// Create a new `Supervisor` object. You can specify the number of threads in the thread pool.
    /// This function will also check your kernel version and show warning or return error if necessary.
    ///
    /// # Examples
    /// ```
    /// use syd::{hook::Supervisor, sandbox::Sandbox};
    /// let supervisor = Supervisor::new(Sandbox::default(), num_cpus::get(), None).unwrap();
    /// ```
    pub fn new(
        sandbox: Sandbox,
        thread_num: usize,
        export_mode: Option<ExportMode>,
    ) -> Result<Self, io::Error> {
        if thread_num == 0 {
            return Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                "thread_num should be greater than 0",
            ));
        }
        // detect kernel version and show warning
        let version = uname().map_err(|e| io::Error::from_raw_os_error(e as i32))?;
        let version = version.release();

        macro_rules! parse_error {
            () => {
                io::Error::new(io::ErrorKind::Other, "unknown version")
            };
        }

        let (major, minor) = {
            let mut iter = version.to_str().ok_or_else(|| parse_error!())?.split('.');
            let major = iter
                .next()
                .ok_or_else(|| parse_error!())?
                .parse::<u32>()
                .map_err(|_| parse_error!())?;
            let minor = iter
                .next()
                .ok_or_else(|| parse_error!())?
                .parse::<u32>()
                .map_err(|_| parse_error!())?;
            (major, minor)
        };
        if major < 5 {
            error!("ctx": "check_kernel", "error": "Your kernel version is too old.");
            return Err(io::Error::new(io::ErrorKind::Other, "kernel too old"));
        } else if major == 5 && minor < 5 {
            error!("ctx": "check_kernel", "error": "Your kernel version is too old (Does not support SECCOMP_USER_NOTIF_FLAG_CONTINUE, etc.).");
            return Err(io::Error::new(io::ErrorKind::Other, "kernel too old"));
        } else if major == 5 && minor < 6 {
            error!("ctx": "check_kernel", "error": "Your kernel version is too old (Does not support pidfd_getfd() and SECCOMP_IOCTL_NOTIF_ADDFD).");
            return Err(io::Error::new(io::ErrorKind::Other, "kernel too old"));
        } else if major == 5 && minor < 9 {
            error!("ctx": "check_kernel", "error": "Your kernel version is too old (Does not support SECCOMP_IOCTL_NOTIF_ADDFD).");
            return Err(io::Error::new(io::ErrorKind::Other, "kernel too old"));
        }
        Ok(Supervisor {
            export: export_mode,
            handlers: HashMap::new(),
            sysallow: HashSet::new(),
            thread_pool: rusty_pool::Builder::new()
                .name("syd".to_string())
                .core_size(thread_num)
                .max_size(rusty_pool::MAX_SIZE)
                .keep_alive(MON_KEEPALIVE)
                .build(),
            sandbox: Arc::new(RwLock::new(sandbox)),
        })
    }

    /// Initialize the environment for the sandbox.
    /// Call this before `init`.
    #[allow(clippy::cognitive_complexity)]
    pub fn init_env(flags: Flag, unshare_user: bool) -> anyhow::Result<()> {
        // SAFETY: Set the no_new_privs attribute.
        if unsafe { nix::libc::prctl(nix::libc::PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0) } != 0 {
            let errno = Errno::last();
            bail!("Error setting no_new_privs attribute: {errno}");
        }
        debug!("ctx": "init", "no_new_privs": true);

        // SAFETY: Set parent-death signal.
        if unsafe { nix::libc::prctl(nix::libc::PR_SET_PDEATHSIG, nix::libc::SIGKILL, 0, 0, 0) }
            != 0
        {
            let errno = Errno::last();
            bail!("Error setting parent-death signal: {errno}");
        }
        debug!("ctx": "init", "parent_death": nix::libc::SIGKILL);

        // SAFETY: Register as a process subreaper if we're not already pid1.
        // This is important because otherwise processes will be
        // reparented to the actual pid1, after which we can no longer
        // access their /proc/pid/mem without ptrace rights.
        if Pid::this().as_raw() != 1 {
            if unsafe { nix::libc::prctl(nix::libc::PR_SET_CHILD_SUBREAPER, 1, 0, 0, 0) } == 0 {
                debug!("ctx": "init", "subreaper": true);
            } else {
                let errno = Errno::last();
                bail!("Error registering as a process subreaper: {errno}");
            }
        }

        let restrict_ptrace = !flags.contains(Flag::FL_ALLOW_UNSAFE_PTRACE);
        if !flags.contains(Flag::FL_ALLOW_UNSAFE_CAPS) {
            for &cap in CAPS_DROP {
                match cap {
                    caps::Capability::CAP_DAC_OVERRIDE if !unshare_user => continue,
                    caps::Capability::CAP_NET_RAW
                        if flags.contains(Flag::FL_ALLOW_UNSAFE_SOCKET) =>
                    {
                        continue
                    }
                    caps::Capability::CAP_SYS_PTRACE if !restrict_ptrace => continue,
                    caps::Capability::CAP_SYS_TIME
                        if flags.contains(Flag::FL_ALLOW_UNSAFE_ADJTIME) =>
                    {
                        continue
                    }
                    _ => {}
                }

                // SAFETY: Drop capabilities as early as possible.
                if let Ok(true) = caps::has_cap(None, caps::CapSet::Permitted, cap) {
                    let _ = caps::drop(None, caps::CapSet::Effective, cap);
                    info!("ctx": "init", "cap_drop" : cap.to_string());
                }
            }
        }
        let capeff = caps::read(None, caps::CapSet::Effective).unwrap_or_default();
        let _ = caps::set(None, caps::CapSet::Inheritable, &capeff);
        let capeff = capeff
            .into_iter()
            .map(|cap| cap.to_string())
            .collect::<Vec<_>>();
        if !capeff.is_empty() {
            info!("ctx": "init", "caps": capeff, "type": "effective");
        } else {
            debug!("ctx": "init", "caps": capeff, "type": "effective");
        }

        // Apply seccomp hardening for the syd process itself.
        let restrict_perf = !flags.contains(Flag::FL_ALLOW_UNSAFE_PERF);
        Self::setup_seccomp_parent(restrict_perf, restrict_ptrace)
            .context("Failed to create parent seccomp filter")?
            .load()
            .context("Failed to load parent seccomp filter")?;

        Ok(())
    }

    /// Initilizes the supervisor by adding the system call handlers.
    pub fn init(&mut self) -> &mut Self {
        // For performance reasons, we apply pid and memory sandboxing at startup only.
        let sandbox = self.sandbox.read();
        let has_mem = sandbox.enabled(Capability::CAP_MEM);
        let has_pid = sandbox.enabled(Capability::CAP_PID);
        drop(sandbox); // release the read lock.
        if has_mem {
            // memory sandboxing
            self.insert_handler("brk", sys_brk);
            self.insert_handler("mmap", sys_mmap);
            self.insert_handler("mmap2", sys_mmap2);
            self.insert_handler("mremap", sys_mremap);
        }
        if has_pid {
            // PID sandboxing
            self.insert_handler("fork", sys_fork);
            self.insert_handler("vfork", sys_vfork);
            self.insert_handler("clone", sys_clone);
            self.insert_handler("clone3", sys_clone3);
        }

        // signal protection
        self.insert_handler("kill", sys_kill);
        self.insert_handler("tkill", sys_tkill);
        self.insert_handler("tgkill", sys_tgkill);
        self.insert_handler("pidfd_open", sys_pidfd_open);

        // network sandboxing
        self.insert_handler("socketcall", sys_socketcall);
        self.insert_handler("bind", sys_bind);
        self.insert_handler("connect", sys_connect);
        self.insert_handler("recvfrom", sys_recvfrom);
        self.insert_handler("sendto", sys_sendto);

        // exec sandboxing
        self.insert_handler("execve", sys_execve);
        self.insert_handler("execveat", sys_execveat);

        // stat sandboxing
        self.insert_handler("chdir", sys_chdir);
        self.insert_handler("fchdir", sys_fchdir);
        self.insert_handler("getdents", sys_getdents);
        self.insert_handler("getdents64", sys_getdents);
        self.insert_handler("stat", sys_stat);
        self.insert_handler("stat64", sys_stat);
        self.insert_handler("fstat", sys_fstat);
        self.insert_handler("fstat64", sys_fstat);
        self.insert_handler("lstat", sys_lstat);
        self.insert_handler("lstat64", sys_lstat);
        self.insert_handler("statx", sys_statx);
        self.insert_handler("fstatat64", sys_newfstatat);
        self.insert_handler("newfstatat", sys_newfstatat);
        self.insert_handler("readlink", sys_readlink);
        self.insert_handler("readlinkat", sys_readlinkat);

        // read/write sandboxing
        self.insert_handler("access", sys_access);
        self.insert_handler("faccessat", sys_faccessat);
        self.insert_handler("faccessat2", sys_faccessat2);
        self.insert_handler("chmod", sys_chmod);
        self.insert_handler("fchmod", sys_fchmod);
        //We support flags in fchmodat to be nice.
        self.insert_handler("fchmodat", sys_fchmodat2);
        //TODO: libseccomp does not support this yet.
        //self.insert_handler("fchmodat2", sys_fchmodat2);
        self.insert_handler("chown", sys_chown);
        self.insert_handler("fchown", sys_fchown);
        self.insert_handler("fchown32", sys_fchown);
        self.insert_handler("lchown", sys_lchown);
        self.insert_handler("lchown32", sys_lchown);
        self.insert_handler("fchownat", sys_fchownat);
        self.insert_handler("creat", sys_creat);
        self.insert_handler("link", sys_link);
        self.insert_handler("symlink", sys_symlink);
        self.insert_handler("unlink", sys_unlink);
        self.insert_handler("linkat", sys_linkat);
        self.insert_handler("symlinkat", sys_symlinkat);
        self.insert_handler("unlinkat", sys_unlinkat);
        self.insert_handler("mkdir", sys_mkdir);
        self.insert_handler("rmdir", sys_rmdir);
        self.insert_handler("mkdirat", sys_mkdirat);
        self.insert_handler("mknod", sys_mknod);
        self.insert_handler("mknodat", sys_mknodat);
        self.insert_handler("mount", sys_mount);
        self.insert_handler("umount", sys_umount);
        self.insert_handler("umount2", sys_umount2);
        self.insert_handler("open", sys_open);
        self.insert_handler("openat", sys_openat);
        self.insert_handler("openat2", sys_openat2);
        self.insert_handler("rename", sys_rename);
        self.insert_handler("renameat", sys_renameat);
        self.insert_handler("renameat2", sys_renameat2);
        self.insert_handler("utime", sys_utime);
        self.insert_handler("utimes", sys_utimes);
        self.insert_handler("futimesat", sys_futimesat);
        self.insert_handler("utimensat", sys_utimensat);
        self.insert_handler("truncate", sys_truncate);
        self.insert_handler("truncate64", sys_truncate);
        self.insert_handler("getxattr", sys_getxattr);
        self.insert_handler("fgetxattr", sys_fgetxattr);
        self.insert_handler("lgetxattr", sys_lgetxattr);
        self.insert_handler("setxattr", sys_setxattr);
        self.insert_handler("fsetxattr", sys_fsetxattr);
        self.insert_handler("lsetxattr", sys_lsetxattr);
        self.insert_handler("listxattr", sys_listxattr);
        self.insert_handler("flistxattr", sys_flistxattr);
        self.insert_handler("llistxattr", sys_llistxattr);
        self.insert_handler("removexattr", sys_removexattr);
        self.insert_handler("fremovexattr", sys_fremovexattr);
        self.insert_handler("lremovexattr", sys_lremovexattr);
        self.insert_handler("getrandom", sys_getrandom);

        // Allowlist safe system calls.
        for sysname in SAFE_SYSCALLS {
            let syscall = ScmpSyscall::new(sysname);
            if i32::from(syscall) == libseccomp_sys::__NR_SCMP_ERROR {
                error!("ctx": "allow_safe_syscall", "error": "scmp_error", "sys": sysname);
                continue;
            }
            self.allow_syscall(syscall);
        }

        self
    }

    /// Insert this system call to the list of allowed system calls.
    /// No filtering is done one these system calls and they're allowed at the kernel level.
    pub fn allow_syscall(&mut self, syscall: ScmpSyscall) {
        self.sysallow.insert(syscall);
    }

    ///
    /// # Examples
    ///
    /// ```no_run
    /// use libseccomp::ScmpSyscall;
    /// use syd::{
    ///     hook::{Supervisor, UNotifyEventRequest},
    ///     sandbox::Sandbox,
    /// };
    ///
    /// fn close_handler(req: &UNotifyEventRequest) -> libseccomp::ScmpNotifResp {
    ///     println!("close");
    ///     unsafe { req.continue_syscall() }
    /// }
    ///
    /// let mut supervisor = Supervisor::new(Sandbox::default(), num_cpus::get(), None).unwrap();
    /// supervisor.insert_handler("open", |req| {
    ///     println!("open: {}", req.get_request().data.args[0]);
    ///     unsafe { req.continue_syscall() }
    /// });
    /// supervisor.insert_handler("close", close_handler);
    /// ```
    pub fn insert_handler(
        &mut self,
        syscall_name: &'static str,
        handler: impl Fn(&UNotifyEventRequest) -> libseccomp::ScmpNotifResp
            + Clone
            + Send
            + Sync
            + 'static,
    ) {
        for arch in SCMP_ARCH {
            if let Ok(sys) = ScmpSyscall::from_name_by_arch(syscall_name, *arch) {
                self.handlers.insert(
                    Sydcall(sys, *arch),
                    (syscall_name, Arc::new(Box::new(handler.clone()))),
                );
            }
        }
    }

    /// Run a command with seccomp filter.
    /// This method will fork a child process, do some preparations and run the command in it.
    /// It returns a `Pid`, a `JoinHandle` of supervising thread, and a `ThreadPool` handle of syscall user functions.
    /// It's recommended to use `Supervisor::wait()` to wait for the child process.
    pub fn spawn(self, mut command: crate::unshare::Command) -> anyhow::Result<Child> {
        let seccomp_filter = self
            .setup_seccomp()
            .context("Failed to set up seccomp filters.")?;
        command.seccomp_filter(seccomp_filter);

        // Spawn child under sandbox.
        let sandbox = command
            .spawn()
            .context("Failed to spawn sandbox process.")?;
        let pid = sandbox.id();
        #[allow(clippy::disallowed_methods)]
        let fd = sandbox.seccomp_fd;

        // Ignore some signals to ensure uniterrupted tracing.
        let _ = crate::ignore_signal(SIGTSTP);
        let _ = crate::ignore_signal(SIGTTIN);
        let _ = crate::ignore_signal(SIGTTOU);
        let _ = crate::ignore_signal(SIGHUP);

        #[allow(clippy::cast_possible_wrap)]
        let pid = Pid::from_raw(pid as i32);
        let mut sandbox = self.sandbox.write();
        sandbox.set_child_pid(pid);
        drop(sandbox);

        self.supervise(pid, fd)
    }

    // Set up seccomp for the sandbox process.
    #[allow(clippy::cognitive_complexity)]
    fn setup_seccomp(&self) -> IOResult<ScmpFilterContext> {
        let mut ctx =
            ScmpFilterContext::new_filter(ScmpAction::Errno(nix::libc::EACCES)).map_err(|e| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!("failed to create seccomp filter: {}", e),
                )
            })?;
        // Enforce the NO_NEW_PRIVS functionality before
        // loading the seccomp filter into the kernel.
        let _ = ctx.set_ctl_nnp(true);
        // We don't want ECANCELED, we want actual errnos.
        let _ = ctx.set_api_sysrawrc(true);
        // We deny with EACCES for bad system call, and kill process for bad arch.
        let _ = ctx.set_act_badarch(ScmpAction::KillProcess);
        // We log all filter actions other than Allow to kernel log if the log level is <=Debug.
        if log_enabled!(Level::Debug) {
            let _ = ctx.set_ctl_log(true);
        }

        seccomp_add_architectures(&mut ctx)?;

        // Acquire the read lock to sandbox configuration.
        let sandbox = self.sandbox.read();
        let fake_root = sandbox.get_fake_root();
        let has_mem = sandbox.enabled(Capability::CAP_MEM);
        let has_pid = sandbox.enabled(Capability::CAP_PID);
        drop(sandbox); // release the read lock.

        // Allow memory and pid calls unless sandboxing is on.
        let mut allow_calls: SmallVec<[&str; 8]> = smallvec![];
        if !has_mem {
            allow_calls.extend(["brk", "mmap", "mmap2", "mremap"]);
        }
        if !has_pid {
            allow_calls.extend(["fork", "vfork", "clone", "clone3"]);
        }
        for sysname in allow_calls {
            let syscall = ScmpSyscall::new(sysname);
            ctx.add_rule(ScmpAction::Allow, syscall).map_err(|e| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!(
                        "failed to add rule for system call {} ( {} ): {}",
                        syscall, sysname, e
                    ),
                )
            })?;
        }

        // Fakeroot
        let id_action = if fake_root {
            ScmpAction::Errno(0)
        } else {
            ScmpAction::Allow
        };
        for sysname in GET_ID_SYSCALLS {
            let syscall = ScmpSyscall::new(sysname);
            ctx.add_rule(id_action, syscall).map_err(|e| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!(
                        "failed to add rule for system call {} ( {} ): {}",
                        syscall, sysname, e
                    ),
                )
            })?;
        }

        // Add notify rules for system calls with handlers.
        let syscall_notif: HashSet<&'static str> = self.handlers
            .values()
            .map(|(name, _)| *name) // Extract the name from values
            .collect(); // Collect names into a HashSet to ensure uniqueness
        for name in syscall_notif {
            let syscall = ScmpSyscall::new(name);
            let _ = ctx.set_syscall_priority(syscall, crate::syscall_priority(name));
            ctx.add_rule(ScmpAction::Notify, syscall).map_err(|e| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!(
                        "failed to add rule for system call {} ( {} ): {}",
                        syscall, name, e
                    ),
                )
            })?;
        }

        // Add allow rules for system calls in the default allow list.
        let syscall_allow: Vec<_> = self.sysallow.iter().copied().collect();
        for syscall in &syscall_allow {
            ctx.add_rule(ScmpAction::Allow, *syscall).map_err(|e| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!(
                        "failed to add rule for system call {} ( {} ): {}",
                        *syscall,
                        ScmpSyscall::get_name(*syscall).unwrap_or("?".to_string()),
                        e
                    ),
                )
            })?;
        }

        // Apply sandbox restrictions as necessary.
        let sandbox = self.sandbox.read();
        let deny_tsc = sandbox.deny_tsc();
        let restrict_ioctl = !sandbox.allow_unsafe_ioctl();
        let restrict_prctl = !sandbox.allow_unsafe_prctl();
        let restrict_prlimit = !sandbox.allow_unsafe_prlimit();
        let restrict_adjtime = !sandbox.allow_unsafe_adjtime();
        let restrict_iouring = !sandbox.allow_unsafe_iouring();
        let restrict_perf = !sandbox.allow_unsafe_perf();
        let restrict_ptrace = !sandbox.allow_unsafe_ptrace();
        drop(sandbox);

        // Restriction 0: Turn compiled-in list of noop syscalls into no-ops (see config.rs)
        // Restriction 1: Turn set*id syscalls into no-ops.
        for sysname in SET_ID_SYSCALLS.iter().chain(NOOP_SYSCALLS) {
            let syscall = ScmpSyscall::new(sysname);
            ctx.add_rule(ScmpAction::Errno(0), syscall)
                .map_err(|error| {
                    io::Error::new(io::ErrorKind::Other, format!("SeccompError: {error}"))
                })?;
        }

        // Restriction 2: Allowlist known-safe ioctls.
        #[allow(clippy::disallowed_methods)]
        let sys_ioctl = ScmpSyscall::from_name("ioctl").unwrap();
        if restrict_ioctl {
            #[allow(clippy::unnecessary_cast)]
            for opt in ALLOWLIST_IOCTL.iter().map(|(_, k)| *k as u64) {
                let cmp = ScmpArgCompare::new(1, ScmpCompareOp::Equal, opt);
                ctx.add_rule_conditional(ScmpAction::Allow, sys_ioctl, &[cmp])
                    .map_err(|error| {
                        io::Error::new(io::ErrorKind::Other, format!("SeccompError: {error}"))
                    })?;
            }
        } else {
            ctx.add_rule(ScmpAction::Allow, sys_ioctl)
                .map_err(|error| {
                    io::Error::new(io::ErrorKind::Other, format!("SeccompError: {error}"))
                })?;
        }

        // Restriction 3: Allowlist known-safe prctls.
        // Things like PR_SET_MM, PR_SET_PTRACER, and PR_SET_SPECULATION_CTRL are left out.
        // PR_SET_TSC is out if deny-tsc is set and allow_unsafe_prctl is unset.
        #[allow(clippy::disallowed_methods)]
        let sys_prctl = ScmpSyscall::from_name("prctl").unwrap();
        if restrict_prctl {
            for (name, opt) in ALLOWLIST_PRCTL {
                if deny_tsc && *name == "PR_SET_TSC" {
                    continue;
                }
                let cmp = ScmpArgCompare::new(0, ScmpCompareOp::Equal, *opt);
                ctx.add_rule_conditional(ScmpAction::Allow, sys_prctl, &[cmp])
                    .map_err(|error| {
                        io::Error::new(io::ErrorKind::Other, format!("SeccompError: {error}"))
                    })?;
            }
        } else {
            ctx.add_rule(ScmpAction::Allow, sys_prctl)
                .map_err(|error| {
                    io::Error::new(io::ErrorKind::Other, format!("SeccompError: {error}"))
                })?;
        }

        // Restriction 4: Disallow prlimit from setting resources.
        #[allow(clippy::disallowed_methods)]
        let sys_prlimit = ScmpSyscall::from_name("prlimit64").unwrap();
        if restrict_prlimit {
            ctx.add_rule_conditional(
                ScmpAction::Allow,
                sys_prlimit,
                &[ScmpArgCompare::new(2, ScmpCompareOp::Equal, 0)],
            )
            .map_err(|error| {
                io::Error::new(io::ErrorKind::Other, format!("SeccompError: {error}"))
            })?;
        } else {
            #[allow(clippy::disallowed_methods)]
            let sys_setrlimit = ScmpSyscall::from_name("setrlimit").unwrap();

            ctx.add_rule(ScmpAction::Allow, sys_prlimit)
                .map_err(|error| {
                    io::Error::new(io::ErrorKind::Other, format!("SeccompError: {error}"))
                })?;
            ctx.add_rule(ScmpAction::Allow, sys_setrlimit)
                .map_err(|error| {
                    io::Error::new(io::ErrorKind::Other, format!("SeccompError: {error}"))
                })?;
        }

        // Restriction 5: Disallow adjusting system time.
        if !restrict_adjtime {
            for sysname in TIME_SYSCALLS {
                let syscall = ScmpSyscall::new(sysname);
                ctx.add_rule(ScmpAction::Allow, syscall).map_err(|error| {
                    io::Error::new(io::ErrorKind::Other, format!("SeccompError: {error}"))
                })?;
            }
        }

        // Restriction 6: Disallow io_uring interface.
        if !restrict_iouring {
            for sysname in IOURING_SYSCALLS {
                let syscall = ScmpSyscall::new(sysname);
                ctx.add_rule(ScmpAction::Allow, syscall).map_err(|error| {
                    io::Error::new(io::ErrorKind::Other, format!("SeccompError: {error}"))
                })?;
            }
        }

        // Restriction 7: Disallow perf calls.
        if !restrict_perf {
            let syscall = ScmpSyscall::new("perf_event_open");
            ctx.add_rule(ScmpAction::Allow, syscall).map_err(|error| {
                io::Error::new(io::ErrorKind::Other, format!("SeccompError: {error}"))
            })?;
        }

        // Restriction 8: Disallow ptrace calls.
        if !restrict_ptrace {
            for sysname in PTRACE_SYSCALLS {
                let syscall = ScmpSyscall::new(sysname);
                ctx.add_rule(ScmpAction::Allow, syscall).map_err(|error| {
                    io::Error::new(io::ErrorKind::Other, format!("SeccompError: {error}"))
                })?;
            }
        }

        // Export seccomp rules if requested.
        if let Some(mode) = self.export {
            self.seccomp_export(&ctx, mode)?;
        }

        Ok(ctx)
    }

    /// Set up seccomp for the syd process.
    /// This is important to restrict potential attack space in case
    /// syd process is compromised somehow.
    fn setup_seccomp_parent(
        restrict_perf: bool,
        restrict_ptrace: bool,
    ) -> IOResult<ScmpFilterContext> {
        let mut ctx = ScmpFilterContext::new_filter(ScmpAction::Allow).map_err(|e| {
            io::Error::new(
                io::ErrorKind::Other,
                format!("failed to create seccomp filter for syd: {}", e),
            )
        })?;
        // We don't want ECANCELED, we want actual errnos.
        let _ = ctx.set_api_sysrawrc(true);
        // We kill for bad system cal and bad arch.
        let _ = ctx.set_act_badarch(ScmpAction::KillProcess);
        // We log all filter actions other than Allow to kernel log.
        let _ = ctx.set_ctl_log(true);

        seccomp_add_architectures(&mut ctx)?;

        // Restriction 0: Disable list of compiled-in dead system calls.
        // These system calls are not used by syd.
        let mut dead = DEAD_SYSCALLS.to_vec();
        if restrict_perf {
            dead.push("perf_event_open");
        }
        if restrict_ptrace {
            dead.push("ptrace");
        }
        for sysname in dead {
            let syscall = ScmpSyscall::new(sysname);
            ctx.add_rule(ScmpAction::Errno(nix::libc::ENOSYS), syscall)
                .map_err(|e| {
                    io::Error::new(
                        io::ErrorKind::Other,
                        format!(
                            "failed to add rule for system call {} ( {} ): {}",
                            syscall, sysname, e
                        ),
                    )
                })?;
        }

        // Restriction 1: Turn compiled-in list of noop syscalls into no-ops (see config.rs)
        // Restriction 2: Turn set*id syscalls into no-ops.
        for sysname in SET_ID_SYSCALLS.iter().chain(NOOP_SYSCALLS) {
            let syscall = ScmpSyscall::new(sysname);
            ctx.add_rule(ScmpAction::Errno(0), syscall).map_err(|e| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!(
                        "failed to add rule for system call {} ( {} ): {}",
                        syscall, sysname, e
                    ),
                )
            })?;
        }

        // Export seccomp rules if requested.
        if std::env::var("SYD_SECX").is_ok() {
            println!("# syd parent rules");
            ctx.export_pfc(&mut io::stdout()).map_err(|error| {
                io::Error::new(
                    io::ErrorKind::Other,
                    format!("failed to export rules in pfc format: {error}"),
                )
            })?;
        }

        Ok(ctx)
    }

    /// Export a seccomp context as bpf or pfc.
    fn seccomp_export(&self, ctx: &ScmpFilterContext, mode: ExportMode) -> IOResult<()> {
        match mode {
            ExportMode::BerkeleyPacketFilter => ctx.export_bpf(&mut io::stdout()),
            ExportMode::PseudoFiltercode => {
                #[allow(clippy::disallowed_methods)]
                let fd = match mkstemp("syd-tmp-XXXXXX") {
                    Ok((fd, path)) => {
                        unlink(path.as_path()).expect("Failed to delete temporary file.");
                        fd
                    }
                    Err(error) => {
                        return Err(error.into());
                    }
                };
                // SAFETY: In libc, we trust.
                let mut file = unsafe { File::from_raw_fd(fd) };

                if let Err(error) = ctx.export_pfc(&mut file) {
                    return Err(io::Error::new(io::ErrorKind::Other, format!("{error}")));
                }

                file.seek(SeekFrom::Start(0))?;
                let mut buffer = Vec::new();
                file.read_to_end(&mut buffer)?;

                let output = String::from_utf8_lossy(&buffer);
                let output = output.replace("0x7fc00000", "NOTIFY");

                #[allow(clippy::disallowed_methods)]
                let libver = ScmpVersion::current()
                    .expect("Failed to determine libseccomp library version.");
                println!(
                    "# syd v{} seccomp rules generated by libseccomp v{}.{}.{}",
                    env!("CARGO_PKG_VERSION"),
                    libver.major,
                    libver.minor,
                    libver.micro
                );
                println!("# API Version: {API_VERSION}");

                #[allow(clippy::disallowed_methods)]
                let mut syscall_allow: Vec<_> = self
                    .sysallow
                    .iter()
                    .copied()
                    .map(|sys| sys.get_name().unwrap())
                    .collect();
                let syscall_notif: HashSet<&'static str> = self.handlers
                    .values()
                    .map(|(name, _)| *name) // Extract the name from values
                    .collect(); // Collect names into a HashSet to ensure uniqueness
                let mut syscall_notif: Vec<&'static str> = syscall_notif.into_iter().collect();
                syscall_allow.sort();
                syscall_notif.sort();

                println!("# System calls with Action=ALLOW: {}", syscall_allow.len());
                println!("# System calls with Action=NOTIF: {}", syscall_notif.len());

                let uidcall = GET_ID_SYSCALLS.to_vec().join(", ");
                let sandbox = self.sandbox.read();
                println!(
                    "# Fake Root: {} ( {uidcall} )",
                    if sandbox.get_fake_root() { "yes" } else { "no" }
                );
                println!(
                    "{}",
                    sandbox
                        .to_string()
                        .lines()
                        .map(|line| format!("# {}", line))
                        .collect::<Vec<_>>()
                        .join("\n")
                );
                drop(sandbox);

                println!("# Action=NOTIF: {}", syscall_notif.len());
                for name in &syscall_notif {
                    println!("#    - {name}");
                }
                println!("# Action=ALLOW: {}", syscall_allow.len());
                for name in &syscall_allow {
                    println!("#    - {name}");
                }
                print!("{output}");
                Ok(())
            }
        }
        .map_err(|error| {
            io::Error::new(
                io::ErrorKind::Other,
                format!("failed to export rules in {mode:?} format: {error}"),
            )
        })
    }

    /// Logic for the supervise child thread.
    fn supervise(self, pid: Pid, fd: RawFd) -> anyhow::Result<Child> {
        let pool_handle = self.thread_pool.clone();

        // Trace, aka "dry run" mode.
        // We check the trace mode once on startup.
        let sandbox = SandboxGuard::Read(self.sandbox.read());
        let trace = sandbox.trace();
        drop(sandbox);

        let thread_handle = Builder::new().name("syd_main".into()).spawn(move || {
            // SAFETY: Set thread priority to a relatively high value.
            let _ = unsafe { nix::libc::setpriority(nix::libc::PRIO_PROCESS, 0, 1) };

            // 0. Set (process-wide) umask to 0.
            let _ = umask(Mode::empty());

            // 1. Wrap fd in an OwnedFd to ensure it gets closed properly.
            // SAFETY: We're passing a valid fd to OwnedFd.
            let _guard_fd = unsafe { OwnedFd::from_raw_fd(fd) };

            // 2. Create an epoll instance
            let epfd = epoll_create1(EpollCreateFlags::EPOLL_CLOEXEC)?;

            // 3. Wrap epoll fd in an OwnedFd to ensure it gets closed properly.
            // SAFETY: We're passing a valid fd to OwnedFd.
            let _guard_epfd = unsafe { OwnedFd::from_raw_fd(epfd) };

            // 4. Add the file descriptor to the epoll instance
            #[allow(clippy::cast_sign_loss)]
            let mut event = EpollEvent::new(EpollFlags::EPOLLIN, fd as u64);
            epoll_ctl(epfd, EpollOp::EpollCtlAdd, fd, Some(&mut event))?;

            // 5. Wait for events
            loop {
                let mut events = [EpollEvent::empty(); 1];
                let nfds = match epoll_wait(epfd, &mut events, -1) {
                    Ok(nfds) => nfds,
                    Err(Errno::EINTR) => {
                        // An epoll_wait call can - in absence of any
                        // user defined signal handlers - fail with
                        // EINTR on SIGTRAP (eg, attaching strace to a
                        // running process) or SIGCONT (continuing a
                        // stopped process):
                        // https://lwn.net/Articles/851813/
                        continue;
                    },
                    Err(error) => return Err(error),
                };

                if nfds == 0 {
                    error!("ctx": "epoll", "error": "no file descriptors returned from epoll_wait");
                    return Err(Errno::EINVAL);
                } else if events[0].events().contains(EpollFlags::EPOLLHUP) {
                    debug!("ctx": "epoll", "error": "unexpected epoll hung up event", "events": format!("{:?}", events[0].events()));
                    return Err(Errno::EINVAL);
                } else if !events[0].events().contains(EpollFlags::EPOLLIN) {
                    error!("ctx": "epoll", "error": "unexpected epoll event", "events": format!("{:?}", events[0].events()));
                    return Err(Errno::EINVAL);
                }

                #[allow(unused_variables)]
                match ScmpNotifReq::receive(fd) {
                    Ok(req) => {
                        if !self.syscall(fd, req, trace) {
                            // Handling system call failed, break.
                            return Ok(())
                        }
                    }
                    Err(error) => {
                        // SAFETY: We need to continue with epoll after
                        // any error as otherwise the sandbox process will
                        // fail all syscalls with ENOSYS or will just deadlock.
                        match Errno::last() {
                            Errno::ENOENT => {
                                // ENOENT: The task was interrupted by a fatal
                                // signal between the time we get the poll event
                                // and when we attempted to receive the notification.
                            },
                            errno => {
                                debug!("ctx": "recv", "error": error.to_string(), "errno": errno as i32);
                            }
                        }
                    }
                }
            }
        }).context("Failed to spawn the poller thread.")?;

        Ok((pid, thread_handle, pool_handle))
    }

    fn syscall(&self, fd: RawFd, req: ScmpNotifReq, trace: bool) -> bool {
        let event_req = UNotifyEventRequest::new(req, fd, Arc::clone(&self.sandbox));

        // Prepare a fake continue request for trace mode as necessary.
        // We'll set the id on each call before responding with this.
        let cont_resp = if trace {
            Some(libseccomp::ScmpNotifResp::new_continue(
                0,
                ScmpNotifRespFlags::empty(),
            ))
        } else {
            None
        };

        let sydcall = Sydcall(req.data.syscall, req.data.arch);
        match self.handlers.get(&sydcall) {
            Some((_, handler)) => {
                let handler_in_thread = Arc::clone(handler);
                self.thread_pool.execute(move || {
                    PRIORITY_SET.with(|priority_set| {
                        if !priority_set.get() {
                            // SAFETY: Set thread priority to a low value.
                            let _ =
                                unsafe { nix::libc::setpriority(nix::libc::PRIO_PROCESS, 0, 20) };
                            // SAFETY: Set CPU scheduling priority to idle.
                            let _ = set_cpu_priority_idle();
                            // SAFETY: Set I/O priority to idle.
                            let _ = set_io_priority_idle();

                            priority_set.set(true);
                        }
                    });

                    trace!("ctx": "req", "req": SydNotifReq(req));
                    let response = handler_in_thread(&event_req);
                    let response = match response.error {
                        EACCES | ENOENT => cont_resp.map_or_else(
                            || response,
                            |mut cont_resp| {
                                // Tracing enabled, continue syscall.
                                cont_resp.id = response.id;
                                cont_resp
                            },
                        ),
                        _ => response, // Call succeeded or failed genuinely, use as is.
                    };

                    let _result = response.respond(fd);
                    #[cfg(feature = "log")]
                    {
                        match _result {
                            Ok(_) => {
                                trace!("ctx": "res", "res": SydNotifResp(response));
                            }
                            Err(error) => {
                                debug!("ctx": "res", "res": SydNotifResp(response),
                                    "errno": Errno::last() as i32,
                                    "error": error.to_string());
                            }
                        }
                    }
                });
                true
            }
            None => unreachable!("invalid syscall hook!"),
        }
    }

    /// Wait for the child process to exit and cleanup the supervisor thread and thread pool.
    /// It returns `WaitStatus` of the child process.
    ///
    /// # Examples
    ///
    /// ```ignore
    /// let status = Supervisor::wait(pid, thread_handle, pool, wait_all).unwrap();
    /// ```
    pub fn wait(
        child: Pid,
        thread_handle: ChildHandle,
        pool_handle: ThreadPool,
        wait_all: bool,
    ) -> Result<i32, Errno> {
        let mut exit_code: i32 = 127;
        loop {
            #[allow(clippy::cast_possible_truncation)]
            #[allow(clippy::cast_sign_loss)]
            match waitpid(None, Some(WaitPidFlag::__WNOTHREAD)) {
                Ok(WaitStatus::Exited(pid, code)) if pid == child => {
                    exit_code = code;
                    if !wait_all {
                        break;
                    }
                }
                Ok(WaitStatus::Signaled(pid, signal, _)) if pid == child => {
                    exit_code = 128_i32.saturating_add(signal as i32);
                    if !wait_all {
                        break;
                    }
                }
                Err(Errno::ECHILD) => break,
                Err(Errno::EINTR) => {}
                Err(errno) => return Err(errno),
                _ => {}
            }
        }
        if wait_all {
            let _ = thread_handle.join().map_err(|_| Errno::EPIPE)?;
            pool_handle.join();
        }
        Ok(exit_code)
    }
}

/// Processes the address family of a `SockaddrStorage` object and performs logging or other
/// required operations specific to the syscall being handled.
///
/// This helper function isolates the logic involved in dealing with different address families
/// and reduces code duplication across different syscall handler functions.
///
/// # Parameters
///
/// - `addr`: Reference to a `SockaddrStorage`, representing the socket address involved in the syscall.
/// - `syscall_name`: A string slice holding the name of the syscall being handled, used for logging purposes.
///
/// # Safety
///
/// The function contains unsafe blocks due to potential TOCTOU (Time-of-Check Time-of-Use)
/// vulnerabilities. Each unsafe block within this function has been annotated with a detailed
/// safety comment to ensure that unsafe operations are used correctly and securely.
///
/// # Errors
///
/// The function returns an `io::Error` in cases where:
/// - The conversion from `SockaddrStorage` to a specific address family representation fails.
/// - Any other unexpected error condition occurs during the processing of the address family.
///
/// # Returns
///
/// Returns an `Result<(), Errno>`:
/// - `Ok(())` if the processing is successful.
/// - `Err(Errno)` containing a description of the error, if any error occurs during processing.
fn sandbox_addr(
    proc: &RemoteProcess,
    request: &UNotifyEventRequest,
    addr: &SockaddrStorage,
    op: u8,
    caps: Capability,
) -> Result<(), Errno> {
    match addr.family() {
        Some(AddressFamily::Unix) => sandbox_addr_unix(proc, request, addr, op, caps),
        Some(AddressFamily::Inet) => sandbox_addr_inet(proc, request, addr, op, caps),
        Some(AddressFamily::Inet6) => sandbox_addr_inet6(proc, request, addr, op, caps),
        Some(_) | None => sandbox_addr_notsup(proc, request),
    }
}

/// Process a `AddressFamily::Unix` socket address.
fn sandbox_addr_unix(
    _proc: &RemoteProcess,
    request: &UNotifyEventRequest,
    addr: &SockaddrStorage,
    op: u8,
    caps: Capability,
) -> Result<(), Errno> {
    let addr = addr.as_unix_addr().ok_or(Errno::EINVAL)?;
    let (path, abs) = match (addr.path(), addr.as_abstract()) {
        (Some(path), _) => {
            let path = path.as_os_str().as_bytes();
            let null = path.iter().position(|&b| b == 0).unwrap_or(path.len());
            let path = PathBuf::from(OsStr::from_bytes(&path[..null]));
            trace!("ctx": "sys", "sys": op2name(op), "addr": format!("{}", path.display()), "cap": caps);
            (path, false)
        }
        (_, Some(path)) => {
            let null = path.iter().position(|&b| b == 0).unwrap_or(path.len());
            let path = PathBuf::from(OsStr::from_bytes(&path[..null]));
            trace!("ctx": "sys", "sys": op2name(op), "addr": format!("{}", path.display()), "cap": caps);
            (path, true)
        }
        _ => {
            // unnamed unix socket
            return Ok(());
        }
    };

    // Check for access.
    let sandbox = request.get_sandbox(false);
    let action = sandbox.check_unix(caps, &path);
    drop(sandbox);

    if action == Action::Deny {
        // Report access violation.
        let req = request.get_request();
        warn!("ctx": "access",
            "cap": caps,
            "unix": format!("{}", path.display()),
            "abs": abs,
            "pid": req.pid,
            "sys": op2name(op));
    }
    if action == Action::Allow {
        Ok(())
    } else {
        // Deny or Filter.
        Err(Errno::EACCES)
    }
}

/// Process an `AddressFamily::Inet` socket address.
fn sandbox_addr_inet(
    _proc: &RemoteProcess,
    request: &UNotifyEventRequest,
    addr: &SockaddrStorage,
    op: u8,
    caps: Capability,
) -> Result<(), Errno> {
    let addr = addr.as_sockaddr_in().ok_or(Errno::EINVAL)?;
    let port = addr.port();
    let addr = IpAddr::V4(Ipv4Addr::from(addr.ip()));
    trace!("ctx": "sys", "sys": op2name(op), "addr": format!("{addr}!{port}"), "cap": caps, "ipv": 4);

    // Check for access.
    let sandbox = request.get_sandbox(false);
    let action = sandbox.check_ip(caps, addr, port);
    drop(sandbox);

    if action == Action::Deny {
        // Report access violation.
        let req = request.get_request();
        warn!("ctx": "access",
            "cap": caps,
            "addr": format!("{addr}!{port}"),
            "ipv": 4,
            "pid": req.pid,
            "sys": op2name(op));
    }
    if action == Action::Allow {
        Ok(())
    } else {
        // Deny or Filter.
        Err(Errno::EACCES)
    }
}

/// Process an `AddressFamily::Inet6` socket address.
fn sandbox_addr_inet6(
    _proc: &RemoteProcess,
    request: &UNotifyEventRequest,
    addr: &SockaddrStorage,
    op: u8,
    caps: Capability,
) -> Result<(), Errno> {
    let addr = addr.as_sockaddr_in6().ok_or(Errno::EINVAL)?;
    let port = addr.port();
    // Check if the IPv6 address is a mapped IPv4 address
    let (addr, ipv) = if let Some(v4addr) = addr.ip().to_ipv4_mapped() {
        // It's a mapped IPv4 address, convert to IPv4
        (IpAddr::V4(v4addr), 4)
    } else {
        // It's a regular IPv6 address
        (IpAddr::V6(addr.ip()), 6)
    };
    trace!("ctx": "sys", "sys": op2name(op), "addr": format!("{addr}!{port}"), "cap": caps, "ipv": ipv);

    // Check for access.
    let sandbox = request.get_sandbox(false);
    let action = sandbox.check_ip(caps, addr, port);
    drop(sandbox);

    if action == Action::Deny {
        // Report access violation.
        let req = request.get_request();
        warn!("ctx": "access",
            "cap": caps,
            "addr": format!("{addr}!{port}"),
            "ipv": ipv,
            "pid": req.pid,
            "sys": op2name(op));
    }
    if action == Action::Allow {
        Ok(())
    } else {
        // Deny or Filter.
        Err(Errno::EACCES)
    }
}

/// Process a socket address of an unsupported socket family.
fn sandbox_addr_notsup(_proc: &RemoteProcess, request: &UNotifyEventRequest) -> Result<(), Errno> {
    let sandbox = request.get_sandbox(false);
    let ok = sandbox.allow_unsupp_socket();
    drop(sandbox);

    if ok {
        Ok(())
    } else {
        Err(Errno::EAFNOSUPPORT)
    }
}

/// Process the first path argument.
#[allow(clippy::cognitive_complexity)]
fn sandbox_path_1(
    path: &Path,
    caps: Capability,
    syscall_name: &str,
    request: &UNotifyEventRequest,
    _: &RemoteProcess,
) -> Result<(), Errno> {
    let req = request.get_request();
    trace!("ctx": "sys", "sys": syscall_name, "cap": caps, "path": format!("{}", path.display()));

    if caps.is_empty() {
        return Err(Errno::EINVAL);
    }

    let sandbox = request.get_sandbox(false);
    let trace = sandbox.trace();
    let mut action = Action::Allow;

    // Sandboxing
    if caps.contains(Capability::CAP_READ) {
        action = sandbox.check_path(Capability::CAP_READ, path);
    }
    if action == Action::Allow && caps.contains(Capability::CAP_STAT) {
        action = sandbox.check_path(Capability::CAP_STAT, path);
    }
    if action == Action::Allow && caps.contains(Capability::CAP_WRITE) {
        action = sandbox.check_path(Capability::CAP_WRITE, path);
    }
    if action == Action::Allow && caps.contains(Capability::CAP_EXEC) {
        action = sandbox.check_path(Capability::CAP_EXEC, path);
    }

    // exec/kill
    if caps.contains(Capability::CAP_EXEC) && sandbox.check_exec(path) == Action::Kill {
        warn!("ctx": "exec/kill", "cap": caps, "path": format!("{}", path.display()), "pid": req.pid, "sys": syscall_name);
        #[allow(clippy::cast_possible_wrap)]
        let _ = kill(Pid::from_raw(req.pid as i32), Some(SIGKILL));
        action = Action::Kill;
    }

    // Check if path is hidden for stat sandbox compat.
    let cap_stat = caps.contains(Capability::CAP_STAT);
    let hidden = if !cap_stat && action != Action::Allow {
        sandbox.is_hidden(path)
    } else {
        false
    };
    drop(sandbox); // release the read lock.

    match action {
        Action::Allow => Ok(()),
        _ if cap_stat => {
            // We do not report violations for Stat capability because
            // we are essentially hiding paths unless:
            // 1. Extended logging is enabled: level=debug
            // 2. Trace mode is enabled: level=warn

            if trace {
                warn!("ctx": "access",
                    "act": action,
                    "cap": caps,
                    "path": format!("{}", path.display()),
                    "pid": req.pid,
                    "sys": syscall_name);
            } else {
                debug!("ctx": "access",
                    "act": action,
                    "cap": caps,
                    "path": format!("{}", path.display()),
                    "pid": req.pid,
                    "sys": syscall_name);
            }

            Err(Errno::ENOENT)
        }
        Action::Deny | Action::Filter if hidden => {
            // SAFETY: We do not report violations when the path is
            // hidden and return ENOENT, so as to make read/write/exec
            // sandboxing consistent with stat sandboxing. With this
            // restriction in place, it is not possible to enumerate
            // existing hidden paths by attempting to read, write or
            // execute them.
            //
            // Two exceptions:
            // 1. Extended logging is enabled: level=debug
            // 2. Trace mode is enabled: level=warn

            if trace {
                warn!("ctx": "access",
                    "act": action,
                    "cap": caps,
                    "path": format!("{}", path.display()),
                    "pid": req.pid,
                    "sys": syscall_name);
            } else {
                debug!("ctx": "access",
                    "act": action,
                    "cap": caps,
                    "path": format!("{}", path.display()),
                    "pid": req.pid,
                    "sys": syscall_name);
            }

            Err(Errno::ENOENT)
        }
        Action::Filter | Action::Kill => Err(Errno::EACCES),
        Action::Deny => {
            // Report access violation.
            warn!("ctx": "access",
                "cap": caps,
                "path": format!("{}", path.display()),
                "pid": req.pid,
                "sys": syscall_name);
            Err(Errno::EACCES)
        }
    }
}

/// Process both the first and the second path argument.
fn sandbox_path_2(
    paths: &[PathBuf],
    caps: Capability,
    syscall_name: &str,
    request: &UNotifyEventRequest,
    proc: &RemoteProcess,
) -> Result<(), Errno> {
    let source = &paths[0];
    let target = &paths[1];
    trace!("ctx": "sys",
        "sys": syscall_name,
        "cap": caps,
        "source": format!("{}", source.display()),
        "target": format!("{}", target.display()));

    sandbox_path_1(source, caps, syscall_name, request, proc)?;
    sandbox_path_1(target, caps, syscall_name, request, proc)?;
    Ok(())
}

/*
 * System call handlers
 */
// System page size
static PAGE_SIZE: Lazy<u64> = Lazy::new(|| {
    sysconf(SysconfVar::PAGE_SIZE)
        .unwrap_or(Some(4096))
        .unwrap_or(4096) as u64
});

fn sys_brk(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_mem_handler(request, "brk")
}

fn sys_mmap(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_mem_handler(request, "mmap")
}

fn sys_mmap2(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_mem_handler(request, "mmap2")
}

fn sys_mremap(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let old_size = req.data.args[1] as usize;
    #[allow(clippy::cast_possible_truncation)]
    let new_size = req.data.args[2] as usize;
    if new_size < old_size {
        // SAFETY: System call wants to shrink memory.
        return unsafe { request.continue_syscall() };
    }
    syscall_mem_handler(request, "mremap")
}

#[allow(clippy::cognitive_complexity)]
fn syscall_mem_handler(request: &UNotifyEventRequest, name: &str) -> ScmpNotifResp {
    // Get mem & vm max.
    let sandbox = request.get_sandbox(false);
    let enabled = sandbox.enabled(Capability::CAP_MEM);
    let mem_max = sandbox.mem_max;
    let mem_vm_max = sandbox.mem_vm_max;
    let mem_filter = sandbox.mem_filter;
    let kill_mem = sandbox.kill_mem();
    drop(sandbox); // release the read lock.

    if !enabled || (mem_max == 0 && mem_vm_max == 0) {
        // SAFETY: No pointer dereference in security check.
        // This is safe to continue.
        return unsafe { request.continue_syscall() };
    }

    // Get process entry.
    let req = request.get_request();
    #[allow(clippy::cast_possible_wrap)]
    let proc = match Process::new(req.pid as nix::libc::pid_t) {
        Ok(proc) => proc,
        Err(error) => {
            if !mem_filter {
                warn!("ctx" : "access",
                    "cap" : "m",
                    "sys" : name,
                    "pid" : req.pid,
                    "max" : mem_max,
                    "max_vm" : mem_vm_max,
                    "kill" : kill_mem,
                    "error": error.to_string());
            }
            if kill_mem {
                #[allow(clippy::cast_possible_wrap)]
                let _ = kill(Pid::from_raw(req.pid as i32), Some(SIGKILL));
            }
            return request.fail_syscall(nix::libc::ENOMEM);
        }
    };

    // Check VmSize
    if mem_vm_max > 0 {
        match proc.statm() {
            Ok(statm) if statm.size.saturating_mul(*PAGE_SIZE) >= mem_vm_max => {
                if !mem_filter {
                    warn!("ctx" : "access",
                        "cap" : "m",
                        "pid" : req.pid,
                        "sys" : name,
                        "kill" : kill_mem,
                        "max" : mem_max,
                        "max_vm": mem_vm_max);
                }
                if kill_mem {
                    #[allow(clippy::cast_possible_wrap)]
                    let _ = kill(Pid::from_raw(req.pid as i32), Some(SIGKILL));
                }
                return request.fail_syscall(nix::libc::ENOMEM);
            }
            Err(error) => {
                if !mem_filter {
                    warn!("ctx" : "access",
                        "cap" : "m",
                        "sys" : name,
                        "pid" : req.pid,
                        "kill" : kill_mem,
                        "max" : mem_max,
                        "max_vm" : mem_vm_max,
                        "error" : error.to_string());
                }
                if kill_mem {
                    #[allow(clippy::cast_possible_wrap)]
                    let _ = kill(Pid::from_raw(req.pid as i32), Some(SIGKILL));
                }
                return request.fail_syscall(nix::libc::ENOMEM);
            }
            _ => { /* fall through */ }
        }
    }

    // Check PSS
    match proc_mem_limit(&proc, mem_max) {
        Ok(false) => {
            // SAFETY: No pointer dereference in security check.
            // This is safe to continue.
            unsafe { request.continue_syscall() }
        }
        Ok(true) => {
            if !mem_filter {
                warn!("ctx" : "access",
                    "cap" : "m",
                    "pid" : req.pid,
                    "sys" : name,
                    "kill" : kill_mem,
                    "max" : mem_max,
                    "max_vm": mem_vm_max);
            }
            if kill_mem {
                #[allow(clippy::cast_possible_wrap)]
                let _ = kill(Pid::from_raw(req.pid as i32), Some(SIGKILL));
            }
            request.fail_syscall(nix::libc::ENOMEM)
        }
        Err(errno) => {
            if !mem_filter {
                warn!("ctx" : "access",
                    "cap" : "m",
                    "sys" : name,
                    "pid" : req.pid,
                    "kill" : kill_mem,
                    "max" : mem_max,
                    "max_vm" : mem_vm_max,
                    "errno": errno as i32);
            }
            if kill_mem {
                #[allow(clippy::cast_possible_wrap)]
                let _ = kill(Pid::from_raw(req.pid as i32), Some(SIGKILL));
            }
            request.fail_syscall(nix::libc::ENOMEM)
        }
    }
}

fn sys_fork(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_pid_handler(request, "fork")
}

fn sys_vfork(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_pid_handler(request, "vfork")
}

fn sys_clone(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_pid_handler(request, "clone")
}

fn sys_clone3(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_pid_handler(request, "clone3")
}

#[allow(clippy::cognitive_complexity)]
fn syscall_pid_handler(request: &UNotifyEventRequest, name: &str) -> ScmpNotifResp {
    // Get pid max.
    let sandbox = request.get_sandbox(false);
    let enabled = sandbox.enabled(Capability::CAP_PID);
    let pid_max = sandbox.pid_max;
    let pid_filter = sandbox.pid_filter;
    let kill_pid = sandbox.kill_pid();
    drop(sandbox); // release the read lock.

    if !enabled || pid_max == 0 {
        // SAFETY: No pointer dereference in security check.
        // This is safe to continue.
        return unsafe { request.continue_syscall() };
    }

    // Get pid count.
    let req = request.get_request();
    match proc_task_limit(pid_max) {
        Ok(false) => {
            // SAFETY: No pointer dereference in security check.
            // This is safe to continue.
            unsafe { request.continue_syscall() }
        }
        Ok(true) => {
            if !pid_filter {
                warn!("ctx" : "access",
                    "cap" : "p",
                    "pid" : req.pid,
                    "sys" : name,
                    "max" : pid_max,
                    "kill" : kill_pid);
            }
            if kill_pid {
                #[allow(clippy::cast_possible_wrap)]
                let _ = kill(Pid::from_raw(req.pid as i32), Some(SIGKILL));
            }
            request.fail_syscall(nix::libc::EACCES)
        }
        Err(errno) => {
            if !pid_filter {
                warn!("ctx" : "access",
                    "cap" : "p",
                    "sys" : name,
                    "pid" : req.pid,
                    "max" : pid_max,
                    "kill" : kill_pid,
                    "errno" : errno as i32);
            }
            if kill_pid {
                #[allow(clippy::cast_possible_wrap)]
                let _ = kill(Pid::from_raw(req.pid as i32), Some(SIGKILL));
            }
            request.fail_syscall(nix::libc::EACCES)
        }
    }
}

fn sys_kill(request: &UNotifyEventRequest) -> ScmpNotifResp {
    // SAFETY: Allow signal 0, ie checking if pid1 is alive.
    let req = request.get_request();
    if req.data.args[1] == 0 {
        return unsafe { request.continue_syscall() };
    }

    syscall_signal_handler(request, false)
}

fn sys_tkill(request: &UNotifyEventRequest) -> ScmpNotifResp {
    // SAFETY: Allow signal 0, ie checking if pid1 is alive.
    let req = request.get_request();
    if req.data.args[1] == 0 {
        return unsafe { request.continue_syscall() };
    }

    syscall_signal_handler(request, false)
}

fn sys_tgkill(request: &UNotifyEventRequest) -> ScmpNotifResp {
    // SAFETY: Allow signal 0, ie checking if pid1 is alive.
    let req = request.get_request();
    if req.data.args[2] == 0 {
        return unsafe { request.continue_syscall() };
    }

    syscall_signal_handler(request, true)
}

fn sys_pidfd_open(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_signal_handler(request, false)
}

fn sys_socketcall(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let (req, proc) = request.prepare();

    // Determine system call
    // 0x2 => bind
    // 0x3 => connect
    // 0xb => sendto
    // 0xc => recvfrom
    #[allow(clippy::cast_possible_truncation)]
    let op = match req.data.args[0] {
        n @ (0x2 | 0x3 | 0xb | 0xc) => n as u8,
        _ => {
            // SAFETY: safe network call, continue.
            return unsafe { request.continue_syscall() };
        }
    };

    // Determine system call arguments
    let args = {
        let mut args = [0u32; 6];
        let size = std::mem::size_of_val(&args);
        let mut buf = vec![0u8; size];
        #[allow(clippy::cast_possible_truncation)]
        match proc.read_mem(&mut buf, req.data.args[1] as usize, request) {
            Ok(bytes_read) if bytes_read >= size => {
                // unsigned long is typically 32 bits (4 bytes) in size.
                for (i, chunk) in buf.chunks_exact(4).enumerate() {
                    match chunk.try_into() {
                        Ok(bytes) => args[i] = u32::from_ne_bytes(bytes),
                        Err(error) => {
                            error!("ctx": "socketcall_decode",
                                "pid": req.pid,
                                "addr": req.data.args[1],
                                "read": bytes_read,
                                "size": size,
                                "error": error.to_string());
                            return request.fail_syscall(nix::libc::EFAULT);
                        }
                    }
                }
                args.map(u64::from)
            }
            Ok(n) => {
                error!("ctx": "socketcall_decode",
                    "pid": req.pid,
                    "addr": req.data.args[1],
                    "read": n,
                    "size": size,
                    "error": "short read");
                return request.fail_syscall(nix::libc::EFAULT);
            }
            Err(error) => {
                error!("ctx": "socketcall_decode",
                    "pid": req.pid,
                    "addr": req.data.args[1],
                    "read": 0,
                    "size": size,
                    "error": error.to_string());
                return request.fail_syscall(nix::libc::EFAULT);
            }
        }
    };

    // SAFETY: If addr or receiver/send buffer is NULL,
    // we must return EFAULT here without further processing.
    if args[1] == 0 {
        return request.fail_syscall(nix::libc::EFAULT);
    }

    if op == 0xc {
        sys_recvfrom_with_args(request, &args)
    } else {
        syscall_network_handler(request, &args, op)
    }
}

fn sys_bind(request: &UNotifyEventRequest) -> ScmpNotifResp {
    // SAFETY: If addr is NULL we must return EFAULT
    // here without further processing.
    let req = request.get_request();
    if req.data.args[1] == 0 {
        return request.fail_syscall(nix::libc::EFAULT);
    }
    syscall_network_handler(request, &req.data.args, 0x2)
}

fn sys_connect(request: &UNotifyEventRequest) -> ScmpNotifResp {
    // SAFETY: If addr is NULL we must return EFAULT
    // here without further processing.
    let req = request.get_request();
    if req.data.args[1] == 0 {
        return request.fail_syscall(nix::libc::EFAULT);
    }
    syscall_network_handler(request, &req.data.args, 0x3)
}

fn sys_sendto(request: &UNotifyEventRequest) -> ScmpNotifResp {
    // SAFETY: If send buffer is NULL we must
    // return EFAULT here without further
    // processing.
    let req = request.get_request();
    if req.data.args[1] == 0 {
        return request.fail_syscall(nix::libc::EFAULT);
    }
    syscall_network_handler(request, &req.data.args, 0xb)
}

fn sys_recvfrom(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    sys_recvfrom_with_args(request, &req.data.args)
}

fn sys_recvfrom_with_args(request: &UNotifyEventRequest, args: &[u64; 6]) -> ScmpNotifResp {
    syscall_handler!(request, |_req: &ScmpNotifReq, proc: &RemoteProcess| {
        let sandbox = request.get_sandbox(false);
        let (check, allow_unsupp_socket) = (
            sandbox.enabled(Capability::CAP_BIND),
            sandbox.allow_unsupp_socket(),
        );
        drop(sandbox); // release the read lock.

        // Return immediately if sandboxing is not enabled for current capability,
        if !check {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU,
            // however since the sandboxing for the requested capability
            // is disabled this is safe here.
            return unsafe { Ok(request.continue_syscall()) };
        }

        let fd = proc.get_fd(args[0] as RawFd, request)?;
        match getsockname::<SockaddrStorage>(fd.as_raw_fd()) {
            Ok(addr) => {
                match addr.family() {
                    Some(AddressFamily::Unix | AddressFamily::Inet | AddressFamily::Inet6) => {
                        // Check for access.
                        sandbox_addr(proc, request, &addr, 0xc, Capability::CAP_BIND)?;
                        // SAFETY: No pointer dereference in access check, safe to continue.
                        unsafe { Ok(request.continue_syscall()) }
                    }
                    _ if allow_unsupp_socket => unsafe { Ok(request.continue_syscall()) },
                    _ => Err(Errno::EAFNOSUPPORT),
                }
            }
            Err(_errno) => {
                debug!("ctx": "getsockname", "sys": "recvfrom", "errno": _errno as i32);
                // SAFETY: No source address, continue.
                unsafe { Ok(request.continue_syscall()) }
            }
        }
    })
}

fn sys_execve(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "execve", ARGV, |_, _, _| {
        // SAFETY: This is vulnerable to TOCTOU,
        // unfortunately we cannot emulate exec,
        // so we have to live with it...
        Ok(unsafe { request.continue_syscall() })
    })
}

fn sys_execveat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    let empty = req.data.args[4] & nix::libc::AT_EMPTY_PATH as u64 != 0;
    let argv: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        empty,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "execveat", argv, |_, _, _| {
        // SAFETY: This is vulnerable to TOCTOU,
        // unfortunately we cannot emulate exec,
        // so we have to live with it...
        Ok(unsafe { request.continue_syscall() })
    })
}

fn sys_chdir(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "chdir", ARGV, |_, _, _| {
        // SAFETY: This is vulnerable to TOCTOU,
        // unfortunately we cannot emulate chdir,
        // so we have to live with it...
        Ok(unsafe { request.continue_syscall() })
    })
}

fn sys_fchdir(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        null: false,
        empty: true,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "chdir", ARGV, |_, _, _| {
        // SAFETY: This is vulnerable to TOCTOU,
        // unfortunately we cannot emulate fchdir,
        // so we have to live with it...
        Ok(unsafe { request.continue_syscall() })
    })
}

#[allow(clippy::cognitive_complexity)]
fn sys_getdents(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_handler!(request, |req: &ScmpNotifReq, proc: &RemoteProcess| {
        if req.data.args[1] == 0 {
            // SAFETY: If the second argument which must hold a pointer to a
            // linux_dirent structure is NULL, we must return EFAULT
            // without further processing here.
            return Err(Errno::EFAULT);
        } else if req.data.args[2] == 0 {
            // SAFETY:Result buffer is too small
            return Err(Errno::EINVAL);
        }

        // If sandboxing for List capability is off, return immediately.
        let sandbox = request.get_sandbox(false);
        #[allow(clippy::cast_possible_wrap)]
        let is_lock = sandbox.locked_for_pid(req.pid as nix::libc::pid_t);
        let is_stat = sandbox.enabled(Capability::CAP_STAT);
        let is_trace = sandbox.trace();
        drop(sandbox); // release the read lock.
        if is_lock && !is_stat {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU.
            // Since stat sandboxing is disabled here, this is safe.
            return unsafe { Ok(request.continue_syscall()) };
        }

        // Get remote fd and readlink /proc/self/fd/$fd.
        let fd = proc.get_fd(req.data.args[0] as RawFd, request)?;
        let mut buf0 = itoa::Buffer::new();
        let mut buf1 = itoa::Buffer::new();
        let mut sym = PathBuf::from("/proc");
        sym.push(buf0.format(Pid::this().as_raw()));
        sym.push("fd");
        sym.push(buf1.format(fd.as_raw_fd()));
        let dir = match read_link(&sym) {
            Ok(dir) => {
                if dir.is_relative() {
                    // /proc/1/fd/0 -> pipe:42
                    return Err(Errno::EBADF);
                } else {
                    // genuine dir, check for access.
                    sandbox_path_1(&dir, Capability::CAP_STAT, "getdents64", request, proc)?;
                    dir
                }
            }
            Err(errno) => {
                error!("sys": "getdents64",
                    "ctx": "readlink",
                    "fd": req.data.args[0],
                    "errno": errno as i32);
                return Err(Errno::ENOENT);
            }
        };

        // SAFETY: The count argument to the getdents call
        // must not be fully trusted, it can be overly large,
        // and allocating a Vector of that capacity may overflow.
        // This bug was discovered by trinity in this build:
        // https://builds.sr.ht/~alip/job/1077263
        #[allow(clippy::cast_possible_truncation)]
        let count = req.data.args[2] as usize;
        let count = count.min(1000000); // Cap count at 1mio
        let mut entries = Vec::with_capacity(count.saturating_add(1) /* /dev/syd */);
        let mut dot: u8 = 0;
        loop {
            match getdents(fd.as_raw_fd(), count) {
                Err(error) => {
                    return Err(error);
                }
                Ok(None) => {
                    // getdents returned None, there are no more entries.
                    if !entries.is_empty() {
                        break;
                    } else {
                        return Ok(request.return_syscall(0));
                    }
                }
                Ok(Some(e)) => {
                    for entry in e.iter() {
                        let name = entry.to_path_buf();
                        #[allow(clippy::arithmetic_side_effects)]
                        if dot < 2 && path_is_dot(&name) {
                            // SAFETY: Allow . and ..
                            entries.push(entry.clone());
                            dot += 1;
                            continue;
                        }
                        let path = dir.join(&name);
                        if is_stat {
                            match sandbox_path_1(
                                &path,
                                Capability::CAP_STAT,
                                "getdents64",
                                request,
                                proc,
                            ) {
                                Ok(_) /*allow*/ => {
                                    trace!("ctx": "stat",
                                        "sys": "getdents64",
                                        "path": format!("{}", path.display()),
                                        "act": "Allow");
                                    entries.push(entry.clone());
                                },
                                _ if is_trace => {
                                    // Tracing: report access violation and allow call.
                                    warn!("ctx": "access",
                                        "cap": "s",
                                        "pid": req.pid,
                                        "sys": "getdents64",
                                        "path": format!("{}", path.display()));
                                    entries.push(entry.clone());
                                }
                                _ => {
                                    /* this entry is denied, skip it. */
                                    debug!("ctx": "stat",
                                        "sys": "getdents64",
                                        "path": format!("{}", path.display()),
                                        "act": "Hide");
                                },
                            };
                        } else {
                            // Stat sandboxing is off.
                            // We're here because the lock is off or tracing is on.
                            trace!("ctx": "stat",
                                "sys": "getdents64",
                                "path": format!("{}", path.display()),
                                "act": "Allow");
                            entries.push(entry.clone());
                        }
                    }
                    if !entries.is_empty() {
                        break; // exit the loop once we have allowed entries
                    }
                }
            };
        }

        let mut buffer = Vec::new();
        for entry in &entries {
            let bytes = &entry.dirent;

            // Ensure we don't append more bytes than the buffer can hold.
            if buffer.len().saturating_add(bytes.len()) > count {
                break;
            }

            buffer.extend_from_slice(bytes);
        }

        #[allow(clippy::cast_possible_truncation)]
        proc.write_mem(&buffer, req.data.args[1] as usize, request)?;

        #[allow(clippy::cast_possible_wrap)]
        Ok(request.return_syscall(buffer.len() as i64))
    })
}

fn sys_access(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Normal,
    }];
    syscall_path_handler(request, "access", ARGV, |paths, request, _proc| {
        let req = request.get_request();
        #[allow(clippy::cast_possible_truncation)]
        let mode = AccessFlags::from_bits_truncate(req.data.args[1] as nix::libc::c_int);
        access(&paths[0], mode).map(|_| request.return_syscall(0))
    })
}

fn sys_faccessat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Normal,
    }];
    syscall_path_handler(request, "faccessat", ARGV, |paths, request, _proc| {
        let req = request.get_request();
        #[allow(clippy::cast_possible_truncation)]
        let mode = AccessFlags::from_bits_truncate(req.data.args[2] as nix::libc::c_int);
        access(&paths[0], mode).map(|_| request.return_syscall(0))
    })
}

fn sys_faccessat2(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    let resolve = req.data.args[3] & nix::libc::AT_SYMLINK_NOFOLLOW as u64 == 0;
    let argv = &[SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        empty: false,
        dotlast: None,
        miss: Normal,
        resolve,
    }];
    syscall_path_handler(request, "faccessat2", argv, |paths, request, _proc| {
        let req = request.get_request();
        #[allow(clippy::cast_possible_truncation)]
        let mode = AccessFlags::from_bits_truncate(req.data.args[2] as nix::libc::c_int);
        #[allow(clippy::cast_possible_truncation)]
        let flags = AtFlags::from_bits_truncate(req.data.args[3] as nix::libc::c_int);
        faccessat(None, &paths[0], mode, flags).map(|_| request.return_syscall(0))
    })
}

fn sys_chmod(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: false,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "chmod", ARGV, |paths, request, _proc| {
        let req = request.get_request();
        #[allow(clippy::cast_possible_truncation)]
        let mode = Mode::from_bits_truncate(req.data.args[1] as u32);
        fchmodat(None, &paths[0], mode, FchmodatFlags::FollowSymlink)
            .map(|_| request.return_syscall(0))
    })
}

fn sys_fchmod(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        null: false,
        empty: true,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "fchmod", ARGV, |_, request, proc| {
        let req = request.get_request();
        #[allow(clippy::cast_possible_truncation)]
        let mode = Mode::from_bits_truncate(req.data.args[1] as u32);
        let fd = proc.get_fd(req.data.args[0] as RawFd, request)?;
        fchmod(fd.as_raw_fd(), mode).map(|_| request.return_syscall(0))
    })
}

/*
fn sys_fchmodat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        dotlast: None,
        miss: Existing,
        empty: false,
        resolve: true,
    }];
    syscall_path_handler(request, "fchmodat", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let pid = Pid::from_raw(req.pid as i32);
        #[allow(clippy::cast_possible_truncation)]
        let mode = Mode::from_bits_truncate(req.data.args[2] as u32);
        // Handle special proc file descriptors.
        // SAFETY: Magic symlinks are disallowed with ELOOP here, see proc_fd.
        if let Some(fd) = proc_fd(pid, &paths[0])? {
            let fd = proc.get_fd(fd as RawFd, request)?;
            fchmod(fd.as_raw_fd(), mode).map(|_| request.return_syscall(0))
        } else {
            fchmodat(None, &paths[0], mode, FchmodatFlags::FollowSymlink)
                .map(|_| request.return_syscall(0))
        }
    })
}
*/

fn sys_fchmodat2(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();

    let empty = req.data.args[3] & nix::libc::AT_EMPTY_PATH as u64 != 0;
    let resolve = req.data.args[3] & nix::libc::AT_SYMLINK_NOFOLLOW as u64 == 0;
    let argv = &[SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        dotlast: None,
        miss: Existing,
        empty,
        resolve,
    }];

    syscall_path_handler(request, "fchmodat2", argv, |paths, request, proc| {
        let req = request.get_request();
        #[allow(clippy::cast_possible_wrap)]
        let pid = Pid::from_raw(req.pid as i32);
        #[allow(clippy::cast_possible_truncation)]
        let mode = Mode::from_bits_truncate(req.data.args[2] as u32);
        // Handle special proc file descriptors.
        // SAFETY: Magic symlinks are disallowed with ELOOP here, see proc_fd.
        if let Some(fd) = proc_fd(pid, &paths[0])? {
            let fd = proc.get_fd(fd as RawFd, request)?;
            fchmod(fd.as_raw_fd(), mode).map(|_| request.return_syscall(0))
        } else {
            let flags = if resolve {
                FchmodatFlags::FollowSymlink
            } else {
                FchmodatFlags::NoFollowSymlink
            };
            fchmodat(None, &paths[0], mode, flags).map(|_| request.return_syscall(0))
        }
    })
}

fn sys_chown(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "chown", ARGV, |paths, request, _proc| {
        let req = request.get_request();
        #[allow(clippy::cast_possible_wrap)]
        let owner = if req.data.args[1] as i64 == -1 {
            None
        } else {
            Some(Uid::from_raw(req.data.args[1] as nix::libc::uid_t))
        };
        #[allow(clippy::cast_possible_wrap)]
        let group = if req.data.args[2] as i64 == -1 {
            None
        } else {
            Some(Gid::from_raw(req.data.args[2] as nix::libc::gid_t))
        };
        chown(&paths[0], owner, group).map(|_| request.return_syscall(0))
    })
}

fn sys_lchown(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: false,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "chown", ARGV, |paths, request, _proc| {
        let req = request.get_request();
        #[allow(clippy::cast_possible_wrap)]
        let owner = if req.data.args[1] as i64 == -1 {
            None
        } else {
            Some(Uid::from_raw(req.data.args[1] as nix::libc::uid_t))
        };
        #[allow(clippy::cast_possible_wrap)]
        let group = if req.data.args[2] as i64 == -1 {
            None
        } else {
            Some(Gid::from_raw(req.data.args[2] as nix::libc::gid_t))
        };
        fchownat(
            None,
            &paths[0],
            owner,
            group,
            FchownatFlags::NoFollowSymlink,
        )
        .map(|_| request.return_syscall(0))
    })
}

fn sys_fchown(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        null: false,
        empty: true,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "fchown", ARGV, |_, request, proc| {
        let req = request.get_request();
        #[allow(clippy::cast_possible_wrap)]
        let owner = if req.data.args[1] as i64 == -1 {
            None
        } else {
            Some(Uid::from_raw(req.data.args[1] as nix::libc::uid_t))
        };
        #[allow(clippy::cast_possible_wrap)]
        let group = if req.data.args[2] as i64 == -1 {
            None
        } else {
            Some(Gid::from_raw(req.data.args[2] as nix::libc::gid_t))
        };
        let fd = proc.get_fd(req.data.args[0] as RawFd, request)?;
        fchown(fd.as_raw_fd(), owner, group).map(|_| request.return_syscall(0))
    })
}

fn sys_fchownat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    let empty = req.data.args[4] & nix::libc::AT_EMPTY_PATH as u64 != 0;
    let resolve = req.data.args[4] & nix::libc::AT_SYMLINK_NOFOLLOW as u64 == 0;
    let argv = &[SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        dotlast: None,
        miss: Existing,
        empty,
        resolve,
    }];
    syscall_path_handler(request, "fchownat", argv, |paths, request, proc| {
        let req = request.get_request();
        #[allow(clippy::cast_possible_wrap)]
        let pid = Pid::from_raw(req.pid as i32);
        #[allow(clippy::cast_possible_wrap)]
        let owner = if req.data.args[2] as i64 == -1 {
            None
        } else {
            Some(Uid::from_raw(req.data.args[2] as nix::libc::uid_t))
        };
        #[allow(clippy::cast_possible_wrap)]
        let group = if req.data.args[3] as i64 == -1 {
            None
        } else {
            Some(Gid::from_raw(req.data.args[3] as nix::libc::gid_t))
        };
        // Handle special proc file descriptors.
        // SAFETY: Magic symlinks are disallowed with ELOOP here, see proc_fd.
        if let Some(fd) = proc_fd(pid, &paths[0])? {
            let fd = proc.get_fd(fd as RawFd, request)?;
            fchown(fd.as_raw_fd(), owner, group).map(|_| request.return_syscall(0))
        } else {
            let flags = if resolve {
                FchownatFlags::FollowSymlink
            } else {
                FchownatFlags::NoFollowSymlink
            };
            fchownat(None, &paths[0], owner, group, flags).map(|_| request.return_syscall(0))
        }
    })
}

fn sys_link(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[
        SyscallPathArgument {
            dirfd: None,
            path: Some(0),
            null: false,
            empty: false,
            resolve: false,
            dotlast: None,
            miss: Existing,
        },
        SyscallPathArgument {
            dirfd: None,
            path: Some(1),
            null: false,
            empty: false,
            resolve: false,
            dotlast: Some(Errno::ENOENT),
            miss: Normal,
        },
    ];
    syscall_path_handler(request, "link", ARGV, |paths, request, _proc| {
        linkat(
            None,
            &paths[0],
            None,
            &paths[1],
            LinkatFlags::NoSymlinkFollow,
        )
        .map(|_| request.return_syscall(0))
    })
}

fn sys_symlink(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_handler!(request, |req: &ScmpNotifReq, proc: &RemoteProcess| {
        // SAFETY: No checking of the target is done.
        // This is consistent with the system call.
        const PATH_ARG: SyscallPathArgument = SyscallPathArgument {
            dirfd: None,
            path: Some(1),
            null: false,
            empty: false,
            resolve: false,
            dotlast: None,
            miss: Normal,
        };

        // If sandboxing for all the selected capabilities is off, return immediately.
        let mut check = false;
        let sandbox = request.get_sandbox(false);
        if sandbox.enabled(Capability::CAP_WRITE) {
            check = true;
        }
        drop(sandbox); // release the read lock.
        if !check {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU,
            // however since sandboxing for the respective Capability
            // is off, this is safe here.
            return unsafe { Ok(request.continue_syscall()) };
        }

        // SAFETY: symlink() returns ENOENT if target is an empty string.
        let target = remote_path_n!(proc, req, 0, &request)?;
        if target.is_empty() {
            return Err(Errno::ENOENT);
        }
        let target = OsStr::from_bytes(target.to_bytes());

        let path = proc.read_path(request, &PATH_ARG)?;
        sandbox_path_1(&path, Capability::CAP_WRITE, "symlink", request, proc)?;
        symlinkat(target, None, &path).map(|_| request.return_syscall(0))
    })
}

fn sys_unlink(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: false,
        dotlast: Some(Errno::EINVAL),
        miss: Existing,
    }];
    syscall_path_handler(request, "unlink", ARGV, |paths, request, _proc| {
        unlink(&paths[0]).map(|_| request.return_syscall(0))
    })
}

fn sys_linkat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let flags = req.data.args[4] as nix::libc::c_int;
    let empty = flags & nix::libc::AT_EMPTY_PATH != 0;
    let resolve = flags & nix::libc::AT_SYMLINK_FOLLOW != 0;
    let argv = &[
        SyscallPathArgument {
            dirfd: Some(0),
            path: Some(1),
            null: false,
            dotlast: None,
            miss: Existing,
            empty,
            resolve,
        },
        SyscallPathArgument {
            dirfd: Some(2),
            path: Some(3),
            null: false,
            empty: false,
            resolve: false,
            dotlast: Some(Errno::ENOENT),
            miss: Normal,
        },
    ];
    syscall_path_handler(request, "linkat", argv, |paths, request, _proc| {
        let flags = if resolve {
            LinkatFlags::SymlinkFollow
        } else {
            LinkatFlags::NoSymlinkFollow
        };
        linkat(None, &paths[0], None, &paths[1], flags).map(|_| request.return_syscall(0))
    })
}

fn sys_symlinkat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_handler!(request, |req: &ScmpNotifReq, proc: &RemoteProcess| {
        // SAFETY: No checking of the target is done.
        // This is consistent with the system call.
        const PATH_ARG: SyscallPathArgument = SyscallPathArgument {
            dirfd: Some(1),
            path: Some(2),
            null: false,
            empty: false,
            resolve: false,
            dotlast: None,
            miss: Normal,
        };

        // If sandboxing for all the selected capabilities is off, return immediately.
        let mut check = false;
        let sandbox = request.get_sandbox(false);
        if sandbox.enabled(Capability::CAP_WRITE) {
            check = true;
        }
        drop(sandbox); // release the read lock.
        if !check {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU,
            // however since sandboxing for the respective Capability
            // is off, this is safe here.
            return unsafe { Ok(request.continue_syscall()) };
        }

        // SAFETY: symlinkat() returns ENOENT if target is an empty string.
        let target = remote_path_n!(proc, req, 0, &request)?;
        if target.is_empty() {
            return Err(Errno::ENOENT);
        }
        let target = OsStr::from_bytes(target.to_bytes());

        let path = proc.read_path(request, &PATH_ARG)?;
        sandbox_path_1(&path, Capability::CAP_WRITE, "symlinkat", request, proc)?;
        symlinkat(target, None, &path).map(|_| request.return_syscall(0))
    })
}

fn sys_unlinkat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        empty: false,
        resolve: false,
        dotlast: Some(Errno::EINVAL),
        miss: Existing,
    }];
    syscall_path_handler(request, "unlinkat", ARGV, |paths, request, _proc| {
        let req = request.get_request();
        #[allow(clippy::cast_possible_truncation)]
        let flags = if req.data.args[2] as nix::libc::c_int & nix::libc::AT_REMOVEDIR != 0 {
            UnlinkatFlags::RemoveDir
        } else {
            UnlinkatFlags::NoRemoveDir
        };
        unlinkat(None, &paths[0], flags).map(|_| request.return_syscall(0))
    })
}

fn sys_mkdir(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: false,
        dotlast: Some(Errno::ENOENT),
        miss: Missing,
    }];
    syscall_path_handler(request, "mkdir", ARGV, |paths, request, _proc| {
        let req = request.get_request();
        // SAFETY: syd's umask is 0 here so we respect process' umask.
        #[allow(clippy::cast_possible_wrap)]
        let pid = Pid::from_raw(req.pid as i32);
        let mask = proc_umask(pid)?.bits();
        #[allow(clippy::cast_possible_truncation)]
        #[allow(clippy::cast_sign_loss)]
        let mode = Mode::from_bits_truncate((req.data.args[1] as nix::libc::c_int as u32) & !mask);
        mkdir(&paths[0], mode).map(|_| request.return_syscall(0))
    })
}

fn sys_rmdir(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: Some(Errno::EINVAL),
        miss: Existing,
    }];
    syscall_path_handler(request, "rmdir", ARGV, |paths, request, _proc| {
        unlinkat(None, &paths[0], UnlinkatFlags::RemoveDir).map(|_| request.return_syscall(0))
    })
}

fn sys_mkdirat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        empty: false,
        resolve: false,
        dotlast: Some(Errno::ENOENT),
        miss: Missing,
    }];
    syscall_path_handler(request, "mkdirat", ARGV, |paths, request, _proc| {
        let req = request.get_request();
        // SAFETY: syd's umask is 0 here so we respect process' umask.
        #[allow(clippy::cast_possible_wrap)]
        let pid = Pid::from_raw(req.pid as i32);
        let mask = proc_umask(pid)?.bits();
        #[allow(clippy::cast_possible_truncation)]
        #[allow(clippy::cast_sign_loss)]
        let mode = Mode::from_bits_truncate((req.data.args[2] as nix::libc::c_int as u32) & !mask);
        mkdir(&paths[0], mode).map(|_| request.return_syscall(0))
    })
}

fn sys_mknod(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let kind =
        SFlag::from_bits_truncate(req.data.args[1] as nix::libc::mode_t & SFlag::S_IFMT.bits());
    if !matches!(kind, SFlag::S_IFIFO | SFlag::S_IFREG | SFlag::S_IFSOCK) {
        // SAFETY: We do not allow:
        // 1. Device special files because this may
        //    circumvent path based access control.
        return request.fail_syscall(nix::libc::EACCES);
    }
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Missing,
    }];
    syscall_path_handler(request, "mknod", ARGV, |paths, request, _proc| {
        let req = request.get_request();
        // SAFETY: syd's umask is 0 here so we respect process' umask.
        #[allow(clippy::cast_possible_wrap)]
        let pid = Pid::from_raw(req.pid as i32);
        let mask = proc_umask(pid)?.bits();
        #[allow(clippy::cast_possible_truncation)]
        let perm = Mode::from_bits_truncate(req.data.args[1] as u32 & !SFlag::S_IFMT.bits());
        let perm = Mode::from_bits_truncate(perm.bits() & !mask);
        #[allow(clippy::cast_possible_truncation)]
        let dev = req.data.args[2] as nix::libc::dev_t;
        mknod(&paths[0], kind, perm, dev).map(|_| request.return_syscall(0))
    })
}

fn sys_mknodat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let kind =
        SFlag::from_bits_truncate(req.data.args[2] as nix::libc::mode_t & SFlag::S_IFMT.bits());
    if !matches!(kind, SFlag::S_IFIFO | SFlag::S_IFREG | SFlag::S_IFSOCK) {
        // SAFETY: We do not allow:
        // 1. Device special files because this may
        //    circumvent path based access control.
        return request.fail_syscall(nix::libc::EACCES);
    }
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Missing,
    }];
    syscall_path_handler(request, "mknodat", ARGV, |paths, request, _proc| {
        let req = request.get_request();
        // SAFETY: syd's umask is 0 here so we respect process' umask.
        #[allow(clippy::cast_possible_wrap)]
        let pid = Pid::from_raw(req.pid as i32);
        let mask = proc_umask(pid)?.bits();
        #[allow(clippy::cast_possible_truncation)]
        let perm = Mode::from_bits_truncate(req.data.args[2] as u32 & !SFlag::S_IFMT.bits());
        let perm = Mode::from_bits_truncate(perm.bits() & !mask);
        #[allow(clippy::cast_possible_truncation)]
        let dev = req.data.args[3] as nix::libc::dev_t;
        mknod(&paths[0], kind, perm, dev).map(|_| request.return_syscall(0))
    })
}

fn sys_mount(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[
        SyscallPathArgument {
            dirfd: None,
            path: Some(0),
            null: true,
            empty: false,
            resolve: true,
            dotlast: None,
            miss: Normal,
        },
        SyscallPathArgument {
            dirfd: None,
            path: Some(1),
            null: false,
            empty: false,
            resolve: true,
            dotlast: None,
            miss: Existing,
        },
    ];
    syscall_path_handler(request, "mount", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let source: Option<&PathBuf> = if req.data.args[0] == 0 {
            None
        } else {
            Some(&paths[0])
        };
        let target = if req.data.args[1] == 0 {
            return Err(Errno::EFAULT);
        } else {
            &paths[1]
        };
        let fstype = if req.data.args[2] == 0 {
            None
        } else {
            let mut fstype = vec![0u8; nix::libc::PATH_MAX as usize];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut fstype, req.data.args[2] as usize, request)?;
            Some(PathBuf::from(OsString::from_vec(fstype)))
        };
        let flags = MsFlags::from_bits_truncate(req.data.args[3] as nix::libc::c_ulong);
        let data = if req.data.args[4] == 0 {
            None
        } else {
            let mut data = vec![0u8; nix::libc::PATH_MAX as usize];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut data, req.data.args[4] as usize, request)?;
            Some(PathBuf::from(OsString::from_vec(data)))
        };
        mount(source, target, fstype.as_ref(), flags, data.as_ref())
            .map(|_| request.return_syscall(0))
    })
}

fn sys_umount(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "umount", ARGV, |paths, request, _proc| {
        umount(&paths[0]).map(|_| request.return_syscall(0))
    })
}

fn sys_umount2(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let flags = MntFlags::from_bits_truncate(req.data.args[1] as nix::libc::c_int);
    let argv = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: !flags.contains(MntFlags::UMOUNT_NOFOLLOW),
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "umount2", argv, |paths, request, _proc| {
        umount2(&paths[0], flags).map(|_| request.return_syscall(0))
    })
}

fn sys_creat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARG: SyscallPathArgument = SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        dotlast: None,
        miss: Normal,
        resolve: true,
    };

    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let mode = Mode::from_bits_truncate(req.data.args[1] as nix::libc::mode_t);
    let flags = OFlag::O_CREAT | OFlag::O_WRONLY | OFlag::O_TRUNC;

    syscall_open_handler(request, &ARG, flags, mode)
}

fn sys_open(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let mode = Mode::from_bits_truncate(req.data.args[2] as nix::libc::mode_t);
    #[allow(clippy::cast_possible_truncation)]
    let flags = OFlag::from_bits_truncate(req.data.args[1] as nix::libc::c_int);
    // SAFETY: We do not resolve symbolic links if O_CREAT|O_EXCL
    // is specified to support creating files through dangling
    // symbolic links, see the creat_thru_dangling test for more
    // information.
    let arg = SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: flags.contains(OFlag::O_TMPFILE),
        dotlast: None,
        miss: if flags.contains(OFlag::O_CREAT) {
            Normal
        } else {
            Existing
        },
        resolve: !(flags.contains(OFlag::O_NOFOLLOW)
            || flags.contains(OFlag::O_CREAT | OFlag::O_EXCL)),
    };
    syscall_open_handler(request, &arg, flags, mode)
}

fn sys_openat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let mode = Mode::from_bits_truncate(req.data.args[3] as nix::libc::mode_t);
    #[allow(clippy::cast_possible_truncation)]
    let flags = OFlag::from_bits_truncate(req.data.args[2] as nix::libc::c_int);
    let arg = SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        empty: flags.contains(OFlag::O_TMPFILE),
        dotlast: None,
        miss: if flags.contains(OFlag::O_CREAT) {
            Normal
        } else {
            Existing
        },
        resolve: !(flags.contains(OFlag::O_NOFOLLOW)
            || flags.contains(OFlag::O_CREAT | OFlag::O_EXCL)),
    };
    syscall_open_handler(request, &arg, flags, mode)
}

fn sys_openat2(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_wrap)]
    let proc = RemoteProcess::new(Pid::from_raw(req.pid as i32));
    #[allow(clippy::cast_possible_truncation)]
    let open_how = match proc.remote_ohow(
        req.data.args[2] as usize,
        req.data.args[3] as usize,
        request,
    ) {
        Ok(open_how) => open_how,
        Err(errno) => {
            return request.fail_syscall(errno as i32);
        }
    };
    #[allow(clippy::cast_possible_truncation)]
    let flags = OFlag::from_bits_truncate(open_how.flags as nix::libc::c_int);
    #[allow(clippy::cast_possible_truncation)]
    let mode = Mode::from_bits_truncate(open_how.mode as nix::libc::mode_t);
    let arg = SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        empty: flags.contains(OFlag::O_TMPFILE),
        dotlast: None,
        miss: if flags.contains(OFlag::O_CREAT) {
            Normal
        } else {
            Existing
        },
        resolve: !(flags.contains(OFlag::O_NOFOLLOW)
            || flags.contains(OFlag::O_CREAT | OFlag::O_EXCL)),
    };
    syscall_open_handler(request, &arg, flags, mode)
}

fn sys_rename(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[
        SyscallPathArgument {
            dirfd: None,
            path: Some(0),
            null: false,
            empty: false,
            resolve: false,
            dotlast: Some(Errno::EINVAL),
            miss: Existing,
        },
        SyscallPathArgument {
            dirfd: None,
            path: Some(1),
            null: false,
            empty: false,
            resolve: false,
            dotlast: Some(Errno::EINVAL),
            miss: Normal,
        },
    ];
    syscall_path_handler(request, "rename", ARGV, |paths, request, _proc| {
        renameat(None, &paths[0], None, &paths[1]).map(|_| request.return_syscall(0))
    })
}

fn sys_renameat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[
        SyscallPathArgument {
            dirfd: Some(0),
            path: Some(1),
            null: false,
            empty: false,
            resolve: false,
            dotlast: Some(Errno::EINVAL),
            miss: Existing,
        },
        SyscallPathArgument {
            dirfd: Some(2),
            path: Some(3),
            null: false,
            empty: false,
            resolve: false,
            dotlast: Some(Errno::EINVAL),
            miss: Normal,
        },
    ];
    syscall_path_handler(request, "renameat", ARGV, |paths, request, _proc| {
        renameat(None, &paths[0], None, &paths[1]).map(|_| request.return_syscall(0))
    })
}

fn sys_renameat2(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[
        SyscallPathArgument {
            dirfd: Some(0),
            path: Some(1),
            null: false,
            empty: false,
            resolve: false,
            dotlast: Some(Errno::EINVAL),
            miss: Existing,
        },
        SyscallPathArgument {
            dirfd: Some(2),
            path: Some(3),
            null: false,
            empty: false,
            resolve: false,
            dotlast: Some(Errno::EINVAL),
            miss: Normal,
        },
    ];
    syscall_path_handler(request, "renameat2", ARGV, |paths, request, _proc| {
        let req = request.get_request();
        let path_old = CString::new(paths[0].as_os_str().as_bytes()).map_err(|_| Errno::EINVAL)?;
        let path_new = CString::new(paths[1].as_os_str().as_bytes()).map_err(|_| Errno::EINVAL)?;
        #[allow(clippy::cast_possible_truncation)]
        let flags = req.data.args[4] as u32;
        // Note: musl does not have renameat2 yet.
        // SAFETY: The call to `nix::libc::syscall` for `SYS_renameat2`
        // is safe provided the arguments are correct.
        // `path_old.as_ptr()` and `path_new.as_ptr()` provide valid
        // pointers to null-terminated strings. `flags` is a valid flag
        // for the syscall. Assuming these conditions, the syscall does
        // not lead to undefined behavior.
        if unsafe {
            nix::libc::syscall(
                nix::libc::SYS_renameat2,
                AT_FDCWD,
                path_old.as_ptr(),
                AT_FDCWD,
                path_new.as_ptr(),
                flags,
            )
        } == 0
        {
            Ok(request.return_syscall(0))
        } else {
            Err(Errno::last())
        }
    })
}

fn sys_stat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARG: SyscallPathArgument = SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    };
    syscall_stat_handler(request, &ARG, 1)
}

fn sys_fstat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARG: SyscallPathArgument = SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        null: false,
        empty: true,
        resolve: true,
        dotlast: None,
        miss: Existing,
    };
    syscall_stat_handler(request, &ARG, 1)
}

fn sys_lstat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARG: SyscallPathArgument = SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: false,
        dotlast: None,
        miss: Existing,
    };
    syscall_stat_handler(request, &ARG, 1)
}

fn sys_statx(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    let empty = req.data.args[2] & nix::libc::AT_EMPTY_PATH as u64 != 0;
    let resolve = req.data.args[2] & nix::libc::AT_SYMLINK_NOFOLLOW as u64 == 0;
    let arg = SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        dotlast: None,
        miss: Existing,
        empty,
        resolve,
    };
    syscall_stat_handler(request, &arg, 4)
}

fn sys_newfstatat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    let empty = req.data.args[3] & nix::libc::AT_EMPTY_PATH as u64 != 0;
    let resolve = req.data.args[3] & nix::libc::AT_SYMLINK_NOFOLLOW as u64 == 0;
    let arg = SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        dotlast: None,
        miss: Existing,
        empty,
        resolve,
    };
    syscall_stat_handler(request, &arg, 2)
}

fn sys_readlink(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: false,
        dotlast: None,
        miss: Existing,
    }];
    syscall_readlink_handler(request, ARGV)
}

fn sys_readlinkat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: false,
        empty: false,
        resolve: false,
        dotlast: None,
        miss: Existing,
    }];
    syscall_readlink_handler(request, ARGV)
}

fn syscall_readlink_handler(
    request: &UNotifyEventRequest,
    arg: &[SyscallPathArgument],
) -> ScmpNotifResp {
    syscall_path_handler(request, "readlink", arg, |paths, request, proc| {
        let req = request.get_request();
        let idx = if arg[0].dirfd.is_none() { 1 } else { 2 };
        #[allow(clippy::arithmetic_side_effects)]
        if req.data.args[idx] == 0 {
            return Err(Errno::EFAULT);
        } else if req.data.args[idx + 1] == 0 {
            return Err(Errno::EINVAL);
        }
        #[allow(clippy::arithmetic_side_effects)]
        #[allow(clippy::cast_possible_truncation)]
        let bufsiz = req.data.args[idx + 1] as usize;
        // Cap bufsiz at PATH_MAX
        let bufsiz = bufsiz.min(nix::libc::PATH_MAX as usize);
        // SAFETY: NO_MAGICLINKS!
        // We pass resolve=false to canonicalize, hence only the
        // magiclinks at the last component will be preserved.
        // Here, we handle those safely.
        let path = &paths[0];
        #[allow(clippy::cast_possible_wrap)]
        let pid = Pid::from_raw(req.pid as i32);
        let path = match (proc_fd(pid, path)?, path.as_os_str().as_bytes()) {
            (None, b"/proc/self") => {
                let mut buf = itoa::Buffer::new();
                #[allow(clippy::cast_possible_wrap)]
                PathBuf::from(buf.format(req.pid as i32))
            }
            (None, b"/proc/thread-self") => {
                let mut buf0 = itoa::Buffer::new();
                let mut buf1 = itoa::Buffer::new();
                let mut path = PathBuf::from(buf0.format(proc_tgid(pid)?.as_raw()));
                path.push("task");
                #[allow(clippy::cast_possible_wrap)]
                path.push(buf1.format(req.pid as i32));
                path
            }
            _ => read_link(&paths[0])?,
        };
        // SAFETY: readlink() does not append a terminating null byte to
        // buf. It will (silently) truncate the contents (to a length
        // of bufsiz characters), in case the buffer is too small to
        // hold all of the contents.
        let path = path.as_os_str().as_bytes();
        let path = if path.len() > bufsiz {
            // Truncate
            &path[..bufsiz]
        } else {
            path
        };
        #[allow(clippy::cast_possible_truncation)]
        let bufsiz = proc.write_mem(path, req.data.args[idx] as usize, request)?;
        #[allow(clippy::cast_possible_wrap)]
        Ok(request.return_syscall(bufsiz as i64))
    })
}

fn sys_utime(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "utime", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let path = if req.data.args[0] != 0 {
            Some(&paths[0])
        } else {
            None
        };
        let path = if let Some(path) = path {
            Some(CString::new(path.as_os_str().as_bytes()).map_err(|_| Errno::EINVAL)?)
        } else {
            None
        };
        #[allow(clippy::cast_possible_truncation)]
        let time = match proc.remote_utimbuf(req.data.args[1] as usize, request)? {
            Some(time) => time.as_ptr(),
            None => std::ptr::null(),
        };
        let r = if let Some(path) = path {
            // SAFETY: To be able to pass the arguments
            // path and time as NULL to the system call
            // we need to directly call it here.
            unsafe {
                nix::libc::syscall(nix::libc::SYS_utimensat, AT_FDCWD, path.as_ptr(), time, 0)
            }
        } else {
            let fd = proc.get_fd(req.data.args[0] as RawFd, request)?;
            // SAFETY: The call to `nix::libc::syscall` for
            // `SYS_utimensat` is safe given that the arguments are
            // correct. 'fd.as_raw_fd()' is a valid file descriptor
            // encapsulated in an OwnedFd. 'name' is a valid
            // pointer (in this case, 0, representing a null
            // pointer). 'time' is assumed to be a valid pointer to
            // a timespec array or null. The last argument '0' is a
            // valid flag for the syscall. Assuming these
            // conditions, the syscall does not lead to undefined
            // behavior.
            unsafe { nix::libc::syscall(nix::libc::SYS_utimensat, fd.as_raw_fd(), 0, time, 0) }
        };
        if r == 0 {
            Ok(request.return_syscall(0))
        } else {
            Err(Errno::last())
        }
    })
}

fn sys_utimes(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "utimes", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let path = if req.data.args[0] != 0 {
            Some(&paths[0])
        } else {
            None
        };
        let path = if let Some(path) = path {
            Some(CString::new(path.as_os_str().as_bytes()).map_err(|_| Errno::EINVAL)?)
        } else {
            None
        };
        #[allow(clippy::cast_possible_truncation)]
        let time = match proc.remote_timeval(req.data.args[1] as usize, request)? {
            Some(time) => time.as_ptr(),
            None => std::ptr::null(),
        };
        let r = if let Some(path) = path {
            // SAFETY: To be able to pass the arguments
            // path and time as NULL to the system call
            // we need to directly call it here.
            unsafe {
                nix::libc::syscall(nix::libc::SYS_utimensat, AT_FDCWD, path.as_ptr(), time, 0)
            }
        } else {
            let fd = proc.get_fd(req.data.args[0] as RawFd, request)?;
            // SAFETY: The call to `nix::libc::syscall` for
            // `SYS_utimensat` is safe given that the arguments are
            // correct. 'fd.as_raw_fd()' is a valid file descriptor
            // encapsulated in an OwnedFd. 'name' is a valid
            // pointer (in this case, 0, representing a null
            // pointer). 'time' is assumed to be a valid pointer to
            // a timespec array or null. The last argument '0' is a
            // valid flag for the syscall. Assuming these
            // conditions, the syscall does not lead to undefined
            // behavior.
            unsafe { nix::libc::syscall(nix::libc::SYS_utimensat, fd.as_raw_fd(), 0, time, 0) }
        };
        if r == 0 {
            Ok(request.return_syscall(0))
        } else {
            Err(Errno::last())
        }
    })
}

fn sys_futimesat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    // SAFETY: pathname may be NULL here:
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: true,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "futimesat", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let path = if req.data.args[1] != 0 {
            Some(&paths[0])
        } else {
            None
        };
        let path = if let Some(path) = path {
            Some(CString::new(path.as_os_str().as_bytes()).map_err(|_| Errno::EINVAL)?)
        } else {
            None
        };
        #[allow(clippy::cast_possible_truncation)]
        let time = match proc.remote_timeval(req.data.args[2] as usize, request)? {
            Some(time) => time.as_ptr(),
            None => std::ptr::null(),
        };
        let r = if let Some(path) = path {
            // SAFETY: To be able to pass the arguments
            // path and time as NULL to the system call
            // we need to directly call it here.
            unsafe {
                nix::libc::syscall(nix::libc::SYS_utimensat, AT_FDCWD, path.as_ptr(), time, 0)
            }
        } else {
            let fd = proc.get_fd(req.data.args[0] as RawFd, request)?;
            // SAFETY: The call to `nix::libc::syscall` for
            // `SYS_utimensat` is safe given that the arguments are
            // correct. 'fd.as_raw_fd()' is a valid file descriptor
            // encapsulated in an OwnedFd. 'name' is a valid
            // pointer (in this case, 0, representing a null
            // pointer). 'time' is assumed to be a valid pointer to
            // a timespec array or null. The last argument '0' is a
            // valid flag for the syscall. Assuming these
            // conditions, the syscall does not lead to undefined
            // behavior.
            unsafe { nix::libc::syscall(nix::libc::SYS_utimensat, fd.as_raw_fd(), 0, time, 0) }
        };
        if r == 0 {
            Ok(request.return_syscall(0))
        } else {
            Err(Errno::last())
        }
    })
}

fn sys_utimensat(request: &UNotifyEventRequest) -> ScmpNotifResp {
    let req = request.get_request();
    #[allow(clippy::cast_possible_truncation)]
    let resolve = req.data.args[3] as nix::libc::c_int & nix::libc::AT_SYMLINK_NOFOLLOW == 0;
    // SAFETY: pathname may be NULL here:
    let argv = &[SyscallPathArgument {
        dirfd: Some(0),
        path: Some(1),
        null: true,
        empty: false,
        dotlast: None,
        miss: Existing,
        resolve,
    }];
    syscall_path_handler(request, "utimensat", argv, |paths, request, proc| {
        let req = request.get_request();
        let path = if req.data.args[1] != 0 {
            Some(&paths[0])
        } else {
            None
        };
        let path = if let Some(path) = path {
            Some(CString::new(path.as_os_str().as_bytes()).map_err(|_| Errno::EINVAL)?)
        } else {
            None
        };
        #[allow(clippy::cast_possible_truncation)]
        let time = match proc.remote_timespec(req.data.args[2] as usize, request)? {
            Some(time) => time.as_ptr(),
            None => std::ptr::null(),
        };
        let r = if let Some(path) = path {
            // SAFETY: To be able to pass the arguments
            // path and time as NULL to the system call
            // we need to directly call it here.
            unsafe {
                nix::libc::syscall(
                    nix::libc::SYS_utimensat,
                    AT_FDCWD,
                    path.as_ptr(),
                    time,
                    req.data.args[3],
                )
            }
        } else {
            let fd = proc.get_fd(req.data.args[0] as RawFd, request)?;
            // SAFETY: The call to `nix::libc::syscall` with
            // `SYS_utimensat` is safe provided that the arguments
            // are correct. `fd.as_raw_fd()` provides a valid file
            // descriptor. `name`, if not null, points to a valid
            // CString, and if null, it correctly represents a null
            // pointer. `time` is assumed to be a valid pointer to
            // a timespec array or null. The last argument,
            // `req.data.args[3]`, is expected to be a valid flag.
            // As long as these conditions are met, the syscall
            // should not lead to undefined behavior.
            unsafe {
                nix::libc::syscall(
                    nix::libc::SYS_utimensat,
                    fd.as_raw_fd(),
                    0,
                    time,
                    req.data.args[3],
                )
            }
        };
        if r == 0 {
            Ok(request.return_syscall(0))
        } else {
            Err(Errno::last())
        }
    })
}

fn sys_truncate(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "truncate", ARGV, |paths, request, _proc| {
        let req = request.get_request();
        #[allow(clippy::cast_possible_wrap)]
        let len = req.data.args[1] as nix::libc::off_t;
        truncate(&paths[0], len).map(|_| request.return_syscall(0))
    })
}

fn sys_getxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "getxattr", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let path = &paths[0];
        let path = CString::new(path.as_os_str().as_bytes()).map_err(|_| Errno::ENOENT)?;
        let name = if req.data.args[1] != 0 {
            let mut buf = vec![0u8; nix::libc::PATH_MAX as usize];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut buf, req.data.args[1] as usize, request)?;
            Some(buf)
        } else {
            None
        };
        let name = if let Some(ref name) = name {
            CStr::from_bytes_until_nul(name)
                .map_err(|_| Errno::E2BIG)?
                .as_ptr()
        } else {
            std::ptr::null()
        };
        // SAFETY: The size argument to the getxattr call
        // must not be fully trusted, it can be overly large,
        // and allocating a Vector of that capacity may overflow.
        #[allow(clippy::cast_possible_truncation)]
        let len = req.data.args[3] as usize;
        let len = len.min(nix::libc::PATH_MAX as usize); // Cap count at PATH_MAX
        let mut buf = if len == 0 { None } else { Some(vec![0u8; len]) };
        let ptr = match buf.as_mut() {
            Some(b) => b.as_mut_ptr(),
            None => std::ptr::null_mut(),
        };
        // SAFETY: In libc we trust.
        let n = unsafe {
            nix::libc::getxattr(
                path.as_ptr(),
                name,
                ptr as *mut _ as *mut nix::libc::c_void,
                len,
            )
        };
        let n = if n == -1 {
            return Err(Errno::last());
        } else {
            n as usize
        };
        if let Some(buf) = buf {
            #[allow(clippy::cast_possible_truncation)]
            proc.write_mem(&buf[..n], req.data.args[2] as usize, request)?;
        }
        #[allow(clippy::cast_possible_wrap)]
        Ok(request.return_syscall(n as i64))
    })
}

fn sys_lgetxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: false,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "lgetxattr", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let path = &paths[0];
        let path = CString::new(path.as_os_str().as_bytes()).map_err(|_| Errno::ENOENT)?;
        let name = if req.data.args[1] != 0 {
            let mut buf = vec![0u8; nix::libc::PATH_MAX as usize];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut buf, req.data.args[1] as usize, request)?;
            Some(buf)
        } else {
            None
        };
        let name = if let Some(ref name) = name {
            CStr::from_bytes_until_nul(name)
                .map_err(|_| Errno::E2BIG)?
                .as_ptr()
        } else {
            std::ptr::null()
        };
        // SAFETY: The size argument to the lgetxattr call
        // must not be fully trusted, it can be overly large,
        // and allocating a Vector of that capacity may overflow.
        #[allow(clippy::cast_possible_truncation)]
        let len = req.data.args[3] as usize;
        let len = len.min(nix::libc::PATH_MAX as usize); // Cap count at PATH_MAX
        let mut buf = if len == 0 { None } else { Some(vec![0u8; len]) };
        let ptr = match buf.as_mut() {
            Some(b) => b.as_mut_ptr(),
            None => std::ptr::null_mut(),
        };
        // SAFETY: In libc we trust.
        let n = unsafe {
            nix::libc::lgetxattr(
                path.as_ptr(),
                name,
                ptr as *mut _ as *mut nix::libc::c_void,
                len,
            )
        };
        let n = if n == -1 {
            return Err(Errno::last());
        } else {
            n as usize
        };
        if let Some(buf) = buf {
            #[allow(clippy::cast_possible_truncation)]
            proc.write_mem(&buf[..n], req.data.args[2] as usize, request)?;
        }
        #[allow(clippy::cast_possible_wrap)]
        Ok(request.return_syscall(n as i64))
    })
}

fn sys_fgetxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "fgetxattr", ARGV, |_paths, request, proc| {
        let req = request.get_request();
        let name = if req.data.args[1] != 0 {
            let mut buf = vec![0u8; nix::libc::PATH_MAX as usize];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut buf, req.data.args[1] as usize, request)?;
            Some(buf)
        } else {
            None
        };
        let name = if let Some(ref name) = name {
            CStr::from_bytes_until_nul(name)
                .map_err(|_| Errno::E2BIG)?
                .as_ptr()
        } else {
            std::ptr::null()
        };
        // SAFETY: The size argument to the fgetxattr call
        // must not be fully trusted, it can be overly large,
        // and allocating a Vector of that capacity may overflow.
        #[allow(clippy::cast_possible_truncation)]
        let len = req.data.args[3] as usize;
        let len = len.min(nix::libc::PATH_MAX as usize); // Cap count at PATH_MAX
        let mut buf = if len == 0 { None } else { Some(vec![0u8; len]) };
        let ptr = match buf.as_mut() {
            Some(b) => b.as_mut_ptr(),
            None => std::ptr::null_mut(),
        };
        let fd = proc.get_fd(req.data.args[0] as RawFd, request)?;
        // SAFETY: In libc we trust.
        let n = unsafe {
            nix::libc::fgetxattr(
                fd.as_raw_fd(),
                name,
                ptr as *mut _ as *mut nix::libc::c_void,
                len,
            )
        };
        let e = Errno::last();
        let n = if n == -1 {
            return Err(e);
        } else {
            n as usize
        };
        if let Some(buf) = buf {
            #[allow(clippy::cast_possible_truncation)]
            proc.write_mem(&buf[..n], req.data.args[2] as usize, request)?;
        }
        #[allow(clippy::cast_possible_wrap)]
        Ok(request.return_syscall(n as i64))
    })
}

fn sys_setxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "setxattr", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let path = &paths[0];
        let path = CString::new(path.as_os_str().as_bytes()).map_err(|_| Errno::EINVAL)?;
        let name = if req.data.args[1] != 0 {
            let mut buf = vec![0u8; nix::libc::PATH_MAX as usize];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut buf, req.data.args[1] as usize, request)?;
            Some(buf)
        } else {
            None
        };
        let name = if let Some(ref name) = name {
            CStr::from_bytes_until_nul(name)
                .map_err(|_| Errno::E2BIG)?
                .as_ptr()
        } else {
            std::ptr::null()
        };
        // SAFETY: The size argument to the setxattr call
        // must not be fully trusted, it can be overly large,
        // and allocating a Vector of that capacity may overflow.
        let (buf, len) = if req.data.args[3] == 0 {
            (None, 0)
        } else {
            #[allow(clippy::cast_possible_truncation)]
            let len = req.data.args[3] as usize;
            let len = len.min(nix::libc::PATH_MAX as usize); // Cap count at PATH_MAX
            let mut buf = vec![0u8; len];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut buf, req.data.args[2] as usize, request)?;
            (Some(buf), len)
        };
        let buf = if let Some(mut buf) = buf {
            buf.as_mut_ptr()
        } else {
            std::ptr::null_mut()
        };
        #[allow(clippy::cast_possible_truncation)]
        let flags = req.data.args[4] as nix::libc::c_int;

        // SAFETY: In libc we trust.
        if unsafe {
            nix::libc::setxattr(
                path.as_ptr(),
                name,
                buf as *mut _ as *mut nix::libc::c_void,
                len,
                flags,
            )
        } == 0
        {
            Ok(request.return_syscall(0))
        } else {
            Err(Errno::last())
        }
    })
}

fn sys_fsetxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "fsetxattr", ARGV, |_paths, request, proc| {
        let req = request.get_request();
        let name = if req.data.args[1] != 0 {
            let mut buf = vec![0u8; nix::libc::PATH_MAX as usize];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut buf, req.data.args[1] as usize, request)?;
            Some(buf)
        } else {
            None
        };
        let name = if let Some(ref name) = name {
            CStr::from_bytes_until_nul(name)
                .map_err(|_| Errno::E2BIG)?
                .as_ptr()
        } else {
            std::ptr::null()
        };
        // SAFETY: The size argument to the lsetxattr call
        // must not be fully trusted, it can be overly large,
        // and allocating a Vector of that capacity may overflow.
        let (buf, len) = if req.data.args[3] == 0 {
            (None, 0)
        } else {
            #[allow(clippy::cast_possible_truncation)]
            let len = req.data.args[3] as usize;
            let len = len.min(nix::libc::PATH_MAX as usize); // Cap count at PATH_MAX
            let mut buf = vec![0u8; len];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut buf, req.data.args[2] as usize, request)?;
            (Some(buf), len)
        };
        let buf = if let Some(mut buf) = buf {
            buf.as_mut_ptr()
        } else {
            std::ptr::null_mut()
        };
        #[allow(clippy::cast_possible_truncation)]
        let flags = req.data.args[4] as nix::libc::c_int;
        let fd = proc.get_fd(req.data.args[0] as RawFd, request)?;
        // SAFETY: In libc we trust.
        if unsafe {
            nix::libc::fsetxattr(
                fd.as_raw_fd(),
                name,
                buf as *mut _ as *mut nix::libc::c_void,
                len,
                flags,
            )
        } == 0
        {
            Ok(request.return_syscall(0))
        } else {
            Err(Errno::last())
        }
    })
}

fn sys_lsetxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: false,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "lsetxattr", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let path = &paths[0];
        let path = CString::new(path.as_os_str().as_bytes()).map_err(|_| Errno::EINVAL)?;
        let name = if req.data.args[1] != 0 {
            let mut buf = vec![0u8; nix::libc::PATH_MAX as usize];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut buf, req.data.args[1] as usize, request)?;
            Some(buf)
        } else {
            None
        };
        let name = if let Some(ref name) = name {
            CStr::from_bytes_until_nul(name)
                .map_err(|_| Errno::E2BIG)?
                .as_ptr()
        } else {
            std::ptr::null()
        };
        // SAFETY: The size argument to the lsetxattr call
        // must not be fully trusted, it can be overly large,
        // and allocating a Vector of that capacity may overflow.
        let (buf, len) = if req.data.args[3] == 0 {
            (None, 0)
        } else {
            #[allow(clippy::cast_possible_truncation)]
            let len = req.data.args[3] as usize;
            let len = len.min(nix::libc::PATH_MAX as usize); // Cap count at PATH_MAX
            let mut buf = vec![0u8; len];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut buf, req.data.args[2] as usize, request)?;
            (Some(buf), len)
        };
        let buf = if let Some(mut buf) = buf {
            buf.as_mut_ptr()
        } else {
            std::ptr::null_mut()
        };
        #[allow(clippy::cast_possible_truncation)]
        let flags = req.data.args[4] as nix::libc::c_int;
        // SAFETY: In libc we trust.
        if unsafe {
            nix::libc::lsetxattr(
                path.as_ptr(),
                name,
                buf as *mut _ as *mut nix::libc::c_void,
                len,
                flags,
            )
        } == 0
        {
            Ok(request.return_syscall(0))
        } else {
            Err(Errno::last())
        }
    })
}

fn sys_listxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "listxattr", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let path = &paths[0];
        let path = CString::new(path.as_os_str().as_bytes()).map_err(|_| Errno::ENOENT)?;
        // SAFETY: The size argument to the listxattr call
        // must not be fully trusted, it can be overly large,
        // and allocating a Vector of that capacity may overflow.
        #[allow(clippy::cast_possible_truncation)]
        let len = req.data.args[2] as usize;
        let len = len.min(nix::libc::PATH_MAX as usize); // Cap count at PATH_MAX
        let mut buf = if len == 0 { None } else { Some(vec![0u8; len]) };
        let ptr = match buf.as_mut() {
            Some(b) => b.as_mut_ptr(),
            None => std::ptr::null_mut(),
        };
        // SAFETY: In libc we trust.
        let n = unsafe {
            nix::libc::listxattr(path.as_ptr(), ptr as *mut _ as *mut nix::libc::c_char, len)
        };
        let n = if n == -1 {
            return Err(Errno::last());
        } else {
            n as usize
        };
        if let Some(buf) = buf {
            #[allow(clippy::cast_possible_truncation)]
            proc.write_mem(&buf[..n], req.data.args[1] as usize, request)?;
        }
        #[allow(clippy::cast_possible_wrap)]
        Ok(request.return_syscall(n as i64))
    })
}

fn sys_flistxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        null: false,
        empty: true,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "flistxattr", ARGV, |_paths, request, proc| {
        let req = request.get_request();
        // SAFETY: The size argument to the flistxattr call
        // must not be fully trusted, it can be overly large,
        // and allocating a Vector of that capacity may overflow.
        #[allow(clippy::cast_possible_truncation)]
        let len = req.data.args[2] as usize;
        let len = len.min(nix::libc::PATH_MAX as usize); // Cap count at PATH_MAX
        let mut buf = if len == 0 { None } else { Some(vec![0u8; len]) };
        let ptr = match buf.as_mut() {
            Some(b) => b.as_mut_ptr(),
            None => std::ptr::null_mut(),
        };
        let fd = proc.get_fd(req.data.args[0] as RawFd, request)?;
        // SAFETY: In libc we trust.
        let n = unsafe {
            nix::libc::flistxattr(fd.as_raw_fd(), ptr as *mut _ as *mut nix::libc::c_char, len)
        };
        let n = if n == -1 {
            return Err(Errno::last());
        } else {
            n as usize
        };
        if let Some(buf) = buf {
            #[allow(clippy::cast_possible_truncation)]
            proc.write_mem(&buf[..n], req.data.args[1] as usize, request)?;
        }
        #[allow(clippy::cast_possible_wrap)]
        Ok(request.return_syscall(n as i64))
    })
}

fn sys_llistxattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: false,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "llistxattr", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let path = &paths[0];
        let path = CString::new(path.as_os_str().as_bytes()).map_err(|_| Errno::ENOENT)?;
        // SAFETY: The size argument to the llistxattr call
        // must not be fully trusted, it can be overly large,
        // and allocating a Vector of that capacity may overflow.
        #[allow(clippy::cast_possible_truncation)]
        let len = req.data.args[2] as usize;
        let len = len.min(nix::libc::PATH_MAX as usize); // Cap count at PATH_MAX
        let mut buf = if len == 0 { None } else { Some(vec![0u8; len]) };
        let ptr = match buf.as_mut() {
            Some(b) => b.as_mut_ptr(),
            None => std::ptr::null_mut(),
        };
        // SAFETY: In libc we trust.
        let n = unsafe {
            nix::libc::llistxattr(path.as_ptr(), ptr as *mut _ as *mut nix::libc::c_char, len)
        };
        let n = if n == -1 {
            return Err(Errno::last());
        } else {
            n as usize
        };
        if let Some(buf) = buf {
            #[allow(clippy::cast_possible_truncation)]
            proc.write_mem(&buf[..n], req.data.args[1] as usize, request)?;
        }
        #[allow(clippy::cast_possible_wrap)]
        Ok(request.return_syscall(n as i64))
    })
}

fn sys_removexattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "removexattr", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let path = &paths[0];
        let path = CString::new(path.as_os_str().as_bytes()).map_err(|_| Errno::ENOENT)?;
        let name = if req.data.args[1] != 0 {
            let mut buf = vec![0u8; nix::libc::PATH_MAX as usize];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut buf, req.data.args[1] as usize, request)?;
            Some(buf)
        } else {
            None
        };
        let name = if let Some(ref name) = name {
            CStr::from_bytes_until_nul(name)
                .map_err(|_| Errno::E2BIG)?
                .as_ptr()
        } else {
            std::ptr::null()
        };
        // SAFETY: In libc we trust.
        if unsafe { nix::libc::removexattr(path.as_ptr(), name) } == 0 {
            Ok(request.return_syscall(0))
        } else {
            Err(Errno::last())
        }
    })
}

fn sys_fremovexattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: Some(0),
        path: None,
        null: false,
        empty: true,
        resolve: true,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "fremovexattr", ARGV, |_paths, request, proc| {
        let req = request.get_request();
        let mut buf = vec![0u8; nix::libc::PATH_MAX as usize];
        let name = if req.data.args[1] != 0 {
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut buf, req.data.args[1] as usize, request)?;
            Some(CStr::from_bytes_until_nul(&buf).map_err(|_| Errno::E2BIG)?)
        } else {
            None
        };
        let name = if let Some(name) = name {
            name.as_ptr()
        } else {
            std::ptr::null()
        };
        let fd = proc.get_fd(req.data.args[0] as RawFd, request)?;
        // SAFETY: In libc we trust.
        if unsafe { nix::libc::fremovexattr(fd.as_raw_fd(), name) } == 0 {
            Ok(request.return_syscall(0))
        } else {
            Err(Errno::last())
        }
    })
}

fn sys_lremovexattr(request: &UNotifyEventRequest) -> ScmpNotifResp {
    const ARGV: &[SyscallPathArgument] = &[SyscallPathArgument {
        dirfd: None,
        path: Some(0),
        null: false,
        empty: false,
        resolve: false,
        dotlast: None,
        miss: Existing,
    }];
    syscall_path_handler(request, "lremovexattr", ARGV, |paths, request, proc| {
        let req = request.get_request();
        let path = &paths[0];
        let path = CString::new(path.as_os_str().as_bytes()).map_err(|_| Errno::EINVAL)?;
        let name = if req.data.args[1] != 0 {
            let mut buf = vec![0u8; nix::libc::PATH_MAX as usize];
            #[allow(clippy::cast_possible_truncation)]
            proc.read_mem(&mut buf, req.data.args[1] as usize, request)?;
            Some(buf)
        } else {
            None
        };
        let name = if let Some(ref name) = name {
            CStr::from_bytes_until_nul(name)
                .map_err(|_| Errno::E2BIG)?
                .as_ptr()
        } else {
            std::ptr::null()
        };
        // SAFETY: In libc we trust.
        if unsafe { nix::libc::lremovexattr(path.as_ptr(), name) } == 0 {
            Ok(request.return_syscall(0))
        } else {
            Err(Errno::last())
        }
    })
}

fn sys_getrandom(request: &UNotifyEventRequest) -> ScmpNotifResp {
    syscall_handler!(request, |req: &ScmpNotifReq, _proc: &RemoteProcess| {
        let path = if req.data.args[2] & u64::from(nix::libc::GRND_RANDOM) != 0 {
            "/dev/random"
        } else {
            "/dev/urandom"
        };

        // Check for access.
        let sandbox = request.get_sandbox(false);
        if !sandbox.enabled(Capability::CAP_READ) {
            // SAFETY: Read sandboxing is not enabled.
            // This is safe to continue.
            return Ok(unsafe { request.continue_syscall() });
        }
        let action = sandbox.check_path(Capability::CAP_READ, path);
        if action == Action::Allow {
            // SAFETY: No pointer deref in access check.
            // This is safe to continue.
            return Ok(unsafe { request.continue_syscall() });
        }
        drop(sandbox); // release the read lock.

        match action {
            Action::Filter => Err(Errno::EACCES),
            Action::Deny => {
                // Report access violation.
                warn!("ctx": "access",
                    "cap": Capability::CAP_READ,
                    "path": path,
                    "pid": req.pid,
                    "sys": "getrandom");
                Err(Errno::EACCES)
            }
            _ => unreachable!(),
        }
    })
}

/// Handles syscalls related to signal handling, protecting the syd
/// process and their threads from signals.
///
/// # Parameters
///
/// - `request`: User notification request from seccomp.
/// - `group`: true if the system call has both progress group id and process id (tgkill), false otherwise.
/// - `syscall_name`: The name of the syscall being handled, used for logging and error reporting.
/// # Returns
///
/// - `ScmpNotifResp`: Response indicating the result of the syscall handling.
fn syscall_signal_handler(request: &UNotifyEventRequest, group: bool) -> ScmpNotifResp {
    let req = request.get_request();

    #[allow(clippy::cast_possible_truncation)]
    let pid = [
        req.data.args[0] as nix::libc::pid_t,
        req.data.args[1] as nix::libc::pid_t,
    ];

    let mut bad = false;
    for (idx, tid) in pid.iter().enumerate() {
        if faccessat(
            None,
            format!("/proc/self/task/{tid}").as_str(),
            AccessFlags::F_OK,
            AtFlags::AT_SYMLINK_NOFOLLOW,
        )
        .is_ok()
        {
            bad = true;
            break;
        } else if idx >= 1 || !group {
            // all except tgkill get a single pid argument.
            break;
        }
    }

    if bad {
        return request.fail_syscall(Errno::EACCES as i32);
    }
    // SAFETY: This is safe because we haven't dereferenced
    // any pointers during access check.
    unsafe { request.continue_syscall() }
}

///
/// Handles syscalls related to paths, reducing code redundancy and ensuring a uniform way of dealing with paths.
///
/// # Parameters
///
/// - `request`: User notification request from seccomp.
/// - `syscall_name`: The name of the syscall being handled, used for logging and error reporting.
/// - `arg_mappings`: Non-empty list of argument mappings containing dirfd and path indexes, if applicable.
/// - `handler`: Closure that processes the constructed canonical paths and performs additional syscall-specific operations.
///
/// # Returns
///
/// - `ScmpNotifResp`: Response indicating the result of the syscall handling.
#[allow(clippy::cognitive_complexity)]
fn syscall_path_handler<H>(
    request: &UNotifyEventRequest,
    syscall_name: &str,
    path_argv: &[SyscallPathArgument],
    handler: H,
) -> ScmpNotifResp
where
    H: Fn(&[PathBuf], &UNotifyEventRequest, &RemoteProcess) -> Result<ScmpNotifResp, Errno>,
{
    syscall_handler!(request, |req: &ScmpNotifReq, proc: &RemoteProcess| {
        // Determine system call capabilities.
        let caps = Capability::try_from((proc, request, req, syscall_name))?;

        // If sandboxing for all the selected capabilities is off, return immediately.
        // Exception: CAP_EXEC is available and we have patterns in exec/kill list.
        let mut check = false;
        let sandbox = request.get_sandbox(false);
        if caps.contains(Capability::CAP_EXEC) && sandbox.has_exec_kill() {
            check = true;
        } else {
            for cap in [
                Capability::CAP_READ,
                Capability::CAP_STAT,
                Capability::CAP_WRITE,
                Capability::CAP_EXEC,
                Capability::CAP_CONNECT,
                Capability::CAP_BIND,
            ] {
                if sandbox.enabled(cap) {
                    check = true;
                    break;
                }
            }
        }
        drop(sandbox); // release the read lock.
        if !check {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU,
            // however since sandboxing for the respective Capability
            // is off, this is safe here.
            return unsafe { Ok(request.continue_syscall()) };
        }

        let mut paths: SmallVec<[PathBuf; 2]> = SmallVec::new();
        for arg in path_argv {
            paths.push(proc.read_path(request, arg)?);
        }

        // Call sandbox access checker.
        match paths.len() {
            1 => {
                sandbox_path_1(&paths[0], caps, syscall_name, request, proc)?;
            }
            2 => {
                sandbox_path_2(paths.as_slice(), caps, syscall_name, request, proc)?;
            }
            _ => unreachable!(),
        }

        // Call system call handler
        handler(&paths, request, proc)
    })
}

#[allow(clippy::cognitive_complexity)]
fn syscall_open_handler(
    request: &UNotifyEventRequest,
    arg: &SyscallPathArgument,
    flags: OFlag,
    mode: Mode,
) -> ScmpNotifResp {
    syscall_handler!(request, |req: &ScmpNotifReq, proc: &RemoteProcess| {
        let o_path = flags.contains(OFlag::O_PATH);
        let cap = if o_path {
            Capability::CAP_READ
        } else if flags.contains(OFlag::O_CREAT) {
            Capability::CAP_WRITE
        } else {
            match flags & OFlag::O_ACCMODE {
                OFlag::O_RDONLY => Capability::CAP_READ,
                _ => Capability::CAP_WRITE,
            }
        };

        let sandbox = request.get_sandbox(false);
        #[allow(clippy::cast_possible_wrap)]
        let is_lock = sandbox.locked_for_pid(req.pid as nix::libc::pid_t);
        let enabled = sandbox.enabled(cap);
        if is_lock && !enabled {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU.
            // Since the sandbox is off, this is fine here.
            return unsafe { Ok(request.continue_syscall()) };
        }
        drop(sandbox); // release the read lock.

        // Read the remote path.
        let (mut path, orig) = proc.read_path_opt(request, arg)?;
        let orig = orig.unwrap_or(PathBuf::default()); // used for logging.

        // Handle the special /dev/syd paths.
        if path.as_os_str().as_bytes() == MAGIC_PREFIX.as_bytes() {
            if o_path {
                return Err(Errno::EINVAL);
            }
            if is_lock {
                return Err(Errno::ENOENT);
            }
            let sandbox = request.get_sandbox(false);
            let content = if cap.contains(Capability::CAP_READ) {
                Some(serde_json::to_string(&*sandbox).map_err(|_| Errno::EINVAL)?)
            } else {
                None
            };
            drop(sandbox); // release the read lock.

            let fd = if let Some(content) = content {
                let fd = open(
                    ".",
                    OFlag::O_RDWR | OFlag::O_TMPFILE,
                    Mode::S_IRUSR | Mode::S_IWUSR,
                )?;
                write(fd, content.as_bytes())?;
                lseek(fd, 0, Whence::SeekSet)?;
                fd
            } else {
                open("/dev/null", flags, mode)?
            };

            // Send the file descriptor to the process and return the fd no.
            let result = request.add_fd(fd, true);
            let _ = close(fd);
            return result.map(|fd| request.return_syscall(i64::from(fd)));
        } else if !enabled {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU.
            // Since the sandbox is off, this is fine here.
            return unsafe { Ok(request.continue_syscall()) };
        }

        // SAFETY: We must provide safe access to
        // sandbox process' controlling terminal.
        #[allow(clippy::cast_possible_wrap)]
        let pid = Pid::from_raw(req.pid as i32);
        if path == Path::new("/dev/tty") {
            let dev_tty = proc_tty(pid)?;
            if !request.is_valid() {
                return Err(Errno::ESRCH);
            }
            path = dev_tty;
        }

        // Check for access.
        let sandbox = request.get_sandbox(false);
        let action = sandbox.check_path(cap, &path);
        let hidden = if action != Action::Allow {
            sandbox.is_hidden(&path)
        } else {
            // No need for hidden check if we're allowing.
            false
        };
        let trace = sandbox.trace();
        drop(sandbox); // release the read lock.

        // Perform action: allow->emulate, deny->log.
        match action {
            Action::Allow => {
                // The system call is allowed.
                // To prevent TOCTOU, we open the file ourselves,
                // and put the file descriptor to the process'
                // address space with SECCOMP_IOCTL_NOTIF_ADDFD.

                if o_path {
                    // SAFETY:
                    // seccomp addfd operation returns EBADF for O_PATH file
                    // descriptors so there's no TOCTOU-free way to emulate
                    // this as of yet. However we did our best by
                    // delaying continue up to this point, thereby
                    // including the open request to the sandbox access
                    // check.
                    return unsafe { Ok(request.continue_syscall()) };
                }

                // Resolve /proc file descriptor links.
                // Use get_fd if fd belongs to the current process.
                // SAFETY: Magic symlinks are disallowed with ELOOP here, see proc_fd.
                let result = if let Some(fd) = proc_fd(pid, &path)? {
                    proc.get_fd(fd, request).map_err(|e| match e {
                        Errno::EBADF => Errno::ENOENT,
                        _ => e,
                    })
                } else {
                    let mode = if !flags.intersects(OFlag::O_CREAT | OFlag::O_TMPFILE) {
                        // SAFETY: Mode must be 0 if O_CREAT or O_TMPFILE is not in flags.
                        0
                    } else {
                        // SAFETY: syd's umask is 0 here so we respect process' umask.
                        (mode & !proc_umask(pid)?).bits()
                    };
                    // SAFETY: We have already resolved the symbolic
                    // links in the path as necessary, to prevent a
                    // time-of-check to time-of-use vector we add
                    // O_NOFOLLOW to flags here.
                    let flags = (flags | OFlag::O_NOFOLLOW).bits();
                    let mut how = openat2::OpenHow::new(flags, mode);
                    how.resolve =
                        openat2::ResolveFlags::NO_MAGICLINKS | openat2::ResolveFlags::NO_SYMLINKS;
                    openat2::openat2(None, &path, &how)
                        .map(|fd|
                            // SAFETY: openat2 returns a valid FD.
                            unsafe { OwnedFd::from_raw_fd(fd) })
                        .map_err(|e| Errno::from_i32(e.raw_os_error().unwrap_or(nix::libc::EINVAL)))
                };

                let fd = match result {
                    Ok(fd) => {
                        trace!("ctx": "open",
                            "pid": pid.as_raw(),
                            "path": format!("{}", path.display()),
                            "flags": flags.bits(),
                            "mode": mode.bits());
                        fd
                    }
                    Err(errno)
                        if errno == Errno::EEXIST
                            || errno == Errno::ENOENT
                            || errno == Errno::ENOTDIR =>
                    {
                        debug!("ctx": "open",
                            "pid": pid.as_raw(),
                            "errno": errno as i32,
                            "path": format!("{}", path.display()),
                            "orig": format!("{}", orig.display()),
                            "flags": flags.bits(),
                            "mode": mode.bits());
                        return Err(errno);
                    }
                    Err(errno) => {
                        info!("ctx": "open",
                            "pid": pid.as_raw(),
                            "errno": errno as i32,
                            "path": format!("{}", path.display()),
                            "orig": format!("{}", orig.display()),
                            "flags": flags.bits(),
                            "mode": mode.bits());
                        return Err(errno);
                    }
                };
                let result = request.add_fd(fd.as_raw_fd(), flags.contains(OFlag::O_CLOEXEC));
                drop(fd);
                result.map(|fd| request.return_syscall(i64::from(fd)))
            }
            Action::Deny | Action::Filter if hidden => {
                // SAFETY: We do not report violations when the path is
                // hidden and return ENOENT, so as to make read/write/exec
                // sandboxing consistent with stat sandboxing. With this
                // restriction in place, it is not possible to enumerate
                // existing hidden paths by attempting to read, write or
                // execute them.
                // Two exceptions:
                // 1. Extended logging is enabled: level=debug
                // 2. Trace mode is enabled: level=warn

                if trace {
                    warn!("ctx": "access",
                        "act": action,
                        "cap": cap,
                        "path": format!("{}", path.display()),
                        "orig": format!("{}", orig.display()),
                        "flags": flags.bits(),
                        "mode": mode.bits(),
                        "pid": pid.as_raw(),
                        "sys": "open");
                } else {
                    debug!("ctx": "access",
                        "act": action,
                        "cap": cap,
                        "path": format!("{}", path.display()),
                        "orig": format!("{}", orig.display()),
                        "flags": flags.bits(),
                        "mode": mode.bits(),
                        "pid": pid.as_raw(),
                        "sys": "open");
                }

                Err(Errno::ENOENT)
            }
            Action::Filter | Action::Kill => Err(Errno::EACCES),
            Action::Deny => {
                // Report access violation.
                warn!("ctx": "access",
                    "cap": cap,
                    "path": format!("{}", path.display()),
                    "orig": format!("{}", orig.display()),
                    "flags": flags.bits(),
                    "mode": mode.bits(),
                    "pid": pid.as_raw(),
                    "sys": "open");
                Err(Errno::EACCES)
            }
        }
    })
}

#[allow(clippy::cognitive_complexity)]
fn syscall_stat_handler(
    request: &UNotifyEventRequest,
    arg: &SyscallPathArgument,
    arg_stat: usize,
) -> ScmpNotifResp {
    syscall_handler!(request, |req: &ScmpNotifReq, proc: &RemoteProcess| {
        // If sandboxing for CAP_STAT is off, and magic lock is set, return immediately.
        let sandbox = request.get_sandbox(false);
        #[allow(clippy::cast_possible_wrap)]
        let is_lock = sandbox.locked_for_pid(req.pid as nix::libc::pid_t);
        let is_stat = sandbox.enabled(Capability::CAP_STAT);
        if is_lock && !is_stat {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU.
            // However, since stat sandboxing is disabled here this is
            // safe to call.
            return unsafe { Ok(request.continue_syscall()) };
        }
        drop(sandbox); // release the read lock.

        let mut path = if arg.empty {
            // No need to read the path for AT_EMPTY_PATH,
            // to check for access we're going to pidfd_getfd
            // the file descriptor and only then canonicalize
            // the /proc/pid/fd/$fd symbolic link to workaround
            // potential permission errors.
            // (e.g. hidepid=2 in a user ns)
            PathBuf::default()
        } else {
            let (mut path, path_raw) = proc.read_path_opt(request, arg)?;
            trace!("ctx": "sys", "sys": "stat", "arg": arg, "path": format!("{}", path.display()));

            // SAFETY: Carefully handle magic symlinks.
            if !arg.resolve {
                if let Some(path_raw) = path_raw {
                    let bytes = path_raw.as_os_str().as_bytes();
                    if matches!(
                        bytes,
                        b"/proc/self"
                            | b"/proc/thread-self"
                            | b"/dev/fd"
                            | b"/dev/stdin"
                            | b"/dev/stdout"
                            | b"/dev/stderr"
                    ) {
                        // We're not resolving symbolic links,
                        // and the path is an exact match.
                        // Use the raw path as otherwise the
                        // sandbox process will see e.g. /proc/self
                        // as a directory rather than a symlink which
                        // can be most confusing.
                        path = path_raw;
                    }
                }
            }
            path
        };

        let (path, fd, flags) = if let Ok(cmd) = path.strip_prefix(MAGIC_PREFIX) {
            // Handle magic prefix (ie /dev/syd)
            let sandbox = request.get_sandbox(false);
            #[allow(clippy::cast_possible_wrap)]
            if sandbox.locked_for_pid(req.pid as nix::libc::pid_t) {
                // Magic commands locked, return ENOENT.
                return Ok(request.fail_syscall(nix::libc::ENOENT));
            }
            drop(sandbox);

            // Careful here, Path::strip_prefix removes trailing slashes.
            let mut cmd = cmd.to_path_buf();
            if path_ends_with_slash(&path) {
                cmd.push("");
            }
            let path = cmd;

            // Execute magic command.
            let mut sandbox = request.get_sandbox(true);
            if path.is_empty() {
                sandbox.config("")?;
            } else if path.as_os_str().as_bytes() == b"panic" {
                #[allow(clippy::disallowed_methods)]
                exit(127);
            } else if let Ok(path) = path.strip_prefix("load") {
                // We handle load specially here as it involves process access.
                match parse_fd(path) {
                    Ok(remote_fd) => {
                        let fd = proc.get_fd(remote_fd, request)?;
                        let file = BufReader::new(File::from(fd));
                        if sandbox.parse_config(file).is_err() {
                            return Ok(request.fail_syscall(nix::libc::EINVAL));
                        }
                        if let Err(error) = sandbox.build_globsets() {
                            error!("ctx": "config",
                                "pid": req.pid,
                                "cfg": "load",
                                "fd": remote_fd,
                                "error": error.to_string());
                        } else {
                            info!("ctx": "config",
                                "pid": req.pid,
                                "cfg": "load",
                                "fd": remote_fd);
                        }
                        // Fall through to emulate as /dev/null.
                    }
                    Err(errno) => {
                        return Ok(request.fail_syscall(errno as i32));
                    }
                }
            } else {
                // SAFETY: Conversion from PathBuf to String is OK here,
                // since sandbox config function does not work on the
                // filesystem, rather treats the Strings as sandbox
                // commands.
                match sandbox.config(path.to_string_lossy().as_ref()) {
                    Ok(_) => {
                        if let Err(error) = sandbox.build_globsets() {
                            error!("ctx": "config",
                                "pid": req.pid,
                                "cfg": format!("{}", path.display()),
                                "error": error.to_string());
                        } else {
                            info!("ctx": "config",
                                "pid": req.pid,
                                "cfg": format!("{}", path.display()));
                        }
                    }
                    Err(Errno::ENOENT) => {
                        info!("ctx": "config",
                            "pid": req.pid,
                            "cfg": format!("{}", path.display()),
                            "errno": Errno::ENOENT as i32);
                        return Err(Errno::ENOENT);
                    }
                    Err(errno) => {
                        error!("ctx": "config",
                            "pid": req.pid,
                            "cfg": format!("{}", path.display()),
                            "errno": errno as i32);
                        return Err(errno);
                    }
                };
            }
            drop(sandbox);

            // If the stat buffer is NULL, return immediately.
            if req.data.args[arg_stat] == 0 {
                return Ok(request.return_syscall(0));
            }

            // SAFETY: We trust the contents of the MAGIC_FILE.
            let path = unsafe { CString::from_vec_unchecked(MAGIC_FILE.into()) };

            (path, None, 0)
        } else if is_stat {
            // Resolve /proc file descriptor links.
            // Use get_fd if fd belongs to the current process.
            // SAFETY: Magic symlinks are disallowed with ELOOP here, see proc_fd.
            let fd = if arg.empty || arg.path.is_none() {
                Some(req.data.args[0] as RawFd)
            } else {
                #[allow(clippy::cast_possible_wrap)]
                proc_fd(Pid::from_raw(req.pid as i32), &path)?
            };

            let fd = if let Some(fd) = fd {
                let fd = proc.get_fd(fd, request).map_err(|e| match e {
                    Errno::EBADF => Errno::ENOENT,
                    _ => e,
                })?;

                // SAFETY: Access check is made only at this point, to
                // ensure we do have access to the proc fd even with
                // hidepid=2 in a user namespace.
                // SAFETY: We do not resolve symbolic links for AT_EMPTY_PATH, see syd#25.
                let this = Pid::this().as_raw() as u32;
                let path = RemoteProcess::remote_dirfd(this, Some(fd.as_raw_fd()));
                match read_link(path) {
                    Ok(path) if path.is_absolute() => {
                        // SAFETY: If the path is not absolute, it represents
                        // a special proc file name such as a pipe, or a socket.
                        // In this case, we skip the access check.
                        sandbox_path_1(&path, Capability::CAP_STAT, "stat", request, proc)?;
                    }
                    Err(errno) => {
                        error!("ctx": "stat_path",
                            "arg": arg,
                            "dir": arg.dirfd.unwrap_or(-AT_FDCWD as usize),
                            "errno": errno as i32,
                            "pid": req.pid);
                        return Err(if arg.empty { Errno::EBADF } else { errno });
                    }
                    _ => {} // special proc file, fall through.
                };

                fd
            } else {
                // SAFETY: We have already resolved symbolic
                // links as necessary, from this point on we
                // do not resolve any longer for safety as
                // the filesystem may change which may
                // result in a TOCTOU.
                let mut how = openat2::OpenHow::new(nix::libc::O_PATH, 0);
                how.resolve =
                    openat2::ResolveFlags::NO_MAGICLINKS | openat2::ResolveFlags::NO_SYMLINKS;
                if !arg.resolve {
                    how.flags |= nix::libc::O_NOFOLLOW as u64;
                    // SAFETY: Remove trailing slash if it exists,
                    // or else we may get ELOOP on symbolic links.
                    let path_bytes = path.as_os_str().as_bytes();
                    #[allow(clippy::arithmetic_side_effects)]
                    if path_bytes.ends_with(&[b'/']) && path_bytes.len() > 1 {
                        // SAFETY: Since we're operating on valid path bytes, getting a slice is safe.
                        // This excludes the root path "/" to avoid turning it into an empty path.
                        path = PathBuf::from(OsStr::from_bytes(&path_bytes[..path_bytes.len() - 1]))
                    }
                }

                // Check for access only now that path is in its final form.
                sandbox_path_1(&path, Capability::CAP_STAT, "stat", request, proc)?;

                let fd = openat2::openat2(None, path, &how)
                    .map_err(|e| Errno::from_i32(e.raw_os_error().unwrap_or(nix::libc::EINVAL)))?;
                // SAFETY: Valid FD.
                unsafe { OwnedFd::from_raw_fd(fd) }
            };

            let flags = if arg.resolve {
                nix::libc::AT_EMPTY_PATH
            } else {
                nix::libc::AT_EMPTY_PATH | nix::libc::AT_SYMLINK_NOFOLLOW
            };
            (CString::default(), Some(fd), flags)
        } else {
            // Continue system call normally.
            // SAFETY: This is unsafe due to vulnerability to TOCTOU,
            // however if we're here stat sandboxing is disabled,
            // so this is safe to call.
            return unsafe { Ok(request.continue_syscall()) };
        };

        if arg_stat == 4 {
            // statx
            let mut statx = MaybeUninit::<crate::compat::statx>::uninit();
            #[allow(clippy::cast_possible_truncation)]
            let flags = flags
                | (req.data.args[2] as nix::libc::c_int
                    & !(nix::libc::AT_EMPTY_PATH | nix::libc::AT_SYMLINK_NOFOLLOW));
            #[allow(clippy::cast_possible_truncation)]
            let mask = req.data.args[3] as nix::libc::c_int;
            // SAFETY: In libc we trust.
            let ret = if let Some(fd) = fd {
                unsafe {
                    nix::libc::syscall(
                        nix::libc::SYS_statx,
                        fd.as_raw_fd(),
                        path.as_ptr(),
                        flags,
                        mask,
                        statx.as_mut_ptr(),
                    )
                }
            } else {
                unsafe {
                    nix::libc::syscall(
                        nix::libc::SYS_statx,
                        AT_FDCWD,
                        path.as_ptr(),
                        flags,
                        mask,
                        statx.as_mut_ptr(),
                    )
                }
            };
            let errno = Errno::last();
            if ret == 0 {
                trace!("ctx": "stat",
                    "path": &path,
                    "flags": flags);
            } else if matches!(errno, Errno::EPERM | Errno::ENOENT | Errno::ENOTDIR) {
                debug!("ctx": "stat",
                    "path": format!("{}", path.to_string_lossy()),
                    "flags": flags,
                    "errno": errno as i32);
                return Err(errno);
            } else {
                info!("ctx": "stat",
                    "path": format!("{}", path.to_string_lossy()),
                    "flags": flags,
                    "errno": errno as i32);
                return Err(errno);
            }

            // SAFETY: The following block creates an immutable byte slice representing the memory of `statx`.
            // We ensure that the slice covers the entire memory of `statx` using `std::mem::size_of_val`.
            // Since `statx` is a stack variable and we're only borrowing its memory for the duration of the slice,
            // there's no risk of `statx` being deallocated while the slice exists.
            // Additionally, we ensure that the slice is not used outside of its valid lifetime.
            let statx = unsafe {
                std::slice::from_raw_parts(
                    statx.as_ptr() as *const u8,
                    std::mem::size_of_val(&statx),
                )
            };
            #[allow(clippy::cast_possible_truncation)]
            let addr = req.data.args[4] as usize;
            if addr != 0 {
                proc.write_mem(statx, addr, request)?;
            }
        } else {
            // "stat" | "fstat" | "lstat" | "newfstatat"
            let mut stat = MaybeUninit::<nix::libc::stat>::uninit();
            Errno::clear();
            // SAFETY: In libc we trust.
            let ret = if let Some(fd) = fd {
                unsafe {
                    nix::libc::fstatat(fd.as_raw_fd(), path.as_ptr(), stat.as_mut_ptr(), flags)
                }
            } else {
                unsafe { nix::libc::fstatat(AT_FDCWD, path.as_ptr(), stat.as_mut_ptr(), flags) }
            };
            let errno = Errno::last();
            if ret == 0 {
                trace!("ctx": "stat",
                    "path": format!("{}", path.to_string_lossy()),
                    "flags": flags);
            } else if matches!(errno, Errno::EPERM | Errno::ENOENT | Errno::ENOTDIR) {
                debug!("ctx": "stat",
                    "path": format!("{}", path.to_string_lossy()),
                    "flags": flags,
                    "errno": errno as i32);
                return Err(errno);
            } else {
                info!("ctx": "stat",
                    "path": format!("{}", path.to_string_lossy()),
                    "flags": flags,
                    "errno": errno as i32);
                return Err(errno);
            }

            // SAFETY: stat returned success, stat struct is properly populated.
            unsafe { stat.assume_init() };
            // SAFETY: The following block creates an immutable byte slice representing the memory of `stat`.
            // We ensure that the slice covers the entire memory of `stat` using `std::mem::size_of_val`.
            // Since `stat` is a stack variable and we're only borrowing its memory for the duration of the slice,
            // there's no risk of `stat` being deallocated while the slice exists.
            // Additionally, we ensure that the slice is not used outside of its valid lifetime.
            let stat = unsafe {
                std::slice::from_raw_parts(
                    std::ptr::addr_of!(stat) as *const u8,
                    std::mem::size_of_val(&stat),
                )
            };
            #[allow(clippy::cast_possible_truncation)]
            let addr = req.data.args[arg_stat] as usize;
            if addr != 0 {
                proc.write_mem(stat, addr, request)?;
            }
        }

        // stat system call successfully emulated.
        Ok(request.return_syscall(0))
    })
}

/// A helper function to handle network-related syscalls.
///
/// This function abstracts the common logic involved in handling network syscalls such as `bind`,
/// `connect`, `and `sendto` in a seccomp-based sandboxing environment. It reduces code duplication
/// across different syscall handler functions.
///
/// # Returns
///
/// Returns `ScmpNotifResp` indicating the result of the syscall handling:
/// - If successful, it contains a continued syscall.
/// - If an error occurs, it contains a failed syscall with an `EACCES` error code.
#[allow(clippy::cognitive_complexity)]
fn syscall_network_handler(
    request: &UNotifyEventRequest,
    args: &[u64; 6],
    op: u8,
) -> ScmpNotifResp {
    syscall_handler!(request, |_req: &ScmpNotifReq, proc: &RemoteProcess| {
        let cap = match op {
            0x2 => Capability::CAP_BIND,
            _ => Capability::CAP_CONNECT,
        };

        let sandbox = request.get_sandbox(false);
        let (check, allow_safe_bind, allow_unsupp_socket) = (
            sandbox.enabled(cap),
            sandbox.allow_safe_bind(),
            sandbox.allow_unsupp_socket(),
        );
        drop(sandbox);

        // Return immediately if sandboxing is not enabled for current capability,
        if !check {
            // SAFETY: This is unsafe due to vulnerability to TOCTOU,
            // however since the sandboxing for the requested capability
            // is disabled this is safe here.
            return unsafe { Ok(request.continue_syscall()) };
        }

        let idx = if op == 0xb /* sendto */ { 4 } else { 1 };
        #[allow(clippy::cast_possible_truncation)]
        let addr_remote = args[idx] as usize;
        #[allow(clippy::arithmetic_side_effects)]
        #[allow(clippy::cast_possible_truncation)]
        let addr_len = args[idx + 1] as nix::libc::socklen_t;
        if addr_remote == 0 || addr_len == 0 {
            if op == 0xb {
                // SAFETY: Connection mode socket, safe to continue.
                return Ok(unsafe { request.continue_syscall() });
            } else {
                return Err(Errno::EFAULT);
            }
        }
        let addr = match canon_addr(
            request,
            get_addr(proc, request, addr_remote, addr_len)?,
            cap,
        )? {
            Some(addr) => addr,
            None => return Err(Errno::EAFNOSUPPORT),
        };
        match addr.family() {
            Some(AddressFamily::Unix | AddressFamily::Inet | AddressFamily::Inet6) => {}
            _ if allow_unsupp_socket => return unsafe { Ok(request.continue_syscall()) },
            _ => return Err(Errno::EAFNOSUPPORT),
        };

        // Check for access.
        sandbox_addr(proc, request, &addr, op, cap)?;

        // Emulate syscall.
        let fd = proc.get_fd(args[0] as RawFd, request)?;
        match op {
            0x2 => handle_bind(&fd, &addr, allow_safe_bind, request),
            0x3 => handle_connect(&fd, &addr, request),
            0xb => handle_sendto(&fd, args, proc, request, &addr),
            _ => unreachable!(),
        }
    })
}

#[allow(clippy::cognitive_complexity)]
fn handle_bind(
    fd: &OwnedFd,
    addr: &SockaddrStorage,
    allow_safe_bind: bool,
    request: &UNotifyEventRequest,
) -> Result<ScmpNotifResp, Errno> {
    let unix_sock = addr.as_unix_addr().and_then(|a| a.path());

    let result = if unix_sock.is_some() {
        let req = request.get_request();
        #[allow(clippy::cast_possible_wrap)]
        let mask = match proc_umask(Pid::from_raw(req.pid as i32)) {
            Ok(mask) => mask.bits(),
            Err(_) => {
                return Err(Errno::EACCES);
            }
        };
        let fd = fd.as_raw_fd();
        let addr = addr.as_ptr();
        #[allow(clippy::cast_possible_truncation)]
        const ADDR_LEN: nix::libc::socklen_t =
            std::mem::size_of::<nix::libc::sockaddr_un>() as nix::libc::socklen_t;
        let flags = CloneFlags::CLONE_VM | CloneFlags::CLONE_VFORK | CloneFlags::CLONE_SIGHAND;
        let mut nstack = [0u8; 32 * 1024];
        #[allow(clippy::blocks_in_conditions)]
        let tid = match clone(
            Box::new(|| -> isize {
                // SAFETY: Honour process' umask.
                // Note, the umask is per-thread here.
                let _ = unsafe { nix::libc::umask(mask) };
                if unsafe { nix::libc::syscall(nix::libc::SYS_bind, fd, addr, ADDR_LEN) } == 0 {
                    0
                } else {
                    Errno::last() as isize
                }
            }),
            &mut nstack[..],
            flags,
            Some(SIGCHLD as i32),
        ) {
            Ok(tid) => tid,
            Err(_) => {
                return Err(Errno::EACCES);
            }
        };

        match waitpid(tid, None) {
            Ok(WaitStatus::Exited(_, 0)) => Ok(()),
            Ok(WaitStatus::Exited(_, n)) => Err(Errno::from_i32(n)),
            _ => Err(Errno::EACCES),
        }
    } else {
        bind(fd.as_raw_fd(), addr)
    };
    if let Err(error) = result {
        return Err(error);
    } else if !allow_safe_bind {
        return Ok(request.return_syscall(0));
    }

    // Handle allow_safe_bind
    let cmd = match addr.family() {
        Some(AddressFamily::Unix) => {
            let addr = addr.as_unix_addr().ok_or(Errno::EINVAL)?;
            match (addr.path(), addr.as_abstract()) {
                (Some(path), _) => {
                    let path = path.as_os_str().as_bytes();
                    let null = path.iter().position(|&b| b == 0).unwrap_or(path.len());
                    let path = PathBuf::from(OsStr::from_bytes(&path[..null]));
                    Some(format!("allow/net/connect+{}", path.display()))
                }
                (_, Some(path)) => {
                    let null = path.iter().position(|&b| b == 0).unwrap_or(path.len());
                    let path = PathBuf::from(OsStr::from_bytes(&path[..null]));
                    Some(format!("allow/net/connect+{}", path.display()))
                }
                _ => {
                    // Unnamed UNIX socket
                    None
                }
            }
        }
        Some(AddressFamily::Inet) => {
            let addr = addr.as_sockaddr_in().ok_or(Errno::EINVAL)?;
            let mut port = addr.port();
            let addr = IpAddr::V4(Ipv4Addr::from(addr.ip()));
            if port == 0 {
                port = getsockname::<SockaddrStorage>(fd.as_raw_fd())?
                    .as_sockaddr_in()
                    .ok_or(Errno::EINVAL)?
                    .port();
            }
            Some(format!("allow/net/connect+{addr}!{port}"))
        }
        Some(AddressFamily::Inet6) => {
            let addr = addr.as_sockaddr_in6().ok_or(Errno::EINVAL)?;
            let mut port = addr.port();
            let addr = IpAddr::V6(addr.ip());
            if port == 0 {
                port = getsockname::<SockaddrStorage>(fd.as_raw_fd())?
                    .as_sockaddr_in6()
                    .ok_or(Errno::EINVAL)?
                    .port();
            }
            Some(format!("allow/net/connect+{addr}!{port}"))
        }
        _ => None,
    };
    if let Some(cmd) = cmd {
        let mut sandbox = request.get_sandbox(true);
        let result = sandbox.config(&cmd);
        let pid = request.get_request().pid;
        match result {
            Ok(_) => {
                if let Err(error) = sandbox.build_globsets() {
                    error!("ctx": "config",
                        "pid": pid,
                        "sys": "bind",
                        "cfg": cmd,
                        "error": error.to_string());
                } else {
                    info!("ctx": "config",
                        "pid": pid,
                        "sys": "bind",
                        "cfg": cmd);
                }
            }
            Err(errno) => {
                error!("ctx": "config",
                    "pid": pid,
                    "sys": "bind",
                    "cfg": cmd,
                    "errno": errno as i32);
            }
        };
        drop(sandbox);
    }
    Ok(request.return_syscall(0))
}

fn handle_connect(
    fd: &OwnedFd,
    addr: &SockaddrStorage,
    request: &UNotifyEventRequest,
) -> Result<ScmpNotifResp, Errno> {
    connect(fd.as_raw_fd(), addr).map(|_| request.return_syscall(0))
}

fn handle_sendto(
    fd: &OwnedFd,
    args: &[u64; 6],
    proc: &RemoteProcess,
    request: &UNotifyEventRequest,
    addr: &SockaddrStorage,
) -> Result<ScmpNotifResp, Errno> {
    // SAFETY: The length argument to the sendto call
    // must not be fully trusted, it can be overly large,
    // and allocating a Vector of that capacity may overflow.
    #[allow(clippy::cast_possible_truncation)]
    let len = args[2] as usize;
    let len = len.min(1000000); // Cap count at 1mio
    #[allow(clippy::cast_possible_truncation)]
    let flags = MsgFlags::from_bits_truncate(args[3] as nix::libc::c_int);
    let mut buf = vec![0u8; len];
    #[allow(clippy::cast_possible_truncation)]
    proc.read_mem(&mut buf, args[1] as usize, request)?;
    let n = sendto(fd.as_raw_fd(), &buf, addr, flags)?;
    #[allow(clippy::cast_possible_wrap)]
    Ok(request.return_syscall(n as i64))
}

fn get_addr(
    proc: &RemoteProcess,
    request: &UNotifyEventRequest,
    addr_remote: usize,
    addr_len: nix::libc::socklen_t,
) -> Result<SockaddrStorage, Errno> {
    // SAFETY: Do not fully trust addr_len.
    #[allow(clippy::arithmetic_side_effects)]
    #[allow(clippy::cast_possible_truncation)]
    let addr_len = addr_len.min(
        (std::mem::size_of::<nix::libc::sockaddr_un>() + UNIX_PATH_MAX) as nix::libc::socklen_t,
    );
    let mut addr = vec![0u8; addr_len as usize];
    proc.read_mem(&mut addr, addr_remote, request)?;

    // SAFETY: Invoking `SockaddrStorage::from_raw` is safe because:
    // 1. The memory location of `sockaddr_ptr` is valid, correctly aligned.
    // 2. The memory is allocated based on a valid `sockaddr` structure.
    // 3. There are no concurrent writes to the memory location while reading.
    match unsafe {
        #[allow(clippy::cast_ptr_alignment)]
        SockaddrStorage::from_raw(addr.as_ptr() as *const nix::libc::sockaddr, Some(addr_len))
    } {
        Some(addr) => Ok(addr),
        None => {
            // Invalid socket address.
            Err(Errno::EINVAL)
        }
    }
}

fn canon_addr(
    request: &UNotifyEventRequest,
    addr: SockaddrStorage,
    cap: Capability,
) -> Result<Option<SockaddrStorage>, Errno> {
    #[allow(clippy::cast_possible_truncation)]
    if let Some(path) = addr.as_unix_addr().and_then(|a| a.path()) {
        // SAFETY: Path may have trailing nul-bytes.
        // Truncate the path at the first occurrence of a null byte
        // Note this is _not_ an abstract UNIX socket so it's safe.
        let byte = path.as_os_str().as_bytes();
        let trim = byte.split(|&b| b == 0).next().unwrap_or(&[]);
        let path = PathBuf::from(OsStr::from_bytes(trim));

        // SAFETY: Make sure relative UNIX socket paths match process CWD.
        #[allow(clippy::cast_possible_wrap)]
        let pid = Pid::from_raw(request.get_request().pid as i32);
        let path = if path.is_relative() {
            let mut b = itoa::Buffer::new();
            let mut p = PathBuf::from("/proc");
            p.push(b.format(pid.as_raw()));
            p.push("cwd");
            p.push(path);
            p
        } else {
            path
        };
        // If bind, the path may or may not exist depending on SO_REUSEADDR
        // Else, the path must exist. Always resolve symlinks.
        let miss = if cap == Capability::CAP_BIND {
            Normal
        } else {
            Existing
        };
        let path = canonicalize(pid, path, true, miss)?;

        // Convert the path to a CString
        let osstr: &OsStr = path.as_ref();
        let bytes = osstr.as_bytes();
        let null = bytes.iter().position(|&b| b == 0).unwrap_or(bytes.len());
        let cstr = CString::new(&bytes[..null]).map_err(|_| Errno::EINVAL)?;

        // Create sockaddr_un struct.
        let mut sockaddr = nix::libc::sockaddr_un {
            sun_family: nix::libc::AF_UNIX as nix::libc::sa_family_t,
            sun_path: [0; UNIX_PATH_MAX],
        };

        let bytes_with_nul = cstr.as_bytes_with_nul();
        if bytes_with_nul.len() > UNIX_PATH_MAX {
            return Err(Errno::ENAMETOOLONG);
        }

        // Manually copy the bytes.
        // TODO: Is there a better way?
        #[allow(clippy::cast_possible_wrap)]
        for (dst, &src) in sockaddr.sun_path.iter_mut().zip(bytes_with_nul.iter()) {
            *dst = src as nix::libc::c_char;
        }

        // Calculate the correct size of the sockaddr_un struct, including the family and the path.
        // The size is the offset of the sun_path field plus the length of the path (including the null terminator).
        #[allow(clippy::arithmetic_side_effects)]
        let size = std::mem::size_of::<nix::libc::sa_family_t>() + bytes_with_nul.len();

        // SAFETY: We are converting a sockaddr_un to a
        // SockaddrStorage using a raw pointer. The sockaddr_un
        // is valid for the duration of this operation, ensuring
        // the safety of the pointer. However, this operation is
        // inherently unsafe due to direct pointer manipulation.
        Ok(unsafe {
            SockaddrStorage::from_raw(
                std::ptr::addr_of!(sockaddr) as *const _,
                Some(size as nix::libc::socklen_t),
            )
        })
    } else {
        // No need to canonicalize.
        Ok(Some(addr))
    }
}

/*
#[cfg(test)]
mod tests {
    use std::{ffi::CStr, time::Duration};

    use nix::sys::signal::{kill, Signal::SIGKILL};

    use super::*;

    #[test]
    fn smoke_test_sleep() {
        fn openat_handler(req: &UNotifyEventRequest) -> libseccomp::ScmpNotifResp {
            let path = req.get_request().data.args[1];
            let remote = RemoteProcess::new(Pid::from_raw(req.request.pid as i32)).unwrap();
            let mut buf = [0u8; 256];
            remote.read_mem(&mut buf, path as usize).unwrap();
            eprintln!("open (read from remote): {:?}", buf);
            let path = CStr::from_bytes_until_nul(&buf).unwrap();
            if !req.is_valid() {
                return req.fail_syscall(libc::EACCES);
            }
            eprintln!("open (path CStr): {:?}", path);
            unsafe { req.continue_syscall() }
        }

        let mut supervisor = Supervisor::new(2).unwrap();
        supervisor.insert_handler(ScmpSyscall::new("openat"), openat_handler);
        let mut cmd = Command::new("/bin/sleep");
        let cmd = cmd.arg("1");
        let (pid, thread_handle, pool) = supervisor.spawn(cmd).unwrap();
        let status = Supervisor::wait(pid, thread_handle, pool).unwrap();
        assert!(status.success());
    }

    #[test]
    fn smoke_test_whoami() {
        fn geteuid_handler(req: &UNotifyEventRequest) -> libseccomp::ScmpNotifResp {
            req.return_syscall(0)
        }

        let mut supervisor = Supervisor::new(2).unwrap();
        supervisor.insert_handler(ScmpSyscall::new("geteuid"), geteuid_handler);
        let mut cmd = Command::new("/usr/bin/whoami");
        let cmd = cmd.stdout(Stdio::piped());
        let (pid, thread_handle, pool) = supervisor.spawn(cmd).unwrap();
        let status = Supervisor::wait(pid, thread_handle, pool).unwrap();
        assert!(status.success());
        let whoami_stdout = child.stdout.as_mut().unwrap();
        let mut buf = String::new();
        whoami_stdout.read_to_string(&mut buf).unwrap();
        assert_eq!(buf.trim(), "root");
    }

    #[test]
    fn test_sleep_blocking_syscall() {
        fn clock_nanosleep_handler(req: &UNotifyEventRequest) -> libseccomp::ScmpNotifResp {
            // sleep for extra 60s
            // Please note that it may bring A LOT OF PROBLEMS if you try using pthread_cancel
            // So here we just use the easy way: check valid in the loop
            let (tx, rx) = std::sync::mpsc::channel();
            let handler = std::thread::spawn(move || {
                for _ in 0..60 {
                    if rx.try_recv().is_ok() {
                        break;
                    }
                    std::thread::sleep(Duration::from_secs(1));
                }
            });
            // while handler is running, check valid in the loop
            loop {
                if !req.is_valid() {
                    // cancel the thread
                    eprintln!("canceling thread as req is invalid now");
                    tx.send(()).unwrap();
                    break;
                }
                std::thread::sleep(Duration::from_millis(100));
            }
            handler.join().unwrap();
            unsafe { req.continue_syscall() }
        }

        let mut supervisor = Supervisor::new(2).unwrap();
        supervisor.insert_handler(ScmpSyscall::new("clock_nanosleep"), clock_nanosleep_handler);
        let mut cmd = Command::new("/bin/sleep");
        let cmd = cmd.arg("120");
        let (pid, thread_handle, pool) = supervisor.spawn(cmd).unwrap();
        std::thread::spawn(move || {
            std::thread::sleep(Duration::from_secs(1));
            // kill the child process
            kill(pid, SIGKILL).unwrap();
        });
        let _ = Supervisor::wait(pid, thread_handle, pool).unwrap();
    }

    #[test]
    fn test_new_fd() {
        fn openat_handler(req: &UNotifyEventRequest) -> libseccomp::ScmpNotifResp {
            let path = req.get_request().data.args[1];
            let remote = RemoteProcess::new(Pid::from_raw(req.request.pid as i32)).unwrap();
            let mut buf = [0u8; 256];
            remote.read_mem(&mut buf, path as usize).unwrap();
            eprintln!("open (read from remote): {:?}", buf);
            let path = CStr::from_bytes_until_nul(&buf).unwrap();
            if !req.is_valid() {
                return req.fail_syscall(libc::EACCES);
            }
            eprintln!("open (path CStr): {:?}", path);
            if path.to_str().unwrap() == "/etc/passwd" {
                // open /etc/resolv.conf instead
                let file = File::open("/etc/resolv.conf").unwrap();
                let fd = file.as_raw_fd();
                let remote_fd = req.add_fd(fd).unwrap();
                req.return_syscall(remote_fd as i64)
            } else {
                unsafe { req.continue_syscall() }
            }
        }

        let mut supervisor = Supervisor::new(2).unwrap();
        supervisor.insert_handler(ScmpSyscall::new("openat"), openat_handler);
        let mut cmd = Command::new("/bin/cat");
        let cmd = cmd.arg("/etc/passwd").stdout(Stdio::piped());
        let (pid, thread_handle, pool) = supervisor.spawn(cmd).unwrap();
        let status = Supervisor::wait(pid, thread_handle, pool).unwrap();
        assert!(status.success());
        let cat_stdout = child.stdout.as_mut().unwrap();
        let mut buf = String::new();
        cat_stdout.read_to_string(&mut buf).unwrap();
        assert!(buf.contains("nameserver"));
    }
}
*/
