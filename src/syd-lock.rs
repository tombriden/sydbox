//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/syd-lock.rs: Exit with success if LandLock ABI v3 is fully supported
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::process::ExitCode;

fn main() -> ExitCode {
    ExitCode::from(syd::lock_enabled())
}
