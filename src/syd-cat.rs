//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/syd-cat.rs: Print out the rules of the given sandbox profile and exit
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{fs::File, io::BufReader, process::ExitCode};

use syd::sandbox::Sandbox;

fn main() -> ExitCode {
    let mut args = std::env::args();

    match args.nth(1).as_deref() {
        None | Some("-h") => {
            println!("Usage: syd-cat profile-name|path");
            println!("Print out the rules of the given sandbox profile and exit.");
            println!("Use list as name to get a list of sandboxing profiles.");
            println!("Given a path, validate the rules in the configuration.");
        }
        Some("list") => {
            println!("container");
            println!("immutable");
            println!("landlock");
            println!("paludis");
            println!("pandora");
            println!("noipv4");
            println!("noipv6");
            println!("silent");
            println!("lib");
            println!("user");
        }
        Some("container") => {
            println!("# syd profile: Container");
            println!(
                "# Number of rules: {}",
                syd::config::PROFILE_CONTAINER.len()
            );
            println!("# Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>");
            println!("# SPDX-License-Identifier: GPL-3.0-or-later");
            for command in syd::config::PROFILE_CONTAINER {
                println!("{command}");
            }
        }
        Some("immutable") => {
            println!("# syd profile: Immutable Container");
            println!(
                "# Number of rules: {}",
                syd::config::PROFILE_IMMUTABLE.len()
            );
            println!("# Copyright (c) 2024 Ali Polatel <alip@chesswob.org>");
            println!("# SPDX-License-Identifier: GPL-3.0-or-later");
            for command in syd::config::PROFILE_IMMUTABLE {
                println!("{command}");
            }
        }
        Some("landlock") => {
            println!("# syd profile: LandLock");
            println!("# Number of rules: {}", syd::config::PROFILE_LANDLOCK.len());
            println!("# Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>");
            println!("# SPDX-License-Identifier: GPL-3.0-or-later");
            for command in syd::config::PROFILE_LANDLOCK {
                println!("{command}");
            }
        }
        Some("paludis") => {
            println!("# syd profile: Paludis");
            println!("# Number of rules: {}", syd::config::PROFILE_PALUDIS.len());
            println!("# Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>");
            println!("# SPDX-License-Identifier: GPL-3.0-or-later");
            for command in syd::config::PROFILE_PALUDIS {
                println!("{command}");
            }
        }
        Some("pandora") => {
            println!("# syd profile: Pand☮ra");
            println!("# Number of rules: {}", syd::config::PROFILE_PANDORA.len());
            println!("# Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>");
            println!("# SPDX-License-Identifier: GPL-3.0-or-later");
            for command in syd::config::PROFILE_PANDORA {
                println!("{command}");
            }
        }
        Some("noipv4") => {
            println!("# syd profile: NoIpv4");
            println!("# Number of rules: {}", syd::config::PROFILE_NOIPV4.len());
            println!("# Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>");
            println!("# SPDX-License-Identifier: GPL-3.0-or-later");
            for command in syd::config::PROFILE_NOIPV4 {
                println!("{command}");
            }
        }
        Some("noipv6") => {
            println!("# syd profile: NoIpv6");
            println!("# Number of rules: {}", syd::config::PROFILE_NOIPV6.len());
            println!("# Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>");
            println!("# SPDX-License-Identifier: GPL-3.0-or-later");
            for command in syd::config::PROFILE_NOIPV6 {
                println!("{command}");
            }
        }
        Some("silent") => {
            println!("# syd profile: Silent");
            println!("# Number of rules: {}", syd::config::PROFILE_SILENT.len());
            println!("# Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>");
            println!("# SPDX-License-Identifier: GPL-3.0-or-later");
            for command in syd::config::PROFILE_SILENT {
                println!("{command}");
            }
        }
        Some("lib") => {
            println!("# syd profile: LibSyd");
            println!("# Number of rules: {}", syd::config::PROFILE_SILENT.len());
            println!("# Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>");
            println!("# SPDX-License-Identifier: GPL-3.0-or-later");
            for command in syd::config::PROFILE_LIB {
                println!("{command}");
            }
        }
        Some("user") => {
            let uid = nix::unistd::getuid();
            let name = syd::get_user_name(uid);
            let mut home = syd::get_user_home(&name);

            println!("# syd profile: User \"{name}\"");
            println!(
                "# Number of rules: {}",
                syd::config::PROFILE_USER.len().saturating_add(9)
            );
            println!("# Copyright (c) 2023 Ali Polatel <alip@chesswob.org>");
            println!("# SPDX-License-Identifier: GPL-3.0-or-later");

            // Step 1: Static configuration defined at compile time.
            for command in syd::config::PROFILE_USER {
                println!("{command}");
            }

            // Step 2: Dynamic, user-specific configuration.
            // Keep in sync with Sandbox::parse_profile()
            if !home.ends_with('/') {
                home.push('/');
            }

            println!("allow/lock/write+{home}");
            println!("allow/read+{home}***");
            println!("allow/stat+{home}***");
            println!("allow/write+{home}**");
            println!("allow/exec+{home}**");
            println!("allow/net/bind+{home}**");
            println!("allow/net/connect+{home}**");
            println!("allow/read+/run/user/{uid}/**");
            println!("allow/write+/run/user/{uid}/**");
        }
        Some(path) => {
            let file = match File::open(path) {
                Ok(file) => BufReader::new(file),
                Err(error) => {
                    eprintln!("Error opening {path}: {error}!");
                    return ExitCode::FAILURE;
                }
            };

            let mut syd = Sandbox::new();
            if let Err(error) = syd.parse_config(file) {
                eprintln!("Error parsing {path}: {error}!");
                return ExitCode::FAILURE;
            } else {
                eprintln!("Success parsing {path}.");
            }
        }
    }

    ExitCode::SUCCESS
}
