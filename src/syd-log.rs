//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/syd-check.rs: View syd logs using journalctl.
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    os::unix::process::CommandExt,
    process::{Command, ExitCode},
};

fn main() -> ExitCode {
    Command::new("journalctl")
        .arg("SYSLOG_IDENTIFIER=syd")
        .exec();
    ExitCode::FAILURE
}
