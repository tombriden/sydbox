//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/syd-read.rs: Print the canonicalized path name followed by a newline and exit.
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{io::Write, os::unix::ffi::OsStrExt, path::Path, process::ExitCode};

use nix::unistd::Pid;

fn main() -> ExitCode {
    let mut args = std::env::args();

    match args.nth(1).as_deref() {
        None | Some("-h") => {
            println!("Usage: syd-read path");
            println!("Print the canonicalized path name followed by a newline and exit.");
        }
        Some(value) => {
            let path = Path::new(value);
            let path = if path.is_relative() {
                Path::new("/proc/self/cwd").join(path)
            } else {
                path.to_path_buf()
            };
            let path = match syd::fs::canonicalize(
                Pid::this(),
                path,
                true,
                syd::fs::MissingHandling::Normal,
            ) {
                Ok(path) => path,
                Err(error) => {
                    eprintln!("Error canonicalizing path: {error}!");
                    return ExitCode::FAILURE;
                }
            };
            let path = path.as_os_str().as_bytes();
            // SAFETY: We panic if writing to standard output fails.
            #[allow(clippy::disallowed_methods)]
            std::io::stdout().write_all(path).expect("write to stdout");
            println!();
        }
    }

    ExitCode::SUCCESS
}
