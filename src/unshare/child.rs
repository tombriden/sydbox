#![allow(clippy::undocumented_unsafe_blocks)]

use std::{mem, ptr};

use nix::{
    self,
    libc::{self, signal, sigset_t, SIG_DFL, SIG_SETMASK},
    unistd::{close, read, write},
};

use crate::{
    caps,
    unshare::{error::ErrorCode as Err, run::ChildInfo},
};

unsafe fn fail(code: Err) -> ! {
    fail_errno(code, nix::errno::errno())
}
unsafe fn fail_errno(code: Err, errno: i32) -> ! {
    let msg = match code {
        Err::CapSet => b"syd: capset error\0".as_ptr(),
        Err::Exec => b"syd: exec error\0".as_ptr(),
        Err::ParentDeathSignal => b"syd: parent-death-signal error\0".as_ptr(),
        Err::PreExec => b"syd: pre-exec error\0".as_ptr(),
        Err::Seccomp => b"syd: seccomp error\0".as_ptr(),
        Err::SeccompNotify => b"syd: seccomp-notify error\0".as_ptr(),
        Err::SetTSC => b"syd: set-tsc error\0".as_ptr(),
    };
    errno::set_errno(errno::Errno(errno));
    libc::perror(msg as *const libc::c_char);
    libc::_exit(errno);
}

macro_rules! fail_safe {
    ($child:expr, $error:expr) => {
        drop($child);
        unsafe { fail($error) };
    };
}

macro_rules! fail_errno_safe {
    ($child:expr, $error:expr, $errno:expr) => {
        drop($child);
        unsafe { fail_errno($error, $errno) };
    };
}

#[allow(clippy::cognitive_complexity)]
pub fn child_after_clone(child: ChildInfo) -> ! {
    if caps::securebits::set_keepcaps(true).is_err() {
        fail_safe!(child, Err::CapSet);
    }

    if let Some(&sig) = child.cfg.death_sig.as_ref() {
        if unsafe { libc::prctl(libc::PR_SET_PDEATHSIG, sig as libc::c_ulong, 0, 0, 0) } != 0 {
            fail_safe!(child, Err::ParentDeathSignal);
        }
    }

    if child.cfg.restore_sigmask {
        unsafe {
            let mut sigmask: sigset_t = mem::zeroed();
            libc::sigemptyset(&mut sigmask);
            libc::pthread_sigmask(SIG_SETMASK, &sigmask, ptr::null_mut());
            for sig in 1..32 {
                signal(sig, SIG_DFL);
            }
        }
    }

    if let Some(callback) = &child.pre_exec {
        if let Err(e) = callback() {
            fail_errno_safe!(child, Err::PreExec, e as i32);
        }
    }

    #[cfg(target_arch = "x86_64")]
    if child.cfg.deny_tsc && unsafe { libc::prctl(libc::PR_SET_TSC, libc::PR_TSC_SIGSEGV) } != 0 {
        fail_safe!(child, Err::SetTSC);
    }

    if let Some(seccomp_filter) = &child.seccomp_filter {
        // Close the write end of the first pipe
        // Close the read end of the second pipe
        let _ = close(child.seccomp_pipefd.0 .1);
        let _ = close(child.seccomp_pipefd.1 .0);
        let pipe_ro = child.seccomp_pipefd.0 .0;
        let pipe_rw = child.seccomp_pipefd.1 .1;

        // Load the seccomp filter.
        if seccomp_filter.load().is_err() {
            fail_safe!(child, Err::Seccomp);
        }

        // Get seccomp notification fd.
        let fd = match seccomp_filter.get_notify_fd() {
            Ok(fd) => fd,
            Err(_) => {
                fail_safe!(child, Err::Seccomp);
            }
        };

        // Write the value of the fd to the pipe.
        let fd_bytes = fd.to_le_bytes();
        if let Err(errno) = write(pipe_rw, &fd_bytes) {
            let _ = close(fd);
            fail_errno_safe!(child, Err::SeccompNotify, errno as i32);
        }
        let _ = close(pipe_rw); // no longer necessary.

        // Wait for the parent to get the file descriptor.
        // Read success notification from the pipe.
        let mut ack = [0u8];
        let result = read(pipe_ro, &mut ack);

        // Close the file descriptors and go on.
        let _ = close(pipe_ro);
        let _ = close(fd);
        if let Err(errno) = result {
            fail_errno_safe!(child, Err::SeccompNotify, errno as i32);
        }
    }

    unsafe { libc::execvp(child.filename, child.args.as_ptr()) };
    fail_safe!(child, Err::Exec);
}
