use std::fmt::{self, Display};

use crate::unshare::Command;

/// This is a builder for various settings of how command may be printed
///
/// Use `format!("{}", cmd.display(style))` to actually print a command.
#[derive(Clone, Debug)]
pub struct Style {
    cmd_only: bool,
    print_env: bool,
    show_path: bool,
}

/// A temporary value returned from `Command::display` for the sole purpose
/// of being `Display`'ed.
pub struct Printer<'a>(&'a Command, &'a Style);

impl Style {
    /// Create a new style object that matches to how `fmt::Debug` works for
    /// the command
    pub fn debug() -> Style {
        Style {
            cmd_only: false,
            print_env: true,
            show_path: true,
        }
    }
    /// Create a simple clean user-friendly display of the command
    ///
    /// Note: this kind of pretty-printing omit many important parts of command
    /// and may be ambiguous.
    pub fn short() -> Style {
        Style {
            cmd_only: true,
            print_env: false,
            show_path: false,
        }
    }
    /// Toggle printing of environment
    ///
    /// When `false` is passed we only show `environ[12]`, i.e. a number of
    /// environment variables. Default is `true` for `Style::debug`
    /// constructor.
    ///
    /// This method does nothing when using `Style::short` construtor
    pub fn env(mut self, enable: bool) -> Style {
        self.print_env = enable;
        self
    }
    /// Toggle printing of full path to the executable
    ///
    /// By default we don't print full executable path in `Style::short` mode.
    ///
    /// Note: if this flag is disabled (default) we only show a name from
    /// `arg0`, instead of executable path. When flag is
    /// enabled, the `arg0` is shown alongside with executable path in
    /// parethesis if the values differ.
    ///
    /// This method does nothing when using `Style::debug` constructor
    pub fn path(mut self, enable: bool) -> Style {
        self.show_path = enable;
        self
    }
}

impl<'a> fmt::Display for Printer<'a> {
    #[allow(clippy::cognitive_complexity)]
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let Printer(cmd, opt) = *self;

        if opt.cmd_only {
            if opt.show_path {
                write!(fmt, "{:?}", cmd.filename)?;
                if cmd.args[0] != cmd.filename {
                    write!(fmt, " ({:?})", &cmd.args[0])?;
                }
            } else {
                let path = if cmd.args[0] != cmd.filename {
                    &cmd.args[0]
                } else {
                    &cmd.filename
                };
                let last_slash = path.as_bytes().iter().rposition(|&x| x == b'/');
                if let Some(off) = last_slash {
                    write!(
                        fmt,
                        "{:?}",
                        &String::from_utf8_lossy(&path.as_bytes()[off.saturating_add(1)..])
                    )?;
                } else {
                    write!(fmt, "{:?}", path)?;
                }
            }
            for arg in &cmd.args[1..] {
                write!(fmt, " {:?}", arg)?;
            }
        } else {
            write!(fmt, "<Command {:?}", cmd.filename)?;
            if cmd.args[0] != cmd.filename {
                write!(fmt, " ({:?})", &cmd.args[0])?;
            }
            for arg in &cmd.args[1..] {
                write!(fmt, " {:?}", arg)?;
            }
            write!(fmt, ">")?
        }
        Ok(())
    }
}

impl Command {
    /// Returns the object that implements Display
    pub fn display<'a>(&'a self, style: &'a Style) -> Printer<'a> {
        Printer(self, style)
    }
}

impl fmt::Debug for Command {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        Printer(self, &Style::debug()).fmt(fmt)
    }
}

#[cfg(test)]
mod test {
    use crate::unshare::{Command, Style};

    #[test]
    fn test_pretty() {
        let mut cmd = Command::new("/bin/hello").unwrap();
        cmd.arg("world!");
        assert_eq!(
            &format!("{}", cmd.display(&Style::short())),
            r#""hello" "world!""#
        );
    }
}
