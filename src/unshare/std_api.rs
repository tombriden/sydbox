// This file was derived from rust's own libstd/process.rs with the following
// copyright:
//
// Copyright 2015 The Rust Project Developers. See the COPYRIGHT
// file at the top-level directory of this distribution and at
// http://rust-lang.org/COPYRIGHT.
//
use std::{default::Default, ffi::OsStr};

use nix::{errno::Errno, fcntl::OFlag, unistd::pipe2};

use crate::unshare::{config::Config, ffi_util::ToCString, Command};

impl Command {
    /// Constructs a new `Command` for launching the program at
    /// path `program`, with the following default configuration:
    ///
    /// * No arguments to the program
    /// * Inherit the current process's environment
    /// * Inherit the current process's working directory
    /// * Inherit stdin/stdout/stderr for `spawn` or `status`, but create pipes for `output`
    ///
    /// Builder methods are provided to change these defaults and
    /// otherwise configure the process.
    pub fn new<S: AsRef<OsStr>>(program: S) -> Result<Command, Errno> {
        Ok(Command {
            filename: program.to_cstring(),
            args: vec![program.to_cstring()],
            config: Config::default(),
            before_unfreeze: None,
            pre_exec: None,
            seccomp_filter: None,
            seccomp_pipefd: (
                pipe2(OFlag::O_CLOEXEC | OFlag::O_DIRECT)?,
                pipe2(OFlag::O_CLOEXEC | OFlag::O_DIRECT)?,
            ),
        })
    }

    /// Add an argument to pass to the program.
    pub fn arg<S: AsRef<OsStr>>(&mut self, arg: S) -> &mut Command {
        self.args.push(arg.to_cstring());
        self
    }

    /// Add multiple arguments to pass to the program.
    pub fn args<I, S>(&mut self, args: I) -> &mut Command
    where
        I: IntoIterator<Item = S>,
        S: AsRef<OsStr>,
    {
        for arg in args {
            self.arg(arg.as_ref());
        }
        self
    }
}
