use nix::libc::{gid_t, uid_t};
use serde::{ser::SerializeMap, Serialize};

/// Entry (row) in the uid map
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct UidMap {
    /// First uid inside the guest namespace
    pub inside_uid: uid_t,
    /// First uid in external (host) namespace
    pub outside_uid: uid_t,
    /// Number of uids that this entry allows starting from inside/outside uid
    pub count: uid_t,
}

/// Entry (row) in the gid map
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct GidMap {
    /// First gid inside the guest namespace
    pub inside_gid: gid_t,
    /// First gid in external (host) namespace
    pub outside_gid: gid_t,
    /// Number of gids that this entry allows starting from inside/outside gid
    pub count: gid_t,
}

impl Serialize for UidMap {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(3))?;

        map.serialize_entry("inside_uid", &self.inside_uid)?;
        map.serialize_entry("outside_uid", &self.outside_uid)?;
        map.serialize_entry("count", &self.count)?;

        map.end()
    }
}

impl Serialize for GidMap {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(3))?;

        map.serialize_entry("inside_gid", &self.inside_gid)?;
        map.serialize_entry("outside_gid", &self.outside_gid)?;
        map.serialize_entry("count", &self.count)?;

        map.end()
    }
}
