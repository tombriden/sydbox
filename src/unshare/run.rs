use std::{
    ffi::CString,
    os::{
        fd::{AsRawFd, FromRawFd, OwnedFd},
        unix::io::RawFd,
    },
    ptr,
};

use libseccomp::ScmpFilterContext;
use nix::{
    self,
    errno::Errno,
    libc::c_char,
    sys::wait::{waitpid, WaitStatus},
    unistd::{close, fork, read, setpgid, write, ForkResult, Pid},
};

use crate::unshare::{child, config::Config, Child, Command};

type ChildPreExecFunc = Box<dyn Fn() -> Result<(), Errno>>;
type PipePair = ((RawFd, RawFd), (RawFd, RawFd));

pub struct ChildInfo<'a> {
    pub filename: *const c_char,
    pub args: &'a [*const c_char],
    pub cfg: Config,
    pub pre_exec: Option<ChildPreExecFunc>,
    pub seccomp_filter: Option<ScmpFilterContext>,
    pub seccomp_pipefd: PipePair,
}

impl<'a> Drop for ChildInfo<'a> {
    fn drop(&mut self) {
        let _ = close(self.seccomp_pipefd.0 .0);
        let _ = close(self.seccomp_pipefd.0 .1);
        let _ = close(self.seccomp_pipefd.1 .0);
        let _ = close(self.seccomp_pipefd.1 .1);
    }
}

fn raw_with_null(arr: &Vec<CString>) -> Vec<*const c_char> {
    let mut vec = Vec::with_capacity(arr.len().saturating_add(1));
    for i in arr {
        vec.push(i.as_ptr());
    }
    vec.push(ptr::null());
    vec
}

impl Command {
    /// Spawn the command and return a handle that can be waited for
    pub fn spawn(self) -> Result<Child, Errno> {
        let c_args = raw_with_null(&self.args);

        // Clear the environment.
        std::env::remove_var("SYD_INIT");
        std::env::remove_var(crate::config::ENV_LOG);
        std::env::remove_var(crate::config::ENV_NO_SYSLOG);
        std::env::remove_var(crate::config::ENV_NO_CROSS_MEMORY_ATTACH);
        std::env::remove_var(crate::config::ENV_SH);

        // SAFETY: In libc we trust.
        match unsafe { fork() }? {
            ForkResult::Parent { child, .. } => {
                let seccomp_fd = match self.after_start(child) {
                    Ok(seccomp_fd) => seccomp_fd,
                    Err(e) => loop {
                        match waitpid(child, None) {
                            Ok(WaitStatus::Exited(_, errno)) => return Err(Errno::from_i32(errno)),
                            Err(Errno::EINTR) => {}
                            _ => return Err(e),
                        }
                    },
                };

                Ok(Child {
                    pid: child.into(),
                    status: None,
                    seccomp_fd,
                })
            }
            ForkResult::Child => {
                let child_info = ChildInfo {
                    filename: self.filename.as_ptr(),
                    args: &c_args[..],
                    cfg: self.config,
                    pre_exec: self.pre_exec,
                    seccomp_filter: self.seccomp_filter,
                    seccomp_pipefd: self.seccomp_pipefd,
                };
                child::child_after_clone(child_info);
            }
        }
    }

    #[allow(clippy::cognitive_complexity)]
    fn after_start(mut self, pid: Pid) -> Result<RawFd, Errno> {
        if self.config.make_group_leader {
            setpgid(pid, pid)?;
        }

        if let Some(ref mut callback) = self.before_unfreeze {
            #[allow(clippy::cast_sign_loss)]
            callback(i32::from(pid) as u32)?;
        }

        // Close the read end of the first pipe
        // Close the write end of the second pipe
        let _ = close(self.seccomp_pipefd.0 .0);
        let _ = close(self.seccomp_pipefd.1 .1);
        // SAFETY: seccomp_pipefds hold return value of pipe2 which are valid FDs.
        let pipe_ro = unsafe { OwnedFd::from_raw_fd(self.seccomp_pipefd.1 .0) };
        // SAFETY: seccomp_pipefds hold return value of pipe2 which are valid FDs.
        let pipe_rw = unsafe { OwnedFd::from_raw_fd(self.seccomp_pipefd.0 .1) };

        // Read the value of the file descriptor from the pipe.
        let mut buf = vec![0u8; std::mem::size_of::<RawFd>()];
        read(pipe_ro.as_raw_fd(), &mut buf)?;
        let fd = match buf.as_slice().try_into() {
            Ok(bytes) => RawFd::from_le_bytes(bytes),
            Err(_) => return Err(Errno::EINVAL),
        };

        // Receive the seccomp notification file descriptor.
        // SAFETY: In libc we trust.
        let pid_fd = match unsafe { nix::libc::syscall(nix::libc::SYS_pidfd_open, pid, 0) } {
            e if e < 0 => return Err(Errno::last()),
            fd => {
                // SAFETY: pidfd_open returns a valid FD.
                unsafe { OwnedFd::from_raw_fd(fd as RawFd) }
            }
        };
        // SAFETY: In libc we trust.
        let fd = match unsafe {
            nix::libc::syscall(nix::libc::SYS_pidfd_getfd, pid_fd.as_raw_fd(), fd, 0)
        } {
            e if e < 0 => return Err(Errno::last()),
            fd => fd as RawFd,
        };

        // Send success notification to the child.
        let ack = [7u8];
        if let Err(errno) = write(pipe_rw.as_raw_fd(), &ack) {
            let _ = close(fd);
            return Err(errno);
        }

        Ok(fd)
    }
}
