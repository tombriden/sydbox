//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/sandbox.rs: Sandbox configuration
//
// Copyright (c) 2023, 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    clone::Clone,
    collections::{HashMap, HashSet},
    env,
    ffi::OsStr,
    fmt,
    fs::File,
    io::{self, BufRead, BufReader, Read},
    net::IpAddr,
    ops::{Deref, DerefMut},
    os::unix::ffi::OsStrExt,
    path::{Path, PathBuf},
    process::{Command, Stdio},
    str::FromStr,
};

use anyhow::{bail, Context};
use bitflags::bitflags;
use globset::{Glob, GlobBuilder, GlobSet, GlobSetBuilder};
use ipnetwork::IpNetwork;
use libseccomp::ScmpNotifReq;
use nix::{errno::Errno, fcntl::OFlag, mount::MsFlags, unistd::Pid};
use once_cell::sync::Lazy;
use parking_lot::{RwLockReadGuard, RwLockWriteGuard};
use regex::{Captures, Regex, RegexBuilder};
use serde::{
    ser::{SerializeMap, SerializeStruct},
    Serialize, Serializer,
};
use smallvec::SmallVec;

use crate::{
    config::*,
    error,
    hook::{RemoteProcess, UNotifyEventRequest},
    info, trace,
    unshare::Namespace,
};

const LINE_MAX: usize = 4096;

static RE_BIND: Lazy<Regex> = Lazy::new(|| {
    #[allow(clippy::disallowed_methods)]
    RegexBuilder::new(
        r"
        \A
        bind
        (?P<mod>[+-^])
        (?P<src>[^:]+)
        :
        (?P<dst>[^:]+)
        :
        (?P<opt>
            (ro|nodev|noexec|nosuid|noatime|nodiratime|relatime)
            (,(ro|nodev|noexec|nosuid|noatime|nodiratime|relatime))*
        )
        \z
    ",
    )
    .ignore_whitespace(true)
    .build()
    .expect("Invalid bind mount regex, please file a bug!")
});

static RE_RULE: Lazy<Regex> = Lazy::new(|| {
    #[allow(clippy::disallowed_methods)]
    RegexBuilder::new(
        r"
        \A
        (
            # We either have exec/kill or an action with a capability.
            exec/kill |
            (?P<act>
                allow |
                deny |
                filter
            )/
            (
                # Match combinations of read, write, exec, and stat
                (?P<cap_many>
                    (
                        read |
                        write |
                        exec |
                        stat
                    )
                    (,
                        (
                            read |
                            write |
                            exec |
                            stat
                        )
                    )*
                ) |
                # Other capabilities, not allowing combinations
                (?P<cap_single>
                    mem |
                    pid |
                    lock/read |
                    lock/write |
                    net/bind |
                    net/connect
                )
            )
        )
        (?P<mod>\+|\-|\^|:)
        (?P<pat>.+)
        \z
    ",
    )
    .ignore_whitespace(true)
    .build()
    .expect("Invalid sandbox rule regex, please file a bug!")
});

static RE_NETALIAS: Lazy<Regex> = Lazy::new(|| {
    #[allow(clippy::disallowed_methods)]
    RegexBuilder::new(
        r"
        \A
        (?P<command>
            (
                allow |
                deny |
                filter
            )
            /net/
            (
                bind |
                connect
            )
            [+-^]
            # SAFETY: Every item in the regex group `alias' below,
            # must have a corresponding item in the MAP_NETALIAS hash map!
            (?P<alias>
                ([aA][nN][yY][46]?) |
                ([lL][oO][cC][aA][lL][46]?) |
                ([lL][oO][oO][pP][bB][aA][cC][kK][46]?) |
                ([lL][iI][nN][kK][lL][oO][cC][aA][lL][46]?)
            )
            [!@]
            [0-9]+
            (-[0-9]+)?
        )
        \z
    ",
    )
    .ignore_whitespace(true)
    .build()
    .expect("Invalid network alias regex, please file a bug!")
});

type AliasMap<'a> = HashMap<&'a str, Vec<&'a str>>;
static MAP_NETALIAS: Lazy<AliasMap> = Lazy::new(|| {
    let mut map = HashMap::new();
    map.insert("any4", vec!["0.0.0.0/0"]);
    map.insert("any6", vec!["::/0"]);
    map.insert("any", vec!["0.0.0.0/0", "::/0"]);
    map.insert("linklocal4", vec!["fe80::/10"]);
    map.insert("linklocal6", vec!["fe80::/10"]);
    map.insert("linklocal", vec!["169.254.0.0/16", "fe80::/10"]);
    map.insert(
        "local4",
        vec![
            "127.0.0.0/8",
            "10.0.0.0/8",
            "172.16.0.0/12",
            "192.168.0.0/16",
        ],
    );
    map.insert("local6", vec!["::1", "fe80::/7", "fc00::/7", "fec0::/7"]);
    map.insert(
        "local",
        vec![
            "127.0.0.0/8",
            "10.0.0.0/8",
            "172.16.0.0/12",
            "192.168.0.0/16",
            "::1/8",
            "fe80::/7",
            "fc00::/7",
            "fec0::/7",
        ],
    );
    map.insert("loopback4", vec!["127.0.0.0/8"]);
    map.insert("loopback6", vec!["::1/8"]);
    map.insert("loopback", vec!["127.0.0.0/8", "::1/8"]);

    map
});

#[inline]
fn strbool(s: &str) -> Result<bool, Errno> {
    match s.to_ascii_lowercase().as_str() {
        "1" | "on" | "t" | "tr" | "tru" | "true" | "✓" => Ok(true),
        "0" | "off" | "f" | "fa" | "fal" | "fals" | "false" | "✗" => Ok(false),
        "" => Err(Errno::EFAULT),
        _ => Err(Errno::EINVAL),
    }
}

type LandlockPathPair = (Vec<String>, Vec<String>);

/// Represents a recursive bind mount operation.
#[derive(Debug)]
pub struct BindMount {
    /// Source directory
    pub src: PathBuf,
    /// Target directory, can be the same as source
    pub dst: PathBuf,
    /// The options that are allowed are:
    /// ro, nosuid, nodev, noexec, noatime, nodiratime and relatime
    /// kernel is going to ignore other options.
    pub opt: MsFlags,
}

impl PartialEq for BindMount {
    // Flags are not used in equality check.
    fn eq(&self, other: &Self) -> bool {
        self.src == other.src && self.dst == other.dst
    }
}

// Eq can be derived as well since PartialEq is implemented
impl Eq for BindMount {}

impl From<&Captures<'_>> for BindMount {
    fn from(captures: &Captures) -> Self {
        Self {
            src: PathBuf::from(&captures["src"]),
            dst: PathBuf::from(&captures["dst"]),
            opt: captures["opt"]
                .split(',')
                .map(|flag| match flag {
                    "ro" => MsFlags::MS_RDONLY,
                    "nodev" => MsFlags::MS_NODEV,
                    "noexec" => MsFlags::MS_NOEXEC,
                    "nosuid" => MsFlags::MS_NOSUID,
                    "noatime" => MsFlags::MS_NOATIME,
                    "nodiratime" => MsFlags::MS_NODIRATIME,
                    "relatime" => MsFlags::MS_RELATIME,
                    _ => unreachable!(),
                })
                .fold(MsFlags::empty(), |acc, flag| acc | flag),
        }
    }
}

bitflags! {
    /// Sandboxing capabilities
    pub struct Capability: u32 {
        /// Read capability
        const CAP_READ = 1;
        /// List capability
        const CAP_STAT = 2;
        /// Write capability
        const CAP_WRITE = 4;
        /// Execute capability
        const CAP_EXEC = 8;
        /// Network connect capability
        const CAP_CONNECT = 16;
        /// Network bind capability
        const CAP_BIND = 32;
        /// Memory capability
        const CAP_MEM = 64;
        /// Pid capability
        const CAP_PID = 128;
        /// Landlock read capability
        const CAP_LOCK_RO = 256;
        /// Landlock read-write capability
        const CAP_LOCK_RW = 512;
        /// Landlock capability
        const CAP_LOCK = Self::CAP_LOCK_RO.bits | Self::CAP_LOCK_RW.bits;
    }
}

impl Capability {
    /// Define a static array containing all capabilities with path/glob rules
    const GLOB: [Capability; 8] = [
        Capability::CAP_READ,
        Capability::CAP_STAT,
        Capability::CAP_WRITE,
        Capability::CAP_EXEC,
        Capability::CAP_CONNECT,
        Capability::CAP_BIND,
        Capability::CAP_LOCK_RO,
        Capability::CAP_LOCK_RW,
    ];
}

impl fmt::Display for Capability {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut capabilities: SmallVec<[&str; 9]> = SmallVec::new();

        if self.contains(Capability::CAP_READ) {
            capabilities.push("Read");
        }
        if self.contains(Capability::CAP_STAT) {
            capabilities.push("Stat");
        }
        if self.contains(Capability::CAP_WRITE) {
            capabilities.push("Write");
        }
        if self.contains(Capability::CAP_EXEC) {
            capabilities.push("Execute");
        }
        if self.contains(Capability::CAP_CONNECT) {
            capabilities.push("Connect");
        }
        if self.contains(Capability::CAP_MEM) {
            capabilities.push("Memory");
        }
        if self.contains(Capability::CAP_PID) {
            capabilities.push("Pid");
        }
        if self.contains(Capability::CAP_BIND) {
            capabilities.push("Bind");
        }
        if self.contains(Capability::CAP_LOCK) {
            capabilities.push("LandLock");
        }

        write!(f, "{}", capabilities.join(", "))
    }
}

impl Serialize for Capability {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut capabilities = String::new();

        if self.contains(Capability::CAP_READ) {
            capabilities.push('r');
        }
        if self.contains(Capability::CAP_STAT) {
            capabilities.push('s');
        }
        if self.contains(Capability::CAP_WRITE) {
            capabilities.push('w');
        }
        if self.contains(Capability::CAP_EXEC) {
            capabilities.push('x');
        }
        if self.contains(Capability::CAP_BIND) {
            capabilities.push('b');
        }
        if self.contains(Capability::CAP_CONNECT) {
            capabilities.push('c');
        }
        if self.contains(Capability::CAP_MEM) {
            capabilities.push('m');
        }
        if self.contains(Capability::CAP_PID) {
            capabilities.push('p');
        }
        if self.contains(Capability::CAP_LOCK) {
            capabilities.push('l');
        }

        if capabilities.is_empty() {
            serializer.serialize_none()
        } else {
            serializer.serialize_str(&capabilities)
        }
    }
}

impl From<&Captures<'_>> for Capability {
    fn from(captures: &Captures) -> Self {
        if let Some(cap) = captures.name("cap_single") {
            match cap.as_str() {
                "mem" => Capability::CAP_MEM,
                "pid" => Capability::CAP_PID,
                "lock/read" => Capability::CAP_LOCK_RO,
                "lock/write" => Capability::CAP_LOCK_RW,
                "net/bind" => Capability::CAP_BIND,
                "net/connect" => Capability::CAP_CONNECT,
                _ => unreachable!(),
            }
        } else if let Some(caps) = captures.name("cap_many") {
            caps.as_str()
                .split(',')
                .map(|cap| match cap {
                    "read" => Capability::CAP_READ,
                    "write" => Capability::CAP_WRITE,
                    "exec" => Capability::CAP_EXEC,
                    "stat" => Capability::CAP_STAT,
                    _ => unreachable!(),
                })
                .fold(Capability::empty(), |acc, cap| acc | cap)
        } else {
            Capability::CAP_EXEC // See the regex.
        }
    }
}

impl TryFrom<(&RemoteProcess, &UNotifyEventRequest, &ScmpNotifReq, &str)> for Capability {
    type Error = Errno;

    // Find out capabilities of the system call using the system call name and seccomp request.
    fn try_from(
        value: (&RemoteProcess, &UNotifyEventRequest, &ScmpNotifReq, &str),
    ) -> Result<Self, Errno> {
        let (proc, request, req, syscall_name) = value;
        match syscall_name {
            name if Capability::exec(name) => Ok(Self::CAP_EXEC),
            name if Capability::stat(name) => Ok(Self::CAP_STAT),
            "open" | "openat" | "openat2" => Capability::open(syscall_name, req, proc, request),
            _ => Ok(Self::CAP_WRITE),
        }
    }
}

impl Capability {
    fn open(
        syscall_name: &str,
        req: &ScmpNotifReq,
        proc: &RemoteProcess,
        request: &UNotifyEventRequest,
    ) -> Result<Self, Errno> {
        Ok(match syscall_name {
            "open" | "openat" => {
                let flidx = if syscall_name == "open" { 1 } else { 2 };
                #[allow(clippy::cast_possible_truncation)]
                let flags = OFlag::from_bits_truncate(req.data.args[flidx] as nix::libc::c_int);
                match flags & OFlag::O_ACCMODE {
                    OFlag::O_RDONLY => Self::CAP_READ,
                    _ => Self::CAP_WRITE,
                }
            }
            "openat2" => {
                #[allow(clippy::cast_possible_truncation)]
                let rohow = proc.remote_ohow(
                    req.data.args[2] as usize,
                    req.data.args[3] as usize,
                    request,
                )?;
                #[allow(clippy::cast_possible_truncation)]
                let flags = OFlag::from_bits_truncate(rohow.flags as nix::libc::c_int);
                match flags & OFlag::O_ACCMODE {
                    OFlag::O_RDONLY => Self::CAP_READ,
                    _ => Self::CAP_WRITE,
                }
            }
            _ => unreachable!(),
        })
    }

    fn exec(syscall_name: &str) -> bool {
        matches!(syscall_name, "execve" | "execveat")
    }

    fn stat(syscall_name: &str) -> bool {
        matches!(
            syscall_name,
            "access"
                | "faccessat"
                | "faccessat2"
                | "chdir"
                | "fchdir"
                | "getdents"
                | "getdents64"
                | "stat"
                | "statx"
                | "fstat"
                | "lstat"
                | "newfstatat"
                | "readlink"
                | "readlinkat"
                | "getxattr"
                | "lgetxattr"
                | "fgetxattr"
                | "listxattr"
                | "flistxattr"
                | "llistxattr"
        )
    }
}

bitflags! {
    /// Sandboxing options
    pub struct Flag: u64 {
        /// Enable trace mode, aka "dry run" mode
        const FL_TRACE = 1 << 0;
        /// Allow successful bind calls for subsequent connect calls
        const FL_ALLOW_SAFE_BIND = 1 << 1;
        /// Allow socket families which are unsupported
        const FL_ALLOW_UNSUPP_SOCKET = 1 << 2;
        /// Send SIGKILL on Memory access violation.
        const FL_KILL_MEM = 1 << 3;
        /// Send SIGKILL on PID access violation.
        const FL_KILL_PID = 1 << 4;
        /// Wait for all processes before exiting.
        const FL_EXIT_WAIT_ALL = 1 << 5;

        /// Allow unsafe perf calls.
        const FL_ALLOW_UNSAFE_PERF = 1 << 44;
        /// Allow unsafe ptrace calls.
        const FL_ALLOW_UNSAFE_PTRACE = 1 << 45;
        /// Allow unsafe Linux capabilities.
        const FL_ALLOW_UNSAFE_CAPS = 1 << 46;
        /// Allow unsafe environment variables.
        const FL_ALLOW_UNSAFE_ENV = 1 << 47;
        /// Allow unsafe socket families (RAW and PACKET).
        const FL_ALLOW_UNSAFE_SOCKET = 1 << 48;
        /// Allow unsafe ioctl calls
        const FL_ALLOW_UNSAFE_IOCTL = 1 << 49;
        /// Allow unsafe prctl calls
        const FL_ALLOW_UNSAFE_PRCTL = 1 << 50;
        /// Allow unsafe prlimit calls
        const FL_ALLOW_UNSAFE_PRLIMIT = 1 << 51;
        /// Allow unsafe adjtimex and clock_adjtime calls
        const FL_ALLOW_UNSAFE_ADJTIME = 1 << 52;
        /// Allow the unsafe io-uring interface
        const FL_ALLOW_UNSAFE_IOURING = 1 << 53;
        /// Deny reading the timestamp counter (x86 only)
        const FL_DENY_TSC = 1 << 54;

        /// Mount private /dev/shm for the sandbox process
        const FL_PRIVATE_SHM = 1 << 55;
        /// Mount private /tmp for the sandbox process
        const FL_PRIVATE_TMP = 1 << 56;
        /// Unshare mount namespace
        const FL_UNSHARE_MOUNT = 1 << 57;
        /// Unshare uts namespace
        const FL_UNSHARE_UTS = 1 << 58;
        /// Unshare ipc namespace
        const FL_UNSHARE_IPC = 1 << 59;
        /// Unshare user namespace
        const FL_UNSHARE_USER = 1 << 60;
        /// Unshare pid namespace
        const FL_UNSHARE_PID = 1 << 61;
        /// Unshare net namespace
        const FL_UNSHARE_NET = 1 << 62;
        /// Unshare cgroup namespace
        const FL_UNSHARE_CGROUP = 1 << 63;
    }
}

impl fmt::Display for Flag {
    #[allow(clippy::cognitive_complexity)]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut flags: SmallVec<[&str; 16]> = SmallVec::new();

        if self.contains(Flag::FL_TRACE) {
            flags.push("Trace");
        }
        if self.contains(Flag::FL_UNSHARE_MOUNT) {
            flags.push("Unshare Mount");
        }
        if self.contains(Flag::FL_UNSHARE_UTS) {
            flags.push("Unshare UTS");
        }
        if self.contains(Flag::FL_UNSHARE_USER) {
            flags.push("Unshare User");
        }
        if self.contains(Flag::FL_UNSHARE_PID) {
            flags.push("Unshare Pid");
        }
        if self.contains(Flag::FL_UNSHARE_NET) {
            flags.push("Unshare Net");
        }
        if self.contains(Flag::FL_UNSHARE_CGROUP) {
            flags.push("Unshare CGroup");
        }
        if self.contains(Flag::FL_PRIVATE_SHM) {
            flags.push("Private /dev/shm");
        }
        if self.contains(Flag::FL_PRIVATE_TMP) {
            flags.push("Private /tmp");
        }
        if self.contains(Flag::FL_EXIT_WAIT_ALL) {
            flags.push("Exit Wait All");
        }
        if self.contains(Flag::FL_KILL_MEM) {
            flags.push("Kill Mem");
        }
        if self.contains(Flag::FL_KILL_PID) {
            flags.push("Kill Pid");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_PERF) {
            flags.push("Allow Unsafe Perf");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_PTRACE) {
            flags.push("Allow Unsafe PTrace");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_CAPS) {
            flags.push("Allow Unsafe Capabilities");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_ENV) {
            flags.push("Allow Unsafe Environment");
        }
        if self.contains(Flag::FL_ALLOW_SAFE_BIND) {
            flags.push("Allow Safe Bind");
        }
        if self.contains(Flag::FL_ALLOW_UNSUPP_SOCKET) {
            flags.push("Allow Unsupported Socket Families");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_SOCKET) {
            flags.push("Allow Unsafe Socket Families");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_IOCTL) {
            flags.push("Allow Unsafe IOctl");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_PRCTL) {
            flags.push("Allow Unsafe PRctl");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_PRLIMIT) {
            flags.push("Allow Unsafe PRlimit");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_ADJTIME) {
            flags.push("Allow Unsafe Adjust Time");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_IOURING) {
            flags.push("Allow Unsafe IO_Uring");
        }
        if self.contains(Flag::FL_DENY_TSC) {
            flags.push("Deny TSC");
        }

        write!(f, "{}", flags.join(", "))
    }
}

impl Serialize for Flag {
    #[allow(clippy::cognitive_complexity)]
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut flags: SmallVec<[&str; 16]> = SmallVec::new();
        if self.is_empty() {
            return serializer.collect_seq(flags);
        }
        if self.contains(Flag::FL_TRACE) {
            flags.push("trace");
        }
        if self.contains(Flag::FL_UNSHARE_MOUNT) {
            flags.push("unshare-mount");
        }
        if self.contains(Flag::FL_UNSHARE_UTS) {
            flags.push("unshare-uts");
        }
        if self.contains(Flag::FL_UNSHARE_USER) {
            flags.push("unshare-user");
        }
        if self.contains(Flag::FL_UNSHARE_PID) {
            flags.push("unshare-pid");
        }
        if self.contains(Flag::FL_UNSHARE_NET) {
            flags.push("unshare-net");
        }
        if self.contains(Flag::FL_UNSHARE_CGROUP) {
            flags.push("unshare-cgroup");
        }
        if self.contains(Flag::FL_PRIVATE_SHM) {
            flags.push("private-shm");
        }
        if self.contains(Flag::FL_PRIVATE_TMP) {
            flags.push("private-tmp");
        }
        if self.contains(Flag::FL_EXIT_WAIT_ALL) {
            flags.push("exit-wait-all");
        }
        if self.contains(Flag::FL_KILL_MEM) {
            flags.push("kill-mem");
        }
        if self.contains(Flag::FL_KILL_PID) {
            flags.push("kill-pid");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_PERF) {
            flags.push("allow-unsafe-perf");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_PTRACE) {
            flags.push("allow-unsafe-ptrace");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_CAPS) {
            flags.push("allow-unsafe-caps");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_ENV) {
            flags.push("allow-unsafe-env");
        }
        if self.contains(Flag::FL_ALLOW_SAFE_BIND) {
            flags.push("allow-safe-bind");
        }
        if self.contains(Flag::FL_ALLOW_UNSUPP_SOCKET) {
            flags.push("allow-unsupp-socket");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_SOCKET) {
            flags.push("allow-unsafe-socket");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_IOCTL) {
            flags.push("allow-unsafe-ioctl");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_PRCTL) {
            flags.push("allow-unsafe-prctl");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_PRLIMIT) {
            flags.push("allow-unsafe-prlimit");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_ADJTIME) {
            flags.push("allow-unsafe-adjtime");
        }
        if self.contains(Flag::FL_ALLOW_UNSAFE_IOURING) {
            flags.push("allow-unsafe-uring");
        }
        if self.contains(Flag::FL_DENY_TSC) {
            flags.push("deny-tsc");
        }
        serializer.collect_seq(flags)
    }
}

/// Represents a network address pattern
#[derive(Debug, Eq, PartialEq)]
pub struct AddressPattern {
    addr: IpNetwork,
    port: [u16; 2],
}

impl fmt::Display for AddressPattern {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.port[0] == self.port[1] {
            write!(f, "{}:{}", self.addr, self.port[0])
        } else {
            write!(f, "{}:{}-{}", self.addr, self.port[0], self.port[1])
        }
    }
}

impl Serialize for AddressPattern {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_map(Some(2))?;
        map.serialize_entry("addr", &self.addr)?;
        if self.port[0] == self.port[1] {
            // Single port
            map.serialize_entry("port", &self.port[0])?;
        } else {
            // Port range
            map.serialize_entry("port", &self.port)?;
        }
        map.end()
    }
}

/// Represents a rule action
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Action {
    /// Filter
    Filter,
    /// Allow
    Allow,
    /// Deny
    Deny,
    /// Kill
    Kill,
}

impl fmt::Display for Action {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Action::Filter => write!(f, "Filter"),
            Action::Allow => write!(f, "Allow"),
            Action::Deny => write!(f, "Deny"),
            Action::Kill => write!(f, "Kill"),
        }
    }
}

impl Serialize for Action {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl From<&Captures<'_>> for Action {
    fn from(captures: &Captures) -> Self {
        if let Some(act) = captures.name("act") {
            match act.as_str() {
                "allow" => Action::Allow,
                "deny" => Action::Deny,
                "filter" => Action::Filter,
                _ => unreachable!(),
            }
        } else {
            Action::Kill
        }
    }
}

/// Represents a network address sandboxing rule.
#[derive(Debug)]
pub struct CidrRule {
    act: Action,
    cap: Capability,
    pat: AddressPattern,
}

/// Represents a glob sandboxing rule.
#[derive(Debug)]
pub struct GlobRule {
    act: Action,
    cap: Capability,
    pat: Glob,
}

impl fmt::Display for CidrRule {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Action: {}, Capability: {}, Pattern: {}",
            self.act, self.cap, self.pat
        )
    }
}

impl fmt::Display for GlobRule {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Action: {}, Capability: {}, Pattern: {}",
            self.act,
            self.cap,
            self.pat.glob()
        )
    }
}

impl Serialize for CidrRule {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut state = serializer.serialize_struct("cidr", 3)?;
        state.serialize_field("act", &self.act)?;
        state.serialize_field("cap", &self.cap)?;
        state.serialize_field("pat", &self.pat)?;
        state.end()
    }
}

impl Serialize for GlobRule {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut state = serializer.serialize_struct("glob", 3)?;
        state.serialize_field("act", &self.act)?;
        state.serialize_field("cap", &self.cap)?;
        state.serialize_field("pat", &self.pat.glob())?;
        state.end()
    }
}

/// Represents the state of the magic command lock.
#[derive(Debug, Eq, PartialEq)]
pub enum LockState {
    /// Lock is off, sandbox commands are allowed.
    Off,
    /// Sandbox commands are only allowed to the syd execve child.
    Exec,
    /// Lock is set, sandbox commands are not allowed.
    Set,
}

impl Serialize for LockState {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(match self {
            LockState::Exec => "exec",
            LockState::Set => "set",
            LockState::Off => "off",
        })
    }
}

type SandboxGlobSet = (GlobSet, GlobSetBuilder, bool);

/// Sandbox
#[derive(Debug)]
pub struct Sandbox {
    /// Sandbox options represented using a set of `Flag` flags.
    pub flags: Flag,

    /// Sandbox state represented using a set of `Capability` flags.
    pub state: Capability,

    /// State of the magic lock.
    pub lock: LockState,

    /// Process ID of the syd execve child.
    cpid: nix::libc::pid_t,

    /// If `true` all id system calls return 0 in the sandbox.
    fake_root: bool,

    /// If `true` root is mapped to current user in the user namespace.
    pub map_root: bool,

    /// Change root to this directory on startup.
    pub root: Option<PathBuf>,

    /// Mountpoint for the proc filesystem.
    pub proc: Option<PathBuf>,

    /// Mount propagation flags.
    pub propagation: Option<MsFlags>,

    /// Hostname in UTS namespace.
    pub hostname: String,

    /// Domainname in UTS namespace.
    pub domainname: String,

    /// Per-process memory limit in bytes for memory sandboxing.
    pub mem_max: u64,

    /// Per-process virtual memory limit in bytes for memory sandboxing.
    pub mem_vm_max: u64,

    /// A boolean specifying whether memory sandboxing violations
    /// should be reported.
    pub mem_filter: bool,

    /// Pid limit for PID sandboxing.
    pub pid_max: usize,

    /// A boolean specifying whether PID sandboxing violations
    /// should be reported.
    pub pid_filter: bool,

    // List of bind mounts.
    bind_mounts: Option<Vec<BindMount>>,

    // List of network address sandboxing rules.
    cidr_rules: Vec<CidrRule>,
    // List of glob sandboxing rules.
    glob_rules: Vec<GlobRule>,

    // read glob sets for access and filter.
    globset_access_r: SandboxGlobSet,
    globset_filter_r: SandboxGlobSet,

    // stat glob sets for access and filter.
    globset_access_s: SandboxGlobSet,
    globset_filter_s: SandboxGlobSet,

    // write glob sets for access and filter.
    globset_access_w: SandboxGlobSet,
    globset_filter_w: SandboxGlobSet,

    // execute glob sets for access, filter, and kill
    globset_access_x: SandboxGlobSet,
    globset_filter_x: SandboxGlobSet,
    globset_kill_x: SandboxGlobSet,

    // network bind glob sets for access and filter.
    globset_access_nb: SandboxGlobSet,
    globset_filter_nb: SandboxGlobSet,

    // network connect glob sets for access and filter.
    globset_access_nc: SandboxGlobSet,
    globset_filter_nc: SandboxGlobSet,
}

/// Sandbox guard to use it practically under a read/write lock.
#[derive(Debug)]
pub enum SandboxGuard<'a> {
    /// Sandbox locked for read
    Read(RwLockReadGuard<'a, Sandbox>),
    /// Sandbox locked for write
    Write(RwLockWriteGuard<'a, Sandbox>),
}

impl Default for Flag {
    fn default() -> Self {
        Self::empty()
    }
}

impl Default for Capability {
    fn default() -> Self {
        Self::CAP_READ
            | Self::CAP_WRITE
            | Self::CAP_EXEC
            | Self::CAP_STAT
            | Self::CAP_BIND
            | Self::CAP_CONNECT
    }
}

impl Default for LockState {
    fn default() -> Self {
        Self::Exec
    }
}

impl FromStr for LockState {
    type Err = io::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "off" => Ok(Self::Off),
            "on" => Ok(Self::Set),
            "exec" => Ok(Self::Exec),
            _ => Err(io::Error::from_raw_os_error(nix::libc::EINVAL)),
        }
    }
}

impl<'a> Deref for SandboxGuard<'a> {
    type Target = Sandbox;
    fn deref(&self) -> &Self::Target {
        match self {
            SandboxGuard::Read(guard) => guard,
            SandboxGuard::Write(guard) => guard,
        }
    }
}

impl<'a> DerefMut for SandboxGuard<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        match self {
            SandboxGuard::Write(guard) => guard,
            _ => panic!("Cannot mutate a read-locked Sandbox!"),
        }
    }
}

impl Default for Sandbox {
    fn default() -> Self {
        Sandbox {
            flags: Flag::default(),
            state: Capability::default(),
            lock: LockState::default(),
            cpid: 0,
            proc: Some(PathBuf::from("/proc")),
            root: None,
            fake_root: false,
            map_root: false,
            propagation: Some(MsFlags::MS_PRIVATE | MsFlags::MS_REC),
            hostname: "syd".to_string(),
            domainname: API_VERSION.to_string(),
            mem_max: 128_u64.saturating_mul(1024).saturating_mul(1024),
            mem_vm_max: 4096_u64.saturating_mul(1024).saturating_mul(1024),
            mem_filter: false,
            pid_max: 128,
            pid_filter: false,
            bind_mounts: None,
            cidr_rules: Vec::new(),
            glob_rules: Vec::new(),
            globset_access_r: (GlobSet::empty(), GlobSetBuilder::new(), false),
            globset_access_s: (GlobSet::empty(), GlobSetBuilder::new(), false),
            globset_access_w: (GlobSet::empty(), GlobSetBuilder::new(), false),
            globset_access_x: (GlobSet::empty(), GlobSetBuilder::new(), false),
            globset_access_nb: (GlobSet::empty(), GlobSetBuilder::new(), false),
            globset_access_nc: (GlobSet::empty(), GlobSetBuilder::new(), false),
            globset_filter_r: (GlobSet::empty(), GlobSetBuilder::new(), false),
            globset_filter_s: (GlobSet::empty(), GlobSetBuilder::new(), false),
            globset_filter_w: (GlobSet::empty(), GlobSetBuilder::new(), false),
            globset_filter_x: (GlobSet::empty(), GlobSetBuilder::new(), false),
            globset_filter_nb: (GlobSet::empty(), GlobSetBuilder::new(), false),
            globset_filter_nc: (GlobSet::empty(), GlobSetBuilder::new(), false),
            globset_kill_x: (GlobSet::empty(), GlobSetBuilder::new(), false),
        }
    }
}

impl fmt::Display for Sandbox {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "syd:")?;
        writeln!(f, "  Lock: {:?}", self.lock)?;
        writeln!(f, "  Capabilities: {}", self.state)?;
        writeln!(f, "  Fake Root: {}", self.fake_root)?;
        writeln!(f, "  Memory Max: {}", self.mem_max)?;
        writeln!(f, "  Virtual Memory Max: {}", self.mem_vm_max)?;
        writeln!(f, "  Pid Max: {}", self.pid_max)?;
        writeln!(f, "  Process ID: {}", self.cpid)?;
        writeln!(f, "  Options: {}", self.flags)?;
        writeln!(
            f,
            "  Cidr Rules: (total {}, highest precedence first)",
            self.cidr_rules.len()
        )?;
        for (idx, rule) in self.cidr_rules.iter().rev().enumerate() {
            // rev() because last matching rule wins.
            let idx = idx.saturating_add(1);
            writeln!(f, "    {idx}. {rule}")?;
        }
        writeln!(
            f,
            "  Glob Rules: (total {}, highest precedence first)",
            self.cidr_rules.len()
        )?;
        for (idx, rule) in self.glob_rules.iter().rev().enumerate() {
            // rev() because last matching rule wins.
            let idx = idx.saturating_add(1);
            writeln!(f, "    {idx}. {rule}")?;
        }
        Ok(())
    }
}

impl Serialize for Sandbox {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(9))?;

        map.serialize_entry("flags", &self.flags)?;
        map.serialize_entry("state", &self.state)?;
        map.serialize_entry("lock", &self.lock)?;
        map.serialize_entry("cpid", &self.cpid)?;
        map.serialize_entry("fake_root", &self.fake_root)?;
        map.serialize_entry("mem_max", &self.mem_max)?;
        map.serialize_entry("mem_vm_max", &self.mem_vm_max)?;
        map.serialize_entry("pid_max", &self.pid_max)?;
        map.serialize_entry("mem_filter", &self.mem_filter)?;
        map.serialize_entry("pid_filter", &self.pid_filter)?;
        map.serialize_entry("cidr_rules", &self.cidr_rules)?;
        map.serialize_entry("glob_rules", &self.glob_rules)?;

        map.end()
    }
}

impl Sandbox {
    /// Parses a configuration from a given file-like object and applies its configuration to the sandbox.
    ///
    /// This function reads from the given file-like object line by line. It skips lines that are either
    /// empty or start with a '#' (treated as comments). For each valid line, it applies its
    /// configuration to the provided sandbox.
    ///
    /// # Arguments
    ///
    /// * `file` - A file-like object to read the configuration from. This can be any type that
    ///            implements the `Read` and `BufRead` traits.
    ///
    /// # Returns
    ///
    /// * A Result indicating the success or failure of the operation.
    ///
    /// # Errors
    ///
    /// This function will return an error if:
    /// * There's an error reading a line from the file.
    /// * There's an issue in parsing and applying a configuration line to the sandbox.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use std::{fs::File, io::BufReader};
    ///
    /// use syd::sandbox::Sandbox;
    ///
    /// let file = BufReader::new(File::open("/path/to/config/file").expect("Failed to open file"));
    /// let mut sandbox = Sandbox::new();
    /// sandbox
    ///     .parse_config(file)
    ///     .expect("Failed to parse configuration");
    /// ```
    pub fn parse_config<F: Read + BufRead>(&mut self, mut file: F) -> anyhow::Result<()> {
        let mut line = vec![0; LINE_MAX as usize];
        let mut temp = Vec::new();
        let mut line_count = 1;

        loop {
            let bytes_read = file.read(&mut line[..])?;
            if bytes_read == 0 {
                break; // End of file reached.
            }

            if !line.iter().take(bytes_read).any(|&b| b == b'\n') {
                // If no newline is found in the current chunk and we're
                // reading from a file like /dev/zero, return an error
                // indicating the line count at which this was detected.
                return Err(anyhow::anyhow!(
                    "No newline found in the first {LINE_MAX} bytes at line {line_count}!",
                ));
            }
            temp.extend_from_slice(&line[..bytes_read]);

            while let Some(pos) = temp.iter().position(|&b| b == b'\n') {
                let line = &temp[..pos];
                let line = std::str::from_utf8(line).context(format!(
                    "Failed to convert bytes to UTF-8 at line {line_count}",
                ))?;
                let line = line.trim();
                if !line.is_empty() && !line.starts_with('#') {
                    self.config(line)
                        .with_context(|| format!("Failed to parse line {line_count}: `{line}'"))?;
                }
                // Remove the processed line from temp storage.
                temp.drain(..=pos);
                line_count += 1; // Increment line count after processing each line.
            }
        }

        Ok(())
    }

    /// Parses a configuration file and applies its configuration to the sandbox.
    ///
    /// This function reads the given configuration file line by line. It skips lines that are either
    /// empty or start with a '#' (treated as comments). For each valid line, it applies its
    /// configuration to the provided sandbox.
    ///
    /// # Arguments
    ///
    /// * `path` - A reference to the path of the configuration file. This can be any type that
    ///            implements the `AsRef<Path>` trait.
    ///
    /// # Returns
    ///
    /// * A Result indicating the success or failure of the operation.
    ///
    /// # Errors
    ///
    /// This function will return an error if:
    /// * There's an issue in opening the configuration file.
    /// * There's an error reading a line from the file.
    /// * There's an issue in parsing and applying a configuration line to the sandbox.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use syd::sandbox::Sandbox;
    ///
    /// let path = "/path/to/config/file";
    /// let mut sandbox = Sandbox::new();
    /// sandbox
    ///     .parse_config_file(&path)
    ///     .expect("Failed to parse configuration file");
    /// ```
    pub fn parse_config_file<P: AsRef<Path>>(&mut self, path: P) -> anyhow::Result<()> {
        let file = File::open(path).context("Failed to open configuration file.")?;
        let reader = BufReader::new(file);
        self.parse_config(reader)
    }

    /// Parses the given profile and applies its configuration to the sandbox.
    ///
    /// This function supports multiple predefined profiles such as "paludis", "noipv4", "noipv6", and "user".
    /// Each profile corresponds to a set of configuration lines which are applied to the sandbox.
    /// The "user" profile includes both static configurations and dynamic ones that depend on the
    /// environment and the existence of a user-specific configuration file.
    ///
    /// # Arguments
    ///
    /// * `name` - A string slice that holds the name of the profile to be parsed.
    /// * `sandbox` - A mutable reference to the sandbox where the profile configurations will be applied.
    ///
    /// # Returns
    ///
    /// * A Result indicating the success or failure of the operation.
    ///
    /// # Errors
    ///
    /// This function will return an error if:
    /// * The profile name is invalid.
    /// * There's an issue in parsing the configuration lines.
    /// * There's an issue in reading or parsing the user-specific configuration file for the "user" profile.
    pub fn parse_profile(&mut self, name: &str) -> anyhow::Result<()> {
        // Inner function to handle repetitive logic of applying configurations
        fn apply_config(sandbox: &mut Sandbox, profile: &[&str]) -> anyhow::Result<()> {
            for (idx, line) in profile.iter().enumerate() {
                let lcnt = idx.saturating_add(1);
                sandbox
                    .config(line)
                    .context(format!("Failed to parse line {lcnt}: `{line}'."))?;
            }
            Ok(())
        }

        match name {
            "container" => apply_config(self, PROFILE_CONTAINER),
            "immutable" => apply_config(self, PROFILE_IMMUTABLE),
            "landlock" => apply_config(self, PROFILE_LANDLOCK),
            "paludis" => apply_config(self, PROFILE_PALUDIS),
            "pandora" => apply_config(self, PROFILE_PANDORA),
            "noipv4" => apply_config(self, PROFILE_NOIPV4),
            "noipv6" => apply_config(self, PROFILE_NOIPV6),
            "silent" => apply_config(self, PROFILE_SILENT),
            "lib" => apply_config(self, PROFILE_LIB),
            "user" => {
                // Step 1: Apply static user configuration defined at compile-time.
                apply_config(self, PROFILE_USER)?;

                // Step 2: Apply dynamic, user-specific configuration.
                let uid = nix::unistd::getuid();
                let name = crate::get_user_name(uid);
                let home = crate::get_user_home(&name);

                // Save the user from some annoying warnings.
                if env::var("GIT_CEILING_DIRECTORIES").is_err() {
                    env::set_var("GIT_CEILING_DIRECTORIES", &home);
                }

                // /home
                // We allow read(/home/user) but not write(/home/user),
                // read|write(/home/user/**) is ok, i.e. the user can
                // not delete their home directory under the sandbox
                // which is a nice and funny protection.
                self.config(&format!("allow/lock/write+{home}"))?;
                self.config(&format!("allow/read+{home}/***"))?;
                self.config(&format!("allow/stat+{home}/***"))?;
                self.config(&format!("allow/write+{home}/**"))?;
                self.config(&format!("allow/exec+{home}/***"))?;
                self.config(&format!("allow/net/bind+{home}/**"))?;
                self.config(&format!("allow/net/connect+{home}/**"))?;
                // /run/user/uid
                self.config(&format!("allow/read+/run/user/{uid}/**"))?;
                self.config(&format!("allow/write+/run/user/{uid}/**"))?;
                self.config(&format!("allow/net/connect+/run/user/{uid}/**"))?;

                // Step 3: Parse the system & user configuration file if it exists.
                let rc = vec![
                    PathBuf::from(PATH_ETC).join(format!("user.syd-{API_VERSION}")),
                    Path::new(&home).join(format!(".user.syd-{API_VERSION}")),
                ];
                for p in rc {
                    if self.locked() {
                        info!("ctx": "config", "path": format!("{}", p.display()), "error": "lock");
                    } else if p.exists() {
                        self.parse_config_file(&p)?;
                    }
                }
                Ok(())
            }
            _ => bail!("Invalid profile `{name}'"),
        }
    }

    /// Configures the sandbox using a specified command.
    ///
    /// This method provides a central point for configuring the sandbox. It interprets and
    /// processes a variety of commands to adjust the sandbox's state, manage its tracing,
    /// handle regex-based configurations, and more.
    ///
    /// # Arguments
    ///
    /// * `command` - A string slice that represents the command to be executed.
    ///
    /// # Returns
    ///
    /// * A `Result` that indicates the success or failure of the operation. In the event of a
    ///   failure, it returns an appropriate error from the `Errno` enum.
    ///
    /// # Commands
    ///
    /// - If the command is empty or matches the API version, it simply returns `Ok(())`.
    /// - If the command starts with "lock", it attempts to set the sandbox's lock state.
    /// - If the command matches one of the supported commands, it applies the command to the sandbox.
    ///   See the ["Configuration" section in the README.md file](https://crates.io/crates/syd#configuration)
    ///   for a list of supported commands.
    /// - If none of the above conditions are met, it returns an error indicating invalid input.
    ///
    /// # Examples
    ///
    /// ```
    /// use syd::sandbox::Sandbox;
    ///
    /// let mut sandbox = Sandbox::new();
    /// sandbox
    ///     .config("lock:on")
    ///     .expect("Failed to lock the sandbox");
    /// ```
    #[allow(clippy::cognitive_complexity)]
    pub fn config(&mut self, command: &str) -> Result<(), Errno> {
        if command.is_empty() || command == API_VERSION {
            Ok(())
        } else if command == "reset" {
            // SAFETY: We must preserve child pid or lock:exec can be bypassed!
            *self = Self {
                cpid: self.cpid,
                ..Self::default()
            };
            Ok(())
        } else if command == "stat" {
            eprint!("{self}");
            Ok(())
        } else if let Some(state) = command.strip_prefix("lock:") {
            self.lock = LockState::from_str(state).map_err(|_| Errno::EINVAL)?;
            Ok(())
        } else if let Some(command) = command.strip_prefix("cmd/") {
            self.handle_sandbox_command(command)
        } else if let Some(command) = command.strip_prefix("sandbox/") {
            self.handle_sandbox_config(command)
        } else if let Some(command) = command.strip_prefix("mem/") {
            self.handle_mem_config(command)
        } else if let Some(command) = command.strip_prefix("pid/") {
            self.handle_pid_config(command)
        } else if let Some(command) = command.strip_prefix("trace/") {
            self.handle_trace_config(command)
        } else if let Some(command) = command.strip_prefix("unshare/") {
            self.handle_unshare_config(command)
        } else if let Some(command) = command.strip_prefix("name/") {
            self.handle_name_config(command)
        } else if let Some(command) = command.strip_prefix("root") {
            self.handle_root_config(command)
        } else if let Some(captures) = RE_BIND.captures(command) {
            self.handle_bind_config(&captures)
        } else if let Some(captures) = RE_NETALIAS.captures(command) {
            let alias = captures["alias"].to_ascii_lowercase();
            let command = captures["command"].to_ascii_lowercase();
            self.handle_netalias_config(&command, &alias)
        } else if let Some(captures) = RE_RULE.captures(command) {
            self.handle_rule_config(&captures)
        } else {
            Err(Errno::EINVAL)
        }
    }

    fn handle_netalias_config(&mut self, command: &str, alias: &str) -> Result<(), Errno> {
        if let Some(addr_vec) = MAP_NETALIAS.get(&alias) {
            for addr in addr_vec {
                let c = command.replacen(alias, addr, 1);
                self.config(&c)?;
            }
            Ok(())
        } else {
            // This should never happen,
            // but let's handle it safely anyway.
            Err(Errno::EAFNOSUPPORT)
        }
    }

    fn handle_name_config(&mut self, command: &str) -> Result<(), Errno> {
        if let Some(value) = command.strip_prefix("host:") {
            self.hostname = value.to_string();
        } else if let Some(value) = command.strip_prefix("domain:") {
            self.domainname = value.to_string();
        } else {
            return Err(Errno::EINVAL);
        }
        self.set_unshare_uts(true);
        Ok(())
    }

    fn handle_root_config(&mut self, command: &str) -> Result<(), Errno> {
        if let Some(root) = command.strip_prefix(':') {
            if !root.starts_with('/') {
                return Err(Errno::EINVAL);
            }
            let root = PathBuf::from(root);
            self.root = Some(root.clone());
            self.proc = Some(root.join("proc"));
            self.set_unshare_mount(true);
            self.set_unshare_pid(true);
        } else if let Some(value) = command.strip_prefix("/map:") {
            self.map_root = strbool(value)?;
        } else if let Some(value) = command.strip_prefix("/fake:") {
            self.fake_root = strbool(value)?;
        } else {
            return Err(Errno::EINVAL);
        }
        Ok(())
    }

    #[allow(clippy::cognitive_complexity)]
    fn handle_sandbox_config(&mut self, command: &str) -> Result<(), Errno> {
        let (action, cap) = match command.chars().last() {
            Some('?') => (&command[..command.len().saturating_sub(1)], Some("?")),
            _ => {
                let mut splits = command.splitn(2, ':');
                (splits.next().unwrap_or(""), splits.next())
            }
        };

        match (action, cap) {
            ("mem", Some("?")) => {
                if self.state.contains(Capability::CAP_MEM) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("mem", Some(state)) => {
                if strbool(state)? {
                    self.state.insert(Capability::CAP_MEM)
                } else {
                    self.state.remove(Capability::CAP_MEM)
                }
            }

            ("pid", Some("?")) => {
                if self.state.contains(Capability::CAP_PID) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("pid", Some(state)) => {
                if strbool(state)? {
                    self.state.insert(Capability::CAP_PID)
                } else {
                    self.state.remove(Capability::CAP_PID)
                }
            }

            ("lock", Some("?")) => {
                if self.state.contains(Capability::CAP_LOCK) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("lock", Some(state)) => {
                if strbool(state)? {
                    self.state.insert(Capability::CAP_LOCK)
                } else {
                    self.state.remove(Capability::CAP_LOCK)
                }
            }

            ("read", Some("?")) => {
                if self.state.contains(Capability::CAP_READ) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("read", Some(state)) => {
                if strbool(state)? {
                    self.state.insert(Capability::CAP_READ)
                } else {
                    self.state.remove(Capability::CAP_READ)
                }
            }

            ("stat", Some("?")) => {
                if self.state.contains(Capability::CAP_STAT) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("stat", Some(state)) => {
                if strbool(state)? {
                    self.state.insert(Capability::CAP_STAT)
                } else {
                    self.state.remove(Capability::CAP_STAT)
                }
            }

            ("write", Some("?")) => {
                if self.state.contains(Capability::CAP_WRITE) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("write", Some(state)) => {
                if strbool(state)? {
                    self.state.insert(Capability::CAP_WRITE)
                } else {
                    self.state.remove(Capability::CAP_WRITE)
                }
            }

            ("exec", Some("?")) => {
                if self.state.contains(Capability::CAP_EXEC) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("exec", Some(state)) => {
                if strbool(state)? {
                    self.state.insert(Capability::CAP_EXEC)
                } else {
                    self.state.remove(Capability::CAP_EXEC)
                }
            }

            ("net", Some("?")) => {
                if !self.state.contains(Capability::CAP_BIND)
                    && !self.state.contains(Capability::CAP_CONNECT)
                {
                    return Err(Errno::ENOENT);
                } else {
                    return Ok(());
                }
            }
            ("net", Some("bind")) => self.state.insert(Capability::CAP_BIND),
            ("net", Some("connect")) => self.state.insert(Capability::CAP_CONNECT),
            ("net", Some(state)) => {
                if strbool(state)? {
                    self.state.insert(Capability::CAP_BIND);
                    self.state.insert(Capability::CAP_CONNECT);
                } else {
                    self.state.remove(Capability::CAP_BIND);
                    self.state.remove(Capability::CAP_CONNECT);
                }
            }

            _ => return Err(Errno::EINVAL),
        }
        Ok(())
    }

    fn handle_sandbox_command(&mut self, command: &str) -> Result<(), Errno> {
        if let Some(command) = command.strip_prefix("exec!") {
            // Splitting the command using the Unit Separator character
            let parts: Vec<&str> = command.split('\x1F').collect();

            // Paranoid checks: Ensure the command and its arguments are not empty
            if parts.is_empty() || parts[0].is_empty() {
                error!("ctx": "cmd/exec", "errno": Errno::EINVAL as i32);
                return Err(Errno::EINVAL);
            }

            let program = parts[0];
            let args = &parts[1..];

            // SAFETY: We're spawning a child outside the sandbox here.
            // We should take some precautions so that the process to be
            // executed has a sane environment. That's why we change
            // the current directory to / and close the standard input.
            match Command::new(program)
                .args(args)
                .current_dir("/")
                .stdin(Stdio::null())
                .spawn()
            {
                Ok(_) => Ok(()),
                Err(error) => {
                    error!("ctx": "cmd/exec",
                        "cmd": program,
                        "args": format!("{args:?}"),
                        "error": error.to_string());
                    Err(Errno::from_i32(
                        error.raw_os_error().unwrap_or(nix::libc::EINVAL),
                    ))
                }
            }
        } else {
            Err(Errno::ENOENT)
        }
    }

    fn handle_mem_config(&mut self, command: &str) -> Result<(), Errno> {
        if let Some(value) = command.strip_prefix("kill:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_KILL_MEM);
            } else {
                self.flags.remove(Flag::FL_KILL_MEM);
            }
        } else if let Some(value) = command.strip_prefix("max:") {
            match parse_size::Config::new().with_binary().parse_size(value) {
                Ok(value) => {
                    self.mem_max = value;
                }
                Err(_) => {
                    return Err(Errno::EINVAL);
                }
            }
        } else if let Some(value) = command.strip_prefix("vm_max:") {
            match parse_size::Config::new().with_binary().parse_size(value) {
                Ok(value) => {
                    self.mem_vm_max = value;
                }
                Err(_) => {
                    return Err(Errno::EINVAL);
                }
            }
        }
        Ok(())
    }

    fn handle_pid_config(&mut self, command: &str) -> Result<(), Errno> {
        if let Some(value) = command.strip_prefix("kill:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_KILL_PID);
            } else {
                self.flags.remove(Flag::FL_KILL_PID);
            }
        } else if let Some(value) = command.strip_prefix("max:") {
            match value.parse::<usize>() {
                Ok(value) => {
                    self.pid_max = value;
                }
                Err(_) => {
                    return Err(Errno::EINVAL);
                }
            }
        }
        Ok(())
    }

    #[allow(clippy::cognitive_complexity)]
    fn handle_trace_config(&mut self, command: &str) -> Result<(), Errno> {
        if let Some(value) = command.strip_prefix("exit_wait_all:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_EXIT_WAIT_ALL);
            } else {
                self.flags.remove(Flag::FL_EXIT_WAIT_ALL);
            }
        } else if let Some(value) = command.strip_prefix("private_shm:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_PRIVATE_SHM);
            } else {
                self.flags.remove(Flag::FL_PRIVATE_SHM);
            }
        } else if let Some(value) = command.strip_prefix("private_tmp:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_PRIVATE_TMP);
            } else {
                self.flags.remove(Flag::FL_PRIVATE_TMP);
            }
        } else if let Some(value) = command.strip_prefix("allow_unsafe_perf:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_ALLOW_UNSAFE_PERF);
            } else {
                self.flags.remove(Flag::FL_ALLOW_UNSAFE_PERF);
            }
        } else if let Some(value) = command.strip_prefix("allow_unsafe_ptrace:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_ALLOW_UNSAFE_PTRACE);
            } else {
                self.flags.remove(Flag::FL_ALLOW_UNSAFE_PTRACE);
            }
        } else if let Some(value) = command.strip_prefix("allow_unsafe_caps:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_ALLOW_UNSAFE_CAPS);
            } else {
                self.flags.remove(Flag::FL_ALLOW_UNSAFE_CAPS);
            }
        } else if let Some(value) = command.strip_prefix("allow_unsafe_env:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_ALLOW_UNSAFE_ENV);
            } else {
                self.flags.remove(Flag::FL_ALLOW_UNSAFE_ENV);
            }
        } else if let Some(value) = command.strip_prefix("allow_safe_bind:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_ALLOW_SAFE_BIND);
            } else {
                self.flags.remove(Flag::FL_ALLOW_SAFE_BIND);
            }
        } else if let Some(value) = command.strip_prefix("allow_unsupp_socket:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_ALLOW_UNSUPP_SOCKET);
            } else {
                self.flags.remove(Flag::FL_ALLOW_UNSUPP_SOCKET);
            }
        } else if let Some(value) = command.strip_prefix("allow_unsafe_socket:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_ALLOW_UNSAFE_SOCKET);
            } else {
                self.flags.remove(Flag::FL_ALLOW_UNSAFE_SOCKET);
            }
        } else if let Some(value) = command.strip_prefix("allow_unsafe_ioctl:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_ALLOW_UNSAFE_IOCTL);
            } else {
                self.flags.remove(Flag::FL_ALLOW_UNSAFE_IOCTL);
            }
        } else if let Some(value) = command.strip_prefix("allow_unsafe_prctl:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_ALLOW_UNSAFE_PRCTL);
            } else {
                self.flags.remove(Flag::FL_ALLOW_UNSAFE_PRCTL);
            }
        } else if let Some(value) = command.strip_prefix("allow_unsafe_prlimit:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_ALLOW_UNSAFE_PRLIMIT);
            } else {
                self.flags.remove(Flag::FL_ALLOW_UNSAFE_PRLIMIT);
            }
        } else if let Some(value) = command.strip_prefix("allow_unsafe_adjtime:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_ALLOW_UNSAFE_ADJTIME);
            } else {
                self.flags.remove(Flag::FL_ALLOW_UNSAFE_ADJTIME);
            }
        } else if let Some(value) = command.strip_prefix("allow_unsafe_uring:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_ALLOW_UNSAFE_IOURING);
            } else {
                self.flags.remove(Flag::FL_ALLOW_UNSAFE_IOURING);
            }
        } else if let Some(value) = command.strip_prefix("deny_tsc:") {
            if strbool(value)? {
                self.flags.insert(Flag::FL_DENY_TSC);
            } else {
                self.flags.remove(Flag::FL_DENY_TSC);
            }
        } else if let Some(value) = command.strip_prefix("memory_access:") {
            match value {
                "0" => {
                    env::remove_var(ENV_NO_CROSS_MEMORY_ATTACH);
                }
                "1" => {
                    env::set_var(ENV_NO_CROSS_MEMORY_ATTACH, "1");
                }
                _ => return Err(Errno::EINVAL),
            }
        } else {
            return Err(Errno::EINVAL);
        }
        Ok(())
    }

    fn handle_unshare_config(&mut self, command: &str) -> Result<(), Errno> {
        let (action, cap) = match command.chars().last() {
            Some('?') => (&command[..command.len().saturating_sub(1)], Some("?")),
            _ => {
                let mut splits = command.splitn(2, ':');
                (splits.next().unwrap_or(""), splits.next())
            }
        };

        match (action, cap) {
            ("mount", Some("?")) => {
                if self.flags.contains(Flag::FL_UNSHARE_MOUNT) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("mount", Some(state)) => {
                if strbool(state)? {
                    self.flags.insert(Flag::FL_UNSHARE_MOUNT);
                } else {
                    self.flags.remove(Flag::FL_UNSHARE_MOUNT);
                }
            }

            ("uts", Some("?")) => {
                if self.flags.contains(Flag::FL_UNSHARE_UTS) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("uts", Some(state)) => {
                if strbool(state)? {
                    self.flags.insert(Flag::FL_UNSHARE_UTS);
                } else {
                    self.flags.remove(Flag::FL_UNSHARE_UTS);
                }
            }

            ("ipc", Some("?")) => {
                if self.flags.contains(Flag::FL_UNSHARE_IPC) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("ipc", Some(state)) => {
                if strbool(state)? {
                    self.flags.insert(Flag::FL_UNSHARE_IPC);
                } else {
                    self.flags.remove(Flag::FL_UNSHARE_IPC);
                }
            }

            ("user", Some("?")) => {
                if self.flags.contains(Flag::FL_UNSHARE_USER) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("user", Some(state)) => {
                if strbool(state)? {
                    self.flags.insert(Flag::FL_UNSHARE_USER);
                } else {
                    self.flags.remove(Flag::FL_UNSHARE_USER);
                }
            }

            ("pid", Some("?")) => {
                if self.flags.contains(Flag::FL_UNSHARE_PID) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("pid", Some(state)) => {
                if strbool(state)? {
                    self.flags.insert(Flag::FL_UNSHARE_PID);
                } else {
                    self.flags.remove(Flag::FL_UNSHARE_PID);
                }
            }

            ("net", Some("?")) => {
                if self.flags.contains(Flag::FL_UNSHARE_NET) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("net", Some(state)) => {
                if strbool(state)? {
                    self.flags.insert(Flag::FL_UNSHARE_NET);
                } else {
                    self.flags.remove(Flag::FL_UNSHARE_NET);
                }
            }

            ("cgroup", Some("?")) => {
                if self.flags.contains(Flag::FL_UNSHARE_CGROUP) {
                    return Ok(());
                } else {
                    return Err(Errno::ENOENT);
                }
            }
            ("cgroup", Some(state)) => {
                if strbool(state)? {
                    self.flags.insert(Flag::FL_UNSHARE_CGROUP);
                } else {
                    self.flags.remove(Flag::FL_UNSHARE_CGROUP);
                }
            }

            _ => return Err(Errno::EINVAL),
        }
        Ok(())
    }

    fn handle_bind_config(&mut self, captures: &Captures) -> Result<(), Errno> {
        let op = &captures["mod"];
        let mount = BindMount::from(captures);

        match op {
            "+" => self.add_bind_mount(mount),
            "-" => self.del_bind_mount(mount),
            "^" => self.rem_bind_mount(mount),
            _ => return Err(Errno::EINVAL),
        };

        Ok(())
    }

    fn handle_rule_config(&mut self, captures: &Captures) -> Result<(), Errno> {
        let act = Action::from(captures);
        let cap = Capability::from(captures);
        let op = &captures["mod"];
        let pat = &captures["pat"];

        match cap {
            Capability::CAP_MEM => {
                if op != ":" {
                    return Err(Errno::EINVAL);
                }
                self.mem_filter = strbool(pat)?;
                return Ok(());
            }
            Capability::CAP_PID => {
                if op != ":" {
                    return Err(Errno::EINVAL);
                }
                self.pid_filter = strbool(pat)?;
                return Ok(());
            }
            _ => {}
        }

        let ip =
            !pat.starts_with('/') && cap.intersects(Capability::CAP_BIND | Capability::CAP_CONNECT);
        match op {
            "+" => {
                // add rule
                if ip {
                    self.rule_add_cidr(act, cap, pat.as_ref())
                } else {
                    self.rule_add_glob(act, cap, pat.as_ref())
                }
            }
            "-" => {
                // remove rule
                if ip {
                    self.rule_del_cidr(act, cap, pat.as_ref())
                } else {
                    self.rule_del_glob(act, cap, pat.as_ref())
                }
            }
            "^" => {
                // remove all matching rules
                if ip {
                    self.rule_rem_cidr(act, cap, pat.as_ref())
                } else {
                    self.rule_rem_glob(act, cap, pat.as_ref())
                }
            }
            _ => Err(Errno::EINVAL),
        }
    }

    /// Remove CIDR with port range, removes all matching instances.
    pub fn rule_rem_cidr(&mut self, act: Action, cap: Capability, pat: &str) -> Result<(), Errno> {
        let mut split = pat.splitn(2, ['!', '@']);
        if let (Some(addr), Some(port)) = (split.next(), split.next()) {
            let mut split = port.splitn(2, '-');
            if let Some(port0) = split.next() {
                if let Ok(port0) = port0.parse::<u16>() {
                    let port1 = if let Some(port1) = split.next() {
                        port1.parse::<u16>()
                    } else {
                        Ok(port0)
                    };
                    if let Ok(port1) = port1 {
                        if let Ok(addr) = IpNetwork::from_str(addr) {
                            self.cidr_rules.retain(|rule| {
                                !(act == rule.act
                                    && rule.cap.contains(cap)
                                    && port0 == rule.pat.port[0]
                                    && port1 == rule.pat.port[1]
                                    && addr == rule.pat.addr)
                            });
                            return Ok(());
                        }
                    }
                }
            }
        }
        Err(Errno::ENOENT)
    }

    /// Remove CIDR with port range, removes the first instance from the end for predictability.
    pub fn rule_del_cidr(&mut self, act: Action, cap: Capability, pat: &str) -> Result<(), Errno> {
        let mut split = pat.splitn(2, ['!', '@']);
        if let (Some(addr), Some(port)) = (split.next(), split.next()) {
            let mut split = port.splitn(2, '-');
            if let Some(port0) = split.next() {
                if let Ok(port0) = port0.parse::<u16>() {
                    let port1 = if let Some(port1) = split.next() {
                        port1.parse::<u16>()
                    } else {
                        Ok(port0)
                    };
                    if let Ok(port1) = port1 {
                        if let Ok(addr) = IpNetwork::from_str(addr) {
                            if let Some((index, _)) =
                                self.cidr_rules.iter().enumerate().rev().find(|(_, rule)| {
                                    act == rule.act
                                        && rule.cap.contains(cap)
                                        && port0 == rule.pat.port[0]
                                        && port1 == rule.pat.port[1]
                                        && addr == rule.pat.addr
                                })
                            {
                                self.cidr_rules.remove(index);
                            }
                            return Ok(());
                        }
                    }
                }
            }
        }
        Err(Errno::ENOENT)
    }

    /// Add CIDR with port range
    /// The rule is either a Unix shell style pattern, or
    /// a network address, one of the following formats:
    ///
    /// 1. GLOB-PATTERN
    /// 2. IP/NETMASK!$PORT
    ///
    /// - GLOB-PATTERN must start with a slash, `/`.
    /// - /NETMASK may be omitted.
    /// - PORT is a single integer or two in format port1-port2
    pub fn rule_add_cidr(&mut self, act: Action, cap: Capability, pat: &str) -> Result<(), Errno> {
        let mut split = pat.splitn(2, ['!', '@']);
        if let (Some(addr), Some(port)) = (split.next(), split.next()) {
            let mut split = port.splitn(2, '-');
            if let Some(port0) = split.next() {
                if let Ok(port0) = port0.parse::<u16>() {
                    let port1 = if let Some(port1) = split.next() {
                        port1.parse::<u16>()
                    } else {
                        Ok(port0)
                    };
                    if let Ok(port1) = port1 {
                        if let Ok(addr) = IpNetwork::from_str(addr) {
                            self.cidr_rules.push(CidrRule {
                                act,
                                cap,
                                pat: AddressPattern {
                                    addr,
                                    port: [port0, port1],
                                },
                            });
                            return Ok(());
                        }
                    }
                }
            }
        }
        Err(Errno::EINVAL)
    }

    /// Remove Unix shell style pattern, removes all matching instances.
    pub fn rule_rem_glob(&mut self, act: Action, cap: Capability, pat: &str) -> Result<(), Errno> {
        // Expand foo/*** to [foo, foo/**]
        if let Some(pat) = pat.strip_suffix("/***") {
            let pat_doublestar = format!("{pat}/**");
            self.rule_rem_glob(act, cap, if pat.is_empty() { "/" } else { pat })?;
            self.rule_rem_glob(act, cap, &pat_doublestar)?;
            return Ok(());
        }

        self.glob_rules
            .retain(|rule| !(act == rule.act && rule.cap.contains(cap) && pat == rule.pat.glob()));
        self.del_glob(act, cap); // Rebuilds the whole GlobBuilder.

        Ok(())
    }

    /// Remove Unix shell style pattern, removes the first instance from the end for predictability.
    pub fn rule_del_glob(&mut self, act: Action, cap: Capability, pat: &str) -> Result<(), Errno> {
        // Expand foo/*** to [foo, foo/**]
        if let Some(pat) = pat.strip_suffix("/***") {
            let pat_doublestar = format!("{pat}/**");
            self.rule_del_glob(act, cap, if pat.is_empty() { "/" } else { pat })?;
            self.rule_del_glob(act, cap, &pat_doublestar)?;
            return Ok(());
        }

        for &capability in &Capability::GLOB {
            if cap.contains(capability) {
                if let Some((index, _)) =
                    self.glob_rules.iter().enumerate().rev().find(|(_, rule)| {
                        act == rule.act && capability == rule.cap && pat == rule.pat.glob()
                    })
                {
                    self.glob_rules.remove(index);
                    self.del_glob(act, capability); // Rebuilds the whole GlobBuilder.
                }
            }
        }

        Ok(())
    }

    /// Add Unix shell style pattern.
    pub fn rule_add_glob(&mut self, act: Action, cap: Capability, pat: &str) -> Result<(), Errno> {
        // Landlock rules are not glob patterns but path beneath rules.
        if !cap.contains(Capability::CAP_LOCK) {
            // Expand foo/*** to [foo, foo/**]
            if let Some(pat) = pat.strip_suffix("/***") {
                let pat_doublestar = format!("{pat}/**");
                self.rule_add_glob(act, cap, if pat.is_empty() { "/" } else { pat })?;
                self.rule_add_glob(act, cap, &pat_doublestar)?;
                return Ok(());
            }
        }

        let pat = GlobBuilder::new(pat)
            .empty_alternates(true)
            .build()
            .map_err(|_| Errno::EINVAL)?;
        for &capability in &Capability::GLOB {
            if cap.contains(capability) {
                let pat = pat.clone();
                self.add_glob(act, capability, &pat);
                self.glob_rules.push(GlobRule {
                    act,
                    cap: capability,
                    pat,
                });
            }
        }

        Ok(())
    }

    /// Extract the Landlock read-only and read-write path lists.
    /// Returns None if Landlock sandboxing is disabled.
    pub fn collect_landlock(&mut self) -> Option<LandlockPathPair> {
        if !self.landlocked() {
            return None;
        }
        // Use HashSets to avoid duplicate paths.
        let mut path_ro = HashSet::new();
        let mut path_rw = HashSet::new();
        self.glob_rules
            .retain(|rule| match (rule.act, rule.cap, &rule.pat) {
                (Action::Allow, Capability::CAP_LOCK_RO, pat) => {
                    path_ro.insert(pat.glob().to_string());
                    false
                }
                (Action::Allow, Capability::CAP_LOCK_RW, pat) => {
                    path_rw.insert(pat.glob().to_string());
                    false
                }
                _ => true,
            });

        Some((
            path_ro.iter().cloned().collect(),
            path_rw.iter().cloned().collect(),
        ))
    }

    /// Check if the given path is hidden (ie denylisted for stat sandboxing)
    pub fn is_hidden<P: AsRef<Path>>(&self, path: P) -> bool {
        self.enabled(Capability::CAP_STAT)
            && self.check_path(Capability::CAP_STAT, &path) != Action::Allow
    }

    /// Check IPv{4,6} address for access.
    pub(crate) fn check_ip(&self, cap: Capability, addr: IpAddr, port: u16) -> Action {
        for rule in self.cidr_rules.iter().rev() {
            // rev() because last matching rule wins.
            if cap != rule.cap {
                continue;
            }
            if rule.act != Action::Allow && rule.act != Action::Deny {
                continue;
            }
            let port_match = if rule.pat.port[0] == rule.pat.port[1] {
                port == rule.pat.port[0]
            } else {
                port >= rule.pat.port[0] && port <= rule.pat.port[1]
            };
            if port_match && rule.pat.addr.contains(addr) {
                if rule.act == Action::Allow {
                    trace!("ctx": "check_ip", "rule": rule, "cap": cap, "addr": format!("{addr}!{port}"));
                    return Action::Allow;
                } else if self.filter_ip(cap, &addr, port) {
                    // Check filter to determine whether violation is to be reported.
                    return Action::Filter;
                } else {
                    trace!("ctx": "check_ip", "rule": rule, "cap": cap, "addr": format!("{addr}!{port}"));
                    return Action::Deny;
                }
            }
        }

        // If no specific rule is found, return based on capability being enabled or not.
        if self.enabled(cap) {
            if self.filter_ip(cap, &addr, port) {
                trace!("ctx": "check", "act": "filter", "cap": cap, "addr": format!("{addr}!{port}"));
                Action::Filter
            } else {
                trace!("ctx": "check_ip", "act": "deny", "cap": cap, "addr": format!("{addr}!{port}"));
                Action::Deny
            }
        } else {
            trace!("ctx": "check_ip", "act": "allow", "cap": cap, "addr": format!("{addr}!{port}"));
            Action::Allow
        }
    }

    /// Check UNIX socket for access.
    pub(crate) fn check_unix<P: AsRef<Path>>(&self, cap: Capability, path: P) -> Action {
        // First, see if there's a matching allow or deny rule for the path.
        if let Some(action) = self.match_action(cap, &path) {
            if action == Action::Allow {
                return Action::Allow;
            }

            // If the action is Deny, then we must check if it's filtered.
            if self.filter_path(cap, &path) {
                trace!("ctx": "check_unix", "act": "filter", "cap": cap, "path": format!("{}", path.as_ref().display()));
                return Action::Filter;
            }
            trace!("ctx": "check_unix", "act": "deny", "cap": cap, "path": format!("{}", path.as_ref().display()));
            return Action::Deny;
        }

        // If no specific rule is found, return based on capability being enabled or not.
        if self.enabled(cap) {
            if self.filter_path(cap, &path) {
                trace!("ctx": "check_unix", "act": "filter", "cap": cap, "path": format!("{}", path.as_ref().display()));
                Action::Filter
            } else {
                trace!("ctx": "check_unix", "act": "deny", "cap": cap, "path": format!("{}", path.as_ref().display()));
                Action::Deny
            }
        } else {
            trace!("ctx": "check_unix", "act": "allow", "cap": cap, "path": format!("{}", path.as_ref().display()));
            Action::Allow
        }
    }

    /// Check path for access.
    pub(crate) fn check_path<P: AsRef<Path>>(&self, cap: Capability, path: P) -> Action {
        // Drop trailing slash which can cause inconsistencies with expectations.
        let path_bytes = path.as_ref().as_os_str().as_bytes();
        #[allow(clippy::arithmetic_side_effects)]
        let path = if path_bytes.ends_with(&[b'/']) && path_bytes.len() > 1 {
            // SAFETY: Since we're operating on valid path bytes, getting a slice is safe.
            // This excludes the root path "/" to avoid turning it into an empty path.
            PathBuf::from(OsStr::from_bytes(&path_bytes[..path_bytes.len() - 1]))
        } else {
            path.as_ref().to_path_buf()
        };

        // First, see if there's a matching allow or deny rule for the path.
        if let Some(action) = self.match_action(cap, &path) {
            if action == Action::Allow {
                return Action::Allow;
            }

            // If the action is Deny, then we must check if it's filtered.
            if self.filter_path(cap, &path) {
                trace!("ctx": "check_path", "act": "filter", "cap": cap, "path": format!("{}", path.display()));
                return Action::Filter;
            }
            trace!("ctx": "check_path", "act": "deny", "cap": cap, "path": format!("{}", path.display()));
            return Action::Deny;
        }

        // If no specific rule is found, return based on capability being enabled or not.
        if self.enabled(cap) {
            if self.filter_path(cap, &path) {
                trace!("ctx": "check_path", "act": "filter", "cap": cap, "path": format!("{}", path.display()));
                Action::Filter
            } else {
                trace!("ctx": "check_path", "act": "deny", "cap": cap, "path": format!("{}", path.display()));
                Action::Deny
            }
        } else {
            trace!("ctx": "check_path", "act": "allow", "cap": cap, "path": format!("{}", path.display()));
            Action::Allow
        }
    }

    /// Check exec for kill.
    pub(crate) fn check_exec<P: AsRef<Path>>(&self, path: P) -> Action {
        if self.globset_kill_x.0.is_match(path) {
            Action::Kill
        } else {
            Action::Allow
        }
    }

    /// Find a matching action (Allow or Deny) for the given path.
    pub fn match_action<P: AsRef<Path>>(&self, cap: Capability, path: P) -> Option<Action> {
        if let Some((set, _, ready)) = self.get_globset(Action::Deny, cap) {
            // matches is a Vec<usize> which has a
            // sequence number of every matching pattern
            // and last matching patterns wins.
            assert!(ready, "match_action called before building globsets!");
            set.matches(&path).last().map(|idx| {
                self.glob_rules
                    .iter()
                    .filter(|rule| {
                        cap == rule.cap && (rule.act == Action::Allow || rule.act == Action::Deny)
                    })
                    .nth(*idx)
                    .map(|rule| {
                        trace!("ctx": "check_path", "cap": cap, "rule": rule, "path": format!("{}", path.as_ref().display()));
                        rule.act
                    })
                    .unwrap_or(Action::Deny)
            })
        } else {
            None
        }
    }

    /// Check if the ip address with the given port is filtered.
    fn filter_ip(&self, cap: Capability, addr: &IpAddr, port: u16) -> bool {
        self.cidr_rules
            .iter()
            .filter(|filter| filter.act == Action::Filter && filter.cap == cap)
            .any(|filter| {
                let port_match = if filter.pat.port[0] == filter.pat.port[1] {
                    port == filter.pat.port[0]
                } else {
                    port >= filter.pat.port[0] && port <= filter.pat.port[1]
                };
                #[allow(clippy::needless_bool)]
                if port_match && filter.pat.addr.contains(*addr) {
                    trace!("ctx": "check_ip", "cap": cap, "rule": filter, "addr": format!("{addr}!{port}"));
                    true
                } else {
                    false
                }
            })
    }

    /// Check if the path is filtered.
    fn filter_path<P: AsRef<Path>>(&self, cap: Capability, path: P) -> bool {
        let path_ref = path.as_ref();

        if cap.contains(Capability::CAP_READ) && self.globset_filter_r.0.is_match(path_ref) {
            return true;
        }
        if cap.contains(Capability::CAP_WRITE) && self.globset_filter_w.0.is_match(path_ref) {
            return true;
        }
        if cap.contains(Capability::CAP_EXEC) && self.globset_filter_x.0.is_match(path_ref) {
            return true;
        }
        if cap.contains(Capability::CAP_BIND) && self.globset_filter_nb.0.is_match(path_ref) {
            return true;
        }
        if cap.contains(Capability::CAP_CONNECT) && self.globset_filter_nc.0.is_match(path_ref) {
            return true;
        }

        false
    }

    /// Build `GlobSet`s from `GlobRule`s.
    /// This function must be called once before check functions.
    pub fn build_globsets(&mut self) -> Result<(), globset::Error> {
        fn build_globset(globset_data: &mut SandboxGlobSet) -> Result<(), globset::Error> {
            if !globset_data.2 {
                globset_data.0 = globset_data.1.clone().build()?;
                globset_data.2 = true;
            }
            Ok(())
        }

        build_globset(&mut self.globset_access_r)?;
        build_globset(&mut self.globset_access_s)?;
        build_globset(&mut self.globset_access_w)?;
        build_globset(&mut self.globset_access_x)?;
        build_globset(&mut self.globset_access_nb)?;
        build_globset(&mut self.globset_access_nc)?;
        build_globset(&mut self.globset_filter_r)?;
        build_globset(&mut self.globset_filter_s)?;
        build_globset(&mut self.globset_filter_w)?;
        build_globset(&mut self.globset_filter_x)?;
        build_globset(&mut self.globset_filter_nb)?;
        build_globset(&mut self.globset_filter_nc)?;
        build_globset(&mut self.globset_kill_x)?;

        Ok(())
    }

    fn add_glob(&mut self, act: Action, cap: Capability, glob: &Glob) {
        if let Some(set) = self.get_globset_mut(act, cap) {
            set.1.add(glob.clone());
            set.2 = false;
        }
    }

    fn del_glob(&mut self, act: Action, cap: Capability) {
        // Temporarily take the rules out
        let rules = std::mem::take(&mut self.glob_rules);

        if let Some(set) = self.get_globset_mut(act, cap) {
            let mut glob_set = GlobSetBuilder::new();
            for rule in rules.iter().filter(|rule| {
                if !rule.cap.contains(cap) {
                    false
                } else {
                    match rule.act {
                        Action::Filter => act == Action::Filter,
                        Action::Kill => act == Action::Kill,
                        _ => act == Action::Allow || act == Action::Deny,
                    }
                }
            }) {
                glob_set.add(rule.pat.clone());
            }

            set.1 = glob_set;
            set.2 = false;
        }

        // Swap the rules back in
        self.glob_rules = rules;
    }

    fn get_globset(&self, act: Action, cap: Capability) -> Option<&SandboxGlobSet> {
        Some(match (cap, act) {
            (Capability::CAP_READ, Action::Filter) => &self.globset_filter_r,
            (Capability::CAP_READ, _) => &self.globset_access_r,
            (Capability::CAP_STAT, Action::Filter) => &self.globset_filter_s,
            (Capability::CAP_STAT, _) => &self.globset_access_s,
            (Capability::CAP_WRITE, Action::Filter) => &self.globset_filter_w,
            (Capability::CAP_WRITE, _) => &self.globset_access_w,
            (Capability::CAP_EXEC, Action::Filter) => &self.globset_filter_x,
            (Capability::CAP_EXEC, Action::Kill) => &self.globset_kill_x,
            (Capability::CAP_EXEC, _) => &self.globset_access_x,
            (Capability::CAP_BIND, Action::Filter) => &self.globset_filter_nb,
            (Capability::CAP_BIND, _) => &self.globset_access_nb,
            (Capability::CAP_CONNECT, Action::Filter) => &self.globset_filter_nc,
            (Capability::CAP_CONNECT, _) => &self.globset_access_nc,
            _ => {
                /* Landlock et al. have no GlobSet. */
                return None;
            }
        })
    }

    fn get_globset_mut(&mut self, act: Action, cap: Capability) -> Option<&mut SandboxGlobSet> {
        Some(match (cap, act) {
            (Capability::CAP_READ, Action::Filter) => &mut self.globset_filter_r,
            (Capability::CAP_READ, _) => &mut self.globset_access_r,
            (Capability::CAP_STAT, Action::Filter) => &mut self.globset_filter_s,
            (Capability::CAP_STAT, _) => &mut self.globset_access_s,
            (Capability::CAP_WRITE, Action::Filter) => &mut self.globset_filter_w,
            (Capability::CAP_WRITE, _) => &mut self.globset_access_w,
            (Capability::CAP_EXEC, Action::Filter) => &mut self.globset_filter_x,
            (Capability::CAP_EXEC, Action::Kill) => &mut self.globset_kill_x,
            (Capability::CAP_EXEC, _) => &mut self.globset_access_x,
            (Capability::CAP_BIND, Action::Filter) => &mut self.globset_filter_nb,
            (Capability::CAP_BIND, _) => &mut self.globset_access_nb,
            (Capability::CAP_CONNECT, Action::Filter) => &mut self.globset_filter_nc,
            (Capability::CAP_CONNECT, _) => &mut self.globset_access_nc,
            _ => {
                /* Landlock et al. have no GlobSet. */
                return None;
            }
        })
    }

    /// Check if there are any patterns in `exec/kill` list.
    pub(crate) fn has_exec_kill(&self) -> bool {
        !self.globset_kill_x.0.is_empty()
    }

    /// Get the process ID of the syd execve child.
    pub fn get_child_pid(&self) -> Pid {
        Pid::from_raw(self.cpid)
    }

    /// Set the process ID of the syd execve child.
    pub fn set_child_pid(&mut self, pid: Pid) {
        self.cpid = pid.as_raw();
    }

    /// Get fake root
    pub fn get_fake_root(&self) -> bool {
        self.fake_root
    }

    /// If fake root is set, all id system calls return 0 in the sandbox.
    pub fn set_fake_root(&mut self, on: bool) {
        self.fake_root = on
    }

    /// Return true if the sandboxing is enabled for the given capability.
    pub fn enabled(&self, cap: Capability) -> bool {
        self.state.contains(cap)
    }

    /// Lock sandbox.
    pub fn lock(&mut self) {
        self.lock = LockState::Set
    }

    /// Returns true if the sandbox is locked.
    pub fn locked(&self) -> bool {
        self.lock == LockState::Set
    }

    /// Returns true if the sandbox is locked for the given process ID.
    pub fn locked_for_pid(&self, pid: nix::libc::pid_t) -> bool {
        match self.lock {
            LockState::Set => true,
            LockState::Exec => self.cpid != 0 && pid != self.cpid,
            _ => false,
        }
    }

    /// Returns true if Landlock sandboxing is enabled.
    pub fn landlocked(&self) -> bool {
        self.state.contains(Capability::CAP_LOCK)
    }

    /// Get the `Namespace` settings of the Sandbox.
    pub fn namespaces(&self) -> Vec<Namespace> {
        let mut namespaces = Vec::new();

        if self.flags.contains(Flag::FL_UNSHARE_MOUNT) {
            namespaces.push(Namespace::Mount);
        }
        if self.flags.contains(Flag::FL_UNSHARE_UTS) {
            namespaces.push(Namespace::Uts);
        }
        if self.flags.contains(Flag::FL_UNSHARE_IPC) {
            namespaces.push(Namespace::Ipc);
        }
        if self.flags.contains(Flag::FL_UNSHARE_USER) {
            namespaces.push(Namespace::User);
        }
        if self.flags.contains(Flag::FL_UNSHARE_PID) {
            namespaces.push(Namespace::Pid);
        }
        if self.flags.contains(Flag::FL_UNSHARE_NET) {
            namespaces.push(Namespace::Net);
        }
        if self.flags.contains(Flag::FL_UNSHARE_CGROUP) {
            namespaces.push(Namespace::Cgroup);
        }

        namespaces
    }

    /// Get the value of the unshare-mount flag.
    pub fn unshare_mount(&self) -> bool {
        self.flags.contains(Flag::FL_UNSHARE_MOUNT)
    }

    /// Set the value of the unshare-mount flag.
    pub fn set_unshare_mount(&mut self, state: bool) {
        if state {
            self.flags.insert(Flag::FL_UNSHARE_MOUNT);
        } else {
            self.flags.remove(Flag::FL_UNSHARE_MOUNT);
        }
    }

    /// Get the value of the unshare-uts flag.
    pub fn unshare_uts(&self) -> bool {
        self.flags.contains(Flag::FL_UNSHARE_UTS)
    }

    /// Set the value of the unshare-uts flag.
    pub fn set_unshare_uts(&mut self, state: bool) {
        if state {
            self.flags.insert(Flag::FL_UNSHARE_UTS);
        } else {
            self.flags.remove(Flag::FL_UNSHARE_UTS);
        }
    }

    /// Get the value of the unshare-ipc flag.
    pub fn unshare_ipc(&self) -> bool {
        self.flags.contains(Flag::FL_UNSHARE_IPC)
    }

    /// Set the value of the unshare-ipc flag.
    pub fn set_unshare_ipc(&mut self, state: bool) {
        if state {
            self.flags.insert(Flag::FL_UNSHARE_IPC);
        } else {
            self.flags.remove(Flag::FL_UNSHARE_IPC);
        }
    }

    /// Get the value of the unshare-user flag.
    pub fn unshare_user(&self) -> bool {
        self.flags.contains(Flag::FL_UNSHARE_USER)
    }

    /// Set the value of the unshare-user flag.
    pub fn set_unshare_user(&mut self, state: bool) {
        if state {
            self.flags.insert(Flag::FL_UNSHARE_USER);
        } else {
            self.flags.remove(Flag::FL_UNSHARE_USER);
        }
    }

    /// Get the value of the unshare-pid flag.
    pub fn unshare_pid(&self) -> bool {
        self.flags.contains(Flag::FL_UNSHARE_PID)
    }

    /// Set the value of the unshare-pid flag.
    pub fn set_unshare_pid(&mut self, state: bool) {
        if state {
            self.flags.insert(Flag::FL_UNSHARE_PID);
        } else {
            self.flags.remove(Flag::FL_UNSHARE_PID);
        }
    }

    /// Get the value of the unshare-net flag.
    pub fn unshare_net(&self) -> bool {
        self.flags.contains(Flag::FL_UNSHARE_NET)
    }

    /// Set the value of the unshare-net flag.
    pub fn set_unshare_net(&mut self, state: bool) {
        if state {
            self.flags.insert(Flag::FL_UNSHARE_NET);
        } else {
            self.flags.remove(Flag::FL_UNSHARE_NET);
        }
    }

    /// Get the value of the unshare-cgroup flag.
    pub fn unshare_cgroup(&self) -> bool {
        self.flags.contains(Flag::FL_UNSHARE_CGROUP)
    }

    /// Set the value of the unshare-cgroup flag.
    pub fn set_unshare_cgroup(&mut self, state: bool) {
        if state {
            self.flags.insert(Flag::FL_UNSHARE_CGROUP);
        } else {
            self.flags.remove(Flag::FL_UNSHARE_CGROUP);
        }
    }

    /// Get the value of the private-shm flag.
    pub fn private_shm(&self) -> bool {
        self.flags.contains(Flag::FL_PRIVATE_SHM)
    }

    /// Get the value of the private-tmp flag.
    pub fn private_tmp(&self) -> bool {
        self.flags.contains(Flag::FL_PRIVATE_TMP)
    }

    /// Returns true if exit wait flag is set.
    pub fn exit_wait_all(&self) -> bool {
        self.flags.contains(Flag::FL_EXIT_WAIT_ALL)
    }

    /// Returns true if kill mem flag is set.
    pub fn kill_mem(&self) -> bool {
        self.flags.contains(Flag::FL_KILL_MEM)
    }

    /// Returns true if kill pid flag is set.
    pub fn kill_pid(&self) -> bool {
        self.flags.contains(Flag::FL_KILL_PID)
    }

    /// Returns true if unsafe perf flag is set.
    pub fn allow_unsafe_perf(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_UNSAFE_PERF)
    }

    /// Returns true if unsafe ptrace flag is set.
    pub fn allow_unsafe_ptrace(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_UNSAFE_PTRACE)
    }

    /// Returns true if unsafe capabilities flag is set.
    pub fn allow_unsafe_caps(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_UNSAFE_CAPS)
    }

    /// Returns true if unsafe environment flag is set.
    pub fn allow_unsafe_env(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_UNSAFE_ENV)
    }

    /// Returns true if successful bind addresses should be allowed for subsequent connect calls.
    pub fn allow_safe_bind(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_SAFE_BIND)
    }

    /// Returns true if unsupported socket families should be allowed.
    pub fn allow_unsupp_socket(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_UNSUPP_SOCKET)
    }

    /// Returns true if unsupported socket families should be allowed.
    pub fn allow_unsafe_socket(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_UNSAFE_SOCKET)
    }

    /// Returns true if unsafe ioctl calls are allowed.
    pub fn allow_unsafe_ioctl(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_UNSAFE_IOCTL)
    }

    /// Returns true if unsafe prctl calls are allowed.
    pub fn allow_unsafe_prctl(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_UNSAFE_PRCTL)
    }

    /// Returns true if unsafe prlimit calls are allowed.
    pub fn allow_unsafe_prlimit(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_UNSAFE_PRLIMIT)
    }

    /// Returns true if unsafe adjtime calls are allowed.
    pub fn allow_unsafe_adjtime(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_UNSAFE_ADJTIME)
    }

    /// Returns true if unsafe io_uring calls are allowed.
    pub fn allow_unsafe_iouring(&self) -> bool {
        self.flags.contains(Flag::FL_ALLOW_UNSAFE_IOURING)
    }

    /// Returns true if reading the timestamp counter is denied (x86 only).
    pub fn deny_tsc(&self) -> bool {
        self.flags.contains(Flag::FL_DENY_TSC)
    }

    /// Returns true if sandbox is in trace, aka "dry run" mode
    pub fn trace(&self) -> bool {
        self.flags.contains(Flag::FL_TRACE)
    }

    /// Set trace mode on or off.
    pub fn set_trace(&mut self, state: bool) {
        if state {
            self.flags.insert(Flag::FL_TRACE);
        } else {
            self.flags.remove(Flag::FL_TRACE);
        }
    }

    /// Add a bind mount action to the list of mount actions.
    pub fn add_bind_mount(&mut self, mount: BindMount) {
        if let Some(ref mut mounts) = self.bind_mounts {
            mounts.push(mount);
        } else {
            self.bind_mounts = Some(vec![mount]);
        }
    }

    /// Remove the first matching item from the end of the list of mount actions.
    pub fn del_bind_mount(&mut self, mount: BindMount) {
        if let Some(ref mut mounts) = self.bind_mounts {
            if let Some(pos) = mounts.iter().rposition(|m| m == &mount) {
                mounts.remove(pos);
            }
        }
    }

    /// Remove all matchign items from the list of mount actions.
    pub fn rem_bind_mount(&mut self, mount: BindMount) {
        if let Some(ref mut mounts) = self.bind_mounts {
            mounts.retain(|m| m != &mount);
        }
    }

    /// Extract the bind mount list.
    pub fn collect_bind_mounts(&mut self) -> Option<Vec<BindMount>> {
        std::mem::take(&mut self.bind_mounts)
    }

    /// Returns a new sandbox in default state.
    pub fn new() -> Self {
        Sandbox::default()
    }
}

// Note to self: To renumber the tests, do
// :let i=1 | g/sandbox_config_rules_\zs\d\+/s//\=i/ | let i+=1
// in VIM.
#[cfg(test)]
mod tests {
    use std::io::Result as IOResult;

    use super::*;

    #[test]
    fn sandbox_config_api() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("")?;
        sandbox.config("3")?;
        assert!(sandbox.config("2").is_err(), "{sandbox}");
        assert!(sandbox.config("1").is_err(), "{sandbox}");
        assert!(sandbox.config("0").is_err(), "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_sandbox_read() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(sandbox.state.contains(Capability::CAP_READ));
        assert!(sandbox.config("sandbox/read?").is_ok());
        sandbox.config("sandbox/read:off")?;
        assert!(!sandbox.state.contains(Capability::CAP_READ));
        assert!(sandbox.config("sandbox/read?").is_err());
        sandbox.config("sandbox/read:on")?;
        assert!(sandbox.state.contains(Capability::CAP_READ));
        assert!(sandbox.config("sandbox/read?").is_ok());

        Ok(())
    }

    #[test]
    fn sandbox_config_sandbox_stat() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(sandbox.state.contains(Capability::CAP_STAT));
        assert!(sandbox.config("sandbox/stat?").is_ok());
        sandbox.config("sandbox/stat:off")?;
        assert!(!sandbox.state.contains(Capability::CAP_STAT));
        assert!(sandbox.config("sandbox/stat?").is_err());
        sandbox.config("sandbox/stat:on")?;
        assert!(sandbox.state.contains(Capability::CAP_STAT));
        assert!(sandbox.config("sandbox/stat?").is_ok());

        Ok(())
    }

    #[test]
    fn sandbox_config_sandbox_write() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(sandbox.state.contains(Capability::CAP_WRITE));
        assert!(sandbox.config("sandbox/write?").is_ok());
        sandbox.config("sandbox/write:off")?;
        assert!(!sandbox.state.contains(Capability::CAP_WRITE));
        assert!(sandbox.config("sandbox/write?").is_err());
        sandbox.config("sandbox/write:on")?;
        assert!(sandbox.state.contains(Capability::CAP_WRITE));
        assert!(sandbox.config("sandbox/write?").is_ok());
        Ok(())
    }

    #[test]
    fn sandbox_config_sandbox_exec() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(sandbox.state.contains(Capability::CAP_EXEC));
        assert!(sandbox.config("sandbox/exec?").is_ok());
        sandbox.config("sandbox/exec:off")?;
        assert!(!sandbox.state.contains(Capability::CAP_EXEC));
        assert!(sandbox.config("sandbox/exec?").is_err());
        sandbox.config("sandbox/exec:on")?;
        assert!(sandbox.state.contains(Capability::CAP_EXEC));
        assert!(sandbox.config("sandbox/exec?").is_ok());
        Ok(())
    }

    #[test]
    fn sandbox_config_sandbox_network() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(sandbox.state.contains(Capability::CAP_BIND));
        assert!(sandbox.state.contains(Capability::CAP_CONNECT));
        assert!(sandbox.config("sandbox/net?").is_ok());
        sandbox.config("sandbox/net:off")?;
        assert!(!sandbox.state.contains(Capability::CAP_BIND));
        assert!(!sandbox.state.contains(Capability::CAP_CONNECT));
        assert!(sandbox.config("sandbox/net?").is_err());
        sandbox.config("sandbox/net:on")?;
        assert!(sandbox.state.contains(Capability::CAP_BIND));
        assert!(sandbox.state.contains(Capability::CAP_CONNECT));
        assert!(sandbox.config("sandbox/net?").is_ok());

        sandbox.config("sandbox/net:off")?;
        sandbox.config("sandbox/net:bind")?;
        assert!(sandbox.state.contains(Capability::CAP_BIND));
        assert!(!sandbox.state.contains(Capability::CAP_CONNECT));
        assert!(sandbox.config("sandbox/net?").is_ok());

        sandbox.config("sandbox/net:off")?;
        sandbox.config("sandbox/net:connect")?;
        assert!(sandbox.state.contains(Capability::CAP_CONNECT));
        assert!(!sandbox.state.contains(Capability::CAP_BIND));
        assert!(sandbox.config("sandbox/net?").is_ok());

        Ok(())
    }

    #[test]
    fn sandbox_config_lock() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(sandbox.lock == LockState::Exec);
        sandbox.config("lock:off")?;
        assert!(sandbox.lock == LockState::Off);
        sandbox.config("lock:exec")?;
        assert!(sandbox.lock == LockState::Exec);
        sandbox.config("lock:on")?;
        assert!(sandbox.lock == LockState::Set);
        Ok(())
    }

    #[test]
    fn sandbox_config_trace() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert!(!sandbox.flags.contains(Flag::FL_ALLOW_SAFE_BIND));
        sandbox.config("trace/allow_safe_bind:true")?;
        assert!(sandbox.flags.contains(Flag::FL_ALLOW_SAFE_BIND));
        sandbox.config("trace/allow_safe_bind:false")?;
        assert!(!sandbox.flags.contains(Flag::FL_ALLOW_SAFE_BIND));
        sandbox.config("trace/allow_safe_bind:t")?;
        assert!(sandbox.flags.contains(Flag::FL_ALLOW_SAFE_BIND));
        sandbox.config("trace/allow_safe_bind:f")?;
        assert!(!sandbox.flags.contains(Flag::FL_ALLOW_SAFE_BIND));
        sandbox.config("trace/allow_safe_bind:1")?;
        assert!(sandbox.flags.contains(Flag::FL_ALLOW_SAFE_BIND));
        sandbox.config("trace/allow_safe_bind:0")?;
        assert!(!sandbox.flags.contains(Flag::FL_ALLOW_SAFE_BIND));

        assert_eq!(
            sandbox.config("trace/allow_safe_bind_invalid:t"),
            Err(Errno::EINVAL)
        );
        assert_eq!(
            sandbox.config("trace/allow_safe_bind!x"),
            Err(Errno::EINVAL)
        );
        assert_eq!(
            sandbox.config("trace/allow_safe_bind:x"),
            Err(Errno::EINVAL)
        );
        assert_eq!(
            sandbox.config("trace/allow_safe_bind:☮"),
            Err(Errno::EINVAL)
        );

        assert!(!sandbox.flags.contains(Flag::FL_ALLOW_UNSUPP_SOCKET));
        sandbox.config("trace/allow_unsupp_socket:true")?;
        assert!(sandbox.flags.contains(Flag::FL_ALLOW_UNSUPP_SOCKET));
        sandbox.config("trace/allow_unsupp_socket:false")?;
        assert!(!sandbox.flags.contains(Flag::FL_ALLOW_UNSUPP_SOCKET));
        sandbox.config("trace/allow_unsupp_socket:t")?;
        assert!(sandbox.flags.contains(Flag::FL_ALLOW_UNSUPP_SOCKET));
        sandbox.config("trace/allow_unsupp_socket:f")?;
        assert!(!sandbox.flags.contains(Flag::FL_ALLOW_UNSUPP_SOCKET));
        sandbox.config("trace/allow_unsupp_socket:1")?;
        assert!(sandbox.flags.contains(Flag::FL_ALLOW_UNSUPP_SOCKET));
        sandbox.config("trace/allow_unsupp_socket:0")?;
        assert!(!sandbox.flags.contains(Flag::FL_ALLOW_UNSUPP_SOCKET));

        assert_eq!(
            sandbox.config("trace/allow_unsupp_socket_invalid:t"),
            Err(Errno::EINVAL)
        );
        assert_eq!(
            sandbox.config("trace/allow_unsupp_socket!x"),
            Err(Errno::EINVAL)
        );
        assert_eq!(
            sandbox.config("trace/allow_unsupp_socket:x"),
            Err(Errno::EINVAL)
        );
        assert_eq!(
            sandbox.config("trace/allow_unsupp_socket:☮"),
            Err(Errno::EINVAL)
        );

        assert_eq!(sandbox.config("trace/memory_access:0"), Ok(()));
        assert_eq!(sandbox.config("trace/memory_access:1"), Ok(()));
        assert_eq!(
            sandbox.config("trace/memory_access_invalid:t"),
            Err(Errno::EINVAL)
        );
        assert_eq!(sandbox.config("trace/memory_access!x"), Err(Errno::EINVAL));
        assert_eq!(sandbox.config("trace/memory_access:x"), Err(Errno::EINVAL));
        assert_eq!(sandbox.config("trace/memory_access:t"), Err(Errno::EINVAL));
        assert_eq!(sandbox.config("trace/memory_access:☮"), Err(Errno::EINVAL));
        assert_eq!(sandbox.config("trace/memory_access:f"), Err(Errno::EINVAL));

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_1() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/read+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");

        sandbox.config("allow/read-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_2() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/read+/usr")?;
        sandbox.config("allow/read+/usr/**")?;
        sandbox.config("allow/read+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");

        sandbox.config("allow/read^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_3() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/write+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/write-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_4() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/write+/usr")?;
        sandbox.config("allow/write+/usr/**")?;
        sandbox.config("allow/write+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/write^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_5() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/exec+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/exec-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_6() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/exec+/usr")?;
        sandbox.config("allow/exec+/usr/**")?;
        sandbox.config("allow/exec+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/exec^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_7() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/bind-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_8() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+/usr/**")?;
        sandbox.config("allow/net/bind+/usr")?;
        sandbox.config("allow/net/bind+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/bind^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_9() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/connect-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_10() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+/usr/**")?;
        sandbox.config("allow/net/connect+/usr/**")?;
        sandbox.config("allow/net/connect+/usr")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/connect^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_11() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/bind-127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_12() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+127.0.0.0/8!1024")?;
        sandbox.config("allow/net/bind+127.0.0.0/8!1024-65535")?;
        sandbox.config("allow/net/bind+127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/bind^127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_13() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/connect-127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_14() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+127.0.0.0/8!1024-65535")?;
        sandbox.config("allow/net/connect+127.0.0.0/7!1024-65535")?;
        sandbox.config("allow/net/connect+127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/connect^127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_15() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/bind-::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_16() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+::1/8!1024-65535")?;
        sandbox.config("allow/net/bind+::1/8!1024-65535")?;
        sandbox.config("allow/net/bind+::1/8!1024-65525")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/bind^::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_17() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/connect-::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_18() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+::1/8!1024-65535")?;
        sandbox.config("allow/net/connect+::1/8!1024-65535")?;
        sandbox.config("allow/net/connect+::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/connect^::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_19() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/read+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/read-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_20() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/read+/usr/**")?;
        sandbox.config("deny/read+/usr/*")?;
        sandbox.config("deny/read+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/read^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_21() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/write+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/write-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_22() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/write+/usr/**")?;
        sandbox.config("deny/write+/usr/**")?;
        sandbox.config("deny/write+/usr/*")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/write^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_23() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/exec+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/exec-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_24() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/exec+/usr/**")?;
        sandbox.config("deny/exec+/usr/**")?;
        sandbox.config("deny/exec+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/exec^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_25() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/bind-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_26() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+/usr")?;
        sandbox.config("deny/net/bind+/usr/*")?;
        sandbox.config("deny/net/bind+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/bind-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 2, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_27() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/connect-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_28() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+/usr/**")?;
        sandbox.config("deny/net/connect+/usr/*")?;
        sandbox.config("deny/net/connect+/usr")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/connect^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 2, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_29() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/bind-127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_30() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+127.0.1.0/8!1024-65535")?;
        sandbox.config("deny/net/bind+127.0.0.1/8!1024-65535")?;
        sandbox.config("deny/net/bind+127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/bind^127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_31() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/connect-127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_32() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+127.0.0.0/8!1024-65535")?;
        sandbox.config("deny/net/connect+127.0.0.1/8!1024-65535")?;
        sandbox.config("deny/net/connect+127.0.0.0/8!1024-65535")?;
        sandbox.config("deny/net/connect+127.0.1.0/8!1024-65535")?;
        sandbox.config("deny/net/connect+127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 5, "{sandbox}");
        sandbox.config("deny/net/connect^127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_33() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/bind-::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_34() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+::1/16!1024-65535")?;
        sandbox.config("deny/net/bind+::1/8!1024-65535")?;
        sandbox.config("deny/net/bind+::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/bind^::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_35() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/connect-::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_36() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+::1/8!1024-65535")?;
        sandbox.config("deny/net/connect+::1/8!1024-65535")?;
        sandbox.config("deny/net/connect+::1!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/connect^::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_37() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/read+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/read-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_38() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/read+/usr/**")?;
        sandbox.config("filter/read+/usr/**")?;
        sandbox.config("filter/read+/usr/*")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/read^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_39() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/write+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/write-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_40() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/write+/usr/**/")?;
        sandbox.config("filter/write+/usr/**")?;
        sandbox.config("filter/write+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/write^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_41() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/exec+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/exec-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_42() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/exec+/usr/**")?;
        sandbox.config("filter/exec+/usr/*")?;
        sandbox.config("filter/exec+/usr/**")?;
        sandbox.config("filter/exec+/usr/./**")?;
        sandbox.config("filter/exec+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 5, "{sandbox}");
        sandbox.config("filter/exec^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 2, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_43() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_44() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+/usr/**")?;
        sandbox.config("filter/net/bind+/usr/**")?;
        sandbox.config("filter/net/bind+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/bind^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_45() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_46() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+/usr/**")?;
        sandbox.config("filter/net/connect+/usr/**/")?;
        sandbox.config("filter/net/connect+/usr/*")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/connect-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 2, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_47() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_48() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+127.0.0.0/8!1024-65535")?;
        sandbox.config("filter/net/bind+127.0.0.0/8!1024-65535")?;
        sandbox.config("filter/net/bind+127.0.0.0/8!1023-65535")?;
        sandbox.config("filter/net/bind+127.0.0.0/8!1023-65534")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("filter/net/bind^127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_49() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_50() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+127.0.0.0/8!1024-65534")?;
        sandbox.config("filter/net/connect+127.0.0.0/8!1024-65535")?;
        sandbox.config("filter/net/connect+127.0.0.1/8!1024-65535")?;
        sandbox.config("filter/net/connect+127.0.0.0/8!1024-65535")?;
        sandbox.config("filter/net/connect+127.0.0.0/8!1025-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 5, "{sandbox}");
        sandbox.config("filter/net/connect^127.0.0.0/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_51() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_52() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+::1/8!1024-65535")?;
        sandbox.config("filter/net/bind+::1/8!1024-65535")?;
        sandbox.config("filter/net/bind+::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/bind^::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_53() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_54() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+::1/8!1024-65535")?;
        sandbox.config("filter/net/connect+::1/8!1024-65535")?;
        sandbox.config("filter/net/connect+::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/connect^::1/8!1024-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_55() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("exec/kill+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("exec/kill-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_56() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("exec/kill+/usr/**")?;
        sandbox.config("exec/kill+/usr/**")?;
        sandbox.config("exec/kill+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("exec/kill^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_57() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/bind-loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_58() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+loopback4!0")?;
        sandbox.config("allow/net/bind+loopback4!0")?;
        sandbox.config("allow/net/bind+loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/bind^loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_59() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/connect-loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_60() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+loopback4!0")?;
        sandbox.config("allow/net/connect+loopback4!0")?;
        sandbox.config("allow/net/connect+loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/connect^loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_61() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/bind-loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_62() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+loopback4!0")?;
        sandbox.config("deny/net/bind+loopback4!0")?;
        sandbox.config("deny/net/bind+loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/bind^loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_63() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/connect-loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_64() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+127.0.0.0/16!0-65535")?;
        sandbox.config("deny/net/connect+loopback4!0")?;
        sandbox.config("deny/net/connect+loopback4!0")?;
        sandbox.config("deny/net/connect+127.0.0.0/16!0-65535")?;
        sandbox.config("deny/net/connect+loopback4!0")?;
        sandbox.config("filter/net/connect+127.0.0.0/16!0-65535")?;
        sandbox.config("deny/net/connect+loopback4!0")?;
        sandbox.config("deny/net/bind+127.0.0.0/8!0-65535")?;
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        sandbox.config("deny/net/connect^loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_65() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_66() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+loopback4!0")?;
        sandbox.config("filter/net/bind+loopback4!0")?;
        sandbox.config("filter/net/bind+loopback4!1")?;
        sandbox.config("filter/net/bind+loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("filter/net/bind^loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_67() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_68() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+loopback4!0")?;
        sandbox.config("filter/net/connect+loopback4!0")?;
        sandbox.config("filter/net/connect+loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/connect^loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_69() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/bind-loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_70() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+loopback6!0")?;
        sandbox.config("allow/net/bind+loopback6!0")?;
        sandbox.config("allow/net/bind+loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/bind^loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_71() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/connect-loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_72() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+loopback6!0")?;
        sandbox.config("allow/net/connect+loopback6!0")?;
        sandbox.config("allow/net/connect+loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/connect^loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_73() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/bind-loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_74() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+loopback6!0")?;
        sandbox.config("deny/net/bind+loopback6!0")?;
        sandbox.config("deny/net/bind+loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/bind^loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_75() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/connect-loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_76() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+loopback6!0")?;
        sandbox.config("deny/net/connect+loopback6!0")?;
        sandbox.config("deny/net/connect+loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/connect^loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_77() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_78() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+loopback6!0")?;
        sandbox.config("filter/net/bind+loopback6!0")?;
        sandbox.config("filter/net/bind+loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/bind^loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_79() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_80() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+loopback6!0")?;
        sandbox.config("filter/net/connect+loopback6!0")?;
        sandbox.config("filter/net/connect+loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/connect^loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_81() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("allow/net/bind-loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_82() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("allow/net/bind-loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_83() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+loopback!0")?;
        sandbox.config("allow/net/bind+loopback!0")?;
        sandbox.config("allow/net/bind+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("allow/net/bind^loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_84() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("allow/net/connect-loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_85() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+loopback!0")?;
        sandbox.config("allow/net/connect+loopback!0")?;
        sandbox.config("allow/net/connect+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("allow/net/connect^loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_86() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("deny/net/bind-loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_87() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+loopback!0")?;
        sandbox.config("deny/net/bind+loopback!0")?;
        sandbox.config("deny/net/bind+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("deny/net/bind^loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_88() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("deny/net/connect-loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_89() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+loopback!0")?;
        sandbox.config("deny/net/connect+loopback!0")?;
        sandbox.config("deny/net/connect+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("deny/net/connect^loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_90() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("filter/net/bind-loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_91() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+loopback!0")?;
        sandbox.config("filter/net/bind+loopback!0")?;
        sandbox.config("filter/net/bind+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("filter/net/bind^loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_92() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("filter/net/connect-loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_93() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+loopback!0")?;
        sandbox.config("filter/net/connect+loopback!0")?;
        sandbox.config("filter/net/connect+loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("filter/net/connect^loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_94() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("allow/net/bind-local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_95() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+local4!0")?;
        sandbox.config("allow/net/bind+local4!0")?;
        sandbox.config("allow/net/bind+local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("allow/net/bind^local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_96() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("allow/net/connect-local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_97() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+local4!0")?;
        sandbox.config("allow/net/connect+local4!0")?;
        sandbox.config("allow/net/connect+local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("allow/net/connect^local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_98() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("deny/net/bind-local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_99() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+local4!0")?;
        sandbox.config("deny/net/bind+local4!0")?;
        sandbox.config("deny/net/bind+local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("deny/net/bind^local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_100() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("deny/net/connect-local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_101() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+local4!0")?;
        sandbox.config("deny/net/connect+local4!0")?;
        sandbox.config("deny/net/connect+local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("deny/net/connect^local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_102() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("filter/net/bind-local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_103() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+local4!0")?;
        sandbox.config("filter/net/bind+local4!0")?;
        sandbox.config("filter/net/bind+local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("filter/net/bind^local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_104() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("filter/net/connect-local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_105() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+local4!0")?;
        sandbox.config("filter/net/connect+local4!0")?;
        sandbox.config("filter/net/connect+local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("filter/net/connect^local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_106() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("allow/net/bind-local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_107() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+local6!0")?;
        sandbox.config("allow/net/bind+local6!0")?;
        sandbox.config("allow/net/bind+local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("allow/net/bind^local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_108() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("allow/net/connect-local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_109() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+local6!0")?;
        sandbox.config("allow/net/connect+local6!0")?;
        sandbox.config("allow/net/connect+local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("allow/net/connect^local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_110() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("deny/net/bind-local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_111() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+local6!0")?;
        sandbox.config("deny/net/bind+local6!0")?;
        sandbox.config("deny/net/bind+local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("deny/net/bind^local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_112() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("deny/net/connect-local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_113() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+local6!0")?;
        sandbox.config("deny/net/connect+local6!0")?;
        sandbox.config("deny/net/connect+local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("deny/net/connect^local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_114() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("filter/net/bind-local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_115() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+local6!0")?;
        sandbox.config("filter/net/bind+local6!0")?;
        sandbox.config("filter/net/bind+local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("filter/net/bind^local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_116() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("filter/net/connect-local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_117() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+local6!0")?;
        sandbox.config("filter/net/connect+local6!0")?;
        sandbox.config("filter/net/connect+local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("filter/net/connect^local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_118() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        sandbox.config("filter/net/bind-local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_119() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+local!0")?;
        sandbox.config("filter/net/bind+local!0")?;
        sandbox.config("filter/net/bind+local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 24, "{sandbox}");
        sandbox.config("filter/net/bind^local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_120() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        sandbox.config("filter/net/connect-local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_121() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+local!0")?;
        sandbox.config("filter/net/connect+local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 16, "{sandbox}");
        sandbox.config("filter/net/connect^local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_122() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        sandbox.config("allow/net/bind-local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_123() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+local!1")?;
        sandbox.config("allow/net/bind+local!0")?;
        sandbox.config("allow/net/bind+local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 24, "{sandbox}");
        sandbox.config("allow/net/bind^local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_124() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        sandbox.config("allow/net/connect-local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_125() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+127.0.0.2!0")?;
        sandbox.config("allow/net/connect+local!0")?;
        sandbox.config("allow/net/connect+local!0")?;
        sandbox.config("allow/net/connect+local!1")?;
        assert_eq!(sandbox.cidr_rules.len(), 25, "{sandbox}");
        sandbox.config("allow/net/connect^local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 9, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_126() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        sandbox.config("deny/net/bind-local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_127() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+local!0")?;
        sandbox.config("deny/net/bind+127.0.0.1!0")?;
        sandbox.config("deny/net/bind+local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 17, "{sandbox}");
        sandbox.config("deny/net/bind^local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_128() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        sandbox.config("deny/net/connect-local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_129() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+local!0")?;
        sandbox.config("deny/net/connect+local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 16, "{sandbox}");
        sandbox.config("deny/net/connect^local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_130() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/bind-linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_131() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+linklocal4!0")?;
        sandbox.config("allow/net/bind+linklocal4!0")?;
        sandbox.config("allow/net/bind+linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/bind^linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_132() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/connect-linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_133() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+127.0.0.3!7")?;
        sandbox.config("allow/net/connect+linklocal4!0")?;
        sandbox.config("allow/net/connect+linklocal4!0")?;
        sandbox.config("allow/net/connect+linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("allow/net/connect^linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_134() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/bind-linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_135() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+linklocal4!0")?;
        sandbox.config("deny/net/bind+linklocal4!0")?;
        sandbox.config("deny/net/bind+linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/bind^linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_136() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/connect-linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_137() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+linklocal4!0")?;
        sandbox.config("deny/net/connect+linklocal4!0")?;
        sandbox.config("deny/net/connect+linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/connect^linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_138() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_139() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+linklocal4!0")?;
        sandbox.config("filter/net/bind+linklocal4!0")?;
        sandbox.config("filter/net/bind+linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/bind^linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_140() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_141() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+linklocal4!0")?;
        sandbox.config("filter/net/connect+linklocal4!0")?;
        sandbox.config("filter/net/connect+linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/connect^linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_142() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/bind-linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_143() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+linklocal6!0")?;
        sandbox.config("allow/net/bind+linklocal6!0")?;
        sandbox.config("allow/net/bind+linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/bind^linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_144() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/connect-linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_145() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+linklocal6!0")?;
        sandbox.config("allow/net/connect+linklocal6!0")?;
        sandbox.config("allow/net/connect+linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/connect^linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_146() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/bind-linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_147() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+linklocal6!0")?;
        sandbox.config("deny/net/bind+linklocal6!0")?;
        sandbox.config("deny/net/bind+linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/bind^linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_148() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/connect-linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_149() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+linklocal6!0")?;
        sandbox.config("deny/net/connect+linklocal6!0")?;
        sandbox.config("deny/net/connect+linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/connect^linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_150() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_151() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+linklocal6!0")?;
        sandbox.config("filter/net/bind+linklocal6!0")?;
        sandbox.config("filter/net/bind+linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/bind^linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_152() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_153() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+linklocal6!0")?;
        sandbox.config("filter/net/connect+linklocal6!0")?;
        sandbox.config("filter/net/connect+linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/connect^linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_154() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("filter/net/bind-linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_155() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+linklocal!0")?;
        sandbox.config("filter/net/bind+linklocal!0")?;
        sandbox.config("filter/net/bind+linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("filter/net/bind^linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_156() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("filter/net/connect-linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_157() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+linklocal!0")?;
        sandbox.config("filter/net/connect+linklocal!0")?;
        sandbox.config("filter/net/connect+linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("filter/net/connect^linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_158() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("allow/net/bind-linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_159() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+linklocal!0")?;
        sandbox.config("allow/net/bind+linklocal!0")?;
        sandbox.config("allow/net/bind+linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("allow/net/bind^linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_160() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("allow/net/connect-linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_161() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+linklocal!0")?;
        sandbox.config("allow/net/connect+linklocal!0")?;
        sandbox.config("allow/net/connect+linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("allow/net/connect^linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_162() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("deny/net/bind-linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_163() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+linklocal!0")?;
        sandbox.config("deny/net/bind+linklocal!0")?;
        sandbox.config("deny/net/bind+linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("deny/net/bind^linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_164() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("deny/net/connect-linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_165() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+linklocal!0")?;
        sandbox.config("deny/net/connect+linklocal!0")?;
        sandbox.config("deny/net/connect+linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("deny/net/connect^linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_166() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/bind-any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_167() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+any4!0")?;
        sandbox.config("allow/net/bind+any4!0")?;
        sandbox.config("allow/net/bind+any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/bind^any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_168() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/connect-any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_169() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+any4!0")?;
        sandbox.config("allow/net/connect+any4!0")?;
        sandbox.config("allow/net/connect+any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/connect^any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_170() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/bind-any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_171() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+any4!0")?;
        sandbox.config("deny/net/bind+any4!0")?;
        sandbox.config("deny/net/bind+any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/bind^any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_172() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/connect-any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_173() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+any4!0")?;
        sandbox.config("deny/net/connect+any4!0")?;
        sandbox.config("deny/net/connect+any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/connect^any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_174() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_175() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+any4!0")?;
        sandbox.config("filter/net/bind+any4!0")?;
        sandbox.config("filter/net/bind+any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/bind^any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_176() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_177() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+any4!0")?;
        sandbox.config("filter/net/connect+any4!0")?;
        sandbox.config("filter/net/connect+any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/connect^any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_178() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/bind-any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_179() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+any6!0")?;
        sandbox.config("allow/net/bind+any6!0")?;
        sandbox.config("allow/net/bind+any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/bind^any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_180() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/connect-any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_181() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+any6!0")?;
        sandbox.config("allow/net/connect+any6!0")?;
        sandbox.config("allow/net/connect+any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/connect^any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_182() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/bind-any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_183() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+any6!0")?;
        sandbox.config("deny/net/bind+any6!0")?;
        sandbox.config("deny/net/bind+any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/bind^any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_184() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/connect-any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_185() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+any6!0")?;
        sandbox.config("deny/net/connect+any6!0")?;
        sandbox.config("deny/net/connect+any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("deny/net/connect^any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_186() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/bind-any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_187() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+any6!0")?;
        sandbox.config("filter/net/bind+any6!0")?;
        sandbox.config("filter/net/bind+any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/bind^any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_188() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/net/connect-any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_189() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+any6!0")?;
        sandbox.config("filter/net/connect+any6!0")?;
        sandbox.config("filter/net/connect+any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/net/connect^any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_190() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("allow/net/bind-any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_191() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+any!0")?;
        sandbox.config("allow/net/bind+any!0")?;
        sandbox.config("allow/net/bind+any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("allow/net/bind^any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_192() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("allow/net/connect-any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_193() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+any!0")?;
        sandbox.config("allow/net/connect+any!0")?;
        sandbox.config("allow/net/connect+any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("allow/net/connect^any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_194() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("deny/net/bind-any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_195() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+any!0")?;
        sandbox.config("deny/net/bind+any!0")?;
        sandbox.config("deny/net/bind+any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("deny/net/bind^any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_196() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("deny/net/connect-any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_197() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+any!0")?;
        sandbox.config("deny/net/connect+any!0")?;
        sandbox.config("deny/net/connect+any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("deny/net/connect^any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_198() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("filter/net/bind-any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_199() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/bind+any!0")?;
        sandbox.config("filter/net/bind+any!0")?;
        sandbox.config("filter/net/bind+any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("filter/net/bind^any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_200() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("filter/net/connect-any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_201() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/net/connect+any!0")?;
        sandbox.config("filter/net/connect+any!0")?;
        sandbox.config("filter/net/connect+any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("filter/net/connect^any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_202() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..8 {
            sandbox.config("allow/net/bind+loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        for _ in 0..8 {
            sandbox.config("allow/net/bind-loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_203() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..8 {
            sandbox.config("allow/net/bind+loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        sandbox.config("allow/net/bind^loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_204() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..9 {
            sandbox.config("allow/net/connect+loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 9, "{sandbox}");
        for _ in 0..9 {
            sandbox.config("allow/net/connect-loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_205() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..9 {
            sandbox.config("allow/net/connect+loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 9, "{sandbox}");
        sandbox.config("allow/net/connect^loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_206() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..10 {
            sandbox.config("deny/net/bind+loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 10, "{sandbox}");
        for _ in 0..10 {
            sandbox.config("deny/net/bind-loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_207() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..10 {
            sandbox.config("deny/net/bind+loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 10, "{sandbox}");
        sandbox.config("deny/net/bind^loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_208() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..11 {
            sandbox.config("deny/net/connect+loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 11, "{sandbox}");
        for _ in 0..11 {
            sandbox.config("deny/net/connect-loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_209() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..11 {
            sandbox.config("deny/net/connect+loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 11, "{sandbox}");
        sandbox.config("deny/net/connect^loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_210() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..12 {
            sandbox.config("filter/net/bind+loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        for _ in 0..12 {
            sandbox.config("filter/net/bind-loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_211() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..12 {
            sandbox.config("filter/net/bind+loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("filter/net/bind^loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_212() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..13 {
            sandbox.config("filter/net/connect+loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 13, "{sandbox}");
        for _ in 0..13 {
            sandbox.config("filter/net/connect-loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_213() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..13 {
            sandbox.config("filter/net/connect+loopback4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 13, "{sandbox}");
        sandbox.config("filter/net/connect^loopback4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_214() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..8 {
            sandbox.config("allow/net/bind+loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        for _ in 0..8 {
            sandbox.config("allow/net/bind-loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_215() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..8 {
            sandbox.config("allow/net/bind+loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        sandbox.config("allow/net/bind^loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_216() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..9 {
            sandbox.config("allow/net/connect+loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 9, "{sandbox}");
        for _ in 0..9 {
            sandbox.config("allow/net/connect-loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_217() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..9 {
            sandbox.config("allow/net/connect+loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 9, "{sandbox}");
        sandbox.config("allow/net/connect^loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_218() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..10 {
            sandbox.config("deny/net/bind+loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 10, "{sandbox}");
        for _ in 0..10 {
            sandbox.config("deny/net/bind-loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_219() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..10 {
            sandbox.config("deny/net/bind+loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 10, "{sandbox}");
        sandbox.config("deny/net/bind^loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_220() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..11 {
            sandbox.config("deny/net/connect+loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 11, "{sandbox}");
        for _ in 0..11 {
            sandbox.config("deny/net/connect-loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_221() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..11 {
            sandbox.config("deny/net/connect+loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 11, "{sandbox}");
        sandbox.config("deny/net/connect^loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_222() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..12 {
            sandbox.config("filter/net/bind+loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        for _ in 0..12 {
            sandbox.config("filter/net/bind-loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_223() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..12 {
            sandbox.config("filter/net/bind+loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("filter/net/bind^loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_224() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..13 {
            sandbox.config("filter/net/connect+loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 13, "{sandbox}");
        for _ in 0..13 {
            sandbox.config("filter/net/connect-loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_225() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..13 {
            sandbox.config("filter/net/connect+loopback6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 13, "{sandbox}");
        sandbox.config("filter/net/connect^loopback6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_226() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..2 {
            sandbox.config("allow/net/bind+loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        for _ in 0..2 {
            sandbox.config("allow/net/bind-loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_227() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..2 {
            sandbox.config("allow/net/bind+loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("allow/net/bind^loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_228() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..3 {
            sandbox.config("allow/net/connect+loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        for _ in 0..3 {
            sandbox.config("allow/net/connect-loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_229() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..3 {
            sandbox.config("allow/net/connect+loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("allow/net/connect^loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_230() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..4 {
            sandbox.config("deny/net/bind+loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        for _ in 0..4 {
            sandbox.config("deny/net/bind-loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_231() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..4 {
            sandbox.config("deny/net/bind+loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        sandbox.config("deny/net/bind^loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_232() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..5 {
            sandbox.config("deny/net/connect+loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 10, "{sandbox}");
        for _ in 0..5 {
            sandbox.config("deny/net/connect-loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_233() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..5 {
            sandbox.config("deny/net/connect+loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 10, "{sandbox}");
        sandbox.config("deny/net/connect^loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_234() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..6 {
            sandbox.config("filter/net/bind+loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        for _ in 0..6 {
            sandbox.config("filter/net/bind-loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_235() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..6 {
            sandbox.config("filter/net/bind+loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("filter/net/bind^loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_236() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..7 {
            sandbox.config("filter/net/connect+loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 14, "{sandbox}");
        for _ in 0..7 {
            sandbox.config("filter/net/connect-loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_237() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..7 {
            sandbox.config("filter/net/connect+loopback!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 14, "{sandbox}");
        sandbox.config("filter/net/connect^loopback!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_238() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..14 {
            sandbox.config("allow/net/bind+local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 14, "{sandbox}");
        for _ in 0..14 {
            sandbox.config("allow/net/bind-local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_239() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..14 {
            sandbox.config("allow/net/bind+local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 14, "{sandbox}");
        sandbox.config("allow/net/bind^local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_240() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..15 {
            sandbox.config("allow/net/connect+local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 15, "{sandbox}");
        for _ in 0..15 {
            sandbox.config("allow/net/connect-local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_241() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..15 {
            sandbox.config("allow/net/connect+local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 15, "{sandbox}");
        sandbox.config("allow/net/connect^local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_242() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..16 {
            sandbox.config("deny/net/bind+local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 16, "{sandbox}");
        for _ in 0..16 {
            sandbox.config("deny/net/bind-local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_243() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..16 {
            sandbox.config("deny/net/bind+local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 16, "{sandbox}");
        sandbox.config("deny/net/bind^local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_244() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..17 {
            sandbox.config("deny/net/connect+local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 17, "{sandbox}");
        for _ in 0..17 {
            sandbox.config("deny/net/connect-local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_245() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..17 {
            sandbox.config("deny/net/connect+local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 17, "{sandbox}");
        sandbox.config("deny/net/connect^local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_246() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..18 {
            sandbox.config("filter/net/bind+local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 18, "{sandbox}");
        for _ in 0..18 {
            sandbox.config("filter/net/bind-local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_247() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..18 {
            sandbox.config("filter/net/bind+local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 18, "{sandbox}");
        sandbox.config("filter/net/bind^local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_248() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..19 {
            sandbox.config("filter/net/connect+local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 19, "{sandbox}");
        for _ in 0..19 {
            sandbox.config("filter/net/connect-local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_249() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..19 {
            sandbox.config("filter/net/connect+local4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 19, "{sandbox}");
        sandbox.config("filter/net/connect^local4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_250() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..20 {
            sandbox.config("allow/net/bind+local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 20, "{sandbox}");
        for _ in 0..20 {
            sandbox.config("allow/net/bind-local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_251() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..20 {
            sandbox.config("allow/net/bind+local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 20, "{sandbox}");
        sandbox.config("allow/net/bind^local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_252() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..21 {
            sandbox.config("allow/net/connect+local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 21, "{sandbox}");
        for _ in 0..21 {
            sandbox.config("allow/net/connect-local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_253() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..21 {
            sandbox.config("allow/net/connect+local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 21, "{sandbox}");
        sandbox.config("allow/net/connect^local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_254() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..22 {
            sandbox.config("deny/net/bind+local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 22, "{sandbox}");
        for _ in 0..22 {
            sandbox.config("deny/net/bind-local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_255() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..22 {
            sandbox.config("deny/net/bind+local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 22, "{sandbox}");
        sandbox.config("deny/net/bind^local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_256() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..23 {
            sandbox.config("deny/net/connect+local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 23, "{sandbox}");
        for _ in 0..23 {
            sandbox.config("deny/net/connect-local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_257() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..23 {
            sandbox.config("deny/net/connect+local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 23, "{sandbox}");
        sandbox.config("deny/net/connect^local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_258() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..24 {
            sandbox.config("filter/net/bind+local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 24, "{sandbox}");
        for _ in 0..24 {
            sandbox.config("filter/net/bind-local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_259() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..24 {
            sandbox.config("filter/net/bind+local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 24, "{sandbox}");
        sandbox.config("filter/net/bind^local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_260() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..25 {
            sandbox.config("filter/net/connect+local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 25, "{sandbox}");
        for _ in 0..25 {
            sandbox.config("filter/net/connect-local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_261() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..25 {
            sandbox.config("filter/net/connect+local6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4 * 25, "{sandbox}");
        sandbox.config("filter/net/connect^local6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_262() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..20 {
            sandbox.config("allow/net/bind+local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8 * 20, "{sandbox}");
        for _ in 0..20 {
            sandbox.config("allow/net/bind-local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_263() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..20 {
            sandbox.config("allow/net/bind+local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8 * 20, "{sandbox}");
        sandbox.config("allow/net/bind^local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_264() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..21 {
            sandbox.config("allow/net/connect+local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8 * 21, "{sandbox}");
        for _ in 0..21 {
            sandbox.config("allow/net/connect-local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_265() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..21 {
            sandbox.config("allow/net/connect+local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8 * 21, "{sandbox}");
        sandbox.config("allow/net/connect^local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_266() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..22 {
            sandbox.config("deny/net/bind+local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8 * 22, "{sandbox}");
        for _ in 0..22 {
            sandbox.config("deny/net/bind-local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_267() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..22 {
            sandbox.config("deny/net/bind+local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8 * 22, "{sandbox}");
        sandbox.config("deny/net/bind^local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_268() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..23 {
            sandbox.config("deny/net/connect+local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8 * 23, "{sandbox}");
        for _ in 0..23 {
            sandbox.config("deny/net/connect-local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_269() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..23 {
            sandbox.config("deny/net/connect+local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8 * 23, "{sandbox}");
        sandbox.config("deny/net/connect^local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_270() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..24 {
            sandbox.config("filter/net/bind+local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8 * 24, "{sandbox}");
        for _ in 0..24 {
            sandbox.config("filter/net/bind-local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_271() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..24 {
            sandbox.config("filter/net/bind+local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8 * 24, "{sandbox}");
        sandbox.config("filter/net/bind^local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_272() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..25 {
            sandbox.config("filter/net/connect+local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8 * 25, "{sandbox}");
        for _ in 0..25 {
            sandbox.config("filter/net/connect-local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_273() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..25 {
            sandbox.config("filter/net/connect+local!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8 * 25, "{sandbox}");
        sandbox.config("filter/net/connect^local!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_274() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..4 {
            sandbox.config("deny/net/bind+any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        for _ in 0..4 {
            sandbox.config("deny/net/bind-any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_275() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..4 {
            sandbox.config("deny/net/bind+any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 4, "{sandbox}");
        sandbox.config("deny/net/bind^any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_276() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..5 {
            sandbox.config("deny/net/connect+any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 5, "{sandbox}");
        for _ in 0..5 {
            sandbox.config("deny/net/connect-any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_277() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..5 {
            sandbox.config("deny/net/connect+any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 5, "{sandbox}");
        sandbox.config("deny/net/connect^any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_278() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..14 {
            sandbox.config("allow/net/bind+linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 14, "{sandbox}");
        for _ in 0..14 {
            sandbox.config("allow/net/bind-linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_279() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..14 {
            sandbox.config("allow/net/bind+linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 14, "{sandbox}");
        sandbox.config("allow/net/bind^linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_280() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..15 {
            sandbox.config("allow/net/connect+linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 15, "{sandbox}");
        for _ in 0..15 {
            sandbox.config("allow/net/connect-linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_281() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..15 {
            sandbox.config("allow/net/connect+linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 15, "{sandbox}");
        sandbox.config("allow/net/connect^linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_282() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..16 {
            sandbox.config("deny/net/bind+linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 16, "{sandbox}");
        for _ in 0..16 {
            sandbox.config("deny/net/bind-linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_283() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..16 {
            sandbox.config("deny/net/bind+linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 16, "{sandbox}");
        sandbox.config("deny/net/bind^linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_284() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..17 {
            sandbox.config("deny/net/connect+linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 17, "{sandbox}");
        for _ in 0..17 {
            sandbox.config("deny/net/connect-linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_285() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..17 {
            sandbox.config("deny/net/connect+linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 17, "{sandbox}");
        sandbox.config("deny/net/connect^linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_286() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..18 {
            sandbox.config("filter/net/bind+linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 18, "{sandbox}");
        for _ in 0..18 {
            sandbox.config("filter/net/bind-linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_287() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..18 {
            sandbox.config("filter/net/bind+linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 18, "{sandbox}");
        sandbox.config("filter/net/bind^linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_288() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..19 {
            sandbox.config("filter/net/connect+linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 19, "{sandbox}");
        for _ in 0..19 {
            sandbox.config("filter/net/connect-linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_289() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..19 {
            sandbox.config("filter/net/connect+linklocal4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 19, "{sandbox}");
        sandbox.config("filter/net/connect^linklocal4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_290() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..20 {
            sandbox.config("allow/net/bind+linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 20, "{sandbox}");
        for _ in 0..20 {
            sandbox.config("allow/net/bind-linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_291() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..20 {
            sandbox.config("allow/net/bind+linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 20, "{sandbox}");
        sandbox.config("allow/net/bind^linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_292() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..21 {
            sandbox.config("allow/net/connect+linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 21, "{sandbox}");
        for _ in 0..21 {
            sandbox.config("allow/net/connect-linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_293() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..21 {
            sandbox.config("allow/net/connect+linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 21, "{sandbox}");
        sandbox.config("allow/net/connect^linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_294() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..22 {
            sandbox.config("deny/net/bind+linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 22, "{sandbox}");
        for _ in 0..22 {
            sandbox.config("deny/net/bind-linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_295() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..22 {
            sandbox.config("deny/net/bind+linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 22, "{sandbox}");
        sandbox.config("deny/net/bind^linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_296() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..23 {
            sandbox.config("deny/net/connect+linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 23, "{sandbox}");
        for _ in 0..23 {
            sandbox.config("deny/net/connect-linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_297() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..23 {
            sandbox.config("deny/net/connect+linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 23, "{sandbox}");
        sandbox.config("deny/net/connect^linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_298() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..24 {
            sandbox.config("filter/net/bind+linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 24, "{sandbox}");
        for _ in 0..24 {
            sandbox.config("filter/net/bind-linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_299() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..24 {
            sandbox.config("filter/net/bind+linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 24, "{sandbox}");
        sandbox.config("filter/net/bind^linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_300() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..25 {
            sandbox.config("filter/net/connect+linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 25, "{sandbox}");
        for _ in 0..25 {
            sandbox.config("filter/net/connect-linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_301() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..25 {
            sandbox.config("filter/net/connect+linklocal6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 25, "{sandbox}");
        sandbox.config("filter/net/connect^linklocal6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_302() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..20 {
            sandbox.config("allow/net/bind+linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2 * 20, "{sandbox}");
        for _ in 0..20 {
            sandbox.config("allow/net/bind-linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_303() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..20 {
            sandbox.config("allow/net/bind+linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2 * 20, "{sandbox}");
        sandbox.config("allow/net/bind^linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_304() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..21 {
            sandbox.config("allow/net/connect+linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2 * 21, "{sandbox}");
        for _ in 0..21 {
            sandbox.config("allow/net/connect-linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_305() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..21 {
            sandbox.config("allow/net/connect+linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2 * 21, "{sandbox}");
        sandbox.config("allow/net/connect^linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_306() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..22 {
            sandbox.config("deny/net/bind+linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2 * 22, "{sandbox}");
        for _ in 0..22 {
            sandbox.config("deny/net/bind-linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_307() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..22 {
            sandbox.config("deny/net/bind+linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2 * 22, "{sandbox}");
        sandbox.config("deny/net/bind^linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_308() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..23 {
            sandbox.config("deny/net/connect+linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2 * 23, "{sandbox}");
        for _ in 0..23 {
            sandbox.config("deny/net/connect-linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_309() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..23 {
            sandbox.config("deny/net/connect+linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2 * 23, "{sandbox}");
        sandbox.config("deny/net/connect^linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_310() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..24 {
            sandbox.config("filter/net/bind+linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2 * 24, "{sandbox}");
        for _ in 0..24 {
            sandbox.config("filter/net/bind-linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_311() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..24 {
            sandbox.config("filter/net/bind+linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2 * 24, "{sandbox}");
        sandbox.config("filter/net/bind^linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_312() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..25 {
            sandbox.config("filter/net/connect+linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2 * 25, "{sandbox}");
        for _ in 0..25 {
            sandbox.config("filter/net/connect-linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_313() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..25 {
            sandbox.config("filter/net/connect+linklocal!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2 * 25, "{sandbox}");
        sandbox.config("filter/net/connect^linklocal!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_314() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..2 {
            sandbox.config("allow/net/bind+any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        for _ in 0..2 {
            sandbox.config("allow/net/bind-any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_315() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..2 {
            sandbox.config("allow/net/bind+any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 2, "{sandbox}");
        sandbox.config("allow/net/bind^any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_316() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..3 {
            sandbox.config("allow/net/connect+any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        for _ in 0..3 {
            sandbox.config("allow/net/connect-any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_317() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..3 {
            sandbox.config("allow/net/connect+any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 3, "{sandbox}");
        sandbox.config("allow/net/connect^any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_318() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..6 {
            sandbox.config("filter/net/bind+any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        for _ in 0..6 {
            sandbox.config("filter/net/bind-any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_319() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..6 {
            sandbox.config("filter/net/bind+any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 6, "{sandbox}");
        sandbox.config("filter/net/bind^any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_320() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..7 {
            sandbox.config("filter/net/connect+any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 7, "{sandbox}");
        for _ in 0..7 {
            sandbox.config("filter/net/connect-any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_321() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..7 {
            sandbox.config("filter/net/connect+any4!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 7, "{sandbox}");
        sandbox.config("filter/net/connect^any4!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_322() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..8 {
            sandbox.config("allow/net/bind+any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        for _ in 0..8 {
            sandbox.config("allow/net/bind-any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_323() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..8 {
            sandbox.config("allow/net/bind+any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 8, "{sandbox}");
        sandbox.config("allow/net/bind^any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_324() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..9 {
            sandbox.config("allow/net/connect+any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 9, "{sandbox}");
        for _ in 0..9 {
            sandbox.config("allow/net/connect-any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_325() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..9 {
            sandbox.config("allow/net/connect+any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 9, "{sandbox}");
        sandbox.config("allow/net/connect^any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_326() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..10 {
            sandbox.config("deny/net/bind+any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 10, "{sandbox}");
        for _ in 0..10 {
            sandbox.config("deny/net/bind-any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_327() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..10 {
            sandbox.config("deny/net/bind+any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 10, "{sandbox}");
        sandbox.config("deny/net/bind^any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_328() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..11 {
            sandbox.config("deny/net/connect+any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 11, "{sandbox}");
        for _ in 0..11 {
            sandbox.config("deny/net/connect-any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_329() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..11 {
            sandbox.config("deny/net/connect+any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 11, "{sandbox}");
        sandbox.config("deny/net/connect^any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_330() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..12 {
            sandbox.config("filter/net/bind+any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        for _ in 0..12 {
            sandbox.config("filter/net/bind-any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_331() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..12 {
            sandbox.config("filter/net/bind+any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 12, "{sandbox}");
        sandbox.config("filter/net/bind^any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_332() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..13 {
            sandbox.config("filter/net/connect+any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 13, "{sandbox}");
        for _ in 0..13 {
            sandbox.config("filter/net/connect-any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_333() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..13 {
            sandbox.config("filter/net/connect+any6!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 13, "{sandbox}");
        sandbox.config("filter/net/connect^any6!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_334() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..14 {
            sandbox.config("allow/net/bind+any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 14 * 2, "{sandbox}");
        for _ in 0..14 {
            sandbox.config("allow/net/bind-any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_335() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..14 {
            sandbox.config("allow/net/bind+any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 14 * 2, "{sandbox}");
        sandbox.config("allow/net/bind^any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_336() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..15 {
            sandbox.config("allow/net/connect+any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 15 * 2, "{sandbox}");
        for _ in 0..15 {
            sandbox.config("allow/net/connect-any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_337() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..15 {
            sandbox.config("allow/net/connect+any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 15 * 2, "{sandbox}");
        sandbox.config("allow/net/connect^any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_338() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..16 {
            sandbox.config("deny/net/bind+any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 16 * 2, "{sandbox}");
        for _ in 0..16 {
            sandbox.config("deny/net/bind-any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_339() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..16 {
            sandbox.config("deny/net/bind+any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 16 * 2, "{sandbox}");
        sandbox.config("deny/net/bind^any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_340() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..17 {
            sandbox.config("deny/net/connect+any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 17 * 2, "{sandbox}");
        for _ in 0..17 {
            sandbox.config("deny/net/connect-any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_341() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..17 {
            sandbox.config("deny/net/connect+any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 17 * 2, "{sandbox}");
        sandbox.config("deny/net/connect^any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_342() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..18 {
            sandbox.config("filter/net/bind+any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 18 * 2, "{sandbox}");
        for _ in 0..18 {
            sandbox.config("filter/net/bind-any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_343() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..18 {
            sandbox.config("filter/net/bind+any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 18 * 2, "{sandbox}");
        sandbox.config("filter/net/bind^any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_344() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..19 {
            sandbox.config("filter/net/connect+any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 19 * 2, "{sandbox}");
        for _ in 0..19 {
            sandbox.config("filter/net/connect-any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_345() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        for _ in 0..19 {
            sandbox.config("filter/net/connect+any!0")?;
        }
        assert_eq!(sandbox.cidr_rules.len(), 19 * 2, "{sandbox}");
        sandbox.config("filter/net/connect^any!0")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_346() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/stat+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/stat-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_347() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/stat+/usr/*")?;
        sandbox.config("allow/stat+/usr/**")?;
        sandbox.config("allow/stat+/usr/**")?;
        sandbox.config("allow/stat+/usr/**")?;
        sandbox.config("allow/stat+/usr")?;
        assert_eq!(sandbox.glob_rules.len(), 5, "{sandbox}");
        sandbox.config("allow/stat^/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 2, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_348() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/stat+/usr/**")?;
        sandbox.config("deny/stat+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 2, "{sandbox}");
        sandbox.config("deny/stat-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/stat-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_349() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        sandbox.config("filter/stat+/usr/**")?;
        sandbox.config("filter/stat+/usr/**")?;
        sandbox.config("filter/stat+/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 3, "{sandbox}");
        sandbox.config("filter/stat-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 2, "{sandbox}");
        sandbox.config("filter/stat-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 1, "{sandbox}");
        sandbox.config("filter/stat-/usr/**")?;
        assert_eq!(sandbox.glob_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_config_rules_350() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/bind+1.1.1.1!80")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/bind-1.1.1.1!80")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/bind+1.1.1.1!80")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/bind-1.1.1.1!80")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("allow/net/connect+1.1.1.1!80")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("allow/net/connect-1.1.1.1!80")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        sandbox.config("deny/net/connect+1.1.1.1!80")?;
        assert_eq!(sandbox.cidr_rules.len(), 1, "{sandbox}");
        sandbox.config("deny/net/connect-1.1.1.1!80")?;
        assert_eq!(sandbox.cidr_rules.len(), 0, "{sandbox}");

        Ok(())
    }

    #[test]
    fn sandbox_glob_doublestar_does_not_match_basename() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox.config("allow/read+/dev/**")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/dev"),
            None,
            "/dev =~ /dev/**, {sandbox}"
        );

        Ok(())
    }

    #[test]
    fn sandbox_glob_doublestar_matches_basename_with_slash() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox.config("allow/read+/dev/**")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/dev/"),
            Some(Action::Allow),
            "/dev/ !~ /dev/**, {sandbox}"
        );

        Ok(())
    }

    #[test]
    fn sandbox_glob_doublestar_matches_pathname() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox.config("allow/read+/dev/**")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/dev/null"),
            Some(Action::Allow),
            "/dev/null !~ /dev/**, {sandbox}"
        );

        Ok(())
    }

    #[test]
    fn sandbox_glob_triplestar_matches_basename() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox.config("allow/read+/dev/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/dev"),
            Some(Action::Allow),
            "/dev =~ /dev/***, {sandbox}"
        );

        Ok(())
    }

    #[test]
    fn sandbox_glob_triplestar_matches_basename_with_slash() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox.config("allow/read+/dev/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/dev/"),
            Some(Action::Allow),
            "/dev/ !~ /dev/***, {sandbox}"
        );

        Ok(())
    }

    #[test]
    fn sandbox_glob_triplestar_matches_pathname() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox.config("allow/read+/dev/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/dev/null"),
            Some(Action::Allow),
            "/dev/null !~ /dev/***, {sandbox}"
        );

        Ok(())
    }

    #[test]
    fn sandbox_access_last_matching_rule_wins() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/read:on")?;

        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/etc/passwd"),
            None,
            "{sandbox}"
        );
        sandbox.config("allow/read+/etc/passwd")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        sandbox.config("deny/read+/etc/passwd")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/etc/passwd"),
            Some(Action::Deny),
            "{sandbox}"
        );
        sandbox.config("allow/read+/etc/passwd")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        sandbox.config("deny/read+/etc/passwd")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/etc/passwd"),
            Some(Action::Deny),
            "{sandbox}"
        );
        for _ in 0..2 {
            sandbox.config("deny/read-/etc/passwd")?;
        }
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        for _ in 0..2 {
            sandbox.config("allow/read-/etc/passwd")?;
        }
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_READ, "/etc/passwd"),
            None,
            "{sandbox}"
        );

        assert_eq!(
            sandbox.match_action(Capability::CAP_WRITE, "/etc/passwd"),
            None,
            "{sandbox}"
        );
        sandbox.config("allow/write+/etc/**")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_WRITE, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        sandbox.config("deny/write+/etc/**")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_WRITE, "/etc/passwd"),
            Some(Action::Deny),
            "{sandbox}"
        );
        sandbox.config("allow/write+/etc/**")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_WRITE, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        sandbox.config("deny/write+/etc/**")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_WRITE, "/etc/passwd"),
            Some(Action::Deny),
            "{sandbox}"
        );
        for _ in 0..2 {
            sandbox.config("deny/write-/etc/**")?;
        }
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_WRITE, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        for _ in 0..2 {
            sandbox.config("allow/write-/etc/**")?;
        }
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_WRITE, "/etc/passwd"),
            None,
            "{sandbox}"
        );

        assert_eq!(
            sandbox.match_action(Capability::CAP_EXEC, "/etc/passwd"),
            None,
            "{sandbox}"
        );
        sandbox.config("allow/exec+/etc/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_EXEC, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        sandbox.config("deny/exec+/etc/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_EXEC, "/etc/passwd"),
            Some(Action::Deny),
            "{sandbox}"
        );
        sandbox.config("allow/exec+/etc/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_EXEC, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        sandbox.config("deny/exec+/etc/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_EXEC, "/etc/passwd"),
            Some(Action::Deny),
            "{sandbox}"
        );
        for _ in 0..2 {
            sandbox.config("deny/exec-/etc/***")?;
        }
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_EXEC, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        for _ in 0..2 {
            sandbox.config("allow/exec-/etc/***")?;
        }
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_EXEC, "/etc/passwd"),
            None,
            "{sandbox}"
        );

        assert_eq!(
            sandbox.match_action(Capability::CAP_STAT, "/etc/passwd"),
            None,
            "{sandbox}"
        );
        sandbox.config("allow/stat+/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_STAT, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        sandbox.config("deny/stat+/etc/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_STAT, "/etc/passwd"),
            Some(Action::Deny),
            "{sandbox}"
        );
        sandbox.config("allow/stat+/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_STAT, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        sandbox.config("deny/stat+/etc/passwd")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_STAT, "/etc/passwd"),
            Some(Action::Deny),
            "{sandbox}"
        );
        sandbox.config("deny/stat-/etc/***")?;
        sandbox.config("deny/stat-/etc/passwd")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_STAT, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        for _ in 0..2 {
            sandbox.config("allow/stat-/***")?;
        }
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_STAT, "/etc/passwd"),
            None,
            "{sandbox}"
        );

        assert_eq!(
            sandbox.match_action(Capability::CAP_BIND, "/etc/passwd"),
            None,
            "{sandbox}"
        );
        sandbox.config("allow/net/bind+/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_BIND, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        sandbox.config("deny/net/bind+/etc/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_BIND, "/etc/passwd"),
            Some(Action::Deny),
            "{sandbox}"
        );
        sandbox.config("allow/net/bind+/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_BIND, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        sandbox.config("deny/net/bind+/etc/passwd")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_BIND, "/etc/passwd"),
            Some(Action::Deny),
            "{sandbox}"
        );
        sandbox.config("deny/net/bind-/etc/***")?;
        sandbox.config("deny/net/bind-/etc/passwd")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_BIND, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        for _ in 0..2 {
            sandbox.config("allow/net/bind-/***")?;
        }
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_BIND, "/etc/passwd"),
            None,
            "{sandbox}"
        );

        assert_eq!(
            sandbox.match_action(Capability::CAP_CONNECT, "/etc/passwd"),
            None,
            "{sandbox}"
        );
        sandbox.config("allow/net/connect+/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_CONNECT, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        sandbox.config("deny/net/connect+/etc/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_CONNECT, "/etc/passwd"),
            Some(Action::Deny),
            "{sandbox}"
        );
        sandbox.config("allow/net/connect+/***")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_CONNECT, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        sandbox.config("deny/net/connect+/etc/passwd")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_CONNECT, "/etc/passwd"),
            Some(Action::Deny),
            "{sandbox}"
        );
        sandbox.config("deny/net/connect-/etc/***")?;
        sandbox.config("deny/net/connect-/etc/passwd")?;
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_CONNECT, "/etc/passwd"),
            Some(Action::Allow),
            "{sandbox}"
        );
        for _ in 0..2 {
            sandbox.config("allow/net/connect-/***")?;
        }
        sandbox
            .build_globsets()
            .map_err(|error| io::Error::new(io::ErrorKind::Other, format!("glob:{error}")))?;
        assert_eq!(
            sandbox.match_action(Capability::CAP_CONNECT, "/etc/passwd"),
            None,
            "{sandbox}"
        );

        Ok(())
    }

    #[test]
    fn sandbox_check_filter_ip_port_range() -> IOResult<()> {
        let mut sandbox = Sandbox::default();
        sandbox.config("sandbox/net:on")?;
        sandbox.config("allow/net/connect+any!0")?;

        let addr = "127.0.0.1".parse::<IpAddr>().unwrap();
        assert_eq!(
            sandbox.check_ip(Capability::CAP_CONNECT, addr, 0),
            Action::Allow
        );
        for port in 1..=65535 {
            assert_eq!(
                sandbox.check_ip(Capability::CAP_CONNECT, addr, port),
                Action::Deny,
                "{addr}!{port} {sandbox}"
            );
        }

        sandbox.config("filter/net/connect+any!1-65535")?;
        assert_eq!(
            sandbox.check_ip(Capability::CAP_CONNECT, addr, 0),
            Action::Allow
        );
        for port in 1..=65535 {
            assert_eq!(
                sandbox.check_ip(Capability::CAP_CONNECT, addr, port),
                Action::Filter,
                "{addr}!{port} {sandbox}"
            );
        }

        Ok(())
    }
}
