use std::{
    fs::OpenOptions,
    io::Error,
    mem::zeroed,
    os::unix::{
        fs::OpenOptionsExt,
        io::{AsFd, AsRawFd, BorrowedFd, OwnedFd},
    },
    path::Path,
};

#[cfg(test)]
use strum::IntoEnumIterator;

use crate::landlock::{
    access::Access, compat::private::OptionCompatLevelMut, uapi, AddRuleError, AddRulesError,
    CompatError, CompatLevel, CompatResult, CompatState, Compatible, HandleAccessError,
    HandleAccessesError, PathBeneathError, PathFdError, PrivateAccess, PrivateRule, Rule, Ruleset,
    RulesetCreated, RulesetError, TailoredCompatLevel, TryCompat, ABI,
};
#[cfg(test)]
use crate::landlock::{RulesetAttr, RulesetCreatedAttr};

bitflags::bitflags! {
    /// File system access right.
    ///
    /// Each variant of `AccessFs` is an [access right](https://www.kernel.org/doc/html/latest/userspace-api/landlock.html#access-rights)
    /// for the file system.
    /// A set of access rights can be created with [`BitFlags<AccessFs>`](BitFlags).
    ///
    /// # Warning
    ///
    /// To avoid unknown restrictions **don't use `BitFlags::<AccessFs>::all()` nor `BitFlags::ALL`**,
    /// but use a version you tested and vetted instead,
    /// for instance [`AccessFs::from_all(ABI::V1)`](Access::from_all).
    /// Direct use of **the [`BitFlags`] API is deprecated**.
    /// See [`ABI`] for the rationale and help to test it.
    #[derive(Default)]
    pub struct AccessFs: u64 {
        /// Execute a file.
        const EXECUTE = uapi::LANDLOCK_ACCESS_FS_EXECUTE as u64;
        /// Open a file with write access.
        const WRITE_FILE = uapi::LANDLOCK_ACCESS_FS_WRITE_FILE as u64;
        /// Open a file with read access.
        const READ_FILE = uapi::LANDLOCK_ACCESS_FS_READ_FILE as u64;
        /// Open a directory or list its content.
        const READ_DIR = uapi::LANDLOCK_ACCESS_FS_READ_DIR as u64;
        /// Remove an empty directory or rename one.
        const REMOVE_DIR = uapi::LANDLOCK_ACCESS_FS_REMOVE_DIR as u64;
        /// Unlink (or rename) a file.
        const REMOVE_FILE = uapi::LANDLOCK_ACCESS_FS_REMOVE_FILE as u64;
        /// Create (or rename or link) a character device.
        const MAKE_CHAR = uapi::LANDLOCK_ACCESS_FS_MAKE_CHAR as u64;
        /// Create (or rename) a directory.
        const MAKE_DIR = uapi::LANDLOCK_ACCESS_FS_MAKE_DIR as u64;
        /// Create (or rename or link) a regular file.
        const MAKE_REG = uapi::LANDLOCK_ACCESS_FS_MAKE_REG as u64;
        /// Create (or rename or link) a UNIX domain socket.
        const MAKE_SOCK = uapi::LANDLOCK_ACCESS_FS_MAKE_SOCK as u64;
        /// Create (or rename or link) a named pipe.
        const MAKE_FIFO = uapi::LANDLOCK_ACCESS_FS_MAKE_FIFO as u64;
        /// Create (or rename or link) a block device.
        const MAKE_BLOCK = uapi::LANDLOCK_ACCESS_FS_MAKE_BLOCK as u64;
        /// Create (or rename or link) a symbolic link.
        const MAKE_SYM = uapi::LANDLOCK_ACCESS_FS_MAKE_SYM as u64;
        /// Link or rename a file from or to a different directory.
        const REFER = uapi::LANDLOCK_ACCESS_FS_REFER as u64;
        /// Truncate a file with `truncate(2)`, `ftruncate(2)`, `creat(2)`, or `open(2)` with `O_TRUNC`.
        const TRUNCATE = uapi::LANDLOCK_ACCESS_FS_TRUNCATE as u64;
    }
}

///////////////////////
// enumflags2 compat //
// ////////////////////
#[derive(Copy, Clone, Debug, PartialEq, Eq, Default)]
pub struct BitFlags<T: Sized>(pub T);

impl<T: Sized> BitFlags<T> {
    pub fn bits(&self) -> &T {
        &self.0
    }
}

impl BitFlags<AccessFs> {
    pub fn insert(&mut self, other: AccessFs) {
        self.0.insert(other);
    }
}

impl std::ops::BitOrAssign for BitFlags<AccessFs> {
    fn bitor_assign(&mut self, rhs: Self) {
        self.0 |= rhs.0;
    }
}

impl std::ops::BitXor for BitFlags<AccessFs> {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Self::Output {
        BitFlags(self.0 ^ rhs.0)
    }
}

impl<A> BitFlags<A>
where
    A: Access
        + std::ops::BitAnd<Output = A>
        + std::ops::BitOr<Output = A>
        + PartialEq
        + std::cmp::PartialEq
        + From<AccessFs>
        + Copy
        + Default,
{
    // Checks if the BitFlags is empty
    pub fn is_empty(&self) -> bool {
        // Assuming AccessFs provides an EMPTY constant to represent no flags
        *self == BitFlags(A::from(AccessFs::empty()))
    }

    // Checks if the current BitFlags contains all the flags of another BitFlags
    pub fn contains(&self, other: &Self) -> bool {
        // Assuming bitwise AND will return the common flags of both BitFlags
        (self.0 & other.0) == other.0
    }
}

/*
impl<T: Default> BitFlags<T> {
    pub const EMPTY: Self = BitFlags(Default::default());
}
*/

impl<T: std::ops::BitAnd<Output = T> + Sized> std::ops::BitAnd for BitFlags<T> {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output {
        BitFlags(self.0 & rhs.0)
    }
}

impl<T: std::ops::BitOr<Output = T> + Copy + Sized> std::ops::BitOr for BitFlags<T> {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self::Output {
        BitFlags(<T as std::ops::BitOr>::bitor(self.0, rhs.0))
    }
}

impl<T: std::ops::Not<Output = T>> std::ops::Not for BitFlags<T> {
    type Output = Self;

    fn not(self) -> Self::Output {
        BitFlags(!(self.0))
    }
}

impl<T: Sized + From<AccessFs>> BitFlags<T> {
    pub fn all() -> BitFlags<T> {
        BitFlags(T::from(AccessFs::all()))
    }
}

pub trait BitFlag {}
impl BitFlag for AccessFs {}
///////////////////////

impl Access for AccessFs {
    // Roughly read (i.e. not all FS actions are handled).
    fn from_read(abi: ABI) -> BitFlags<Self> {
        match abi {
            ABI::Unsupported => BitFlags(AccessFs::empty()),
            ABI::V1 | ABI::V2 | ABI::V3 => {
                BitFlags(AccessFs::EXECUTE | AccessFs::READ_FILE | AccessFs::READ_DIR)
            }
        }
    }

    // Roughly write (i.e. not all FS actions are handled).
    fn from_write(abi: ABI) -> BitFlags<Self> {
        match abi {
            ABI::Unsupported => BitFlags(AccessFs::empty()),
            ABI::V1 => BitFlags(
                AccessFs::WRITE_FILE
                    | AccessFs::REMOVE_DIR
                    | AccessFs::REMOVE_FILE
                    | AccessFs::MAKE_CHAR
                    | AccessFs::MAKE_DIR
                    | AccessFs::MAKE_REG
                    | AccessFs::MAKE_SOCK
                    | AccessFs::MAKE_FIFO
                    | AccessFs::MAKE_BLOCK
                    | AccessFs::MAKE_SYM,
            ),
            ABI::V2 => Self::from_write(ABI::V1) | BitFlags(AccessFs::REFER),
            ABI::V3 => Self::from_write(ABI::V2) | BitFlags(AccessFs::TRUNCATE),
        }
    }
}

#[test]
fn consistent_access_fs_rw() {
    for abi in ABI::iter() {
        let access_all = AccessFs::from_all(abi);
        let access_read = AccessFs::from_read(abi);
        let access_write = AccessFs::from_write(abi);
        assert_eq!(access_read, !access_write & access_all);
        assert_eq!(access_read | access_write, access_all);
    }
}

impl AccessFs {
    /// Gets the access rights legitimate for non-directory files.
    pub fn from_file(abi: ABI) -> BitFlags<Self> {
        Self::from_all(abi)
            & BitFlags(
                AccessFs::READ_FILE | AccessFs::WRITE_FILE | AccessFs::EXECUTE | AccessFs::TRUNCATE,
            )
    }
}

impl PrivateAccess for AccessFs {
    fn ruleset_handle_access(
        ruleset: &mut Ruleset,
        access: BitFlags<Self>,
    ) -> Result<(), HandleAccessesError> {
        // We need to record the requested accesses for PrivateRule::check_consistency().
        ruleset.requested_handled_fs |= access;
        ruleset.actual_handled_fs |= match access
            .try_compat(
                ruleset.compat.abi(),
                ruleset.compat.level,
                &mut ruleset.compat.state,
            )
            .map_err(HandleAccessError::Compat)?
        {
            Some(a) => a,
            None => return Ok(()),
        };
        Ok(())
    }

    fn into_add_rules_error(error: AddRuleError<Self>) -> AddRulesError {
        AddRulesError::Fs(error)
    }

    fn into_handle_accesses_error(error: HandleAccessError<Self>) -> HandleAccessesError {
        HandleAccessesError::Fs(error)
    }
}

// XXX: What should we do when a stat call failed?
fn is_file<F>(fd: F) -> Result<bool, Error>
where
    F: AsFd,
{
    unsafe {
        let mut stat = zeroed();
        match nix::libc::fstat(fd.as_fd().as_raw_fd(), &mut stat) {
            0 => Ok((stat.st_mode & nix::libc::S_IFMT) != nix::libc::S_IFDIR),
            _ => Err(Error::last_os_error()),
        }
    }
}

/// Landlock rule for a file hierarchy.
///
/// # Example
///
/// ```
/// use syd::landlock::*;
///
/// fn home_dir() -> Result<PathBeneath<PathFd>, PathFdError> {
///     Ok(PathBeneath::new(
///         PathFd::new("/home")?,
///         BitFlags(AccessFs::READ_DIR),
///     ))
/// }
/// ```
#[cfg_attr(test, derive(Debug))]
pub struct PathBeneath<F> {
    attr: uapi::landlock_path_beneath_attr,
    // Ties the lifetime of a file descriptor to this object.
    parent_fd: F,
    allowed_access: BitFlags<AccessFs>,
    compat_level: Option<CompatLevel>,
}

impl<F> PathBeneath<F>
where
    F: AsFd,
{
    /// Creates a new `PathBeneath` rule identifying the `parent` directory of a file hierarchy,
    /// or just a file, and allows `access` on it.
    /// The `parent` file descriptor will be automatically closed with the returned `PathBeneath`.
    pub fn new<A>(parent: F, access: A) -> Self
    where
        A: Into<BitFlags<AccessFs>>,
    {
        PathBeneath {
            attr: uapi::landlock_path_beneath_attr {
                // Invalid access-rights until try_compat() is called.
                allowed_access: 0,
                parent_fd: parent.as_fd().as_raw_fd(),
            },
            parent_fd: parent,
            allowed_access: access.into(),
            compat_level: None,
        }
    }

    fn sync_attr(mut self) -> Self {
        // Synchronizes rule attributes.
        self.attr.allowed_access = self.allowed_access.bits().bits();
        self
    }
}

impl<F> TryCompat<AccessFs> for PathBeneath<F>
where
    F: AsFd,
{
    fn try_compat_children<L>(
        mut self,
        abi: ABI,
        parent_level: L,
        compat_state: &mut CompatState,
    ) -> Result<Option<Self>, CompatError<AccessFs>>
    where
        L: Into<CompatLevel>,
    {
        // Checks with our own compatibility level, if any.
        self.allowed_access = match self.allowed_access.try_compat(
            abi,
            self.tailored_compat_level(parent_level),
            compat_state,
        )? {
            Some(a) => a,
            None => return Ok(None),
        };
        Ok(Some(self))
    }

    fn try_compat_inner(
        mut self,
        _abi: ABI,
    ) -> Result<CompatResult<Self, AccessFs>, CompatError<AccessFs>> {
        // self.attr.allowed_access was updated with try_compat_children(), called by try_compat().

        // Gets subset of valid accesses according the FD type.
        let valid_access =
            if is_file(&self.parent_fd).map_err(|e| PathBeneathError::StatCall { source: e })? {
                self.allowed_access
                    & BitFlags(
                        AccessFs::READ_FILE
                            | AccessFs::WRITE_FILE
                            | AccessFs::EXECUTE
                            | AccessFs::TRUNCATE,
                    )
            } else {
                self.allowed_access
            };

        if self.allowed_access != valid_access {
            let error = PathBeneathError::DirectoryAccess {
                access: self.allowed_access,
                incompatible: self.allowed_access ^ valid_access,
            }
            .into();
            self.allowed_access = valid_access;
            // Linux would return EINVAL.
            Ok(CompatResult::Partial(self.sync_attr(), error))
        } else {
            Ok(CompatResult::Full(self.sync_attr()))
        }
    }
}

#[test]
fn path_beneath_try_compat() {
    use crate::landlock::*;

    let abi = ABI::V1;

    for file in &["/etc/passwd", "/dev/null"] {
        // TODO: test try_compat_children

        let mut compat_state = CompatState::Init;
        let ro_access = BitFlags(AccessFs::READ_DIR | AccessFs::READ_FILE);
        assert!(matches!(
            PathBeneath::new(PathFd::new(file).unwrap(), ro_access)
                .try_compat(abi, CompatLevel::HardRequirement, &mut compat_state)
                .unwrap_err(),
            CompatError::PathBeneath(PathBeneathError::DirectoryAccess { access, incompatible })
                if access == ro_access && incompatible == BitFlags(AccessFs::READ_DIR)
        ));

        let mut compat_state = CompatState::Init;
        assert!(matches!(
            PathBeneath::new(PathFd::new(file).unwrap(), BitFlags(AccessFs::empty()))
                .try_compat(abi, CompatLevel::BestEffort, &mut compat_state)
                .unwrap_err(),
            CompatError::Access(AccessError::Empty)
        ));
    }

    let full_access = AccessFs::from_all(ABI::V1);
    for compat_level in &[
        CompatLevel::BestEffort,
        CompatLevel::SoftRequirement,
        CompatLevel::HardRequirement,
    ] {
        let mut compat_state = CompatState::Init;
        let raw_access = PathBeneath::new(PathFd::new("/").unwrap(), full_access)
            .try_compat(abi, *compat_level, &mut compat_state)
            .unwrap()
            .unwrap()
            .attr
            .allowed_access;
        assert_eq!(raw_access, full_access.bits().bits());
        assert_eq!(compat_state, CompatState::Full);
    }
}

impl<F> OptionCompatLevelMut for PathBeneath<F> {
    fn as_option_compat_level_mut(&mut self) -> &mut Option<CompatLevel> {
        &mut self.compat_level
    }
}

impl<F> OptionCompatLevelMut for &mut PathBeneath<F> {
    fn as_option_compat_level_mut(&mut self) -> &mut Option<CompatLevel> {
        &mut self.compat_level
    }
}

impl<F> Compatible for PathBeneath<F> {}

impl<F> Compatible for &mut PathBeneath<F> {}

#[test]
fn path_beneath_compatibility() {
    let mut path = PathBeneath::new(PathFd::new("/").unwrap(), AccessFs::from_all(ABI::V1));
    let path_ref = &mut path;

    let level = path_ref.as_option_compat_level_mut();
    assert_eq!(level, &None);
    assert_eq!(
        <Option<CompatLevel> as Into<CompatLevel>>::into(*level),
        CompatLevel::BestEffort
    );

    path_ref.set_compatibility(CompatLevel::SoftRequirement);
    assert_eq!(
        path_ref.as_option_compat_level_mut(),
        &Some(CompatLevel::SoftRequirement)
    );

    path.set_compatibility(CompatLevel::HardRequirement);
}

// It is useful for documentation generation to explicitely implement Rule for every types, instead
// of doing it generically.
impl<F> Rule<AccessFs> for PathBeneath<F> where F: AsFd {}

impl<F> PrivateRule<AccessFs> for PathBeneath<F>
where
    F: AsFd,
{
    fn as_ptr(&self) -> *const nix::libc::c_void {
        std::ptr::addr_of!(self.attr) as *const _
    }

    fn get_type_id(&self) -> uapi::landlock_rule_type {
        uapi::landlock_rule_type_LANDLOCK_RULE_PATH_BENEATH
    }

    fn get_flags(&self) -> u32 {
        0
    }

    fn check_consistency(&self, ruleset: &RulesetCreated) -> Result<(), AddRulesError> {
        // Checks that this rule doesn't contain a superset of the access-rights handled by the
        // ruleset.  This check is about requested access-rights but not actual access-rights.
        // Indeed, we want to get a deterministic behavior, i.e. not based on the running kernel
        // (which is handled by Ruleset and RulesetCreated).
        if ruleset.requested_handled_fs.contains(&self.allowed_access) {
            Ok(())
        } else {
            Err(AddRuleError::UnhandledAccess {
                access: self.allowed_access,
                incompatible: self.allowed_access & !ruleset.requested_handled_fs,
            }
            .into())
        }
    }
}

#[ignore]
#[test]
fn path_beneath_check_consistency() {
    use crate::landlock::*;

    let ro_access = BitFlags(AccessFs::READ_DIR | AccessFs::READ_FILE);
    let rx_access = BitFlags(AccessFs::EXECUTE | AccessFs::READ_FILE);
    assert!(matches!(
        Ruleset::from(ABI::Unsupported)
            .handle_access(ro_access)
            .unwrap()
            .create()
            .unwrap()
            .add_rule(PathBeneath::new(PathFd::new("/").unwrap(), rx_access))
            .unwrap_err(),
        RulesetError::AddRules(AddRulesError::Fs(AddRuleError::UnhandledAccess { access, incompatible }))
            if access == rx_access && incompatible == BitFlags(AccessFs::EXECUTE)
    ));
}

/// Simple helper to open a file or a directory with the `O_PATH` flag.
///
/// This is the recommended way to identify a path
/// and manage the lifetime of the underlying opened file descriptor.
/// Indeed, using other [`AsFd`] implementations such as [`File`] brings more complexity
/// and may lead to unexpected errors (e.g., denied access).
///
/// [`File`]: std::fs::File
///
/// # Example
///
/// ```
/// use syd::landlock::*;
///
/// fn allowed_root_dir(access: AccessFs) -> Result<PathBeneath<PathFd>, PathFdError> {
///     let fd = PathFd::new("/")?;
///     Ok(PathBeneath::new(fd, BitFlags(access)))
/// }
/// ```
#[cfg_attr(test, derive(Debug))]
pub struct PathFd {
    fd: OwnedFd,
}

impl PathFd {
    pub fn new<T>(path: T) -> Result<Self, PathFdError>
    where
        T: AsRef<Path>,
    {
        Ok(PathFd {
            fd: OpenOptions::new()
                .read(true)
                // If the O_PATH is not supported, it is automatically ignored (Linux < 2.6.39).
                .custom_flags(nix::libc::O_PATH | nix::libc::O_CLOEXEC)
                .open(path.as_ref())
                .map_err(|e| PathFdError::OpenCall {
                    source: e,
                    path: path.as_ref().into(),
                })?
                .into(),
        })
    }
}

impl AsFd for PathFd {
    fn as_fd(&self) -> BorrowedFd<'_> {
        self.fd.as_fd()
    }
}

#[ignore]
#[test]
fn path_fd() {
    use std::{fs::File, io::Read};

    PathBeneath::new(PathFd::new("/").unwrap(), BitFlags(AccessFs::EXECUTE));
    PathBeneath::new(File::open("/").unwrap(), BitFlags(AccessFs::EXECUTE));

    let mut buffer = [0; 1];
    // Checks that PathFd really returns an FD opened with O_PATH (Bad file descriptor error).
    File::from(PathFd::new("/etc/passwd").unwrap().fd)
        .read(&mut buffer)
        .unwrap_err();
}

/// Helper to quickly create an iterator of PathBeneath rules.
///
/// Silently ignores paths that cannot be opened, and automatically adjust access rights according
/// to file types when possible.
///
/// # Example
///
/// ```
/// use syd::landlock::{
///     path_beneath_rules, Access, AccessFs, Ruleset, RulesetAttr, RulesetCreatedAttr,
///     RulesetError, RulesetStatus, ABI,
/// };
///
/// fn restrict_thread() -> Result<(), RulesetError> {
///     let abi = ABI::V1;
///     let status = Ruleset::default()
///         .handle_access(AccessFs::from_all(abi))?
///         .create()?
///         // Read-only access to /usr, /etc and /dev.
///         .add_rules(path_beneath_rules(&["/usr", "/etc", "/dev"], AccessFs::from_read(abi)))?
///         // Read-write access to /home and /tmp.
///         .add_rules(path_beneath_rules(&["/home", "/tmp"], AccessFs::from_all(abi)))?
///         .restrict_self()?;
///     match status.ruleset {
///         // The FullyEnforced case must be tested by the developer.
///         RulesetStatus::FullyEnforced => println!("Fully sandboxed."),
///         RulesetStatus::PartiallyEnforced => println!("Partially sandboxed."),
///         // Users should be warned that they are not protected.
///         RulesetStatus::NotEnforced => println!("Not sandboxed! Please update your kernel."),
///     }
///     Ok(())
/// }
/// ```
pub fn path_beneath_rules<I, P, A>(
    paths: I,
    access: A,
) -> impl Iterator<Item = Result<PathBeneath<PathFd>, RulesetError>>
where
    I: IntoIterator<Item = P>,
    P: AsRef<Path>,
    A: Into<BitFlags<AccessFs>>,
{
    let access = access.into();
    paths.into_iter().filter_map(move |p| match PathFd::new(p) {
        Ok(f) => {
            let valid_access = match is_file(&f) {
                Ok(true) => {
                    access
                        & BitFlags(
                            AccessFs::READ_FILE
                                | AccessFs::WRITE_FILE
                                | AccessFs::EXECUTE
                                | AccessFs::TRUNCATE,
                        )
                }
                // If the stat call failed, let's blindly rely on the requested access rights.
                Err(_) | Ok(false) => access,
            };
            Some(Ok(PathBeneath::new(f, valid_access)))
        }
        Err(_) => None,
    })
}

#[ignore]
#[test]
fn path_beneath_rules_iter() {
    let _ = Ruleset::default()
        .handle_access(AccessFs::from_all(ABI::V1))
        .unwrap()
        .create()
        .unwrap()
        .add_rules(path_beneath_rules(
            &["/usr", "/opt", "/does-not-exist", "/root"],
            BitFlags(AccessFs::EXECUTE),
        ))
        .unwrap();
}
