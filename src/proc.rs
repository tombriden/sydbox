//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/proc.rs: /proc utilities
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// Used only by proc_cmdline.
#[cfg(feature = "log")]
use std::ffi::OsString;
#[cfg(feature = "log")]
use std::os::unix::ffi::OsStringExt;
use std::{
    ffi::OsStr,
    fs::{read_dir, read_link, File},
    io::{Read, Write},
    os::{fd::RawFd, unix::ffi::OsStrExt},
    path::{Component, Path, PathBuf},
};

use nix::{errno::Errno, libc::pid_t, sys::stat::Mode, unistd::Pid};
use procfs::process::{MMapPath, Process};
use smallvec::SmallVec;

const TGID_PREFIX: &[u8] = b"Tgid:\t";
const UMASK_PREFIX: &[u8] = b"Umask:\t";

/// Retrieves the thread group ID (TGID) for the specified thread ID (TID).
pub fn proc_tgid(tid: Pid) -> Result<Pid, Errno> {
    // Construct path to the appropriate status file
    let mut buf = itoa::Buffer::new();
    let mut proc = PathBuf::from("/proc");
    proc.push(buf.format(tid.as_raw()));
    proc.push("status");

    // Open the file
    let mut file = File::open(proc).map_err(|_| Errno::ENOENT)?;
    let mut buf = [0; 84]; // Read the first 84 bytes where the tgid is likely to be.
    let bytes_read = file.read(&mut buf).map_err(|_| Errno::ENOENT)?;

    // Search for "Tgid:" pattern directly in bytes
    if let Some(position) = buf[..bytes_read]
        .windows(TGID_PREFIX.len())
        .position(|window| window == TGID_PREFIX)
    {
        #[allow(clippy::arithmetic_side_effects)]
        let start = position + TGID_PREFIX.len();
        // Find the end of the tgid value assuming it ends with a newline.
        if let Some(end) = buf[start..].iter().position(|&b| b == b'\n') {
            // Parse tgid directly from bytes
            let mut tgid: pid_t = 0;
            #[allow(clippy::arithmetic_side_effects)]
            for &digit in &buf[start..start + end] {
                tgid = tgid * 10 + pid_t::from(digit - b'0');
            }
            return Ok(Pid::from_raw(tgid));
        }
    }

    Err(Errno::EIO)
}

/// Get the umask of the given `Pid`.
pub(crate) fn proc_umask(pid: Pid) -> Result<Mode, Errno> {
    // Construct path to the appropriate status file
    let mut buf = itoa::Buffer::new();
    let mut proc = PathBuf::from("/proc");
    proc.push(buf.format(pid.as_raw()));
    proc.push("status");

    // Open the file
    let mut file = File::open(proc).map_err(|_| Errno::ENOENT)?;
    let mut buf = [0u8; 42]; // Read the first 42 bytes where the umask is likely to be.
    let bytes_read = file.read(&mut buf).map_err(|_| Errno::ENOENT)?;

    // Search for "Umask:" pattern directly in bytes
    if let Some(position) = buf[..bytes_read]
        .windows(UMASK_PREFIX.len())
        .position(|window| window == UMASK_PREFIX)
    {
        #[allow(clippy::arithmetic_side_effects)]
        let start = position + UMASK_PREFIX.len();
        // Find the end of the umask value assuming it ends with a newline.
        if let Some(end) = buf[start..].iter().position(|&b| b == b'\n') {
            // Parse umask directly from bytes
            let mut umask = 0u32;
            #[allow(clippy::arithmetic_side_effects)]
            for &digit in &buf[start..start + end] {
                umask = umask * 8 + u32::from(digit - b'0');
            }
            return Ok(Mode::from_bits_truncate(umask));
        }
    }

    Err(Errno::EIO)
}

/// Retrieves the command line of the specified process ID (PID)
/// concatenated as a single string.
///
/// This function reads the `/proc/<pid>/cmdline` file and concatenates
/// the arguments using spaces. The function takes care of replacing null
/// bytes (`'\0'`) with spaces to format the command line as a readable string.
///
/// # Arguments
///
/// * `pid` - The process ID for which to retrieve the command line.
///
/// # Returns
///
/// * `Ok(String)` - The command line of the process as a single string.
/// * `Err(Errno)` - An error, depending on the issue encountered while reading
///                  or processing the cmdline file.
///
/// # Examples
///
/// ```rust
/// use nix::unistd::Pid;
/// use syd::proc::proc_cmdline;
///
/// let cmdline = proc_cmdline(Pid::this());
/// assert!(cmdline.is_ok());
/// ```
#[cfg(feature = "log")]
pub fn proc_cmdline(pid: Pid) -> Result<OsString, Errno> {
    const LIMIT: usize = 256;

    // Construct path to the appropriate cmdline file.
    let mut buf = itoa::Buffer::new();
    let mut cmd = PathBuf::from("/proc");
    cmd.push(buf.format(pid.as_raw()));
    cmd.push("cmdline");

    let mut file = File::open(cmd).map_err(|_| Errno::ENOENT)?;
    let mut buf = vec![0u8; LIMIT];
    let bytes_read = file.read(&mut buf).map_err(|_| Errno::ENOENT)?;

    if bytes_read == 0 {
        return Ok(OsString::new()); // empty cmdline
    }

    // Determine if EOF was reached or if we hit the limit
    if bytes_read == LIMIT {
        // Check if the last byte read is not a null byte, indicating there's more data
        if buf[LIMIT - 1] != 0 {
            // Append ellipsis to indicate truncation
            buf.extend_from_slice("…".as_bytes());
        }
    } else {
        // If EOF was hit before the limit, resize the buffer to bytes_read
        buf.resize(bytes_read, 0);
    }

    // Replace null bytes with spaces
    for byte in &mut buf {
        if *byte == 0 {
            *byte = b' ';
        }
    }

    Ok(OsString::from_vec(buf))
}

/// Retrieves the command name (comm) of the specified process ID (PID)
/// as a single string.
///
/// This function reads the `/proc/<pid>/comm` file. It reads up to 16 characters,
/// which is typically sufficient for process names.
///
/// # Arguments
///
/// * `pid` - The process ID for which to retrieve the command name.
///
/// # Returns
///
/// * `Ok(String)` - The command name of the process as a single string.
/// * `Err(Errno)` - An error, depending on the issue encountered while reading
///                  the comm file.
///
/// # Examples
///
/// ```rust
/// use nix::unistd::Pid;
/// use syd::proc::proc_comm;
///
/// let comm = proc_comm(Pid::this());
/// assert!(comm.is_ok());
/// ```
#[cfg(not(feature = "log"))]
pub fn proc_comm(pid: Pid) -> Result<String, Errno> {
    const LIMIT: usize = 16;
    let mut buf = itoa::Buffer::new();
    let mut path = PathBuf::from("/proc");
    path.push(buf.format(pid.as_raw()));
    path.push("comm");

    let mut file = File::open(path).map_err(|_| Errno::last())?;
    let mut data = Vec::with_capacity(LIMIT);

    // Read up to LIMIT characters or until EOF
    file.read_to_end(&mut data).map_err(|_| Errno::last())?;

    let comm = String::from_utf8_lossy(&data);
    Ok(comm.trim_end().to_string())
}

/// Retrieves the current working directory (CWD) of the specified process ID (PID).
///
/// This function reads the symbolic link `/proc/<pid>/cwd` to determine the CWD.
///
/// # Arguments
///
/// * `pid` - The process ID for which to retrieve the current working directory.
///
/// # Returns
///
/// * `Ok(PathBuf)` - The current working directory of the process.
/// * `Err(Errno)` - An error, depending on the issue encountered while reading
///                  or resolving the `cwd` symlink.
///
/// # Examples
///
/// ```rust
/// use nix::unistd::Pid;
/// use syd::proc::proc_cwd;
///
/// let cwd = proc_cwd(Pid::this());
/// assert!(cwd.is_ok());
/// ```
pub fn proc_cwd(pid: Pid) -> Result<PathBuf, Errno> {
    let mut buf = itoa::Buffer::new();
    let mut path = PathBuf::from("/proc");
    path.push(buf.format(pid.as_raw()));
    path.push("cwd");
    read_link(path)
        .map(|p| p.to_path_buf())
        .map_err(|_| Errno::last())
}

/// Reads the tty number from /proc/[pid]/stat and figures out the corresponding /dev/tty device node path.
pub fn proc_tty(pid: Pid) -> Result<PathBuf, Errno> {
    let mut buf = itoa::Buffer::new();
    let mut path = PathBuf::from("/proc");
    path.push(buf.format(pid.as_raw()));
    path.push("stat");

    let mut file = File::open(path).map_err(|_| Errno::ENOENT)?;
    let mut buf = [0u8; 84]; // Read the first 84 bytes where the tty_nr is likely to be.
    file.read(&mut buf).map_err(|_| Errno::ENOENT)?;

    // Count fields and accumulate tty_nr directly when in the 7th field.
    let mut fields = 0;
    let mut tty_nr = 0;

    #[allow(clippy::arithmetic_side_effects)]
    for &b in &buf {
        // Increment field count on space and skip to next byte.
        if b == b' ' {
            fields += 1;
            continue;
        }

        match fields {
            0..=5 => {}
            6 => {
                // Calculate tty_nr when in the 7th field.
                // We don't handle unexpected non-digit on purpose.
                tty_nr = tty_nr * 10 + i32::from(b - b'0');
            }
            _ => {
                // Break after processing the 7th field.
                break;
            }
        }
    }

    // Convert tty_nr to the corresponding /dev/tty device node path.
    if tty_nr <= 0 {
        // Process has no controlling terminal
        Err(Errno::ENXIO)
    } else {
        // Construct the path based on the major and minor device numbers.
        let major = (tty_nr >> 8) & 0xfff;
        let minor = (tty_nr & 0xff) | ((tty_nr >> 12) & 0xfff00);

        // Determine the correct device path.
        match major {
            // Unix 98 PTYs (e.g., /dev/pts/N)
            136 => {
                let mut buf = itoa::Buffer::new();
                let mut path = PathBuf::from("/dev/pts");
                path.push(buf.format(minor));
                Ok(path)
            }
            // Standard TTYs (e.g., /dev/ttyN)
            14 => {
                let mut buf = itoa::Buffer::new();
                Ok(PathBuf::from("/dev/tty".to_owned() + buf.format(minor)))
            }
            // Other cases: unknown or unsupported major number
            _ => Err(Errno::ENXIO),
        }
    }
}

/// Sets the maximum number of user namespaces.
///
/// This function opens the file `/proc/sys/user/max_user_namespaces`
/// and writes the value `1` into it. It is used to limit the number of
/// user namespaces that can be created.
///
/// # Returns
/// * `Ok(())` on success.
/// * `Err(Errno)` on failure, with the error converted from `std::io::Error`.
pub fn proc_limit_userns() -> Result<(), Errno> {
    match File::create("/proc/sys/user/max_user_namespaces") {
        Ok(mut file) => match file.write_all(b"1") {
            Ok(_) => Ok(()),
            Err(e) => Err(e.raw_os_error().map(Errno::from_i32).unwrap_or(Errno::EIO)),
        },
        Err(e) => Err(e.raw_os_error().map(Errno::from_i32).unwrap_or(Errno::EIO)),
    }
}

/// Checks if the number of tasks across all processes in the system
/// exceeds the given limit by inspecting the `/proc` filesystem.
///
/// This function iterates over each entry in the `/proc` directory,
/// assuming each numeric directory name represents a process ID. For
/// each process, it counts the number of tasks (threads) by counting
/// entries in the `/proc/[pid]/task` directory. The function skips
/// over the initial entry `/proc/1`.
///
/// # Returns
/// `Ok(true)`: Number of tasks reach the given limit.
/// `Err(Errno)`: An error occurred during the operation, excluding
/// ENOENT (file not found), EACCES (access denied), EPERM (permission
/// denied), and ESRCH (process not found) errors, which are ignored.
///
/// # Errors
/// This function returns an error for any issue encountered while
/// reading the `/proc` directory or task subdirectories, except for
/// ENOENT, EACCES, EPERM, and ESRCH errors, which are ignored. The
/// error is wrapped in an `Errno` type.
pub fn proc_task_limit(max: usize) -> Result<bool, Errno> {
    let proc_entries = read_dir("/proc")
        .map_err(|e| Errno::from_i32(e.raw_os_error().unwrap_or(nix::libc::EINVAL)))?;

    let result = proc_entries
        .filter_map(Result::ok)
        .filter(|entry| entry.file_name().as_bytes() != b"1" && is_numeric(&entry.file_name()))
        .try_fold(
            (0usize, SmallVec::<[PathBuf; 256]>::new()),
            |(count, mut entries), entry| {
                let count = count.saturating_add(1);
                if count >= max {
                    // Early termination with Err to break out of the try_fold
                    return Err(());
                }
                entries.push(entry.path());
                Ok((count, entries))
            },
        );

    // Unwrap the result of try_fold, handling early termination
    let (mut pid_count, eligible_entries) = match result {
        Ok(data) => data,
        Err(_) => return Ok(true),
    };

    // Step 2: Count tasks in each PID
    for path in eligible_entries {
        match read_dir(&path.join("task")) {
            Ok(tasks) => {
                pid_count = pid_count.saturating_add(tasks.count()).saturating_sub(1);
                if pid_count >= max {
                    return Ok(true);
                }
            }
            Err(error) => match error.raw_os_error() {
                Some(
                    nix::libc::ENOENT | nix::libc::EPERM | nix::libc::EACCES | nix::libc::ESRCH,
                ) => {}
                Some(error) => return Err(Errno::from_i32(error)),
                None => return Err(Errno::EINVAL),
            },
        }
    }

    Ok(false)
}

#[inline]
fn is_numeric(name: &OsStr) -> bool {
    name.as_bytes().iter().all(|&b| b.is_ascii_digit())
}

// 1. The path must start with /proc.
// 2. The second component must be a numeric PID equal to the TGID of the given PID.
// 3. An optional task/[TID] part, where [TID] is a numeric PID.
// 4. Finally, it checks for an fd component followed by a numeric file descriptor.
// SAFETY: This function returns Err(Errno::ELOOP) if PID is present but not equal to given PID.
// This function does not allocate.
#[allow(clippy::cognitive_complexity)]
pub(crate) fn proc_fd<P: AsRef<Path>>(pid: Pid, path: P) -> Result<Option<RawFd>, Errno> {
    let mut components = path.as_ref().components();

    // Check for the root directory component "/"
    if components.next() != Some(Component::RootDir) {
        return Ok(None);
    }

    // Check if the first component is "proc"
    match components.next() {
        Some(Component::Normal(os_str)) if os_str.as_bytes() == b"proc" => {}
        _ => return Ok(None),
    }

    // Check if the second component starts with an ASCII digit
    match components.next() {
        Some(Component::Normal(pid_str)) if starts_with_ascii_digit(pid_str) => {
            // Optionally handle "task/[TID]" part
            let next = components.next();
            let mut has_task = false;
            if let Some(Component::Normal(os_str)) = next {
                match os_str.as_bytes() {
                    b"cwd" | b"exe" | b"root" => {
                        // SAFETY: NO_MAGICLINKS!
                        return match parse_ascii_digits_to_pid(pid_str) {
                            p if p == pid.as_raw() || p == proc_tgid(pid)?.as_raw() => Ok(None),
                            _ => Err(Errno::ELOOP),
                        };
                    }
                    b"task" => {
                        has_task = true;
                        match components.next() {
                            Some(Component::Normal(os_str)) if starts_with_ascii_digit(os_str) => {}
                            _ => return Ok(None),
                        }
                    }
                    b"fd" => { // If not "task", it should be "fd" directly
                         // fall through
                    }
                    _ => return Ok(None),
                }
            }

            // If it has "task", the next should be "fd"
            // Also handle task/pid/{cwd,exe,root} securely.
            if has_task {
                match components.next() {
                    Some(Component::Normal(os_str)) if os_str.as_bytes() == b"fd" => {}
                    Some(Component::Normal(os_str))
                        if matches!(os_str.as_bytes(), b"cwd" | b"exe" | b"root") =>
                    {
                        // SAFETY: NO_MAGICLINKS!
                        return match parse_ascii_digits_to_pid(pid_str) {
                            p if p == pid.as_raw() || p == proc_tgid(pid)?.as_raw() => Ok(None),
                            _ => Err(Errno::ELOOP),
                        };
                    }
                    _ => return Ok(None),
                }
            }

            // Finally, check if the last component is a numeric FD (starting with a digit here is enough)
            match components.next() {
                Some(Component::Normal(os_str)) if starts_with_ascii_digit(os_str) => {
                    if components.next().is_none() {
                        // SAFETY: NO_MAGICLINKS!
                        // Only check here so we only deny /proc/pid/fd/$n links.
                        match parse_ascii_digits_to_pid(pid_str) {
                            p if p == pid.as_raw() || p == proc_tgid(pid)?.as_raw() => {
                                Ok(Some(parse_ascii_digits_to_raw_fd(os_str)))
                            }
                            _ => Err(Errno::ELOOP),
                        }
                    } else {
                        Ok(None)
                    }
                }
                _ => Ok(None),
            }
        }
        _ => Ok(None),
    }
}

#[inline]
fn parse_ascii_digits_to_pid(s: &OsStr) -> pid_t {
    #[allow(clippy::arithmetic_side_effects)]
    s.as_bytes()
        .iter()
        .fold(0, |acc, &b| acc * 10 + pid_t::from(b - b'0'))
}

#[inline]
fn parse_ascii_digits_to_raw_fd(s: &OsStr) -> RawFd {
    #[allow(clippy::arithmetic_side_effects)]
    s.as_bytes()
        .iter()
        .fold(0, |acc, &b| acc * 10 + RawFd::from(b - b'0'))
}

// Helper function to check if the OsStr starts with an ASCII digit
#[inline]
pub(crate) fn starts_with_ascii_digit(s: &OsStr) -> bool {
    s.as_bytes().first().map_or(false, |&b| b.is_ascii_digit())
}

/// Checks whether process memory usage is within the give maximum.
///
/// This function uses the `procfs` crate to obtain detailed memory maps
/// from `/proc/[pid]/smaps`. It sums multiple memory usage values reported in these maps
/// to calculate a more comprehensive total memory usage.
///
/// # Arguments
///
/// * `process` - `Process` instance representing the process.
///
/// # Returns
///
/// This function returns a `Result<bool, Errno>`. It returns Ok(true)
/// if the limit was exceeded Ok(false) otherwise. On failure, it
/// returns `Errno`.
///
/// # Errors
///
/// This function returns an error if it fails to retrieve the process's memory maps,
/// typically due to insufficient permissions or an invalid process ID.
pub fn proc_mem_limit(process: &Process, max: u64) -> Result<bool, Errno> {
    match process.smaps() {
        Ok(maps) => {
            let mut total_size: u64 = 0;
            for map in &maps.memory_maps {
                match &map.pathname {
                    MMapPath::Path(_)
                    | MMapPath::Anonymous
                    | MMapPath::Stack
                    | MMapPath::Other(_) => {
                        let pss = map.extension.map.get("Pss").copied().unwrap_or(0);
                        let private_dirty =
                            map.extension.map.get("Private_Dirty").copied().unwrap_or(0);
                        let shared_dirty =
                            map.extension.map.get("Shared_Dirty").copied().unwrap_or(0);

                        total_size = total_size.saturating_add(
                            pss.saturating_add(private_dirty)
                                .saturating_add(shared_dirty),
                        );

                        // Stop processing if total size exceeds or equals max
                        if total_size >= max {
                            return Ok(true);
                        }
                    }
                    _ => (),
                }
            }

            // If we're at this point, we did not hit the limit.
            Ok(false)
        }
        Err(_) => Err(Errno::last()),
    }
}

#[cfg(test)]
mod tests {
    use nix::{sys::stat::umask, unistd::Pid};

    use super::*;

    #[test]
    fn test_invalid_pid() {
        let result = proc_umask(Pid::from_raw(i32::MAX));
        assert!(result.is_err(), "{result:?}");
    }

    #[test]
    fn test_parsing_valid_umask_values() {
        // This test sets various umask values and then checks if our function correctly identifies them.
        let umasks = [
            Mode::from_bits_truncate(0o0000),
            Mode::from_bits_truncate(0o0002),
            Mode::from_bits_truncate(0o0022),
            Mode::from_bits_truncate(0o0077),
            Mode::from_bits_truncate(0o0777),
        ];

        for &my_umask in &umasks {
            umask(my_umask);
            let result = proc_umask(Pid::this()).unwrap();
            assert_eq!(result, my_umask, "{result:o} != {my_umask:o}");
        }

        // Resetting the umask to a default value after test
        umask(Mode::from_bits_truncate(0o0022));
    }
}
