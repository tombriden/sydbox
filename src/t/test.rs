//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/test/test.rs: Integration tests
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#![allow(clippy::disallowed_methods)]

use std::{
    env,
    fs::{remove_file, File},
    io::{Read, Write},
    os::{
        fd::{AsRawFd, FromRawFd},
        unix::process::ExitStatusExt,
    },
    process::Stdio,
};

use nix::{
    sys::stat::{umask, Mode},
    unistd::{unlink, Pid},
};

use crate::{assert, assert_eq, assert_ne, fixup, ignore, util::*, KERNEL_VERSION};

/// Represents a test case.
pub type Test<'a> = (&'a str, fn() -> TestResult);

macro_rules! test_entry {
    ($func:expr) => {
        (stringify!($func), $func)
    };
}

/// List of integration tests.
pub const TESTS: &[Test] = &[
    test_entry!(test_syd_true_returns_success),
    test_entry!(test_syd_true_returns_success_with_many_processes),
    test_entry!(test_syd_true_returns_success_with_many_threads),
    test_entry!(test_syd_false_returns_failure),
    test_entry!(test_syd_true_returns_failure_with_many_processes),
    test_entry!(test_syd_true_returns_failure_with_many_threads),
    test_entry!(test_syd_sigint_returns_130),
    test_entry!(test_syd_sigabrt_returns_134),
    test_entry!(test_syd_sigkill_returns_137),
    test_entry!(test_syd_reap_zombies_bare),
    test_entry!(test_syd_reap_zombies_wrap),
    test_entry!(test_syd_whoami_returns_root_fake),
    test_entry!(test_syd_whoami_returns_root_user),
    test_entry!(test_syd_environment_filter),
    test_entry!(test_syd_environment_harden),
    test_entry!(test_syd_lock),
    test_entry!(test_syd_lock_exec),
    test_entry!(test_syd_chk),
    test_entry!(test_syd_read_sandbox_open_allow),
    test_entry!(test_syd_read_sandbox_open_deny),
    test_entry!(test_syd_stat_sandbox_chdir_allow),
    test_entry!(test_syd_stat_sandbox_chdir_hide),
    test_entry!(test_syd_stat_sandbox_stat_allow),
    test_entry!(test_syd_stat_sandbox_stat_hide),
    test_entry!(test_syd_stat_sandbox_getdents_allow),
    test_entry!(test_syd_stat_sandbox_getdents_hide),
    test_entry!(test_syd_stat_bypass_with_read),
    test_entry!(test_syd_stat_bypass_with_write),
    test_entry!(test_syd_stat_bypass_with_exec),
    test_entry!(test_syd_write_sandbox_open_allow),
    test_entry!(test_syd_write_sandbox_open_deny),
    test_entry!(test_syd_exec_sandbox_open_allow),
    test_entry!(test_syd_exec_sandbox_open_deny),
    test_entry!(test_syd_network_sandbox_connect_ipv4_allow),
    test_entry!(test_syd_network_sandbox_connect_ipv4_deny),
    test_entry!(test_syd_network_sandbox_connect_ipv6_allow),
    test_entry!(test_syd_network_sandbox_connect_ipv6_deny),
    test_entry!(test_syd_network_sandbox_allow_safe_bind_ipv4_failure),
    test_entry!(test_syd_network_sandbox_allow_safe_bind_ipv4_success),
    test_entry!(test_syd_network_sandbox_allow_safe_bind_ipv6_failure),
    test_entry!(test_syd_network_sandbox_allow_safe_bind_ipv6_success),
    test_entry!(test_syd_exit_wait_all),
    test_entry!(test_syd_exit_wait_pid),
    test_entry!(test_syd_cli_args_override_user_profile),
    test_entry!(test_syd_exp_symlink_toctou),
    test_entry!(test_syd_exp_symlinkat_toctou),
    test_entry!(test_syd_exp_ptrmod_toctou_open),
    test_entry!(test_syd_exp_ptrmod_toctou_creat),
    test_entry!(test_syd_exp_ptrmod_toctou_opath),
    test_entry!(test_syd_io_uring_escape_strict),
    test_entry!(test_syd_io_uring_escape_unsafe),
    test_entry!(test_syd_opath_escape),
    test_entry!(test_syd_devfd_escape_chdir),
    test_entry!(test_syd_devfd_escape_chdir_relpath_1),
    test_entry!(test_syd_devfd_escape_chdir_relpath_2),
    test_entry!(test_syd_devfd_escape_chdir_relpath_3),
    test_entry!(test_syd_devfd_escape_chdir_relpath_4),
    test_entry!(test_syd_devfd_escape_chdir_relpath_5),
    test_entry!(test_syd_devfd_escape_chdir_relpath_6),
    test_entry!(test_syd_devfd_escape_chdir_relpath_7),
    test_entry!(test_syd_devfd_escape_chdir_relpath_8),
    test_entry!(test_syd_devfd_escape_chdir_relpath_9),
    test_entry!(test_syd_devfd_escape_chdir_relpath_10),
    test_entry!(test_syd_devfd_escape_chdir_relpath_11),
    test_entry!(test_syd_devfd_escape_chdir_relpath_12),
    test_entry!(test_syd_devfd_escape_chdir_relpath_13),
    test_entry!(test_syd_devfd_escape_chdir_relpath_14),
    test_entry!(test_syd_devfd_escape_chdir_relpath_15),
    test_entry!(test_syd_devfd_escape_chdir_relpath_16),
    test_entry!(test_syd_devfd_escape_chdir_relpath_17),
    test_entry!(test_syd_devfd_escape_chdir_relpath_18),
    test_entry!(test_syd_devfd_escape_chdir_relpath_19),
    test_entry!(test_syd_devfd_escape_chdir_relpath_20),
    test_entry!(test_syd_devfd_escape_open),
    test_entry!(test_syd_devfd_escape_open_relpath_1),
    test_entry!(test_syd_devfd_escape_open_relpath_2),
    test_entry!(test_syd_devfd_escape_open_relpath_3),
    test_entry!(test_syd_devfd_escape_open_relpath_4),
    test_entry!(test_syd_devfd_escape_open_relpath_5),
    test_entry!(test_syd_devfd_escape_open_relpath_6),
    test_entry!(test_syd_devfd_escape_open_relpath_7),
    test_entry!(test_syd_devfd_escape_open_relpath_8),
    test_entry!(test_syd_devfd_escape_open_relpath_9),
    test_entry!(test_syd_devfd_escape_open_relpath_10),
    test_entry!(test_syd_devfd_escape_open_relpath_11),
    test_entry!(test_syd_devfd_escape_open_relpath_12),
    test_entry!(test_syd_devfd_escape_open_relpath_13),
    test_entry!(test_syd_devfd_escape_open_relpath_14),
    test_entry!(test_syd_devfd_escape_open_relpath_15),
    test_entry!(test_syd_devfd_escape_open_relpath_16),
    test_entry!(test_syd_devfd_escape_open_relpath_17),
    test_entry!(test_syd_devfd_escape_open_relpath_18),
    test_entry!(test_syd_devfd_escape_open_relpath_19),
    test_entry!(test_syd_devfd_escape_open_relpath_20),
    test_entry!(test_syd_procself_escape_chdir),
    test_entry!(test_syd_procself_escape_chdir_relpath_1),
    test_entry!(test_syd_procself_escape_chdir_relpath_2),
    test_entry!(test_syd_procself_escape_chdir_relpath_3),
    test_entry!(test_syd_procself_escape_chdir_relpath_4),
    test_entry!(test_syd_procself_escape_chdir_relpath_5),
    test_entry!(test_syd_procself_escape_chdir_relpath_6),
    test_entry!(test_syd_procself_escape_chdir_relpath_7),
    test_entry!(test_syd_procself_escape_open),
    test_entry!(test_syd_procself_escape_open_relpath_1),
    test_entry!(test_syd_procself_escape_open_relpath_2),
    test_entry!(test_syd_procself_escape_open_relpath_3),
    test_entry!(test_syd_procself_escape_open_relpath_4),
    test_entry!(test_syd_procself_escape_open_relpath_5),
    test_entry!(test_syd_procself_escape_open_relpath_6),
    test_entry!(test_syd_procself_escape_open_relpath_7),
    test_entry!(test_syd_procself_escape_relpath),
    test_entry!(test_syd_procself_escape_symlink),
    test_entry!(test_syd_procself_escape_symlink_within_container),
    test_entry!(test_syd_umask_bypass_077),
    test_entry!(test_syd_umask_bypass_277),
    test_entry!(test_syd_emulate_opath),
    test_entry!(test_syd_emulate_otmpfile),
    test_entry!(test_syd_honor_umask),
    test_entry!(test_syd_open_utf8_invalid),
    test_entry!(test_syd_exec_in_inaccessible_directory),
    test_entry!(test_syd_disallow_setuid),
    test_entry!(test_syd_disallow_setgid),
    test_entry!(test_syd_fstat_on_pipe),
    test_entry!(test_syd_fstat_on_socket),
    test_entry!(test_syd_fstat_on_deleted_file),
    test_entry!(test_syd_fstat_on_temp_file),
    test_entry!(test_syd_fchmodat_on_proc_fd),
    test_entry!(test_syd_linkat_on_fd),
    test_entry!(test_syd_block_ioctl_tiocsti),
    test_entry!(test_syd_block_prctl_ptrace),
    test_entry!(test_syd_block_dev_random),
    test_entry!(test_syd_block_dev_urandom),
    test_entry!(test_syd_kill_during_syscall),
    test_entry!(test_syd_open_toolong_path),
    test_entry!(test_syd_open_null_path),
    test_entry!(test_syd_utimensat_null),
    test_entry!(test_syd_normalize_path),
    test_entry!(test_syd_path_resolution),
    test_entry!(test_syd_remove_empty_path),
    test_entry!(test_syd_symlink_readonly_path),
    test_entry!(test_syd_open_trailing_slash),
    test_entry!(test_syd_openat_trailing_slash),
    test_entry!(test_syd_lstat_trailing_slash),
    test_entry!(test_syd_fstatat_trailing_slash),
    test_entry!(test_syd_mkdir_trailing_dot),
    test_entry!(test_syd_mkdirat_trailing_dot),
    test_entry!(test_syd_rmdir_trailing_slashdot),
    test_entry!(test_syd_fopen_supports_mode_e),
    test_entry!(test_syd_fopen_supports_mode_x),
    test_entry!(test_syd_link_no_symlink_deref),
    test_entry!(test_syd_link_posix),
    test_entry!(test_syd_linkat_posix),
    test_entry!(test_syd_getcwd_long),
    test_entry!(test_syd_creat_thru_dangling),
    test_entry!(test_syd_mkdirat_non_dir_fd),
    test_entry!(test_syd_blocking_udp4),
    test_entry!(test_syd_blocking_udp6),
    test_entry!(test_syd_close_on_exec),
    test_entry!(test_syd_exp_open_exclusive_restart),
    test_entry!(test_syd_open_exclusive_repeat),
    test_entry!(test_syd_pty_io_rust),
    test_entry!(test_syd_pty_io_gawk),
    test_entry!(test_syd_diff_dev_fd),
    test_entry!(test_syd_fifo_multiple_readers),
    test_entry!(test_syd_bind_unix_socket),
    test_entry!(test_syd_signal_protection),
    test_entry!(test_syd_exp_emulate_open_fifo_1),
    test_entry!(test_syd_exp_emulate_open_fifo_2),
    test_entry!(test_syd_deny_magiclinks),
    test_entry!(test_syd_unshare_user_bypass_limit),
    test_entry!(test_syd_exp_interrupt_mkdir),
    test_entry!(test_syd_exp_interrupt_bind_ipv4),
    test_entry!(test_syd_exp_interrupt_bind_unix),
    test_entry!(test_syd_exp_interrupt_connect_ipv4),
    test_entry!(test_syd_exp_repetitive_clone),
    test_entry!(test_syd_exp_syscall_fuzz_bare),
    test_entry!(test_syd_exp_syscall_fuzz_wrap),
    test_entry!(test_syd_pid_fork_deny),
    test_entry!(test_syd_pid_thread_deny),
    test_entry!(test_syd_pid_fork_bomb),
    test_entry!(test_syd_pid_fork_bomb_asm),
    test_entry!(test_syd_pid_thread_bomb),
    test_entry!(test_syd_pid_stress_ng_deny),
    test_entry!(test_syd_pid_stress_ng_allow),
    test_entry!(test_syd_pid_stress_ng_fork),
    test_entry!(test_syd_mem_alloc),
    test_entry!(test_syd_mem_stress_ng_malloc_1),
    test_entry!(test_syd_mem_stress_ng_malloc_2),
    test_entry!(test_syd_mem_stress_ng_mmap),
];

// Tests if `true` returns success under sandbox.
fn test_syd_true_returns_success() -> TestResult {
    let status = syd()
        .arg("-mallow/exec+/***")
        .arg("-mallow/read+/***")
        .arg("-mallow/stat+/***")
        .arg("--")
        .arg("true")
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");

    let status = syd()
        .arg("-mallow/exec+/***")
        .arg("-mallow/read+/***")
        .arg("-mallow/stat+/***")
        .args(["--", "true"])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");

    Ok(())
}

// Tests if `syd` returns success for a sandbox running many processes,
// in case the execve child returns success.
fn test_syd_true_returns_success_with_many_processes() -> TestResult {
    env::set_var("SYD_DO", "fork");
    let status = syd()
        .arg("-mallow/exec+/***")
        .arg("-mallow/read+/***")
        .arg("-mallow/stat+/***")
        .args(["--", &SYD_DO, "0", "8"])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");

    Ok(())
}

// Tests if `syd` returns success for a sandbox running many threads,
// in case the execve child returns success.
fn test_syd_true_returns_success_with_many_threads() -> TestResult {
    env::set_var("SYD_DO", "thread");
    let status = syd()
        .arg("-mallow/exec+/***")
        .arg("-mallow/read+/***")
        .arg("-mallow/stat+/***")
        .args(["--", &SYD_DO, "0", "8"])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");

    Ok(())
}

// Tests if `false` returns failure under sandbox.
fn test_syd_false_returns_failure() -> TestResult {
    let status = syd()
        .arg("-mallow/exec+/***")
        .arg("-mallow/read+/***")
        .arg("-mallow/stat+/***")
        .arg("--")
        .arg("false")
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0, "status:{status:?}");

    let status = syd().args(["--", "false"]).status().expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0, "status:{status:?}");

    Ok(())
}

// Tests if `syd` returns failure for a sandbox running many processes,
// in case the execve child returns failure.
fn test_syd_true_returns_failure_with_many_processes() -> TestResult {
    env::set_var("SYD_DO", "fork");
    let status = syd()
        .arg("-mallow/exec+/***")
        .arg("-mallow/read+/***")
        .arg("-mallow/stat+/***")
        .args(["--", &SYD_DO, "7", "8"])
        .status()
        .expect("execute syd");
    assert!(!status.success());
    assert_eq!(status.code().unwrap_or(127), 7);

    Ok(())
}

// Tests if `syd` returns failure for a sandbox running many threads,
// in case the execve child returns failure.
fn test_syd_true_returns_failure_with_many_threads() -> TestResult {
    env::set_var("SYD_DO", "thread");
    let status = syd()
        .arg("-mallow/exec+/***")
        .arg("-mallow/read+/***")
        .arg("-mallow/stat+/***")
        .args(["--", &SYD_DO, "7", "8"])
        .status()
        .expect("execute syd");
    assert!(!status.success());
    assert_eq!(status.code().unwrap_or(127), 7);

    Ok(())
}

fn test_syd_sigint_returns_130() -> TestResult {
    let status = syd()
        .arg("-ppaludis")
        .args(["sh", "-c"])
        .arg(r#"exec kill -INT $$"#)
        .status()
        .expect("execute syd");
    assert!(!status.success());
    assert_eq!(status.code().unwrap_or(127), 130);
    Ok(())
}

fn test_syd_sigabrt_returns_134() -> TestResult {
    let status = syd()
        .arg("-ppaludis")
        .args(["sh", "-c"])
        .arg(r#"exec kill -ABRT $$"#)
        .status()
        .expect("execute syd");
    assert!(!status.success());
    assert_eq!(status.code().unwrap_or(127), 134);
    Ok(())
}

fn test_syd_sigkill_returns_137() -> TestResult {
    let status = syd()
        .arg("-ppaludis")
        .args(["sh", "-c"])
        .arg(r#"exec kill -KILL $$"#)
        .status()
        .expect("execute syd");
    assert!(!status.success());
    assert_eq!(status.code().unwrap_or(127), 137);
    Ok(())
}

fn test_syd_reap_zombies_bare() -> TestResult {
    let status = syd()
        .arg("-ppaludis")
        .args(["bash", "-c"])
        .arg(
            r#"
set -e
for i in {1..10}; do
    ( sleep $i ) &
done
echo >&2 "Spawned 10 processes in the background."
echo >&2 "Disowning and exiting..."
disown
exit 42
        "#,
        )
        .status()
        .expect("execute syd");
    assert!(!status.success());
    assert_eq!(status.code().unwrap_or(127), 42);

    Ok(())
}

fn test_syd_reap_zombies_wrap() -> TestResult {
    match check_unshare() {
        Some(false) => {
            eprintln!("Test requires Linux namespaces, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError(
                "Failed to test for Linux namespaces!".to_string(),
            ));
        }
        _ => {}
    };
    let status = syd()
        .args(["-ppaludis", "-pcontainer"])
        .args(["bash", "-c"])
        .arg(
            r#"
set -e
for i in {1..10}; do
    ( sleep $i ) &
done
echo >&2 "Spawned 10 processes in the background."
echo >&2 "Disowning and exiting..."
disown
exit 42
        "#,
        )
        .status()
        .expect("execute syd");
    assert!(!status.success());
    assert_eq!(status.code().unwrap_or(127), 42);

    Ok(())
}

// Tests if `whoami` returns `root` with `root/fake:1`
fn test_syd_whoami_returns_root_fake() -> TestResult {
    let output = syd()
        .arg("-plib")
        .args(["-mroot/fake:1", "--", "whoami"])
        .output()
        .expect("execute syd");
    assert!(output.stdout.starts_with(b"root"));

    Ok(())
}

// Tests if `whoami` returns `root` with `root/map:1`
fn test_syd_whoami_returns_root_user() -> TestResult {
    match check_unshare() {
        Some(false) => {
            eprintln!("Test requires Linux namespaces, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError(
                "Failed to test for Linux namespaces!".to_string(),
            ));
        }
        _ => {}
    };

    let output = syd()
        .arg("-plib")
        .args(["-mroot/map:1", "--", "whoami"])
        .stderr(Stdio::inherit())
        .output()
        .expect("execute syd");
    assert!(output.stdout.starts_with(b"root"), "output:{output:?}");

    Ok(())
}

// Checks environment filtering
fn test_syd_environment_filter() -> TestResult {
    const ENV: &str = "SAFE";
    env::set_var(ENV, "/var/empty");

    // Step 1: Allow by default
    let output = syd()
        .arg("-plib")
        .arg("--")
        .args(["/bin/sh", "-c", &format!("echo ${ENV}")])
        .output()
        .expect("execute syd");
    let output = String::from_utf8_lossy(&output.stdout);
    let output = output.trim_end();
    assert!(output == "/var/empty", "output1:{output}");

    // Step 2: Override with -evar=val
    let output = syd()
        .arg("-plib")
        .arg(&format!("-e{ENV}=/var/empty:/var/empty"))
        .arg("--")
        .args(["/bin/sh", "-c", &format!("echo ${ENV}")])
        .output()
        .expect("execute syd");
    let output = String::from_utf8_lossy(&output.stdout);
    let output = output.trim_end();
    assert!(output == "/var/empty:/var/empty", "output2:{output}");

    // Step 3: Unset with -evar
    let output = syd()
        .arg("-plib")
        .arg(&format!("-e{ENV}"))
        .arg("--")
        .args(["/bin/sh", "-c", &format!("echo ${ENV}")])
        .output()
        .expect("execute syd");
    let output = String::from_utf8_lossy(&output.stdout);
    let output = output.trim_end();
    assert!(output.is_empty(), "output3:{output}");

    // Step 4: Pass-through with -evar=
    let output = syd()
        .arg("-plib")
        .arg(&format!("-e{ENV}="))
        .arg("--")
        .args(["/bin/sh", "-c", &format!("echo ${ENV}")])
        .output()
        .expect("execute syd");
    let output = String::from_utf8_lossy(&output.stdout);
    let output = output.trim_end();
    assert!(output == "/var/empty", "output4:{output}");

    env::remove_var(ENV);

    Ok(())
}

// Checks environment hardening and -e pass-through.
fn test_syd_environment_harden() -> TestResult {
    const ENV: &str = "LD_LIBRARY_PATH";
    env::set_var(ENV, "/var/empty");

    // Step 1: Deny by default
    let output = syd()
        .arg("-plib")
        .arg("--")
        .args(["/bin/sh", "-c", &format!("echo ${ENV}")])
        .output()
        .expect("execute syd");
    let output = String::from_utf8_lossy(&output.stdout);
    let output = output.trim_end();
    assert!(output.is_empty(), "output1:{output}");

    // Step 2: Override with -evar=val
    let output = syd()
        .arg("-plib")
        .arg(&format!("-e{ENV}=/var/empty:/var/empty"))
        .arg("--")
        .args(["/bin/sh", "-c", &format!("echo ${ENV}")])
        .output()
        .expect("execute syd");
    let output = String::from_utf8_lossy(&output.stdout);
    let output = output.trim_end();
    assert!(output == "/var/empty:/var/empty", "output2:{output}");

    // Step 3: Unset with -evar
    let output = syd()
        .arg("-plib")
        .arg(&format!("-e{ENV}"))
        .arg("--")
        .args(["/bin/sh", "-c", &format!("echo ${ENV}")])
        .output()
        .expect("execute syd");
    let output = String::from_utf8_lossy(&output.stdout);
    let output = output.trim_end();
    assert!(output.is_empty(), "output3:{output}");

    // Step 4: Pass-through with -evar=
    let output = syd()
        .arg("-plib")
        .arg(&format!("-e{ENV}="))
        .arg("--")
        .args(["/bin/sh", "-c", &format!("echo ${ENV}")])
        .output()
        .expect("execute syd");
    let output = String::from_utf8_lossy(&output.stdout);
    let output = output.trim_end();
    assert!(output == "/var/empty", "output4:{output}");

    // Step 5: Allow with -m trace/allow_unsafe_env:1
    let output = syd()
        .arg("-plib")
        .arg("-mtrace/allow_unsafe_env:1")
        .arg("--")
        .args(["/bin/sh", "-c", &format!("echo ${ENV}")])
        .output()
        .expect("execute syd");
    let output = String::from_utf8_lossy(&output.stdout);
    let output = output.trim_end();
    assert!(output == "/var/empty", "output:{output}");

    // Step 6: Toggle -m trace/allow_unsafe_env
    let output = syd()
        .arg("-plib")
        .arg("-mtrace/allow_unsafe_env:1")
        .arg("-mtrace/allow_unsafe_env:0")
        .arg("--")
        .args(["/bin/sh", "-c", &format!("echo ${ENV}")])
        .output()
        .expect("execute syd");
    let output = String::from_utf8_lossy(&output.stdout);
    let output = output.trim_end();
    assert!(output.is_empty(), "output1:{output}");

    env::remove_var(ENV);

    Ok(())
}

// Tests if `lock:on` command disables access to `/dev/syd`.
fn test_syd_lock() -> TestResult {
    eprintln!("+ bash -c \"test -e /dev/syd\"");
    let status = syd()
        .arg("-plib")
        .args(["--", "/bin/bash", "-c", "test -e /dev/syd"])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    eprintln!("+ bash -c \"test -e /dev/syd\"");
    let status = syd()
        .arg("-plib")
        .args(["-mlock:on", "--", "/bin/bash", "-c", "test -e /dev/syd"])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if `lock:exec` locks the sandbox for all except the exec child.
fn test_syd_lock_exec() -> TestResult {
    eprintln!(r#"+ bash -c "test -e /dev/syd""#);
    let status = syd()
        .arg("-plib")
        .args(["-mlock:exec", "--", "/bin/bash", "-c", "test -e /dev/syd"])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    eprintln!(r#"+ bash -c "( test -e /dev/syd )""#);
    let status = syd()
        .arg("-plib")
        .args([
            "-mlock:exec",
            "--",
            "/bin/bash",
            "-c",
            "( test -e /dev/syd )",
        ])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if syd-chk works to check if process is running under syd.
fn test_syd_chk() -> TestResult {
    eprintln!("+ syd-chk");
    let status = std::process::Command::new(&*SYD_CHK)
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 1);

    eprintln!("+ syd -- syd-chk");
    let status = syd()
        .arg("-plib")
        .args(["--", &SYD_CHK])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if read sandboxing for open works to allow.
fn test_syd_read_sandbox_open_allow() -> TestResult {
    eprintln!("+ dd if=/dev/null");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-mallow/stat+/***",
            "-mallow/read+/***",
            "-mdeny/read+/dev/***",
            "-mallow/read+/dev/null",
            "--",
            "dd",
            "if=/dev/null",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if read sandboxing for open works to deny.
fn test_syd_read_sandbox_open_deny() -> TestResult {
    eprintln!("+ cat /dev/null");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "-mdeny/read+/dev/null",
            "--",
            "cat",
            "/dev/null",
        ])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if stat sandboxing for chdir works to allow.
fn test_syd_stat_sandbox_chdir_allow() -> TestResult {
    eprintln!("+ bash -c \"cd /dev\"");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/stat:on",
            "-mdeny/stat+/dev",
            "-mallow/stat+/dev",
            "--",
            "/bin/sh",
            "-c",
            "cd /dev",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if stat sandboxing for stat works to hide.
fn test_syd_stat_sandbox_chdir_hide() -> TestResult {
    eprintln!("+ cd /dev");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/stat:on",
            "-mallow/stat+/***",
            "-mdeny/stat+/dev",
            "--",
            "/bin/sh",
            "-c",
            "cd /dev || exit 42",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 42);

    Ok(())
}

// Tests if stat sandboxing for stat works to allow.
fn test_syd_stat_sandbox_stat_allow() -> TestResult {
    eprintln!("+ ls /dev/null");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/stat:on",
            "-mdeny/stat+/dev/null",
            "-mallow/stat+/dev/null",
            "--",
            "ls",
            "/dev/null",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if stat sandboxing for stat works to hide.
fn test_syd_stat_sandbox_stat_hide() -> TestResult {
    eprintln!("+ ls /dev/null");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/stat:on",
            "-mallow/stat+/***",
            "-mdeny/stat+/dev/null",
            "--",
            "ls",
            "/dev/null",
        ])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if stat sandboxing for getdents works to allow.
fn test_syd_stat_sandbox_getdents_allow() -> TestResult {
    eprintln!("+ ls /dev");
    let output = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/stat:on",
            "-mdeny/stat+/dev/null",
            "-mallow/stat+/dev/null",
            "--",
            "ls",
            "/dev",
        ])
        .output()
        .expect("execute syd");
    assert!(
        output
            .stdout
            .windows(b"null".len())
            .any(|window| window == b"null"),
        "Stdout:\n{:?}",
        output.stdout
    );

    Ok(())
}

// Tests if stat sandboxing for getdents works to hide.
fn test_syd_stat_sandbox_getdents_hide() -> TestResult {
    eprintln!("+ ls /dev");
    let output = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/stat:on",
            "-mallow/stat+/***",
            "-mdeny/stat+/dev/null",
            "--",
            "ls",
            "/dev",
        ])
        .output()
        .expect("execute syd");
    assert!(
        output
            .stdout
            .windows(b"null".len())
            .any(|window| window != b"null"),
        "Stdout:{:?}",
        output.stdout
    );

    Ok(())
}

// Tests if stat sandboxing can be bypassed by read attempt
fn test_syd_stat_bypass_with_read() -> TestResult {
    env::set_var("SYD_DO", "stat_bypass_with_read");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mdeny/read+/etc/***",
            "-mdeny/stat+/etc/***",
            "-mallow/read+/etc/ld*/***",
            "-mallow/stat+/etc/ld*/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");

    Ok(())
}

// Tests if stat sandboxing can be bypassed by write attempt
fn test_syd_stat_bypass_with_write() -> TestResult {
    env::set_var("SYD_DO", "stat_bypass_with_write");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "-mdeny/stat+/etc/***",
            "-mdeny/write+/etc/***",
            "-mallow/stat+/etc/ld*/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");

    Ok(())
}

// Tests if stat sandboxing can be bypassed by exec attempt
fn test_syd_stat_bypass_with_exec() -> TestResult {
    env::set_var("SYD_DO", "stat_bypass_with_exec");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/exec:on",
            "-msandbox/stat:on",
            "-mallow/exec+/***",
            "-mallow/stat+/***",
            "-mdeny/exec+/**/z?sh",
            "-mdeny/stat+/**/z?sh",
            "-mdeny/exec+/**/[bd]ash",
            "-mdeny/stat+/**/[bd]ash",
            "-mdeny/exec+/**/busybox",
            "-mdeny/stat+/**/busybox",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");

    Ok(())
}

// Tests if write sandboxing for open works to allow.
fn test_syd_write_sandbox_open_allow() -> TestResult {
    eprintln!("+ bash -c \"echo welcome to the machine >> /dev/null\"");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/write:on",
            "-mdeny/write+/dev/***",
            "-mallow/write+/dev/null",
            "--",
            "/bin/bash",
            "-c",
            "echo welcome to the machine >> /dev/null",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if write sandboxing for open works to deny.
fn test_syd_write_sandbox_open_deny() -> TestResult {
    eprintln!("+ bash -c \"echo welcome to the machine >> /dev/null\"");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/write:on",
            "-mallow/write+/***",
            "-mdeny/write+/dev/null",
            "--",
            "/bin/bash",
            "-c",
            "echo welcome to the machine >> /dev/null",
        ])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if exec sandboxing works to allow.
fn test_syd_exec_sandbox_open_allow() -> TestResult {
    let bin = which("true")?;
    eprintln!("+ {bin}");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/exec:on",
            "-mdeny/exec+/***",
            &format!("-mallow/exec+{bin}"),
            "-atrue", // this may be busybox
            "--",
            &bin.to_string(),
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if exec sandboxing works to deny.
fn test_syd_exec_sandbox_open_deny() -> TestResult {
    let bin = which("true")?;
    eprintln!("+ {bin}");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/exec:on",
            "-mallow/exec+/***",
            &format!("-mdeny/exec+{bin}"),
            "-atrue", // this may be busybox
            "--",
            &bin.to_string(),
        ])
        .status()
        .expect("execute syd");
    assert_ne!(status.code().unwrap_or(127), 0);

    Ok(())
}

// Tests if network connect sandboxing works to allow.
fn test_syd_network_sandbox_connect_ipv4_allow() -> TestResult {
    env::set_var("SYD_DO", "connect4");
    let status = syd()
        .args([
            "-mallow/exec+/***",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mtrace/allow_safe_bind:0",
            "-mallow/net/bind+127.0.0.1!4242",
            "-mallow/net/connect+127.0.0.1!4242",
            "--",
            &SYD_DO,
            "127.0.0.1",
            "4242",
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

// Tests if network connect sandboxing works to deny.
fn test_syd_network_sandbox_connect_ipv4_deny() -> TestResult {
    env::set_var("SYD_DO", "connect4");
    let status = syd()
        .args([
            "-mallow/exec+/***",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mtrace/allow_safe_bind:0",
            "-mallow/net/bind+127.0.0.1!4242",
            "-mdeny/net/connect+127.0.0.1!4242",
            "--",
            &SYD_DO,
            "127.0.0.1",
            "4242",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 13 /* EACCES */);
    Ok(())
}

// Tests if network connect sandboxing works to allow.
fn test_syd_network_sandbox_connect_ipv6_allow() -> TestResult {
    if !check_ipv6() {
        env::set_var("SYD_TEST_SOFT_FAIL", "1");
        return Ok(());
    }

    env::set_var("SYD_DO", "connect6");
    let status = syd()
        .args([
            "-mallow/exec+/***",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mtrace/allow_safe_bind:0",
            "-mallow/net/bind+::1!4242",
            "-mallow/net/connect+::1!4242",
            "--",
            &SYD_DO,
            "::1",
            "4242",
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

// Tests if network connect sandboxing works to deny.
fn test_syd_network_sandbox_connect_ipv6_deny() -> TestResult {
    if !check_ipv6() {
        env::set_var("SYD_TEST_SOFT_FAIL", "1");
        return Ok(());
    }

    env::set_var("SYD_DO", "connect6");
    let status = syd()
        .args([
            "-mallow/exec+/***",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mtrace/allow_safe_bind:0",
            "-mallow/net/bind+::1!4242",
            "-mdeny/net/connect+::1!4242",
            "--",
            &SYD_DO,
            "::1",
            "4242",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 13 /* EACCES */);
    Ok(())
}

fn test_syd_network_sandbox_allow_safe_bind_ipv4_failure() -> TestResult {
    env::set_var("SYD_DO", "connect4_0");
    let status = syd()
        .args([
            "-mallow/exec+/***",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mtrace/allow_safe_bind:0",
            "-mallow/net/bind+127.0.0.1!0",
            "--",
            &SYD_DO,
            "127.0.0.1",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 13 /* EACCES */);
    Ok(())
}

fn test_syd_network_sandbox_allow_safe_bind_ipv4_success() -> TestResult {
    env::set_var("SYD_DO", "connect4_0");
    let status = syd()
        .args([
            "-mallow/exec+/***",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mtrace/allow_safe_bind:1",
            "-mallow/net/bind+127.0.0.1!0",
            "--",
            &SYD_DO,
            "127.0.0.1",
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_network_sandbox_allow_safe_bind_ipv6_failure() -> TestResult {
    if !check_ipv6() {
        env::set_var("SYD_TEST_SOFT_FAIL", "1");
        return Ok(());
    }

    env::set_var("SYD_DO", "connect6_0");
    let status = syd()
        .args([
            "-mallow/exec+/***",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mtrace/allow_safe_bind:0",
            "-mallow/net/bind+::1!0",
            "--",
            &SYD_DO,
            "::1",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 13 /* EACCES */);
    Ok(())
}

fn test_syd_network_sandbox_allow_safe_bind_ipv6_success() -> TestResult {
    if !check_ipv6() {
        env::set_var("SYD_TEST_SOFT_FAIL", "1");
        return Ok(());
    }

    env::set_var("SYD_DO", "connect6_0");
    let status = syd()
        .args([
            "-mallow/exec+/***",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mtrace/allow_safe_bind:1",
            "-mallow/net/bind+::1!0",
            "--",
            &SYD_DO,
            "::1",
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_exit_wait_all() -> TestResult {
    env::set_var("SYD_TEST_TIMEOUT", "3s");
    let status = syd()
        .args([
            "-ppaludis",
            "-mtrace/exit_wait_all:1",
            "--",
            "/bin/sh",
            "-x",
            "-c",
            "nohup sleep 5 & exit 1",
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");
    let signal = status.signal().unwrap_or(0);
    let excode = status.code().unwrap_or(127);
    assert!(
        signal == nix::libc::SIGKILL || excode == 128 + nix::libc::SIGKILL,
        "status:{status:?}"
    );
    Ok(())
}

fn test_syd_exit_wait_pid() -> TestResult {
    env::set_var("SYD_TEST_TIMEOUT", "3s");
    let status = syd()
        .args([
            "-ppaludis",
            "-mtrace/exit_wait_all:0",
            "--",
            "/bin/sh",
            "-x",
            "-c",
            "nohup sleep 5 & exit 7",
        ])
        .status()
        .expect("execute syd");
    assert_eq!(status.code().unwrap_or(127), 7, "status:{status:?}");
    env::remove_var("SYD_TEST_TIMEOUT");
    Ok(())
}

fn test_syd_cli_args_override_user_profile() -> TestResult {
    let _ = unlink(".user.syd-3");
    let mut file = File::create(".user.syd-3").expect("Failed to create .user.syd-3");
    file.write_all(b"mem/max:4242\npid/max:2525\n")
        .expect("Failed to write to .user.syd-3");

    let mut child = syd()
        .args(["-mpid/max:4242", "-mstat", "-ctrue"])
        .stderr(Stdio::piped())
        .spawn()
        .expect("execute syd");

    // Read the output from the child process
    let child_stderr = child.stderr.as_mut().expect("child stderr");
    let mut output = String::new();
    let raw_fd = child_stderr.as_raw_fd();
    let mut file = unsafe { File::from_raw_fd(raw_fd) };
    if let Err(error) = file.read_to_string(&mut output) {
        return Err(TestError(format!(
            "Failed to read output of child process: {error}"
        )));
    }
    print!("Child output:\n{output}");

    assert!(output.contains("Pid Max: 4242"));
    //This may fail if the site-wide config file has lock:on.
    //assert!(output.contains("Memory Max: 4242"));

    Ok(())
}

fn test_syd_exp_symlink_toctou() -> TestResult {
    env::set_var(
        "SYD_LOG",
        env::var("SYD_LOG").unwrap_or_else(|_| "error".to_string()),
    );
    env::set_var("SYD_DO", "symlink_toctou");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mdeny/stat+/etc/***",
            "-mallow/stat+/etc/ld*",
            "-mdeny/read+/etc/passwd",
            "-mdeny/write+/etc/passwd",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_exp_symlinkat_toctou() -> TestResult {
    env::set_var(
        "SYD_LOG",
        env::var("SYD_LOG").unwrap_or_else(|_| "error".to_string()),
    );
    env::set_var("SYD_DO", "symlinkat_toctou");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mdeny/stat+/etc/***",
            "-mallow/stat+/etc/ld*",
            "-mdeny/read+/etc/passwd",
            "-mdeny/write+/etc/passwd",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_exp_ptrmod_toctou_open() -> TestResult {
    env::set_var(
        "SYD_LOG",
        env::var("SYD_LOG").unwrap_or_else(|_| "error".to_string()),
    );
    env::set_var("SYD_DO", "ptrmod_toctou_open");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mdeny/stat+/etc/***",
            "-mallow/stat+/etc/ld*",
            "-mdeny/read+/etc/passwd",
            "-mdeny/write+/etc/passwd",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_exp_ptrmod_toctou_creat() -> TestResult {
    env::set_var(
        "SYD_LOG",
        env::var("SYD_LOG").unwrap_or_else(|_| "error".to_string()),
    );
    env::set_var("SYD_DO", "ptrmod_toctou_creat");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mdeny/stat+/etc/***",
            "-mallow/stat+/etc/ld*",
            "-mdeny/write+/**/deny.syd-tmp",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_exp_ptrmod_toctou_opath() -> TestResult {
    env::set_var(
        "SYD_LOG",
        env::var("SYD_LOG").unwrap_or_else(|_| "error".to_string()),
    );
    env::set_var("SYD_DO", "ptrmod_toctou_opath");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/etc",
            "-mdeny/stat+/etc/**",
            "-mallow/stat+/etc/ld*",
            "-mdeny/read,stat,write+/etc/passwd",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_io_uring_escape_strict() -> TestResult {
    #[cfg(feature = "uring")]
    {
        // Step 1: Default is strict.
        env::set_var("SYD_DO", "io_uring_escape");
        let status = syd()
            .args([
                "-ppaludis",
                "-msandbox/lock:off",
                "-msandbox/read:on",
                "-msandbox/stat:on",
                "-msandbox/write:on",
                "-mallow/read+/***",
                "-mdeny/stat+/etc/***",
                "-mallow/stat+/etc/ld*",
                "-mdeny/read,write+/etc/passwd",
                "--",
                &SYD_DO,
                "0",
            ])
            .status()
            .expect("execute syd");
        assert!(status.success(), "status:{status:?}");
    }
    Ok(())
}

fn test_syd_io_uring_escape_unsafe() -> TestResult {
    #[cfg(feature = "uring")]
    {
        // Step 2: Relax uring restriction.
        env::set_var("SYD_DO", "io_uring_escape");
        let status = syd()
            .args([
                "-ppaludis",
                "-msandbox/lock:off",
                "-msandbox/read:on",
                "-msandbox/stat:on",
                "-msandbox/write:on",
                "-mallow/read+/***",
                "-mdeny/stat+/etc/***",
                "-mallow/stat+/etc/ld*",
                "-mdeny/read,write+/etc/passwd",
                "-mtrace/allow_unsafe_uring:1",
                "--",
                &SYD_DO,
                "1",
            ])
            .status()
            .expect("execute syd");
        assert!(status.success(), "status:{status:?}");
    }

    Ok(())
}

fn test_syd_opath_escape() -> TestResult {
    env::set_var("SYD_DO", "opath_escape");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mdeny/stat+/etc/***",
            "-mallow/stat+/etc/ld*",
            "-mdeny/read+/etc/passwd",
            "-mdeny/write+/etc/passwd",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_1() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_1");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_2() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_2");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_3() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_3");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_4() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_4");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_5() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_5");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_6() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_6");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_7() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_7");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_8() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_8");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_9() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_9");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_10() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_10");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_11() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_11");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_12() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_12");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_13() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_13");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_14() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_14");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_15() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_15");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_16() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_16");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_17() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_17");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_18() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_18");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_19() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_19");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_chdir_relpath_20() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_chdir_relpath_20");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_1() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_1");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_2() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_2");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_3() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_3");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_4() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_4");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_5() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_5");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_6() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_6");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_7() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_7");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_8() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_8");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_9() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_9");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_10() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_10");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_11() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_11");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_12() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_12");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_13() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_13");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_14() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_14");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_15() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_15");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_16() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_16");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_17() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_17");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_18() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_18");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_19() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_19");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_devfd_escape_open_relpath_20() -> TestResult {
    env::set_var("SYD_DO", "devfd_escape_open_relpath_20");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_chdir() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_chdir");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_chdir_relpath_1() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_chdir_relpath_1");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_chdir_relpath_2() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_chdir_relpath_2");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_chdir_relpath_3() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_chdir_relpath_3");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_chdir_relpath_4() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_chdir_relpath_4");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_chdir_relpath_5() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_chdir_relpath_5");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_chdir_relpath_6() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_chdir_relpath_6");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_chdir_relpath_7() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_chdir_relpath_7");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_open() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_open");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_open_relpath_1() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_open_relpath_1");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_open_relpath_2() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_open_relpath_2");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_open_relpath_3() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_open_relpath_3");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_open_relpath_4() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_open_relpath_4");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_open_relpath_5() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_open_relpath_5");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_open_relpath_6() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_open_relpath_6");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_open_relpath_7() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_open_relpath_7");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_relpath() -> TestResult {
    eprintln!("+ grep 'Name:[[:space:]]syd' /proc/./self/status");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            "grep",
            "Name:[[:space:]]syd",
            "/proc/./self/status",
        ])
        .status()
        .expect("execute syd");
    assert!(
        status.code().unwrap_or(127) == 1,
        "code:{:?}",
        status.code()
    );
    Ok(())
}

fn test_syd_procself_escape_symlink() -> TestResult {
    env::set_var("SYD_DO", "procself_escape_symlink");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_procself_escape_symlink_within_container() -> TestResult {
    match check_unshare() {
        Some(false) => {
            eprintln!("Test requires Linux namespaces, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError(
                "Failed to test for Linux namespaces!".to_string(),
            ));
        }
        _ => {}
    };

    env::set_var("SYD_DO", "procself_escape_symlink");
    let status = syd()
        .args([
            "-mallow/read+/***",
            "-ppaludis",
            "-pcontainer",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_umask_bypass_077() -> TestResult {
    // Set a liberal umask as the test expects.
    let prev_umask = umask(Mode::from_bits_truncate(0o022));
    env::set_var("SYD_DO", "umask_bypass_077");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/write:on",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    let _ = umask(prev_umask);

    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_umask_bypass_277() -> TestResult {
    // Set a liberal umask as the test expects.
    let prev_umask = umask(Mode::from_bits_truncate(0o022));
    env::set_var("SYD_DO", "umask_bypass_277");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/write:on",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    let _ = umask(prev_umask);

    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_emulate_opath() -> TestResult {
    env::set_var("SYD_DO", "emulate_opath");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-mallow/read+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_emulate_otmpfile() -> TestResult {
    env::set_var("SYD_DO", "emulate_otmpfile");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/write:on",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_honor_umask() -> TestResult {
    env::set_var("SYD_DO", "honor_umask");
    let prev_umask = umask(Mode::from_bits_truncate(0o077));
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/write:on",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
            "0600",
        ])
        .status()
        .expect("execute syd");
    let _ = umask(prev_umask);
    assert!(status.success(), "status:{status:?}");

    let prev_umask = umask(Mode::from_bits_truncate(0o022));
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/write:on",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
            "0644",
        ])
        .status()
        .expect("execute syd");
    let _ = umask(prev_umask);
    assert!(status.success(), "status:{status:?}");

    let prev_umask = umask(Mode::from_bits_truncate(0));
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/write:on",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
            "0666",
        ])
        .status()
        .expect("execute syd");
    let _ = umask(prev_umask);
    assert!(status.success(), "status:{status:?}");

    Ok(())
}

fn test_syd_open_utf8_invalid() -> TestResult {
    env::set_var("SYD_DO", "open_utf8_invalid");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/write:on",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_exec_in_inaccessible_directory() -> TestResult {
    env::set_var("SYD_DO", "exec_in_inaccessible_directory");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/lock:off",
            "-msandbox/exec:on",
            "-msandbox/write:on",
            "-mallow/exec+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert!(status.success(), "status:{status:?}");
    Ok(())
}

fn test_syd_disallow_setuid() -> TestResult {
    let status = syd()
        .args([
            "-ppaludis",
            "-mtrace/allow_unsafe_chmod:0",
            "-msandbox/lock:off",
            "-msandbox/read:off",
            "-msandbox/stat:off",
            "-msandbox/write:off",
            "--",
            "/bin/bash",
            "-c",
            "rm -f test && touch test && chmod u+s test",
        ])
        .status()
        .expect("execute syd");

    assert_ne!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_disallow_setgid() -> TestResult {
    let status = syd()
        .args([
            "-ppaludis",
            "-mtrace/allow_unsafe_chmod:0",
            "-msandbox/lock:off",
            "-msandbox/read:off",
            "-msandbox/stat:off",
            "-msandbox/write:off",
            "--",
            "/bin/bash",
            "-c",
            "rm -f test && touch test && chmod g+s test",
        ])
        .status()
        .expect("execute syd");

    assert_ne!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_fstat_on_pipe() -> TestResult {
    env::set_var("SYD_DO", "fstat_on_pipe");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/stat:on",
            "-mallow/stat+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_fstat_on_socket() -> TestResult {
    env::set_var("SYD_DO", "fstat_on_socket");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/stat:on",
            "-mallow/stat+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_fstat_on_deleted_file() -> TestResult {
    env::set_var("SYD_DO", "fstat_on_deleted_file");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_fstat_on_temp_file() -> TestResult {
    env::set_var("SYD_DO", "fstat_on_temp_file");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_fchmodat_on_proc_fd() -> TestResult {
    env::set_var("SYD_DO", "fchmodat_on_proc_fd");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_linkat_on_fd() -> TestResult {
    env::set_var("SYD_DO", "linkat_on_fd");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_block_ioctl_tiocsti() -> TestResult {
    env::set_var("SYD_DO", "block_ioctl_tiocsti");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_block_prctl_ptrace() -> TestResult {
    env::set_var("SYD_DO", "block_prctl_ptrace");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_block_dev_random() -> TestResult {
    env::set_var("SYD_DO", "block_dev_random");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read,stat,write+/***",
            "-mdeny/read+/dev/random",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_block_dev_urandom() -> TestResult {
    env::set_var("SYD_DO", "block_dev_urandom");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read,stat,write+/***",
            "-mdeny/read+/dev/urandom",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_kill_during_syscall() -> TestResult {
    env::set_var("SYD_DO", "kill_during_syscall");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_open_toolong_path() -> TestResult {
    env::set_var("SYD_DO", "open_toolong_path");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_open_null_path() -> TestResult {
    env::set_var("SYD_DO", "open_null_path");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_utimensat_null() -> TestResult {
    env::set_var("SYD_DO", "utimensat_null");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_normalize_path() -> TestResult {
    const NORMALIZE_PATH_TESTS: &[&str] = &[
        "null",
        "./null",
        ".////null",
        ".///.////.///./null",
        "./././././././null",
        "./././.././././dev/null",
        "../dev/././../dev/././null",
    ];

    for path in NORMALIZE_PATH_TESTS {
        let status = syd()
            .args([
                "-ppaludis",
                "-msandbox/read:off",
                "-msandbox/stat:off",
                "-msandbox/write:on",
                "-msandbox/exec:off",
                "-mdeny/write+/***",
                "-mallow/write+/dev/null",
                "--",
                "sh",
                "-c",
                &format!("cd /dev; :> {path}"),
            ])
            .status()
            .expect("execute syd");
        assert_eq!(
            status.code().unwrap_or(127),
            0,
            "path:{path}, status:{status:?}"
        );
    }

    Ok(())
}

fn test_syd_path_resolution() -> TestResult {
    env::set_var("SYD_DO", "path_resolution");
    let cwd = syd::fs::canonicalize(
        Pid::this(),
        "/proc/self/cwd",
        true,
        syd::fs::MissingHandling::Normal,
    )?
    .to_string_lossy()
    .to_string();

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/",
            "-mallow/read+/etc/***",
            "-mallow/read+/lib/***",
            "-mallow/read+/proc/***",
            "-mallow/read+/usr/***",
            "-mallow/stat+/etc/***",
            "-mallow/stat+/lib/***",
            "-mallow/stat+/proc/***",
            "-mallow/stat+/usr/***",
            &format!("-mallow/read+{cwd}/test_file.txt"),
            &format!("-mallow/stat+{cwd}/test_file.txt"),
            &format!("-mallow/write+{cwd}/test_file.txt"),
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.success(), "status:{status:?}");

    Ok(())
}

fn test_syd_symlink_readonly_path() -> TestResult {
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "-mdeny/write+/",
            "--",
            "sh",
            "-c",
            "ln -s / test_syd_symlink_readonly_path && unlink test_syd_symlink_readonly_path",
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_remove_empty_path() -> TestResult {
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            "sh",
            "-c",
            "env LC_ALL=C LANG=C LANGUAGE=C rm '' 2>&1 | tee /dev/stderr | grep -qi 'No such file or directory'"
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_open_trailing_slash() -> TestResult {
    env::set_var("SYD_DO", "open_trailing_slash");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_openat_trailing_slash() -> TestResult {
    env::set_var("SYD_DO", "openat_trailing_slash");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_lstat_trailing_slash() -> TestResult {
    env::set_var("SYD_DO", "lstat_trailing_slash");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_fstatat_trailing_slash() -> TestResult {
    env::set_var("SYD_DO", "fstatat_trailing_slash");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_mkdir_trailing_dot() -> TestResult {
    env::set_var("SYD_DO", "mkdir_trailing_dot");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_mkdirat_trailing_dot() -> TestResult {
    env::set_var("SYD_DO", "mkdirat_trailing_dot");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_rmdir_trailing_slashdot() -> TestResult {
    env::set_var("SYD_DO", "rmdir_trailing_slashdot");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_fopen_supports_mode_x() -> TestResult {
    env::set_var("SYD_DO", "fopen_supports_mode_x");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_fopen_supports_mode_e() -> TestResult {
    env::set_var("SYD_DO", "fopen_supports_mode_e");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_link_no_symlink_deref() -> TestResult {
    env::set_var("SYD_DO", "link_no_symlink_deref");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_link_posix() -> TestResult {
    env::set_var("SYD_DO", "link_posix");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_linkat_posix() -> TestResult {
    env::set_var("SYD_DO", "linkat_posix");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_getcwd_long() -> TestResult {
    env::set_var("SYD_DO", "getcwd_long");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    // FIXME: Fails on musl, possibly not something to fix.
    ignore!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_creat_thru_dangling() -> TestResult {
    env::set_var("SYD_DO", "creat_thru_dangling");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_mkdirat_non_dir_fd() -> TestResult {
    env::set_var("SYD_DO", "mkdirat_non_dir_fd");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_blocking_udp4() -> TestResult {
    env::set_var("SYD_DO", "blocking_udp4");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mtrace/allow_safe_bind:0",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "-mallow/net/bind+loopback!65432",
            "-mallow/net/connect+loopback!65432",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    // FIXME: This test fails sometimes with
    // 64-bit syd sandboxing 32bit syd-test-do.
    ignore!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_blocking_udp6() -> TestResult {
    env::set_var("SYD_DO", "blocking_udp6");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mtrace/allow_safe_bind:0",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "-mallow/net/bind+loopback6!65432",
            "-mallow/net/connect+loopback6!65432",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    // FIXME: This test fails sometimes with
    // 64-bit syd sandboxing 32bit syd-test-do.
    ignore!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_close_on_exec() -> TestResult {
    env::set_var("SYD_DO", "close_on_exec");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_exp_open_exclusive_restart() -> TestResult {
    env::set_var("SYD_DO", "open_exclusive_restart");

    env::set_var("SYD_TEST_TIMEOUT", "15m");
    let status = syd()
        .env("SYD_LOG", "info")
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");

    if KERNEL_VERSION.0 > 5 || (KERNEL_VERSION.0 == 5 && KERNEL_VERSION.1 >= 19) {
        eprintln!("Linux kernel version is 5.19 or newer, good.");
        assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    } else {
        eprintln!("Skipping test because the Linux kernel is older than 5.19.");
        ignore!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    }
    Ok(())
}

fn test_syd_open_exclusive_repeat() -> TestResult {
    env::set_var("SYD_DO", "open_exclusive_repeat");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_pty_io_rust() -> TestResult {
    env::set_var("SYD_DO", "pty_io_rust");

    env::set_var("SYD_TEST_TIMEOUT", "30s");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_pty_io_gawk() -> TestResult {
    env::set_var("SYD_DO", "pty_io_gawk");
    if !is_program_available("gawk") {
        eprintln!("Test requires gawk, skipping!");
        env::set_var("SYD_TEST_SOFT_FAIL", "1");
        return Ok(());
    }

    env::set_var("SYD_TEST_TIMEOUT", "30s");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_diff_dev_fd() -> TestResult {
    env::set_var("SYD_DO", "diff_dev_fd");
    if !is_program_available("diff") {
        eprintln!("Test requires diff, skipping!");
        env::set_var("SYD_TEST_SOFT_FAIL", "1");
        return Ok(());
    }

    env::set_var("SYD_TEST_TIMEOUT", "30s");
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_fifo_multiple_readers() -> TestResult {
    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            "bash",
            "-c",
        ])
        .arg(
            r#"
# Attempt to DOS syd by spawning multiple FIFO readers in the background.
set -ex
fifo=$(env TMPDIR=. mktemp -u)
mkfifo "$fifo"
pids=()
for i in {1..10}; do
    cat "$fifo" &
    pids+=( $! )
done
# Execute a system call that syd must intervene, this must not block.
touch "$fifo".done
rm -f "$fifo".done
# All good, kill all the cats, wait and exit cleanly.
kill "${pids[@]}" || true
wait
rm -f "$fifo"
# Give syd::m☮☮n thread a chance to clean up for statistics.
sleep 9
true
"#,
        )
        .status()
        .expect("execute syd");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_bind_unix_socket() -> TestResult {
    env::set_var("SYD_DO", "bind_unix_socket");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "-mallow/net/bind+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_signal_protection() -> TestResult {
    match check_unshare() {
        Some(false) => {
            eprintln!("Test requires Linux namespaces, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError(
                "Failed to test for Linux namespaces!".to_string(),
            ));
        }
        _ => {}
    };

    let status = syd()
        .args([
            "-ppaludis",
            "-pcontainer",
            "-mdeny/read-/proc/1/**",
            "-mdeny/stat-/proc/1/**",
            "-mdeny/write-/proc/1/***",
            "--",
            "bash",
            "-c",
        ])
        .arg(
            r#"
set -ex
pid=( $(pgrep -w syd) )
# Sending signal 0 must work.
for tid in "${pid[@]}"; do
    kill -0 ${tid}
    sleep 1
done
# Sending other signals are not permitted.
for sig in INT ABRT STOP KILL; do
    for tid in "${pid[@]}"; do
        kill -${sig} ${tid} && exit 1
        sleep 1
    done
done
"#,
        )
        .status()
        .expect("execute syd");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_exp_emulate_open_fifo_1() -> TestResult {
    env::set_var("SYD_DO", "emulate_open_fifo_1");

    env::set_var("SYD_TEST_TIMEOUT", "15m");
    let status = syd()
        .env("SYD_LOG", "error")
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_exp_emulate_open_fifo_2() -> TestResult {
    env::set_var("SYD_DO", "emulate_open_fifo_2");

    env::set_var("SYD_TEST_TIMEOUT", "15m");
    let status = syd()
        .env("SYD_LOG", "error")
        .args([
            "-ppaludis",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");

    assert_eq!(status.code().unwrap_or(127), 0, "status:{status:?}");
    Ok(())
}

fn test_syd_deny_magiclinks() -> TestResult {
    match check_unshare() {
        Some(false) => {
            eprintln!("Test requires Linux namespaces, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError(
                "Failed to test for Linux namespaces!".to_string(),
            ));
        }
        _ => {}
    }

    // Test expects PID namespace.
    env::set_var("SYD_DO", "deny_magiclinks");
    let status = syd()
        .args([
            "-ppaludis",
            "-munshare/user:1",
            "-munshare/pid:1",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_unshare_user_bypass_limit() -> TestResult {
    match check_unshare() {
        Some(false) => {
            eprintln!("Test requires Linux namespaces, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError(
                "Failed to test for Linux namespaces!".to_string(),
            ));
        }
        _ => {}
    }

    env::set_var("SYD_DO", "unshare_user_bypass_limit");
    let status = syd()
        .args(["-plib", "-pcontainer", "--", &SYD_DO])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_exp_interrupt_mkdir() -> TestResult {
    env::set_var("SYD_DO", "interrupt_mkdir");

    env::set_var("SYD_TEST_TIMEOUT", "15m");
    let status = syd()
        .env("SYD_LOG", "info")
        .args([
            "-ppaludis",
            "-mtrace/allow_safe_bind:f",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_exp_interrupt_bind_ipv4() -> TestResult {
    env::set_var("SYD_DO", "interrupt_bind_ipv4");

    env::set_var("SYD_TEST_TIMEOUT", "15m");
    let status = syd()
        .env("SYD_LOG", "info")
        .args([
            "-ppaludis",
            "-mtrace/allow_safe_bind:f",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_exp_interrupt_bind_unix() -> TestResult {
    env::set_var("SYD_DO", "interrupt_bind_unix");

    env::set_var("SYD_TEST_TIMEOUT", "15m");
    let status = syd()
        .env("SYD_LOG", "info")
        .args([
            "-ppaludis",
            "-mtrace/allow_safe_bind:f",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "-mallow/net/bind+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_exp_interrupt_connect_ipv4() -> TestResult {
    env::set_var("SYD_DO", "interrupt_connect_ipv4");

    env::set_var("SYD_TEST_TIMEOUT", "15m");
    let status = syd()
        .env("SYD_LOG", "info")
        .args([
            "-ppaludis",
            "-mtrace/allow_safe_bind:f",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/write+/***",
            "-mallow/net/bind+loopback!65432",
            "-mallow/net/connect+loopback!65432",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");

    assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_exp_repetitive_clone() -> TestResult {
    eprintln!("Running repetitive clone test for 15 minutes...");

    env::set_var("SYD_DO", "repetitive_clone");
    env::set_var("SYD_TEST_TIMEOUT", "15m");
    let status = syd()
        .env("SYD_LOG", "info")
        .args(["-ppaludis", "--", &SYD_DO])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");

    assert!(
        status.code().unwrap_or(0) == 128 + nix::libc::SIGKILL,
        "status:{status:?}"
    );
    Ok(())
}

fn test_syd_exp_syscall_fuzz_bare() -> TestResult {
    env::set_var("SYD_DO", "syscall_fuzz");
    let epoch = std::time::Instant::now();
    let status = syd()
        .env("SYD_LOG", "warn")
        .args([
            "-ppaludis",
            "-mlock:off",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/exec:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/exec+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    let code = status.code().unwrap_or(127);
    let time = format_duration(epoch.elapsed());
    println!("# fuzz completed in {time} with code {code}.");
    ignore!(code == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_exp_syscall_fuzz_wrap() -> TestResult {
    match check_unshare() {
        Some(false) => {
            eprintln!("Test requires Linux namespaces, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError(
                "Failed to test for Linux namespaces!".to_string(),
            ));
        }
        _ => {}
    }

    env::set_var("SYD_DO", "syscall_fuzz");
    let epoch = std::time::Instant::now();
    let status = syd()
        .env("SYD_LOG", "warn")
        .args([
            "-ppaludis",
            "-pcontainer",
            "-mlock:off",
            "-msandbox/lock:off",
            "-msandbox/read:on",
            "-msandbox/stat:on",
            "-msandbox/write:on",
            "-msandbox/exec:on",
            "-msandbox/net:on",
            "-mallow/read+/***",
            "-mallow/stat+/***",
            "-mallow/exec+/***",
            "-mallow/write+/***",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    let code = status.code().unwrap_or(127);
    let time = format_duration(epoch.elapsed());
    println!("# fuzz completed in {time} with code {code}.");
    ignore!(code == 0, "status:{status:?}");
    Ok(())
}

fn test_syd_pid_thread_deny() -> TestResult {
    match check_pid() {
        Some(false) => {
            eprintln!("Test requires PID sandboxing to work in a container, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError("Failed to test for Pid Sanboxing!".to_string()));
        }
        _ => {}
    }

    env::set_var("SYD_DO", "thread");
    let status = syd()
        .args([
            "-ppaludis",
            "-pcontainer",
            "-msandbox/pid:on",
            "-mpid/max:1",
            "--",
            &SYD_DO,
            "0",
            "24",
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_DO");
    // Early fails get EACCES ie 13.
    // In debug mode, this fails with 101.
    // In release mode, this fails with 134.
    // Both indicate thread::spawn paniced.
    assert!(
        matches!(status.code().unwrap_or(127), 13 | 101 | 134),
        "status:{status:?}"
    );

    Ok(())
}

fn test_syd_pid_fork_deny() -> TestResult {
    match check_pid() {
        Some(false) => {
            eprintln!("Test requires PID sandboxing to work in a container, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError("Failed to test for Pid Sanboxing!".to_string()));
        }
        _ => {}
    }

    env::set_var("SYD_DO", "fork");
    let status = syd()
        .args([
            "-ppaludis",
            "-pcontainer",
            "-msandbox/pid:on",
            "-mpid/max:16",
            "--",
            &SYD_DO,
            "0",
            "24",
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_DO");
    assert!(
        status.code().unwrap_or(127) == nix::libc::EACCES,
        "status:{status:?}"
    );

    Ok(())
}

fn test_syd_pid_fork_bomb() -> TestResult {
    match check_pid() {
        Some(false) => {
            eprintln!("Test requires PID sandboxing to work in a container, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError("Failed to test for Pid Sanboxing!".to_string()));
        }
        _ => {
            eprintln!("PID sandboxing works in a container!");
            eprintln!("Proceeding with test...");
        }
    }

    eprintln!("Running the fork bomb test for 10 seconds...");
    env::set_var("SYD_DO", "fork_bomb");
    env::set_var("SYD_TEST_FORCE", "IKnowWhatIAmDoing");
    env::set_var("SYD_TEST_TIMEOUT", "10s");
    let child = syd()
        .args([
            "-plib",
            "-pcontainer",
            "-msandbox/pid:on",
            "-mpid/max:16",
            "--",
            &SYD_DO,
        ])
        .env("SYD_LOG", "info")
        .stdout(Stdio::inherit())
        .stderr(Stdio::piped())
        .spawn()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");
    env::remove_var("SYD_TEST_FORCE");
    env::remove_var("SYD_DO");

    eprintln!("Done spawning test, waiting test to complete...");
    let output = child.wait_with_output().expect("wait syd");
    let output = String::from_utf8_lossy(&output.stderr);
    eprintln!("{output}");

    // Check if the output contains the specified string and assert based on it
    assert!(
        output.contains(r#""cap":"p""#),
        "syd has not raised a pid violation!"
    );

    Ok(())
}

fn test_syd_pid_fork_bomb_asm() -> TestResult {
    match check_pid() {
        Some(false) => {
            eprintln!("Test requires PID sandboxing to work in a container, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError("Failed to test for Pid Sanboxing!".to_string()));
        }
        _ => {
            eprintln!("PID sandboxing works in a container!");
            eprintln!("Proceeding with test...");
        }
    }

    eprintln!("Running the fork bomb test for 10 seconds...");
    env::set_var("SYD_DO", "fork_bomb_asm");
    env::set_var("SYD_TEST_FORCE", "IKnowWhatIAmDoing");
    env::set_var("SYD_TEST_TIMEOUT", "10s");
    let child = syd()
        .args([
            "-plib",
            "-pcontainer",
            "-msandbox/pid:on",
            "-mpid/max:16",
            "--",
            &SYD_DO,
        ])
        .stdout(Stdio::inherit())
        .stderr(Stdio::piped())
        .spawn()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");
    env::remove_var("SYD_TEST_FORCE");
    env::remove_var("SYD_DO");

    eprintln!("Done spawning test, waiting test to complete...");
    let output = child.wait_with_output().expect("wait syd");
    let output = String::from_utf8_lossy(&output.stderr);
    eprintln!("{output}");

    // Check if the output contains the specified string and assert based on it
    assert!(
        output.contains(r#""cap":"p""#),
        "syd has not raised a pid violation!"
    );

    Ok(())
}

fn test_syd_pid_thread_bomb() -> TestResult {
    match check_pid() {
        Some(false) => {
            eprintln!("Test requires PID sandboxing to work in a container, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError("Failed to test for Pid Sanboxing!".to_string()));
        }
        _ => {
            eprintln!("PID sandboxing works in a container!");
            eprintln!("Proceeding with test...");
        }
    }

    eprintln!("Running the thread bomb test for 10 seconds...");
    env::set_var("SYD_DO", "thread_bomb");
    env::set_var("SYD_TEST_FORCE", "IKnowWhatIAmDoing");
    env::set_var("SYD_TEST_TIMEOUT", "10s");
    let child = syd()
        .args([
            "-plib",
            "-pcontainer",
            "-msandbox/pid:on",
            "-mpid/max:16",
            "--",
            &SYD_DO,
        ])
        .stdout(Stdio::inherit())
        .stderr(Stdio::piped())
        .spawn()
        .expect("execute syd");
    env::remove_var("SYD_TEST_TIMEOUT");
    env::remove_var("SYD_TEST_FORCE");
    env::remove_var("SYD_DO");

    eprintln!("Done spawning test, waiting test to complete...");
    let output = child.wait_with_output().expect("wait syd");
    let output = String::from_utf8_lossy(&output.stderr);
    eprintln!("{output}");

    // Check if the output contains the specified string and assert based on it
    assert!(
        output.contains(r#""cap":"p""#),
        "syd has not raised a pid violation!"
    );

    Ok(())
}

fn test_syd_pid_stress_ng_deny() -> TestResult {
    if !is_program_available("stress-ng") {
        eprintln!("Test requires stress-ng, skipping!");
        env::set_var("SYD_TEST_SOFT_FAIL", "1");
        return Ok(());
    };
    match check_pid() {
        Some(false) => {
            eprintln!("Test requires PID sandboxing to work in a container, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError("Failed to test for Pid Sanboxing!".to_string()));
        }
        _ => {}
    }

    let _ = remove_file("log");
    let status = syd()
        .args([
            "-ppaludis",
            "-pcontainer",
            "-msandbox/pid:on",
            "-mpid/max:1",
            "--",
            "stress-ng",
            "--log-file",
            "log",
            "-c",
            "1",
            "-t",
            "7",
        ])
        .status()
        .expect("execute syd");
    // Fails on CI.
    if !*CI_BUILD {
        assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    } else {
        ignore!(status.code().unwrap_or(127) == 0, "status:{status:?}");
        return Ok(());
    }

    let mut file = File::open("log")?;
    let mut logs = String::new();
    file.read_to_string(&mut logs)?;
    assert!(logs.contains("errno=13"), "logs:{logs:?}");

    Ok(())
}

fn test_syd_pid_stress_ng_allow() -> TestResult {
    if !is_program_available("stress-ng") {
        eprintln!("Test requires stress-ng, skipping!");
        env::set_var("SYD_TEST_SOFT_FAIL", "1");
        return Ok(());
    };
    match check_pid() {
        Some(false) => {
            eprintln!("Test requires PID sandboxing to work in a container, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError("Failed to test for Pid Sanboxing!".to_string()));
        }
        _ => {}
    }

    let _ = remove_file("log");
    let status = syd()
        .args([
            "-ppaludis",
            "-pcontainer",
            "-msandbox/pid:on",
            "-mpid/max:2",
            "--",
            "stress-ng",
            "--log-file",
            "log",
            "-c",
            "1",
            "-t",
            "7",
        ])
        .status()
        .expect("execute syd");
    // Fails on CI.
    if !*CI_BUILD {
        assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    } else {
        ignore!(status.code().unwrap_or(127) == 0, "status:{status:?}");
        return Ok(());
    }

    let mut file = File::open("log")?;
    let mut logs = String::new();
    file.read_to_string(&mut logs)?;

    assert!(!logs.contains("errno="), "logs:{logs:?}");

    Ok(())
}

fn test_syd_pid_stress_ng_fork() -> TestResult {
    if !is_program_available("stress-ng") {
        eprintln!("Test requires stress-ng, skipping!");
        env::set_var("SYD_TEST_SOFT_FAIL", "1");
        return Ok(());
    };
    match check_pid() {
        Some(false) => {
            eprintln!("Test requires PID sandboxing to work in a container, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError("Failed to test for Pid Sanboxing!".to_string()));
        }
        _ => {}
    }

    let _ = remove_file("log");
    let status = syd()
        .env("SYD_LOG", "warn")
        .args([
            "-ppaludis",
            "-pcontainer",
            "-msandbox/pid:on",
            "-mpid/max:128",
            "-mfilter/pid:1",
            "--",
            "stress-ng",
            "--log-file",
            "log",
            "-f",
            "4",
            "-t",
            "15",
            "--fork-max",
            "1024",
        ])
        .status()
        .expect("execute syd");
    // Fails on CI.
    if !*CI_BUILD {
        assert!(status.code().unwrap_or(127) == 0, "status:{status:?}");
    } else {
        ignore!(status.code().unwrap_or(127) == 0, "status:{status:?}");
        return Ok(());
    }

    let mut file = File::open("log")?;
    let mut logs = String::new();
    file.read_to_string(&mut logs)?;
    assert!(!logs.contains("errno="), "logs:{logs:?}");

    Ok(())
}

fn test_syd_mem_alloc() -> TestResult {
    env::set_var("SYD_DO", "alloc");
    env::set_var("SYD_TEST_FORCE", "IKnowWhatIAmDoing");

    let status = syd()
        .args([
            "-ppaludis",
            "-msandbox/mem:on",
            "-mmem/max:128M",
            "-mmem/vm_max:256M",
            "--",
            &SYD_DO,
        ])
        .status()
        .expect("execute syd");
    env::remove_var("SYD_TEST_FORCE");
    env::remove_var("SYD_DO");
    // Segmentation fault is expected.
    // Iot is confusing but happens on alpine+musl.
    // Otherwise we require ENOMEM.
    const SIGIOT: i32 = 128 + nix::libc::SIGIOT;
    const SIGSEGV: i32 = 128 + nix::libc::SIGSEGV;
    assert!(
        matches!(
            status.code().unwrap_or(127),
            nix::libc::ENOMEM | SIGIOT | SIGSEGV
        ),
        "status:{status:?}"
    );

    Ok(())
}

fn test_syd_mem_stress_ng_malloc_1() -> TestResult {
    if !is_program_available("stress-ng") {
        eprintln!("Test requires stress-ng, skipping!");
        env::set_var("SYD_TEST_SOFT_FAIL", "1");
        return Ok(());
    };
    match check_unshare() {
        Some(false) => {
            eprintln!("Test requires Linux namespaces, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError(
                "Failed to test for Linux namespaces!".to_string(),
            ));
        }
        _ => {}
    };

    let command = syd()
        .args([
            "-ppaludis",
            "-pcontainer",
            "-msandbox/mem:on",
            "-mmem/max:32M",
            "-mmem/vm_max:256M",
            "--",
            "stress-ng",
            "-v",
            "-t",
            "5",
            "--malloc",
            "4",
            "--malloc-bytes",
            "128M",
        ])
        .env("SYD_LOG", "info")
        .stdout(Stdio::inherit())
        .stderr(Stdio::piped())
        .spawn()
        .expect("spawn syd");

    let output = command.wait_with_output().expect("wait syd");
    let output = String::from_utf8_lossy(&output.stderr);
    eprintln!("{output}");
    assert!(output.contains(r#""cap":"m""#), "out:{output:?}");

    Ok(())
}

fn test_syd_mem_stress_ng_malloc_2() -> TestResult {
    if !is_program_available("stress-ng") {
        eprintln!("Test requires stress-ng, skipping!");
        env::set_var("SYD_TEST_SOFT_FAIL", "1");
        return Ok(());
    };
    match check_unshare() {
        Some(false) => {
            eprintln!("Test requires Linux namespaces, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError(
                "Failed to test for Linux namespaces!".to_string(),
            ));
        }
        _ => {}
    };

    let command = syd()
        .args([
            "-ppaludis",
            "-pcontainer",
            "-msandbox/mem:on",
            "-mmem/max:32M",
            "-mmem/vm_max:256M",
            "--",
            "stress-ng",
            "-v",
            "-t",
            "5",
            "--malloc",
            "4",
            "--malloc-bytes",
            "128M",
            "--malloc-touch",
        ])
        .env("SYD_LOG", "info")
        .stdout(Stdio::inherit())
        .stderr(Stdio::piped())
        .spawn()
        .expect("spawn syd");

    let output = command.wait_with_output().expect("wait syd");
    let output = String::from_utf8_lossy(&output.stderr);
    eprintln!("{output}");
    assert!(output.contains(r#""cap":"m""#), "out:{output:?}");

    Ok(())
}

fn test_syd_mem_stress_ng_mmap() -> TestResult {
    if !is_program_available("stress-ng") {
        eprintln!("Test requires stress-ng, skipping!");
        env::set_var("SYD_TEST_SOFT_FAIL", "1");
        return Ok(());
    };
    match check_unshare() {
        Some(false) => {
            eprintln!("Test requires Linux namespaces, skipping!");
            env::set_var("SYD_TEST_SOFT_FAIL", "1");
            return Ok(());
        }
        None => {
            return Err(TestError(
                "Failed to test for Linux namespaces!".to_string(),
            ));
        }
        _ => {}
    };

    let command = syd()
        .args([
            "-ppaludis",
            "-pcontainer",
            "-msandbox/mem:on",
            "-mmem/max:16M",
            "-mmem/vm_max:64M",
            "--",
            "stress-ng",
            "-v",
            "-t",
            "5",
            "--mmap",
            "4",
            "--mmap-bytes",
            "1G",
        ])
        .env("SYD_LOG", "info")
        .stdout(Stdio::inherit())
        .stderr(Stdio::piped())
        .spawn()
        .expect("spawn syd");

    let output = command.wait_with_output().expect("wait syd");
    let output = String::from_utf8_lossy(&output.stderr);
    eprintln!("{output}");
    fixup!(output.contains(r#""cap":"m""#), "out:{output:?}");

    Ok(())
}
