//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/test-do.rs: Integration test cases
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
// Based in part upon coreutils' autoconf tests which are
// Copyright (C) 2003-2007, 2009-2023 Free Software Foundation, Inc.
//
// SPDX-License-Identifier: GPL-3.0-or-later

#![allow(clippy::disallowed_methods)]

use std::{
    arch::asm,
    env,
    ffi::{CStr, CString, OsStr, OsString},
    fs,
    fs::File,
    io::{ErrorKind, Read, Write},
    net::{Ipv4Addr, Ipv6Addr, SocketAddrV4, SocketAddrV6, TcpListener, TcpStream},
    os::{
        fd::{AsRawFd, OwnedFd, RawFd},
        unix::{
            ffi::{OsStrExt, OsStringExt},
            fs::{symlink, PermissionsExt},
            net::UnixListener,
            prelude::FromRawFd,
            process::CommandExt,
        },
    },
    path::{Path, PathBuf},
    process::{exit, Command, Stdio},
    sync::{Arc, Barrier},
    thread,
    thread::sleep,
    time::{Duration, Instant},
};

use nix::{
    errno::Errno,
    fcntl::{self, fcntl, open, openat, AtFlags, FcntlArg, FdFlag, OFlag},
    libc::{_exit, pthread_create, pthread_join, pthread_t},
    pty::{grantpt, posix_openpt, ptsname, unlockpt},
    sched::{unshare, CloneFlags},
    sys::{
        signal::{kill, sigaction, SaFlags, SigAction, SigHandler, SigSet, SIGALRM, SIGKILL},
        socket::{
            bind, connect, recvfrom, sendto, socket, AddressFamily, MsgFlags, SockFlag, SockType,
            SockaddrIn, SockaddrIn6, UnixAddr,
        },
        stat::{fchmodat, fstat, fstatat, lstat, mkdirat, stat, umask, FchmodatFlags, Mode, SFlag},
        wait::waitpid,
    },
    unistd::{
        access, chdir, close, fork, mkdir, mkfifo, pipe, read, setsid, symlinkat, unlink, unlinkat,
        write, AccessFlags, ForkResult, Pid, UnlinkatFlags,
    },
    NixPath,
};

type TestCase<'a> = (&'a str, &'a str, fn() -> !);
const TESTS: &[TestCase] = &[
    ("alloc",
     "Keep allocating more and more memory until allocation fails with ENOMEM",
     do_alloc),
    ("thread",
     "Given an exit code and number of threads, spawns threads exiting with random codes and parent exits with the given value",
     do_thread),
    ("fork",
     "Given an exit code and number of processes, spawns processes exiting with random codes and parent exits with the given value",
     do_fork),
    ("connect4",
     "Connect to the given Ipv4 address and port",
     do_connect4),
    ("connect6",
     "Connect to the given Ipv6 address and port",
     do_connect6),
    ("connect4_0",
     "Check if bind to port zero is allowlisted with allowlist_safe_bind for Ipv4 addresses (requires an Ipv4 address as argument)",
     do_connect4_0),
    ("connect6_0",
     "Check if bind to port zero is allowlisted with allowlist_safe_bind for Ipv6 addresses (requires an Ipv6 address as argument)",
     do_connect6_0),
    ("stat_bypass_with_read",
     "Check if stat sandboxing can be bypassed by attempting to read from denylisted path",
     do_stat_bypass_with_read),
    ("stat_bypass_with_write",
     "Check if stat sandboxing can be bypassed by attempting to write to denylisted path",
     do_stat_bypass_with_write),
    ("stat_bypass_with_exec",
     "Check if stat sandboxing can be bypassed by attempting to execute denylisted path",
     do_stat_bypass_with_exec),
    ("symlink_toctou",
     "Escape the sandbox with a symlink attack (assumes /etc/passwd is denylisted)",
     do_symlink_toctou),
    ("symlinkat_toctou",
     "Escape the sandbox with a symlink attack utilizing symlinkat (assumes /etc/passwd is denylisted)",
     do_symlinkat_toctou),
    ("ptrmod_toctou_open",
     "Escape the sandbox with a pointer modification attack (assumes /etc/passwd is denylisted)",
     do_ptrmod_toctou_open),
    ("ptrmod_toctou_creat",
     "Escape the sandbox with a pointer modification attack to create a denylisted file (assumes the file `deny.syd-tmp' is denylisted)",
     do_ptrmod_toctou_creat),
    ("ptrmod_toctou_opath",
     "Leak hidden path in the sandbox with a pointer modification attack (assumes /etc/passwd is hidden)",
     do_ptrmod_toctou_opath),
    ("io_uring_escape",
     "Escape the sandbox by opening and reading a file through io-uring interface (assumes /etc/passwd is denylisted)",
     do_io_uring_escape),
    ("opath_escape",
     "Escape the sandbox by reopening a fd opened initially with O_PATH",
     do_opath_escape),
    ("devfd_escape_chdir",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev)",
     do_devfd_escape_chdir),
    ("devfd_escape_chdir_relpath_1",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(fd/./{fd})",
     do_devfd_escape_chdir_relpath_1),
    ("devfd_escape_chdir_relpath_2",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(./fd/{fd})",
     do_devfd_escape_chdir_relpath_2),
    ("devfd_escape_chdir_relpath_3",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(./fd/././{fd})",
     do_devfd_escape_chdir_relpath_3),
    ("devfd_escape_chdir_relpath_4",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(fd/../fd/{fd})",
     do_devfd_escape_chdir_relpath_4),
    ("devfd_escape_chdir_relpath_5",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(./././fd/{fd})",
     do_devfd_escape_chdir_relpath_5),
    ("devfd_escape_chdir_relpath_6",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(foo/../fd/{fd})",
     do_devfd_escape_chdir_relpath_6),
    ("devfd_escape_chdir_relpath_7",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(fd/foo/..//{fd})",
     do_devfd_escape_chdir_relpath_7),
    ("devfd_escape_chdir_relpath_8",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(fd/foo/.././{fd})",
     do_devfd_escape_chdir_relpath_8),
    ("devfd_escape_chdir_relpath_9",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(fd/foo/bar/../../{fd})",
     do_devfd_escape_chdir_relpath_9),
    ("devfd_escape_chdir_relpath_10",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(././fd/foo/../././{fd})",
     do_devfd_escape_chdir_relpath_10),
    ("devfd_escape_chdir_relpath_11",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(fd/./././foo/../{fd})",
     do_devfd_escape_chdir_relpath_11),
    ("devfd_escape_chdir_relpath_12",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(fd/bar/./../{fd})",
     do_devfd_escape_chdir_relpath_12),
    ("devfd_escape_chdir_relpath_13",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(foo/bar/../../fd/{fd})",
     do_devfd_escape_chdir_relpath_13),
    ("devfd_escape_chdir_relpath_14",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(foo/./bar/../../fd/{fd})",
     do_devfd_escape_chdir_relpath_14),
    ("devfd_escape_chdir_relpath_15",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(././foo/../fd/././{fd})",
     do_devfd_escape_chdir_relpath_15),
    ("devfd_escape_chdir_relpath_16",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(fd/././foo/bar/../.././{fd})",
     do_devfd_escape_chdir_relpath_16),
    ("devfd_escape_chdir_relpath_17",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(fd/foo/./bar/../../{fd})",
     do_devfd_escape_chdir_relpath_17),
    ("devfd_escape_chdir_relpath_18",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(./fd/./bar/.././{fd})",
     do_devfd_escape_chdir_relpath_18),
    ("devfd_escape_chdir_relpath_19",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(fd/../.././fd/./{fd})",
     do_devfd_escape_chdir_relpath_19),
    ("devfd_escape_chdir_relpath_20",
     "Read /dev/fd where self belongs to syd rather than the process utilizing chdir(/dev) and open(fd/./././././././{fd})",
     do_devfd_escape_chdir_relpath_20),
    ("devfd_escape_open",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, fd/{fd})",
     do_devfd_escape_open),
    ("devfd_escape_open_relpath_1",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, fd/./{fd})",
     do_devfd_escape_open_relpath_1),
    ("devfd_escape_open_relpath_2",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, ./fd/{fd})",
     do_devfd_escape_open_relpath_2),
    ("devfd_escape_open_relpath_3",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, ./fd/././{fd})",
     do_devfd_escape_open_relpath_3),
    ("devfd_escape_open_relpath_4",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, fd/../fd/{fd})",
     do_devfd_escape_open_relpath_4),
    ("devfd_escape_open_relpath_5",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, ./././fd/{fd})",
     do_devfd_escape_open_relpath_5),
    ("devfd_escape_open_relpath_6",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, foo/../fd/{fd})",
     do_devfd_escape_open_relpath_6),
    ("devfd_escape_open_relpath_7",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, fd/foo/..//{fd})",
     do_devfd_escape_open_relpath_7),
    ("devfd_escape_open_relpath_8",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, fd/foo/.././{fd})",
     do_devfd_escape_open_relpath_8),
    ("devfd_escape_open_relpath_9",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, fd/foo/bar/../../{fd})",
     do_devfd_escape_open_relpath_9),
    ("devfd_escape_open_relpath_10",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, ././fd/foo/../././{fd})",
     do_devfd_escape_open_relpath_10),
    ("devfd_escape_open_relpath_11",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, fd/./././foo/../{fd})",
     do_devfd_escape_open_relpath_11),
    ("devfd_escape_open_relpath_12",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, fd/bar/./../{fd})",
     do_devfd_escape_open_relpath_12),
    ("devfd_escape_open_relpath_13",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, foo/bar/../../fd/{fd})",
     do_devfd_escape_open_relpath_13),
    ("devfd_escape_open_relpath_14",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, foo/./bar/../../fd/{fd})",
     do_devfd_escape_open_relpath_14),
    ("devfd_escape_open_relpath_15",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, ././foo/../fd/././{fd})",
     do_devfd_escape_open_relpath_15),
    ("devfd_escape_open_relpath_16",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, fd/././foo/bar/../.././{fd})",
     do_devfd_escape_open_relpath_16),
    ("devfd_escape_open_relpath_17",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, fd/foo/./bar/../../{fd})",
     do_devfd_escape_open_relpath_17),
    ("devfd_escape_open_relpath_18",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, ./fd/./bar/.././{fd})",
     do_devfd_escape_open_relpath_18),
    ("devfd_escape_open_relpath_19",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, fd/../.././fd/./{fd})",
     do_devfd_escape_open_relpath_19),
    ("devfd_escape_open_relpath_20",
     "Read /dev/fd where self belongs to syd rather than the process utilizing open(/dev) and openat(dirfd, fd/./././././././{fd})",
     do_devfd_escape_open_relpath_20),
    ("procself_escape_chdir",
     "Read /proc/self where self belongs to syd rather than the process utilizing chdir(/proc) and open(self/status)",
     do_procself_escape_chdir),
    ("procself_escape_chdir_relpath_1",
     "Read /proc/self where self belongs to syd rather than the process utilizing chdir(/proc) and open(self/./status)",
     do_procself_escape_chdir_relpath_1),
    ("procself_escape_chdir_relpath_2",
     "Read /proc/self where self belongs to syd rather than the process utilizing chdir(/proc) and open(./self/status)",
     do_procself_escape_chdir_relpath_2),
    ("procself_escape_chdir_relpath_3",
     "Read /proc/self where self belongs to syd rather than the process utilizing chdir(/proc) and open(./self/././status)",
     do_procself_escape_chdir_relpath_3),
    ("procself_escape_chdir_relpath_4",
     "Read /proc/self where self belongs to syd rather than the process utilizing chdir(/proc) and open(self/../self/status)",
     do_procself_escape_chdir_relpath_4),
    ("procself_escape_chdir_relpath_5",
     "Read /proc/self where self belongs to syd rather than the process utilizing chdir(/proc) and open(./././self/status)",
     do_procself_escape_chdir_relpath_5),
    ("procself_escape_chdir_relpath_6",
     "Read /proc/self where self belongs to syd rather than the process utilizing chdir(/proc) and open(self/../.././self/./status)",
     do_procself_escape_chdir_relpath_6),
    ("procself_escape_chdir_relpath_7",
     "Read /proc/self where self belongs to syd rather than the process utilizing chdir(/proc) and open(self/./././././././status)",
     do_procself_escape_chdir_relpath_7),
    ("procself_escape_open",
     "Read /proc/self where self belongs to syd rather than the process utilizing open(/proc) and openat(dirfd, self/status)",
     do_procself_escape_open),
    ("procself_escape_open_relpath_1",
     "Read /proc/self where self belongs to syd rather than the process utilizing open(/proc) and openat(dirfd, self/./status)",
     do_procself_escape_open_relpath_1),
    ("procself_escape_open_relpath_2",
     "Read /proc/self where self belongs to syd rather than the process utilizing open(/proc) and openat(dirfd, ./self/status)",
     do_procself_escape_open_relpath_2),
    ("procself_escape_open_relpath_3",
     "Read /proc/self where self belongs to syd rather than the process utilizing open(/proc) and openat(dirfd, ./self/././status)",
     do_procself_escape_open_relpath_3),
    ("procself_escape_open_relpath_4",
     "Read /proc/self where self belongs to syd rather than the process utilizing open(/proc) and openat(dirfd, self/../self/status)",
     do_procself_escape_open_relpath_4),
    ("procself_escape_open_relpath_5",
     "Read /proc/self where self belongs to syd rather than the process utilizing open(/proc) and openat(dirfd, ./././self/status)",
     do_procself_escape_open_relpath_5),
    ("procself_escape_open_relpath_6",
     "Read /proc/self where self belongs to syd rather than the process utilizing open(/proc) and openat(dirfd, self/../.././self/./status)",
     do_procself_escape_open_relpath_6),
    ("procself_escape_open_relpath_7",
     "Read /proc/self where self belongs to syd rather than the process utilizing open(/proc) and openat(dirfd, self/./././././././status)",
     do_procself_escape_open_relpath_7),
    ("procself_escape_symlink",
     "Read /proc/self where self belongs to syd rather than the process utilizing symlink(self, /proc/./self/) and open(self/status)",
     do_procself_escape_symlink),
    ("umask_bypass_277", "Set umask to 277 and check if it's bypassed",
     do_umask_bypass_277),
    ("umask_bypass_077", "Set umask to 077 and check if it's bypassed",
     do_umask_bypass_077),
    ("emulate_opath", "Open a file relative to a fd opened with O_PATH",
     do_emulate_opath),
    ("emulate_otmpfile", "Open a file with O_TMPFILE flag",
     do_emulate_otmpfile),
    ("honor_umask", "Check if umask is honored (requires expected file mode as argument)",
     do_honor_umask),
    ("open_utf8_invalid",
     "Check if a file with invalid UTF-8 in its pathname can be opened",
     do_open_utf8_invalid),
    ("exec_in_inaccessible_directory",
     "Check if exec calls work from within an inaccessible directory",
     do_exec_in_inaccessible_directory),
    ("fstat_on_pipe",
     "Check if fstat on a pipe fd succeeds",
     do_fstat_on_pipe),
    ("fstat_on_socket",
     "Check if fstat on a socket fd succeeds",
     do_fstat_on_socket),
    ("fstat_on_deleted_file",
     "Check if fstat on a deleted file with an open fd succeeds",
     do_fstat_on_deleted_file),
    ("fstat_on_temp_file",
     "Check if fstat on a fd opened with O_TMPFILE succeeds",
     do_fstat_on_temp_file),
    ("fchmodat_on_proc_fd",
     "Check if fchmodat on a /proc/self/fd link works",
     do_fchmodat_on_proc_fd),
    ("linkat_on_fd",
     "Check if linkat using a fd and AT_EMPTY_PATH works",
     do_linkat_on_fd),
    ("block_ioctl_tiocsti",
     "Check if TIOCSTI ioctl is properly blocked by the sandbox",
     do_block_ioctl_tiocsti),
    ("block_prctl_ptrace",
     "Check if prctl option PR_SET_PTRACER is blocked by the sandbox",
     do_block_prctl_ptrace),
    ("block_dev_random",
     "Check if getrandom with the flag GRND_RANDOM is blocked by the sandbox",
     do_block_dev_random),
    ("block_dev_urandom",
     "Check if getrandom without the flag GRND_RANDOM is blocked by the sandbox",
     do_block_dev_urandom),
    ("kill_during_syscall",
     "Kill child during a busy system call loop which may hand the sandbox",
     do_kill_during_syscall),
    ("open_toolong_path",
     "Try to open a file with a path name longer than PATH_MAX",
     do_open_toolong_path),
    ("open_null_path",
     "Try to open a file with NULL pointer as path",
     do_open_null_path),
    ("utimensat_null",
     "Try to call utimensat with a NULL pointer as path",
     do_utimensat_null),
    ("path_resolution",
     "Try to open a path with various functionally identical absolute and relative paths",
     do_path_resolution),
    ("emulate_open_fifo_1",
     "Try to open a FIFO and see if the emulated open call deadlocks syd (version 1: pure Rust)",
     do_emulate_open_fifo_1),
    ("emulate_open_fifo_2",
     "Try to open a FIFO and see if the emulated open call deadlocks syd (version 2: using SH)",
     do_emulate_open_fifo_2),
    ("deny_magiclinks",
     "Try to access /proc/1/fd, /proc/1/cwd, and /proc/1/exe and expect ELOOP",
     do_deny_magiclinks),
    ("open_trailing_slash",
     "Check if open with trailing slash is handled correctly",
     do_open_trailing_slash),
    ("openat_trailing_slash",
     "Check if openat with trailing slash is handled correctly",
     do_openat_trailing_slash),
    ("lstat_trailing_slash",
     "Check if lstat with trailing slash is handled correctly",
     do_lstat_trailing_slash),
    ("fstatat_trailing_slash",
     "Check if fstatat with trailing slash is handled correctly",
     do_fstatat_trailing_slash),
    ("mkdir_trailing_dot",
     "Check if mkdir with trailing dot is handled correctly",
     do_mkdir_trailing_dot),
    ("mkdirat_trailing_dot",
     "Check if mkdirat with trailing dot is handled correctly",
     do_mkdirat_trailing_dot),
    ("rmdir_trailing_slashdot",
     "Check if rmdir with trailing slash and dot are handled correctly",
     do_rmdir_trailing_slashdot),
    ("fopen_supports_mode_e",
     "Check if fopen supports mode 'e' in case the libc is GNU Libc",
     do_fopen_supports_mode_e),
    ("fopen_supports_mode_x",
     "Check if fopen supports mode 'x' in case the libc is GNU Libc",
     do_fopen_supports_mode_x),
    ("link_no_symlink_deref",
     "Check if link(2) dereferences symlinks",
     do_link_no_symlink_deref),
    ("link_posix",
     "Check if link(2) obeys POSIX",
     do_link_posix),
    ("linkat_posix",
     "Check if linkat(2) obeys POSIX",
     do_linkat_posix),
    ("getcwd_long",
     "Check if getcwd handles long file names properly by dynamically creating a deep structure and checking for errors at each step",
     do_getcwd_long),
    ("creat_thru_dangling",
     "Check if creating a file throug a dangling symbolic link behaves as expected",
     do_creat_thru_dangling),
    ("mkdirat_non_dir_fd",
     "Check if invalid file descriptors fail with ENOTDIR",
     do_mkdirat_non_dir_fd),
    ("blocking_udp4",
     "Check if blocking UDP with Ipv4 deadlocks the sandbox",
     do_blocking_udp4),
    ("blocking_udp6",
     "Check if blocking UDP with Ipv6 deadlocks the sandbox",
     do_blocking_udp6),
    ("close_on_exec",
     "Check if open with O_CLOEXEC is handled correctly by the sandbox",
     do_close_on_exec),
    ("open_exclusive_restart",
     "Check if open with O_CREAT|O_EXCL works even if restarted after a signal handler",
     do_open_exclusive_restart),
    ("open_exclusive_repeat",
     "Check if open with O_CREAT|O_EXCL works when repeated quickly",
     do_open_exclusive_repeat),
    ("unshare_user_bypass_limit",
     "Check if user namespace limitations can be bypassed by changing the sysctl.",
     do_unshare_user_bypass_limit),
    ("pty_io_rust",
     "Check if input/output with PTYs work (using nix crate, purely in Rust)",
     do_pty_io_rust),
    ("pty_io_gawk",
     "Check if input/output with PTYs work (using GNU Awk)",
     do_pty_io_gawk),
    ("diff_dev_fd",
     "Check if bash -c \"diff -u <(cat /etc/passwd) <(cat /etc/passwd)\"",
     do_diff_dev_fd),
    ("bind_unix_socket",
     "Check if binding to a relative UNIX domain socket works and the umask is preserved.",
     do_bind_unix_socket),
    ("interrupt_mkdir",
     "Check if interrupted mkdir system calls deadlocks syd",
     do_interrupt_mkdir),
    ("interrupt_bind_ipv4",
     "Check if interrupted bind system calls using an Ipv4 address deadlocks syd",
     do_interrupt_bind_ipv4),
    ("interrupt_bind_unix",
     "Check if interrupted bind system calls using a UNIX socket address deadlocks syd",
     do_interrupt_bind_unix),
    ("interrupt_connect_ipv4",
     "Check if interrupted connect system calls using an Ipv4 address deadlocks syd",
     do_interrupt_connect_ipv4),
    ("repetitive_clone",
     "Check if repetitive clone calls cause fork failure under syd",
     do_repetitive_clone),
    ("syscall_fuzz",
     "Fuzz system calls under syd for stress testing (requires: trinity)",
     do_syscall_fuzz),
    ("fork_bomb",
     "Check if a fork bomb in a container crashes syd",
     do_fork_bomb),
    ("fork_bomb_asm",
     "Check if a fork bomb using inline assembly in a container crashes syd",
     do_fork_bomb_asm),
    ("thread_bomb",
     "Check if a thread bomb in a container crashes syd",
     do_thread_bomb),
];

extern "C" fn modify_ptr(ptr: *mut nix::libc::c_void) -> *mut nix::libc::c_void {
    let ptr = ptr as *mut i8;
    for _ in 0..10_000 {
        unsafe {
            ptr.copy_from_nonoverlapping(b"/etc/passwd".as_ptr() as *const _, b"/etc/passwd".len())
        };
    }
    std::ptr::null_mut()
}

extern "C" fn modify_ptr_creat(ptr: *mut nix::libc::c_void) -> *mut nix::libc::c_void {
    let ptr = ptr as *mut i8;
    for _ in 0..10_000 {
        unsafe {
            ptr.copy_from_nonoverlapping(
                b"./deny.syd-tmp".as_ptr() as *const _,
                b"./deny.syd-tmp".len(),
            )
        };
    }
    std::ptr::null_mut()
}

fn retry_open(path: &str, flags: OFlag, mode: Mode) -> Result<RawFd, nix::Error> {
    loop {
        match open(path, flags, mode) {
            Ok(fd) => return Ok(fd),
            Err(Errno::EAGAIN) => {
                sleep(Duration::from_millis(100));
                continue;
            }
            Err(error) => return Err(error),
        }
    }
}

#[cfg(target_arch = "x86_64")]
#[inline(always)]
unsafe fn fork_fast() {
    // Inline assembly for x86-64
    asm!(
        "mov rax, 57", // 57 is the syscall number for fork on x86-64
        "syscall",
        out("rax") _,
    );
}

#[cfg(target_arch = "aarch64")]
#[inline(always)]
unsafe fn fork_fast() {
    asm!(
        "mov x0, 17",  // SIGCHLD
        "mov x1, 0",   // child_stack (null, not recommended)
        "mov x8, 220", // syscall number for clone
        "svc 0",
        options(nostack),
    );
}

#[cfg(not(any(target_arch = "x86_64", target_arch = "aarch64")))]
#[inline(always)]
fn fork_fast() {
    let _ = unsafe { fork() };
}

fn setup_sigalarm_handler(interval: Option<nix::libc::suseconds_t>) {
    extern "C" fn sigalarm_handler(_: nix::libc::c_int) {
        // SIGALRM handler does nothing; it's just here to interrupt syscalls
    }

    let action = SigAction::new(
        SigHandler::Handler(sigalarm_handler),
        SaFlags::SA_RESTART,
        SigSet::empty(),
    );

    unsafe { sigaction(SIGALRM, &action).expect("Failed to set SIGALRM handler") };

    // Raise an alarm every 7 µs by default.
    let it_interval = nix::libc::timeval {
        tv_sec: 0,
        tv_usec: interval.unwrap_or(7),
    };
    let it_value = it_interval;
    let timer = nix::libc::itimerval {
        it_interval,
        it_value,
    };

    unsafe {
        nix::libc::syscall(nix::libc::SYS_setitimer, nix::libc::ITIMER_REAL, &timer, 0);
    }
}

fn setup_ipv4_server() {
    let listener = TcpListener::bind("127.0.0.1:65432").expect("Failed to bind to address");

    loop {
        // Start listening for connections
        match listener.accept() {
            Ok((mut stream, _addr)) => {
                // Send "HELO" message and then close the connection
                let message = "HELO";
                let _ = stream.write_all(message.as_bytes());
            }
            Err(error) => {
                eprintln!("Connection failed: {error}");
                // Continue the loop to listen for the next connection
            }
        }
    }
}

fn help() {
    println!("Usage: env SYD_DO=<command> syd-test-do <args>");
    println!("Commands:");
    for (name, descr, _) in TESTS.iter() {
        println!("- {}: {}", name, descr);
    }
}

fn main() {
    if let Ok(command) = env::var("SYD_DO") {
        if let Some((_, _, test)) = TESTS.iter().find(|&&(name, _, _)| name == command) {
            test();
        }
    }
    help();
    exit(1);
}

fn trinity_available() -> bool {
    Command::new("which")
        .arg("trinity")
        .status()
        .expect("Failed to execute command")
        .success()
}

fn do_fork_bomb() -> ! {
    // Ensure the caller knows what they're doing.
    match env::var("SYD_TEST_FORCE") {
        Ok(ref s) if s == "IKnowWhatIAmDoing" => {}
        _ => {
            eprintln!("Set SYD_TEST_FORCE environment variable to IKnowWhatIAmDoing to continue.");
            exit(1);
        }
    }
    loop {
        unsafe {
            let _ = fork();
        }
    }
}

fn do_fork_bomb_asm() -> ! {
    // Ensure the caller knows what they're doing.
    match env::var("SYD_TEST_FORCE") {
        Ok(ref s) if s == "IKnowWhatIAmDoing" => {}
        _ => {
            eprintln!("Set SYD_TEST_FORCE environment variable to IKnowWhatIAmDoing to continue.");
            exit(1);
        }
    }
    loop {
        unsafe {
            fork_fast();
        }
    }
}

fn do_thread_bomb() -> ! {
    // Ensure the caller knows what they're doing.
    match env::var("SYD_TEST_FORCE") {
        Ok(ref s) if s == "IKnowWhatIAmDoing" => {}
        _ => {
            eprintln!("Set SYD_TEST_FORCE environment variable to IKnowWhatIAmDoing to continue.");
            exit(1);
        }
    }
    loop {
        thread::spawn(|| loop {
            thread::spawn(|| {});
        });
    }
}

fn do_syscall_fuzz() -> ! {
    if !trinity_available() {
        eprintln!("trinity not found in PATH. Skipping the test.");
        exit(0);
    }

    let mut syscalls = Vec::new();
    for syscall in syd::config::HOOK_SYSCALLS {
        match *syscall {
            "faccessat2" | "openat2" | "umount2" => {}
            "stat" => {
                syscalls.push("newstat".to_string());
            }
            "fstat" => {
                syscalls.push("newfstat".to_string());
            }
            "newfstatat" => {
                syscalls.push("fstatat64".to_string());
            }
            name if name.ends_with("32") => {}
            _ => {
                syscalls.push(syscall.to_string());
            }
        }
    }
    syscalls.sort();
    println!(
        "# fuzzing {} system calls with trinity: {}",
        syscalls.len(),
        syscalls.join(", ")
    );

    let nsyscall: usize = match env::var("SYD_DO_FUZZ")
        .unwrap_or("1000000".to_string())
        .parse()
    {
        Ok(n) => n,
        Err(error) => {
            eprintln!("Invalid value for SYD_DO_FUZZ: {error}");
            exit(127);
        }
    };
    let syscalls: Vec<String> = syscalls
        .into_iter()
        .map(|name| format!("-c{name}"))
        .collect();

    // Let the game begin!
    Command::new("trinity")
        .args(["-q", "--stats"])
        .arg(format!("-C{}", *syd::NPROC * 3))
        .arg(format!("-N{nsyscall}"))
        .args(syscalls)
        .stderr(Stdio::inherit())
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .exec();
    eprintln!("Failed to execute trinity: {}", Errno::last());
    exit(1);
}

fn do_interrupt_connect_ipv4() -> ! {
    // Fork the process
    match unsafe { fork() } {
        Ok(ForkResult::Child) => {
            // In child: Set up a simple IPv4 server
            setup_ipv4_server();
            exit(0);
        }
        Ok(ForkResult::Parent { child, .. }) => {
            // In parent: Start connecting in a loop

            // Backspace for progress indicator.
            let back = "\x08".repeat(128);

            // Set up SIGALRM handler.
            setup_sigalarm_handler(None);

            // Run the test multiple times
            const TEST_DURATION: Duration = Duration::from_secs(60 * 15 - 7);
            let epoch = Instant::now();
            let mut i = 0;

            loop {
                // Create a socket to connect to the server
                let sock = match socket(
                    AddressFamily::Inet,
                    SockType::Stream,
                    SockFlag::empty(),
                    None,
                ) {
                    Ok(sock) => sock,
                    Err(error) => {
                        eprintln!("socket creation failed: {error}");
                        let _ = kill(child, SIGKILL);
                        exit(1);
                    }
                };
                // TODO: Starting with nix-0.27 socket returns an OwnedFd.
                // SAFETY: Valid FD.
                let sock = unsafe { OwnedFd::from_raw_fd(sock) };

                let now = Instant::now();
                let addr = SockaddrIn::new(127, 0, 0, 1, 65432);
                let res = connect(sock.as_raw_fd(), &addr);
                i += 1;
                match res {
                    Ok(()) => {}
                    Err(Errno::ECONNREFUSED) => {
                        // Wait for the Ipv4 server to set itself up.
                    }
                    Err(error) => {
                        let sec = now.elapsed().as_secs_f64();
                        eprintln!("Failed to connect to server on attempt {i}: {error}. Time taken {sec} seconds.");
                        let _ = kill(child, SIGKILL);
                        exit(1);
                    }
                };

                let elapsed = epoch.elapsed();
                if elapsed >= TEST_DURATION {
                    eprintln!("Timeout reached. Finalizing test.");
                    break;
                } else if i % 10 == 0 {
                    eprint!(
                        "{}{} attempts in {} seconds, {} seconds left...\t",
                        back,
                        i,
                        elapsed.as_secs(),
                        TEST_DURATION.as_secs().saturating_sub(elapsed.as_secs())
                    );
                }
            }

            eprintln!("Interrupt connect test completed.");
            let _ = kill(child, SIGKILL);
            exit(0);
        }
        Err(error) => {
            eprintln!("Fork failed: {error}");
            exit(1);
        }
    }
}

fn do_interrupt_bind_ipv4() -> ! {
    // Bind to localhost port 65432.
    let addr = SockaddrIn::new(127, 0, 0, 1, 65432);

    // Backspace for progress indicator.
    let back = "\x08".repeat(128);

    // Set up SIGALRM handler.
    setup_sigalarm_handler(None);

    // Run the test multiple times
    const TEST_DURATION: Duration = Duration::from_secs(60 * 15 - 7);
    let epoch = Instant::now();
    let mut i = 0;
    loop {
        let sock = match socket(
            AddressFamily::Inet,
            SockType::Stream,
            SockFlag::empty(),
            None,
        ) {
            Ok(sock) => sock,
            Err(error) => {
                eprintln!("socket creation failed: {error}");
                exit(1);
            }
        };
        // TODO: Starting with nix-0.27 socket returns an OwnedFd.
        // SAFETY: Valid FD.
        let sock = unsafe { OwnedFd::from_raw_fd(sock) };

        let now = Instant::now();
        let res = bind(sock.as_raw_fd(), &addr);
        i += 1;
        if let Err(error) = res {
            let sec = now.elapsed().as_secs_f64();
            eprintln!("Failed to bind to 127.0.0.1 port 65432 attempt {i}: {error}. Time taken {sec} seconds.");
            exit(1);
        }

        let elapsed = epoch.elapsed();
        if elapsed >= TEST_DURATION {
            eprintln!("Timeout reached. Finalizing test.");
            break;
        } else if i % 10 == 0 {
            eprint!(
                "{}{} attempts in {} seconds, {} seconds left...\t",
                back,
                i,
                elapsed.as_secs(),
                TEST_DURATION.as_secs().saturating_sub(elapsed.as_secs())
            );
        }
    }

    eprintln!("Interrupt bind test completed.");
    exit(0);
}

fn do_interrupt_bind_unix() -> ! {
    // Path to the Unix domain socket.
    let path = "test.socket";
    let addr = UnixAddr::new(path).unwrap();

    // Backspace for progress indicator.
    let back = "\x08".repeat(128);

    // Set up SIGALRM handler.
    setup_sigalarm_handler(None);

    // Run the test multiple times
    const TEST_DURATION: Duration = Duration::from_secs(60 * 15 - 7);
    let epoch = Instant::now();
    let mut i = 0;
    loop {
        // Remove the socket file before each iteration.
        let _ = fs::remove_file(path);

        let sock = match socket(
            AddressFamily::Unix,
            SockType::Stream,
            SockFlag::empty(),
            None,
        ) {
            Ok(sock) => sock,
            Err(error) => {
                eprintln!("Socket creation failed: {error}");
                exit(1);
            }
        };
        // SAFETY: Valid FD.
        let sock = unsafe { OwnedFd::from_raw_fd(sock) };

        let now = Instant::now();
        let res = bind(sock.as_raw_fd(), &addr);
        i += 1;
        if let Err(error) = res {
            let sec = now.elapsed().as_secs_f64();
            eprintln!("Failed to bind to Unix domain socket 'test.socket' attempt {i}: {error}. Time taken {sec} seconds.");
            exit(1);
        }

        let elapsed = epoch.elapsed();
        if elapsed >= TEST_DURATION {
            eprintln!("Timeout reached. Finalizing test.");
            break;
        } else if i % 10 == 0 {
            eprint!(
                "{}{} attempts in {} seconds, {} seconds left...\t",
                back,
                i,
                elapsed.as_secs(),
                TEST_DURATION.as_secs().saturating_sub(elapsed.as_secs())
            );
        }
    }

    eprintln!("Interrupt bind test completed.");
    exit(0);
}

fn do_interrupt_mkdir() -> ! {
    // Backspace for progress indicator.
    let back = "\x08".repeat(128);

    // Set up SIGALRM handler.
    setup_sigalarm_handler(Some(3));

    // Run the test multiple times
    const TEST_DURATION: Duration = Duration::from_secs(60 * 15 - 7);
    let epoch = Instant::now();
    let mut i = 0;
    loop {
        // Clean up
        let _ = unlinkat(None, "test.dir", UnlinkatFlags::RemoveDir);

        let now = Instant::now();
        let res = mkdir("test.dir", Mode::from_bits_truncate(0o700));
        i += 1;
        if let Err(error) = res {
            let sec = now.elapsed().as_secs_f64();
            eprintln!(
                "Failed to mkdir test.dir at attempt {i}: {error}. Time taken {sec} seconds."
            );
            exit(1);
        }

        let elapsed = epoch.elapsed();
        if elapsed >= TEST_DURATION {
            eprintln!("Timeout reached. Finalizing test.");
            break;
        } else if i % 10 == 0 {
            eprint!(
                "{}{} attempts in {} seconds, {} seconds left...\t",
                back,
                i,
                elapsed.as_secs(),
                TEST_DURATION.as_secs().saturating_sub(elapsed.as_secs())
            );
        }
    }

    eprintln!("Interrupt mkdir test completed.");
    exit(0);
}

fn do_bind_unix_socket() -> ! {
    const UMASK: nix::libc::mode_t = 0o077;

    // Remove the socket file, ignoring errors
    let _ = fs::remove_file("test.socket");

    // Set an uncommon umask
    let _ = umask(Mode::from_bits_truncate(UMASK));

    // Try to bind to the socket
    let _ = match UnixListener::bind("test.socket") {
        Ok(listener) => listener,
        Err(error) => {
            eprintln!("Failed to bind to socket: {error}");
            exit(1);
        }
    };

    // Calculate expected permissions based on the umask
    let expected_mode = 0o777 & !UMASK;

    // Check if the socket was created with correct permissions
    let metadata = fs::metadata("test.socket").expect("Failed to retrieve metadata");
    let permissions = metadata.permissions();
    let mode = permissions.mode() & 0o777; // Mask out file type bits
    if mode != expected_mode {
        eprintln!("Socket does not have correct permissions. Expected: {expected_mode:o}, Found: {mode:o}");
        exit(1);
    }

    eprintln!("Test succeeded!");
    exit(0);
}

fn do_diff_dev_fd() -> ! {
    // Spawn a child process running the gawk script
    let mut child = Command::new("bash")
        .arg("-c")
        .arg("diff -u <(cat /etc/passwd) <(cat /etc/passwd)")
        .stdin(Stdio::inherit())
        .stdout(Stdio::piped())
        .stderr(Stdio::inherit())
        .spawn()
        .unwrap_or_else(|error| {
            eprintln!("Failed to execute gawk: {error}");
            exit(1);
        });

    // Get stdout handle
    let child_stdout = child.stdout.as_mut().unwrap_or_else(|| {
        eprintln!("Failed to get child stdout.");
        exit(1);
    });

    // Read the output from the child process
    let mut output = String::new();
    let raw_fd = child_stdout.as_raw_fd();
    let mut file = unsafe { File::from_raw_fd(raw_fd) };

    if let Err(error) = file.read_to_string(&mut output) {
        eprintln!("Failed to read output of child process: {error}");
        exit(1);
    }
    println!("Child output: {output}");
    if !output.is_empty() {
        eprintln!("Unexpected output!");
        exit(1);
    }

    // Ensure the child process finishes
    let status = match child.wait() {
        Ok(status) => status,
        Err(error) => {
            eprintln!("Failed to wait for child: {error}");
            exit(1);
        }
    };
    if !status.success() {
        eprintln!("Child process failed.");
        exit(1);
    }

    exit(0);
}

fn do_repetitive_clone() -> ! {
    // Spawn a child process running the shell script
    let mut child = Command::new("bash")
        .args(["-e", "-c"])
        .arg(
            r#"
yes syd |
while read i
do
    name=$(echo "$i" | sed -e 's|syd|box|')
    echo "${name}" >> /dev/null
done
"#,
        )
        .spawn()
        .unwrap_or_else(|error| {
            eprintln!("Failed to execute bash: {error}");
            exit(1);
        });

    // Ensure the child process finishes
    let status = match child.wait() {
        Ok(status) => status,
        Err(error) => {
            eprintln!("Failed to wait for child: {error}");
            exit(1);
        }
    };
    if !status.success() {
        eprintln!("Child process failed.");
        exit(1);
    }

    exit(0);
}

fn do_pty_io_gawk() -> ! {
    // Spawn a child process running the gawk script
    let mut child = Command::new("gawk")
        .arg(
            r#"BEGIN {
            c = "echo 123 > /dev/tty; read x < /dev/tty; echo \"x is $x\"";
            PROCINFO[c, "pty"] = 1;
            c |& getline; print;
            print "abc" |& c;
            c |& getline; print;
        }"#,
        )
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::inherit())
        .spawn()
        .unwrap_or_else(|error| {
            eprintln!("Failed to execute gawk: {error}");
            exit(1);
        });

    // Get stdin and stdout handles
    let child_stdin = child.stdin.as_mut().unwrap_or_else(|| {
        eprintln!("Failed to get child stdin.");
        exit(1);
    });
    let child_stdout = child.stdout.as_mut().unwrap_or_else(|| {
        eprintln!("Failed to get child stdout.");
        exit(1);
    });

    // Write input "abc" to the child process
    if let Err(error) = child_stdin.write_all(b"abc\n") {
        eprintln!("Failed to write input to child process: {error}");
        exit(1);
    }

    // Read the output from the child process
    let mut output = String::new();
    let raw_fd = child_stdout.as_raw_fd();
    let mut file = unsafe { File::from_raw_fd(raw_fd) };

    if let Err(error) = file.read_to_string(&mut output) {
        eprintln!("Failed to read output of child process: {error}");
        exit(1);
    }
    print!("Child output:\n{output}");
    if output != "123\nx is abc\n" {
        eprintln!("Unexpected output!");
        exit(1);
    }

    // Ensure the child process finishes
    let status = match child.wait() {
        Ok(status) => status,
        Err(error) => {
            eprintln!("Failed to wait for child: {error}");
            exit(1);
        }
    };
    if !status.success() {
        eprintln!("Child process failed.");
        exit(1);
    }

    exit(0);
}

fn do_pty_io_rust() -> ! {
    let mut master_fd = match posix_openpt(OFlag::O_RDWR) {
        Ok(fd) => fd,
        Err(error) => {
            eprintln!("Failed to open PTY: {error}");
            exit(1);
        }
    };

    if let Err(error) = grantpt(&master_fd) {
        eprintln!("Failed to grant PTY: {error}");
        exit(1);
    }

    if let Err(error) = unlockpt(&master_fd) {
        eprintln!("Failed to unlock PTY: {error}");
        exit(1);
    }

    let slave_name = match unsafe { ptsname(&master_fd) } {
        Ok(name) => name,
        Err(error) => {
            eprintln!("Failed to get slave PTY name: {error}");
            exit(1);
        }
    };

    let (pipe_read, pipe_write) = match pipe() {
        Ok((r, w)) => (r, w),
        Err(e) => {
            eprintln!("Failed to create pipe: {}", e);
            exit(1);
        }
    };

    match unsafe { fork() } {
        Ok(ForkResult::Parent { .. }) => {
            // Parent process
            close(pipe_write).unwrap();

            // Wait for child to be ready after "123\n"
            let mut buffer = [0];
            read(pipe_read, &mut buffer).unwrap();
            if buffer[0] != 1 {
                eprintln!("Sync error: Child not ready after 123");
                exit(1);
            }

            // Read "123\n" from child
            let mut output_1 = [0; 64];
            let bytes_read = match master_fd.read(&mut output_1) {
                Ok(n) => n,
                Err(error) => {
                    eprintln!("Failed to read 123 from child: {error}");
                    exit(1);
                }
            };
            let output_1 = std::str::from_utf8(&output_1[..bytes_read]).unwrap();

            // Write "abc\n" to child
            if let Err(error) = master_fd.write_all(b"abc\n") {
                eprintln!("Failed to write abc to child: {error}");
                exit(1);
            }

            // Wait for child to be ready after "x is abc\n"
            read(pipe_read, &mut buffer).unwrap();
            if buffer[0] != 2 {
                eprintln!("Sync error: Child not ready after x is abc");
                exit(1);
            } else {
                // Wait a bit for the message to appear in the PTY.
                sleep(Duration::from_millis(100));
            }

            // Read "x is abc\n" from child
            let mut output_2 = [0; 64];
            let bytes_read = match master_fd.read(&mut output_2) {
                Ok(n) => n,
                Err(error) => {
                    eprintln!("Failed to read x is abc from child: {error}");
                    exit(1);
                }
            };
            let output_2 = std::str::from_utf8(&output_2[..bytes_read]).unwrap();

            // Check and finalize the test.
            let output_1 = output_1.trim_end();
            let output_2 = output_2.trim_end();
            eprintln!("Child output: {output_1:?}+{output_2:?}");
            if output_1 != "123" || !output_2.ends_with("x is abc") {
                eprintln!("Unexpected output!");
                exit(1);
            } else {
                eprintln!("Test succeeded!");
                exit(0);
            }
        }
        Ok(ForkResult::Child) => {
            // Child process
            close(pipe_read).unwrap();
            drop(master_fd);

            // Start a new session to set the PTY as the controlling terminal
            setsid().expect("setsid failed");

            eprintln!("Child: opening PTY {slave_name}");
            let slave_fd = match open(slave_name.as_str(), OFlag::O_RDWR, Mode::empty()) {
                Ok(fd) => fd,
                Err(error) => {
                    eprintln!("Failed to open {slave_name}: {error}");
                    exit(1);
                }
            };

            // Make the PTY the controlling terminal
            if unsafe { nix::libc::ioctl(slave_fd, nix::libc::TIOCSCTTY, 0) } == -1 {
                eprintln!(
                    "Failed to set PTY {slave_name} as controlling terminal: {}",
                    Errno::last()
                );
                exit(1);
            }

            eprintln!("Child: opening /dev/tty");
            // Open /dev/tty, which now refers to the slave end of the PTY
            let tty_fd = match open("/dev/tty", OFlag::O_RDWR, Mode::empty()) {
                Ok(fd) => fd,
                Err(error) => {
                    eprintln!("Failed to open /dev/tty: {error}");
                    exit(1);
                }
            };

            // Write "123\n" directly to slave_fd
            if let Err(error) = write(tty_fd, b"123\n") {
                eprintln!("Failed to write 123 to slave FD: {error}");
                exit(1);
            }

            // Notify parent that child is ready
            write(pipe_write, &[1]).unwrap();

            // Read input directly from slave_fd
            let mut input = [0; 64];
            let nbytes = match read(slave_fd, &mut input) {
                Ok(n) => n,
                Err(error) => {
                    eprintln!("Failed to read abc from slave FD: {error}");
                    exit(1);
                }
            };
            let input_str = std::str::from_utf8(&input[..nbytes]).unwrap().trim();

            // Write response directly to slave_fd
            let response = format!("x is {}\n", input_str);
            if let Err(error) = write(tty_fd, response.as_bytes()) {
                eprintln!("Failed to write x is abc to slave FD: {error}");
                exit(1);
            }

            // Notify parent that child is ready
            write(pipe_write, &[2]).unwrap();

            close(slave_fd).unwrap();
            close(tty_fd).unwrap();

            exit(0);
        }
        Err(error) => {
            eprintln!("Failed to fork: {error}");
            exit(1);
        }
    }
}

fn do_open_exclusive_repeat() -> ! {
    // Try to unlink the file; it's okay if it doesn't exist.
    let _ = unlink("test-file");

    // Try to open the file with O_CREAT | O_EXCL
    let fd = match open(
        "test-file",
        OFlag::O_CREAT | OFlag::O_EXCL,
        Mode::from_bits_truncate(0o644),
    ) {
        Ok(fd) => fd,
        Err(error) => {
            eprintln!("Failed to create file exclusively: {error}");
            exit(1);
        }
    };
    let _ = close(fd);

    // Try the same again right after.
    match open(
        "test-file",
        OFlag::O_CREAT | OFlag::O_EXCL,
        Mode::from_bits_truncate(0o644),
    ) {
        Ok(fd) => {
            let _ = close(fd);
            eprintln!("Second exclusive open succeded!");
            exit(1);
        }
        Err(error) => {
            eprintln!("Failed to create file exclusively: {error}");
        }
    }

    eprintln!("Test succeeded!");
    exit(0);
}

fn do_open_exclusive_restart() -> ! {
    // Try to unlink the file; it's okay if it doesn't exist.
    let _ = unlink("test-file");

    // Backspace for progress indicator.
    let back = "\x08".repeat(128);

    // Set up SIGALRM handler.
    setup_sigalarm_handler(Some(5));

    const TEST_DURATION: Duration = Duration::from_secs(60 * 15 - 7);
    let epoch = Instant::now();
    let mut i = 0;
    loop {
        // Try to open the file with O_CREAT | O_EXCL
        let fd = match open(
            "test-file",
            OFlag::O_CREAT | OFlag::O_EXCL,
            Mode::from_bits_truncate(0o644),
        ) {
            Ok(fd) => fd,
            Err(Errno::EINTR) => continue,
            Err(error) => {
                eprintln!("Failed to create file exclusively: {error}");
                exit(1);
            }
        };

        // If we're here, it means open succeeded;
        // close the file descriptor and remove the file.
        let _ = close(fd);
        let _ = unlink("test-file");

        i += 1;
        let elapsed = epoch.elapsed();
        if elapsed >= TEST_DURATION {
            eprintln!("Timeout reached. Finalizing test.");
            break;
        } else if i % 10 == 0 {
            eprint!(
                "{}{} attempts in {} seconds, {} seconds left...\t",
                back,
                i,
                elapsed.as_secs(),
                TEST_DURATION.as_secs().saturating_sub(elapsed.as_secs())
            );
        }
    }

    eprintln!("Test succeeded!");
    exit(0);
}

fn do_unshare_user_bypass_limit() -> ! {
    // Step 1: Write a large number to the file
    if let Err(error) = File::create("/proc/sys/user/max_user_namespaces")
        .and_then(|mut file| file.write_all(b"10000"))
    {
        eprintln!("Failed to write to /proc/sys/user/max_user_namespaces: {error:?}");
        //Let's fallthrough and check for sure.
        //exit(1);
    }

    // Step 2: Attempt to unshare with CLONE_NEWUSER
    match unshare(CloneFlags::CLONE_NEWUSER) {
        Ok(_) => {
            eprintln!("Test failed: user namespace limitation bypassed!");
            exit(1);
        }
        Err(Errno::ENOSPC) => {
            eprintln!("Expected error occurred: {:?}", Errno::ENOSPC);
            exit(0);
        }
        Err(error) => {
            eprintln!("Unexpected error occurred: {error:?}");
            exit(1);
        }
    }
}

fn do_close_on_exec() -> ! {
    // 1. Open /dev/null with O_CLOEXEC using open
    let fd = match open(
        "/dev/null",
        OFlag::O_RDONLY | OFlag::O_CLOEXEC,
        Mode::empty(),
    ) {
        Ok(fd) => fd,
        Err(error) => {
            eprintln!("Error opening /dev/null with open: {error}");
            exit(1);
        }
    };

    // Check if O_CLOEXEC is set
    let flags = match fcntl(fd, FcntlArg::F_GETFD) {
        Ok(flags) => flags,
        Err(error) => {
            eprintln!("Error getting flags with fcntl: {error}");
            exit(1);
        }
    };
    let _ = close(fd); // Close the file descriptor

    if flags & nix::libc::FD_CLOEXEC == 0 {
        eprintln!("O_CLOEXEC is not set for open: {flags:?}");
        exit(1);
    }

    // 2. Open /dev/null with O_CLOEXEC using openat
    let fd = match openat(
        nix::libc::AT_FDCWD,
        "/dev/null",
        OFlag::O_RDONLY | OFlag::O_CLOEXEC,
        Mode::empty(),
    ) {
        Ok(fd) => fd,
        Err(error) => {
            eprintln!("Error opening /dev/null with openat: {error}");
            exit(1);
        }
    };

    // Check if O_CLOEXEC is set
    let flags = match fcntl(fd, FcntlArg::F_GETFD) {
        Ok(flags) => flags,
        Err(error) => {
            eprintln!("Error getting flags with fcntl: {error}");
            exit(1);
        }
    };
    let _ = close(fd); // Close the file descriptor

    if flags & nix::libc::FD_CLOEXEC == 0 {
        eprintln!("O_CLOEXEC is not set for openat: {flags:?}");
        exit(1);
    }

    // If both checks pass, print success message and exit with 0
    eprintln!("Success: O_CLOEXEC is set for both open and openat");
    exit(0);
}

fn do_blocking_udp6() -> ! {
    const NUM_THREADS: usize = 64;
    const SERVER_PORT: u16 = 65432;

    // Spawn a blocking UDP server
    thread::spawn(move || {
        let server_fd = socket(
            AddressFamily::Inet6,
            SockType::Datagram,
            SockFlag::empty(),
            None,
        )
        .unwrap_or_else(|error| {
            eprintln!("Failed to create server socket: {error}");
            exit(1);
        });
        // SAFETY: Valid FD.
        let server_fd = unsafe { OwnedFd::from_raw_fd(server_fd) };

        let sockaddr_v6 = SocketAddrV6::new("::1".parse().unwrap(), SERVER_PORT, 0, 0);
        let sockaddr = SockaddrIn6::from(sockaddr_v6);
        bind(server_fd.as_raw_fd(), &sockaddr).unwrap_or_else(|error| {
            eprintln!("Failed to bind server socket: {error}");
            exit(1);
        });

        let mut buf = [0; 1024];
        loop {
            // Blocking call to receive data
            let (len, _) =
                recvfrom::<SockaddrIn6>(server_fd.as_raw_fd(), &mut buf).unwrap_or_else(|error| {
                    eprintln!("Server failed to receive data: {error}");
                    exit(1);
                });
            eprintln!("Server received: {:?}", &buf[..len]);
        }
    });

    let barrier = Arc::new(Barrier::new(NUM_THREADS + 1));

    // Spawn many threads connecting to the UDP server
    for _ in 0..NUM_THREADS {
        let barrier = Arc::clone(&barrier);
        thread::spawn(move || {
            let client_fd = socket(
                AddressFamily::Inet6,
                SockType::Datagram,
                SockFlag::empty(),
                None,
            )
            .unwrap_or_else(|error| {
                eprintln!("Failed to create client socket: {error}");
                exit(1);
            });
            // SAFETY: Valid FD.
            let client_fd = unsafe { OwnedFd::from_raw_fd(client_fd) };

            let sockaddr_v6 = SocketAddrV6::new("::1".parse().unwrap(), SERVER_PORT, 0, 0);
            let sockaddr = SockaddrIn6::from(sockaddr_v6);
            connect(client_fd.as_raw_fd(), &sockaddr).unwrap_or_else(|error| {
                eprintln!("Client failed to connect: {error}");
                exit(1);
            });

            barrier.wait();

            // Now all threads will send data simultaneously
            sendto(
                client_fd.as_raw_fd(),
                b"Heavy is the root of light. Still is the master of moving.",
                &sockaddr,
                MsgFlags::empty(),
            )
            .unwrap_or_else(|error| {
                eprintln!("Client failed to send data: {error}");
                exit(1);
            });

            let mut buf = [0; 1024];
            recvfrom::<SockaddrIn6>(client_fd.as_raw_fd(), &mut buf).unwrap_or_else(|error| {
                eprintln!("Client failed to receive data: {error}");
                exit(1);
            });
        });
    }

    // Ensure main thread waits for all child threads
    barrier.wait();
    eprintln!("Test completed successfully!");
    exit(0);
}

fn do_blocking_udp4() -> ! {
    const NUM_THREADS: usize = 64;
    const SERVER_PORT: u16 = 65432;

    // Spawn a blocking UDP server
    thread::spawn(move || {
        let server_fd = socket(
            AddressFamily::Inet,
            SockType::Datagram,
            SockFlag::empty(),
            None,
        )
        .unwrap_or_else(|error| {
            eprintln!("Failed to create server socket: {error}");
            exit(1);
        });
        // SAFETY: Valid FD.
        let server_fd = unsafe { OwnedFd::from_raw_fd(server_fd) };

        let sockaddr = SockaddrIn::new(127, 0, 0, 1, SERVER_PORT);
        bind(server_fd.as_raw_fd(), &sockaddr).unwrap_or_else(|error| {
            eprintln!("Failed to bind server socket: {error}");
            exit(1);
        });

        let mut buf = [0; 1024];
        loop {
            // Blocking call to receive data
            let (len, _) =
                recvfrom::<SockaddrIn>(server_fd.as_raw_fd(), &mut buf).unwrap_or_else(|error| {
                    eprintln!("Server failed to receive data: {error}");
                    exit(1);
                });
            eprintln!("Server received: {:?}", &buf[..len]);
        }
    });

    let barrier = Arc::new(Barrier::new(NUM_THREADS + 1));

    // Spawn many threads connecting to the UDP server
    for _ in 0..NUM_THREADS {
        let barrier = Arc::clone(&barrier);
        thread::spawn(move || {
            let client_fd = socket(
                AddressFamily::Inet,
                SockType::Datagram,
                SockFlag::empty(),
                None,
            )
            .unwrap_or_else(|error| {
                eprintln!("Failed to create client socket: {error}");
                exit(1);
            });

            let sockaddr = SockaddrIn::new(127, 0, 0, 1, SERVER_PORT);
            connect(client_fd.as_raw_fd(), &sockaddr).unwrap_or_else(|error| {
                eprintln!("Client failed to connect: {error}");
                exit(1);
            });

            barrier.wait();

            // Now all threads will send data simultaneously
            sendto(
                client_fd.as_raw_fd(),
                b"Heavy is the root of light. Still is the master of moving.",
                &sockaddr,
                MsgFlags::empty(),
            )
            .unwrap_or_else(|error| {
                eprintln!("Client failed to send data: {error}");
                exit(1);
            });

            let mut buf = [0; 1024];
            recvfrom::<SockaddrIn>(client_fd.as_raw_fd(), &mut buf).unwrap_or_else(|error| {
                eprintln!("Client failed to receive data: {error}");
                exit(1);
            });
        });
    }

    // Ensure main thread waits for all child threads
    barrier.wait();
    eprintln!("Test completed successfully!");
    exit(0);
}

fn do_mkdirat_non_dir_fd() -> ! {
    // Open a file descriptor to /dev/null
    let fd = open("/dev/null", OFlag::O_RDONLY, Mode::empty()).expect("Failed to open /dev/null");

    // Try to use this file descriptor as a directory file descriptor
    let result = mkdirat(fd, "dir", Mode::from_bits_truncate(0o700));

    // Close the file descriptor
    let _ = close(fd);

    match result {
        Err(Errno::ENOTDIR) => {
            // Expected error case: parent is not a directory
            eprintln!("Attempted to create directory under /dev/null, got ENOTDIR as expected.");
            exit(0);
        }
        Err(error) => {
            // Any other error case
            eprintln!("Unexpected error while trying to create directory under /dev/null: {error}");
            exit(1);
        }
        Ok(_) => {
            // Unexpected success case
            eprintln!("Unexpectedly succeeded in creating directory under /dev/null.");
            exit(1);
        }
    }
}

fn do_creat_thru_dangling() -> ! {
    let mut result = 0;

    // Create a dangling symlink
    if let Err(error) = symlink("no-such", "dangle") {
        eprintln!("symlink(no-such, dangle) failed: {error}");
        result |= 1;
    }

    // Test open with O_CREAT | O_EXCL flags
    match open(
        "dangle",
        OFlag::O_WRONLY | OFlag::O_CREAT | OFlag::O_EXCL,
        Mode::empty(),
    ) {
        Ok(fd) => {
            let _ = close(fd);
            let _ = unlink("no-such");
            eprintln!("Opening dangling symlink with O_CREAT|O_EXCL created the target file unexpectedly.");
            result |= 2;
        }
        Err(Errno::EEXIST) => {
            eprintln!("Opening dangling symlink with O_CREAT|O_EXCL failed with EEXIST.");
        }
        Err(error) => {
            eprintln!("Opening dangling symlink with O_CREAT|O_EXCL failed with unexpected error: {error}");
            result |= 4;
        }
    }

    // Test openat with O_CREAT | O_EXCL flags
    match openat(
        nix::libc::AT_FDCWD,
        "dangle",
        OFlag::O_WRONLY | OFlag::O_CREAT | OFlag::O_EXCL,
        Mode::empty(),
    ) {
        Ok(fd) => {
            let _ = close(fd);
            let _ = unlink("no-such");
            eprintln!("openat'ing dangling symlink with O_CREAT|O_EXCL created the target file unexpectedly.");
            result |= 8;
        }
        Err(Errno::EEXIST) => {
            eprintln!("openat'ing dangling symlink with O_CREAT|O_EXCL failed with EEXIST.");
        }
        Err(error) => {
            eprintln!("openat'ing dangling symlink with O_CREAT|O_EXCL failed with unexpected error: {error}");
            result |= 16;
        }
    }

    // Test open with O_CREAT flag
    match open("dangle", OFlag::O_WRONLY | OFlag::O_CREAT, Mode::empty()) {
        Ok(fd) => {
            let _ = close(fd);
            let _ = unlink("no-such");
            eprintln!("Opening dangling symlink with O_CREAT created the target file as expected.");
        }
        Err(error) => {
            eprintln!("Opening dangling symlink with O_CREAT failed: {error}");
            result |= 32;
        }
    }

    // Test openat with O_CREAT flag
    match openat(
        nix::libc::AT_FDCWD,
        "dangle",
        OFlag::O_WRONLY | OFlag::O_CREAT,
        Mode::empty(),
    ) {
        Ok(fd) => {
            let _ = close(fd);
            let _ = unlink("no-such");
            eprintln!(
                "openat'ing dangling symlink with O_CREAT created the target file as expected."
            );
        }
        Err(error) => {
            eprintln!("openat'ing dangling symlink with O_CREAT failed: {error}");
            result |= 64;
        }
    }

    // Cleanup
    let _ = unlink("dangle");

    if result == 0 {
        eprintln!("Test succeded!");
        exit(0);
    } else {
        eprintln!("Test failed: {result}");
        exit(1);
    }
}

fn do_getcwd_long() -> ! {
    const PATH_MAX: usize = nix::libc::PATH_MAX as usize;
    const DIR_NAME: &str = "confdir3";
    const DIR_NAME_SIZE: usize = DIR_NAME.len() + 1;
    const DOTDOTSLASH_LEN: usize = 3;
    const BUF_SLOP: usize = 20;

    let max = nix::libc::PATH_MAX as usize;
    let mut buf = vec![0; max * (DIR_NAME_SIZE / DOTDOTSLASH_LEN + 1) + DIR_NAME_SIZE + BUF_SLOP];
    let cwd = unsafe { nix::libc::getcwd(buf.as_mut_ptr(), max) };
    if cwd.is_null() {
        eprintln!("Failed to get current working directory: {}", Errno::last());
        exit(1);
    }
    let cwd = unsafe { CStr::from_ptr(cwd) };
    let cwd = OsString::from_vec(cwd.to_bytes().to_vec());
    let cwd = Path::new(&cwd);

    let mut cwd_len = cwd.as_os_str().len();
    let initial_cwd_len = cwd_len;
    let mut n_chdirs = 0;
    let mut fail = 0;

    loop {
        let dotdot_max = PATH_MAX * (DIR_NAME_SIZE / DOTDOTSLASH_LEN);
        let mut c: Option<usize> = None;

        cwd_len += DIR_NAME_SIZE;
        let dir_path = Path::new(DIR_NAME);
        if mkdir(dir_path, Mode::from_bits_truncate(0o700)).is_err() || chdir(dir_path).is_err() {
            if Errno::last() != Errno::ERANGE && Errno::last() != Errno::ENOENT {
                #[cfg(target_os = "linux")]
                if Errno::last() != Errno::EINVAL {
                    fail = 20;
                }
            }
            break;
        }

        if (PATH_MAX..PATH_MAX + DIR_NAME_SIZE).contains(&cwd_len) {
            let cwd = unsafe { nix::libc::getcwd(buf.as_mut_ptr(), max) };
            if cwd.is_null() && Errno::last() == Errno::ENOENT {
                fail = 11;
                eprintln!("getcwd is partly working: {fail}");
                break;
            }
            if !cwd.is_null() {
                fail = 31;
                eprintln!("getcwd has the AIX bug!");
                break;
            }
            if Errno::last() != Errno::ERANGE {
                fail = 21;
                eprintln!("getcwd isn't working ({fail}): {}", Errno::last());
                break;
            }

            let cwd = unsafe { nix::libc::getcwd(buf.as_mut_ptr(), cwd_len + 1) };
            if !cwd.is_null() {
                let cwd = unsafe { CStr::from_ptr(cwd) };
                let cwd = OsString::from_vec(cwd.to_bytes().to_vec());
                let cwd = Path::new(&cwd);
                if stat(cwd).is_err() && Errno::last() == Errno::ERANGE {
                    eprintln!("getcwd works but with shorter paths.");
                    fail = 32;
                    break;
                }
            }
            c = Some(unsafe { CStr::from_ptr(cwd) }.len());
        }

        if dotdot_max <= cwd_len - initial_cwd_len {
            if dotdot_max + DIR_NAME_SIZE + BUF_SLOP < cwd_len - initial_cwd_len {
                break;
            }
            let cwd = unsafe { nix::libc::getcwd(buf.as_mut_ptr(), cwd_len + 1) };
            if cwd.is_null() {
                match Errno::last() {
                    Errno::ERANGE | Errno::ENOENT | Errno::ENAMETOOLONG => {
                        fail = 12;
                        eprintln!("getcwd is partly working: {fail}: {}", Errno::last());
                        break;
                    }
                    errno => {
                        eprintln!("getcwd isn't working ({fail}): {errno}");
                        fail = 22;
                        break;
                    }
                }
            } else {
                c = Some(unsafe { CStr::from_ptr(cwd) }.len());
            }
        }

        if let Some(len) = c {
            if len != cwd_len {
                fail = 23;
                eprintln!("getcwd isn't working ({fail}).");
                break;
            }
        }
        n_chdirs += 1;
    }

    let path = Path::new(DIR_NAME);
    let path = CString::new(path.as_os_str().as_bytes()).unwrap();
    unsafe { nix::libc::rmdir(path.as_ptr()) };
    for _ in 0..=n_chdirs {
        if chdir(Path::new("..")).is_ok() && unsafe { nix::libc::rmdir(path.as_ptr()) } == 0 {
            break;
        }
    }

    if fail == 0 {
        eprintln!("Test succeded!");
        exit(0);
    } else if fail < 20 {
        eprintln!("Test succeded partially!");
        exit(0);
    } else {
        eprintln!("Test failed: {fail}");
        exit(fail);
    }
}

fn do_linkat_posix() -> ! {
    // Cleanup any existing files
    let _ = unlink("conftest.a");
    let _ = unlink("conftest.b");
    let _ = unlink("conftest.lnk");
    let file_a = CString::new("conftest.a").unwrap();
    let file_b = CString::new("conftest.b").unwrap();
    let s_link = CString::new("conftest.lnk").unwrap();
    let file_b_slash = CString::new("conftest.b/").unwrap();
    let s_link_slash = CString::new("conftest.lnk/").unwrap();

    // Create a regular file
    let fd = unsafe { nix::libc::creat(file_a.as_ptr(), 0o644) };
    if fd < 0 {
        eprintln!("Failed to create conftest.a: {}", Errno::last());
        exit(1);
    }
    let _ = unsafe { nix::libc::close(fd) };

    // Create a symlink
    if unsafe { nix::libc::symlink(file_a.as_ptr(), s_link.as_ptr()) } != 0 {
        eprintln!("Failed to create symlink: {}", Errno::last());
        exit(1);
    }

    // Check whether link obeys POSIX
    let mut result = 0;

    if unsafe {
        nix::libc::linkat(
            nix::libc::AT_FDCWD,
            file_a.as_ptr(),
            nix::libc::AT_FDCWD,
            file_b_slash.as_ptr(),
            0,
        )
    } == 0
    {
        result |= 1;
    }

    let mut sb: nix::libc::stat = unsafe { std::mem::zeroed() };
    if unsafe { nix::libc::lstat(s_link_slash.as_ptr(), &mut sb) } == 0
        && unsafe {
            nix::libc::linkat(
                nix::libc::AT_FDCWD,
                s_link_slash.as_ptr(),
                nix::libc::AT_FDCWD,
                file_b.as_ptr(),
                0,
            )
        } == 0
    {
        result |= 2;
    }

    if unsafe { nix::libc::rename(file_a.as_ptr(), file_b.as_ptr()) } != 0 {
        result |= 4;
    }

    if unsafe {
        nix::libc::linkat(
            nix::libc::AT_FDCWD,
            file_b.as_ptr(),
            nix::libc::AT_FDCWD,
            s_link.as_ptr(),
            0,
        )
    } == 0
    {
        result |= 8;
    }

    // Cleanup
    let _ = unlink("conftest.a");
    let _ = unlink("conftest.b");
    let _ = unlink("conftest.lnk");

    if result != 0 {
        eprintln!("Test failed: linkat does not obey POSIX: {result}");
        exit(1);
    }

    eprintln!("Test succeeded: linkat obeys POSIX");
    exit(0);
}

fn do_link_posix() -> ! {
    // Cleanup any existing files
    let _ = unlink("conftest.a");
    let _ = unlink("conftest.b");
    let _ = unlink("conftest.lnk");
    let file_a = CString::new("conftest.a").unwrap();
    let file_b = CString::new("conftest.b").unwrap();
    let s_link = CString::new("conftest.lnk").unwrap();
    let file_b_slash = CString::new("conftest.b/").unwrap();
    let s_link_slash = CString::new("conftest.lnk/").unwrap();

    // Create a regular file
    let fd = unsafe { nix::libc::creat(file_a.as_ptr(), 0o644) };
    if fd < 0 {
        eprintln!("Failed to create conftest.a: {}", Errno::last());
        exit(1);
    }
    let _ = unsafe { nix::libc::close(fd) };

    // Create a symlink
    if unsafe { nix::libc::symlink(file_a.as_ptr(), s_link.as_ptr()) } != 0 {
        eprintln!("Failed to create symlink: {}", Errno::last());
        exit(1);
    }

    // Check whether link obeys POSIX
    let mut result = 0;

    if unsafe { nix::libc::link(file_a.as_ptr(), file_b_slash.as_ptr()) } == 0 {
        result |= 1;
    }

    let mut sb: nix::libc::stat = unsafe { std::mem::zeroed() };
    if unsafe { nix::libc::lstat(s_link_slash.as_ptr(), &mut sb) } == 0
        && unsafe { nix::libc::link(s_link_slash.as_ptr(), file_b.as_ptr()) } == 0
    {
        result |= 2;
    }

    if unsafe { nix::libc::rename(file_a.as_ptr(), file_b.as_ptr()) } != 0 {
        result |= 4;
    }

    if unsafe { nix::libc::link(file_b.as_ptr(), s_link.as_ptr()) } == 0 {
        result |= 8;
    }

    // Cleanup
    let _ = unlink("conftest.a");
    let _ = unlink("conftest.b");
    let _ = unlink("conftest.lnk");

    if result != 0 {
        eprintln!("Test failed: link does not obey POSIX: {result}");
        exit(1);
    }

    eprintln!("Test succeeded: link obeys POSIX");
    exit(0);
}

fn do_link_no_symlink_deref() -> ! {
    // Cleanup any existing files
    let _ = unlink("conftest.file");
    let _ = unlink("conftest.sym");
    let _ = unlink("conftest.hard");
    let file = CString::new("conftest.file").unwrap();
    let slink = CString::new("conftest.sym").unwrap();
    let hlink = CString::new("conftest.hard").unwrap();

    // Create a regular file.
    let fd = unsafe { nix::libc::creat(file.as_ptr(), 0o644) };
    if fd < 0 {
        eprintln!("Failed to create conftest.file: {}", Errno::last());
        exit(1);
    }
    let _ = unsafe { nix::libc::close(fd) };

    // Create a symlink to the regular file.
    if unsafe { nix::libc::symlink(file.as_ptr(), slink.as_ptr()) } != 0 {
        eprintln!("Failed to create symlink");
        exit(1);
    }

    // Attempt to create a hard link to the symlink.
    if unsafe { nix::libc::link(slink.as_ptr(), hlink.as_ptr()) } != 0 {
        eprintln!("Failed to create hard link: {}", Errno::last());
        exit(1);
    }

    // Check the metadata of the hard link and the file
    // If the dev/inode of hard and file are the same, then
    // the link call followed the symlink.
    let mut sb_hard: nix::libc::stat = unsafe { std::mem::zeroed() };
    let mut sb_file: nix::libc::stat = unsafe { std::mem::zeroed() };
    let mut sb_link: nix::libc::stat = unsafe { std::mem::zeroed() };

    if unsafe { nix::libc::lstat(hlink.as_ptr(), &mut sb_hard) } != 0
        || unsafe { nix::libc::lstat(slink.as_ptr(), &mut sb_link) } != 0
        || unsafe { nix::libc::stat(file.as_ptr(), &mut sb_file) } != 0
    {
        eprintln!("Failed to get file metadata: {}", Errno::last());
        exit(1);
    }

    // If the dev/inode of hard and file are the same, then the link call followed the symlink.
    if sb_hard.st_dev == sb_file.st_dev && sb_hard.st_ino == sb_file.st_ino {
        eprintln!("Test failed: link(2) dereferences symbolic links:");
        eprintln!("file: {sb_file:?}");
        eprintln!("link: {sb_link:?}");
        eprintln!("hard: {sb_hard:?}");
        exit(1);
    }

    // Cleanup
    let _ = unlink("conftest.file");
    let _ = unlink("conftest.sym");
    let _ = unlink("conftest.hard");

    eprintln!("Test succeeded: link(2) does not dereference symbolic links");
    exit(0);
}

fn do_fopen_supports_mode_e() -> ! {
    let mut result = 0;
    let filename = CString::new("conftest.e").unwrap();
    let mode_re = CString::new("re").unwrap();

    let _ = unlink("conftest.e");
    let mut file = fs::File::create("conftest.e").expect("Failed to create conftest.e");
    file.write_all(b"Heavy is the root of light. Still is the master of moving.")
        .expect("Failed to write to conftest.x");

    let fp = unsafe { nix::libc::fopen(filename.as_ptr(), mode_re.as_ptr()) };
    if !fp.is_null() {
        let fd = unsafe { nix::libc::fileno(fp) };
        let flags = FdFlag::from_bits_truncate(
            fcntl::fcntl(fd, fcntl::F_GETFD).expect("Failed to get file descriptor flags"),
        );
        if !flags.contains(FdFlag::FD_CLOEXEC) {
            eprintln!("File descriptor does not have close-on-exec flag: {flags:?}");
            result |= 2;
        }
        unsafe {
            nix::libc::fclose(fp);
        }
    } else {
        eprintln!("The 'e' flag is rejected!");
        result |= 4;
    }

    let _ = unlink("conftest.e");

    if result == 0 {
        eprintln!("Test succeded!");
        exit(0);
    } else {
        eprintln!("Test failed: {result}");
        exit(1);
    }
}

fn do_fopen_supports_mode_x() -> ! {
    let mut result = 0;
    let filename = CString::new("conftest.x").unwrap();
    let mode_w = CString::new("w").unwrap();
    let mode_wx = CString::new("wx").unwrap();

    let _ = unlink("conftest.x");

    let fp_w = unsafe { nix::libc::fopen(filename.as_ptr(), mode_w.as_ptr()) };
    if fp_w.is_null() {
        eprintln!("Failed to create conftest.x: {}", Errno::last());
        result |= 1;
    } else {
        unsafe {
            nix::libc::fclose(fp_w);
        }

        let fp_wx = unsafe { nix::libc::fopen(filename.as_ptr(), mode_wx.as_ptr()) };
        if !fp_wx.is_null() {
            eprintln!("The 'x' flag is ignored");
            result |= 2;
            unsafe {
                nix::libc::fclose(fp_wx);
            }
        } else {
            let error = Errno::last();
            if error != Errno::EEXIST {
                eprintln!("The 'x' flag is rejected");
                result |= 4;
            }
        }
    }

    let _ = unlink("conftest.x");

    if result == 0 {
        eprintln!("Test succeded!");
        exit(0);
    } else {
        eprintln!("Test failed: {result}");
        exit(1);
    }
}

fn do_rmdir_trailing_slashdot() -> ! {
    // 1. Remove test.file and test.dir, ignoring errors
    let _ = fs::remove_file("test.file");
    let _ = fs::remove_dir_all("test.dir");
    let _ = fs::remove_dir_all("test.");

    // 2. Create test.file as a file, panic on errors
    fs::write("test.file", "").expect("Failed to create test.file");

    // 3. Create test directories, panic on errors
    fs::create_dir("test.dir").expect("Failed to create test.dir");
    fs::create_dir("test.").expect("Failed to create test.");

    // 4. Convert the rmdirs in the C test, fails should cause eprintln!() and context
    let mut result = 0;

    // Try to remove test.file/ (this should fail)
    let path = CString::new("test.file/").unwrap();
    if unsafe { nix::libc::rmdir(path.as_ptr()) } == 0 {
        eprintln!("Unexpected success removing non-directory test.file/");
        result |= 1;
    } else if Errno::last() != nix::errno::Errno::ENOTDIR {
        result |= 2;
        eprintln!("Failed to remove test.file/: {}", Errno::last());
    } else {
        eprintln!("Removing non-directory test.file/ failed with ENOTDIR.");
    }

    // Try to remove test.dir/./ (this should fail with EINVAL)
    let path = CString::new("test.dir/./").unwrap();
    if unsafe { nix::libc::rmdir(path.as_ptr()) } == 0 {
        eprintln!("Unexpected success rmdir'ing path with dot as final component");
        result |= 4;
    } else if Errno::last() != nix::errno::Errno::EINVAL {
        result |= 8;
        eprintln!("Failed to remove test.dir/./: {}", Errno::last());
    } else {
        eprintln!("rmdir'ing path with dot as final component failed with EINVAL.");
    }

    // Try to remove test., this should succeed
    let path = CString::new("test.").unwrap();
    if unsafe { nix::libc::rmdir(path.as_ptr()) } != 0 {
        result |= 16;
        eprintln!("Failed to remove test.: {}", Errno::last());
    } else {
        eprintln!("Removing directory test. succeded.");
    }

    // 5. Exit 0 on success, exit 1 if any fails
    if result == 0 {
        eprintln!("Test succeeded!");
        exit(0);
    } else {
        println!("Test failed with result: {}", result);
        exit(1);
    }
}

fn do_mkdirat_trailing_dot() -> ! {
    // 1. Remove conftest.dir, ignoring errors
    let _ = fs::remove_dir_all("conftest.dir");

    // 2. Convert the mkdirats in the C test, fails should cause eprintln!() and context
    let mut result = 0;

    // 3. Try to mkdirat "." (this should fail with EEXIST)
    let path = CString::new(".").unwrap();
    if unsafe { nix::libc::mkdirat(nix::libc::AT_FDCWD, path.as_ptr(), 0o700) } == 0 {
        eprintln!("Unexpected success mkdirat'ing dot");
        result |= 1;
    } else if Errno::last() != Errno::EEXIST {
        result |= 2;
        eprintln!("Failed to mkdirat .: {}", Errno::last());
    } else {
        eprintln!("mkdirat'ing . failed with EEXIST.");
    }

    // 4. Try to mkdirat ".." (this should fail with EEXIST)
    let path = CString::new("..").unwrap();
    if unsafe { nix::libc::mkdirat(nix::libc::AT_FDCWD, path.as_ptr(), 0o700) } == 0 {
        eprintln!("Unexpected success mkdirat'ing ..");
        result |= 4;
    } else if Errno::last() != Errno::EEXIST {
        result |= 8;
        eprintln!("Failed to mkdirat ..: {}", Errno::last());
    } else {
        eprintln!("mkdirat'ing .. failed with EEXIST.");
    }

    // 5. Try to mkdirat conftest.dir/./ (this should fail with ENOENT)
    let path = CString::new("conftest.dir/././././////").unwrap();
    if unsafe { nix::libc::mkdirat(nix::libc::AT_FDCWD, path.as_ptr(), 0o700) } == 0 {
        eprintln!("Unexpected success mkdirat'ing path with dot as final component");
        result |= 16;
    } else if Errno::last() != Errno::ENOENT {
        result |= 32;
        eprintln!(
            "Failed to mkdirat conftest.dir/././././////: {}",
            Errno::last()
        );
    } else {
        eprintln!("mkdirat'ing path with dot as final component failed with ENOENT.");
    }

    // 6. Exit 0 on success, exit 1 if any fails
    if result == 0 {
        eprintln!("Test succeeded!");
        exit(0);
    } else {
        println!("Test failed with result: {}", result);
        exit(1);
    }
}

fn do_mkdir_trailing_dot() -> ! {
    // 1. Remove conftest.dir, ignoring errors
    let _ = fs::remove_dir_all("conftest.dir");

    // 2. Convert the mkdirs in the C test, fails should cause eprintln!() and context
    let mut result = 0;

    // 3. Try to mkdir "." (this should fail with EEXIST)
    let path = CString::new(".").unwrap();
    if unsafe { nix::libc::mkdir(path.as_ptr(), 0o700) } == 0 {
        eprintln!("Unexpected success mkdir'ing dot");
        result |= 1;
    } else if Errno::last() != Errno::EEXIST {
        result |= 2;
        eprintln!("Failed to mkdir .: {}", Errno::last());
    } else {
        eprintln!("mkdir'ing . failed with EEXIST.");
    }

    // 4. Try to mkdir ".." (this should fail with EEXIST)
    let path = CString::new("..").unwrap();
    if unsafe { nix::libc::mkdir(path.as_ptr(), 0o700) } == 0 {
        eprintln!("Unexpected success mkdir'ing ..");
        result |= 4;
    } else if Errno::last() != Errno::EEXIST {
        result |= 8;
        eprintln!("Failed to mkdir ..: {}", Errno::last());
    } else {
        eprintln!("mkdir'ing .. failed with EEXIST.");
    }

    // 5. Try to mkdir conftest.dir/./ (this should fail with ENOENT)
    let path = CString::new("conftest.dir/././././////").unwrap();
    if unsafe { nix::libc::mkdir(path.as_ptr(), 0o700) } == 0 {
        eprintln!("Unexpected success mkdir'ing path with dot as final component");
        result |= 16;
    } else if Errno::last() != Errno::ENOENT {
        result |= 32;
        eprintln!(
            "Failed to mkdir conftest.dir/././././////: {}",
            Errno::last()
        );
    } else {
        eprintln!("mkdir'ing path with dot as final component failed with ENOENT.");
    }

    // 6. Exit 0 on success, exit 1 if any fails
    if result == 0 {
        eprintln!("Test succeeded!");
        exit(0);
    } else {
        println!("Test failed with result: {}", result);
        exit(1);
    }
}

fn do_fstatat_trailing_slash() -> ! {
    let _ = fs::remove_file("conftest.file");
    let _ = fs::remove_file("conftest.sym");

    // Create a symlink conftest.sym pointing to conftest.file
    if let Err(error) = symlink("conftest.file", "conftest.sym") {
        eprintln!("Failed to create symlink: {error}");
        exit(1);
    }

    let mut result = 0;
    // Try to newfstatat conftest.sym/ and expect it to fail with ENOENT
    match fstatat(
        nix::libc::AT_FDCWD,
        "conftest.sym/",
        AtFlags::AT_SYMLINK_NOFOLLOW,
    ) {
        Ok(stat) => {
            eprintln!(
                "Failed: fstatat with trailing slash on dangling symlink succeeded: {stat:?}"
            );
            result |= 1;
        }
        Err(Errno::ENOENT) => {
            eprintln!(
                "Success: newfstatat with trailing slash on dangling symlink returned ENOENT."
            );
        }
        Err(error) => {
            eprintln!("Failed: newfstatat with trailing slash on dangling symlink failed with error: {error}");
            result |= 2;
        }
    }

    fs::File::create("conftest.file").expect("failed to create test file");
    // Try to newfstatat conftest.sym/ and expect it to fail with ENOTDIR
    match fstatat(
        nix::libc::AT_FDCWD,
        "conftest.sym/",
        AtFlags::AT_SYMLINK_NOFOLLOW,
    ) {
        Ok(stat) => {
            eprintln!(
                "Failed: newfstatat with trailing slash on symlink to non directory succeeded: {stat:?}"
            );
            result |= 4;
        }
        Err(Errno::ENOTDIR) => {
            eprintln!("Success: newfstatat with trailing slash on symlink to non directory returned ENOTDIR.");
        }
        Err(error) => {
            eprintln!("Failed: newfstatat with trailing slash on symlink to non directory failed with error: {error}");
            result |= 8;
        }
    }

    if result == 0 {
        eprintln!("Test succeded!");
        exit(0);
    } else {
        eprintln!("Test failed: {result}");
        exit(1);
    }
}

fn do_lstat_trailing_slash() -> ! {
    let _ = fs::remove_file("conftest.file");
    let _ = fs::remove_file("conftest.sym");

    // Create a symlink conftest.sym pointing to conftest.file
    if let Err(error) = symlink("conftest.file", "conftest.sym") {
        eprintln!("Failed to create symlink: {error}");
        exit(1);
    }

    let mut result = 0;
    // Try to lstat conftest.sym/ and expect it to fail with ENOENT.
    // Note this may call newfstatat on arches such as aarch64.
    match lstat("conftest.sym/") {
        Ok(stat) => {
            eprintln!("Failed: lstat with trailing slash succeeded: {stat:?}");
            result |= 1;
        }
        Err(Errno::ENOENT) => {
            eprintln!("Success: lstat with trailing slash returned ENOENT.");
        }
        Err(error) => {
            eprintln!("Failed: lstat with trailing slash failed with error: {error}");
            result |= 2;
        }
    }

    fs::File::create("conftest.file").expect("failed to create test file");
    // Try to lstat conftest.sym/ and expect it to fail with ENOTDIR.
    match lstat("conftest.sym/") {
        Ok(stat) => {
            eprintln!("Failed: lstat with trailing slash succeeded: {stat:?}");
            result |= 4;
        }
        Err(Errno::ENOTDIR) => {
            eprintln!("Success: lstat with trailing slash returned ENOTDIR.");
        }
        Err(error) => {
            eprintln!("Failed: lstat with trailing slash failed with error: {error}");
            result |= 8;
        }
    }

    if result == 0 {
        eprintln!("Test succeded!");
        exit(0);
    } else {
        eprintln!("Test failed: {result}");
        exit(1);
    }
}

fn do_openat_trailing_slash() -> ! {
    let mut result = 0;

    // Ensure files are cleaned up first
    let _ = unlink("conftest.tmp");
    let _ = unlink("conftest.lnk");
    let _ = unlink("conftest.sl");

    // Create files and symlinks required
    fs::File::create("conftest.tmp").expect("Failed to create conftest.tmp");
    if let Err(error) = symlink("conftest.tmp", "conftest.lnk") {
        eprintln!("Failed to create symlink: {error}");
        result |= 1;
    }

    // Test openat() with trailing slash on symlink
    match openat(
        nix::libc::AT_FDCWD,
        "conftest.lnk/",
        OFlag::O_RDONLY,
        Mode::empty(),
    ) {
        Ok(fd) => {
            eprintln!("openat should not succeed on symlink with trailing slash");
            result |= 2;
            let _ = close(fd);
        }
        Err(Errno::ENOTDIR) => {
            // Expected: openat should fail with ENOTDIR
            eprintln!("openat with trailing slash on symlink failed with ENOTDIR.");
        }
        Err(error) => {
            eprintln!("Unexpected error: {error}");
            result |= 4;
        }
    }

    // Test openat() with trailing slash and O_CREAT
    match openat(
        nix::libc::AT_FDCWD,
        "conftest.sl/",
        OFlag::O_CREAT,
        Mode::from_bits_truncate(0o600),
    ) {
        Ok(fd) => {
            eprintln!("openat should not succeed with trailing slash and O_CREAT");
            result |= 8;
            let _ = close(fd);
        }
        Err(Errno::EISDIR) => {
            // Expected: openat should fail with EISDIR
            eprintln!("openat with trailing slash and O_CREAT failed with EISDIR.");
        }
        Err(error) => {
            eprintln!("Unexpected error: {error}");
            result |= 16;
        }
    }

    // Clean up
    let _ = unlink("conftest.sl");
    let _ = unlink("conftest.tmp");
    let _ = unlink("conftest.lnk");

    if result == 0 {
        eprintln!("Test succeded!");
        exit(0);
    } else {
        eprintln!("Test failed: {result}");
        exit(1);
    }
}

fn do_open_trailing_slash() -> ! {
    let mut result = 0;

    // Ensure files are cleaned up first
    let _ = unlink("conftest.tmp");
    let _ = unlink("conftest.lnk");
    let _ = unlink("conftest.sl");

    // Create files and symlinks required
    fs::File::create("conftest.tmp").expect("Failed to create conftest.tmp");
    if let Err(error) = symlink("conftest.tmp", "conftest.lnk") {
        eprintln!("Failed to create symlink: {error}");
        result |= 1;
    }

    // Test open() with trailing slash on symlink
    match open("conftest.lnk/", OFlag::O_RDONLY, Mode::empty()) {
        Ok(fd) => {
            eprintln!("open should not succeed on symlink with trailing slash");
            result |= 2;
            let _ = close(fd);
        }
        Err(Errno::ENOTDIR) => {
            // Expected: open should fail with ENOTDIR
            eprintln!("open with trailing slash on symlink failed with ENOTDIR.");
        }
        Err(error) => {
            eprintln!("Unexpected error: {error}");
            result |= 4;
        }
    }

    // Test open() with trailing slash and O_CREAT
    match open(
        "conftest.sl/",
        OFlag::O_CREAT,
        Mode::from_bits_truncate(0o600),
    ) {
        Ok(fd) => {
            eprintln!("open should not succeed with trailing slash and O_CREAT");
            result |= 8;
            let _ = close(fd);
        }
        Err(Errno::EISDIR) => {
            // Expected: open should fail with EISDIR
            eprintln!("open with trailing slash and O_CREAT failed with EISDIR.");
        }
        Err(error) => {
            eprintln!("Unexpected error: {error}");
            result |= 16;
        }
    }

    // Clean up
    let _ = unlink("conftest.sl");
    let _ = unlink("conftest.tmp");
    let _ = unlink("conftest.lnk");

    if result == 0 {
        eprintln!("Test succeded!");
        exit(0);
    } else {
        eprintln!("Test failed: {result}");
        exit(1);
    }
}

fn do_emulate_open_fifo_1() -> ! {
    fn test_open_fifo() {
        // Remove fifo file if exists, create fifo
        let fifo_path = "syd_test_fifo";
        let _ = fs::remove_file(fifo_path); // Remove file if exists, ignore errors
        if let Err(error) = mkfifo(fifo_path, Mode::empty()) {
            eprintln!("FIFO creation failed: {error}");
            exit(1);
        }

        match unsafe { fork() } {
            Ok(ForkResult::Parent { child }) => {
                // Check if opening fifo for read fails with EPERM
                match retry_open(fifo_path, OFlag::O_RDONLY, Mode::empty()) {
                    Err(Errno::EACCES | Errno::EPERM) => {
                        // Change mode to allow reading
                        let _ = std::fs::set_permissions(
                            fifo_path,
                            std::fs::Permissions::from_mode(0o600),
                        );
                    }
                    Err(error) => {
                        eprintln!("Parent failed to open inaccessible FIFO for reading: {error}");
                        exit(1);
                    }
                    Ok(fd) => {
                        let _ = close(fd);
                        eprintln!("Parent succeeded to open inaccessible FIFO for reading!");
                        exit(1);
                    }
                };

                // Parent process: open fifo for reading
                let fd = match retry_open(fifo_path, OFlag::O_RDONLY, Mode::empty()) {
                    Ok(fd) => fd,
                    Err(error) => {
                        eprintln!("Parent failed to open FIFO for reading: {error}");
                        let _ = fs::remove_file(fifo_path);
                        exit(1);
                    }
                };

                loop {
                    let mut buf = [0; 1024];
                    match read(fd, &mut buf) {
                        Ok(n) => {
                            let message = String::from_utf8_lossy(&buf[..n]);
                            assert_eq!(
                                message,
                                "Heavy is the root of light. Still is the master of moving."
                            );
                            let _ = close(fd);
                            let _ = fs::remove_file(fifo_path);
                            let _ = kill(child, SIGKILL);
                            let _ = waitpid(Some(child), None);
                            // eprintln!("Input/output using a FIFO worked fine.");
                            return;
                        }
                        Err(Errno::EAGAIN) => {
                            sleep(Duration::from_millis(100));
                            continue;
                        }
                        Err(error) => {
                            eprintln!("Failed to read from FIFO: {error}");
                            let _ = close(fd);
                            let _ = fs::remove_file(fifo_path);
                            exit(1);
                        }
                    }
                }
            }
            Ok(ForkResult::Child) => {
                // Child process: open fifo for writing
                let fd = loop {
                    match retry_open(fifo_path, OFlag::O_WRONLY, Mode::empty()) {
                        Ok(fd) => break fd,
                        Err(Errno::EACCES | Errno::EPERM) => {
                            // eprintln!("Child failed to open FIFO for writing: {}", Errno::EACCES);
                            // Wait for the parent to update permissions on the FIFO...
                        }
                        Err(error) => {
                            eprintln!("Child failed to open FIFO for writing: {error}");
                            unsafe { nix::libc::_exit(1) };
                        }
                    }
                };
                loop {
                    match write(
                        fd,
                        b"Heavy is the root of light. Still is the master of moving.",
                    ) {
                        Ok(_) => {
                            unsafe { nix::libc::_exit(0) };
                        }
                        Err(Errno::EAGAIN) => {
                            sleep(Duration::from_millis(100));
                            continue;
                        }
                        Err(error) => {
                            eprintln!("Failed to write to FIFO: {error}");
                            unsafe { nix::libc::_exit(1) };
                        }
                    };
                }
            }
            Err(error) => {
                eprintln!("Fork failed: {error}");
                exit(1);
            }
        }
    }

    // Run multiple times to increase chance of failure.
    const TEST_DURATION: Duration = Duration::from_secs(60 * 15 - 7);
    let back = "\x08".repeat(128);
    let epoch = Instant::now();
    let mut i = 0;
    loop {
        test_open_fifo();

        i += 1;
        let elapsed = epoch.elapsed();
        if elapsed >= TEST_DURATION {
            eprintln!("Timeout reached. Finalizing test.");
            break;
        } else if i % 10 == 0 {
            eprint!(
                "{}{} attempts in {} seconds, {} seconds left...\t",
                back,
                i,
                elapsed.as_secs(),
                TEST_DURATION.as_secs().saturating_sub(elapsed.as_secs())
            );
        }
    }

    eprintln!("Input/output using a FIFO works fine.");
    exit(0);
}

fn do_emulate_open_fifo_2() -> ! {
    fn test_fifo_ipc() {
        let status = Command::new("sh")
            .arg("-c")
            .arg(
                r#"
set -e
rm -f in out
mkfifo in out
(while read -r line; do echo "$line"; done <in >out &)
exec 9>in
exec 8<out
trap 'exec 9>&-' EXIT
trap 'exec 8>&-' EXIT
echo >&9 one
read response <&8
test x"$response" = xone
echo >&9 two
read response <&8
test x"$response" = xtwo
true
"#,
            )
            .status()
            .expect("execute sh");

        if status.code().unwrap_or(127) != 0 {
            eprintln!("Input/output to FIFO failed: {status:?}");
            exit(1);
        }
    }

    // Run multiple times to increase chance of failure.
    const TEST_DURATION: Duration = Duration::from_secs(60 * 15 - 7);
    let back = "\x08".repeat(128);
    let epoch = Instant::now();
    let mut i = 0;
    loop {
        test_fifo_ipc();

        i += 1;
        let elapsed = epoch.elapsed();
        if elapsed >= TEST_DURATION {
            eprintln!("Timeout reached. Finalizing test.");
            break;
        } else if i % 10 == 0 {
            eprint!(
                "{}{} attempts in {} seconds, {} seconds left...\t",
                back,
                i,
                elapsed.as_secs(),
                TEST_DURATION.as_secs().saturating_sub(elapsed.as_secs())
            );
        }
    }

    eprintln!("Input/output using a FIFO works fine.");
    exit(0);
}

fn do_deny_magiclinks() -> ! {
    let paths = [
        "/proc/1/fd/0",
        "/proc/1/fd/1",
        "/proc/1/fd/2",
        "/proc/1/task/1/fd/0",
        "/proc/1/task/1/fd/1",
        "/proc/1/task/1/fd/2",
        "/proc/1/cwd",
        "/proc/1/exe",
        "/proc/1/root",
        "/proc/1/task/1/cwd",
        "/proc/1/task/1/exe",
        "/proc/1/task/1/root",
    ];

    for path in &paths {
        match File::open(path) {
            Ok(_) => {
                eprintln!("Unexpected success in opening {path}, expected ELOOP error!");
                exit(1);
            }
            Err(e) if e.raw_os_error().unwrap_or(nix::libc::EINVAL) == nix::libc::ELOOP => {
                eprintln!("Opening {path} returned ELOOP as expected.");
            }
            Err(e) => {
                eprintln!("Error opening {path}: {e}");
                exit(1);
            }
        }
    }

    // If all paths produce the expected ELOOP error, exit with 0
    exit(0);
}

fn do_path_resolution() -> ! {
    // Get the current directory's name
    let current_dir = env::current_dir().unwrap();

    // 0. Define the array of test cases
    #[allow(clippy::type_complexity)]
    let test_cases: [(&str, Option<&str>); 15] = [
        // absolute paths, relative paths, and chdir combinations
        ("./test_file.txt", None),
        ("test_file.txt", None),
        ("././test_file.txt", None),
        ("../test_file.txt", Some("./sub_dir")),
        ("../../test_file.txt", Some("./sub_dir/nested_sub_dir")),
        ("sub_dir/../test_file.txt", None),
        ("./sub_dir/../test_file.txt", None),
        ("../../test_file.txt", Some("./sub_dir/nested_sub_dir")),
        ("./../../test_file.txt", Some("./sub_dir/nested_sub_dir")),
        (".//./././//test_file.txt", None),
        ("./////test_file.txt", None),
        ("sub_dir/./../test_file.txt", None),
        ("sub_dir//nested_sub_dir/../..//test_file.txt", None),
        ("./sub_dir/./../test_file.txt", None),
        ("sub_dir/./.././test_file.txt", None),
    ];

    // 1. Create the test file in the current directory
    if let Err(error) = fs::create_dir_all(current_dir.join("./sub_dir/nested_sub_dir")) {
        eprintln!("Failed to create nested directories: {error}");
        exit(1);
    }
    let mut file = match fs::File::create(current_dir.join("test_file.txt")) {
        Ok(f) => f,
        Err(error) => {
            eprintln!("Failed to create test file: {error}");
            exit(1);
        }
    };
    if let Err(error) =
        file.write_all(b"Heavy is the root of light. Still is the master of moving.")
    {
        eprintln!("Failed to write to test file: {error}");
        exit(1);
    }

    let mut fail_count = 0;

    // 2. Probe all test cases one by one
    for (path, chdir_opt) in &test_cases {
        if let Some(chdir) = chdir_opt {
            if let Err(error) = env::set_current_dir(&current_dir.join(chdir)) {
                eprintln!("Failed to change directory to {chdir}: {error}");
                exit(1);
            }
        }

        if fs::File::open(path).is_err() {
            eprintln!("Failed to open: {path}, after changing dir to: {chdir_opt:?}");
            fail_count += 1;
        }

        // Reset directory after each test
        if let Err(error) = env::set_current_dir(&current_dir) {
            eprintln!("Failed to reset current directory: {error}");
            exit(1);
        }
    }

    // 3. Exit with the number of test cases failed count
    if fail_count > 0 {
        eprintln!("path_resolution: {fail_count} test cases failed.");
        exit(fail_count);
    } else {
        println!("path_resolution: All test cases passed.");
        exit(0);
    }
}

fn do_utimensat_null() -> ! {
    match unsafe { nix::libc::syscall(nix::libc::SYS_utimensat, 0, 0, 0, 0) } {
        -1 => {
            eprintln!(
                "utimensat with NULL arguments failed with error: {}",
                Errno::last()
            );
            exit(1);
        }
        _ => {
            eprintln!("utimensat with NULL arguments succeeded!");
            exit(0);
        }
    }
}

fn do_open_null_path() -> ! {
    // Attempt to open with NULL argument
    let fd = unsafe { nix::libc::open(std::ptr::null(), nix::libc::O_RDONLY) };

    if fd == -1 {
        let error = Errno::last();
        if error == Errno::EFAULT {
            eprintln!("Failed to open the file with error EFAULT.");
            exit(0);
        } else {
            eprintln!("Failed to open the file with unexpected error: {error}");
            exit(1);
        }
    } else {
        match fs::read_link(format!("/proc/self/fd/{fd}")) {
            Ok(link_path) => {
                eprintln!("Unexpectedly opened a file, it points to: {link_path:?}");
                let _ = close(fd);
                exit(1);
            }
            Err(error) => {
                eprintln!("Error reading the symbolic link: {error}");
                let _ = close(fd);
                exit(1);
            }
        }
    }
}

fn do_open_toolong_path() -> ! {
    // Constructing a path longer than PATH_MAX
    let long_name = PathBuf::from("x".repeat(nix::libc::PATH_MAX as usize + 7));
    let result = open(&long_name, OFlag::O_WRONLY | OFlag::O_CREAT, Mode::empty());

    match result {
        Ok(_) => {
            eprintln!("Successfully opened the file with a path longer than PATH_MAX.");
            exit(1);
        }
        Err(Errno::ENAMETOOLONG) => {
            eprintln!("Failed to open the file with error ENAMETOOLONG.");
            exit(0);
        }
        Err(error) => {
            eprintln!("Failed to open the file with unexpected error: {error}");
            exit(1);
        }
    };
}

fn do_kill_during_syscall() -> ! {
    // Create a pipe for parent-child communication
    let (pipe_r, pipe_w) = match pipe() {
        Ok((r, w)) => (r, w),
        Err(error) => {
            eprintln!("Failed to create pipe: {error}");
            exit(1);
        }
    };

    // Fork
    match unsafe { fork() } {
        Err(error) => {
            eprintln!("Failed to fork: {error}");
            exit(1);
        }
        Ok(ForkResult::Child) => {
            // This is the child process
            let _ = close(pipe_w);

            // Wait for the parent's go-ahead
            let mut buf = [0u8; 1];
            if unsafe { nix::libc::read(pipe_r, buf.as_mut_ptr() as *mut nix::libc::c_void, 1) }
                <= 0
            {
                eprintln!("Failed to read from pipe: {}", Errno::last());
                unsafe { nix::libc::_exit(1) };
            }

            loop {
                let _ = fs::File::open("/dev/null");
                // The file will be closed automatically when it goes out of scope
            }
        }
        Ok(ForkResult::Parent { child }) => {
            // This is the parent process
            let _ = close(pipe_r);

            // Notify the child to start the loop
            let buf = [1u8; 1];
            if unsafe { nix::libc::write(pipe_w, buf.as_ptr() as *const nix::libc::c_void, 1) } <= 0
            {
                eprintln!("Failed to write to pipe: {}", Errno::last());
                exit(1);
            }

            // Get a random duration between 1 to 10 seconds using getrandom
            let mut random_duration = [0u8; 1];
            if unsafe {
                nix::libc::getrandom(random_duration.as_mut_ptr() as *mut nix::libc::c_void, 1, 0)
            } == -1
            {
                eprintln!(
                    "Failed to get random bytes using getrandom: {}",
                    Errno::last()
                );
                exit(1);
            }
            let wait_seconds = 1 + (random_duration[0] % 10) as u64;
            sleep(Duration::from_secs(wait_seconds));

            // Kill the child and wait a bit.
            unsafe { nix::libc::kill(child.as_raw(), nix::libc::SIGKILL) };
            sleep(Duration::from_secs(wait_seconds));

            // If we caused the sandbox poll thread to exit,
            // then the following open call must block forever.
            match fs::File::open("/dev/null") {
                Ok(_) => {
                    eprintln!("Successfully opened file after killing child.");
                    exit(0);
                }
                Err(error) => {
                    eprintln!("Unexpected error opening /dev/null: {error}");
                    exit(1);
                }
            };
        }
    };
}

fn do_block_dev_random() -> ! {
    const BUF_SIZE: usize = 2; // arbitrary size for the test
    let mut buf = [0u8; BUF_SIZE];

    // Step 1: Access /dev/random
    match unsafe {
        nix::libc::getrandom(
            buf.as_mut_ptr() as *mut nix::libc::c_void,
            BUF_SIZE,
            nix::libc::GRND_RANDOM,
        )
    } {
        -1 if Errno::last() == Errno::EACCES => {
            eprintln!("Sandbox denied access to /dev/random.");
        }
        -1 => {
            eprintln!("Unexpected error accessing /dev/random: {}", Errno::last());
            exit(1);
        }
        _ => {
            eprintln!("Sandbox allowed access to /dev/random!");
            exit(1);
        }
    };

    // Step 2: Access /dev/random in non-blocking mode.
    match unsafe {
        nix::libc::getrandom(
            buf.as_mut_ptr() as *mut nix::libc::c_void,
            BUF_SIZE,
            nix::libc::GRND_RANDOM | nix::libc::GRND_NONBLOCK,
        )
    } {
        -1 if Errno::last() == Errno::EACCES => {
            eprintln!("Sandbox denied access to /dev/random in non-blocking mode.");
        }
        -1 => {
            eprintln!(
                "Unexpected error accessing /dev/random in non-blocking mode: {}",
                Errno::last()
            );
            exit(1);
        }
        _ => {
            eprintln!("Sandbox allowed access to /dev/random in non-blocking mode!");
            exit(1);
        }
    };

    // Step 3: Access /dev/urandom
    match unsafe { nix::libc::getrandom(buf.as_mut_ptr() as *mut nix::libc::c_void, BUF_SIZE, 0) } {
        -1 if Errno::last() == Errno::EACCES => {
            eprintln!("Sandbox denied access to /dev/urandom!");
            exit(1);
        }
        -1 => {
            eprintln!("Error accessing /dev/urandom: {}", Errno::last());
        }
        _ => {
            eprintln!("Sandbox allowed access to /dev/urandom.");
        }
    };

    // Step 4: Access /dev/urandom
    match unsafe {
        nix::libc::getrandom(
            buf.as_mut_ptr() as *mut nix::libc::c_void,
            BUF_SIZE,
            nix::libc::GRND_NONBLOCK,
        )
    } {
        -1 if Errno::last() == Errno::EACCES => {
            eprintln!("Sandbox denied access to /dev/urandom in non-blocking mode!");
            exit(1);
        }
        -1 => {
            eprintln!(
                "Error accessing /dev/urandom in non-blocking mode: {}",
                Errno::last()
            );
            exit(0);
        }
        _ => {
            eprintln!("Sandbox allowed access to /dev/urandom in non-blocking mode.");
            exit(0);
        }
    };
}

fn do_block_dev_urandom() -> ! {
    const BUF_SIZE: usize = 2; // arbitrary size for the test
    let mut buf = [0u8; BUF_SIZE];

    // Step 1: Access /dev/urandom
    match unsafe { nix::libc::getrandom(buf.as_mut_ptr() as *mut nix::libc::c_void, BUF_SIZE, 0) } {
        -1 if Errno::last() == Errno::EACCES => {
            eprintln!("Sandbox denied access to /dev/urandom.");
        }
        -1 => {
            eprintln!("Unexpected error accessing /dev/urandom: {}", Errno::last());
            exit(1);
        }
        _ => {
            eprintln!("Sandbox allowed access to /dev/urandom!");
            exit(1);
        }
    };

    // Step 2: Access /dev/urandom in non-blocking mode.
    match unsafe {
        nix::libc::getrandom(
            buf.as_mut_ptr() as *mut nix::libc::c_void,
            BUF_SIZE,
            nix::libc::GRND_NONBLOCK,
        )
    } {
        -1 if Errno::last() == Errno::EACCES => {
            eprintln!("Sandbox denied access to /dev/urandom in non-blocking mode.");
        }
        -1 => {
            eprintln!(
                "Unexpected error accessing /dev/urandom in non-blocking mode: {}",
                Errno::last()
            );
            exit(1);
        }
        _ => {
            eprintln!("Sandbox allowed access to /dev/urandom in non-blocking mode!");
            exit(1);
        }
    };

    // Step 3: Access /dev/random
    match unsafe {
        nix::libc::getrandom(
            buf.as_mut_ptr() as *mut nix::libc::c_void,
            BUF_SIZE,
            nix::libc::GRND_RANDOM,
        )
    } {
        -1 if Errno::last() == Errno::EACCES => {
            eprintln!("Sandbox denied access to /dev/random!");
            exit(1);
        }
        -1 => {
            eprintln!("Error accessing /dev/random: {}", Errno::last());
        }
        _ => {
            eprintln!("Sandbox allowed access to /dev/random.");
        }
    };

    // Step 4: Access /dev/random in non-blocking-mode.
    match unsafe {
        nix::libc::getrandom(
            buf.as_mut_ptr() as *mut nix::libc::c_void,
            BUF_SIZE,
            nix::libc::GRND_RANDOM | nix::libc::GRND_NONBLOCK,
        )
    } {
        -1 if Errno::last() == Errno::EACCES => {
            eprintln!("Sandbox denied access to /dev/random in non-blocking mode!");
            exit(1);
        }
        -1 => {
            eprintln!(
                "Error accessing /dev/random in non-blocking mode: {}",
                Errno::last()
            );
            exit(0);
        }
        _ => {
            eprintln!("Sandbox allowed access to /dev/random in non-blocking mode.");
            exit(0);
        }
    };
}

fn do_block_prctl_ptrace() -> ! {
    // Try to set the calling process as its own tracer.
    unsafe {
        nix::libc::prctl(
            nix::libc::PR_SET_PTRACER,
            Pid::this().as_raw() as u64,
            0,
            0,
            0,
        )
    };
    match Errno::last() {
        Errno::EACCES => {
            eprintln!("Successfully blocked by sandbox.");
            exit(0);
        }
        Errno::UnknownErrno => {
            eprintln!("Sandbox escape possible, prctl did not block the action.");
            exit(1);
        }
        errno => {
            eprintln!("Unexpected error: {errno}");
            exit(1);
        }
    }
}

fn do_block_ioctl_tiocsti() -> ! {
    let c = 'x';
    unsafe {
        nix::libc::ioctl(
            nix::libc::STDIN_FILENO,
            nix::libc::TIOCSTI,
            &c as *const _ as *const _,
        )
    };
    match Errno::last() {
        Errno::EACCES => {
            eprintln!("syd blocked write access to the controlling terminal.");
            exit(0);
        }
        Errno::UnknownErrno => {
            eprintln!("syd allowed write access to the controlling terminal.");
            exit(1);
        }
        errno => {
            eprintln!(
                "syd did not block write access to the controlling terminal properly: {errno}"
            );
            exit(1);
        }
    }
}

fn do_fstat_on_temp_file() -> ! {
    let fd = match openat(
        nix::libc::AT_FDCWD,
        "",
        OFlag::O_WRONLY | OFlag::O_TMPFILE,
        Mode::from_bits_truncate(0o600),
    ) {
        Ok(fd) => fd,
        Err(error) => {
            eprintln!("Error creating file with O_TMPFILE: {error}");
            if error == Errno::EOPNOTSUPP {
                eprintln!("Filesystem does not support O_TMPFILE, skipping!");
                exit(0);
            } else {
                exit(1);
            }
        }
    };

    match fstat(fd) {
        Err(error) => {
            eprintln!("Failed to fstat the temporary file fd: {error}");
            exit(1);
        }
        Ok(stat) => {
            // Check if this is a regular file.
            if stat.st_mode & SFlag::S_IFMT.bits() != SFlag::S_IFREG.bits() {
                eprintln!("The stat did not return a regular file.");
                exit(1);
            }
        }
    }

    exit(0);
}

fn do_fstat_on_deleted_file() -> ! {
    let fd = match open(
        "test-deleted",
        OFlag::O_WRONLY | OFlag::O_CREAT,
        Mode::S_IRUSR | Mode::S_IWUSR,
    ) {
        Ok(fd) => fd,
        Err(error) => {
            eprintln!("Error creating test file: {error}");
            exit(1);
        }
    };

    let stat_orig = match fstat(fd) {
        Ok(stat) => stat,
        Err(error) => {
            eprintln!("Failed to stat file: {error}");
            exit(1);
        }
    };

    if let Err(error) = unlink("test-deleted") {
        eprintln!("Failed to remove test file: {error}");
        exit(1);
    }

    match fstat(fd) {
        Err(error) => {
            eprintln!("Failed to fstat the deleted file fd: {error}");
            exit(1);
        }
        Ok(stat) => {
            if stat.st_dev != stat_orig.st_dev || stat.st_ino != stat_orig.st_ino {
                eprintln!("fstat returned incorrect result");
                exit(1);
            }
        }
    }

    exit(0);
}

fn do_fstat_on_socket() -> ! {
    let fd = match socket(
        AddressFamily::Unix,
        SockType::Stream,
        SockFlag::empty(),
        None,
    ) {
        Ok(fd) => fd,
        Err(error) => {
            eprintln!("Failed to create socket: {error}");
            exit(1);
        }
    };

    match fstat(fd.as_raw_fd()) {
        Ok(stat) => {
            eprintln!("fstat on socket fd: {stat:?}");
            exit(0);
        }
        Err(error) => {
            eprintln!("Failed to fstat the socket fd: {error}");
            exit(1);
        }
    }
}

fn do_fstat_on_pipe() -> ! {
    let (pipe_r, pipe_w) = match pipe() {
        Ok((r, w)) => (r, w),
        Err(error) => {
            eprintln!("Failed to create pipe: {error}");
            exit(1);
        }
    };

    let mut r = 0;
    if let Err(error) = fstat(pipe_r) {
        eprintln!("Failed to fstat the read end of the pipe: {error}");
        r += 1;
    }
    if let Err(error) = fstat(pipe_w) {
        eprintln!("Failed to fstat the write end of the pipe: {error}");
        r += 1;
    }
    exit(r);
}

fn do_fchmodat_on_proc_fd() -> ! {
    // Step 0: Delete the file if it exists.
    let _ = fs::remove_file("fchmodat-test");

    // Step 1: Create a file
    let fd = match open(
        "fchmodat-test",
        OFlag::O_WRONLY | OFlag::O_CREAT,
        Mode::S_IRUSR | Mode::S_IWUSR,
    ) {
        Ok(fd) => fd,
        Err(error) => {
            eprintln!("Error creating test file: {error}");
            exit(1);
        }
    };

    // Step 2 & 3: Call fchmodat and assert new mode
    let new_mode = Mode::S_IRUSR;
    let fd_path = PathBuf::from(format!("/proc/self/fd/{fd}"));
    if let Err(error) = fchmodat(None, &fd_path, new_mode, FchmodatFlags::FollowSymlink) {
        eprintln!("Failed to change mode of file: {error}");
        exit(1);
    }

    // Step 4: Stat the file and check the mode
    let file_stat = match stat("fchmodat-test") {
        Ok(stat) => stat,
        Err(error) => {
            eprintln!("Failed to stat file: {error}");
            exit(1);
        }
    };

    let actual_mode = file_stat.st_mode & 0o777;
    if actual_mode != new_mode.bits() {
        eprintln!(
            "File mode did not change as expected: {} != {}",
            actual_mode,
            new_mode.bits()
        );
        exit(1);
    }

    eprintln!("Test succeded!");
    exit(0);
}

fn do_linkat_on_fd() -> ! {
    // Step 0: Delete file if it exists
    let _ = fs::remove_file("linkat-file");

    // Step 1: Create file "linkat-file"
    let file = match File::create("linkat-file") {
        Ok(file) => file,
        Err(error) => {
            eprintln!("Error creating 'linkat-file': {error}");
            exit(1);
        }
    };

    // Step 2: Open "linkat-file"
    let fd = file.as_raw_fd();

    // Step 3: Use linkat to link
    let old_path = "";
    let new_path = "linkat-link";

    if old_path
        .with_nix_path(|oldcstr| {
            new_path.with_nix_path(|newcstr| unsafe {
                nix::libc::linkat(
                    fd,
                    oldcstr.as_ptr(),
                    nix::libc::AT_FDCWD,
                    newcstr.as_ptr(),
                    nix::libc::AT_EMPTY_PATH,
                )
            })
        })
        .unwrap()
        .unwrap()
        != 0
    {
        let errno = Errno::last();
        if errno == Errno::ENOENT {
            eprintln!("linkat returned ENOENT");
            eprintln!("Missing CAP_DAC_READ_SEARCH?");
            eprintln!("Skipping test!");
            exit(0);
        } else {
            eprintln!("Failed to create link using linkat: {}", Errno::last());
            exit(1);
        }
    }

    // Step 4: Check if "new-file" exists and is the same inode
    let stat_original = match stat("linkat-file") {
        Ok(stat) => stat,
        Err(error) => {
            eprintln!("Failed to stat 'linkat-file': {error}");
            exit(1);
        }
    };

    let stat_new = match stat("linkat-link") {
        Ok(stat) => stat,
        Err(error) => {
            eprintln!("Failed to stat 'linkat-link': {error}");
            exit(1);
        }
    };

    if stat_original.st_ino != stat_new.st_ino {
        eprintln!(
            "Inode numbers do not match: {} != {}",
            stat_original.st_ino, stat_new.st_ino
        );
        exit(1);
    }

    eprintln!("Test succeeded!");
    exit(0);
}

fn do_exec_in_inaccessible_directory() -> ! {
    // Create a directory
    let dir_name = "inaccessible_dir";
    if let Err(error) = mkdir(dir_name, Mode::S_IRWXU) {
        eprintln!("Error creating directory: {error}.");
        exit(1);
    }

    // Change into the directory
    if let Err(error) = chdir(dir_name) {
        eprintln!("Error changing into directory: {error}.");
        exit(1);
    }

    // Make the directory inaccessible
    if let Err(error) = fchmodat(None, ".", Mode::empty(), FchmodatFlags::FollowSymlink) {
        eprintln!("Error changing permissions: {error}");
        exit(1);
    }

    // Try to execute "bash -c true"
    let output = Command::new("bash").args(["-c", "true"]).output();

    match output {
        Ok(output) => {
            if !output.status.success() {
                eprintln!("Failed to execute \"bash -c true\"");
                eprintln!("Stdout: {}", String::from_utf8_lossy(&output.stdout));
                eprintln!("Stderr: {}", String::from_utf8_lossy(&output.stderr));
                exit(1);
            }
        }
        Err(error) => {
            eprintln!("Error executing \"bash -c true\": {error}.");
            exit(1);
        }
    }

    exit(0);
}

fn do_open_utf8_invalid() -> ! {
    // "test-" followed by invalid UTF-8 bytes
    let invalid_name = OsString::from_vec(
        b"test-"
            .iter()
            .copied()
            .chain(vec![0xFF, 0xFF, 0xFF])
            .collect(),
    );
    let invalid_path = OsStr::new(&invalid_name);

    let fd = match open(
        invalid_path,
        OFlag::O_WRONLY | OFlag::O_CREAT,
        Mode::S_IRUSR | Mode::S_IWUSR,
    ) {
        Ok(fd) => fd,
        Err(error) => {
            eprintln!("Failed to create a file with invalid UTF-8 path: {error}.");
            exit(1);
        }
    };

    if let Err(error) = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.",
    ) {
        eprintln!("Failed to write to a file with invalid UTF-8 path: {error}.");
        exit(1);
    }

    if let Err(error) = unlink(invalid_path) {
        eprintln!("Failed to remove a file with invalid UTF-8 path: {error}.");
        exit(1);
    }

    eprintln!("File with invalid UTF-8 path was successfully created and written to.");
    exit(0);
}

fn do_honor_umask() -> ! {
    // Parsing the first argument as an octal mode
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        eprintln!("Expected exactly one argument for expected file mode");
        exit(1);
    }

    let mode_exp: u32 = u32::from_str_radix(&args[1], 8).expect("Failed to parse mode as octal");

    // Removing the file if it exists and creating a new one
    let path = "test";
    let _ = fs::remove_file(path);
    fs::File::create(path).expect("Failed to create file");

    // Checking the file's mode
    let metadata = fs::metadata(path).expect("Failed to get metadata");
    let permissions = metadata.permissions();
    let mode = permissions.mode() & 0o777; // Masking to get the last 3 octal digits

    if mode == mode_exp {
        exit(0);
    } else {
        eprintln!("Mode {mode:o} != {mode_exp:o}");
        exit(1);
    }
}

fn do_emulate_otmpfile() -> ! {
    match open(
        "",
        OFlag::O_WRONLY | OFlag::O_TMPFILE,
        Mode::from_bits_truncate(0o600),
    ) {
        Ok(_) => exit(0),
        Err(error) => {
            eprintln!("Failed to open file with O_TMPFILE flag: {error}");
            if error == Errno::EOPNOTSUPP {
                eprintln!("Filesystem does not support O_TMPFILE, skipping!");
                exit(0);
            } else {
                exit(1);
            }
        }
    }
}

fn do_emulate_opath() -> ! {
    let path = "emulate";

    // Make sure the file does not exist.
    let _ = fs::remove_file(path);

    let fd = open(
        ".",
        OFlag::O_RDONLY | OFlag::O_DIRECTORY | OFlag::O_PATH,
        Mode::from_bits_truncate(0o600),
    )
    .expect("failed to open current directory");
    let file = openat(
        fd,
        "emulate",
        OFlag::O_WRONLY | OFlag::O_CREAT,
        Mode::from_bits_truncate(0o600),
    )
    .expect("failed to open file with O_PATH fd");

    let _ = close(fd);
    let _ = close(file);

    exit(0);
}

fn do_umask_bypass_277() -> ! {
    let path = "umask";
    let prev_umask = umask(Mode::from_bits_truncate(0o277));

    // Make sure the file doesn't exist
    let _ = fs::remove_file(path);

    // Create a file with 0777 permissions
    let fd = open(
        path,
        OFlag::O_CREAT | OFlag::O_WRONLY,
        Mode::from_bits_truncate(0o777),
    )
    .expect("Failed to create test file");

    // Reset umask to its previous value
    let _ = umask(prev_umask);

    // Close the file descriptor
    let _ = close(fd);

    // Check the file's permissions
    let metadata = fs::metadata(path).expect("Failed to retrieve test file metadata");
    let permissions = metadata.permissions().mode() & 0o777;

    // Clean up the test file
    let _ = fs::remove_file(path);

    // Verify that the umask was applied correctly
    if permissions == (0o777 & !0o277) {
        eprintln!("Umask was applied correctly.");
        exit(0);
    } else {
        eprintln!(
            "Umask was not applied correctly. Expected: {:o}, Found: {:o}",
            0o777 & !0o277,
            permissions
        );
        std::process::exit(1);
    }
}

fn do_umask_bypass_077() -> ! {
    let path = "umask";
    let prev_umask = umask(Mode::from_bits_truncate(0o077));

    // Make sure the file doesn't exist
    let _ = fs::remove_file(path);

    // Create a file with 0777 permissions
    let fd = open(
        path,
        OFlag::O_CREAT | OFlag::O_WRONLY,
        Mode::from_bits_truncate(0o777),
    )
    .expect("Failed to create test file");

    // Reset umask to its previous value
    let _ = umask(prev_umask);

    // Close the file descriptor
    let _ = close(fd);

    // Check the file's permissions
    let metadata = fs::metadata(path).expect("Failed to retrieve test file metadata");
    let permissions = metadata.permissions().mode() & 0o777;

    // Clean up the test file
    let _ = fs::remove_file(path);

    // Verify that the umask was applied correctly
    if permissions == (0o777 & !0o077) {
        eprintln!("Umask was applied correctly.");
        exit(0);
    } else {
        eprintln!(
            "Umask was not applied correctly. Expected: {:o}, Found: {:o}",
            0o777 & !0o077,
            permissions
        );
        std::process::exit(1);
    }
}

fn do_devfd_escape_chdir() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_1() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/./{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_2() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("./fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_3() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("./fd/././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_4() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/../fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_5() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("./././fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_6() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("foo/../fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_7() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/foo/..//{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_8() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/foo/.././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_9() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/foo/bar/../../{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_10() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("././fd/foo/../././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_11() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/./././foo/../{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_12() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/bar/./../{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_13() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("foo/bar/../../fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_14() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("foo/./bar/../../fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_15() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("././foo/../fd/././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_16() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/././foo/bar/../.././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_17() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/foo/./bar/../../{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_18() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("./fd/./bar/.././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_19() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/.././fd/./{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_chdir_relpath_20() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // chdir into /dev
    if let Err(error) = chdir("/dev") {
        eprintln!("Failed to change directory to /dev: {error}.");
        exit(1);
    }

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/./././././././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd = openat(nix::libc::AT_FDCWD, fd_path, OFlag::O_RDONLY, Mode::empty())
        .unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_procself_escape_chdir() -> ! {
    // Change directory into /proc
    if let Err(error) = chdir("/proc") {
        eprintln!("Failed to change directory to /proc: {error}.");
        exit(1);
    }

    // Open /proc/self/status with the above file descriptor
    let fd = openat(
        nix::libc::AT_FDCWD,
        "self/status",
        OFlag::O_RDONLY,
        Mode::empty(),
    )
    .expect("Failed to open /proc/self/status using openat");

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by changing directory to /proc.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_chdir_relpath_1() -> ! {
    // Change directory into /proc
    if let Err(error) = chdir("/proc") {
        eprintln!("Failed to change directory to /proc: {error}.");
        exit(1);
    }

    // Open /proc/self/status with the above file descriptor
    let fd = openat(
        nix::libc::AT_FDCWD,
        "self/./status",
        OFlag::O_RDONLY,
        Mode::empty(),
    )
    .expect("Failed to open /proc/self/status using openat");

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by changing directory to /proc.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_chdir_relpath_2() -> ! {
    // Change directory into /proc
    if let Err(error) = chdir("/proc") {
        eprintln!("Failed to change directory to /proc: {error}.");
        exit(1);
    }

    // Open /proc/self/status with the above file descriptor
    let fd = openat(
        nix::libc::AT_FDCWD,
        "./self/status",
        OFlag::O_RDONLY,
        Mode::empty(),
    )
    .expect("Failed to open /proc/self/status using openat");

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by changing directory to /proc.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_chdir_relpath_3() -> ! {
    // Change directory into /proc
    if let Err(error) = chdir("/proc") {
        eprintln!("Failed to change directory to /proc: {error}.");
        exit(1);
    }

    // Open /proc/self/status with the above file descriptor
    let fd = openat(
        nix::libc::AT_FDCWD,
        "./self/././status",
        OFlag::O_RDONLY,
        Mode::empty(),
    )
    .expect("Failed to open /proc/self/status using openat");

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by changing directory to /proc.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_chdir_relpath_4() -> ! {
    // Change directory into /proc
    if let Err(error) = chdir("/proc") {
        eprintln!("Failed to change directory to /proc: {error}.");
        exit(1);
    }

    // Open /proc/self/status with the above file descriptor
    let fd = openat(
        nix::libc::AT_FDCWD,
        "self/../self/status",
        OFlag::O_RDONLY,
        Mode::empty(),
    )
    .expect("Failed to open /proc/self/status using openat");

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by changing directory to /proc.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_chdir_relpath_5() -> ! {
    // Change directory into /proc
    if let Err(error) = chdir("/proc") {
        eprintln!("Failed to change directory to /proc: {error}.");
        exit(1);
    }

    // Open /proc/self/status with the above file descriptor
    let fd = openat(
        nix::libc::AT_FDCWD,
        "./././self/status",
        OFlag::O_RDONLY,
        Mode::empty(),
    )
    .expect("Failed to open /proc/self/status using openat");

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by changing directory to /proc.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_chdir_relpath_6() -> ! {
    // Change directory into /proc
    if let Err(error) = chdir("/proc") {
        eprintln!("Failed to change directory to /proc: {error}.");
        exit(1);
    }

    // Open /proc/self/status with the above file descriptor
    let fd = openat(
        nix::libc::AT_FDCWD,
        "self/.././self/./status",
        OFlag::O_RDONLY,
        Mode::empty(),
    )
    .expect("Failed to open /proc/self/status using openat");

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by changing directory to /proc.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_chdir_relpath_7() -> ! {
    // Change directory into /proc
    if let Err(error) = chdir("/proc") {
        eprintln!("Failed to change directory to /proc: {error}.");
        exit(1);
    }

    // Open /proc/self/status with the above file descriptor
    let fd = openat(
        nix::libc::AT_FDCWD,
        "self/./././././././status",
        OFlag::O_RDONLY,
        Mode::empty(),
    )
    .expect("Failed to open /proc/self/status using openat");

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by changing directory to /proc.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_devfd_escape_open() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_1() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/./{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_2() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("./fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_3() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("./fd/././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_4() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/../fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_5() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("./././fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_6() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("foo/../fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_7() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/foo/..//{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_8() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/foo/.././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_9() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/foo/bar/../../{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_10() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("././fd/foo/../././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_11() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/./././foo/../{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_12() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/bar/./../{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_13() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("foo/bar/../../fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_14() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("foo/./bar/../../fd/{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_15() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("././foo/../fd/././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_16() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/././foo/bar/../.././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_17() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/foo/./bar/../../{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_18() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("./fd/./bar/.././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_19() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/.././fd/./{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_devfd_escape_open_relpath_20() -> ! {
    // Create and open a temporary test file with O_RDWR
    let fd = open("test", OFlag::O_RDWR | OFlag::O_CREAT, Mode::S_IRWXU).unwrap_or_else(|error| {
        eprintln!("Failed to create and open the test file: {error}.");
        exit(1);
    });

    // Write some data to the file
    let _ = write(
        fd,
        b"Heavy is the root of light. Still is the master of moving.\n",
    );

    // Stat the test file to get its attributes
    let test_stat = fstat(fd).unwrap_or_else(|error| {
        eprintln!("Failed to stat the test file: {error}.");
        exit(1);
    });

    // Open /dev with O_PATH
    let dev_fd = open("/dev", OFlag::O_PATH, Mode::empty()).unwrap_or_else(|error| {
        eprintln!("Failed to open /dev with O_PATH: {error}.");
        exit(1);
    });

    // Construct the path to /dev/fd/{fd}
    let fd_path = format!("fd/./././././././{fd}");
    let fd_path = Path::new(&fd_path);

    // Stat the file via /dev/fd/{fd}
    let linked_fd =
        openat(dev_fd, fd_path, OFlag::O_RDONLY, Mode::empty()).unwrap_or_else(|error| {
            if error == Errno::ENOENT {
                eprintln!("Skipping test: /dev/fd does not exist!");
                exit(0);
            } else {
                eprintln!("Escaped sandbox due to failed access to file via /dev/fd/: {error}.");
                exit(1);
            }
        });

    let dev_fd_stat = fstat(linked_fd).unwrap_or_else(|error| {
        eprintln!("Escaped sandbox due to failed stat on the file via /dev/fd/: {error}.");
        exit(1);
    });

    // Check if the attributes match
    if dev_fd_stat.st_dev == test_stat.st_dev && dev_fd_stat.st_ino == test_stat.st_ino {
        eprintln!("Failed to escape sandbox, we accessed the identical file.");
        exit(0);
    } else {
        eprintln!("Escaped sandbox: File accessed via /dev/fd/ is different than the test file.");
        exit(1);
    }
}

fn do_procself_escape_open() -> ! {
    // Open /proc with O_PATH
    let proc_fd = open("/proc", OFlag::O_PATH, Mode::empty()).expect("Failed to open /proc");

    // Open /proc/self/status with the above file descriptor
    let fd = openat(proc_fd, "self/status", OFlag::O_RDONLY, Mode::empty())
        .expect("Failed to open /proc/self/status using openat");

    // Close the /proc fd
    let _ = close(proc_fd);

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by opening /proc as O_DIRECTORY.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_open_relpath_1() -> ! {
    // Open /proc with O_PATH
    let proc_fd = open("/proc", OFlag::O_PATH, Mode::empty()).expect("Failed to open /proc");

    // Open /proc/self/status with the above file descriptor
    let fd = openat(proc_fd, "self/./status", OFlag::O_RDONLY, Mode::empty())
        .expect("Failed to open /proc/self/status using openat");

    // Close the /proc fd
    let _ = close(proc_fd);

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by opening /proc as O_DIRECTORY.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_open_relpath_2() -> ! {
    // Open /proc with O_PATH
    let proc_fd = open("/proc", OFlag::O_PATH, Mode::empty()).expect("Failed to open /proc");

    // Open /proc/self/status with the above file descriptor
    let fd = openat(proc_fd, "./self/status", OFlag::O_RDONLY, Mode::empty())
        .expect("Failed to open /proc/self/status using openat");

    // Close the /proc fd
    let _ = close(proc_fd);

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by opening /proc as O_DIRECTORY.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_open_relpath_3() -> ! {
    // Open /proc with O_PATH
    let proc_fd = open("/proc", OFlag::O_PATH, Mode::empty()).expect("Failed to open /proc");

    // Open /proc/self/status with the above file descriptor
    let fd = openat(proc_fd, "./self/././status", OFlag::O_RDONLY, Mode::empty())
        .expect("Failed to open /proc/self/status using openat");

    // Close the /proc fd
    let _ = close(proc_fd);

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by opening /proc as O_DIRECTORY.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_open_relpath_4() -> ! {
    // Open /proc with O_PATH
    let proc_fd = open("/proc", OFlag::O_PATH, Mode::empty()).expect("Failed to open /proc");

    // Open /proc/self/status with the above file descriptor
    let fd = openat(
        proc_fd,
        "self/../self/status",
        OFlag::O_RDONLY,
        Mode::empty(),
    )
    .expect("Failed to open /proc/self/status using openat");

    // Close the /proc fd
    let _ = close(proc_fd);

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by opening /proc as O_DIRECTORY.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_open_relpath_5() -> ! {
    // Open /proc with O_PATH
    let proc_fd = open("/proc", OFlag::O_PATH, Mode::empty()).expect("Failed to open /proc");

    // Open /proc/self/status with the above file descriptor
    let fd = openat(proc_fd, "./././self/status", OFlag::O_RDONLY, Mode::empty())
        .expect("Failed to open /proc/self/status using openat");

    // Close the /proc fd
    let _ = close(proc_fd);

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by opening /proc as O_DIRECTORY.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_open_relpath_6() -> ! {
    // Open /proc with O_PATH
    let proc_fd = open("/proc", OFlag::O_PATH, Mode::empty()).expect("Failed to open /proc");

    // Open /proc/self/status with the above file descriptor
    let fd = openat(
        proc_fd,
        "self/.././self/./status",
        OFlag::O_RDONLY,
        Mode::empty(),
    )
    .expect("Failed to open /proc/self/status using openat");

    // Close the /proc fd
    let _ = close(proc_fd);

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by opening /proc as O_DIRECTORY.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_open_relpath_7() -> ! {
    // Open /proc with O_PATH
    let proc_fd = open("/proc", OFlag::O_PATH, Mode::empty()).expect("Failed to open /proc");

    // Open /proc/self/status with the above file descriptor
    let fd = openat(
        proc_fd,
        "self/./././././././status",
        OFlag::O_RDONLY,
        Mode::empty(),
    )
    .expect("Failed to open /proc/self/status using openat");

    // Close the /proc fd
    let _ = close(proc_fd);

    // Read the file
    let mut buf = vec![0; 128];
    let n = read(fd, &mut buf).expect("Failed to read /proc/self/status");

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by opening /proc as O_DIRECTORY.");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_procself_escape_symlink() -> ! {
    let _ = fs::remove_file("self");

    // symlink /proc/./self to self
    if let Err(error) = symlinkat("/proc/./self", None, "self") {
        eprintln!("Failed to symlink /proc/./self: {error}");
        exit(1);
    }

    // Open /proc/self/status with the above file descriptor
    let fd = match open("self/status", OFlag::O_RDONLY, Mode::empty()) {
        Ok(fd) => fd,
        Err(error) => {
            eprintln!("Failed to open /proc/self/status using open: {error}");
            exit(0);
        }
    };

    // Read the file
    let mut buf = vec![0; 128];
    let n = match read(fd, &mut buf) {
        Ok(n) => n,
        Err(error) => {
            eprintln!("Failed to read /proc/self/status: {error}");
            exit(0);
        }
    };

    // Close the status fd
    let _ = close(fd);

    // Perform a lossy conversion to UTF-8
    let content = String::from_utf8_lossy(&buf[..n]);

    // Check for `syd'
    if content.contains("Name:\tsyd\n") || content.contains("Name:\tsydbox\n") {
        eprintln!("Escaped sandbox by symlinking /proc/./self");
        eprintln!("Snippet from /proc/sydbox/status which is not /proc/pid/status:");
        eprintln!("{content}");
        exit(1);
    } else {
        exit(0);
    }
}

fn do_opath_escape() -> ! {
    // Open with O_PATH
    let fd = match open("/etc/passwd", OFlag::O_PATH, Mode::empty()) {
        Ok(fd) => fd,
        Err(error) => {
            eprintln!("Error opening /etc/passwd: {error}");
            exit(0);
        }
    };

    // Convert to a real fd with AT_EMPTY_PATH
    let real_fd = match openat(fd, "", OFlag::O_RDONLY | OFlag::O_CLOEXEC, Mode::empty()) {
        Ok(real_fd) => {
            let _ = close(fd);
            real_fd
        }
        Err(error) => {
            let _ = close(fd);
            eprintln!("Error reopening O_PATH fd: {error}");
            exit(0);
        }
    };

    // Read from the fd
    let mut buf = [0u8; 1024];
    let n = match read(real_fd, &mut buf) {
        Ok(n) => {
            let _ = close(real_fd);
            n
        }
        Err(error) => {
            let _ = close(real_fd);
            eprintln!("Failed to read from /etc/passwd: {error}");
            exit(0);
        }
    };

    // Print the first line of /etc/passwd
    if let Some(line) = String::from_utf8_lossy(&buf[..n]).lines().next() {
        eprintln!("Escaped sandbox using a O_PATH file descriptor!");
        eprintln!("Snippet from /etc/passwd which is denylisted:");
        eprintln!("{line}");
        exit(1); // Report a failure
    } else {
        exit(0); // Report success if /etc/passwd is empty or unreadable
    }
}

#[cfg(not(feature = "uring"))]
fn do_io_uring_escape() -> ! {
    eprintln!("uring feature disabled, skipping test!");
    exit(0);
}

#[cfg(feature = "uring")]
fn do_io_uring_escape() -> ! {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("Pass 0 as first argument for normal operation.");
        eprintln!("Pass 1 as first argument to expect successful escape.");
        exit(1);
    }
    let (exit_succ, exit_fail) = match args[1].as_str() {
        "0" => (0, 1),
        "1" => (1, 0),
        _ => {
            eprintln!("Pass 0 as first argument for normal operation.");
            eprintln!("Pass 1 as first argument to expect successful escape.");
            exit(1);
        }
    };

    const FILE_PATH: &str = "/etc/passwd";
    const BUF_SIZE: usize = 1024;
    let path = std::ffi::CString::new(FILE_PATH).unwrap();

    let mut ring = match io_uring::IoUring::new(3) {
        Ok(ring) => ring,
        Err(error) => {
            if error.raw_os_error().unwrap_or(nix::libc::ENOSYS) == nix::libc::EACCES {
                eprintln!(
                    "Error initializing io_uring: {}, this is fine.",
                    Errno::EACCES
                );
                exit(exit_succ);
            } else {
                eprintln!("Error initializing io_uring: {error}, unexpected error!");
                exit(exit_fail);
            }
        }
    };

    // Open the file using io_uring
    let open_at_opcode =
        io_uring::opcode::OpenAt::new(io_uring::types::Fd(nix::libc::AT_FDCWD), path.as_ptr());
    let open_entry = open_at_opcode.build();
    {
        let mut submission_queue = ring.submission();
        // SAFETY: We ensure that the buffer and the queue
        // entry live longer than the push operation, and that
        // the pointer to the file path remains valid.
        unsafe {
            if let Err(error) = submission_queue.push(&open_entry) {
                eprintln!("Failed to push open entry to submission queue: {error}.");
                exit(exit_succ);
            }
        }
        // Synchronize the submission queue.
        submission_queue.sync();
    }

    if let Err(error) = ring.submit() {
        eprintln!("Failed to submit open request: {error}.");
        exit(exit_succ);
    }

    let epoch = Instant::now();
    let timeout = Duration::from_secs(5);
    let mut open_completion = None;

    while epoch.elapsed() < timeout {
        if let Some(completion) = ring.completion().next() {
            open_completion = Some(completion);
            break;
        }
        // Check every 100ms for open completion.
        sleep(Duration::from_millis(100));
    }

    let open_completion = match open_completion {
        Some(completion) => completion,
        None => {
            eprintln!("No completion event for open operation");
            exit(exit_succ);
        }
    };

    let fd = match open_completion.result() {
        n if n < 0 => {
            eprintln!("Failed to open /etc/passwd using io_uring");
            exit(exit_succ);
        }
        n => n as std::os::fd::RawFd,
    };

    // Read the file using io_uring
    let mut buf = vec![0; BUF_SIZE];
    let read_opcode =
        io_uring::opcode::Read::new(io_uring::types::Fd(fd), buf.as_mut_ptr(), buf.len() as _);
    let read_entry = read_opcode.build();
    {
        let mut submission_queue = ring.submission();
        // SAFETY: We ensure that the buffer for reading and
        // the queue entry live longer than the push
        // operation.
        unsafe {
            if let Err(error) = submission_queue.push(&read_entry) {
                eprintln!("Failed to push read entry to submission queue: {error}.");
                exit(exit_succ);
            }
        }
        // Synchronize the submission queue.
        submission_queue.sync();
    }

    if let Err(error) = ring.submit() {
        eprintln!("Failed to submit read request: {error}.");
        exit(exit_succ);
    }

    let epoch = Instant::now();
    let timeout = Duration::from_secs(5);
    let mut read_completion = None;

    while epoch.elapsed() < timeout {
        if let Some(completion) = ring.completion().next() {
            read_completion = Some(completion);
            break;
        }
        // Check every 100ms for read completion.
        sleep(Duration::from_millis(100));
    }

    let read_completion = match read_completion {
        Some(completion) => completion,
        None => {
            eprintln!("No completion event for read operation.");
            exit(exit_succ);
        }
    };

    if read_completion.result() < 0 {
        eprintln!("Failed to read /etc/passwd using io_uring.");
        exit(exit_succ);
    }

    // Convert buffer to string and print first line
    let contents = String::from_utf8_lossy(&buf);
    if let Some(passwd) = contents.lines().next() {
        eprintln!("Escaped sandbox using io-uring interface!");
        eprintln!("Snippet from /etc/passwd which is denylisted:");
        eprintln!("{passwd}");
        exit(exit_fail);
    }

    exit(exit_succ);
}

fn do_ptrmod_toctou_open() -> ! {
    // The function aims to exploit a
    // time-of-check-to-time-of-use (TOCTOU) race condition by
    // modifying a pointer that references a file path during
    // the system call.
    fn toctou_poc() -> bool {
        // STEP 1: SETUP
        // Create a benign file that we'll try to open.
        fs::write(
            "src.syd-tmp",
            "Heavy is the root of light. Still is the master of moving.",
        )
        .unwrap();

        // Prepare a null-terminated string pointing to the benign file.
        let benign_path = b"src.syd-tmp".to_vec();
        let mut benign_path_cstring = benign_path.clone();
        benign_path_cstring.push(0); // NUL-terminate to make it
                                     // compatible with C-style
                                     // string operations.

        // Obtain a mutable pointer to the string.
        // This pointer is what will be modified in our TOCTOU attack.
        let ptr = benign_path_cstring.as_mut_ptr();

        // Prepare a thread which, when run, will repeatedly attempt to modify the pointer
        // to point to a different file path (i.e., /etc/passwd).
        let mut thread: pthread_t = unsafe { std::mem::zeroed() };

        unsafe {
            // STEP 2: START TOCTOU RACE
            // Create the new thread to initiate the attack.
            // The thread will try to overwrite the pointer during the `open` system call.
            pthread_create(
                &mut thread,
                std::ptr::null(),
                modify_ptr,
                ptr as *mut nix::libc::c_void,
            );

            // STEP 3: ATTEMPT TO OPEN FILE
            // Attempt to open the benign file, but due to the race condition,
            // it may end up opening a different file (i.e., /etc/passwd).
            let fd = nix::libc::open(ptr as *const nix::libc::c_char, nix::libc::O_RDONLY);

            // If we failed to open the file, the attack was unsuccessful this round.
            if fd == -1 {
                return false;
            }

            // STEP 4: CHECK FOR SUCCESSFUL EXPLOITATION
            // If the attack was successful, we would've opened
            // /etc/passwd instead of the benign file.
            // Let's read the file contents and check.
            let mut file = fs::File::from_raw_fd(fd);
            let mut content = String::new();
            file.read_to_string(&mut content).unwrap();
            pthread_join(thread, std::ptr::null_mut());

            // Check if we successfully read /etc/passwd
            if content.contains("root:") {
                let passwd = content.lines().next().unwrap_or("");
                eprintln!("Escaped sandbox using a pointer modification attack!");
                eprintln!("Snippet from /etc/passwd which is denylisted:");
                eprintln!("{passwd}");
                return true;
            }
        }

        false
    }

    // Run multiple times to increase chance of failure.
    const TEST_DURATION: Duration = Duration::from_secs(60 * 3 - 7);
    let back = "\x08".repeat(128);
    let epoch = Instant::now();
    let mut i = 0;
    loop {
        if toctou_poc() {
            exit(1);
        }

        i += 1;
        let elapsed = epoch.elapsed();
        if elapsed >= TEST_DURATION {
            eprintln!("Timeout reached. Finalizing test.");
            break;
        } else if i % 10 == 0 {
            eprint!(
                "{}{} attempts in {} seconds, {} seconds left...\t",
                back,
                i,
                elapsed.as_secs(),
                TEST_DURATION.as_secs().saturating_sub(elapsed.as_secs())
            );
        }
    }

    eprintln!("Test succeded!");
    exit(0);
}

fn do_ptrmod_toctou_creat() -> ! {
    // The function aims to exploit a
    // time-of-check-to-time-of-use (TOCTOU) race condition by
    // modifying a pointer that references a file path during
    // the system call.
    fn toctou_poc() -> bool {
        // STEP 1: SETUP
        // Create a benign file that we'll try to open.
        fs::write(
            "src.syd-tmp",
            "Heavy is the root of light. Still is the master of moving.",
        )
        .unwrap();

        // Prepare a null-terminated string pointing to the benign file.
        let benign_path = b"src.syd-tmp".to_vec();
        let mut benign_path_cstring = benign_path.clone();
        benign_path_cstring.push(0); // NUL-terminate to make it
                                     // compatible with C-style
                                     // string operations.

        // Obtain a mutable pointer to the string.
        // This pointer is what will be modified in our TOCTOU attack.
        let ptr = benign_path_cstring.as_mut_ptr();

        // Prepare a thread which, when run, will repeatedly attempt to modify the pointer
        // to point to a different file path (i.e., /etc/passwd).
        let mut thread: pthread_t = unsafe { std::mem::zeroed() };

        unsafe {
            // STEP 2: START TOCTOU RACE
            // Create the new thread to initiate the attack.
            // The thread will try to overwrite the pointer during the `open` system call.
            pthread_create(
                &mut thread,
                std::ptr::null(),
                modify_ptr_creat,
                ptr as *mut nix::libc::c_void,
            );

            // STEP 3: ATTEMPT TO OPEN FILE
            // Attempt to open the benign file, but due to the race condition,
            // it may end up opening a different file (i.e., /etc/passwd).
            let fd = nix::libc::creat(ptr as *const nix::libc::c_char, 0o600);

            // If we failed to open the file, the attack was unsuccessful this round.
            if fd == -1 {
                return false;
            }

            pthread_join(thread, std::ptr::null_mut());
            let _ = nix::libc::close(fd);

            // STEP 4: CHECK FOR SUCCESSFUL EXPLOITATION
            // If the attack was successful, we would've
            // created `deny.syd-tmp' instead of the
            // the benign file. Let's stat the file and
            // check:
            if Path::new("./deny.syd-tmp").exists() {
                eprintln!("Escaped sandbox using a pointer modification attack!");
                eprintln!("Denylisted file `deny.syd-tmp' has been successfully created.");
                return true;
            }
        }

        false
    }

    // Run multiple times to increase chance of failure.
    const TEST_DURATION: Duration = Duration::from_secs(60 * 3 - 7);
    let back = "\x08".repeat(128);
    let epoch = Instant::now();
    let mut i = 0;
    loop {
        if toctou_poc() {
            exit(1);
        }

        i += 1;
        let elapsed = epoch.elapsed();
        if elapsed >= TEST_DURATION {
            eprintln!("Timeout reached. Finalizing test.");
            break;
        } else if i % 10 == 0 {
            eprint!(
                "{}{} attempts in {} seconds, {} seconds left...\t",
                back,
                i,
                elapsed.as_secs(),
                TEST_DURATION.as_secs().saturating_sub(elapsed.as_secs())
            );
        }
    }

    eprintln!("Test succeded!");
    exit(0);
}

fn do_ptrmod_toctou_opath() -> ! {
    // The function aims to exploit a
    // time-of-check-to-time-of-use (TOCTOU) race condition by
    // modifying a pointer that references a file path during
    // the system call.
    fn toctou_poc() -> bool {
        // STEP 1: SETUP
        // Create a benign file that we'll try to open.
        fs::write(
            "src.syd-tmp",
            "Heavy is the root of light. Still is the master of moving.",
        )
        .unwrap();

        // Prepare a null-terminated string pointing to the benign file.
        let benign_path = b"src.syd-tmp".to_vec();
        let mut benign_path_cstring = benign_path.clone();
        benign_path_cstring.push(0); // NUL-terminate to make it
                                     // compatible with C-style
                                     // string operations.

        // Obtain a mutable pointer to the string.
        // This pointer is what will be modified in our TOCTOU attack.
        let ptr = benign_path_cstring.as_mut_ptr();

        // Prepare a thread which, when run, will repeatedly attempt to modify the pointer
        // to point to a different file path (i.e., /etc/passwd).
        let mut thread: pthread_t = unsafe { std::mem::zeroed() };

        unsafe {
            // STEP 2: START TOCTOU RACE
            // Create the new thread to initiate the attack.
            // The thread will try to overwrite the pointer during the `open` system call.
            pthread_create(
                &mut thread,
                std::ptr::null(),
                modify_ptr,
                ptr as *mut nix::libc::c_void,
            );

            // STEP 3: ATTEMPT TO OPEN FILE
            // Attempt to open the benign file, but due to the race condition,
            // it may end up opening a different file (i.e., /etc/passwd).
            let fd = nix::libc::open(ptr as *const nix::libc::c_char, nix::libc::O_PATH);

            // If we failed to open the file, the attack was unsuccessful this round.
            if fd == -1 {
                return false;
            }

            // STEP 4: CHECK FOR SUCCESSFUL EXPLOITATION
            // If the attack was successful, we would've opened
            // /etc/passwd instead of the benign file.
            // Let's read the proc symlink to check.
            match syd::fs::read_link(format!("/proc/self/fd/{fd}")) {
                Ok(path) if path == PathBuf::from("/etc/passwd") => {
                    eprintln!("Leaked hidden path in sandbox using a pointer modification attack!");
                    eprintln!("Success opening /etc/passwd with O_PATH which is hidden:");
                    Command::new("sh")
                        .arg("-xc")
                        .arg(format!(
                            "readlink /proc/self/fd/{fd}; ls -la /etc /proc/self/fd"
                        ))
                        .stderr(Stdio::inherit())
                        .stdin(Stdio::inherit())
                        .stdout(Stdio::inherit())
                        .spawn()
                        .expect("exec ls")
                        .wait()
                        .expect("wait ls");
                    eprintln!();
                    return true;
                }
                _ => {
                    // If we failed to read the symbolic link,
                    // or it does not point to /etc/passwd,
                    // the attack was unsuccessful this round.
                    //
                    // Fall through.
                }
            }

            // Join the attacker thread.
            pthread_join(thread, std::ptr::null_mut());
        }

        false
    }

    // Run multiple times to increase chance of failure.
    const TEST_DURATION: Duration = Duration::from_secs(60 * 3 - 7);
    let back = "\x08".repeat(128);
    let epoch = Instant::now();
    let mut i = 0;
    loop {
        if toctou_poc() {
            exit(1);
        }

        i += 1;
        let elapsed = epoch.elapsed();
        if elapsed >= TEST_DURATION {
            eprintln!("Timeout reached. Finalizing test.");
            break;
        } else if i % 10 == 0 {
            eprint!(
                "{}{} attempts in {} seconds, {} seconds left...\t",
                back,
                i,
                elapsed.as_secs(),
                TEST_DURATION.as_secs().saturating_sub(elapsed.as_secs())
            );
        }
    }

    eprintln!("Test succeded!");
    exit(0);
}

fn do_symlinkat_toctou() -> ! {
    // Cleanup
    let _ = fs::remove_file("src.syd-tmp");
    let _ = fs::remove_file("dst.syd-tmp");

    // Create a benign file
    fs::write(
        "src.syd-tmp",
        "Heavy is the root of light. Still is the master of moving.",
    )
    .unwrap();

    // Create a symlink that initially points to the benign file
    symlinkat("src.syd-tmp", None, "dst.syd-tmp").unwrap();

    // Define the main PoC as an inner function
    fn toctou_poc() -> bool {
        // Spawn a thread to repeatedly try and change the symlink to point to /etc/passwd
        thread::spawn(|| {
            loop {
                let _ = fs::remove_file("dst.syd-tmp");
                let _ = symlinkat("/etc/passwd", None, "dst.syd-tmp");
                // Adjust this sleep for fine-tuning the race condition
                sleep(Duration::from_micros(10));
            }
        });

        for _ in 0..100 {
            // Try to open the 'dst.syd-tmp' symlink, expecting it to be 'src.syd-tmp'
            let content = fs::read_to_string("dst.syd-tmp").unwrap_or_else(|_| String::from(""));
            if content.contains("root:") {
                let passwd = content.lines().next().unwrap_or("");
                eprintln!("Escaped sandbox using a symlink attack with the symlinkat syscall!");
                eprintln!("Snippet from /etc/passwd which is denylisted:");
                eprintln!("{passwd}");
                return true;
            }
        }
        false
    }

    // Run multiple times to increase chance of failure.
    const TEST_DURATION: Duration = Duration::from_secs(60 * 3 - 7);
    let back = "\x08".repeat(128);
    let epoch = Instant::now();
    let mut i = 0;
    loop {
        if toctou_poc() {
            exit(1);
        }

        i += 1;
        let elapsed = epoch.elapsed();
        if elapsed >= TEST_DURATION {
            eprintln!("Timeout reached. Finalizing test.");
            break;
        } else if i % 10 == 0 {
            eprint!(
                "{}{} attempts in {} seconds, {} seconds left...\t",
                back,
                i,
                elapsed.as_secs(),
                TEST_DURATION.as_secs().saturating_sub(elapsed.as_secs())
            );
        }
    }

    eprintln!("Test succeded!");
    exit(0);
}

fn do_symlink_toctou() -> ! {
    // Cleanup
    let _ = fs::remove_file("src.syd-tmp");
    let _ = fs::remove_file("dst.syd-tmp");

    // Create a benign file
    fs::write(
        "src.syd-tmp",
        "Heavy is the root of light. Still is the master of moving.",
    )
    .unwrap();

    // Create a symlink that initially points to the benign file
    symlink("src.syd-tmp", "dst.syd-tmp").unwrap();

    // Define the main PoC as an inner function
    fn toctou_poc() -> bool {
        // Spawn a thread to repeatedly try and change the symlink to point to /etc/passwd
        thread::spawn(|| {
            loop {
                let _ = fs::remove_file("dst.syd-tmp");
                let _ = symlink("/etc/passwd", "dst.syd-tmp");
                // Adjust this sleep for fine-tuning the race condition
                sleep(Duration::from_micros(10));
            }
        });

        for _ in 0..100 {
            // Try to open the 'dst.syd-tmp' symlink, expecting it to be 'src.syd-tmp'
            let content = fs::read_to_string("dst.syd-tmp").unwrap_or_else(|_| String::from(""));
            if content.contains("root:") {
                let passwd = content.lines().next().unwrap_or("");
                eprintln!("Escaped sandbox using a symlink attack!");
                eprintln!("Snippet from /etc/passwd which is denylisted:");
                eprintln!("{passwd}");
                return true;
            }
        }
        false
    }

    // Run multiple times to increase chance of failure.
    const TEST_DURATION: Duration = Duration::from_secs(60 * 3 - 7);
    let back = "\x08".repeat(128);
    let epoch = Instant::now();
    let mut i = 0;
    loop {
        if toctou_poc() {
            exit(1);
        }

        i += 1;
        let elapsed = epoch.elapsed();
        if elapsed >= TEST_DURATION {
            eprintln!("Timeout reached. Finalizing test.");
            break;
        } else if i % 10 == 0 {
            eprint!(
                "{}{} attempts in {} seconds, {} seconds left...\t",
                back,
                i,
                elapsed.as_secs(),
                TEST_DURATION.as_secs().saturating_sub(elapsed.as_secs())
            );
        }
    }

    eprintln!("Test succeded!");
    exit(0);
}

fn do_stat_bypass_with_exec() -> ! {
    // Try to access /bin/sh with X_OK and check for ENOENT
    match access("/bin/sh", AccessFlags::X_OK) {
        Ok(_) => {
            eprintln!("Unexpected success in accessing /bin/sh");
            exit(1);
        }
        Err(Errno::ENOENT) => eprintln!("ENOENT error as expected for /bin/sh"),
        Err(error) => {
            eprintln!("Unexpected error when accessing /bin/sh {error}");
            exit(1);
        }
    }

    // Try to access /bin/SH with X_OK and check for ENOENT
    match access("/bin/SH", AccessFlags::X_OK) {
        Ok(_) => {
            eprintln!("Unexpected success in accessing /bin/SH");
            exit(1);
        }
        Err(Errno::ENOENT) => eprintln!("ENOENT error as expected for /bin/SH"),
        Err(error) => {
            eprintln!("Unexpected error when accessing /bin/SH {error}");
            exit(1);
        }
    }

    // Exit normally if all checks pass
    exit(0);
}

fn do_stat_bypass_with_write() -> ! {
    // Try to write to /etc/passwd and check for ENOENT
    match open("/etc/passwd", OFlag::O_WRONLY, Mode::empty()) {
        Ok(_) => {
            eprintln!("Unexpected success in opening /etc/passwd");
            exit(1);
        }
        Err(Errno::ENOENT) => eprintln!("ENOENT error as expected for /etc/passwd"),
        Err(error) => {
            eprintln!("Unexpected error when opening /etc/passwd: {error}");
            exit(1);
        }
    }

    // Try to read from /etc/password and check for ENOENT
    match open("/etc/password", OFlag::O_WRONLY, Mode::empty()) {
        Ok(_) => {
            eprintln!("Unexpected success in opening /etc/password");
            exit(1);
        }
        Err(Errno::ENOENT) => eprintln!("ENOENT error as expected for /etc/password"),
        Err(error) => {
            eprintln!("Unexpected error when opening /etc/password: {error}");
            exit(1);
        }
    }

    // Exit normally if all checks pass
    exit(0);
}

fn do_stat_bypass_with_read() -> ! {
    // Try to read from /etc/passwd and check for ENOENT
    match open("/etc/passwd", OFlag::O_RDONLY, Mode::empty()) {
        Ok(_) => {
            eprintln!("Unexpected success in opening /etc/passwd");
            exit(1);
        }
        Err(Errno::ENOENT) => eprintln!("ENOENT error as expected for /etc/passwd"),
        Err(error) => {
            eprintln!("Unexpected error when opening /etc/passwd: {error}");
            exit(1);
        }
    }

    // Try to read from /etc/password and check for ENOENT
    match open("/etc/password", OFlag::O_RDONLY, Mode::empty()) {
        Ok(_) => {
            eprintln!("Unexpected success in opening /etc/password");
            exit(1);
        }
        Err(Errno::ENOENT) => eprintln!("ENOENT error as expected for /etc/password"),
        Err(error) => {
            eprintln!("Unexpected error when opening /etc/password: {error}");
            exit(1);
        }
    }

    // Exit normally if all checks pass
    exit(0);
}

fn do_connect4_0() -> ! {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        panic!("Expected only an IPv4 address as argument.");
    }
    let addr: Ipv4Addr = args[1].parse().expect("Invalid IPv4 address");
    let (read_fd, write_fd) = pipe().expect("Failed to create pipe");

    match unsafe { fork() } {
        Ok(ForkResult::Parent { .. }) => {
            // Close the write end of the pipe in the parent
            close(write_fd).expect("Failed to close write_fd in parent");

            // Allow some time for the child process to start the listener
            sleep(Duration::from_secs(3));

            // Read port from the pipe
            let mut port_buf = [0; 2];
            nix::unistd::read(read_fd, &mut port_buf).expect("Failed to read from pipe");
            let port = u16::from_be_bytes(port_buf);

            let sock = SocketAddrV4::new(addr, port);

            // Attempt to connect to the address and exit with errno.
            exit(match TcpStream::connect(sock) {
                Ok(_) => {
                    // Successfully connected
                    0
                }
                Err(error) => {
                    eprintln!("Connect failed: {:?}", error);
                    error.raw_os_error().unwrap_or(-1)
                }
            });
        }
        Ok(ForkResult::Child) => {
            // Close the read end of the pipe in the child
            close(read_fd).expect("Failed to close read_fd in child");

            let sock = SocketAddrV4::new(addr, 0); // 0 means OS chooses the port
            let listener = TcpListener::bind(sock).expect("Failed to bind address");

            // Fetch the assigned port and write it to the pipe
            if let Ok(local_addr) = listener.local_addr() {
                let port_bytes = local_addr.port().to_be_bytes();
                write(write_fd, &port_bytes).expect("Failed to write to pipe");
            }

            listener
                .set_nonblocking(true)
                .expect("Failed to set socket to nonblocking mode.");

            let epoch = Instant::now();

            // Attempt to accept a connection and exit on the first established connection.
            loop {
                match listener.accept() {
                    Ok(_) => {
                        exit(0);
                    }
                    Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
                        if epoch.elapsed() > Duration::from_secs(10) {
                            eprintln!("Timed out waiting for a connection");
                            exit(Errno::ETIMEDOUT as i32);
                        }
                        sleep(Duration::from_millis(100));
                    }
                    Err(error) => {
                        eprintln!("Accept failed: {:?}", error);
                        exit(error.raw_os_error().unwrap_or(-1));
                    }
                };
            }
        }
        Err(error) => panic!("Fork failed: {:?}", error),
    };
}

fn do_connect6_0() -> ! {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        panic!("Expected only an IPv6 address as argument.");
    }
    let addr: Ipv6Addr = args[1].parse().expect("Invalid IPv6 address");
    let (read_fd, write_fd) = pipe().expect("Failed to create pipe");

    match unsafe { fork() } {
        Ok(ForkResult::Parent { .. }) => {
            // Close the write end of the pipe in the parent
            close(write_fd).expect("Failed to close write_fd in parent");

            // Allow some time for the child process to start the listener
            sleep(Duration::from_secs(3));

            // Read port from the pipe
            let mut port_buf = [0; 2];
            nix::unistd::read(read_fd, &mut port_buf).expect("Failed to read from pipe");
            let port = u16::from_be_bytes(port_buf);

            let sock = SocketAddrV6::new(addr, port, 0, 0);

            // Attempt to connect to the address and exit with errno.
            exit(match TcpStream::connect(sock) {
                Ok(_) => {
                    // Successfully connected
                    0
                }
                Err(error) => {
                    eprintln!("Connect failed: {:?}", error);
                    error.raw_os_error().unwrap_or(-1)
                }
            });
        }
        Ok(ForkResult::Child) => {
            // Close the read end of the pipe in the child
            close(read_fd).expect("Failed to close read_fd in child");

            // 0 in second argument means OS chooses the port.
            let sock = SocketAddrV6::new(addr, 0, 0, 0);
            let listener = TcpListener::bind(sock).expect("Failed to bind address");

            // Fetch the assigned port and write it to the pipe
            if let Ok(local_addr) = listener.local_addr() {
                let port_bytes = local_addr.port().to_be_bytes();
                write(write_fd, &port_bytes).expect("Failed to write to pipe");
            }

            listener
                .set_nonblocking(true)
                .expect("Failed to set socket to nonblocking mode.");

            let epoch = Instant::now();

            // Attempt to accept a connection and exit on the first established connection.
            loop {
                match listener.accept() {
                    Ok(_) => {
                        exit(0);
                    }
                    Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
                        if epoch.elapsed() > Duration::from_secs(10) {
                            eprintln!("Timed out waiting for a connection");
                            exit(Errno::ETIMEDOUT as i32);
                        }
                        sleep(Duration::from_millis(100));
                    }
                    Err(error) => {
                        eprintln!("Accept failed: {:?}", error);
                        exit(error.raw_os_error().unwrap_or(-1));
                    }
                };
            }
        }
        Err(error) => panic!("Fork failed: {:?}", error),
    };
}

fn do_connect4() -> ! {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        panic!("Expected an IPv4 address and port as arguments.");
    }
    let addr: Ipv4Addr = args[1].parse().expect("Invalid IPv4 address");
    let port: u16 = args[2].parse().expect("Invalid port number");
    let sock = SocketAddrV4::new(addr, port);

    match unsafe { fork() } {
        Ok(ForkResult::Parent { .. }) => {
            // Allow some time for the child process to start the listener
            sleep(Duration::from_secs(3));

            // Attempt to connect to the address and exit with errno.
            exit(match TcpStream::connect(sock) {
                Ok(_) => {
                    // Successfully connected
                    0
                }
                Err(error) => {
                    eprintln!("Connect failed: {error}");
                    error.raw_os_error().unwrap_or(-1)
                }
            });
        }
        Ok(ForkResult::Child) => {
            let listener = TcpListener::bind(sock).expect("Failed to bind address");
            listener
                .set_nonblocking(true)
                .expect("Failed to set socket to nonblocking mode.");

            let epoch = Instant::now();

            // Attempt to accept a connection and exit on the first established connection.
            loop {
                match listener.accept() {
                    Ok(_) => {
                        exit(0);
                    }
                    Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
                        if epoch.elapsed() > Duration::from_secs(10) {
                            eprintln!("Timed out waiting for a connection");
                            exit(Errno::ETIMEDOUT as i32);
                        }
                        sleep(Duration::from_millis(100));
                    }
                    Err(error) => {
                        eprintln!("Accept failed: {error}");
                        exit(error.raw_os_error().unwrap_or(-1));
                    }
                };
            }
        }
        Err(error) => panic!("Fork failed: {error}"),
    };
}

fn do_connect6() -> ! {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        panic!("Expected an IPv6 address and port as arguments.");
    }
    let addr: Ipv6Addr = args[1].parse().expect("Invalid IPv6 address");
    let port: u16 = args[2].parse().expect("Invalid port number");
    let sock = SocketAddrV6::new(addr, port, 0, 0);

    match unsafe { fork() } {
        Ok(ForkResult::Parent { .. }) => {
            // Allow some time for the child process to start the listener
            sleep(Duration::from_secs(3));

            // Attempt to connect to the address and exit with errno.
            exit(match TcpStream::connect(sock) {
                Ok(_) => {
                    // Successfully connected
                    0
                }
                Err(error) => {
                    eprintln!("Connect failed: {error}");
                    error.raw_os_error().unwrap_or(-1)
                }
            });
        }
        Ok(ForkResult::Child) => {
            let listener = TcpListener::bind(sock).expect("Failed to bind address");
            listener
                .set_nonblocking(true)
                .expect("Failed to set socket to nonblocking mode.");

            let epoch = Instant::now();

            // Attempt to accept a connection and exit on the first established connection.
            loop {
                match listener.accept() {
                    Ok(_) => {
                        exit(0);
                    }
                    Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
                        if epoch.elapsed() > Duration::from_secs(10) {
                            eprintln!("Timed out waiting for a connection");
                            exit(Errno::ETIMEDOUT as i32);
                        }
                        sleep(Duration::from_millis(100));
                    }
                    Err(error) => {
                        eprintln!("Accept failed: {error}");
                        exit(error.raw_os_error().unwrap_or(-1));
                    }
                };
            }
        }
        Err(error) => panic!("Fork failed: {error}"),
    };
}

fn do_fork() -> ! {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        panic!("Expected exit code and number of processes as arguments.");
    }
    eprintln!("do_fork: {args:?}");
    let xcode: i32 = args[1].parse().expect("Failed to parse the exit code.");
    if xcode < 0 || xcode > u8::MAX as i32 {
        panic!("Invalid exit code: {xcode}.");
    }
    let nproc: i32 = args[2]
        .parse()
        .expect("Failed to parse the number of processes.");
    if !(0..=4096).contains(&nproc) {
        panic!("Invalid number for number of processes.");
    }

    for i in 0..nproc {
        match unsafe { fork() } {
            Ok(ForkResult::Parent { .. }) => {
                // Avoid hitting the TTL.
                eprintln!("Iteration {i} of {nproc} done, sleeping for 1 second...");
                sleep(Duration::from_secs(1));
            }
            Ok(ForkResult::Child) => {
                sleep(Duration::from_secs(7));
                // SAFETY: In libc we trust.
                unsafe { _exit((i % 254) + 1) };
            }
            Err(errno) => {
                eprintln!("Failed to fork: {errno}.");
                exit(errno as i32);
            }
        }
    }

    exit(xcode);
}

fn do_thread() -> ! {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        panic!("Expected exit code and number of processes as arguments.");
    }
    let xcode: i32 = args[1].parse().expect("Failed to parse the exit code.");
    if xcode < 0 || xcode > u8::MAX as i32 {
        panic!("Invalid exit code: {xcode}.");
    }
    let nproc: i32 = args[2]
        .parse()
        .expect("Failed to parse the number of processes.");
    if !(0..=4096).contains(&nproc) {
        panic!("Invalid number for number of processes.");
    }

    for _ in 0..nproc {
        // We don't join the threads deliberately here.
        let _ = thread::spawn(|| {
            sleep(Duration::from_micros(4242));
        });
    }

    // SAFETY: In libc we trust.
    unsafe { _exit(xcode) };
}

/// Gradually allocates memory and exits gracefully when memory is exhausted.
fn do_alloc() -> ! {
    let mut total_allocated: usize = 0;
    let mut allocations: Vec<Vec<u8>> = Vec::new();
    let mut current_alloc_size: usize = 1_024_000; // Start with 1 MB

    loop {
        // Check for integer overflow in allocation size
        let new_alloc_size = match current_alloc_size.checked_mul(2) {
            Some(size) => size,
            None => {
                eprintln!(
                    "Allocation size overflow. Total allocated: {} bytes",
                    syd::human_size(total_allocated)
                );
                exit(Errno::EOVERFLOW as i32);
            }
        };

        // Attempt to allocate memory
        let mut mem_block = vec![0u8; current_alloc_size];
        total_allocated += current_alloc_size;
        println!(
            "Allocated: {} bytes (Total: {} bytes)",
            syd::human_size(current_alloc_size),
            syd::human_size(total_allocated)
        );

        // Use the allocated memory to prevent it from being optimized out
        for byte in mem_block.iter_mut() {
            *byte = 7; // This operation ensures the memory is used
        }

        allocations.push(mem_block);

        // Update the allocation size for next iteration
        current_alloc_size = new_alloc_size;
    }
}
