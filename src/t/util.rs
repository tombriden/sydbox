//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/test/util.rs: Utilities for integration tests
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#![allow(clippy::disallowed_methods)]

use std::{
    env,
    error::Error,
    fmt,
    fs::canonicalize,
    net::{Ipv6Addr, SocketAddrV6, TcpListener},
    process::Command,
    time::Duration,
};

use nix::errno::Errno;
use once_cell::sync::Lazy;

#[derive(Debug)]
pub struct TestError(pub String);
pub type TestResult = Result<(), TestError>;

impl fmt::Display for TestError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl<E: Error> From<E> for TestError {
    fn from(err: E) -> Self {
        TestError(err.to_string())
    }
}

#[macro_export]
macro_rules! assert {
    ($cond:expr) => {
        if !$cond {
            return Err(TestError(format!("Assertion failed: {}", stringify!($cond))));
        }
    };
    ($cond:expr, $($arg:tt)*) => {
        if !$cond {
            return Err(TestError(format!("Assertion failed: {}: {}", stringify!($cond), format_args!($($arg)*))));
        }
    };
}

#[macro_export]
macro_rules! assert_eq {
    ($left:expr, $right:expr) => {
        if $left != $right {
            return Err(TestError(format!("Assertion failed in {}:{}: (left: `{}`, right: `{}`)", file!(), line!(), $left, $right)));
        }
    };
    ($left:expr, $right:expr, $($arg:tt)*) => {
        if $left != $right {
            return Err(TestError(format!("Assertion failed in {}:{}: (left: `{}`, right: `{}`): {}", file!(), line!(), $left, $right, format_args!($($arg)*))));
        }
    };
}

#[macro_export]
macro_rules! assert_ne {
    ($left:expr, $right:expr) => {
        if $left == $right {
            return Err(TestError(format!("Assertion failed in {}:{}: (left: `{}`, right: `{}`)", file!(), line!(), $left, $right)));
        }
    };
    ($left:expr, $right:expr, $($arg:tt)*) => {
        if $left == $right {
            return Err(TestError(format!("Assertion failed in {}:{}: (left: `{}`, right: `{}`): {}", file!(), line!(), $left, $right, format_args!($($arg)*))));
        }
    };
}

#[macro_export]
macro_rules! fixup {
    ($cond:expr) => {
        if $cond {
            return Err(TestError(format!("Known issue fixed in {}:{}", file!(), line!())));
        } else {
            std::env::set_var("SYD_TEST_SOFT_FAIL", "1");
            eprintln!("Warning: Known issue still present in {}:{}", file!(), line!());
        }
    };
    ($cond:expr, $($arg:tt)*) => {
        if $cond {
            return Err(TestError(format!("Known issue fixed in {}:{}: {}", file!(), line!(), format_args!($($arg)*))));
        } else {
            std::env::set_var("SYD_TEST_SOFT_FAIL", "1");
            eprintln!("Warning: Known issue still present in {}:{}: {}", file!(), line!(), format_args!($($arg)*));
        }
    };
}

#[macro_export]
macro_rules! ignore {
    ($cond:expr) => {
        if $cond {
            eprintln!("Warning: Known issue fixed in {}:{}", file!(), line!());
        } else {
            std::env::set_var("SYD_TEST_SOFT_FAIL", "1");
            eprintln!("Warning: Known issue still present in {}:{}", file!(), line!());
        }
    };
    ($cond:expr, $($arg:tt)*) => {
        if $cond {
            eprintln!("Warning: Known issue fixed in {}:{}", file!(), line!());
        } else {
            std::env::set_var("SYD_TEST_SOFT_FAIL", "1");
            eprintln!("Warning: Known issue still present in {}:{}: {}", file!(), line!(), format_args!($($arg)*));
        }
    };
}

/// Holds a `String` to run `syd`.
/// Honours CARGO_BIN_EXE_syd environment variable.
pub static SYD: Lazy<String> =
    Lazy::new(|| env::var("CARGO_BIN_EXE_syd").unwrap_or("syd".to_string()));

/// Holds a `String` to run `syd-chk`.
/// Honours CARGO_BIN_EXE_syd-chk environment variable.
pub static SYD_CHK: Lazy<String> =
    Lazy::new(|| env::var("CARGO_BIN_EXE_syd-chk").unwrap_or("syd-chk".to_string()));

/// Holds a `String` to run `syd-test-do`.
/// Honours CARGO_BIN_EXE_syd-test-do environment variable.
pub static SYD_DO: Lazy<String> = Lazy::new(|| match env::var("CARGO_BIN_EXE_syd-test-do") {
    Ok(var) => var,
    Err(_) => {
        if env::var("SYD_TEST_32").is_ok() {
            "syd-test-do32".to_string()
        } else {
            "syd-test-do".to_string()
        }
    }
});

/// A boolean which specifies if we're running under SourceHut CI.
pub static CI_BUILD: Lazy<bool> = Lazy::new(|| env::var("JOB_ID").ok().is_some());

/// Returns a `Command` to run `syd`.
/// Honours CARGO_BIN_EXE_syd environment variable.
pub fn syd() -> Command {
    static USE_PERF: Lazy<bool> = Lazy::new(|| env::var("SYD_TEST_PERF").ok().is_some());
    static USE_STRACE: Lazy<bool> = Lazy::new(|| env::var("SYD_TEST_STRACE").ok().is_some());
    static USE_VALGRIND: Lazy<bool> = Lazy::new(|| env::var("SYD_TEST_VALGRIND").ok().is_some());
    static USE_HELGRIND: Lazy<bool> = Lazy::new(|| env::var("SYD_TEST_HELGRIND").ok().is_some());
    let mut cmd = Command::new("timeout");
    if check_timeout_foreground() {
        cmd.arg("--foreground");
        cmd.arg("--preserve-status");
        cmd.arg("--verbose");
    }
    cmd.arg("-sKILL");
    cmd.arg(env::var("SYD_TEST_TIMEOUT").unwrap_or("5m".to_string()));
    if *USE_PERF {
        cmd.arg("perf");
        cmd.arg("record");
        cmd.arg("-F99");
        cmd.arg("--call-graph=dwarf");
        cmd.arg("-o/tmp/syd-perf.data"); // FIXME
        cmd.arg("--");
    } else if *USE_STRACE {
        cmd.arg("strace");
        cmd.arg("-f");
        cmd.arg("--");
    } else if *USE_VALGRIND {
        cmd.arg("valgrind");
        cmd.arg("--leak-check=yes");
        cmd.arg("--track-origins=yes");
        cmd.arg("--track-fds=yes");
        cmd.arg("--trace-children=no");
        cmd.arg("--");
    } else if *USE_HELGRIND {
        cmd.arg("valgrind");
        cmd.arg("--tool=helgrind");
        cmd.arg("--");
    }
    cmd.arg(&*SYD);
    cmd.env("RUST_BACKTRACE", "full");
    cmd.env("SYD_LOG", "trace");
    cmd.env("SYD_NO_SYSLOG", "1");
    cmd
}

/// 1. Check if a program exists in PATH
/// 2. Check if "program --version" returns success.
pub fn is_program_available(command: &str) -> bool {
    // Check if the command exists in PATH
    let which_result = Command::new("which").arg(command).status();

    if let Ok(status) = which_result {
        if status.success() {
            // Check if `command --version` executes successfully
            return Command::new(command)
                .arg("--version")
                .status()
                .map(|version_status| version_status.success())
                .unwrap_or(false);
        }
    }

    false
}

/// Resembles the `which` command, finds a program in PATH.
pub fn which(command: &str) -> Result<String, Errno> {
    let out = Command::new("which")
        .arg(command)
        .output()
        .expect("execute which")
        .stdout;
    if out.is_empty() {
        return Err(Errno::ENOENT);
    }

    let bin = String::from_utf8_lossy(&out);
    let bin = bin.trim();
    Ok(canonicalize(bin)
        .map_err(|_| Errno::last())?
        .to_string_lossy()
        .into_owned())
}

/// Check if IPv6 is supported
pub fn check_ipv6() -> bool {
    // Preliminary check for IPv6 availability
    let test_sock = SocketAddrV6::new(Ipv6Addr::LOCALHOST, 0, 0, 0);
    if let Err(error) = TcpListener::bind(test_sock) {
        eprintln!("IPv6 is not available on this system. Skipping test: {error}");
        false
    } else {
        true
    }
}

/// Check if namespaces are supported
/// Returns None if syd process was terminated by a signal.
pub fn check_unshare() -> Option<bool> {
    syd()
        .args(["-ppaludis", "-pcontainer", "true"])
        .status()
        .map(|stat| stat.code())
        .ok()?
        .map(|code| code == 0)
}

/// Check if PID sandboxing is working as expected
/// Returns None if syd process was terminated by a signal.
pub fn check_pid() -> Option<bool> {
    env::set_var("SYD_DO", "fork");
    let result = syd()
        .args([
            "-ppaludis",
            "-pcontainer",
            "-msandbox/pid:on",
            "-mpid/max:1",
            "--",
            &SYD_DO,
            "0",
            "1",
        ])
        .status()
        .map(|stat| stat.code())
        .ok()?
        .map(|code| code == 13);
    env::remove_var("SYD_DO");

    result
}

/// Format a `Duration` into a human readable `String`.
pub fn format_duration(d: Duration) -> String {
    let total_seconds = d.as_secs();
    let hours = total_seconds / 3600;
    let minutes = (total_seconds % 3600) / 60;
    let seconds = total_seconds % 60;

    format!("{}h {}m {}s", hours, minutes, seconds)
}

// Check if timeout --foreground is supported.
fn check_timeout_foreground() -> bool {
    Command::new("timeout")
        .arg("--foreground")
        .arg("-sKILL")
        .arg("60s")
        .arg("true")
        .status()
        .map(|status| status.success())
        .unwrap_or(false)
}
