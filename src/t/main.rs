//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/test/main.rs: Run integration tests with TAP output
//
// Copyright (c) 2023 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    env,
    ffi::CString,
    io,
    ops::Range,
    path::{Path, PathBuf},
    process::{Command, ExitCode},
};

use anyhow::{Context, Result};
use nix::{
    errno::Errno,
    libc::mkdtemp,
    sys::utsname::uname,
    unistd::{chdir, getuid, Uid},
};
use once_cell::sync::Lazy;
use regex::Regex;

mod test;
mod util;
use test::*;

static KERNEL_VERSION: Lazy<(u32, u32)> = Lazy::new(|| {
    // Function to parse the kernel version
    fn parse_kernel_version() -> io::Result<(u32, u32)> {
        // detect kernel version and show warning
        let version = uname().map_err(|e| io::Error::from_raw_os_error(e as i32))?;
        let version = version.release();

        macro_rules! parse_error {
            () => {
                io::Error::new(io::ErrorKind::Other, "unknown version")
            };
        }

        let (major, minor) = {
            let mut iter = version.to_str().ok_or_else(|| parse_error!())?.split('.');
            let major = iter
                .next()
                .ok_or_else(|| parse_error!())?
                .parse::<u32>()
                .map_err(|_| parse_error!())?;
            let minor = iter
                .next()
                .ok_or_else(|| parse_error!())?
                .parse::<u32>()
                .map_err(|_| parse_error!())?;
            (major, minor)
        };
        Ok((major, minor))
    }

    // Call the function and unwrap the result. If the function returns an Err,
    // this will panic, terminating the program.
    #[allow(clippy::disallowed_methods)]
    parse_kernel_version().expect("Failed to parse kernel version")
});

#[derive(Debug)]
struct TempDir {
    path: Option<PathBuf>,
}

impl TempDir {
    fn new<P: AsRef<Path>>(path: P) -> Self {
        TempDir {
            path: Some(path.as_ref().to_path_buf()),
        }
    }
}

impl Drop for TempDir {
    fn drop(&mut self) {
        if let Some(ref path) = self.path {
            match Command::new("rm").arg("-rf").arg(path).status() {
                Ok(status) => {
                    if !status.success() {
                        let path = path.display();
                        eprintln!("Failed to remove temporary directory \"{path}\": rm returned non-zero.");
                    }
                }
                Err(error) => {
                    let path = path.display();
                    eprintln!("Failed to remove temporary directory \"{path}\": {error}.");
                }
            };
        }
    }
}

#[derive(Debug)]
enum Arguments {
    Index(usize),
    Range(Range<usize>),
    Pattern(String),
}

struct ArgVec(Vec<Arguments>);

impl From<String> for Arguments {
    fn from(arg: String) -> Self {
        if let Ok(idx) = arg.parse::<usize>() {
            Arguments::Index(idx)
        } else if let Some(range) = arg.split_once("..") {
            if let (Ok(start), Ok(end)) = (range.0.parse::<usize>(), range.1.parse::<usize>()) {
                Arguments::Range(start..end)
            } else {
                Arguments::Pattern(arg)
            }
        } else {
            Arguments::Pattern(arg)
        }
    }
}

impl From<String> for ArgVec {
    fn from(arg: String) -> Self {
        if let Ok(idx) = arg.parse::<usize>() {
            ArgVec(vec![Arguments::Index(idx)])
        } else if let Some(range) = arg.split_once("..") {
            if let (Ok(start), Ok(end)) = (range.0.parse::<usize>(), range.1.parse::<usize>()) {
                ArgVec(vec![Arguments::Range(start..end)])
            } else {
                ArgVec(vec![Arguments::Pattern(arg)])
            }
        } else {
            ArgVec(vec![Arguments::Pattern(arg)])
        }
    }
}

fn main() -> Result<ExitCode> {
    println!("# syd-test: Welcome to the Machine!");
    println!("# usage: syd -t, --test [<name-regex>|<number>|<number>..<number>]..");
    std::env::set_var("SYD_NO_SYSLOG", "YesPlease");

    // Refuse to run the tests as root.
    // The tests will randomly fail when run with elevated privileges.
    if getuid() == Uid::from_raw(0) {
        println!("# running tests as root.");
        println!("# cowardly refusing to continue!");
        println!("1..0 # SKIP running as root.");
        return Ok(ExitCode::from(1));
    }

    // Create a temporary directory and enter it, failures are OK.
    // The directory is removed when the guard is dropped.
    let _guard = {
        #[allow(clippy::disallowed_methods)]
        let mut tmp = CString::new("/tmp/syd_test_XXXXXX")
            .unwrap()
            .into_bytes_with_nul();
        let ptr = unsafe { mkdtemp(tmp.as_mut_ptr() as *mut _) };
        if !ptr.is_null() {
            let path = unsafe { std::ffi::CStr::from_ptr(ptr).to_str()? };
            match chdir(path) {
                Ok(_) => {
                    println!("# running tests under '{path}'.");
                    env::set_var("HOME", path);
                    Some(TempDir::new(path))
                }
                Err(error) => {
                    println!("# chdir failed: {error}.");
                    None
                }
            }
        } else {
            println!("# mkdtemp failed: {:?}.", Errno::last());
            None
        }
    };

    let mut test_indices = Vec::new();

    // Step 1: Handle the SYD_TEST environment variable.
    let mut test_env_arg = false;
    if let Ok(env) = std::env::var("SYD_TEST") {
        if !env.is_empty() {
            test_env_arg = true;

            let arg: Arguments = env.into();
            match arg {
                Arguments::Index(i) => test_indices.push(i),
                Arguments::Range(r) => test_indices.extend(r),
                Arguments::Pattern(p) => {
                    let regex = Regex::new(&p).context("Bad regex pattern")?;
                    for (idx, (name, _)) in TESTS.iter().enumerate() {
                        #[allow(clippy::disallowed_methods)]
                        let name = name.strip_prefix("test_syd_").unwrap();
                        if regex.is_match(name) {
                            test_indices.push(idx + 1);
                            if p.to_ascii_lowercase().contains("exp") && name.starts_with("exp_") {
                                env::set_var("SYD_TEST_EXPENSIVE", "1");
                            }
                        }
                    }
                }
            }
        }
    }

    // Step 2: Handle command line arguments.
    let args = std::env::args().skip(1).collect::<Vec<_>>();
    let args_is_empty = args.is_empty();
    let args: Vec<Arguments> = args
        .into_iter()
        .map(ArgVec::from)
        .flat_map(|arg_vec| arg_vec.0)
        .collect();
    for arg in args {
        match arg {
            Arguments::Index(i) => test_indices.push(i),
            Arguments::Range(r) => test_indices.extend(r),
            Arguments::Pattern(p) => {
                let regex = Regex::new(&p).context("Bad regex pattern")?;
                for (idx, (name, _)) in TESTS.iter().enumerate() {
                    #[allow(clippy::disallowed_methods)]
                    let name = name.strip_prefix("test_syd_").unwrap();
                    if regex.is_match(name) {
                        test_indices.push(idx + 1);
                        if p.to_ascii_lowercase().contains("exp") && name.starts_with("exp_") {
                            env::set_var("SYD_TEST_EXPENSIVE", "1");
                        }
                    }
                }
            }
        }
    }

    // If SYD_TEST was not set and no arguments are provided,
    // run all tests.
    if !test_env_arg && args_is_empty {
        test_indices.extend(1..=TESTS.len());
    }

    // Print TAP plan.
    if syd::syd_enabled() {
        println!("1..0 # SKIP syd is enabled!");
        return Ok(ExitCode::from(0));
    }
    println!("1..{}", TESTS.len());

    let exp_test = env::var("SYD_TEST_EXPENSIVE").ok().is_some();
    let mut fail_hard = 0;
    let mut fail_soft = 0;
    let mut skip = 0;
    let mut fail_names = Vec::new();
    let mut skip_names = Vec::new();
    let mut soft_fails = Vec::new();
    for (idx, (name, test)) in TESTS.iter().enumerate() {
        #[allow(clippy::disallowed_methods)]
        let name = name.strip_prefix("test_syd_").unwrap();
        if test_indices.contains(&(idx + 1)) {
            if exp_test && !name.starts_with("exp_") {
                println!(
                    "# ok {} - {} # SKIP not an expensive test, unset SYD_TEST_EXPENSIVE to run",
                    idx + 1,
                    name
                );
                skip += 1;
                skip_names.push(name.to_string());
                continue;
            } else if !exp_test && name.starts_with("exp_") {
                println!(
                    "# ok {} - {} # SKIP expensive test, set SYD_TEST_EXPENSIVE or specify test name in arguments to run",
                    idx + 1,
                    name
                );
                skip += 1;
                skip_names.push(name.to_string());
                continue;
            }
            println!("*** {name} ***");
            match test() {
                Ok(_) => {
                    if std::env::var("SYD_TEST_SOFT_FAIL").is_ok() {
                        fail_soft += 1;
                        soft_fails.push(name.to_string());
                        std::env::remove_var("SYD_TEST_SOFT_FAIL");
                        println!("ok {} - {} # TODO", idx + 1, name);
                    } else {
                        println!("ok {} - {}", idx + 1, name);
                    }
                }
                Err(error) => {
                    println!("not ok {} - {} - FAIL: {error}", idx + 1, name);
                    fail_hard += 1;
                    fail_names.push(name.to_string());
                }
            }
        } else {
            println!("ok {} - {} # SKIP skipped by command line", idx + 1, name);
            skip += 1;
            skip_names.push(name.to_string());
        }
    }

    let succ = TESTS.len() - fail_hard - fail_soft - skip;
    println!("# {succ} tests passed.");
    if fail_soft > 0 {
        soft_fails.sort();
        println!("# {fail_soft} tests failed soft: {}", soft_fails.join(", "));
    } else {
        println!("# {fail_soft} tests failed soft.");
    }
    if fail_hard > 0 {
        fail_names.sort();
        println!("# {fail_hard} tests failed hard: {}", fail_names.join(", "));
    } else {
        println!("# {fail_hard} tests failed hard.");
    }
    if !skip_names.is_empty() {
        skip_names.sort();
        println!("# {skip} tests skipped: {}", skip_names.join(", "));
    } else {
        println!("# {skip} tests skipped.");
    }
    Ok(ExitCode::from(fail_hard.try_into().unwrap_or(127)))
}
