//
// syd: seccomp and landlock based application sandbox with support for namespaces
// src/syd-err.rs: Given a number, print the matching errno name and exit.
//                 Given a regex, print case-insensitively matching errno names and exit.
//
// Copyright (c) 2024 Ali Polatel <alip@chesswob.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::process::ExitCode;

use nix::errno::Errno;

fn main() -> ExitCode {
    let mut args = std::env::args();

    match args.nth(1).as_deref() {
        None | Some("-h") => {
            println!("Usage: syd-err number|name-regex");
            println!("Given a number, print the matching errno name and exit.");
            println!("Given a regex, print case-insensitively matching errno names and exit.");
        }
        Some(value) => {
            match value.parse::<u16>() {
                Ok(0) => {
                    return ExitCode::FAILURE;
                }
                Ok(num) => {
                    // number -> name
                    let errno = Errno::from_i32(i32::from(num));
                    if errno == Errno::UnknownErrno {
                        return ExitCode::FAILURE;
                    }
                    let estr = errno.to_string();
                    let mut iter = estr.split(": ");
                    let name = iter.next().unwrap_or("?");
                    let desc = iter.next().unwrap_or("?");
                    println!("{num}\t{name}\t{desc}");
                }
                Err(_) => {
                    match regex::RegexBuilder::new(value).build() {
                        Ok(pattern) => {
                            // regex -> [number]
                            let mut ok = false;
                            for errno in (1..u8::MAX).map(|n| Errno::from_i32(i32::from(n))) {
                                if errno == Errno::UnknownErrno {
                                    continue;
                                }
                                let estr = errno.to_string();
                                let mut iter = estr.split(": ");
                                let name = iter.next().unwrap_or("?");
                                let desc = iter.next().unwrap_or("?");
                                if pattern.is_match(name) {
                                    println!("{}\t{}\t{}", errno as i32, name, desc);
                                    ok = true;
                                }
                            }
                            if !ok {
                                return ExitCode::FAILURE;
                            }
                        }
                        Err(error) => {
                            eprintln!("Invalid errno regex \"{value}\": {error}");
                            return ExitCode::FAILURE;
                        }
                    }
                }
            }
        }
    }

    ExitCode::SUCCESS
}
